<?php
include_once('lib/misFunciones.php');
session_start();
$id_solicitud = "";
if ((isset($_REQUEST["id_solicitud"])) && (isset($_REQUEST["expedientes"]))) {
	$id_solicitud = $_REQUEST["id_solicitud"];
	$aExp = explode(',', $_REQUEST["expedientes"]);
	$tExp = count($aExp)-1;
	$datosSolicitud = getSolicitudXid($id_solicitud);
	$tipo = '';
	if ($datosSolicitud["tipo"] == "0") $tipo = "2"; // 2= consulta	
	if ($datosSolicitud["tipo"] == "1") $tipo = "3"; // 3= censo
	if ($datosSolicitud["tipo"] == "2") $tipo = "4"; // 4=expontanea
	$sql = "UPDATE solicitudes SET status='2', fecha_atendida='" . date("Ymd") . "', hora_atendida='" . date("H:i:s") . "' WHERE id_solicitud='" . $id_solicitud . "' LIMIT 1";
	$ej = ejecutarSQL($sql);
	$datosMedico = getMedicoXid($datosSolicitud['id5']);
	$datosConsultorio = getConsultorioXid($datosSolicitud['id3']);
	$datosServicio = getServicioXid($datosSolicitud['id4']);
	$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="750">';
	$out .= '<tr><td colspan="3" class="tituloVentana">Solicitud Atendida ' . ponerCeros($datosSolicitud["id_solicitud"],12) . '</td></tr>';
	$out .= '<tr class="tituloVentana"><td colspan="3">' . ponerAcentos($datosConsultorio['nombre']) . ' -> ' . ponerAcentos($datosServicio) . ' -> ' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td></tr>';
	for ($i=0; $i<$tExp; $i++) {
		$detallesSolicitud = getDetallesXidExpediente($id_solicitud, $aExp[$i]);
		$datosDerecho = getDatosDerecho($aExp[$i]);
		$out .= '<tr class="renglonSolicitudes"><td width="150">' . formatoDia($detallesSolicitud["fecha"],"fecha") . ' - ' . $detallesSolicitud["hora"] . '<br>' . ponerCeros($detallesSolicitud["id_expediente"], 8) . '</td><td align="left" width="250">' . ponerAcentos($datosDerecho["ap_p"]) . ' ' . ponerAcentos($datosDerecho["ap_m"]) . ' ' . ponerAcentos($datosDerecho["nombres"]) . '<br>' . $datosDerecho["cedula"] . '/' . $datosDerecho["cedula_tipo"] . '</td><td align="left" width="350">entregado</td></tr>';
		$sql = "UPDATE solicitudes_detalles SET status='1' WHERE id_solicitud='" . $id_solicitud . "' AND id_expediente='" . $aExp[$i] . "' LIMIT 1";
		$ej = ejecutarSQL($sql);
		$sql = "UPDATE expedientes SET status='2' WHERE id_expediente='" . $aExp[$i] . "' LIMIT 1";
		$ej = ejecutarSQL($sql);
		$sql = "INSERT INTO expedientes_movimientos VALUES(NULL, '" . $aExp[$i] . "', '1', '" . date("Ymd") . "', '" . date("H:i:s") . "', '" . $tipo . "', '', '', '', '1', '" . $id_solicitud . "');"; // 1=ubicacion archivo         1= en transito
		$ej = ejecutarSQL($sql);
	}
	$out .= '</table></center>';
	print($out);
} else {
	print("error");
}

?>

