<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
set_time_limit(300);
include 'lib/misFunciones.php';
if (isset($_REQUEST['id_derecho'])) {
    $id_expediente = $_REQUEST['id_derecho'];
    $expediente = getExpedienteXid($id_expediente);
    $ret = '<br><center><table border="0" cellpadding="5" cellspacing="0" width="700">';
    $ubicacion="&nbsp;";
    if (count($expediente) > 0) {
        $solicitudes = getDetallesXidExp($id_expediente);
        $numMovs = count($solicitudes);
        if ($numMovs > 0) {
            $movimientos = '<table width="700" border="1" cellspacing="0" cellpadding="0" class="ventana">        
								<tr>
								  <td colspan="5" class="tituloVentana">DETALLES DE MOVIMIENTOS</td>
								</tr>
								<tr>
                                                                <th align="center" class="AzulN">Movimiento</th>
								  <th align="center" class="AzulN">Fecha</th><th align="center" class="AzulN">Hora</th><th align="center" class="AzulN">Origen</th><th align="center" class="AzulN">Destino</th>
								</tr>';
            for ($i = 0; $i <= $numMovs - 1; $i++) {
                $detsolicitud = $solicitudes[$i];
				if ($detsolicitud["status"] > 0) { // que si se haya enviado o recibido
					$solicitud = getSolicitudXid($detsolicitud['id_solicitud']);
					$movXSol = getMovXidSolicitud($detsolicitud['id_solicitud']);
					if ($detsolicitud != -1) {
						if ($solicitud['tipo'] != 1) {
							if ($detsolicitud['extra'] == "E") {
								$cita = getCitaExt($detsolicitud['id_cita']);
								if (count($cita) > 0) {
									$medico = getMedicoXid($cita['id_medico']);
									$servicio = getServicioXid($cita['id_servicio']);
								} else {
									$medico = getMedicoXid($solicitud['id3']);
									$servicio = getServicioXid($solicitud['id5']);
								}
								$ubicacion = "Prestamo de Ventanilla a " . ponerAcentos($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']) . "-" . $servicio;
							} else {
								$servicio = getServicioXid($solicitud['id4']);
								$medico = getMedicoXid($solicitud['id5']);
								$ubicacion = "Consulta de $servicio - " . $medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'];
							}
						} else {
							$cama = ConseguirCamaPaciente($id_expediente, $movimiento['fecha_ua'], $movimiento['fecha_ud']);
							$ubi = explode("|", $cama);
							$ubicacion = "Piso " . $ubi[1] . "Cama " . $ubi[0];
						}
					} else {
						$bloque = obtenerBloqueCon($id_expediente);
						$ubicacion = "Bloque " . $bloque;
					}
					$movimiento = "";
					if (count($movXSol) > 0) {
						if ($solicitud['status'] == 5) {
							$movimientos .= '<tr><td align="center" class="Azul" width="100">' . ponerCeros($solicitud["id_solicitud"], 12) . '</td><td align="center" class="Azul" width="70">' . formatoDia($movXSol["fecha_ud"], "fecha") . '</td><td align="center" class="Azul" width="60">' . $movXSol["hora_ud"] . '</td><td align="center" class="Azul" width="80">-</td><td align="center" class="Azul">Regreso a Archivo</td></tr>';
						}
						$movimientos .= '<tr><td align="center" class="Azul" width="100">' . ponerCeros($solicitud["id_solicitud"], 12) . '</td><td align="center" class="Azul" width="70">' . formatoDia($movXSol["fecha_ua"], "fecha") . '</td><td align="center" class="Azul" width="60">' . $movXSol["hora_ua"] . '</td><td align="center" class="Azul" width="80">Archivo</td><td align="center" class="Azul">' . $ubicacion . '</td></tr>';
					} else {
						if ($solicitud['status'] == 5) {
							$movimientos .= '<tr><td align="center" class="Azul" width="100">' . ponerCeros($solicitud["id_solicitud"], 12) . '</td><td align="center" class="Azul" width="70">' . formatoDia($solicitud["fecha_atendida"], "fecha") . '</td><td align="center" class="Azul" width="60">' . $solicitud["hora_atendida"] . '</td><td align="center" class="Azul" width="80">&nbsp;</td><td align="center" class="Azul">Regreso a Archivo</td></tr>';
						}
						$movimientos .= '<tr><td align="center" class="Azul" width="100">' . ponerCeros($solicitud["id_solicitud"], 12) . '</td><td align="center" class="Azul" width="70">' . formatoDia($solicitud["fecha_creacion"], "fecha") . '</td><td align="center" class="Azul" width="60">' . $solicitud["hora_creacion"] . '</td><td align="center" class="Azul" width="80">Archivo</td><td align="center" class="Azul">' . $ubicacion . '</td></tr>';
					}
					$movimientos.=$movimiento;
					if($i==0)
					$ultimaUbicacion=$ubicacion;
				}
            }
        }
        else
            $movimientos = '&nbsp;';
        switch ($expediente["status"]) {
            case 0:
                $estado = "cancelado";
                break;
            case 1:
                $estado = "Presente en ARCHIVO";
                break;
            case 2:
                $estado = "Fuera de archivo";
                break;
            case 3:
                $estado = "concentrado";
                break;
        }
        $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left" width="120">Status: </td><td align="left"><b>Expediente Creado</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="imp_' . $expediente["id_expediente"] . '" id="imp_' . $expediente["id_expediente"] . '" onclick="javascript: expedientesImprimir(\'' . $expediente["id_expediente"] . '\');" value="Imprimir Etiqueta"></td></tr>';
        $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left">C&oacute;digo de Barras: </td><td align="left"><b>' . ponerCeros($expediente["id_expediente"], 8) . '</b></td></tr>';
        $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left">Ubicaci&oacute;n Actual: </td><td align="left"><b>' . $estado . "-" . $ultimaUbicacion . '</b></td></tr>';
        $ret .= '<tr><td align="left" colspan="2">' . $movimientos . '</td></tr>';
    } else { // no existe el expediente
        $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left" width="70">Status: </td><td align="left"><b>No se ha creado el expediente</b></td></tr>';
    }
    $exp = getDatosDerecho($id_expediente);
    $ret .= '</table></center>';
    print($ret);
} else {
    print("error");
}
?>
