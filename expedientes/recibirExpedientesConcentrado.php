<?php
include_once('lib/misFunciones.php');
session_start ();

$id_expediente = '';
$ret = '';

function getRenglon($id_expediente, $expedientes) {
	$estado = '';
	$ubicacion = '';
	$ret = '';
	$aExpedientes = explode(",", $expedientes);
	$expediente = getExpedienteXid($id_expediente);
	if (count($expediente) > 0) { // si existe el expediente
		array_unshift($aExpedientes, $id_expediente);
	}
	$aExpedientes = array_unique($aExpedientes);
	$tExp = count($aExpedientes);
	if ($tExp > 1) { //no esta vacio o tiene solo el que se está regresando
		$tExp2 = $tExp-1;
		foreach ($aExpedientes as $indice => $valor) {
			if (is_numeric($valor)) {
				$estado = '';
				$ubicacion = '';
				$expediente = getExpedienteXid($valor);
				$solicitudes_detalles = getDetallesXidExp($valor);
				$tDetalles = count($solicitudes_detalles);
				if ($tDetalles > 0) {
					$solicitud = getSolicitudXid($solicitudes_detalles[0]["id_solicitud"]);
					if ($solicitudes_detalles[0]["id_cita"] == "0") { // es un prestamo en ventanilla
						$medico = getMedicoXid($solicitud["id3"]);
						$ubicacion = ' - de Ventanilla a ' . ponerAcentos($medico["ap_p"] . ' ' . $medico["ap_m"]. ' ' . $medico["nombres"]);
					} else { // es una cita
						$consultorio = getConsultorioXid($solicitud["id3"]);
						$servicio = getServicioXid($solicitud["id4"]);
						$medico = getMedicoXid($solicitud["id5"]);
						$ubicacion = ' - ' . ponerAcentos($consultorio["nombre"]) . ' ' . ponerAcentos($servicio) . ' ' . ponerAcentos($medico["ap_p"] . ' ' . $medico["ap_m"]. ' ' . $medico["nombres"]);
					}
				}
				if ($expediente["status"] == 0) $estado = "Cancelado";
				if ($expediente["status"] == 1) $estado = "Presente en ARCHIVO";
				if ($expediente["status"] == 2) $estado = "Fuera de archivo";	
				$derecho = getDatosDerecho($expediente["id_expediente"]);
				$ret .= '<tr class="renglonSolicitudes" height="24"><td align="left"><input onclick="return false;" type="checkbox" id="exp_' . $expediente["id_expediente"] . '_' . $solicitudes_detalles[0]["id_solicitud"] . '" name="exp_' . $expediente["id_expediente"] . '_' . $solicitudes_detalles[0]["id_solicitud"] . '" checked="checked">' . ponerCeros($expediente["id_expediente"],8) . '</td><td align="left">' . $derecho["cedula"] . '/' . $derecho["cedula_tipo"] . " - " . ponerAcentos($derecho["ap_p"]) . " " . ponerAcentos($derecho["ap_m"]) . " " . ponerAcentos($derecho["nombres"]) . " " . '</td><td align="left">' . ponerCeros($solicitudes_detalles[0]["id_solicitud"],12) . '</td><td align="left"><b>' . $estado . $ubicacion . '</b></td><td>' . formatoDia($solicitud["fecha_creacion"], "fecha") . '</td></tr>';
			}
		}	
	}
	return $ret;
}


if (isset($_REQUEST["cod_bar"])) {
	$id_expediente = (int)$_REQUEST["cod_bar"];
	$ret .= '<center><table border="0" cellpadding="5" cellspacing="0" width="700">';
	$ret .= '<tr height="18"><td align="center" class="AzulN" width="100">Expediente</td><td align="center" class="AzulN">Derechohabiente</td><td align="center" class="AzulN">Solicitud</td><td align="center" class="AzulN">Estado</td><td align="center" class="AzulN">Fecha</td></tr>';
	$ret .= getRenglon($id_expediente, $_REQUEST["expedientes"]);
	$ret .= '</table></center>';
	print($ret);
} else {
	print("error");
}

?>

