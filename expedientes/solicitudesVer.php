<?php
include_once('lib/misFunciones.php');
session_start ();

$id_solicitud = "";
if (isset($_REQUEST["id_solicitud"])) {
	$id_solicitud = $_REQUEST["id_solicitud"];
	$datosSolicitud = getSolicitudXid($id_solicitud);
	$detallesSolicitud = getDetallesXidSolicitud($datosSolicitud['id_solicitud']);
	$tDetalles = count($detallesSolicitud);
	$datosMedico = getMedicoXid($datosSolicitud['id5']);
	$datosConsultorio = getConsultorioXid($datosSolicitud['id3']);
	$datosServicio = getServicioXid($datosSolicitud['id4']);
	$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="750">';
	$out .= '<tr><td colspan="3" class="tituloVentana">Solicitud de Expedientes ' . ponerCeros($datosSolicitud["id_solicitud"],12) . '</td></tr>';
	$out .= '<tr class="tituloVentana"><td colspan="3">' . ponerAcentos($datosConsultorio['nombre']) . ' -> ' . ponerAcentos($datosServicio) . ' -> ' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td></tr>';
	for ($i=0; $i<$tDetalles; $i++) {
		$datosDerecho = getDatosDerecho($detallesSolicitud[$i]['id_expediente']);
		$opciones = getOpcionesExpedienteVer($detallesSolicitud[$i]['id_expediente']);
		$out .= '<tr class="renglonSolicitudes"><td width="150">' . formatoDia($detallesSolicitud[$i]["fecha"],"fecha") . ' - ' . $detallesSolicitud[$i]["hora"] . '<br>' . ponerCeros($detallesSolicitud[$i]["id_expediente"], 8) . '</td><td align="left" width="250">' . ponerAcentos($datosDerecho["ap_p"]) . ' ' . ponerAcentos($datosDerecho["ap_m"]) . ' ' . ponerAcentos($datosDerecho["nombres"]) . '<br>' . $datosDerecho["cedula"] . '/' . $datosDerecho["cedula_tipo"] . '</td><td align="left" width="350">' . $opciones . '</td></tr>';
	}
	$out .= '</table></center>';
	print($out);
} else {
	print("error");
}

?>

