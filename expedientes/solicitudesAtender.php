<?php
include_once('lib/misFunciones.php');
$id_solicitud = "";
if (isset($_REQUEST["id_solicitud"])) {
	$id_solicitud = $_REQUEST["id_solicitud"];
	if (is_numeric($id_solicitud)) {
	$datosSolicitud = getSolicitudXid($id_solicitud);
	$detallesSolicitud = getDetallesXidSolicitud($datosSolicitud['id_solicitud']);
	$tDetalles = count($detallesSolicitud);
	$datosMedico = getMedicoXid($datosSolicitud['id5']);
	$datosConsultorio = getConsultorioXid($datosSolicitud['id3']);
	$datosServicio = getServicioXid($datosSolicitud['id4']);
	$out = '<form name="forma" id="forma"><center><table border="0" cellpadding="0" cellspacing="0" width="750">';
	$out .= '<tr><td colspan="3" class="tituloVentana">Solicitud de Expedientes ' . ponerCeros($datosSolicitud["id_solicitud"],12) . '</td></tr>';
	$out .= '<tr class="tituloVentana"><td colspan="3">' . ponerAcentos($datosConsultorio['nombre']) . ' -> ' . ponerAcentos($datosServicio) . ' -> ' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td></tr>';
	for ($i=0; $i<$tDetalles; $i++) {
		$datosDerecho = getDatosDerecho($detallesSolicitud[$i]['id_expediente']);
		$opciones = getOpcionesExpediente($detallesSolicitud[$i]['id_expediente']);
		$out .= '<tr class="renglonSolicitudes"><td width="150">' . formatoDia($detallesSolicitud[$i]["fecha"],"fecha") . ' - ' . $detallesSolicitud[$i]["hora"] . '<br>' . ponerCeros($detallesSolicitud[$i]["id_expediente"], 8) . '</td><td align="left" width="250">' . ponerAcentos($datosDerecho["ap_p"]) . ' ' . ponerAcentos($datosDerecho["ap_m"]) . ' ' . ponerAcentos($datosDerecho["nombres"]) . '<br>' . $datosDerecho["cedula"] . '/' . $datosDerecho["cedula_tipo"] . '</td><td align="left" width="350">' . $opciones . '</td></tr>';
	}
	$out .= '<tr><td align="center" height="50" colspan="3"><input type="button" id="aceptar" name="aceptar" value="A C E P T A R" onclick="javascript: solicitudAtenderAceptar(' . $_REQUEST["id_solicitud"] . ');"></td></tr>';
	$out .= '</table></center></form>';
	} else
		$out = "<center>La solicitud no est&aacute; activa</center>";
	print($out);
} else {
	print("error");
}

?>

