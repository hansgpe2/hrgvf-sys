<?php

include_once('lib/misFunciones.php');

if (isset($_REQUEST["expedientes"])) {
    $aExp = explode(',', $_REQUEST["expedientes"]);
    $aSol = explode(',', $_REQUEST["solicitudes"]);
    $tExp = count($aExp) - 1;
    $out = '<center><table border="0" cellpadding="0" cellspacing="0" width="750">';
    $out .= '<tr><td colspan="3" class="tituloVentana">Expedientes Recibidos</td></tr>';
    if ($tExp > 0) {
        for ($i = 0; $i < $tExp; $i++) {
            $sql = "select * from expedientes_movimientos where id_expediente=" . $aExp[$i] . " order by fecha_ua desc Limit 1";
            $datosExp = ejecutarSQL($sql);
            $ubicacion = $datosExp['ubicacion_destino'];
            switch ($ubicacion) {
/*
                case 4://Si el expediente salio por expontanea
                    $sql = "select * from solicitudes_detalles where id_expediente=" . $aExp[$i] . " and (id_cita=0 or extra='E') order by id_solicitud DESC limit 1";
                    $datosSolicitud = ejecutarSQL($sql);
                    $solicitud = $datosSolicitud['id_solicitud'];
                    $sql = "UPDATE solicitudes_detalles SET status='2' WHERE id_solicitud='" . $solicitud . "' AND id_expediente='" . $aExp[$i] . "' LIMIT 1"; // 2 =recibido
                    $ej = ejecutarSQL($sql);
                    $sql = "UPDATE solicitudes SET status='5' WHERE id_solicitud='" . $solicitud . "' LIMIT 1";
                    $ej = ejecutarSQL($sql);
                    break;
*/
                case 5://Si es concentrado
                    $sql = "select * from solicitudes_detalles where id_expediente=" . $aExp[$i] . " and (id_cita=-1) order by id_solicitud DESC limit 1";
                    $datosSolicitud = ejecutarSQL($sql);
                    $solicitud = $datosSolicitud['id_solicitud'];
                    $sql = "UPDATE solicitudes_detalles SET status='2' WHERE id_solicitud='" . $solicitud . "' AND id_expediente='" . $aExp[$i] . "' LIMIT 1"; // 2 =recibido
                    $ej = ejecutarSQL($sql);
                    $sql = "UPDATE solicitudes SET status='5' WHERE id_solicitud='" . $solicitud . "' LIMIT 1";
                    $ej = ejecutarSQL($sql);
                    break;
                default :
                    $sql = "UPDATE solicitudes_detalles SET status='2' WHERE id_solicitud='" . $aSol[$i] . "' AND id_expediente='" . $aExp[$i] . "' LIMIT 1"; // 2 =recibido
                    $ej = ejecutarSQL($sql);
                    $sql = "UPDATE solicitudes SET status='5' WHERE id_solicitud='" . $aSol[$i] . "' LIMIT 1";
                    $ej = ejecutarSQL($sql);
            }
            $sql = "UPDATE expedientes_movimientos SET status='2', fecha_ud='" . date("Ymd") . "', hora_ud='" . date("H:i:s") . "' WHERE id_expediente='" . $aExp[$i] . "' AND extra='" . $aSol[$i] . "'"; // 2=entregado extra = id_solicitud
            $ej = ejecutarSQL($sql);
            $sql = "UPDATE expedientes SET status='1', id_ubicacion=1 WHERE id_expediente='" . $aExp[$i] . "' LIMIT 1";
            $ej = ejecutarSQL($sql);
            $exp = getMovimiento($aExp[$i]);
            $estado = $exp['status'];
            switch ($estado) {
                case 0:
                    $estado = "cancelado";
                    break;
                case 1:
                    $estado = "en transito";
                    break;
                case 2:
                    $estado = "entregado";
                    break;
                case 3:
                    $estado = "concentrado";
                    break;
            }
            $ubicacion = $exp['ubicacion_actual'];
            $ubicacion = ejecutarSQL("select nombre from ubicaciones where id_ubicacion=$ubicacion");
            $ubicacion = htmlentities($ubicacion['nombre']);
            $out .= '<tr class="renglonSolicitudes" height="24"><td align="left">' . ponerCeros($aExp[$i], 8) . '</td><td align="left">' . ponerCeros($aSol[$i], 12) . '</td><td align="left"><b>' . $estado . "-" . $ubicacion . '</b></td></tr>';
        }
    }
    $out .= '</table></center>';
    print($out);
} else {
    print("error");
}
?>

