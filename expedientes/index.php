<?php
session_start ();
$_SESSION['idUsuario'] = "-1";
$_SESSION['tipoUsuario'] = "-1";
$_SESSION['IdCon'] = "-1";
$_SESSION['idServ'] = "-1";
$_SESSION['idDr'] = "-1";
include_once('lib/misFunciones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>ISSSTE - Control de Expedientes</title>

<link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
<link href="lib/impresion.css" media="print" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="lib/themes/default.css">
<link rel="stylesheet" href="lib/themes/default.date.css">


    <script src="lib/jquery-1.7.1.js"></script>
    <script type='text/javascript' src='lib/arreglos.js'></script>
    <script src="lib/picker.js"></script>
    <script src="lib/picker.date.js"></script>
    <script src="lib/legacy.js"></script>

</head>

<body bgcolor="#EEEEEE" onload="javascript: obtenerLogin();">
<center>
  <table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="diseno/logoEncabezado.jpg" width="104" height="108" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Expedientes</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    
    <table id="centro" class="centro" width="800" height="499">
    <tr><td align="center" valign="top">
      <table width="800">
      	<tr><td><div id="menu" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicio.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="250">&nbsp;</td>
                    	<td width="100" align="center"><a href="javascript: reportes();" title="Reportes" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reportes</a></td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuArchivo" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="100"><a href="javascript: inicio('inicioArchivo.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="70" align="center"><a href="javascript: recibirExpedientes();" title="recibir expedientes" class="botones_menu"><img src="diseno/regresar_expontaneo.jpg" width="40" height="40" border="0" /><br>Recibir Expedientes</a></td>
                    	<td width="70" align="center"><a href="javascript: buscarExpedientes();" title="buscar expedientes" class="botones_menu"><img src="diseno/buscar.png" width="40" height="40" border="0" /><br>Buscar Expedientes</a></td>
                        <td width="80" align="center"><a href="javascript: buscar();" title="Buscar Cita" class="botones_menu"><img src="diseno/buscar.png" width="40" height="40" border="0" /><br>Buscar Cita</a></td>
                    	<td width="75">&nbsp;</td>
                        <td width="125" align="center"><a href="javascript: prestarExpontanea();" title="Prestar de Ventanilla" class="botones_menu"><img src="diseno/prestar_expontaneo.jpg" width="40" height="40" border="0" /><br>Prestar de Ventanilla</a></td>
                        <td width="70"><a href="javascript:concentrarExpedientes();" class="botones_menu"><img src="diseno/concentrar_exp.png" width="40" height="40" border="0" /><br />Concentrar Expedientes</a></td>
                    	<td width="70" align="center"><a href="javascript: verCancelados();" title="ver cancelados" class="botones_menu"><img src="diseno/_medRecDel.gif" width="40" height="40" border="0" /><br>Solicitudes Canceladas</a></td>
                    	<td width="80">&nbsp;</td>
                    	<td width="70" align="center"><a href="javascript: verReportes();" title="ver reportes" class="botones_menu"><img src="diseno/_medRecAviso.gif" width="40" height="40" border="0" /><br>Reportes</a></td>
                        <td width="70" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuExterna" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicioExterna.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="250"><a href="javascript: buscarFolios();" title="Buscar Folios" class="botones_menu"><img src="diseno/buscar.png" width="40" height="40" border="0" /><br>Buscar Folios</a></td>
                    	<td width="100" align="center">&nbsp;</td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuFarmacia" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicioFarmacia.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="200"><a href="javascript: verTodasRecetas();" title="Ver todas las recetas" class="botones_menu"><img src="diseno/_medRecAviso.gif" width="40" height="40" border="0" /><br>Ver Todas</a></td>
                    	<td width="200"><a href="javascript: cargarExistencias();" title="Cargar Existencias" class="botones_menu"><img src="diseno/_medAdd.gif" width="40" height="40" border="0" /><br>Cargar Existencias</a></td>
                    	<td width="100" align="center">&nbsp;</td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuColectivos" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicioColectivos.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="200">&nbsp;</td>
                    	<td width="200">&nbsp;</td>
                    	<td width="100" align="center">&nbsp;</td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuVisor" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicioVisor.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="200">&nbsp;</td>
                    	<td width="200">&nbsp;</td>
                    	<td width="100" align="center">&nbsp;</td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
        </td></tr>
        <tr><td align="center"><br />
		      	<div id="contenido">
      			</div>
      	</td></tr>
      </table>
    </td></tr></table>
</td></tr></table>
</center>

</body>
</html>
