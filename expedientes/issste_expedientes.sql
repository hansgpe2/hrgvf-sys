-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 28-02-2012 a las 17:35:05
-- Versión del servidor: 5.5.9
-- Versión de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `issste_expedientes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expedientes`
--

DROP TABLE IF EXISTS `expedientes`;
CREATE TABLE `expedientes` (
  `id_expediente` int(11) NOT NULL,
  `fecha_creacion` varchar(8) NOT NULL,
  `hora_creacion` varchar(8) NOT NULL,
  `id_usuario_creacion` int(11) NOT NULL,
  `impresiones` int(11) NOT NULL,
  `id_ubicacion` int(11) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT '0=eliminado, 1=presente, 2=en_curso',
  `extra` varchar(1) NOT NULL,
  PRIMARY KEY (`id_expediente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `expedientes`
--

INSERT INTO `expedientes` VALUES(20064, '20120228', '16:28:15', 1, 0, 1, '1', '');
INSERT INTO `expedientes` VALUES(63724, '20120228', '16:35:55', 1, 0, 1, '1', '');
INSERT INTO `expedientes` VALUES(85355, '20120228', '16:30:40', 1, 0, 1, '1', '');
INSERT INTO `expedientes` VALUES(87876, '20120228', '17:08:29', 1, 0, 1, '1', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expedientes_movimientos`
--

DROP TABLE IF EXISTS `expedientes_movimientos`;
CREATE TABLE `expedientes_movimientos` (
  `id_movimiento` int(11) NOT NULL AUTO_INCREMENT,
  `id_expediente` int(11) NOT NULL,
  `ubicacion_actual` int(11) NOT NULL,
  `fecha_ua` varchar(8) NOT NULL COMMENT 'fecha ubicacion actual',
  `hora_ua` varchar(8) NOT NULL COMMENT 'hora ubicacion actual',
  `ubicacion_destino` int(11) NOT NULL,
  `fecha_ud` varchar(8) NOT NULL COMMENT 'fecha ubicacion destino',
  `hora_ud` varchar(8) NOT NULL COMMENT 'hora ubicacion destino',
  `observaciones` varchar(250) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT '0=cancelado, 1=en_transito, 2=entregado',
  `extra` varchar(1) NOT NULL,
  PRIMARY KEY (`id_movimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `expedientes_movimientos`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Volcar la base de datos para la tabla `logs`
--

INSERT INTO `logs` VALUES(1, '1|ingreso al sistema 15:01 24/02/2012', '0');
INSERT INTO `logs` VALUES(2, '1|ingreso al sistema 15:06 24/02/2012', '0');
INSERT INTO `logs` VALUES(3, '1|ingreso al sistema 15:07 24/02/2012', '0');
INSERT INTO `logs` VALUES(4, '1|salio del sistema 15:07 24/02/2012', '8');
INSERT INTO `logs` VALUES(5, '1|ingreso al sistema 15:07 24/02/2012', '0');
INSERT INTO `logs` VALUES(6, '1|salio del sistema 15:31 24/02/2012', '8');
INSERT INTO `logs` VALUES(7, '1|ingreso al sistema 15:33 24/02/2012', '0');
INSERT INTO `logs` VALUES(8, '1|salio del sistema 15:34 24/02/2012', '8');
INSERT INTO `logs` VALUES(9, '1|ingreso al sistema 12:10 27/02/2012', '0');
INSERT INTO `logs` VALUES(10, '1|ingreso al sistema 10:34 28/02/2012', '0');
INSERT INTO `logs` VALUES(11, '1|ingreso al sistema 10:48 28/02/2012', '0');
INSERT INTO `logs` VALUES(12, '1|salio del sistema 11:28 28/02/2012', '8');
INSERT INTO `logs` VALUES(13, '1|ingreso al sistema 11:30 28/02/2012', '0');
INSERT INTO `logs` VALUES(14, '1|ingreso al sistema 11:33 28/02/2012', '0');
INSERT INTO `logs` VALUES(15, '1|ingreso al sistema 11:52 28/02/2012', '0');
INSERT INTO `logs` VALUES(16, '1|ingreso al sistema 11:52 28/02/2012', '0');
INSERT INTO `logs` VALUES(17, '1|ingreso al sistema 11:55 28/02/2012', '0');
INSERT INTO `logs` VALUES(18, '1|ingreso al sistema 11:56 28/02/2012', '0');
INSERT INTO `logs` VALUES(19, '1|ingreso al sistema 12:00 28/02/2012', '0');
INSERT INTO `logs` VALUES(20, '1|ingreso al sistema 12:02 28/02/2012', '0');
INSERT INTO `logs` VALUES(21, '1|ingreso al sistema 12:03 28/02/2012', '0');
INSERT INTO `logs` VALUES(22, '1|ingreso al sistema 12:04 28/02/2012', '0');
INSERT INTO `logs` VALUES(23, '1|ingreso al sistema 12:05 28/02/2012', '0');
INSERT INTO `logs` VALUES(24, '1|ingreso al sistema 12:07 28/02/2012', '0');
INSERT INTO `logs` VALUES(25, '1|ingreso al sistema 12:11 28/02/2012', '0');
INSERT INTO `logs` VALUES(26, '1|ingreso al sistema 12:13 28/02/2012', '0');
INSERT INTO `logs` VALUES(27, '1|ingreso al sistema 12:13 28/02/2012', '0');
INSERT INTO `logs` VALUES(28, '1|ingreso al sistema 13:00 28/02/2012', '0');
INSERT INTO `logs` VALUES(29, '1|ingreso al sistema 15:45 28/02/2012', '0');
INSERT INTO `logs` VALUES(30, '1|ingreso al sistema 15:46 28/02/2012', '0');
INSERT INTO `logs` VALUES(31, '1|ingreso al sistema 15:48 28/02/2012', '0');
INSERT INTO `logs` VALUES(32, '1|ingreso al sistema 15:49 28/02/2012', '0');
INSERT INTO `logs` VALUES(33, '1|ingreso al sistema 15:59 28/02/2012', '0');
INSERT INTO `logs` VALUES(34, '1|ingreso al sistema 16:03 28/02/2012', '0');
INSERT INTO `logs` VALUES(35, '1|ingreso al sistema 16:14 28/02/2012', '0');
INSERT INTO `logs` VALUES(36, '1|ingreso al sistema 16:28 28/02/2012', '0');
INSERT INTO `logs` VALUES(37, '1|ingreso al sistema 16:30 28/02/2012', '0');
INSERT INTO `logs` VALUES(38, '1|ingreso al sistema 17:07 28/02/2012', '0');
INSERT INTO `logs` VALUES(39, '1|ingreso al sistema 17:23 28/02/2012', '0');
INSERT INTO `logs` VALUES(40, '1|salio del sistema 17:23 28/02/2012', '8');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes`
--

DROP TABLE IF EXISTS `solicitudes`;
CREATE TABLE `solicitudes` (
  `id_solicitud` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` varchar(8) NOT NULL,
  `hora_creacion` varchar(8) NOT NULL,
  `fecha_atendida` varchar(8) NOT NULL,
  `hora_atendida` varchar(8) NOT NULL,
  `tipo` varchar(1) NOT NULL COMMENT '0=consulta_externa 1=censo 2=expontanea',
  `status` varchar(1) NOT NULL COMMENT '0=cancelada 1=solicitada 2=en_transito 3=recibida 4=transferida 5=atendida(de regreso a archivo)',
  `id1` int(11) NOT NULL COMMENT 'id usuario solicitante',
  `id2` int(11) NOT NULL COMMENT 'id usuario entrega',
  `id3` int(11) NOT NULL COMMENT 'no de empleado si es expontanea o id consultorio si es consulta .. falta ver que id si es censo',
  `id4` int(11) NOT NULL COMMENT 'id servicio si es consulta.. falta ver que id si es censo',
  `id5` int(11) NOT NULL COMMENT 'id dr si es consulta... falta ver que id si es censo',
  `extra` varchar(1) NOT NULL,
  PRIMARY KEY (`id_solicitud`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `solicitudes`
--

INSERT INTO `solicitudes` VALUES(1, '20120227', '12:07:48', '', '', '0', '1', 4, 0, 16, 54, 105, '');
INSERT INTO `solicitudes` VALUES(2, '20120228', '11:29:31', '', '', '0', '1', 4, 0, 16, 54, 106, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes_detalles`
--

DROP TABLE IF EXISTS `solicitudes_detalles`;
CREATE TABLE `solicitudes_detalles` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_solicitud` int(11) NOT NULL,
  `id_expediente` int(11) NOT NULL,
  `id_cita` int(11) NOT NULL,
  `fecha` varchar(8) NOT NULL,
  `hora` varchar(8) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT '0=por buscar 1=enviado 2=recibido',
  `extra` varchar(1) NOT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Volcar la base de datos para la tabla `solicitudes_detalles`
--

INSERT INTO `solicitudes_detalles` VALUES(1, 1, 20064, 888707, '20120227', '12:07:48', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(2, 1, 85355, 883904, '20120227', '12:07:48', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(3, 1, 63724, 874455, '20120227', '12:07:48', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(4, 1, 79800, 888650, '20120227', '12:07:48', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(5, 1, 87876, 884186, '20120227', '12:07:48', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(6, 2, 9019, 900577, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(7, 2, 69190, 873741, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(8, 2, 86081, 812500, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(9, 2, 2225, 822189, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(10, 2, 33705, 887876, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(11, 2, 59686, 846159, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(12, 2, 33132, 846161, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(13, 2, 62484, 900725, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(14, 2, 25588, 889325, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(15, 2, 89794, 888765, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(16, 2, 13306, 887676, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(17, 2, 17818, 837191, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(18, 2, 83535, 858672, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(19, 2, 83536, 862320, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(20, 2, 84666, 871739, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(21, 2, 65485, 900621, '20120228', '11:29:31', '0', '');
INSERT INTO `solicitudes_detalles` VALUES(22, 2, 15751, 900652, '20120228', '11:29:31', '0', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicaciones`
--

DROP TABLE IF EXISTS `ubicaciones`;
CREATE TABLE `ubicaciones` (
  `id_ubicacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(25) NOT NULL,
  `tipo` varchar(15) NOT NULL COMMENT 'archivo, consulta, censo, expontanea',
  `status` varchar(1) NOT NULL COMMENT '0=eliminada 1=activa',
  `extra` varchar(1) NOT NULL,
  PRIMARY KEY (`id_ubicacion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `ubicaciones`
--

INSERT INTO `ubicaciones` VALUES(1, 'Archivo', 'archivo', '1', '');
INSERT INTO `ubicaciones` VALUES(2, 'piso', 'piso', '1', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicaciones_x_usuario`
--

DROP TABLE IF EXISTS `ubicaciones_x_usuario`;
CREATE TABLE `ubicaciones_x_usuario` (
  `id_ubicacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `tipo_usuario` varchar(15) NOT NULL,
  `status` varchar(1) NOT NULL COMMENT '0=eliminada 1=activa',
  `extra` varchar(1) NOT NULL,
  PRIMARY KEY (`id_ubicacion`,`id_usuario`,`tipo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcar la base de datos para la tabla `ubicaciones_x_usuario`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `pass` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_usuario` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT '1=archivo',
  `status` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `sesionId` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `extra1` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `st` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` VALUES(1, 'archivo', '123456', 'usuario de archivo', '1', '1', '9591e686d56a32f85577ae9b8a837094', '', '1');
