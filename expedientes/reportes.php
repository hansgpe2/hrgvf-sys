<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
  <table width="400" border="0" cellspacing="0" cellpadding="0" class="ventana">
    <tr>
      <td class="tituloVentana" height="23">REPORTES</td>
    </tr>
    <tr>
      <td align="left" valign="top">
        <br /><a href="javascript: reportesCargar('reportes_ND_ventanilla.php');" class="linkReportes">- Expedientes no devueltos por ventanilla</a>
      	<br /><br /><a href="javascript: reportesCargar('reportes_ND_consulta.php');" class="linkReportes">- Expedientes no devueltos por consulta externa</a>
        <br /><br /><a href="javascript: reporteVentanillaFecha();" class="linkReportes">- Expedientes no devueltos por ventanilla X rango de fecha</a>
        <br /><br /><a href="javascript: reporteConsultaFecha();" class="linkReportes">- Expedientes no devueltos por consulta externa X rango de fecha</a>
      </td>
    </tr>
  </table>
</body>
</html>
