<?php
include_once('lib/misFunciones.php');
$select_servicios = '';
$servicios = getServicios();
$tServicios = count($servicios);
if ($tServicios > 0) {
	$select_servicios .= '<select name="servicios" id="servicios" onchange="javascript: cargarMedicosXservicio(this);">';
	$select_servicios .= '<option value="" selected="selected"></option>';
	for ($i=0; $i<$tServicios; $i++) {
		$select_servicios .= '<option value="' . $servicios[$i]["id_servicio"] . '">' . $servicios[$i]["nombre"] . '</option>';
	}
	$select_servicios .= '</select>';
}
?>
<center>

<form id="forma" name="forma" onsubmit="javascript: return recibirExpedientesValidar();">
<table border="0" cellpadding="0" cellspacing="10" width="780">
	<tr><td colspan="2" class="tituloVentana">Prestar de Ventanilla</td></tr>
	<tr><td align="right">C&oacute;digo de Barras del Expediente: </td><td align="left"><input id="cod_bar" name="cod_bar" type="text" value="" maxlength="8" onkeypress="javascript: return codigoBarras(event);" onkeyup="javascript: validaEnvioCodigoBarrasExp(this);" /> <input name="buscar" type="button" value="Buscar..." onclick="javascript: buscarCodigoBarrasExp();" /></td></tr>
    <tr>
    	<td colspan="2">
        	<table width="100%" cellpadding="0" cellspacing="0" border="0">
            	<tr>
                	<td>
				        <div id="buscar" style="height:110px; margin-top:10px;">
                              <table width="700" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
                                  <td align="left"><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();">
                                    <option value="cedula" selected="selected">C&eacute;dula</option>
                                    <option value="nombre">Nombre</option>
                                  </select>
                                  </td>
                                </tr>
                                <tr>
                                <td colspan="2">
                                    <div id="buscarPorCedula">
                                    <table width="700" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                                      <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                                        <input name="buscar_ced" type="button" class="botones" id="buscar_ced" value="Buscar..." onclick="javascript: buscarDHbusqueda(document.getElementById('cedulaBuscar').value);" /></td>
                                    </tr>
                                    <tr>
                                      <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                                      <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div>
                                      </td>
                                    </tr>
                                
                                    <tr>
                                      <td colspan="2" align="center">
                                        <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: buscarCodigoBarrasExpXCedula();" value="Seleccionar Expediente" /><br /><br /></td>
                                      </tr>
                                
                                  </table>
                                  </div>
                                 <div id="buscarPorNombre" style="display:none;">
                                <table width="700" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                                  <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                                  <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                                  <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                                </tr>
                                <tr>
                                  <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                                  <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                                    <input name="buscarN" type="button" class="botones" id="buscarN" value="Buscar..." onclick="javascript: buscarDHNbusqueda(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);" /></td>
                                </tr>
                                <tr>
                                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                                  <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div>
                                  </td>
                                </tr>
                            
                                <tr>
                                  <td colspan="4" align="center">
                                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: buscarCodigoBarrasExpXCedula();" value="Seleccionar Expediente" /><br /><br /></td>
                                  </tr>
                            
                              </table>
                              </div>
                               </td>
                                </tr>
                              </table>
                        </div>
                    </td>
                </tr>
            </table>
    	</td>
    </tr>
	<tr>
    	<td colspan="2"><div id="envio">&nbsp;</div></td>
    </tr>
	<tr><td colspan="2"><center><table border="0" cellpadding="0" cellspacing="0" width="750"><tr><td align="center" colspan="3"><div id="solicitudes"></div></td></tr><tr><td align="left" colspan="3">Persona a quien se presta: <?php echo $select_servicios; ?> <div id="div_persona" style="display:inline;"><input type="hidden" name="persona" id="persona" value="" /></div></td></tr>
	<tr><td align="left" colspan="3">Enviado a: <select name="enviadoA" id="enviadoA"><option value="">Selecciona...</option><option value="Consulta Ext.-">Consulta Ext.</option><option value="Hospitalizacion-">Hospitalización</option><option value="Tramite-">Trámite</option></select></td></tr>
    <tr><td align="left" colspan="3">Observaciones: 
	  <textarea name="observaciones" cols="70" rows="4" id="observaciones"></textarea></td></tr><tr><td align="center" height="50" colspan="3"><input type="button" id="aceptar" name="aceptar" value="A C E P T A R" onclick="javascript: prestarExpontaneaAceptar();"></td></tr></table></center></td></tr>
</table>
</form>
</center>
