<?php
// Desactivar toda notificación de error

error_reporting(0);
session_start ();
include_once('lib/misFunciones.php');

$hoy = date('d-m-Y');

function getDatosDerecho2015($id_derecho) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT cedula, cedula_tipo, ap_p, ap_m, nombres FROM derechohabientes WHERE id_derecho='" . $id_derecho . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'cedula' => $row_query['cedula'],
            'cedula_tipo' => $row_query['cedula_tipo'],
            'ap_p' => $row_query['ap_p'],
            'ap_m' => $row_query['ap_m'],
            'nombres' => $row_query['nombres']
        );
    } else {
        $ret = array(
            'cedula' => "-1",
            'cedula_tipo' => "-1",
            'ap_p' => "-1",
            'ap_m' => "-1",
            'nombres' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicos2015() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_connect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT id_medico, ap_p, ap_m, nombres FROM medicos";
    $query = mysql_query($query_query, $bdissste);// or die(mysql_error());
    $row_query = mysql_fetch_array($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[$row_query["id_medico"]] = $row_query;
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServicios2015() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT id_servicio, nombre FROM servicios";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
       do {
            $ret[$row_query["id_servicio"]] = $row_query;
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesXrango($fechaI, $fechaF) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;

	$conexion = new mysqli($hostname_bdissste, $username_bdissste, $password_bdissste, $database_bdissste);
	if ($conexion->connect_error) {
		echo "Ha ocurrido un error: " . $conexion->connect_error . "Número del error: " . $conexion->connect_errno;
	}
//    $query_query = "SELECT DISTINCT id_expediente, id_detalle, id_solicitud, id_cita, fecha, hora, status, extra FROM solicitudes_detalles WHERE fecha>='" . $fechaI . "' AND fecha<='" . $fechaF . "' ORDER BY fecha ASC;";

    $query_query = "SELECT DISTINCT d.id_expediente, d.id_detalle, d.id_solicitud, d.id_cita, d.fecha, d.hora, d.status, d.extra, s.tipo, s.fecha_creacion, s.hora_creacion, s.id3, s.id5 FROM solicitudes_detalles d, solicitudes s WHERE s.id_solicitud=d.id_solicitud AND s.tipo='2' AND d.fecha>='" . $fechaI . "' AND d.fecha<='" . $fechaF . "' ORDER BY d.fecha ASC;";

	$extraer = $conexion->query($query_query);
    $ret = array();
    foreach ($extraer as $key => $value) {
        $ret[] = $value;
    }
	mysqli_close($conexion);
    return $ret;
}

$aFechaI = explode("-", $_GET["fechaI"]);
$aFechaF = explode("-", $_GET["fechaF"]);
$fechaI = $aFechaI[2] . $aFechaI[1] . $aFechaI[0];
$fechaF = $aFechaF[2] . $aFechaF[1] . $aFechaF[0];
$detalles = getDetallesXrango($fechaI, $fechaF);
$medicos = getMedicos2015();
$servicios = getServicios2015();

$ret = '';
$arr = array();
$fechaHoy = date("Ymd");

foreach ($detalles as $iDetalles => $detalle) {
//	if ($solicitud["tipo"] == "2") { // se filtran los expedientes que sean por ventanilla - YA VIENE FILTRADA LA CONSULTA
		$derecho = getDatosDerecho2015($detalle["id_expediente"]);
		$fecha = $detalle["fecha_creacion"];
		$diasDif = diasDiferencia($fecha, $fechaHoy);
		$fecha_salida = formatoDia($detalle["fecha_creacion"],"fecha") . " " . $detalle["hora_creacion"];
        $ret .= '<tr class="Azul"><td style="padding:3px;" align="left">' . $derecho["cedula"] . '/' . $derecho["cedula_tipo"] . '</td><td style="padding:3px;" align="left">' . ponerAcentos($derecho["ap_p"] . ' ' . $derecho["ap_m"] . ' ' . $derecho["nombres"]) . '</td><td style="padding:3px;" align="left">' . ponerAcentos($medicos[$detalle["id3"]]["ap_p"] . ' ' . $medicos[$detalle["id3"]]["ap_m"] . ' ' . $medicos[$detalle["id3"]]["nombres"]) . '</td><td style="padding:3px;" align="left">' . ponerAcentos($servicios[$detalle["id5"]]["nombre"]) . '</td><td width="50" style="padding:3px;" align="left">' . $fecha_salida . '</td><td align="center" width="50" style="padding:3px;">' . $diasDif . '</td><td align="left" width="50" style="padding:3px;">' . ponerCeros($detalle["id_solicitud"], 12) . '</td></tr>';
//	}
}




$titulo = 'Expedientes no devueltos por ventanilla';
$encabezado = '
    <tr>
      <td class="tituloVentana" height="23" colspan="7">' . $titulo . '</td>
    </tr>
';
$pie = '<p align="center"><input type="button" name="imprimir" id="imprimir" value="Imprimir" class="botones" onclick="javascript: window.open(\'' . $_SERVER['PHP_SELF'] . '?imprimir=si\')"></p>';
if (isset($_REQUEST["imprimir"])) {
	$encabezado = '
	  <tr>
		<td colspan="6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="74" align="left"><img src="diseno/logoEncabezado.png" width="74" height="74" /></td>
			<td width="80" class="tituloIssste" align="left">Instituto de<br />
			  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
			<td align="center">SUBDIRECCIÓN GENERAL MÉDICA<br />' . $titulo . '</td>
			<td width="150" valign="bottom" align="right"><table width="150" border="2" cellspacing="0" cellpadding="0">
			  <tr>
				<td align="center">Fecha</td>
			  </tr>
			  <tr>
				<td align="center" class="contenido8bold">' . $hoy . '</td>
			  </tr>
			</table></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
		<td align="center" class="tituloEncabezadoConBorde" colspan="7">Unidad M&eacute;dica: H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN</td>
	  </tr>
	';
	$pie = '';
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $titulo ?></title>
<link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
<link href="lib/impresion.css" media="print" rel="stylesheet" type="text/css" />
</head>

<body>
<center>
  <table width="750" border="1" cellspacing="0" cellpadding="0" class="ventana">
  <?php echo $encabezado ?>
    <tr class="AzulN">
    	<td align="center">C&eacute;dula</td>
    	<td align="center">Nombre del Derechohabiente</td>
    	<td align="center">M&eacute;dico o persona a quien se prest&oacute;</td>
    	<td align="center">Servicio</td>
    	<td align="center">Fecha Salida</td>
    	<td align="center">D&iacute;as sin devolver</td>
        <td align="center">Folio Solicitud</td>
    </tr>
    <?php echo $ret ?>
  </table>
  <?php echo $pie ?>
</center>
</body>
</html>
