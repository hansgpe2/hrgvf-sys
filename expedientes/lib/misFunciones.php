<?php require_once('Connections/bdissste.php'); ?>
<?php
date_default_timezone_set('America/Mexico_City');

$statusRecetas = array("CANCELADA", "SIN SURTIR", "SURTIDA");
$statusMedicam = array("APLICADO", "SIN APLICAR", "CANCELADO", "SIN SURTIR", "SIN CADUCIDAD", "REMANENTE SIN APLICAR");
$tipoLogs = array("ingreso al sistema", "agregar derechohabiente", "agregar receta", "aplicar dosis", "baja medicamento", "baja medicamento caduco", "reutilizar medicamento", "impresion de reporte", "salir del sistema");

$statusSolicitudes = array("CANCELADA", "ACTIVA", "en_transito", "recibida", "transferida", "atendida(de regreso a archivo)");


define("ENTIDAD_FEDERATIVA", "JALISCO");
define("CLAVE_UNIDAD_MEDICA", "1402140100");
// constantes para la importacion de existencias de siam xls
define("SIAM_NOMBRE_TABLA", "SIAM-ExistenciasMasApartados");
define("SIAM_FILA_INICIAL", 7);
define("SIAM_COL_ID_MEDICAMENTO", 0);
define("SIAM_COL_PARTIDA", 1);
define("SIAM_COL_DESCRIPCION", 2);
define("SIAM_COL_EXISTENCIA", 4);
define("SIAM_COL_PRECIO", 9);

define("SERIE", "034RM");
define("FOLIO_INICIAL", "1");
define("FOLIO_FINAL", "9999999");
define("FOLIO_ACTUAL", "1");

function obtenerUltimaSolicitud($fecha_c, $hora_c, $fecha_a, $hora_a, $id1, $id2)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $sql="select * from solicitudes where fecha_creacion='$fecha_c' and hora_creacion='$hora_c' and fecha_atendida='$fecha_a' and hora_atendida='$hora_a' and id1='$id1' and id2='$id2' limit 1";
    $query=  mysql_query($sql,$bdissste);
    $ret=array();
    if(mysql_num_rows($query)>0)
    $ret=  mysql_fetch_array($query);
    mysql_close($bdissste);
    return $ret;
}
function getUltimaSolicitud($fecha_c, $hora_c, $fecha_a, $hora_a, $tipo, $status, $id1, $id2) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes WHERE fecha_creacion='" . $fecha_c . "' AND hora_creacion='" . $hora_c . "' AND fecha_atendida='" . $fecha_a . "' AND hora_atendida='" . $hora_a . "' AND tipo='" . $tipo . "' AND status='" . $status . "' AND id1='" . $id1 . "' AND id2='" . $id2 . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_solicitud' => $row_query['id_solicitud'],
            'fecha_creacion' => $row_query['fecha_creacion'],
            'hora_creacion' => $row_query['hora_creacion'],
            'fecha_atendida' => $row_query['fecha_atendida'],
            'hora_atendida' => $row_query['hora_atendida'],
            'tipo' => $row_query['tipo'],
            'status' => $row_query['status'],
            'id1' => $row_query['id1'],
            'id2' => $row_query['id2'],
            'id3' => $row_query['id3'],
            'id4' => $row_query['id4'],
            'id5' => $row_query['id5'],
            'extra' => $row_query['extra']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function crearExpediente($id_expediente, $fecha, $hora, $id_usuario, $impresiones, $id_ubicacion, $status) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "INSERT INTO expedientes VALUES('" . $id_expediente . "','" . $fecha . "','" . $hora . "','" . $id_usuario . "','" . $impresiones . "','" . $id_ubicacion . "','" . $status . "','',NULL);";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$ret=array(mysql_errno(),mysql_error());
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMovimiento($id_expediente)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM expedientes_movimientos WHERE id_expediente='" . $id_expediente . "' ORDER BY id_movimiento DESC limit 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
            $ret = array(
                'id_movimiento' => $row_query['id_movimiento'],
                'id_expediente' => $row_query['id_expediente'],
                'ubicacion_actual' => $row_query['ubicacion_actual'],
                'fecha_ua' => $row_query['fecha_ua'],
                'hora_ua' => $row_query['hora_ua'],
                'ubicacion_destino' => $row_query['ubicacion_destino'],
                'fecha_ud' => $row_query['fecha_ud'],
                'hora_ud' => $row_query['hora_ud'],
                'observaciones' => $row_query['observaciones'],
                'status' => $row_query['status'],
                'extra' => $row_query['extra']
            );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMovimientos($id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM expedientes_movimientos WHERE id_expediente='" . $id_expediente . "' ORDER BY id_movimiento DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_movimiento' => $row_query['id_movimiento'],
                'id_expediente' => $row_query['id_expediente'],
                'ubicacion_actual' => $row_query['ubicacion_actual'],
                'fecha_ua' => $row_query['fecha_ua'],
                'hora_ua' => $row_query['hora_ua'],
                'ubicacion_destino' => $row_query['ubicacion_destino'],
                'fecha_ud' => $row_query['fecha_ud'],
                'hora_ud' => $row_query['hora_ud'],
                'observaciones' => $row_query['observaciones'],
                'status' => $row_query['status'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMovXidSolicitud($id_solicitud) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM expedientes_movimientos WHERE extra='" . $id_solicitud . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_array($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = $row_query;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUltimoMov($id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM expedientes_movimientos WHERE id_expediente='" . $id_expediente . "' ORDER BY id_movimiento DESC LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_movimiento' => $row_query['id_movimiento'],
            'id_expediente' => $row_query['id_expediente'],
            'ubicacion_actual' => $row_query['ubicacion_actual'],
            'fecha_ua' => $row_query['fecha_ua'],
            'hora_ua' => $row_query['hora_ua'],
            'ubicacion_destino' => $row_query['ubicacion_destino'],
            'fecha_ud' => $row_query['fecha_ud'],
            'hora_ud' => $row_query['hora_ud'],
            'observaciones' => $row_query['observaciones'],
            'status' => $row_query['status'],
            'extra' => $row_query['extra']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getExpedienteXid($id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM expedientes WHERE id_expediente='" . $id_expediente . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_expediente' => $row_query['id_expediente'],
            'fecha_creacion' => $row_query['fecha_creacion'],
            'hora_creacion' => $row_query['hora_creacion'],
            'id_usuario_creacion' => $row_query['id_usuario_creacion'],
            'impresiones' => $row_query['impresiones'],
            'id_ubicacion' => $row_query['id_ubicacion'],
            'status' => $row_query['status'],
            'extra' => $row_query['extra']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getExpedientesActivos() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM expedientes WHERE status>'1'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
		do {
			$ret[] = array(
				'id_expediente' => $row_query['id_expediente'],
				'fecha_creacion' => $row_query['fecha_creacion'],
				'hora_creacion' => $row_query['hora_creacion'],
				'id_usuario_creacion' => $row_query['id_usuario_creacion'],
				'impresiones' => $row_query['impresiones'],
				'id_ubicacion' => $row_query['id_ubicacion'],
				'status' => $row_query['status'],
				'extra' => $row_query['extra']
			);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUbicaciones() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM ubicaciones";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
		do {
			$ret[$row_query['id_ubicacion']] = $row_query['nombre'];
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUbicacionXid($id_ubicacion) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM ubicaciones WHERE id_ubicacion='" . $id_ubicacion . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_ubicacion' => $row_query['id_ubicacion'],
            'nombre' => $row_query['nombre'],
            'tipo' => $row_query['tipo'],
            'status' => $row_query['status'],
            'extra' => $row_query['extra']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getOpcionesExpedienteVer($id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $expediente = getExpedienteXid($id_expediente);
    $ultimo = getUltimoMov($id_expediente);
    $ret = '';
    if (count($expediente) > 0) { // existe el expediente
        if ($expediente["status"] == 1) { // el expediente est� presente en alguna ubicacion
            switch ($expediente["id_ubicacion"]) {
                case "1": // est� en archivo
                    $ret = '<b>EN ARCHIVO</b>';
                    break;
                default : // NO est� en archivo
                    $ubicacion = getUbicacionXid($ultimo["ubicacion_destino"]);
                    $ret = '<b>EXPEDIENTE EN: <span class="mensajeRojo">' . $ubicacion["nombre"] . '</span></b>';
                    break;
            }
        }
        if ($expediente["status"] == 2) { // el expediente est� en transito hacia alguna ubicacion
            $ubicacion = getUbicacionXid($ultimo["ubicacion_destino"]);
            $ret = '<b>EXPEDIENTE ENVIADO A: <span class="mensajeRojo">' . $ubicacion["nombre"] . '</span></b>';
        }
    } else { // no existe el expediente en la base de datos
        $ret = '<b><span class="mensajeRojo">NO EXISTE EL EXPEDIENTE</span></b>';
    }
    return $ret;
}

function getOpcionesExpediente($id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $expediente = getExpedienteXid($id_expediente);
    $ultimo = getUltimoMov($id_expediente);
    $ret = '';
    if (count($expediente) > 0) { // existe el expediente
        if ($expediente["status"] == 1) { // el expediente est� presente en alguna ubicacion
            switch ($expediente["id_ubicacion"]) {
                case "1": // est� en archivo
                    $ret = '<input type="checkbox" id="chk_' . $id_expediente . '" name="chk_' . $id_expediente . '" value="' . $id_expediente . '"> <b><a href="javascript: cambiarChk(\'chk_' . $id_expediente . '\');" class="link_vacio">SE ENTREGA EL EXPEDIENTE</a></b> <input type="button" name="imp_' . $id_expediente . '" id="imp_' . $id_expediente . '" onclick="javascript: expedientesImprimir(\'' . $id_expediente . '\');" value="Imprimir Etiqueta">';
                    break;
                default : // NO est� en archivo
                    $ubicacion = getUbicacionXid($ultimo["ubicacion_destino"]);
                    $ret = '<div id="ajax_' . $id_expediente . '" class="div_renglon">EXPEDIENTE EN: <span class="mensajeRojo">' . $ubicacion["nombre"] . '</span> ' . formatoDia($ultimo["fecha_ua"], "fecha") . '</div>';
                    break;
            }
        }
        if ($expediente["status"] == 2) { // el expediente est� en transito hacia alguna ubicacion
            $ubicacion = getUbicacionXid($ultimo["ubicacion_destino"]);
            $ret = '<div id="ajax_' . $id_expediente . '" class="div_renglon">PRESTADO POR: <span class="mensajeRojo">' . $ubicacion["nombre"] . '</span> ' . formatoDia($ultimo["fecha_ua"], "fecha") . '</div>';
        }
    } else { // no existe el expediente en la base de datos
        $ret = '<div id="ajax_' . $id_expediente . '" class="div_renglon"><span class="mensajeRojo">NO EXISTE EL EXPEDIENTE</span><input type="button" name="add_' . $id_expediente . '" id="add_' . $id_expediente . '" value="Crear Expediente" onclick="javascript: crearExpediente(\'' . $id_expediente . '\');">&nbsp;</div>';
    }
    return $ret;
}

function hayDetallesSinRecibir($id_solicitud) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes_detalles WHERE id_solicitud='" . $id_solicitud . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = true;
    if ($totalRows_query == 0) {
        $ret = false;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesXidExpediente($id_solicitud, $id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes_detalles WHERE id_solicitud='" . $id_solicitud . "' AND id_expediente='" . $id_expediente . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret = array(
                'id_detalle' => $row_query['id_detalle'],
                'id_solicitud' => $row_query['id_solicitud'],
                'id_expediente' => $row_query['id_expediente'],
                'id_cita' => $row_query['id_cita'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'status' => $row_query['status'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesXidExp($id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes_detalles WHERE id_expediente='" . $id_expediente . "' ORDER BY id_detalle DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_detalle' => $row_query['id_detalle'],
                'id_solicitud' => $row_query['id_solicitud'],
                'id_expediente' => $row_query['id_expediente'],
                'id_cita' => $row_query['id_cita'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'status' => $row_query['status'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUltimoDetalle($id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes_detalles WHERE id_expediente='" . $id_expediente . "' ORDER BY id_detalle DESC LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
            $ret = array(
                'id_detalle' => $row_query['id_detalle'],
                'id_solicitud' => $row_query['id_solicitud'],
                'id_expediente' => $row_query['id_expediente'],
                'id_cita' => $row_query['id_cita'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'status' => $row_query['status'],
                'extra' => $row_query['extra']
            );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesXidSolicitud($id_solicitud) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes_detalles WHERE id_solicitud='" . $id_solicitud . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_detalle' => $row_query['id_detalle'],
                'id_solicitud' => $row_query['id_solicitud'],
                'id_expediente' => $row_query['id_expediente'],
                'id_cita' => $row_query['id_cita'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'status' => $row_query['status'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getSolicitudesNoAtendidas($status, $codigo) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $hoy = date("Y-m-d");
    $ayer = date("Ymd", strtotime("-7 day", strtotime($hoy))); // 7 dias antes
//	$ayer = date("Ymd", strtotime( "-1 day", strtotime( $hoy ) ) ); 
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $cod = "";
    if ($codigo != "")
        $cod = " AND id_solicitud='" . $codigo . "'";
    $query_query = "SELECT * FROM solicitudes WHERE fecha_creacion<'" . $ayer . "' AND status='" . $status . "'" . $cod . " ORDER BY fecha_creacion ASC, hora_creacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_solicitud' => $row_query['id_solicitud'],
                'fecha_creacion' => $row_query['fecha_creacion'],
                'hora_creacion' => $row_query['hora_creacion'],
                'fecha_atendida' => $row_query['fecha_atendida'],
                'hora_atendida' => $row_query['hora_atendida'],
                'tipo' => $row_query['tipo'],
                'status' => $row_query['status'],
                'id1' => $row_query['id1'],
                'id2' => $row_query['id2'],
                'id3' => $row_query['id3'],
                'id4' => $row_query['id4'],
                'id5' => $row_query['id5'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getSolicitudes($status, $codigo) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $hoy = date("Y-m-d");
    $ayer = date("Ymd", strtotime("-7 day", strtotime($hoy))); // 7 dias antes
//	$ayer = date("Ymd", strtotime( "-1 day", strtotime( $hoy ) ) ); 
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $cod = "";
    if ($codigo != "")
        $cod = " AND id_solicitud='" . $codigo . "'";
    $query_query = "SELECT * FROM solicitudes WHERE fecha_creacion>='" . $ayer . "' AND status='" . $status . "'" . $cod . " ORDER BY fecha_creacion ASC, hora_creacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_solicitud' => $row_query['id_solicitud'],
                'fecha_creacion' => $row_query['fecha_creacion'],
                'hora_creacion' => $row_query['hora_creacion'],
                'fecha_atendida' => $row_query['fecha_atendida'],
                'hora_atendida' => $row_query['hora_atendida'],
                'tipo' => $row_query['tipo'],
                'status' => $row_query['status'],
                'id1' => $row_query['id1'],
                'id2' => $row_query['id2'],
                'id3' => $row_query['id3'],
                'id4' => $row_query['id4'],
                'id5' => $row_query['id5'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getSolicitudesXtipoYstatus($tipo, $status, $orden) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes WHERE tipo='" . $tipo . "' AND status='" . $status . "' ORDER BY " . $orden;
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_solicitud' => $row_query['id_solicitud'],
                'fecha_creacion' => $row_query['fecha_creacion'],
                'hora_creacion' => $row_query['hora_creacion'],
                'fecha_atendida' => $row_query['fecha_atendida'],
                'hora_atendida' => $row_query['hora_atendida'],
                'tipo' => $row_query['tipo'],
                'status' => $row_query['status'],
                'id1' => $row_query['id1'],
                'id2' => $row_query['id2'],
                'id3' => $row_query['id3'],
                'id4' => $row_query['id4'],
                'id5' => $row_query['id5'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getSolicitudesExpontaneas($status, $codigo) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $cod = "";
    if ($codigo != "")
        $cod = " AND id_solicitud='" . $codigo . "'";
    $query_query = "SELECT * FROM solicitudes WHERE tipo='2' AND status='" . $status . "'" . $cod . " ORDER BY fecha_creacion ASC, hora_creacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_solicitud' => $row_query['id_solicitud'],
                'fecha_creacion' => $row_query['fecha_creacion'],
                'hora_creacion' => $row_query['hora_creacion'],
                'fecha_atendida' => $row_query['fecha_atendida'],
                'hora_atendida' => $row_query['hora_atendida'],
                'tipo' => $row_query['tipo'],
                'status' => $row_query['status'],
                'id1' => $row_query['id1'],
                'id2' => $row_query['id2'],
                'id3' => $row_query['id3'],
                'id4' => $row_query['id4'],
                'id5' => $row_query['id5'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getSolicitudXid($id_solicitud) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes WHERE id_solicitud='" . $id_solicitud . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_array($query);
    $ret = array();
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = $row_query;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getConsultorioXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM consultorios WHERE id_consultorio='" . $id . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste);// or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_consultorio' => $row_query['id_consultorio'],
            'nombre' => $row_query['nombre']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicoXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_connect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM medicos WHERE id_medico=$id";
    $query = mysql_query($query_query, $bdissste);// or die(mysql_error());
    $row_query = mysql_fetch_array($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = $row_query;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServicioXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM servicios WHERE id_servicio='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = NULL;
    if ($totalRows_query > 0) {
        $ret = $row_query['nombre'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServicioXid2($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM servicios WHERE id_servicio='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_servicio' => $row_query['id_servicio'],
            'clave' => $row_query['clave'],
            'nombre' => $row_query['nombre']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDatosDerecho($id_derecho) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM derechohabientes WHERE id_derecho='" . $id_derecho . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'cedula' => $row_query['cedula'],
            'cedula_tipo' => $row_query['cedula_tipo'],
            'ap_p' => $row_query['ap_p'],
            'ap_m' => $row_query['ap_m'],
            'nombres' => $row_query['nombres'],
            'fecha_nacimiento' => $row_query['fecha_nacimiento'],
            'telefono' => $row_query['telefono'],
            'direccion' => $row_query['direccion'],
            'estado' => $row_query['estado'],
            'municipio' => $row_query['municipio'],
            'status' => $row_query['status']
        );
    } else {
        $ret = array(
            'cedula' => "-1",
            'cedula_tipo' => "-1",
            'ap_p' => "-1",
            'ap_m' => "-1",
            'nombres' => "-1",
            'telefono' => "-1",
            'fecha_nacimiento' => "-1",
            'direccion' => "-1",
            'estado' => "-1",
            'municipio' => "-1",
            'status' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function tituloDia($dia_num) {
    switch ($dia_num) {
        case 0: return "<abbr title=\"Domingo\">Domingo</abbr>";
            break;
        case 1: return "<abbr title=\"Lunes\">Lunes</abbr>";
            break;
        case 2: return "<abbr title=\"Martes\">Martes</abbr>";
            break;
        case 3: return "<abbr title=\"Miercoles\">Mi&eacute;rcoles</abbr>";
            break;
        case 4: return "<abbr title=\"Jueves\">Jueves</abbr>";
            break;
        case 5: return "<abbr title=\"Viernes\">Viernes</abbr>";
            break;
        case 6: return "<abbr title=\"Sabado\">S&aacute;bado</abbr>";
            break;
    }
}

function tituloMes($mes) {
    $meses = array(12);
    if ($mes[0] == "0")
        $mes = $mes[1];
    $meses[1] = "Enero";
    $meses[2] = "Febrero";
    $meses[3] = "Marzo";
    $meses[4] = "Abril";
    $meses[5] = "Mayo";
    $meses[6] = "Junio";
    $meses[7] = "Julio";
    $meses[8] = "Agosto";
    $meses[9] = "Septiembre";
    $meses[10] = "Octubre";
    $meses[11] = "Noviembre";
    $meses[12] = "Diciembre";
    return $meses[$mes];
}

function diaSemana($dia_num) {
    switch ($dia_num) {
        case 0: return "DOMINGO";
            break;
        case 1: return "LUNES";
            break;
        case 2: return "MARTES";
            break;
        case 3: return "MIERCOLES";
            break;
        case 4: return "JUEVES";
            break;
        case 5: return "VIERNES";
            break;
        case 6: return "SABADO";
            break;
    }
}

function regresarIdMedico($idConsultorio, $idServicio) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT id_medico FROM servicios_x_consultorio WHERE id_consultorio='" . $idConsultorio . "' and id_servicio='" . $idServicio . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $idMedico = $row_query['id_medico'];
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $idMedico;
}

function formatoHora($hora) {
    return substr($hora, 0, 2) . ":" . substr($hora, 2, 2);
}

function quitarPuntosHora($hora) {
    return substr($hora, 0, 2) . substr($hora, 3, 2);
}

function getDH($where, $order) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM derechohabientes " . $where . " " . $order;
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_derecho' => $row_query['id_derecho'],
                'cedula' => $row_query['cedula'],
                'cedula_tipo' => $row_query['cedula_tipo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres'],
                'fecha_nacimiento' => $row_query['fecha_nacimiento'],
                'telefono' => $row_query['telefono'],
                'direccion' => $row_query['direccion'],
                'estado' => $row_query['estado'],
                'municipio' => $row_query['municipio'],
                'status' => $row_query['status']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function buscarDHxCedulaParaCita($cedula) {
    $dhs = getDH("WHERE cedula like '%" . $cedula . "%'", "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE CEDULA QUE CONTENGA " . strtoupper($cedula) . "</option>";
    }
    $out .= "</select>";
    return $out;
}

function buscarDHxNombreParaCita($ap_p, $ap_m, $nombre) {
    $temp = "";
    if (strlen($ap_p) > 0) {
        $temp .= "ap_p like '%" . $ap_p . "%'";
    }
    if (strlen($ap_m) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR ap_m like '%" . $ap_m . "%'";
        } else {
            $temp .= "ap_m like '%" . $ap_m . "%'";
        }
    }
    if (strlen($nombre) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR nombres like '%" . $nombre . "%'";
        } else {
            $temp .= "nombres like '%" . $nombre . "%'";
        }
    }
    $dhs = getDH("WHERE " . $temp, "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE PACIENTE CON DATOS PROPORCIONADOS</option>";
        $out2 = " <input name=\"agregarDH\" type=\"button\" value=\"Agregar Derechohabiente\" class=\"botones\" id=\"agregarDH\" onClick=\"javascript: agregarDHenCita();\" />";
    }
//	$out.= "<option value=\"fas\">" . $totalMedicos . "</option>";
    $out .= "</select>";
    return $out . $out2;
}

function buscarDHxCedulaParaBusqueda($cedula) {
    $dhs = getDH("WHERE cedula like '%" . $cedula . "%'", "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE CEDULA QUE CONTENGA " . strtoupper($cedula) . "</option>";
        $out2 = "";
    }
    $out .= "</select>";
    return $out . $out2;
}

function buscarDHxNombreParaBusqueda($ap_p, $ap_m, $nombre) {
    $temp = "";
    if (strlen($ap_p) > 0) {
        $temp .= "ap_p like '%" . $ap_p . "%'";
    }
    if (strlen($ap_m) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR ap_m like '%" . $ap_m . "%'";
        } else {
            $temp .= "ap_m like '%" . $ap_m . "%'";
        }
    }
    if (strlen($nombre) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR nombres like '%" . $nombre . "%'";
        } else {
            $temp .= "nombres like '%" . $nombre . "%'";
        }
    }
    $dhs = getDH("WHERE " . $temp, "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE PACIENTE CON DATOS PROPORCIONADOS</option>";
        $out2 = "";
    }
    $out .= "</select>";
    return $out . $out2;
}

function ejecutarSQL($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    if (strstr($query_query, "select"))
        $ret = mysql_fetch_assoc($query);
    else
        $ret = $query;
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function ejecutarSQLR($query_query) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query = mysql_query($query_query, $bdissste); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $error;
}

function existeDuplica($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUsuarioXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM usuarios WHERE id_usuario='" . $id . "' AND st='1' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
	$ret=array();
    if ($totalRows_query > 0) {
        $ret = /*array(
            'id_usuario' => $row_query['id_usuario'],
            'login' => $row_query['login'],
            'pass' => $row_query['pass'],
            'nombre' => $row_query['nombre'],
            'tipo_usuario' => $row_query['tipo_usuario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'status' => $row_query['status']
        )*/$row_query;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function compararFechas($hoy, $otra) { // formato yyyymmdd
    $hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
    $otraMK = mktime(0, 0, 0, substr($otra, 4, 2), substr($otra, 6, 2), substr($otra, 0, 4));
    if ($otraMK >= $hoyMK)
        return true; else
        return false;
}

function quitarAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "&") {
            $temp = substr($Text, $j, 8);
            switch ($temp) {
                case "&aacute;": $cadena .= "(/a)";
                    $j = $j + 7;
                    break;
                case "&Aacute;": $cadena .= "(/A)";
                    $j = $j + 7;
                    break;
                case "&eacute;": $cadena .= "(/e)";
                    $j = $j + 7;
                    break;
                case "&Eacute;": $cadena .= "(/E)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/i)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/I)";
                    $j = $j + 7;
                    break;
                case "&oacute;": $cadena .= "(/o)";
                    $j = $j + 7;
                    break;
                case "&Oacute;": $cadena .= "(/O)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/u)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/U)";
                    $j = $j + 7;
                    break;
                case "&ntilde;": $cadena .= "(/n)";
                    $j = $j + 7;
                    break;
                case "&Ntilde;": $cadena .= "(/N)";
                    $j = $j + 7;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            switch ($cara) {
                case "�": $cadena.="(/a)";
                    break;
                case "�": $cadena.="(/e)";
                    break;
                case "�": $cadena.="(/i)";
                    break;
                case "�": $cadena.="(/o)";
                    break;
                case "�": $cadena.="(/u)";
                    break;
                case "�": $cadena.="(/A)";
                    break;
                case "�": $cadena.="(/E)";
                    break;
                case "�": $cadena.="(/I)";
                    break;
                case "�": $cadena.="(/O)";
                    break;
                case "�": $cadena.="(/U)";
                    break;
                case "�": $cadena.="(/n)";
                    break;
                case "�": $cadena.="(/N)";
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        }
    }
    return $cadena;
}

function ponerAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "(") {
            $temp = substr($Text, $j, 4);
            switch ($temp) {
                case "(/a)": $cadena .= "&aacute;";
                    $j = $j + 3;
                    break;
                case "(/A)": $cadena .= "&Aacute;";
                    $j = $j + 3;
                    break;
                case "(/e)": $cadena .= "&eacute;";
                    $j = $j + 3;
                    break;
                case "(/E)": $cadena .= "&Eacute;";
                    $j = $j + 3;
                    break;
                case "(/i)": $cadena .= "&iacute;";
                    $j = $j + 3;
                    break;
                case "(/I)": $cadena .= "&Iacute;";
                    $j = $j + 3;
                    break;
                case "(/o)": $cadena .= "&oacute;";
                    $j = $j + 3;
                    break;
                case "(/O)": $cadena .= "&Oacute;";
                    $j = $j + 3;
                    break;
                case "(/u)": $cadena .= "&uacute;";
                    $j = $j + 3;
                    break;
                case "(/U)": $cadena .= "&Uacute;";
                    $j = $j + 3;
                    break;
                case "(/n)": $cadena .= "&ntilde;";
                    $j = $j + 3;
                    break;
                case "(/N)": $cadena .= "&Ntilde;";
                    $j = $j + 3;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            $cadena.=$Text[$j];
        }
    }
    return $cadena;
}

function tipoDH($municipio) {
    $ret = "X";
    if (($municipio == "Guadalajara") || ($municipio == "Tlajomulco de Z(/u)(/n)iga") || ($municipio == "Tonal(/a)") || ($municipio == "Zapopan") || ($municipio == "Tlaquepaque"))
        $ret = "&nbsp;";
    return $ret;
}

function queSexoTipoCedula($tipoCedula) {
    $ret = "";
    if (($tipoCedula == "10") || ($tipoCedula == "40") || ($tipoCedula == "41") || ($tipoCedula == "50") || ($tipoCedula == "51") || ($tipoCedula == "70") || ($tipoCedula == "71") || ($tipoCedula == "90") || ($tipoCedula == "92"))
        $ret = "M"; else
        $ret = "F";
    return $ret;
}

// ---------------------------------------------------------------------------------------------  N U E V A S    F U N C I O N E S  -------------
function getDatosReceta($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_receta='" . $id_receta . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'n_serie' => $row_query['n_serie'],
            'codigo_barras' => $row_query['codigo_barras'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_captura' => $row_query['fecha_captura'],
            'fecha_expedicion' => $row_query['fecha_expedicion'],
            'entidad' => $row_query['entidad'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDatosRecetaXserie($n_serie) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE n_serie='" . $n_serie . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'n_serie' => $row_query['n_serie'],
            'codigo_barras' => $row_query['codigo_barras'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_captura' => $row_query['fecha_captura'],
            'fecha_expedicion' => $row_query['fecha_expedicion'],
            'entidad' => $row_query['entidad'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function formatoDia($fecha, $paraDonde) {
    $dia = substr($fecha, 6, 2);
    $mes = substr($fecha, 4, 2);
    $ano = substr($fecha, 0, 4);
    $diaSem = date("N", mktime(0, 0, 0, $mes, $dia, $ano));
    if ($paraDonde == 'tituloCitasXdia') {
        $fechaO = diaSemana($diaSem) . " " . $dia . " DE " . strtoupper(tituloMes($mes)) . " DE " . $ano;
    }
    if ($paraDonde == 'imprimirCita') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    if ($paraDonde == 'fecha') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    return $fechaO;
}

function getRecetaRecienAgregada($n_serie) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE n_serie='" . $n_serie . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = $row_query['id_receta'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosStock() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='1' ORDER BY id_receta ASC, fecha_caducidad DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'dosis' => $row_query['dosis'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosXingresar() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='4' ORDER BY id_receta ASC, fecha_caducidad DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'dosis' => $row_query['dosis'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosStockFechas($fechaI, $fechaF) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE fecha_ingreso>='" . $fechaI . "' AND fecha_ingreso<='" . $fechaF . "' ORDER BY fecha_ingreso ASC, id_receta ASC, fecha_caducidad DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'dosis' => $row_query['dosis'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function datosMedicamentoI($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='0' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'codigo_barras' => $row_query['codigo_barras'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function datosMedicamentoRUI($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='5' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'codigo_barras' => $row_query['codigo_barras'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getContenido($idMed) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $idMed . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = $row_query['contenido'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosRemanentes() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT DISTINCT id_ingreso FROM medicamentos_remanentes WHERE status = '1' ORDER BY fecha_aplicacion ASC, hora_aplicacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query2 = "SELECT * FROM medicamentos_remanentes WHERE id_ingreso = '" . $row_query['id_ingreso'] . "' ORDER BY id_movimiento DESC";
            $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_movimiento' => $row_query2['id_movimiento'],
                'id_ingreso' => $row_query2['id_ingreso'],
                'cantidad_remanente' => $row_query2['cantidad_remanente'],
                'fecha_aplicacion' => $row_query2['fecha_aplicacion'],
                'hora_aplicacion' => $row_query2['hora_aplicacion'],
                'status' => $row_query2['status'],
                'extra1' => $row_query2['extra1']
            );
            @mysql_free_result($query2);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosRemanentesXaplicar() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT DISTINCT id_ingreso FROM medicamentos_remanentes WHERE status = '5' ORDER BY fecha_aplicacion ASC, hora_aplicacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query2 = "SELECT * FROM medicamentos_remanentes WHERE id_ingreso = '" . $row_query['id_ingreso'] . "' ORDER BY id_movimiento DESC";
            $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_movimiento' => $row_query2['id_movimiento'],
                'id_ingreso' => $row_query2['id_ingreso'],
                'cantidad_remanente' => $row_query2['cantidad_remanente'],
                'fecha_aplicacion' => $row_query2['fecha_aplicacion'],
                'hora_aplicacion' => $row_query2['hora_aplicacion'],
                'status' => $row_query2['status'],
                'extra1' => $row_query2['extra1']
            );
            @mysql_free_result($query2);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosCaducos() {
// CALCULO PARA SABER CUALES CADUCARON
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_bajas";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'codigo_barras' => $row_query['codigo_barras'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosEnReceta($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query = "SELECT * FROM medicamentos_inventario WHERE id_receta='" . $id_receta . "' AND id_medicamento='" . $row_query['id_medicamento'] . "'";
            $query1 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query1 = mysql_fetch_assoc($query1);
            $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
            $query2 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'dosis' => $row_query['dosis'],
                'diagnostico' => $row_query['diagnostico'],
                'etapa_clinica' => $row_query['etapa_clinica'],
                'id_ingreso' => $row_query1['id_ingreso'],
                'codigo_barras' => $row_query1['codigo_barras'],
                'contenido' => $row_query1['contenido'],
                'fecha_caducidad' => $row_query1['fecha_caducidad'],
                'fecha_ingreso' => $row_query1['fecha_ingreso'],
                'hora_ingreso' => $row_query1['hora_ingreso'],
                'clave_farmacia' => $row_query2['clave_farmacia'],
                'descripcion' => $row_query2['descripcion'],
                'presentacion' => $row_query2['presentacion'],
                'unidad' => $row_query2['unidad'],
                'tipo' => $row_query2['tipo'],
                'tiempo_cad_remanente' => $row_query2['tiempo_cad_remanente']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosEnRecetaSinSurtir($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE id_receta='" . $id_receta . "' AND status='3'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query = "SELECT * FROM medicamentos_inventario WHERE id_receta='" . $id_receta . "' AND id_medicamento='" . $row_query['id_medicamento'] . "' AND status='3'";
            $query1 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query1 = mysql_fetch_assoc($query1);
            $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
            $query2 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'dosis' => $row_query['dosis'],
                'diagnostico' => $row_query['diagnostico'],
                'etapa_clinica' => $row_query['etapa_clinica'],
                'id_ingreso' => $row_query1['id_ingreso'],
                'codigo_barras' => $row_query1['codigo_barras'],
                'contenido' => $row_query1['contenido'],
                'fecha_caducidad' => $row_query1['fecha_caducidad'],
                'fecha_ingreso' => $row_query1['fecha_ingreso'],
                'hora_ingreso' => $row_query1['hora_ingreso'],
                'clave_farmacia' => $row_query2['clave_farmacia'],
                'descripcion' => $row_query2['descripcion'],
                'presentacion' => $row_query2['presentacion'],
                'unidad' => $row_query2['unidad'],
                'tipo' => $row_query2['tipo'],
                'tiempo_cad_remanente' => $row_query2['tiempo_cad_remanente']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentoXingresar($id_ingreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE id_ingreso='" . $id_ingreso . "' AND status='4' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $row_query['id_receta'] . "' AND id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
        $query1 = mysql_query($query_query, $bdissste) or die(mysql_error());
        $row_query1 = mysql_fetch_assoc($query1);
        $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
        $query2 = mysql_query($query_query, $bdissste) or die(mysql_error());
        $row_query2 = mysql_fetch_assoc($query2);
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'id_medicamento' => $row_query['id_medicamento'],
            'dosis' => $row_query['dosis'],
            'id_ingreso' => $row_query['id_ingreso'],
            'contenido' => $row_query['contenido'],
            'fecha_caducidad' => $row_query['fecha_caducidad'],
            'fecha_ingreso' => $row_query['fecha_ingreso'],
            'hora_ingreso' => $row_query['hora_ingreso'],
            'diagnostico' => $row_query1['diagnostico'],
            'etapa_clinica' => $row_query1['etapa_clinica'],
            'clave_farmacia' => $row_query2['clave_farmacia'],
            'descripcion' => $row_query2['descripcion'],
            'presentacion' => $row_query2['presentacion'],
            'unidad' => $row_query2['unidad'],
            'tipo' => $row_query2['tipo'],
            'tiempo_cad_remanente' => $row_query2['tiempo_cad_remanente']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function listaRecetasXaplicar() {
    $recetas = getRecetas(1); // 1 es sin aplicar
    $totalRecetas = count($recetas);
    $out = "";
    for ($i = 0; $i < $totalRecetas; $i++) {
        $paciente = getDatosDerecho($recetas[$i]['id_derecho']);
        $medico = getMedicoXid($recetas[$i]['id_medico']);
        $medicamentos = getMedicamentosEnReceta($recetas[$i]['id_receta']);
        $totalMedicamentos = count($medicamentos);
        $out .= "<div><a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('R" . $recetas[$i]['id_receta'] . "');\" onmouseout=\"javascript: ocultarDiv('R" . $recetas[$i]['id_receta'] . "');\">&nbsp; - RECETA " . $recetas[$i]['n_serie'] . " - " . formatoDia($recetas[$i]['fecha_captura'], "fecha") . "</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript: aplicarDosis('" . $recetas[$i]['n_serie'] . "');\" class=\"botones_menu\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Aplicar Dosis</a></div>
				 <div id=\"R" . $recetas[$i]['id_receta'] . "\" style=\"position:absolute; margin-left:300px; margin-top:-13px; display:none;\">
				 	<table width=\"400\" class=\"ventanaConFondo\">
						<tr><td class=\"textoAzulN\" align=\"left\">" . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
						<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($recetas[$i]['id_servicio']) . " - 
						" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
        for ($j = 0; $j < $totalMedicamentos; $j++) {
            if ($medicamentos[$j]['descripcion'] != '')
                $out .= "
						<tr><td class=\"textoGrisN\" align=\"left\">" . ponerAcentos($medicamentos[$j]['descripcion'] . " - " . $medicamentos[$j]['indicaciones']) . " <br>&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"textoGris\">FECHA DE CADUCIDAD: " . formatoDia($medicamentos[$j]['fecha_caducidad'], "fecha") . "</span></td></tr>
					";
        }
        $out .= "	</table>
				</div>";
    }
    return $out;
}

function listaMedicamentosXingresar() {
    $medicamentos = getMedicamentosXingresar();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $datosMedicamento = getDatosMedicamentos($medicamentos[$i]['id_medicamento']);
        $fecha_cad = $medicamentos[$i]['fecha_caducidad'];
        $hora_ingreso = $medicamentos[$i]['hora_ingreso'];
        $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
        $fecha_caducidad = formatoHora($hora_ingreso) . " " . formatoDia($fecha_cad, "fecha") . " - " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
        $receta = getDatosReceta($medicamentos[$i]['id_receta']);
        $paciente = getDatosDerecho($receta['id_derecho']);
        $medico = getMedicoXid($receta['id_medico']);
        $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: registrarMedicamento('" . $receta['n_serie'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\"  onmouseover=\"javascript: mostrarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Registrar Medicamento</a>
						</td></tr></table></div>
					 <div id=\"M" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
						<table width=\"400\" class=\"ventanaConFondo\">
							<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . "</td></tr>
							<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
							<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
							<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
							" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
        $out .= "	</table>
					</div>";
    }
    return $out;
}

function listaRecetasXsurtir() {
    $recetas = getRecetas(3); // 3 es sin surtir
    $totalRecetas = count($recetas);
    $out = "";
    for ($i = 0; $i < $totalRecetas; $i++) {
        $paciente = getDatosDerecho($recetas[$i]['id_derecho']);
        $medico = getMedicoXid($recetas[$i]['id_medico']);
        $medicamentos = getMedicamentosEnReceta($recetas[$i]['id_receta']);
        $totalMedicamentos = count($medicamentos);
        $out .= "<div><a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('R" . $recetas[$i]['id_receta'] . "');\" onmouseout=\"javascript: ocultarDiv('R" . $recetas[$i]['id_receta'] . "');\">&nbsp; - RECETA " . $recetas[$i]['n_serie'] . " - " . formatoDia($recetas[$i]['fecha_captura'], "fecha") . "</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript: surtirReceta('" . $recetas[$i]['n_serie'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('R" . $recetas[$i]['id_receta'] . "');\" onmouseout=\"javascript: ocultarDiv('R" . $recetas[$i]['id_receta'] . "');\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Surtir Receta</a></div>
				 <div id=\"R" . $recetas[$i]['id_receta'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
				 	<table width=\"400\" class=\"ventanaConFondo\">
						<tr><td class=\"textoAzulN\" align=\"left\">" . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
						<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($recetas[$i]['id_servicio']) . " - 
						" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
        for ($j = 0; $j < $totalMedicamentos; $j++) {
            if ($medicamentos[$j]['descripcion'] != '')
                $out .= "
						<tr><td class=\"textoGrisN\" align=\"left\">" . ponerAcentos($medicamentos[$j]['descripcion']) . "</td></tr>
					";
        }
        $out .= "	</table>
				</div>";
    }
    return $out;
}

function listaMedicamentosStock() {
    $medicamentos = getMedicamentosStock();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $datosMedicamento = getDatosMedicamentos($medicamentos[$i]['id_medicamento']);
        $fecha_cad = $medicamentos[$i]['fecha_caducidad'];
        $hora_ingreso = $medicamentos[$i]['hora_ingreso'];
        $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
        $fecha_caducidad = formatoHora($hora_ingreso) . " " . formatoDia($fecha_cad, "fecha") . " - " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
        if (strtotime("now") <= strtotime($fecha_caducidad)) {
            $receta = getDatosReceta($medicamentos[$i]['id_receta']);
            $paciente = getDatosDerecho($receta['id_derecho']);
            $medico = getMedicoXid($receta['id_medico']);
            $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: aplicarDosis('" . $receta['n_serie'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\"  onmouseover=\"javascript: mostrarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Aplicar Dosis</a>
						</td></tr></table></div>
					 <div id=\"M" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
						<table width=\"400\" class=\"ventanaConFondo\">
							<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA DE CADUCIDAD: " . formatoDia($medicamentos[$i]['fecha_caducidad'], "fecha") . "</td></tr>
							<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
							<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
							<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
							" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
            $out .= "	</table>
					</div>";
        }
    }
    return $out;
}

function listaMedicamentosRemanentes() {
    $medicamentos = getMedicamentosRemanentes();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $medicamentosI = datosMedicamentoI($medicamentos[$i]['id_ingreso']);
        $datosMedicamento = getDatosMedicamentos($medicamentosI['id_medicamento']);
        if ($datosMedicamento['tipo'] == '0') { // 0 es un medicamento reutilizable ;)
            $fecha_ingreso = $medicamentosI['fecha_ingreso'];
            $hora_ingreso = $medicamentosI['hora_ingreso'];
            $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
            $tiempoIngreso = formatoHora($hora_ingreso) . " " . formatoDia($fecha_ingreso, "fecha") . " + " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
            if (strtotime("now") < strtotime($tiempoIngreso)) {
                //			echo date('H:i d/m/Y',strtotime($tiempoIngreso)) . "<br>";
                $receta = getDatosReceta($medicamentosI['id_receta']);
                $paciente = getDatosDerecho($receta['id_derecho']);
                $medico = getMedicoXid($receta['id_medico']);
                $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: reutilizarMedicamento('" . $medicamentosI['id_medicamento'] . "','" . $receta['id_receta'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Reutilizar</a>
						</td></tr></table></div>
						 <div id=\"M1" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
							<table width=\"400\" class=\"ventanaConFondo\">
								<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">CANTIDAD REMANENTE: " . $medicamentos[$i]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE APLICACION: " . formatoDia($medicamentos[$i]['fecha_aplicacion'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">HORA DE APLICACION: " . formatoHora($medicamentos[$i]['hora_aplicacion']) . "</td></tr>
								<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
								<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
								<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
								" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
                $out .= "	</table>
						</div>";
            }
        }
    }
    return $out;
}

function listaMedicamentosRemanentesXaplicar() {
    $medicamentos = getMedicamentosRemanentesXaplicar();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {

        $medicamentosI = datosMedicamentoRUI($medicamentos[$i]['id_ingreso']);
        $datosMedicamento = getDatosMedicamentos($medicamentosI['id_medicamento']);
        if ($datosMedicamento['tipo'] == '0') { // 0 es un medicamento reutilizable ;)
            $fecha_ingreso = $medicamentosI['fecha_ingreso'];
            $hora_ingreso = $medicamentosI['hora_ingreso'];
            $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
            $tiempoIngreso = formatoHora($hora_ingreso) . " " . formatoDia($fecha_ingreso, "fecha") . " + " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
            if (strtotime("now") < strtotime($tiempoIngreso)) {
                //			echo date('H:i d/m/Y',strtotime($tiempoIngreso)) . "<br>";
                $receta = getDatosReceta($medicamentosI['id_receta']);
                $paciente = getDatosDerecho($receta['id_derecho']);
                $medico = getMedicoXid($receta['id_medico']);
                $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: aplicarDosisRU('" . $medicamentosI['id_medicamento'] . "','" . $receta['n_serie'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Aplicar Dosis</a>
						</td></tr></table></div>
						 <div id=\"M1" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
							<table width=\"400\" class=\"ventanaConFondo\">
								<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">CANTIDAD REMANENTE: " . $medicamentos[$i]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE APLICACION: " . formatoDia($medicamentos[$i]['fecha_aplicacion'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">HORA DE APLICACION: " . formatoHora($medicamentos[$i]['hora_aplicacion']) . "</td></tr>
								<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
								<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
								<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
								" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
                $out .= "	</table>
						</div>";
            }
        }
    }
    return $out;
}

function listaMedicamentosRemanentesCaducos() {
    $medicamentos = getMedicamentosRemanentes();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $medicamentosI = datosMedicamentoI($medicamentos[$i]['id_ingreso']);
        $datosMedicamento = getDatosMedicamentos($medicamentosI['id_medicamento']);
        if ($datosMedicamento['tipo'] == '1') { // 1 es un medicamento NO reutilizable ;)
            $receta = getDatosReceta($medicamentosI['id_receta']);
            $paciente = getDatosDerecho($receta['id_derecho']);
            $medico = getMedicoXid($receta['id_medico']);
            $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M2" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M2" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: bajaMedicamento('" . $medicamentosI['id_medicamento'] . "','" . $receta['id_receta'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M2" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M2" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medDel.gif\" border=\"0\" width=\"30\">Dar de Baja</a>
						</td></tr></table></div>
					 <div id=\"M2" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
						<table width=\"400\" class=\"ventanaConFondo\">
							<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">CANTIDAD REMANENTE: " . $medicamentos[$i]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE APLICACION: " . formatoDia($medicamentos[$i]['fecha_aplicacion'], "fecha") . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">HORA DE APLICACION: " . formatoHora($medicamentos[$i]['hora_aplicacion']) . "</td></tr>
							<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
							<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
							<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
							" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
            $out .= "	</table>
					</div>";
        }
    }
    return $out;
}

function listaMedicamentosReutilizablesCaducos() {
    $medicamentos = getMedicamentosRemanentes();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $medicamentosI = datosMedicamentoI($medicamentos[$i]['id_ingreso']);
        $datosMedicamento = getDatosMedicamentos($medicamentosI['id_medicamento']);
        if ($datosMedicamento['tipo'] == '0') { // 0 es un medicamento reutilizable ;)
            $fecha_ingreso = $medicamentosI['fecha_ingreso'];
            $hora_ingreso = $medicamentosI['hora_ingreso'];
            $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
            $tiempoIngreso = formatoHora($hora_ingreso) . " " . formatoDia($fecha_ingreso, "fecha") . " + " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
            if (strtotime("now") >= strtotime($tiempoIngreso)) {
                //			echo date('H:i d/m/Y',strtotime($tiempoIngreso)) . "<br>";
                $receta = getDatosReceta($medicamentosI['id_receta']);
                $paciente = getDatosDerecho($receta['id_derecho']);
                $medico = getMedicoXid($receta['id_medico']);
                $out .= "<div>
							<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
							<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M3" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M3" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
							</td><td width=\"110\"><a href=\"javascript: bajaMedicamento('" . $medicamentosI['id_medicamento'] . "','" . $receta['id_receta'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M3" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M3" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medDel.gif\" border=\"0\" width=\"30\">Dar de Baja</a>
							</td></tr></table></div>
						 <div id=\"M3" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
							<table width=\"400\" class=\"ventanaConFondo\">
								<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">CANTIDAD REMANENTE: " . $medicamentos[$i]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE APLICACION: " . formatoDia($medicamentos[$i]['fecha_aplicacion'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">HORA DE APLICACION: " . formatoHora($medicamentos[$i]['hora_aplicacion']) . "</td></tr>
								<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
								<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
								<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
								" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
                $out .= "	</table>
						</div>";
            }
        }
    }
    return $out;
}

function listaMedicamentosCaducos() {
    $medicamentos = getMedicamentosStock();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $datosMedicamento = getDatosMedicamentos($medicamentos[$i]['id_medicamento']);
        $fecha_cad = $medicamentos[$i]['fecha_caducidad'];
        $hora_ingreso = $medicamentos[$i]['hora_ingreso'];
        $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
        $fecha_caducidad = formatoHora($hora_ingreso) . " " . formatoDia($fecha_cad, "fecha") . " - " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
        if (strtotime("now") > strtotime($fecha_caducidad)) {
            $receta = getDatosReceta($medicamentos[$i]['id_receta']);
            $paciente = getDatosDerecho($receta['id_derecho']);
            $medico = getMedicoXid($receta['id_medico']);
            $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M4" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M4" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: bajaMedicamentoCaduco('" . $medicamentos[$i]['id_medicamento'] . "','" . $receta['id_receta'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M4" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M4" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medDel.gif\" border=\"0\" width=\"30\">Dar de Baja</a>
						</td></tr></table></div>
					 <div id=\"M4" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-93px; display:none;\">
						<table width=\"400\" class=\"ventanaConFondo\">
							<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA DE CADUCIDAD: " . formatoDia($medicamentos[$i]['fecha_caducidad'], "fecha") . "</td></tr>
							<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
							<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
							<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
							" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
            $out .= "	</table>
					</div>";
        }
    }
    return $out;
}

function listaMedicamentosConMovimientosImp($fechaI, $fechaF) {
    $medicamentos = getMedicamentosStockFechas($fechaI, $fechaF);
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $datosMedicamento = getDatosMedicamentos($medicamentos[$i]['id_medicamento']);
        $fecha_cad = $medicamentos[$i]['fecha_caducidad'];
        $hora_ingreso = $medicamentos[$i]['hora_ingreso'];
        $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
        $fecha_caducidad = formatoHora($hora_ingreso) . " " . formatoDia($fecha_cad, "fecha") . " - " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
        $receta = getDatosReceta($medicamentos[$i]['id_receta']);
        $paciente = getDatosDerecho($receta['id_derecho']);
        $medico = getMedicoXid($receta['id_medico']);
        $out .= "<table border=\"2\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"3\">
					<tr><td class=\"contenido8\" colspan=\"4\">- FECHA DE INGRESO DEL MEDICAMENTO: <span class=\"contenido8bold\">" . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . "</span></td></tr>
					<tr><td class=\"contenido8\" colspan=\"3\">MEDICAMENTO: <span class=\"contenido8bold\">" . $datosMedicamento['descripcion'] . "</span></td>
						<td class=\"contenido8\" align=\"left\">PRESENTACION: <span class=\"contenido8bold\">" . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</span></td></tr>
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">FECHA DE CADUCIDAD: <span class=\"contenido8bold\">" . formatoDia($medicamentos[$i]['fecha_caducidad'], "fecha") . "<span</td></tr>
					<tr><td class=\"contenido8bold\" align=\"center\" colspan=\"4\">INFORMACION DE LA RECETA</td></tr>
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">RECETA No: <span class=\"contenido8bold\">" . $receta['n_serie'] . "</td></tr>
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">PACIENTE: <span class=\"contenido8bold\">" . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</span></td></tr>
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">SERVICIO: <span class=\"contenido8bold\">" . getServicioXid($receta['id_servicio']) . "</span></td></tr> 
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">MEDICO: <span class=\"contenido8bold\">" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</span></td></tr>
					<tr><td class=\"contenido8bold\" align=\"center\" colspan=\"4\">MOVIMIENTOS</td></tr>
					<tr><td class=\"contenido8\" align=\"right\" width=\"20%\">INGRESO: </td><td class=\"contenido8bold\" align=\"left\">" . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " A LAS " . formatoHora($medicamentos[$i]['hora_ingreso']) . " HRS</td><td class=\"contenido8\" align=\"right\">CONTENIDO: </td><td align=\"left\" class=\"contenido8bold\">" . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>";
        $medicamentoAplicado = getMedicamentoRemanente($medicamentos[$i]['id_ingreso']);
        $cAplicado = count($medicamentoAplicado);
        if ($cAplicado > 0) {
            for ($j = 0; $j < $cAplicado; $j++)
                $out .= "	<tr><td class=\"contenido8\" align=\"right\">APLICACION: </td><td class=\"contenido8bold\" align=\"left\">" . formatoDia($medicamentoAplicado[$j]['fecha_aplicacion'], "fecha") . " A LAS " . formatoHora($medicamentoAplicado[$j]['hora_aplicacion']) . " HRS</td><td class=\"contenido8\" align=\"right\">CANTIDAD REMANTE: </td><td align=\"left\" class=\"contenido8bold\">" . $medicamentoAplicado[$j]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>";
        }
        $medicamentoBaja = getMedicamentoBaja($medicamentos[$i]['id_ingreso']);
        $cBaja = count($medicamentoBaja);
        if ($cBaja > 0) {
            for ($j = 0; $j < $cBaja; $j++)
                $out .= "	<tr><td class=\"contenido8\" align=\"right\">BAJA DEL MEDICAMENTO: </td><td class=\"contenido8bold\" align=\"left\">" . formatoDia($medicamentoBaja[$j]['fecha_baja'], "fecha") . " A LAS " . formatoHora($medicamentoBaja[$j]['hora_baja']) . " HRS</td><td class=\"contenido8\" align=\"right\">CANTIDAD BAJA: </td><td align=\"left\" class=\"contenido8bold\">" . $medicamentoBaja[$j]['cantidad_baja'] . " " . $datosMedicamento['unidad'] . "</td></tr>";
        }
        $out .= "</table></td></tr></table>";
    }
    return $out;
}

function getServicios() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT* FROM servicios ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_servicio' => $row_query['id_servicio'],
                'clave' => $row_query['clave'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicos() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT* FROM medicos ORDER BY ap_p ASC, ap_m ASC, nombres ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medico' => $row_query['id_medico'],
                'titulo' => $row_query['titulo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDiagnosticos() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM diagnosticos ORDER BY descripcion";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_diagnostico' => $row_query['id_diagnostico'],
                'descripcion' => $row_query['descripcion']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function listaServicios() {
    $servicios = getServicios();
    $totalServicios = count($servicios);
    $out = "";
    $out.= "<option selected value=\"0\"> </option>";
    if ($totalServicios > 0) {
        for ($i = 0; $i < $totalServicios; $i++) {
            $out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
        }
    }
    return $out;
}

function listaMedicos() {
    $medicos = getMedicos();
    $totalMedicos = count($medicos);
    $out = "";
    $out.= "<option selected value=\"0\"> </option>";
    if ($totalMedicos > 0) {
        for ($i = 0; $i < $totalMedicos; $i++) {
            $out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
        }
    }
    return $out;
}

function listaMedicamentos() {
    $medicamentos = getMedicamentos();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    $out.= "<option selected value=\"0\"> </option>";
    if ($totalMedicamentos > 0) {
        for ($i = 0; $i < $totalMedicamentos; $i++) {
            $out.= "<option value=\"" . $medicamentos[$i]['clave_farmacia'] . "\">" . $medicamentos[$i]['clave_farmacia'] . " - " . ponerAcentos($medicamentos[$i]['descripcion']) . "</option>";
        }
    }
    return $out;
}

function listaDiagnosticos() {
    $medicamentos = getDiagnosticos();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    $out.= "<option selected value=\"0\"> </option>";
    if ($totalMedicamentos > 0) {
        for ($i = 0; $i < $totalMedicamentos; $i++) {
            $out.= "<option value=\"" . $medicamentos[$i]['id_diagnostico'] . "\">" . ponerAcentos($medicamentos[$i]['descripcion']) . "</option>";
        }
    }
    return $out;
}

function formatoFechaBD($fecha) {
    $dia = substr($fecha, 0, 2);
    $mes = substr($fecha, 3, 2);
    $ano = substr($fecha, 6, 4);
    $fechaO = $ano . $mes . $dia;
    return $fechaO;
}

function datosMedicamentoAaplicar($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='1' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $datosMed = getDatosMedicamentos($row_query['id_medicamento']);
        $datosMedR = getDatosMedicamentoEnReceta($row_query['id_receta'], $row_query['id_medicamento']);
        $ret = array(
            'id_ingreso' => $row_query['id_ingreso'],
            'id_medicamento' => $row_query['id_medicamento'],
            'id_receta' => $row_query['id_receta'],
            'dosis' => $row_query['dosis'],
            'contenido' => $row_query['contenido'],
            'fecha_caducidad' => $row_query['fecha_caducidad'],
            'fecha_ingreso' => $row_query['fecha_ingreso'],
            'hora_ingreso' => $row_query['hora_ingreso'],
            'extra1' => $row_query['extra1'],
            'clave_farmacia' => $datosMed['clave_farmacia'],
            'descripcion' => $datosMed['descripcion'],
            'unidad' => $datosMed['unidad'],
            'diagnostico' => $datosMedR['diagnostico'],
            'etapa_clinica' => $datosMedR['etapa_clinica']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function datosMedicamentoRUaAplicar($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='5' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $datosMed = getDatosMedicamentos($row_query['id_medicamento']);
        $datosMedR = getDatosMedicamentoEnReceta($row_query['id_receta'], $row_query['id_medicamento']);
        $ret = array(
            'id_ingreso' => $row_query['id_ingreso'],
            'id_medicamento' => $row_query['id_medicamento'],
            'id_receta' => $row_query['id_receta'],
            'dosis' => $row_query['dosis'],
            'contenido' => $row_query['contenido'],
            'fecha_caducidad' => $row_query['fecha_caducidad'],
            'fecha_ingreso' => $row_query['fecha_ingreso'],
            'hora_ingreso' => $row_query['hora_ingreso'],
            'extra1' => $row_query['extra1'],
            'clave_farmacia' => $datosMed['clave_farmacia'],
            'descripcion' => $datosMed['descripcion'],
            'unidad' => $datosMed['unidad'],
            'diagnostico' => $datosMedR['diagnostico'],
            'etapa_clinica' => $datosMedR['etapa_clinica']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function datosMedicamentoRemanente($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='0' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $datosMed = getDatosMedicamentos($row_query['id_medicamento']);
        $datosMedR = getDatosMedicamentoEnReceta($row_query['id_receta'], $row_query['id_medicamento']);
        $datosMedRem =
                $ret = array(
            'id_ingreso' => $row_query['id_ingreso'],
            'id_medicamento' => $row_query['id_medicamento'],
            'id_receta' => $row_query['id_receta'],
            'dosis' => $row_query['dosis'],
            'contenido' => $row_query['contenido'],
            'fecha_caducidad' => $row_query['fecha_caducidad'],
            'fecha_ingreso' => $row_query['fecha_ingreso'],
            'hora_ingreso' => $row_query['hora_ingreso'],
            'extra1' => $row_query['extra1'],
            'clave_farmacia' => $datosMed['clave_farmacia'],
            'descripcion' => $datosMed['descripcion'],
            'unidad' => $datosMed['unidad'],
            'diagnostico' => $datosMedR['diagnostico'],
            'cantidad_remanente' => getContenidoRemanente($idIngreso),
            'etapa_clinica' => $datosMedR['etapa_clinica']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getContenidoRemanente($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_remanentes WHERE status='1' AND id_ingreso='" . $idIngreso . "' ORDER BY id_movimiento DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $datosMed = getDatosMedicamentos($row_query['id_medicamento']);
        $datosMedR = getDatosMedicamentoEnReceta($row_query['id_receta'], $row_query['id_medicamento']);
        $ret = $row_query['cantidad_remanente'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentoRemanente($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_remanentes WHERE id_ingreso = '" . $idIngreso . "' ORDER BY id_movimiento ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_movimiento' => $row_query['id_movimiento'],
                'id_ingreso' => $row_query['id_ingreso'],
                'cantidad_remanente' => $row_query['cantidad_remanente'],
                'fecha_aplicacion' => $row_query['fecha_aplicacion'],
                'hora_aplicacion' => $row_query['hora_aplicacion'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentoBaja($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_bajas WHERE id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'cantidad_baja' => $row_query['cantidad_baja'],
                'fecha_baja' => $row_query['fecha_baja'],
                'hora_baja' => $row_query['hora_baja'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicosXServicio($idSer) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
//	$idMedico =  regresarIdMedico($idConsultorio,$idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT x.id_consultorio, x.id_servicio, x.id_medico, m.id_medico, m.titulo, m.nombres, m.ap_p, m.ap_m FROM servicios_x_consultorio x, medicos m WHERE x.id_servicio='" . $idSer . "' AND x.id_medico=m.id_medico AND m.st='1' ORDER BY m.ap_p ASC, m.ap_m ASC, m.nombres ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medico' => $row_query['id_medico'],
                'titulo' => $row_query['titulo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentoXclave($clave) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos WHERE clave_farmacia='" . $clave . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_medicamento' => $row_query['id_medicamento'],
            'clave_farmacia' => $row_query['clave_farmacia'],
            'codigo_barras' => $row_query['codigo_barras'],
            'descripcion' => $row_query['descripcion'],
            'presentacion' => $row_query['presentacion'],
            'contenido' => $row_query['contenido'],
            'unidad' => $row_query['unidad'],
            'tipo' => $row_query['tipo'],
            'tiempo_cad_remanente' => $row_query['tiempo_cad_remanente'],
            'status' => $row_query['status']
        );
    } else {
        $ret = array(
            'id_medicamento' => "",
            'clave_farmacia' => "",
            'codigo_barras' => "",
            'descripcion' => "",
            'presentacion' => "",
            'contenido' => "",
            'unidad' => "",
            'tipo' => "",
            'tiempo_cad_remanente' => "",
            'status' => ""
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getNMedicamentosEnReceta($n_serie) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE n_serie='" . $n_serie . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $id_receta = $row_query['id_receta'];
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    return $totalRows_query;
}

function medicamentoRecienInventariado($id_medicamento, $id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE id_medicamento='" . $id_medicamento . "' AND id_receta='" . $id_receta . "';";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $id_ingreso = $row_query['id_ingreso'];
    return $id_ingreso;
}

function getMedicamentosXdiagnostico($id_diagnostico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentosXdiagnostico WHERE id_diagnostico='" . $id_diagnostico . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
            $query2 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_medicamento' => $row_query['id_medicamento'],
                'clave_farmacia' => $row_query2['clave_farmacia'],
                'descripcion' => $row_query2['descripcion'],
                'presentacion' => $row_query2['presentacion'],
                'unidad' => $row_query2['unidad'],
                'tipo' => $row_query2['tipo'],
                'tiempo_cad_remanente' => $row_query2['tiempo_cad_remanente']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDiagnosticoXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM diagnosticos WHERE id_diagnostico='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = ponerAcentos($row_query['descripcion']);
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasXdia($fechaCitas, $idMedico) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $queDia = date("N", mktime(0, 0, 0, substr($fechaCitas, 4, 2), substr($fechaCitas, 6, 2), substr($fechaCitas, 0, 4)));
    $query_query = "SELECT * FROM horarios WHERE id_medico='" . $idMedico . "' and dia='" . diaSemana($queDia) . "' ORDER BY hora_inicio";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_horario' => $row_query['id_horario'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasOcupadasXdia($idHorario, $fechaCita) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $idHorario . "' and fecha_cita='" . $fechaCita . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_derecho' => $row_query['id_derecho'],
            'id_cita' => $row_query['id_cita'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1'],
            'id_usuario' => $row_query['id_usuario'],
            'observaciones' => $row_query['observaciones']
        );
    } else {
        $ret = array(
            'id_derecho' => "-1",
            'id_cita' => "-1",
            'status' => "-1",
            'extra1' => "-1",
            'id_usuario' => "-1",
            'observaciones' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitasExtemporaneas($fechaCitas, $idMedico) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_medico='" . $idMedico . "' AND fecha_cita='" . $fechaCitas . "' ORDER BY hora_inicio ASC, hora_fin ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_cita'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'status' => $row_query['status'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUsuarioCitaXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM usuarios WHERE id_usuario='" . $id . "' AND st='1' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_usuario' => $row_query['id_usuario'],
            'login' => $row_query['login'],
            'pass' => $row_query['pass'],
            'nombre' => $row_query['nombre'],
            'tipo_usuario' => $row_query['tipo_usuario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'status' => $row_query['status']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getConsultorios() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM consultorios WHERE st='1' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_consultorio' => $row_query['id_consultorio'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServiciosXConsultorio($idCon) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $idMedico = regresarIdMedico($idConsultorio, $idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT DISTINCT x.id_servicio, s.id_servicio, s.nombre, s.clave FROM servicios_x_consultorio x, servicios s WHERE x.id_consultorio='" . $idCon . "' AND x.id_servicio=s.id_servicio ORDER BY s.nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_servicio' => $row_query['id_servicio'],
                'clave' => $row_query['clave'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicosXServicioXConsultorio($idCon, $idSer) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $idMedico = regresarIdMedico($idConsultorio, $idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT x.id_consultorio, x.id_servicio, x.id_medico, m.id_medico, m.titulo, m.nombres, m.ap_p, m.ap_m FROM servicios_x_consultorio x, medicos m WHERE x.id_consultorio='" . $idCon . "' AND x.id_servicio='" . $idSer . "' AND x.id_medico=m.id_medico AND m.st='1' ORDER BY m.ap_p ASC, m.ap_m ASC, m.nombres ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medico' => $row_query['id_medico'],
                'titulo' => $row_query['titulo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function opcionesCon($idCon) {
    $consultorios = getConsultorios();
    $totalConsultorios = count($consultorios);
    $out = "";
    if ($totalConsultorios > 0) {
        if ($idCon == "-1") { // que no se ha seleccionado ningun consultorio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalConsultorios; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['IdCon'] = $consultorios[$i]['id_consultorio'];
                } else {
                    $out.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalConsultorios; $i++) {
                if ($idCon == $consultorios[$i]['id_consultorio']) {
                    $out.= "<option selected value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                    $_SESSION['IdCon'] = $consultorios[$i]['id_consultorio'];
                } else {
                    $out.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN CONSULTORIOS</option>";
    }
    return $out;
}

function opcionesSer($idCon, $idSer) {
    $servicios = getServiciosXConsultorio($idCon);
    $totalServicios = count($servicios);
    $out = "";
    if ($totalServicios > 0) {
        if ($idSer == "-1") { // que no se ha seleccionado ningun servicio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalServicios; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['idServ'] = $servicios[$i]['id_servicio'];
                } else {
                    $out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalServicios; $i++) {
                if ($idSer == $servicios[$i]['id_servicio']) {
                    $out.= "<option selected value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                    $_SESSION['idServ'] = $servicios[$i]['id_servicio'];
                } else {
                    $out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN SERVICIOS</option>";
    }
    return $out;
}

function opcionesMed($idCon, $idSer, $idMed) {
    $medicos = getMedicosXServicioXConsultorio($idCon, $idSer);
    $totalMedicos = count($medicos);
    $out = "";
    if ($totalMedicos > 0) {
        if ($idMed == "-1") { // que no se ha seleccionado ningun servicio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalMedicos; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['idDr'] = $medicos[$i]['id_medico'];
                } else {
                    $out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalMedicos; $i++) {
                if ($idMed == $medicos[$i]['id_medico']) {
                    $out.= "<option selected value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                    $_SESSION['idDr'] = $medicos[$i]['id_medico'];
                } else {
                    $out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN MEDICOS</option>";
    }
    return $out;
}

function getFolios($id_medico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM folios WHERE id_medico='" . $id_medico . "' ORDER BY fecha_asignacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_folio' => $row_query['id_folio'],
                'id_medico' => $row_query['id_medico'],
                'serie' => $row_query['serie'],
                'folio_inicial' => $row_query['folio_inicial'],
                'folio_final' => $row_query['folio_final'],
                'folio_actual' => $row_query['folio_actual'],
                'fecha_asignacion' => $row_query['fecha_asignacion']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getSeries() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM series ORDER BY serie ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_serie' => $row_query['id_serie'],
                'serie' => $row_query['serie']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getFoliosXserie($serie) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM folios WHERE serie='" . $serie . "' ORDER BY fecha_asignacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_folio' => $row_query['id_folio'],
                'id_medico' => $row_query['id_medico'],
                'serie' => $row_query['serie'],
                'folio_inicial' => $row_query['folio_inicial'],
                'folio_final' => $row_query['folio_final'],
                'folio_actual' => $row_query['folio_actual'],
                'fecha_asignacion' => $row_query['fecha_asignacion']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getFolioActual($id_medico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM folios WHERE id_medico='" . $id_medico . "' ORDER BY fecha_asignacion DESC LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) { // si ya estan dados de alta sus folios
        $ret["id_folio"] = $row_query['id_folio'];
        $ret["serie"] = $row_query['serie'];
        $ret["folio_inicial"] = intval($row_query['folio_inicial']);
        $ret["folio_final"] = intval($row_query['folio_final']);
        $ret["folio_actual"] = intval($row_query['folio_actual']);
    } else { // si no estan dados de alta sus  folios (medico nuevo)
        $query_query = "INSERT INTO folios VALUES('','" . $id_medico . "','" . SERIE . "','" . FOLIO_INICIAL . "','" . FOLIO_FINAL . "','" . FOLIO_ACTUAL . "','" . date('Ymd') . "');";
        $query = mysql_query($query_query, $bdissste) or die(mysql_error());
        $query_query = "SELECT * FROM folios WHERE id_medico='" . $id_medico . "' ORDER BY fecha_asignacion DESC LIMIT 1";
        $query = mysql_query($query_query, $bdissste) or die(mysql_error());
        $row_query = mysql_fetch_assoc($query);
        $ret["id_folio"] = $row_query['id_folio'];
        $ret["serie"] = $row_query['serie'];
        $ret["folio_inicial"] = intval($row_query['folio_inicial']);
        $ret["folio_final"] = intval($row_query['folio_final']);
        $ret["folio_actual"] = intval($row_query['folio_actual']);
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function ponerCeros($cant, $cuantos) {
    $aCeros = array("", "0", "00", "000", "0000", "00000", "000000", "0000000", "00000000", "000000000", "0000000000", "00000000000", "000000000000", "0000000000000", "00000000000000", "000000000000000");
    $tCant = strlen($cant);
    $cuantos = $cuantos - $tCant;
    return $aCeros[$cuantos] . $cant;
}

function getRecetaXfolio($folio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE folio='" . $folio . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'serie' => $row_query['serie'],
            'folio' => $row_query['folio'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha' => $row_query['fecha'],
            'hora' => $row_query['hora'],
            'codigo_barras' => $row_query['codigo_barras'],
            'entidad_federativa' => $row_query['entidad_federativa'],
            'clave_unidad_medica' => $row_query['clave_unidad_medica'],
            'numero_vale' => $row_query['numero_vale'],
            'dgp' => $row_query['dgp'],
            'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
            'id_usuario_surtio' => $row_query['id_usuario_surtio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetaXCodigoBarras($cod_bar) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE codigo_barras='" . $cod_bar . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query2 > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'serie' => $row_query['serie'],
            'folio' => $row_query['folio'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha' => $row_query['fecha'],
            'hora' => $row_query['hora'],
            'codigo_barras' => $row_query['codigo_barras'],
            'entidad_federativa' => $row_query['entidad_federativa'],
            'clave_unidad_medica' => $row_query['clave_unidad_medica'],
            'numero_vale' => $row_query['numero_vale'],
            'dgp' => $row_query['dgp'],
            'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
            'id_usuario_surtio' => $row_query['id_usuario_surtio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetaXid($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_receta='" . $id_receta . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query2 > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'serie' => $row_query['serie'],
            'folio' => $row_query['folio'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha' => $row_query['fecha'],
            'hora' => $row_query['hora'],
            'codigo_barras' => $row_query['codigo_barras'],
            'entidad_federativa' => $row_query['entidad_federativa'],
            'clave_unidad_medica' => $row_query['clave_unidad_medica'],
            'numero_vale' => $row_query['numero_vale'],
            'dgp' => $row_query['dgp'],
            'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
            'id_usuario_surtio' => $row_query['id_usuario_surtio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function regresarIdConsultorio($idDr) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT id_consultorio FROM servicios_x_consultorio WHERE id_medico='" . $idDr . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $id_servicio = $row_query['id_consultorio'];
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $id_servicio;
}

function regresarIdServicio($idDr) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT id_servicio FROM servicios_x_consultorio WHERE id_medico='" . $idDr . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $id_servicio = $row_query['id_servicio'];
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $id_servicio;
}

function getExistenciasMedicamentos($id_medicamento) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_existencias WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_medicamento' => $row_query['id_medicamento'],
            'existencia_anterior' => $row_query['existencia_anterior'],
            'existencia_actual' => $row_query['existencia_actual']
        );
    } else
        $ret["id_medicamento"] = 0;
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function restaExistenciasMedicamentos($id_medicamento, $cantidad) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_existencias WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $existencia_actual = (int) $row_query["existencia_actual"];
    $resta = $existencia_actual - (int) $cantidad;
    $query_query = "UPDATE medicamentos_existencias SET existencia_actual='" . $resta . "' WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    @mysql_free_result($query);
    @mysql_close($dbissste);
}

function getDatosMedicamentos($id_medicamento) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_medicamento' => $row_query['id_medicamento'],
            'partida' => $row_query['partida'],
            'codigo_barras' => $row_query['codigo_barras'],
            'descripcion' => $row_query['descripcion'],
            'presentacion' => $row_query['presentacion'],
            'unidad' => $row_query['unidad'],
            'precio' => $row_query['precio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    } else
        $ret["id_medicamento"] = 0;
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentos() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos ORDER BY descripcion";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medicamento' => $row_query['id_medicamento'],
                'partida' => $row_query['partida'],
                'codigo_barras' => $row_query['codigo_barras'],
                'descripcion' => $row_query['descripcion'],
                'presentacion' => $row_query['presentacion'],
                'unidad' => $row_query['unidad'],
                'precio' => $row_query['precio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCodigoBarras() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM codigo_barras LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "000000000001";
    if ($totalRows_query > 0) {
        $ret = $row_query['codigo'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getIdRecetaAgregada($serie, $folioSiguiente, $id_medico, $id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT id_receta FROM recetas WHERE serie='" . $serie . "' AND folio='" . $folioSiguiente . "' AND id_medico='" . $id_medico . "' AND id_derecho='" . $id_derecho . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "0";
    if ($totalRows_query > 0) {
        $ret = $row_query['id_receta'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCita($idHorario, $fechaCita) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $idHorario . "' and fecha_cita='" . $fechaCita . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_derecho' => $row_query['id_derecho'],
            'id_cita' => $row_query['id_cita'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1'],
            'id_usuario' => $row_query['id_usuario'],
            'observaciones' => $row_query['observaciones']
        );
    } else {
        $ret = array(
            'id_derecho' => "-1",
            'id_cita' => "-1",
            'status' => "-1",
            'extra1' => "-1",
            'id_usuario' => "-1",
            'observaciones' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitaExt($id_cita) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_cita='" . $id_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'id_derecho' => $row_query['id_derecho'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita'],
            'fecha_cita' => $row_query['fecha_cita'],
            'id_usuario' => $row_query['id_usuario'],
            'extra1' => $row_query['extra1'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetas($status) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE status='" . $status . "' ORDER BY fecha DESC, hora DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDatosMedicamentoEnReceta($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_concepto' => $row_query['id_concepto'],
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'cantidad' => $row_query['cantidad'],
                'unidad' => $row_query['unidad'],
                'dias' => $row_query['dias'],
                'indicaciones' => $row_query['indicaciones'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function cambiarStatusMed($id_receta, $id_med, $status) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "UPDATE recetas_conceptos SET extra1='" . $status . "', fecha='" . date('Ymd') . "', hora='" . date('H:i:s') . "', id_usuario_surtio='" . $_SESSION["id_usuario"] . "' WHERE id_receta='" . $id_receta . "' AND id_medicamento='" . $id_med . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "' AND id_medicamento='" . $id_med . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $row_query["cantidad"];
}

function todosMedicamentosSurtidos($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = "1";
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            if ($row_query['extra1'] == "")
                $ret = "0";
        }while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function cambiarStatusReceta($id_receta, $status) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "UPDATE recetas SET status='" . $status . "' WHERE id_receta='" . $id_receta . "'  LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    @mysql_free_result($query);
    @mysql_close($dbissste);
}

function hayMedicamento($id_med, $cantidad) {
    $existencias = getExistenciasMedicamentos($id_med);
    $exist_actual = (int) $existencias["existencia_actual"];
    $cantidad = (int) $cantidad;
    $ret = false;
    if ($exist_actual < $cantidad)
        $ret = false; else
        $ret = true;
    return $ret;
}

function tieneTratamiento($id_med, $id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = false;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $query_query2 = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $row_query['id_receta'] . "' AND id_medicamento='" . $id_med . "'";
            $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $totalRows_query2 = mysql_num_rows($query2);
            if ($totalRows_query2 > 0) {
                $fecha_factura = substr($row_query["fecha"], 6, 2) . "-" . substr($row_query["fecha"], 4, 2) . "-" . substr($row_query["fecha"], 0, 4);
                $hoy = date("d-m-Y");
                $fecha_termino = strtotime($fecha_factura . " +" . $row_query2["dias"] . " day");
                $fecha_termino = date("Ymd", $fecha_termino);
                $hoy = date("Ymd");
                $hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
                $terminoMK = mktime(0, 0, 0, substr($fecha_termino, 4, 2), substr($fecha_termino, 6, 2), substr($fecha_termino, 0, 4));
                if ($terminoMK > $hoyMK)
                    $ret = true;
            }
        }while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServiciosColectivos() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM servicios WHERE id_grupo_servicio='2' AND st='1' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_servicio' => $row_query['id_servicio'],
                'clave' => $row_query['clave'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasXderechohabiente($id_derecho) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $hoy = date('Ymd');
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_derecho='" . $id_derecho . "' ORDER BY fecha_cita ASC, id_horario ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_horario' => $row_query['id_horario'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasExtXderechohabiente($id_derecho) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $hoy = date('Ymd');
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_derecho='" . $id_derecho . "' AND fecha_cita>='" . $hoy . "' ORDER BY fecha_cita ASC, hora_inicio ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_horario'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getHorarioXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM horarios WHERE id_horario='" . $id . "' ORDER BY id_horario";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = NULL;
    if ($totalRows_query > 0) {
        $ret = array(
            'id_horario' => $row_query['id_horario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'dia' => $row_query['dia'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXderecho($id_derecho, $id_medico, $id_servicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = array();
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "' AND id_medico='" . $id_medico . "' AND id_servicio='" . $id_servicio . "' ORDER BY fecha DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXderechoXcita($id_derecho, $id_medico, $id_servicio, $fecha) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = array();
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "' AND id_medico='" . $id_medico . "' AND id_servicio='" . $id_servicio . "' AND fecha='" . $fecha . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXMedico($idDr, $fechaInicio, $fechaTermino) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;

    $fechaInicio = formatoFechaBD($fechaInicio);
    $fechaTermino = formatoFechaBD($fechaTermino);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_medico='" . $idDr . "' AND fecha<='" . $fechaTermino . "' AND fecha>='" . $fechaInicio . "' ORDER BY fecha DESC, hora DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function verSolicitud($id_solicitud, $exp) {
    $arregloDatos = array();
    $out = '';
    $claveUnidadMedica = "034-203-00";
    $unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
    $delegacion = "JALISCO";
    $servicio = "CONSULTA EXTERNA";
	$saltoLinea="<br>";
    $solicitud = getSolicitudXid($id_solicitud);
    $solicitudDet = getDetallesXidSolicitud($id_solicitud);
    $fechaInicio = formatoDia($solicitud["fecha_creacion"], "fecha") . "<br>" . $solicitud["hora_creacion"];
    $fechaFin = formatoDia($solicitud["fecha_atendida"], "fecha") . "<br>" . $solicitud["hora_atendida"];
    $datosMedico = getMedicoXid($solicitud["id5"]);
    $datosServicio = getServicioXid2($solicitud["id4"]);
    $totalDet = count($solicitudDet);
    $bandera = false;
    for ($i = 0; $i < $totalDet; $i++) {
        if ($solicitudDet[$i]["extra"] == "E") {
            $cita = getCitaExtemporaneaDatos($solicitudDet[$i]["id_cita"]);
        } else {
            $cita = getCitaDatos($solicitudDet[$i]["id_cita"]);
        }
        $datosDH = getDatosDerecho($solicitudDet[$i]["id_expediente"]);
        $nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
        $expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
        $arregloDatos[] = array("id_cita" => $solicitudDet[$i]["id_cita"], "id_horario" => $solicitudDet[$i]["extra"], "id_consultorio" => $solicitud["id3"], "id_servicio" => $solicitud["id4"], "id_medico" => $solicitud["id5"], "fecha_cita" => $solicitudDet[$i]["fecha"], "hora_cita" => $solicitudDet[$i]["hora"], "id_derecho" => $solicitudDet[$i]["id_expediente"], "cedula" => $expediente, "nombre" => ponerAcentos($nombre));
    }

    $arregloDatos = orderMultiDimensionalArrayF($arregloDatos, "cedula", false);
    $tArrDatos = count($arregloDatos);
    $exped = explode(",", $exp);
    if ($tArrDatos > 0) {
        for ($i = 0; $i < $tArrDatos; $i++) {
            if (in_array($arregloDatos[$i]["id_derecho"], $exped))
                $out .= "<tr height=\"60\">
				<td align=\"center\" class=\"contenido8BordeCB\"><img src=\"barcode/barcode.php?code=" . ponerCeros($arregloDatos[$i]["id_derecho"], 8) . "&tam=1\" alt=\"barcode\" /></td>
				<td align=\"left\" class=\"contenido8BordeSI\">" . formatoDia($arregloDatos[$i]["fecha_cita"], "imprimirCita") . " " . formatoHora($arregloDatos[$i]["hora_cita"]) . "</td>
				<td align=\"left\" class=\"contenido8Borde\">" . $arregloDatos[$i]["cedula"] . "</td>
				<td align=\"left\" class=\"contenido8Borde\">" . $arregloDatos[$i]["nombre"] . "</td>
				<td align=\"left\" class=\"contenido8Borde\">" . $arregloDatos[$i]["observaciones"] . "</td>
			  </tr>";
        }
    }
    $claveDelMedico = $datosMedico['n_empleado'];
    $nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
    $claveDeLaEspecialidad = $datosServicio['clave'];
    $especialidad = ponerAcentos($datosServicio['nombre']);
    $horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
    $reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td width=\"74\" align=\"left\"><img src=\"diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
				<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
				  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
				<td align=\"center\">SUBDIRECCI&Oacute;N GENERAL M&Eacute;DICA<br />Formato de Salida para Archivo</td>
				<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"180\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
				  	<td colspan=\"2\" align=\"center\"><img src=\"barcode/barcode.php?code=" . ponerCeros($id_solicitud, 12) . "&tam=1\" alt=\"barcode\" /></td>
				  </tr>
				  </table>
				  <table width=\"180\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
					<td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
				  </tr>
				  <tr>
					<td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
					<td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
				  </tr>
				</table></td>
			  </tr>
			</table></td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad M&Eacute;dica</td>
		  </tr>
		  <tr>
			<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad M&eacute;dica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad M&eacute;dica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Estado: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del M&eacute;dico</td>
		  </tr>
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td align=\"left\" class=\"contenido8bold\" width=\"50\">Clave del M&eacute;dico:</td>
				<td align=\"left\" class=\"contenido8\" width=\"100\">" . $claveDelMedico . "</td>
				<td align=\"left\" class=\"contenido8bold\" width=\"100\">Nombre del M&eacute;dico:</td>
				<td align=\"left\" class=\"contenido8\" width=\"300\">" . $nombreDelMedico . "</td>
				<td align=\"left\" class=\"contenido8bold\" width=\"50\">Servicio:</td>
				<td align=\"left\" class=\"contenido8\" width=\"300\">" . $servicio . "</td>
			  </tr>
			  <tr>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Clave de la Especialidad:</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $claveDeLaEspecialidad . "</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Especialidad:</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $especialidad . "</td>
				<td align=\"left\" class=\"contenido8bold\">Horario:</td>
				<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
			  </tr>
			  <tr>
				<td align=\"left\" class=\"contenido8bold\">Firma:</td>
				<td align=\"left\" class=\"contenido8\" height=\"40\">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		  <tr>
			<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
		  </tr>
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td width=\"120\" align=\"center\" class=\"contenido8boldBorde\">Expediente</td>
				<td width=\"90\" align=\"center\" class=\"contenido8boldBorde\">Fecha y Hora</td>
				<td width=\"60\" align=\"center\" class=\"contenido8boldBorde\">C&eacute;dula</td>
				<td width=\"200\" align=\"center\" class=\"contenido8boldBorde\">Nombre Paciente</td>
				<td align=\"center\" class=\"contenido8boldBorde\">Observaciones</td>
				</tr>" . $out . "
			</table></td>
		  </tr>
		  <tr>
			<td align=\"center\"><br><table width=\"90%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\"><tr><th width=\"50%\">ENTREGA ARCHIVO</th><th>RECIBE SERVICIO</th></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">No. de Empleado:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">No. de Empleado:</td></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">Nombre:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">Nombre:</td></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">Firma:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">Firma:</td></tr></table></td>
		  </tr>
		  <tr>
			<td align=\"center\"><br><table width=\"90%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\"><tr><th width=\"50%\">ENTREGA SERVICIO</th><th>RECIBE ARCHIVO</th></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">No. de Empleado:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">No. de Empleado:</td></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">Nombre:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">Nombre:</td></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">Firma:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">Firma:</td></tr></table></td>
		  </tr>
		</table>" . $saltoLinea;
    return $reporte;
}

function verSolicitudExpon($id_solicitud, $exp) {
    $arregloDatos = array();
    $out = '';
    $claveUnidadMedica = "034-203-00";
    $unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
    $delegacion = "JALISCO";
    $servicio = "CONSULTA EXTERNA";

    $solicitud = getSolicitudXid($id_solicitud);
    $solicitudDet = getDetallesXidSolicitud($id_solicitud);
    $fecha = formatoDia($solicitud["fecha_creacion"], "fecha") . "<br>" . $solicitud["hora_creacion"];
    $datosMedico = getMedicoXid($solicitud["id5"]);
    $datosServicio = getServicioXid($solicitud["id5"]);
    $totalDet = count($solicitudDet);
    $bandera = false;
    for ($i = 0; $i < $totalDet; $i++) {
        if ($solicitudDet[$i]["extra"] == "E") {
            $cita = getCitaExtemporaneaDatos($solicitudDet[$i]["id_cita"]);
        } else {
            $cita = getCitaDatos($solicitudDet[$i]["id_cita"]);
        }
        $datosDH = getDatosDerecho($solicitudDet[$i]["id_expediente"]);
        $nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
        $expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
        $arregloDatos[] = array("id_cita" => $solicitudDet[$i]["id_cita"], "id_horario" => $solicitudDet[$i]["extra"], "id_consultorio" => $solicitud["id3"], "id_servicio" => $solicitud["id4"], "id_medico" => $solicitud["id5"], "fecha_cita" => $solicitudDet[$i]["fecha"], "hora_cita" => $solicitudDet[$i]["hora"], "id_derecho" => $solicitudDet[$i]["id_expediente"], "cedula" => $expediente, "nombre" => ponerAcentos($nombre));
    }

    $arregloDatos = orderMultiDimensionalArrayF($arregloDatos, "cedula", false);
    $tArrDatos = count($arregloDatos);
    $exped = explode(",", $exp);
    if ($tArrDatos > 0) {
        for ($i = 0; $i < $tArrDatos; $i++) {
            if (in_array($arregloDatos[$i]["id_derecho"], $exped))
                $out .= "<tr height=\"60\">
				<td align=\"center\" class=\"contenido8BordeCB\"><img src=\"barcode/barcode.php?code=" . ponerCeros($arregloDatos[$i]["id_derecho"], 8) . "&tam=1\" alt=\"barcode\" /></td>
				<td align=\"left\" class=\"contenido8BordeSI\">" . formatoDia($arregloDatos[$i]["fecha_cita"], "imprimirCita") . " " . substr($arregloDatos[$i]["hora_cita"], 0, 5) . "</td>
				<td align=\"left\" class=\"contenido8Borde\">" . $arregloDatos[$i]["cedula"] . "</td>
				<td align=\"left\" class=\"contenido8Borde\">" . $arregloDatos[$i]["nombre"] . "</td>
				<td align=\"left\" class=\"contenido8Borde\">" . $arregloDatos[$i]["observaciones"] . "</td>
			  </tr>";
        }
    }
    $claveDelMedico = $datosMedico['n_empleado'];
    $nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
    $claveDeLaEspecialidad = $datosServicio['clave'];
    $especialidad = ponerAcentos($datosServicio['nombre']);
    $horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";

    $aQuienSePresta = getMedicoXid($solicitud["id3"]);
    $datosAquienSePresta = ponerAcentos($aQuienSePresta['ap_p'] . " " . $aQuienSePresta['ap_m'] . " " . $aQuienSePresta['nombres']);
    $reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td width=\"74\" align=\"left\"><img src=\"diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
				<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
				  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
				<td align=\"center\">SUBDIRECCI&Oacute;N GENERAL M&Eacute;DICA<br />Formato de Salida para Archivo</td>
				<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"180\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
				  	<td colspan=\"2\" align=\"center\"><img src=\"barcode/barcode.php?code=" . ponerCeros($id_solicitud, 12) . "&tam=1\" alt=\"barcode\" /></td>
				  </tr>
				  </table>
				  <table width=\"180\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td colspan=\"2\" class=\"tituloEncabezado\" align=\"center\">Fecha Solicitud</td>
				  </tr>
				  <tr>
					<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">" . $fecha . "</td>
				  </tr>
				</table></td>
			  </tr>
			</table></td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad M&eacute;dica</td>
		  </tr>
		  <tr>
			<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad M&eacute;dica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad M&eacute;dica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Estado: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del prestamo</td>
		  </tr>
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td align=\"left\" class=\"contenido8bold\" width=\"150\">Persona a quien se presta:</td>
				<td align=\"left\" class=\"contenido8\" width=\"180\">" . $datosAquienSePresta . "</td>
				<td align=\"left\" class=\"contenido8bold\" width=\"80\">Servicio:</td>
				<td align=\"left\" class=\"contenido8\" width=\"180\">" . $datosServicio . "</td>
				<td align=\"left\" class=\"contenido8bold\" width=\"70\">Observaciones:</td>
				<td align=\"left\" class=\"contenido8\" height=\"50\">" . $solicitud["id4"] . "</td>
			  </tr>
			</table></td>
		  </tr>
		  <tr>
			<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
		  </tr>
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td width=\"120\" align=\"center\" class=\"contenido8boldBorde\">Expediente</td>
				<td width=\"90\" align=\"center\" class=\"contenido8boldBorde\">Fecha y Hora</td>
				<td width=\"60\" align=\"center\" class=\"contenido8boldBorde\">C&eacute;dula</td>
				<td width=\"200\" align=\"center\" class=\"contenido8boldBorde\">Nombre Paciente</td>
				<td align=\"center\" class=\"contenido8boldBorde\">Observaciones</td>
				</tr>" . $out . "
			</table></td>
		  </tr>
		  <tr>
			<td align=\"center\"><br><table width=\"90%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\"><tr><th width=\"50%\">RECIBE PERSONA</th><th>RECIBE ARCHIVO</th></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">No. de Empleado:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">No. de Empleado:</td></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">Nombre:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">Nombre:</td></tr><tr><td align=\"left\" valign=\"top\" height=\"40\" class=\"contenido8\">Firma:</td><td align=\"left\" valign=\"top\" class=\"contenido8\">Firma:</td></tr></table></td>
		  </tr>
		</table>" . $saltoLinea;
    return $reporte;
}

function getCitaDatos($id_cita) {
    global $hostname_bdisssteR;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdisssteR, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_cita='" . $id_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_horario' => $row_query['id_horario'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitaExtemporaneaDatos($id_cita) {
    global $hostname_bdisssteR;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdisssteR, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_cita='" . $id_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'id_derecho' => $row_query['id_derecho'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function orderMultiDimensionalArrayF($toOrderArray, $field, $inverse = false) {
    $position = array();
    $newRow = array();
    foreach ($toOrderArray as $key => $row) {
        $position[$key] = $row[$field];
        $newRow[$key] = $row;
    }
    if ($inverse) {
        arsort($position);
    } else {
        asort($position);
    }
    $returnArray = array();
    foreach ($position as $key => $pos) {
        $returnArray[] = $newRow[$key];
    }
    return $returnArray;
}

function tieneCita($id_derecho, $fecha) {
    global $hostname_bdisssteR;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    global $database_bdisssteC;
    global $username_bdisssteC;
    global $password_bdisssteC;
    $ret = false;
    $bdissste = mysql_pconnect($hostname_bdisssteR, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_derecho='" . $id_derecho . "' AND fecha_cita='" . $fecha . "' AND status='1'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalCitas = mysql_num_rows($query);
    @mysql_free_result($query);
    @mysql_close($dbissste);
    if ($totalCitas > 0) { // tiene cita
        $ret = true;
    } else {
        $bdissste = mysql_pconnect($hostname_bdisssteR, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
        mysql_select_db($database_bdisssteR, $bdissste);
        $query_query = "SELECT * FROM citas_extemporaneas WHERE id_derecho='" . $id_derecho . "' AND fecha_cita='" . $fecha . "' AND status='1'";
        $query = mysql_query($query_query, $bdissste) or die(mysql_error());
        $totalCitasExt = mysql_num_rows($query);
        @mysql_free_result($query);
        @mysql_close($dbissste);
        if ($totalCitasExt > 0) { // tiene cita extemporanea
            $ret = true;
        } else {
            $bdissste = mysql_pconnect($hostname_bdisssteR, $username_bdisssteC, $password_bdisssteC) or trigger_error(mysql_error(), E_USER_ERROR);
            mysql_select_db($database_bdisssteC, $bdissste);
            $query_query = "SELECT * FROM pacientes_en_piso WHERE id_derecho='" . $id_derecho . "'";
            $query = mysql_query($query_query, $bdissste) or die(mysql_error());
            $camas = mysql_num_rows($query);
            @mysql_free_result($query);
            @mysql_close($dbissste);
            if ($camas > 0)
                $ret = true;
        }
    }
    return $ret;
}

function diasDiferencia($hoy, $otra) {
    $hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
    $otraMK = mktime(0, 0, 0, substr($otra, 4, 2), substr($otra, 6, 2), substr($otra, 0, 4));
    $segundos_diferencia = $hoyMK - $otraMK;
    $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);
    $dias_diferencia = abs($dias_diferencia);
    $dias_diferencia = floor($dias_diferencia);
    if ($otraMK < $hoyMK) {
        $dias_diferencia = -$dias_diferencia;
    }
    return $dias_diferencia;
}

function ordenarArreglo($arr) {
    $temp = array();
    $enTiempo = array();
    $antes = array();
    $retardo = array();
    $ausencia = array();
    $tRows = count($arr);
    for ($i = 0; $i < $tRows; $i++) {
        switch ($arr[$i]["orden"]) {
            case "4": //en tiempo
                $enTiempo[] = $arr[$i];
                break;
            case "3": //antes
                $antes[] = $arr[$i];
                break;
            case "1": //retardo
                $retardo[] = $arr[$i];
                break;
            case "2": //ausencia
                $ausencia[] = $arr[$i];
                break;
        }
    }
    $temp = array_merge($temp, $enTiempo);
    $temp = array_merge($temp, $antes);
    $temp = array_merge($temp, $retardo);
    $temp = array_merge($temp, $ausencia);
    return $temp;
}

function ordenar_array() {
    $n_parametros = func_num_args(); // Obenemos el n�mero de par�metros 
    if ($n_parametros < 3 || $n_parametros % 2 != 1) { // Si tenemos el n�mero de parametro mal... 
        return false;
    } else { // Hasta aqu� todo correcto...veamos si los par�metros tienen lo que debe ser... 
        $arg_list = func_get_args();

        if (!(is_array($arg_list[0]) && is_array(current($arg_list[0])))) {
            return false; // Si el primero no es un array...MALO! 
        }
        for ($i = 1; $i < $n_parametros; $i++) { // Miramos que el resto de par�metros tb est�n bien... 
            if ($i % 2 != 0) {// Par�metro impar...tiene que ser un campo del array... 
                if (!array_key_exists($arg_list[$i], current($arg_list[0]))) {
                    return false;
                }
            } else { // Par, no falla...si no es SORT_ASC o SORT_DESC...a la calle! 
                if ($arg_list[$i] != SORT_ASC && $arg_list[$i] != SORT_DESC) {
                    return false;
                }
            }
        }
        $array_salida = $arg_list[0];

        // Una vez los par�metros se que est�n bien, proceder� a ordenar... 
        $a_evaluar = "foreach (\$array_salida as \$fila){\n";
        for ($i = 1; $i < $n_parametros; $i+=2) { // Ahora por cada columna... 
            $a_evaluar .= "  \$campo{$i}[] = \$fila['$arg_list[$i]'];\n";
        }
        $a_evaluar .= "}\n";
        $a_evaluar .= "array_multisort(\n";
        for ($i = 1; $i < $n_parametros; $i+=2) { // Ahora por cada elemento... 
            $a_evaluar .= "  \$campo{$i}, SORT_REGULAR, \$arg_list[" . ($i + 1) . "],\n";
        }
        $a_evaluar .= "  \$array_salida);";
        // La verdad es que es m�s complicado de lo que cre�a en principio... :) 

        eval($a_evaluar);
        return $array_salida;
    }
}
function obtenerBloqueCon($id)
{
 $sql="select * from expedientes where id_expediente=".$id;
 $exp=  ejecutarSQL($sql);
 $ret=$exp['bloque_concentrado'];
 return $ret;
}

function MovimientosXidExpediente($id_expediente)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $sql="SELECT * FROM expedientes_movimientos WHERE id_expediente=$id_expediente Order by extra desc;";
    $query=  mysql_query($sql,$bdissste) or die(mysql_error());
    $movs=array();
    $i=0;
    if(mysql_num_rows($query)>0)
        while ($row = mysql_fetch_array($query)) {
         $movs[$i]=$row;
         $i++;
        }  
    return $movs;
}

function DetalleSolicitudXexpSol($id_sol,$id_Exp)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $sql="SELECT * FROM solicitudes_detalles WHERE id_expediente=$id_Exp AND id_solicitud=$id_sol;";
    $query=  mysql_query($sql);
    if(mysql_num_rows($query)>0)
        $ret=  mysql_fetch_assoc ($query);
    return $ret;
}

function ConseguirCamaPaciente($idDH,$fecha1,$fecha2)
{
     global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    $database="sistema_censo";
    $bdissste = mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database, $bdissste);
    $sql="select * from pacientes_en_piso where id_derecho=$idDH and (fecha_ingreso>=$fecha1 and fecha_egreso<=$fecha2)";
    $query=  mysql_query($sql);
    $paciente=  mysql_fetch_assoc($query);
    $sql="select * from camas where id_cama=".$paciente;
    $query=  mysql_query($sql);
    $camas=  mysql_fetch_assoc($query);
    $sql="select * from pisos where id_piso=".$camas['id_piso'];
    $query=  mysql_query($sql);
    $pisos=  mysql_fetch_assoc($query);
    $cama=$camas['nombre'];
    $piso=$pisos['nombre'];
    return $cama."|".$piso;
}

function getDetallesSolxIdFecha($id_exp,$fecha,$hora)
{
     global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    mysql_connect($hostname_bdissste,$username_bdissste,$password_bdissste);
    mysql_select_db($database_bdissste);
    $sql="Select id_solicitud from solicitudes_detalles where id_expediente='$id_exp' and fecha='$fecha' and hora='$hora';";
    $result=mysql_query($sql);
    $sol= mysql_fetch_array($result);
    return $sol[0];
}
?>