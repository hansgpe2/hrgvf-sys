<?php
include_once('lib/misFunciones.php');
?>
<script language="javascript" type="text/javascript">
    document.getElementById("cod_bar").focus();
</script>
<center>
    <form id="forma" name="forma" onsubmit="javascript: return enviarConcentrado();">
        <table border="0" cellpadding="0" cellspacing="10" width="780">
            <tr>
                <td colspan="2" class="tituloVentana">Concentrar Expedientes</td></tr>
            <tr><td align="right">C&oacute;digo de Barras del Expediente: </td><td align="left"><input id="cod_bar" name="cod_bar" type="text" value="" maxlength="8" onkeypress="javascript: return codigoBarras(event);" onkeyup="javascript: concentrarExpedientesCB(this);" /> <input name="buscar" type="button" value="Buscar..." onclick="javascript: concentrarExpedientesCB();" /></td></tr>
            <tr><td colspan="2" height="10" align="center" valign="top"><div id="erro">&nbsp;</div></td></tr>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
            	<tr>
                	<td>
				        <div id="buscar" style="height:110px; margin-top:10px;">
                              <table width="700" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
                                  <td align="left"><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();">
                                    <option value="cedula" selected="selected">C&eacute;dula</option>
                                    <option value="nombre">Nombre</option>
                                  </select>
                                  </td>
                                </tr>
                                <tr>
                                <td colspan="2">
                                    <div id="buscarPorCedula">
                                    <table width="700" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                                      <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                                        <input name="buscar_ced" type="button" class="botones" id="buscar_ced" value="Buscar..." onclick="javascript: buscarDHbusqueda(document.getElementById('cedulaBuscar').value);" /></td>
                                    </tr>
                                    <tr>
                                      <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                                      <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div>
                                      </td>
                                    </tr>
                                
                                    <tr>
                                      <td colspan="2" align="center">
                                        <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: recibirExpedientesCed();" value="Seleccionar Expediente" /><br /><br /></td>
                                      </tr>
                                
                                  </table>
                                  </div>
                                 <div id="buscarPorNombre" style="display:none;">
                                <table width="700" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                                  <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                                  <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                                  <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                                </tr>
                                <tr>
                                  <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                                  <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                                    <input name="buscarN" type="button" class="botones" id="buscarN" value="Buscar..." onclick="javascript: buscarDHNbusqueda(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);" /></td>
                                </tr>
                                <tr>
                                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                                  <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div>
                                  </td>
                                </tr>
                            
                                <tr>
                                  <td colspan="4" align="center">
                                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: buscarCodigoBarrasExpXCedula();" value="Seleccionar Expediente" /><br /><br /></td>
                                  </tr>
                            
                              </table>
                              </div>
                               </td>
                                </tr>
                              </table>
                        </div>
                    </td>
                </tr>
            </table>
            <tr><td colspan="2"><div id="solicitudes"></div></td></tr>
            <tr><td colspan="2" align="center"><input type="submit" id="aceptar" name="aceptar" value="A C E P T A R" /></td></tr>
        </table>
    </form>

</center>
