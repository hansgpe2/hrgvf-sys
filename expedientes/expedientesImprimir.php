<?php
include_once('lib/misFunciones.php');

$id_expediente = "";
if (isset($_REQUEST["id_expediente"])) {
	$exp = getDatosDerecho($_REQUEST["id_expediente"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Imprimir Etiqueta</title>
<link href="lib/impresion.css" rel="stylesheet" type="text/css" media="print" />
</head>

<body onload="window.print();">
<?php	
	print("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"200\"><tr><td align=\"center\" class=\"datos_etiq\">" . $exp["cedula"] . "/" . $exp["cedula_tipo"] . "<br>" . ponerAcentos($exp["ap_p"]) . " " . ponerAcentos($exp["ap_m"]) . "<br>" . ponerAcentos($exp["nombres"]) . "<br><img src=\"barcode/barcode.php?code=" . ponerCeros($_REQUEST["id_expediente"],8) . "&tam=1\" alt=\"barcode\" /></td></tr></table>");
} else {
	print("error");
}

?>

</body>
</html>
