<?php
include_once('lib/misFunciones.php');
$out = '';
if (isset($_GET["id_servicio"])) {
	$medicos = getMedicosXServicio($_GET["id_servicio"]);
	$tMedicos = count($medicos);
	$out .= '<select name="persona" id="persona">';
	$out .= '<option value="" selected="selected"></option>';
	if ($tMedicos > 0) {
		for ($i=0; $i<$tMedicos; $i++) {
			$out .= '<option value="' . $medicos[$i]['id_medico'] . '">' . $medicos[$i]['titulo'] . ' ' . ponerAcentos($medicos[$i]['ap_p'] . ' ' . $medicos[$i]['ap_m'] . ' ' . $medicos[$i]['nombres']) . '</option>';
		}
	}
	$out .= '</select>';
} else {
	$out = 'Error en variable id servicio';
}
?>
<?php echo $out ?>
