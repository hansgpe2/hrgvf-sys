<?php
error_reporting(E_ALL^E_NOTICE);
set_time_limit(1200);
include_once('lib/misFunciones.php');
session_start ();

$cod_bar = "";
$datosSolicitudes = getSolicitudesNoAtendidas("1", $cod_bar); // 1= status solicitada
$tSolicitudes = count($datosSolicitudes);
switch ($tSolicitudes) {
	case 0:
		$hoy = date("d-m-Y");
		$ayer = date("d-m-Y", strtotime( "-1 day", strtotime( $hoy ) ) ); 	
		$out = '<center>No existen solicitudes canceladas</center>';
		break;
	default:
		$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="750">';
		$out .= '<tr><td colspan="7" class="tituloVentana">Solicitudes Canceladas</td></tr>';
		$out .= '<tr><th>Folio</th><th>Fecha y Hora</th><th>Status</th><th>Consultorio</th><th>Servicio</th><th>M&eacute;dico</th><th>Acci&oacute;n</th></tr>';
		for ($i=0; $i<$tSolicitudes; $i++) {
			$datosMedico = getMedicoXid($datosSolicitudes[$i]['id5']);
			$datosConsultorio = getConsultorioXid($datosSolicitudes[$i]['id3']);
			$datosServicio = getServicioXid($datosSolicitudes[$i]['id4']);
	//		$datosDerecho = getDatosDerecho($datosSolicitudes[$i]['id_derecho']);
			$out .= '<tr class="renglonSolicitudes2"><td>' . ponerCeros($datosSolicitudes[$i]["id_solicitud"],12) . '</td><td>' . formatoDia($datosSolicitudes[$i]["fecha_creacion"],"fecha") . ' - ' . $datosSolicitudes[$i]["hora_creacion"] . '</td><td>' . $statusSolicitudes[0] . '</td><td>' . ponerAcentos($datosConsultorio['nombre']) . '</td><td>' . ponerAcentos($datosServicio) . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td align="right"><input type="button" name="atn_' . $datosSolicitudes[$i]["id_solicitud"] . '" id="atn_' . $datosSolicitudes[$i]["id_solicitud"] . '" value="Ver" onclick="javascript: solicitudesVer(\'' . $datosSolicitudes[$i]["id_solicitud"] . '\');"></td></tr>';
		}
		$out .= '</table></center>';
		break;
		
}
print($out);

?>

