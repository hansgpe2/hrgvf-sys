<?php
session_start ();
include_once('lib/misFunciones.php');

$hoy = date('d-m-Y');


$expedientes = getExpedientesActivos();
$tExp = count($expedientes);
for ($i=0; $i<$tExp; $i++) {
	$det = getUltimoDetalle($expedientes[$i]["id_expediente"]);
	$solicitudes = getSolicitudXid($det["id_solicitud"]);
	if ($solicitudes["tipo"] == "2") { // se filtran los expedientes que sean por ventanilla
		$derecho = getDatosDerecho($expedientes[$i]["id_expediente"]);
		$med = getMedicoXid($solicitudes["id3"]);
		$ser = getServicioXid($solicitudes["id5"]);
		$fecha = $solicitudes["fecha_creacion"];
		$diasDif = diasDiferencia($fecha, date("Ymd"));
		$fecha_salida = formatoDia($solicitudes["fecha_creacion"],"fecha") . " " . $solicitudes["hora_creacion"];
		$arr[] = array("id_expediente" => $expedientes[$i]["id_expediente"], "cedula" => $derecho["cedula"] . "/" . $derecho["cedula_tipo"], "nombre" => ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]), "medico" => ponerAcentos($med["ap_p"] . " " . $med["ap_m"] . " " . $med["nombres"]), "servicio" => ponerAcentos($ser), "fecha_salida" => $fecha_salida, "dias" => $diasDif);
	}
}


/*
$solicitudes = getSolicitudesXtipoYstatus("2", "2", "fecha_creacion"); // 2= ventanilla 2=en transito
$tSol = count ($solicitudes);
$ret = '';
$arr = array();

for ($i=0; $i<$tSol; $i++) {
	$det = getDetallesXidSolicitud($solicitudes[$i]["id_solicitud"]);
	$tDet = count ($det);
	for ($j=0; $j<$tDet; $j++) {
		$exp = getExpedienteXid($det[$j]["id_expediente"]);
		if ($exp["status"] > 1 ) {
			$derecho = getDatosDerecho($det[$j]["id_expediente"]);
			$med = getMedicoXid($solicitudes[$i]["id3"]);
			$ser = getServicioXid($solicitudes[$i]["id5"]);
			$fecha = $solicitudes[$i]["fecha_creacion"];
			$diasDif = diasDiferencia($fecha, date("Ymd"));
			$fecha_salida = formatoDia($solicitudes[$i]["fecha_creacion"],"fecha") . " " . $solicitudes[$i]["hora_creacion"];
			$arr[] = array("id_expediente" => $det[$j]["id_expediente"], "cedula" => $derecho["cedula"] . "/" . $derecho["cedula_tipo"], "nombre" => ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]), "medico" => ponerAcentos($med["ap_p"] . " " . $med["ap_m"] . " " . $med["nombres"]), "servicio" => ponerAcentos($ser), "fecha_salida" => $fecha_salida, "dias" => $diasDif);
		}
	}
}
*/
$arr = ordenar_array($arr, 'cedula', SORT_ASC, 'dias', SORT_DESC) or die('<br>ERROR!<br>'); 
$tArr = count($arr);
for ($j=0; $j<$tArr; $j++) {
	$ret .= '<tr class="Azul"><td style="padding:3px;" align="left">' . $arr[$j]["cedula"] . '</td><td style="padding:3px;" align="left">' . $arr[$j]["nombre"] . '</td><td style="padding:3px;" align="left">' . $arr[$j]["medico"] . '</td><td style="padding:3px;" align="left">' . $arr[$j]["servicio"] . '</td><td width="50" style="padding:3px;" align="left">' . $arr[$j]["fecha_salida"] . '</td><td align="center" width="50" style="padding:3px;">' . $arr[$j]["dias"] . '</td></tr>';
}

$titulo = 'Expedientes no devueltos por ventanilla';
$encabezado = '
    <tr>
      <td class="tituloVentana" height="23" colspan="6">' . $titulo . '</td>
    </tr>
';
$pie = '<p align="center"><input type="button" name="imprimir" id="imprimir" value="Imprimir" class="botones" onclick="javascript: window.open(\'' . $_SERVER['PHP_SELF'] . '?imprimir=si\')"></p>';
if (isset($_REQUEST["imprimir"])) {
	$encabezado = '
	  <tr>
		<td colspan="6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="74" align="left"><img src="diseno/logoEncabezado.png" width="74" height="74" /></td>
			<td width="80" class="tituloIssste" align="left">Instituto de<br />
			  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
			<td align="center">SUBDIRECCIÓN GENERAL MÉDICA<br />' . $titulo . '</td>
			<td width="150" valign="bottom" align="right"><table width="150" border="2" cellspacing="0" cellpadding="0">
			  <tr>
				<td align="center">Fecha</td>
			  </tr>
			  <tr>
				<td align="center" class="contenido8bold">' . $hoy . '</td>
			  </tr>
			</table></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
		<td align="center" class="tituloEncabezadoConBorde" colspan="6">Unidad M&eacute;dica: H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN</td>
	  </tr>
	';
	$pie = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $titulo ?></title>
<link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
<link href="lib/impresion.css" media="print" rel="stylesheet" type="text/css" />
</head>

<body>
<center>
  <table width="750" border="1" cellspacing="0" cellpadding="0" class="ventana">
  <?php echo $encabezado ?>
    <tr class="AzulN">
    	<td align="center">C&eacute;dula</td>
    	<td align="center">Nombre del Derechohabiente</td>
    	<td align="center">M&eacute;dico o persona a quien se prest&oacute;</td>
    	<td align="center">Servicio</td>
    	<td align="center">Fecha Salida</td>
    	<td align="center">D&iacute;as sin devolver</td>
    </tr>
    <?php echo $ret ?>
  </table>
  <?php echo $pie ?>
</center>
</body>
</html>
