<?php

include_once('lib/misFunciones.php');
$id_expediente = '';
$ret = '';
if (isset($_REQUEST["id_derecho"])) {
    $id_expediente = $_REQUEST["id_derecho"];
    $expediente = getExpedienteXid($id_expediente);
    $ret .= '<br><center><table border="0" cellpadding="5" cellspacing="0" width="700">';
    if (count($expediente) > 0) { // si existe el expediente
        $estado = '';
        $ubicacion = '';
        $movimientos = '';
        $movimientos_exp = MovimientosXidExpediente($id_expediente);
        $numMovs = count($movimientos_exp);
        if ($numMovs > 0) {
            $movimientos .= '<table width="700" border="1" cellspacing="0" cellpadding="0" class="ventana">        
								<tr>
								  <td colspan="5" class="tituloVentana">DETALLES DE MOVIMIENTOS</td>
								</tr>
								<tr>
                                                                <th align="center" class="AzulN">Movimiento</th>
								  <th align="center" class="AzulN">Fecha</th><th align="center" class="AzulN">Hora</th><th align="center" class="AzulN">Origen</th><th align="center" class="AzulN">Destino</th>
								</tr>';
            for ($i = 0; $i <= $numMovs - 1; $i++) {
                $movimiento = $movimientos_exp[$i];
                $UA = $movimiento[2];//Indice de Ubicacion Actual
                switch ($UA) {
                    case 1:
                        $ubicacionAct = "Archivo";
                        break;
                    case 2:
                        $detallesSolicitud = DetalleSolicitudXexpSol($movimiento['extra'], $id_expediente);
                        $datosHorario = getHorarioXid($detallesSolicitud['extra']);
                        $servicio = getServicioXid($datosHorario['id_servicio']);
                        $medico = getMedicoXid($datosHorario['id_medico']);
                        $ubicacionAct = $servicio . "/" . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'];
                        break;
                    case 3:
                        $cama = ConseguirCamaPaciente($id_expediente, $movimiento['fecha_ua'], $movimiento['fecha_ud']);
                        $ubi = explode("|", $cama);
                        $ubicacionAct = "Piso " . $ubi[1] . "Cama " . $ubi[0];
                        break;
                    case 4:
                        $detallesSolicitud = getSolicitud($movimiento['extra']);
                        $doctor = getMedicoXid($detallesSolicitud['id3']);
                        $servicio = getServicioXid($medico['id_servicio1']);
                        $ubicacionAct = ' - de Ventanilla a ' . $doctor['titulo'] . ' ' . $doctor['ap_p'] . ' ' . $doctor['ap_m'] . ' ' . $doctor['nombres'] . "<b> / </b>" . $servicio;
                        break;
                    case 5:
                        $bloque = obtenerBloqueCon($id_expediente);
                        $ubicacion = "- bloque " . $bloque;
                        break;
                }
                switch ($movimiento['ubicacion_destino']) {
                    case 1:
                        $ubicacionDestino = "Archivo";
                        break;
                    case 2:
                        $detallesSolicitud = DetalleSolicitudXexpSol($movimiento['extra'], $id_expediente);//Cambiar por fetch_array si falla
                        if($detallesSolicitud['extra']=="E")
                            goto extemp;
                        else {
                        $datosHorario = getHorarioXid($detallesSolicitud['extra']);
                        $servicio = getServicioXid($datosHorario['id_servicio']);
                        $medico = getMedicoXid($datosHorario['id_medico']);
                        $ubicacionDestino = "Consulta de $servicio -" . $medico[6] . " " . $medico[7] . " " . $medico[8];
                        }
                        break;
                    case 3:
                        //censo:
                        $cama = ConseguirCamaPaciente($id_expediente, $movimiento['fecha_ua'], $movimiento['fecha_ud']);
                        $ubi = explode("|", $cama);
                        $ubicacionDestino = "Piso " . $ubi[1] . "Cama " . $ubi[0];
                        break;
                    case 4:
                        extemp:
                        $detallesSolicitud = getSolicitudXid($movimiento[10]);//id de solicitud
                        if(isset($detallesSolicitud['id3'])){
                        $doctor = getMedicoXid($detallesSolicitud['id3']);//Id del medico
                        $servicio = getServicioXid($doctor[13]);//Cambiar a indice si falla
                        $ubicacionDestino = 'Prestamo de Ventanilla a ' . $doctor[3] . ' ' . $doctor[6] . ' ' . $doctor[7] . ' ' . $doctor[8] . "<b> / </b>" . $servicio;//Cambiar a numero de indice
                        }
                        else 
                        {
                            $id_solicitud=  getDetallesSolxIdFecha($id_expediente,$movimiento['fecha_ua'],$movimiento['hora_ua']);
                            $solicitud=  getSolicitudXid($id_solicitud);
                            if(count($solicitud)>0){
                            $doctor = getMedicoXid($solicitud['id3']);//Id del medico
                        $servicio = getServicioXid($doctor[13]);//Cambiar a indice si falla
                        $ubicacionDestino = 'Prestamo de Ventanilla a ' . $doctor[3] . ' ' . $doctor[6] . ' ' . $doctor[7] . ' ' . $doctor[8] . "<b> / </b>" . $servicio;//Cambiar a numero de indice
                            }
                            else
                                $ubicacionDestino = 'Prestamo de Ventanilla';//Cambiar a numero de indice
                        } 
                        break;
                    case 5:
                        $bloque = obtenerBloqueCon($id_expediente);
                        $ubicacionDestino = "Concentrado - bloque " . $bloque;
                        break;
                }
                if ($movimiento['fecha_ud'] != "") {


                    $mov = "<tr><td class='Azul'>&nbsp</td><td class='Azul'>" . date("d/m/Y",  strtotime($movimiento['fecha_ud'])) . "</td><td class='Azul'>" . $movimiento['hora_ud'] . "</td><td class='Azul'>" . $ubicacionDestino . "</td><td class='Azul'>$ubicacionAct</td></tr>
                            <tr><td class='Azul'>".ponerCeros($movimiento['extra'],12)."<td class='Azul'>" . date("d/m/Y",strtotime($movimiento['fecha_ua'])) . "</td><td class='Azul'>" . $movimiento['hora_ua'] . "</td><td class='Azul'>$ubicacionAct</td><td class='Azul'>$ubicacionDestino</td></tr>";
                }
                else
                    $mov = "<tr><td class='Azul'>".ponerCeros($movimiento['extra'],12)."<td class='Azul'>" . date("d/m/Y",  strtotime ($movimiento['fecha_ua'])) . "</td><td class='Azul'>" . $movimiento['hora_ua'] . "</td><td class='Azul'>$ubicacionAct</td><td class='Azul'>$ubicacionDestino</td></tr>";
                $movimientos.=$mov;
            }
        } else
            $movimientos .= '&nbsp;';


//		$expedienteMovs = getMovimientos($id_expediente);
        switch ($expediente["status"]) {
            case 0:
                $estado = "cancelado";
                break;
            case 1:
                $estado = "Presente en ARCHIVO";
                break;
            case 2:
                $estado = "Fuera de archivo";
                break;
            case 3:
                $estado = "concentrado";
                break;
        }
        $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left" width="120">Status: </td><td align="left"><b>Expediente Creado</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="imp_' . $expediente["id_expediente"] . '" id="imp_' . $expediente["id_expediente"] . '" onclick="javascript: expedientesImprimir(\'' . $expediente["id_expediente"] . '\');" value="Imprimir Etiqueta"></td></tr>';
        $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left">C&oacute;digo de Barras: </td><td align="left"><b>' . ponerCeros($expediente["id_expediente"], 8) . '</b></td></tr>';
        $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left">Ubicaci&oacute;n Actual: </td><td align="left"><b>' . $estado . $ubicacion . '</b></td></tr>';
        $ret .= '<tr><td align="left" colspan="2">' . $movimientos . '</td></tr>';
    } else { // no existe el expediente
        $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left" width="70">Status: </td><td align="left"><b>No se ha creado el expediente</b></td></tr>';
    }
    $exp = getDatosDerecho($id_expediente);
    $ret .= '</table></center>';
    print($ret);
//	print("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"200\"><tr><td><img src=\"diseno/logo-etiqueta.jpg\" height=\"60\"></td><td align=\"center\" class=\"datos_etiq\">" . $exp["cedula"] . "/" . $exp["cedula_tipo"] . "<br>" . ponerAcentos($exp["ap_p"]) . " " . ponerAcentos($exp["ap_m"]) . "<br>" . ponerAcentos($exp["nombres"]) . "</td></tr></table>");
} else {
    print("error");
}
?>

