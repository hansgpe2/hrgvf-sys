<?php
include_once('lib/misFunciones.php');

$id_expediente = '';
$ret = '';

function getDetallesXidExp2014($id_expediente) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM solicitudes_detalles WHERE id_expediente='" . $id_expediente . "' AND status='1' ORDER BY id_detalle DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_detalle' => $row_query['id_detalle'],
                'id_solicitud' => $row_query['id_solicitud'],
                'id_expediente' => $row_query['id_expediente'],
                'id_cita' => $row_query['id_cita'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'status' => $row_query['status'],
                'extra' => $row_query['extra']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRenglon($id_expediente, $expedientes) {
	$estado = '';
	$ubicacion = '';
	$ret = '';
//	$aExpedientes = explode(",", $expedientes);
	$aExpedientes = array();
	$expediente = getExpedienteXid($id_expediente);
	if (count($expediente) > 0) { // si existe el expediente
		array_unshift($aExpedientes, $id_expediente);
	}
	$aExpedientes = array_unique($aExpedientes);
	$tExp = count($aExpedientes);
	if ($tExp >= 1) { //no esta vacio o tiene solo el que se está regresando
		$tExp2 = $tExp-1;
		foreach ($aExpedientes as $indice => $valor) {
			if (is_numeric($valor)) {
				$estado = '';
				$ubicacion = '';
				//$expediente = getExpedienteXid($valor);
				$solicitudes_detalles = getDetallesXidExp2014($valor);
				$tDetalles = count($solicitudes_detalles);
				if ($tDetalles > 0) {
					$solicitud = getSolicitudXid($solicitudes_detalles[0]["id_solicitud"]);
					if ($solicitudes_detalles[0]["id_cita"] == "0") { // es un prestamo en ventanilla
						$medico = getMedicoXid($solicitud["id3"]);
                                                if(count($medico)>0)
						$ubicacion = ' - de Ventanilla a ' . ponerAcentos($medico["ap_p"] . ' ' . $medico["ap_m"]. ' ' . $medico["nombres"]);
                                                else
                                                {
                                                    $ubicacion = ' - de Ventanilla a ' . ponerAcentos($solicitud['id3']);
                                                }
					} else { // es una cita
						$consultorio = getConsultorioXid($solicitud["id3"]);
						$servicio = getServicioXid($solicitud["id4"]);
						$medico = getMedicoXid($solicitud["id5"]);
						$ubicacion = ' - ' . ponerAcentos($consultorio["nombre"]) . ' ' . ponerAcentos($servicio) . ' ' . ponerAcentos($medico["ap_p"] . ' ' . $medico["ap_m"]. ' ' . $medico["nombres"]);
					}
				}
				switch ($expediente["status"])
                {
                    case 0:
                        $estado="cancelado";
                        break;
                    case 1:
                        $estado = "Presente en ARCHIVO";
                        break;
                    case 2:
                        $estado = "Fuera de archivo";
                        break;
                    case 3:
                        $estado="concentrado";
                        break;
                }	
				$derecho = getDatosDerecho($expediente["id_expediente"]);
              if(count($solicitudes_detalles)>0)
					$ret .= '<tr class="renglonSolicitudes" height="24"><td align="left"><input onclick="return false;" type="checkbox" id="exp_' . $expediente["id_expediente"] . '_' . $solicitudes_detalles[0]["id_solicitud"] . '" name="exp_' . $expediente["id_expediente"] . '_' . $solicitudes_detalles[0]["id_solicitud"] . '" checked="checked">' . ponerCeros($expediente["id_expediente"],8) . '</td><td align="left">' . $derecho["cedula"] . '/' . $derecho["cedula_tipo"] . " - " . ponerAcentos($derecho["ap_p"]) . " " . ponerAcentos($derecho["ap_m"]) . " " . ponerAcentos($derecho["nombres"]) . " " . '</td><td align="left">' . ponerCeros($solicitudes_detalles[0]["id_solicitud"],12) . '</td><td align="left"><b>' . $estado . $ubicacion . '</b></td><td>' . formatoDia($solicitud["fecha_creacion"], "fecha") . '</td></tr>';
              else
                  $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left"><input onclick="return false;" type="checkbox" id="exp_' . $expediente["id_expediente"] . '_" name="exp_' . $expediente["id_expediente"] . '_" checked="checked">' . ponerCeros($expediente["id_expediente"],8) . '</td><td align="left">' . $derecho["cedula"] . '/' . $derecho["cedula_tipo"] . " - " . ponerAcentos($derecho["ap_p"]) . " " . ponerAcentos($derecho["ap_m"]) . " " . ponerAcentos($derecho["nombres"]) . " " . '</td><td align="left">' . ponerCeros('0000',12) . '</td><td align="left"><b>' . $estado . $ubicacion . '</b></td><td>' . formatoDia($expediente["fecha_creacion"], "fecha") . '</td></tr>';    
                                    
			}
		}	
	}
	return $ret;
}


if (isset($_REQUEST["cod_bar"])) {
	$id_expediente = (int)$_REQUEST["cod_bar"];
	$ret = '';
//	$ret .= '<center><table border="0" cellpadding="5" cellspacing="0" width="700">';
//	$ret .= '<tr height="18"><td align="center" class="AzulN" width="100">Expediente</td><td align="center" class="AzulN">Derechohabiente</td><td align="center" class="AzulN">Solicitud</td><td align="center" class="AzulN">Estado</td><td align="center" class="AzulN">Fecha</td></tr>';
$aExp = explode(',',$_REQUEST["expedientes"]);
	if (!in_array($id_expediente, $aExp)) {
	$ret .= getRenglon($id_expediente, $_REQUEST["expedientes"]);
	}
//	$ret .= '</table></center>';
	print($ret);
} else {
	print("error");
}

?>

