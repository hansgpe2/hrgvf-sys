<?php
error_reporting(NULL);
session_start ();
include_once('lib/misFunciones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Formato de Salida de Archivo</title>
<style type="text/css">
	@import url("lib/impresion.css") print;
	@import url("lib/reportes.css") screen;
</style>
</head>
</head>

<body onload="window.print();">

<?php

$id_solicitud = 0;
if (isset($_REQUEST["id_solicitud"])) $id_solicitud = $_REQUEST["id_solicitud"];
$resp = "";

$resp = verSolicitudExpon($id_solicitud, $_REQUEST["expedientes"]);
echo $resp;

?>
</body>
</html>
