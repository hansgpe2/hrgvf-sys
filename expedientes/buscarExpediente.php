<?php

include_once('lib/misFunciones.php');
$id_expediente = '';
$ret = '';

function getRenglon($id_expediente, $expedientes) {
    $estado = '';
    $ubicacion = '';
    $ret = '';
    $aExpedientes = explode(",", $expedientes);
    $expediente = getExpedienteXid($id_expediente);
    if (count($expediente) > 0) { // si existe el expediente
        array_unshift($aExpedientes, $id_expediente);
    }
    $aExpedientes = array_unique($aExpedientes);
    $tExp = count($aExpedientes);
    if ($tExp > 1) { //no esta vacio o tiene solo el que se está regresando
        $tExp2 = $tExp - 1;
        for ($i=0;$i<$tExp2;$i++) {
            $valor=$aExpedientes[$i];
            if (is_integer($valor)) {
                $estado = '';
                $ubicacion = '';
                $expediente = getExpedienteXid($valor);
                $solicitudes_detalles = getDetallesXidExp($valor);
                $tDetalles = count($solicitudes_detalles);
                if($tDetalles>0){
                    
                switch ($solicitudes_detalles[$i]["id_cita"]) {
                    case 0:
                        $solicitud=  getSolicitudXid($solicitudes_detalles[$i]['id_solicitud']);
                        $doctor = getMedicoXid($solicitud["id3"]);
                        $ubicacion = ' - de Ventanilla a ' . $doctor['titulo'] . ' ' . $doctor['ap_p'] . ' ' . $doctor['ap_m'] . ' ' . $doctor['nombres'];
                        break;
                    case -1:
                            $solicitud=getSolicitudXid($solicitudes_detalles[$i]['id_solicitud']);
                        break;
                    default :
                        $solicitud=getSolicitudXid($solicitudes_detalles[$i]['id_solicitud']);
                        $consultorio = getConsultorioXid($solicitud["id3"]);
                        $servicio = getServicioXid($solicitud["id4"]);
                        $medico = getMedicoXid($solicitud["id5"]);
                        $ubicacion = ' - ' . ponerAcentos($consultorio["nombre"]) . ' ' . ponerAcentos($servicio) . ' ' . ponerAcentos($medico["ap_p"] . ' ' . $medico["ap_m"] . ' ' . $medico["nombres"]);
                }
             }
            }
            switch ($expediente["status"]) {
                    case 0:
                        $estado = "cancelado";
                        break;
                    case 1:
                        $estado = "Presente en ARCHIVO";
                        break;
                    case 2:
                        $estado = "Fuera de archivo";
                        break;
                    case 3:
                        $estado = "concentrado";
                        break;
                }
            $derecho = getDatosDerecho($expediente["id_expediente"]);
            if (count($solicitudes_detalles) > 0)
                $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left"><input type="hidden" id="exp_' . $expediente["id_expediente"] . '_' . $solicitudes_detalles[0]["id_solicitud"] . '_' . $expediente["status"] . '" name="exp_' . $expediente["id_expediente"] . '_' . $solicitudes_detalles[$i]["id_solicitud"] . '_' . $expediente["status"] . '">' . ponerCeros($expediente["id_expediente"], 8) . '</td><td align="left">' . $derecho["cedula"] . '/' . $derecho["cedula_tipo"] . " - " . ponerAcentos($derecho["ap_p"]) . " " . ponerAcentos($derecho["ap_m"]) . " " . ponerAcentos($derecho["nombres"]) . " " . '</td><td align="left"><b>' . $estado . $ubicacion . '</b></td><td>' . formatoDia($solicitud["fecha_creacion"], "fecha") . '</td><td><input type="button" name="imp_' . $expediente["id_expediente"] . '" id="imp_' . $expediente["id_expediente"] . '" onclick="javascript: expedientesImprimir(\'' . $expediente["id_expediente"] . '\');" value="Imprimir Etiqueta"></td></tr>';
            else
                $ret .= '<tr class="renglonSolicitudes" height="24"><td align="left"><input type="hidden" id="exp_' . $expediente["id_expediente"] . '_-1_' . $expediente["status"] . '" name="exp_' . $expediente["id_expediente"] . '_-1_' . $expediente["status"] . '">' . ponerCeros($expediente["id_expediente"], 8) . '</td><td align="left">' . $derecho["cedula"] . '/' . $derecho["cedula_tipo"] . " - " . ponerAcentos($derecho["ap_p"]) . " " . ponerAcentos($derecho["ap_m"]) . " " . ponerAcentos($derecho["nombres"]) . " " . '</td><td align="left"><b>' . $estado . $ubicacion . '</b></td><td>' . formatoDia($expediente["fecha_creacion"], "fecha") . '</td><td><input type="button" name="imp_' . $expediente["id_expediente"] . '" id="imp_' . $expediente["id_expediente"] . '" onclick="javascript: expedientesImprimir(\'' . $expediente["id_expediente"] . '\');" value="Imprimir Etiqueta"></td></tr>';
        }
    }
    return $ret;
}

if (isset($_REQUEST["cod_bar"])) {
    $id_expediente = (int) $_REQUEST["cod_bar"];
    $ret .= '<center><table border="0" cellpadding="5" cellspacing="0" width="700">';
    $ret .= '<tr height="18"><td align="center" class="AzulN" width="100">Expediente</td><td align="center" class="AzulN">Derechohabiente</td><td align="center" class="AzulN">Estado</td><td align="center" class="AzulN" width="70">Fecha</td><td align="center" class="AzulN">&nbsp;</td></tr>';
    $ret .= getRenglon($id_expediente, $_REQUEST["expedientes"]);
    $ret .= '</table></center>';
    print($ret);
} else {
    print("error");
}
?>

