<?php
session_start();
include_once('lib/misFunciones.php');
function botonesCitasE($id_citaE, $tipo_cita) {
	$btns = '';
	switch ($_SESSION['tipoUsuario']) {
		case '1':
			$btns = "<a href=\"javascript: eliminarCitaEXdia('" . $id_citaE . "');\" title=\"Eliminar Cita Extemporanea\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";
			break;
		case '3':
			$btns = '';
			break;
		case '5':
			if (($tipo_cita == '0') || ($tipo_cita == '4')) {
				$btns = "<a href=\"javascript: eliminarCitaEXdia('" . $id_citaE . "');\" title=\"Eliminar Cita Extemporanea\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";
			}
			break;
		case '6':
			if (($tipo_cita == '1') || ($tipo_cita == '2')) {
				$btns = "<a href=\"javascript: eliminarCitaEXdia('" . $id_citaE . "');\" title=\"Eliminar Cita Extemporanea\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";
			}
			break;
		case '7':
			if ($tipo_cita == '3') {
				$btns = "<a href=\"javascript: eliminarCitaEXdia('" . $id_citaE . "');\" title=\"Eliminar Cita Extemporanea\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";
			}
			break;
		case '8':
			if ($tipo_cita == '2') {
				$btns = "<a href=\"javascript: eliminarCitaEXdia('" . $id_citaE . "');\" title=\"Eliminar Cita Extemporanea\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";
			}
			break;
	}
	return $btns;
}

function botonesCitas($fecha, $id_horario, $hora_inicio, $hora_fin, $id_cita, $tipo_cita) {
	$btns = array('boton1' => '', 'boton2' => '', 'boton3' => '');
	switch ($_SESSION['tipoUsuario']) {
		case '1':
			$btns['boton1'] = "<a href=\"javascript: agregarCita('" . $id_horario . "','" . $fecha . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Agregar Cita\"><img src=\"diseno/Symbol-Add.png\" border=\"0\"></a>";
			$btns['boton2'] = "<a href=\"javascript: eliminarCitaXdia('" . $id_cita . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Eliminar Cita\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";
			break;
		case '3':
			$btns = '';
			break;
		case '5':
			if (($tipo_cita == '0') || ($tipo_cita == '4')) {
				$btns['boton1'] = "<a href=\"javascript: agregarCita('" . $id_horario . "','" . $fecha . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Agregar Cita\"><img src=\"diseno/Symbol-Add.png\" border=\"0\"></a>";
				$btns['boton2'] = "<a href=\"javascript: eliminarCitaXdia('" . $id_cita . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Eliminar Cita\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";					
			}
			break;
		case '6':
			if (($tipo_cita == '1') || ($tipo_cita == '2')) {
				$btns['boton1'] = "<a href=\"javascript: agregarCita('" . $id_horario . "','" . $fecha . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Agregar Cita\"><img src=\"diseno/Symbol-Add.png\" border=\"0\"></a>";
				$btns['boton2'] = "<a href=\"javascript: eliminarCitaXdia('" . $id_cita . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Eliminar Cita\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";					
			}
			break;
		case '7':
			if ($tipo_cita == '3') {
				$btns['boton1'] = "<a href=\"javascript: agregarCita('" . $id_horario . "','" . $fecha . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Agregar Cita\"><img src=\"diseno/Symbol-Add.png\" border=\"0\"></a>";
				$btns['boton2'] = "<a href=\"javascript: eliminarCitaXdia('" . $id_cita . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Eliminar Cita\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";					
			}
			break;
		case '8':
			if ($tipo_cita == '2') {
				$btns['boton1'] = "<a href=\"javascript: agregarCita('" . $id_horario . "','" . $fecha . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Agregar Cita\"><img src=\"diseno/Symbol-Add.png\" border=\"0\"></a>";
				$btns['boton2'] = "<a href=\"javascript: eliminarCitaXdia('" . $id_cita . "','" . formatoHora($hora_inicio) . "','" . formatoHora($hora_fin) . "');\" title=\"Eliminar Cita\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";					
			}
			break;
	}
	return $btns;
}

function botonesExtempo($fecha) {
	$btns = '';
	switch ($_SESSION['tipoUsuario']) {
		case '1':
			$btns = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href=\"javascript:citaExtemporanea(" . $fecha . ");\" title=\"Agregar Cita Extempor&aacute;nea\" class=\"textoCitaExtemporanea\">Agregar Cita Extempor&aacute;nea <img src=\"diseno/Symbol-Add.png\" alt=\"Agregar Cita Extempor&aacute;nea\" border=\"0\" /></a>";
			break;
		case '3':
			$btns = '';
			break;
		case '5':
		case '6':
		case '7':
		case '8':
			$btns = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href=\"javascript:citaExtemporanea(" . $fecha . ");\" title=\"Agregar Cita Extempor&aacute;nea\" class=\"textoCitaExtemporanea\">Agregar Cita Extempor&aacute;nea <img src=\"diseno/Symbol-Add.png\" alt=\"Agregar Cita Extempor&aacute;nea\" border=\"0\" /></a>";
			break;
	}
	return $btns;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Untitled Document</title>
    </head>

    <body>
        <center>
            <table width="630" border="0" cellspacing="0" cellpadding="0" class="ventana">
                <tr>
                    <td class="tituloVentana" height="23"><?php echo formatoDia($_GET['getdate'], 'tituloCitasXdia'); ?></td>
                </tr>
                <tr>
                    <td align="center"><br /><strong></strong>

                        <?php
                        $extempo = "";
                        $out = "";
                        $out.= "
<table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
                        $horario = citasXdia($_GET['getdate'], $_SESSION['idUsuario'], $_SESSION['IdCon'], $_SESSION['idServ'], $_SESSION['idDr']);
                        $citasDelDia = count($horario);
                        if ($citasDelDia > 0) {
							$extempo = botonesExtempo($_GET['getdate']);
                            for ($i = 0; $i < $citasDelDia; $i++) {
                                $datosCita = citasOcupadasXdia($horario[$i]['id_horario'], $_GET['getdate']);
                                $datosDerecho = getDatosDerecho($datosCita['id_derecho']);
                                if ($datosCita['id_derecho'] == "-1") { //no hay cita a x hora
                                    if ($horario[$i]['tipo_cita'] == "0")
                                        $claseParaDia = "citaXdiaLIBPRV"; //primera vez libre
                                    if ($horario[$i]['tipo_cita'] == "1")
                                        $claseParaDia = "citaXdiaLIBSUB"; //SUBSECUENTE
                                    if ($horario[$i]['tipo_cita'] == "2")
                                        $claseParaDia = "citaXdiaLIBPRO"; //PROCEDIMIENTO
									if ($horario[$i]['tipo_cita'] == "4")
                                        $claseParaDia = "citaXdiaLIBTELE"; //TELEMEDICINA
                                    $nombre = "";
                                    $datos = "";
									$btns = botonesCitas($_GET['getdate'], $horario[$i]['id_horario'], $horario[$i]['hora_inicio'], $horario[$i]['hora_fin'], $datosCita['id_cita'], $horario[$i]['tipo_cita']);
									$botones = $btns['boton1'];
                                } else { // hay cita
                                    
                                    
                                    

                                    $botones = "";
                                    $nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
                                   switch($datosCita['tipo_usuario'])
								   {
                                        case 2:
										$datosCitaMedico = citaMedico($datosCita['id_cita']);
										$datosUsuario = getMedicoXid($datosCitaMedico['id_medico']);
										$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . ponerAcentos($datosUsuario['titulo']." ".$datosUsuario['ap_p']." ".$datosUsuario['ap_m']." ".$datosUsuario['nombres']) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
                                    break;
									case 3:
									$datosUsuario=getUsuarioReferencia($datosCita['id_usuario']);
									$sql="select * from citas_apartadas where id_cita=".$datosCita['id_cita'];
					$datosApartada=ejecutarConsulta($sql);
					$diagnostico=strtok($datosApartada['diagnostico'],"-");
									 $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones']." - Dx= ".$diagnostico;
									 $botones="";
									break;
									default:
									$datosUsuario = getUsuarioXid($datosCita['id_usuario']);
										if (!isset($datosUsuario['nombre'])) $datosUsuario['nombre'] = '';
                                        $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
								   }
//				$botones = "<a href=\"javascript: modificarCita('" . $datosCita['id_cita'] .  "','" . $_GET['getdate'] . "','" . formatoHora($horario[$i]['hora_inicio']) . "','" . formatoHora($horario[$i]['hora_fin']) . "','" . $datosCita['id_derecho'] . "','" . $datosDerecho['cedula'] . "','" . $datosDerecho['cedula_tipo'] . "','" . $datosDerecho['telefono'] . "','" . $datosDerecho['direccion'] . "','" . strtoupper($datosDerecho['ap_p'] . "','" . $datosDerecho['ap_m'] . "','" . $datosDerecho['nombres'])  . "','" . $datosCita['observaciones'] . "','" . $datosDerecho['estado'] . "','" . $datosDerecho['municipio'] . "');\" title=\"Modificar Cita\"><img src=\"diseno/Symbol-Refresh.png\" border=\"0\"></a>";
//				if ($_SESSION['tipoUsuario'] <> "1") { // eliminar cita solo para jefatura y administradores, no para capturistas
//					$botones.= "<a href=\"javascript: eliminarCitaXdia('" . $datosCita['id_cita'] . "','" . $_GET['getdate'] . "','Realmente desea eliminar la cita de " . formatoHora($horario[$i]['hora_inicio']) . " - " . formatoHora($horario[$i]['hora_fin']) . " a nombre de " . $nombre . " ?');\" title=\"Eliminar Cita\"><img src=\"diseno/Symbol-Delete.png\" border=\"0\"></a>";
									$btns = botonesCitas($_GET['getdate'], $horario[$i]['id_horario'], $horario[$i]['hora_inicio'], $horario[$i]['hora_fin'], $datosCita['id_cita'], $horario[$i]['tipo_cita']);
									$botones = $btns['boton2'];
									if($datosCita['tipo_usuario']==3)
									{
										$citaClin=getCitaClinica($datosCita['id_cita']);
									if($citaClin['aceptada']=="-1")
									$botones="";
									}
//					}
                                    if ($horario[$i]['tipo_cita'] == 0)
                                        $claseParaDia = "citaXdiaPRV";
                                    if ($horario[$i]['tipo_cita'] == 1)
                                        $claseParaDia = "citaXdiaSUB";
                                    if ($horario[$i]['tipo_cita'] == 2)
                                        $claseParaDia = "citaXdiaPRO";
									if ($horario[$i]['tipo_cita'] == 4)
                                        $claseParaDia = "citaXdiaTELE";
                                }
                                if (($datosCita['status'] > 2) && ($datosCita['status'] < 9)) { // esta de vacaciones, en congreso, etc.
                                    $nombre = $statusCitas[$datosCita['status']];
                                    $datos = $datosCita['observaciones'];
                                    $botones = "";
                                    $claseParaDia = "citaXdiaNOT";
                                }
                                $hoy = date('Ymd');
                                $fecha = $_GET['getdate'];
                                if ($fecha < $hoy) {
                                    $botones = ""; // si es una fecha anterior a hoy no pone botones
                                    $extempo = "";
                                }
								if($_SESSION["tipoUsuario"]==6) {
								 if ($fecha <= $hoy) {
                                    $botones = ""; // si es una fecha anterior a hoy no pone botones
                                    $extempo = "";
                                }
								}
                                $out.="
  <tr>
    <td class=\"" . $claseParaDia . "\">
        <table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td width=\"80\" class=\"citaXdiaHora\">" . formatoHora($horario[$i]['hora_inicio']) . " - " . formatoHora($horario[$i]['hora_fin']) . "</td>
            <td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
			<td width=\"80\" rowspan=\"2\">" . $botones . "</td>
          </tr>
          <tr>
            <td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\" >" . ponerAcentos($datos) . "</td>
          </tr>
        </table>
    </td>
  </tr>";
                            }
                        } else {
							$extempo = botonesExtempo($_GET['getdate']);
                            $hoy = date('Ymd');
                            $fecha = $_GET['getdate'];
                            if ($fecha < $hoy) {
                                $extempo = ""; // si es una fecha anterior no ponemos boton de citas extemporaneas
                            }
                            echo '<span class="textosParaInputs">NO HAY CITAS DISPONIBLES PARA ESTE DIA<br><br></span>';
                        }
                        $out.= "
</table>";
                        echo $out;
                        ?>
                        <a href="javascript:cambiarMes(<?php echo $_GET['getdate'] ?>);" title="Regresar a la agenda mensual"><img src="diseno/flechaIzq.png" alt="Regresar" border="0" /></a>
                        <?php echo $extempo; ?>
                        <br /><div id="estadoEliminando"></div></td>
                </tr>
            </table>
            <p align="center"><table width="500" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">
                    <tr>
                        <td width="35" height="25" class="citaXdiaPRV"></td><td width="129">Citas Primera Vez</td>
                        <td width="35" class="citaXdiaSUB"></td><td width="138">Citas Subsecuentes</td>
                        <td width="35" class="citaXdiaPRO"></td><td width="138">Citas Procedimientos</td>
                        <td width="35" class="citaXdiaTELE"></td><td width="138">Citas Telemedicina</td>
                    </tr>
                </table></p>

            <?php
            $citasExtemporaneas = getCitasExtemporaneas($_GET['getdate'], $_SESSION['idUsuario'], $_SESSION['IdCon'], $_SESSION['idServ'], $_SESSION['idDr']);
            $out = "<br>";
            $out.= "
<table width=\"630\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
<tr>
  <td class=\"tituloVentana\" height=\"23\">" . formatoDia($_GET['getdate'], 'tituloCitasXdia') . " - EXTEMPORANEAS</td>
</tr>
";
            $totalCitas = count($citasExtemporaneas);
            for ($i = 0; $i < $totalCitas; $i++) {
                if ($citasExtemporaneas[$i]['tipo_cita'] == "0")
                    $claseParaDia = "citaXdiaLIBPRV"; //primera vez libre
                if ($citasExtemporaneas[$i]['tipo_cita'] == "1")
                    $claseParaDia = "citaXdiaLIBSUB"; //primera vez libre
                if ($citasExtemporaneas[$i]['tipo_cita'] == "2")
                    $claseParaDia = "citaXdiaLIBPRO"; //primera vez libre
				if ($citasExtemporaneas[$i]['tipo_cita'] == "4")
                    $claseParaDia = "citaXdiaLIBTELE"; //primera vez libre
                $datosDerecho = getDatosDerecho($citasExtemporaneas[$i]['id_derecho']);
                $datosUsuario = getUsuarioXid($citasExtemporaneas[$i]['id_usuario']);
                $nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
				if (!isset($datosUsuario['nombre'])) $datosUsuario['nombre'] = '';
                $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $citasExtemporaneas[$i]['extra1'] . " - " . $citasExtemporaneas[$i]['observaciones'];
                $hoy = date('Ymd');
                $fecha = $_GET['getdate'];
				$botones = botonesCitasE($citasExtemporaneas[$i]['id_cita'], $citasExtemporaneas[$i]['tipo_cita']);
                if ($fecha < $hoy) {
                    $botones = ""; // si es una fecha anterior a hoy no pone botones
                }
                $out.="
		  <tr>
			<td class=\"" . $claseParaDia . "\">
				<table width=\"630\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td width=\"80\" class=\"citaXdiaHora\">" . formatoHora($citasExtemporaneas[$i]['hora_inicio']) . " - " . formatoHora($citasExtemporaneas[$i]['hora_fin']) . "</td>
					<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
					<td width=\"80\" rowspan=\"2\">" . $botones . "</td>
				  </tr>
				  <tr>
					<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\" >" . ponerAcentos($datos) . "</td>
				  </tr>
				</table>
			</td>
		  </tr>";
            }
            $out .= "</table>";
            if ($totalCitas > 0)
                echo $out;
            ?>

        </center>
    </body>
</html>
