<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<?php
$datosCita = getCitaExtemporaneaDatos($_GET['id_cita']);
$datosDH = getDatosDerecho($datosCita['id_derecho']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaEliminarCita" method="POST" action="javascript: eliminarCitaEXdiaConfirmar('<?php echo $_GET['id_cita'] ?>','<?php echo $datosCita['fecha_cita'] ?>','Realmente desea eliminar la cita extemporanea de <?php echo formatoHora($datosCita['hora_inicio']) . " - " . formatoHora($datosCita['hora_fin']) . " a nombre de " . $datosDH['ap_p']. " " . $datosDH['ap_m'] . " " . $datosDH['nombres']; ?> ?');">
<input name="id_derecho" id="id_derecho" type="hidden" value="<?php echo $datosCita['id_derecho']; ?>" />
<input name="id_cita" id="id_cita" type="hidden" value="<?php echo $_GET['id_cita']; ?>" />
<input name="fechaCita" id="fechaCita" type="hidden" value="<?php echo $datosCita['fecha_cita'] ?>" />

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CANCELAR CITA EXTEMPORANEA EL <?php echo formatoDia($datosCita['fecha_cita'], 'tituloCitasXdia'); ?> DE <?php echo formatoHora($datosCita['hora_inicio']) ?> A <?php echo formatoHora($datosCita['hora_fin']) ?> HORAS</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><?php echo $datosDH['cedula'] . "/" . $datosDH['cedula_tipo']; ?>
    </td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><?php echo ponerAcentos($datosDH['ap_p']. " " . $datosDH['ap_m'] . " " . $datosDH['nombres']); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">FECHA NACIMIENTO:</td>
    <td align="left"><?php echo $datosDH['fecha_nacimiento']; ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><?php echo $datosDH['telefono']; ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><?php echo ponerAcentos($datosDH['direccion']); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><?php echo ponerAcentos($datosDH['estado']); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MUNICIPIO:</td>
    <td align="left"><?php echo ponerAcentos($datosDH['municipio']); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MOTIVO DE CANCELACION:</td>
    <td align="left"><select name="motivo" id="motivo">
      <option value="PACIENTE" selected="selected">PACIENTE</option>
      <option value="MEDICO">MEDICO</option>
      <option value="AUTORIDAD">AUTORIDAD</option>
      <option value="OTRO">OTRO</option>
    </select>
    </td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">DESCRIPCION DEL MOTIVO:</td>
    <td align="left"><input name="desc" type="text" id="desc" size="40" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">NOMBRE DE QUIEN AUTORIZO CANCELACION:</td>
    <td align="left"><input name="nombreAutorizo" type="text" id="nombreAutorizo" size="40" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="selDia('<?php echo $datosCita['fecha_cita']; ?>','<?php echo $datosCita['fecha_cita']; ?>');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Eliminar Cita" class="botones" />
  <br /><br /><div id="estadoEliminando"></div></td>
  </tr>
</table>
</form>

</body>
</html>
