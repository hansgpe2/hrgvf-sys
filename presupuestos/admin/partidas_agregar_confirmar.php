<?php
session_start ();
require_once("../lib/funcionesAdmin.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ISSSTE - Administraci&oacute;n - Sistema de Registro Auxiliar</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<?php if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if ((isset($_POST['partida'])) && (isset($_POST['presupuesto_asignado']))) {
		$partida = getPartidaCatalogo($_POST['partida']);
		$aPartida = explode("-",$partida['partida']);
		$duplica = existeDuplica("SELECT * FROM partidas WHERE nombre='" . $partida['nombre'] . "' AND partida='" . $aPartida[0] . "' AND subpartida='" . $aPartida[1] . "' AND subsubpartida='" . $aPartida[2] . "'");
		if(!$duplica) {
			$query_query = "INSERT INTO logs values('" . date("Ymd") . "','" . date("Hi") . "','" . $_SESSION['idUsuario'] . "','" . getIps() . "','agrego partida " . $_POST['partida'] . "')";
			$log = ejecutarSQL($query_query);
			$query = "INSERT INTO partidas VALUES(NULL,'','" . $aPartida[0] . "','" . $aPartida[1] . "','" . $aPartida[2] . "','" . $partida['nombre'] . "','" . $partida['proyecto'] . "','" . $_POST['presupuesto_asignado'] . "','0')";
			$res = ejecutarSQL($query);
			if ($res[0] == 0) {// no hay error
				$id_partida = traerUltimo("SELECT * FROM partidas WHERE partida='" . $aPartida[0] . "' AND subpartida='" . $aPartida[1] . "' AND subsubpartida='" . $aPartida[2] . "' AND nombre='" . $partida['nombre'] . "' LIMIT 1","id_partida");
				for ($i=1; $i <= 12; $i++) {
					$query = "INSERT INTO partidas_x_mes VALUES('" . $id_partida . "','" . $i . "','" . $_POST[$mes[$i]] . "','0','" .$_POST[$mes[$i]] . "','" . $_POST[$mes[$i]] . "','')";
					$res = ejecutarSQL($query);
				}
			} else { // hay error en el mysql
				echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de agregar la partida'); history.back();</script>";
			}
		} else {
			echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Actualmente existe una partida llamada " . $partida['nombre'] . ". Por favor introduzca un nombre diferente'); history.back();</script>";
		}
	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en  variables recibidas'); history.back();</script>";
	}

?>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Sistema de Registro Auxiliar&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="consultorios_agregar_confirmar.php" onsubmit="MM_validateForm('nombre','','R');return document.MM_returnValue" method="post" name="forma">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td>Agregar Partida</td>
          </tr>
		  <tr><td align="center"><br>
		      <span class="error">La Partida "<b><?php echo $partida['nombre']; ?></b>" se agreg&oacute; correctamente</span><br>
		  <br>
		      <input name="continuar" id="continuar" type="button" value="Continuar" class="botones" onclick="javascript: location.replace('partidas.php');" />
		      &nbsp;&nbsp;</td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } 
?>