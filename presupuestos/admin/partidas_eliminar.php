<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if (isset($_GET['id_partida'])) {
		$res = getPartidaXid($_GET['id_partida']);
		$meses = getPartidaXmesXid($_GET['id_partida']);
	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No existe la variable id del piso'); history.back();</script>";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ISSSTE - Administración - Sistema de Registro Auxiliar</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Sistema de Registro Auxiliar&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form method="post" name="forma">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td colspan="2">Modificar Partida</td>
          </tr>
		  <tr>
		    <td align="right">N. de partida:</td><td><input name="partida" type="text" size="15" maxlength="4" id="partida" value="<?php echo $res['partida']; ?>" disabled="disabled" /></td></tr>
		  <tr>
		    <td align="right">N. de subpartida:</td><td><input name="subpartida" type="text" disabled="disabled" id="subpartida" value="<?php echo $res['subpartida']; ?>" size="15" maxlength="3" /></td></tr>
		  <tr>
		    <td align="right">N. de subsubpartida:</td>
		    <td><select name="subsubpartida" id="subsubpartida" disabled="disabled"><option selected="selected" value="<?php echo $res['subsubpartida']; ?>"><?php echo $res['subsubpartida']; ?><option value=""></select>
  		    </td>
		  </tr>
		  <tr>
		    <td align="right">Nombre:</td>
		    <td><input name="nombre" type="text" disabled="disabled" id="nombre" value="<?php echo $res['nombre']; ?>" size="35" maxlength="50" /></td></tr>
		  <tr>
		    <td align="right">Proyecto:</td>
		    <td><input name="proyecto" type="text" disabled="disabled" id="proyecto" value="<?php echo $res['proyecto']; ?>" size="10" maxlength="10" /></td></tr>
		  <tr>
		    <td align="right">Presupuesto Anual Asignado: $</td>
		    <td><input name="presupuesto_asignado" type="text" disabled="disabled" id="presupuesto_asignado" value="<?php echo $res['presupuesto_asignado']; ?>" size="10" maxlength="10" /> 
		      pesos</td></tr>
		  <tr>
		    <td align="right">Enero: $</td>
		    <td><input name="enero" type="text" disabled="disabled" id="enero" value="<?php echo $meses[0]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Febrero: $</td>
		    <td><input name="febrero" type="text" disabled="disabled" id="febrero" value="<?php echo $meses[1]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Marzo: $</td>
		    <td><input name="marzo" type="text" disabled="disabled" id="marzo" value="<?php echo $meses[2]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Abril: $</td>
		    <td><input name="abril" type="text" disabled="disabled" id="abril" value="<?php echo $meses[3]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Mayo: $</td>
		    <td><input name="mayo" type="text" disabled="disabled" id="mayo" value="<?php echo $meses[4]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Junio: $</td>
		    <td><input name="junio" type="text" disabled="disabled" id="junio" value="<?php echo $meses[5]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Julio: $</td>
		    <td><input name="julio" type="text" disabled="disabled" id="julio" value="<?php echo $meses[6]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Agosto: $</td>
		    <td><input name="agosto" type="text" disabled="disabled" id="agosto" value="<?php echo $meses[7]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Septiembre: $</td>
		    <td><input name="septiembre" type="text" disabled="disabled" id="septiembre" value="<?php echo $meses[8]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Octubre: $</td>
		    <td><input name="octubre" type="text" disabled="disabled" id="octubre" value="<?php echo $meses[9]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Noviembre: $</td>
		    <td><input name="noviembre" type="text" disabled="disabled" id="noviembre" value="<?php echo $meses[10]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Diciembre: $</td>
		    <td><input name="diciembre" type="text" disabled="disabled" id="diciembre" value="<?php echo $meses[11]['presupuesto_asignado']; ?>" size="10" maxlength="10" /> pesos</td>
          </tr>
		  <tr><td align="center" colspan="2"><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="button" class="botones" id="agregar" value="Eliminar Partida" onclick="location.href = 'partidas_eliminar_confirmar.php?id_partida=<?php echo $_GET["id_partida"] ?>';" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>