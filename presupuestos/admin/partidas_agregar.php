<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	$opcionesPartidas = getOpcionesPartidas("");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ISSSTE - Administración - Sistema de Registro Auxiliar</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Sistema de Registro Auxiliar&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="partidas_agregar_confirmar.php" onsubmit="return validarAgregarPartida()" method="post" name="forma">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td colspan="2">Agregar Partida</td>
          </tr>
		  <tr>
		    <td align="right">Seleccinar partida:</td><td><select name="partida" id="partida"><?php echo $opcionesPartidas; ?></select></td></tr>
		  <tr>
		    <td align="right">Presupuesto Anual Asignado: $</td>
		    <td><input name="presupuesto_asignado" type="text" size="10" maxlength="10" id="presupuesto_asignado" /> 
		      pesos</td></tr>
		  <tr>
		    <td align="right">Enero: $</td>
		    <td><input name="enero" type="text" size="10" maxlength="10" id="enero" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Febrero: $</td>
		    <td><input name="febrero" type="text" size="10" maxlength="10" id="febrero" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Marzo: $</td>
		    <td><input name="marzo" type="text" size="10" maxlength="10" id="marzo" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Abril: $</td>
		    <td><input name="abril" type="text" size="10" maxlength="10" id="abril" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Mayo: $</td>
		    <td><input name="mayo" type="text" size="10" maxlength="10" id="mayo" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Junio: $</td>
		    <td><input name="junio" type="text" size="10" maxlength="10" id="junio" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Julio: $</td>
		    <td><input name="julio" type="text" size="10" maxlength="10" id="julio" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Agosto: $</td>
		    <td><input name="agosto" type="text" size="10" maxlength="10" id="agosto" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Septiembre: $</td>
		    <td><input name="septiembre" type="text" size="10" maxlength="10" id="septiembre" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Octubre: $</td>
		    <td><input name="octubre" type="text" size="10" maxlength="10" id="octubre" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Noviembre: $</td>
		    <td><input name="noviembre" type="text" size="10" maxlength="10" id="noviembre" /> pesos</td>
          </tr>
		  <tr>
		    <td align="right">Diciembre: $</td>
		    <td><input name="diciembre" type="text" size="10" maxlength="10" id="diciembre" /> pesos</td>
          </tr>
		  <tr><td align="center" colspan="2"><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" value="Agregar Partida" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>