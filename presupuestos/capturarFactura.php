<?php session_start ();
include_once('lib/misFunciones.php');
$contrato = getContratoXid($_GET['id_contrato']);
$proveedor = getProveedorXid($contrato['id_proveedor']);
$partidas = getPartidasXcontrato($contrato['id_contrato']);
$tPartidas = count($partidas);
$datosPartidas = "";
$ids_partidas = "";
for($i=0; $i<$tPartidas; $i++) {
	$partida = getPartidaXid($partidas[$i]['id_partida']);
	$datosPartidas .= "<tr><td class=\"conteoCitasVerde\">" . $partida['partida'] . "-" .$partida['subpartida'] . "-" . $partida['subsubpartida'] . "</td><td class=\"conteoCitasVerde\">" . $partida['nombre'] . "</td><td class=\"conteoCitasVerde\" align=\"right\">$" . number_format($partidas[$i]['monto_x_ejercer'],2,".",",") . "</td><td><input name=\"saldo_" . $partidas[$i]['id_partida'] . "\" type=\"hidden\" id=\"saldo_" . $partidas[$i]['id_partida'] . "\" value=\"" . $partidas[$i]['monto_x_ejercer'] . "\" /><input name=\"afectar_" . $partidas[$i]['id_partida'] . "\" id=\"afectar_" . $partidas[$i]['id_partida'] . "\" type=\"text\" size=\"10\" maxlenght=\"10\" onkeyup=\"javascript: saldosKeyUp(event, this);\" value=\"0\" /></td></tr>";
	$ids_partidas .= $partidas[$i]['id_partida'] . "|";
}
$ids_partidas = substr($ids_partidas,0,strlen($ids_partidas)-1);

$conceptos = getConceptosContrato($contrato['id_contrato'], "actuales");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarCapturarFactura();">
<input name="id_contrato" type="hidden" id="id_contrato" value="<?php echo $_GET['id_contrato'] ?>" />
<input name="id_proveedor" type="hidden" id="id_proveedor" value="<?php echo $contrato['id_proveedor'] ?>" />
<input name="ids_partidas" type="hidden" id="ids_partidas" value="<?php echo $ids_partidas ?>" />
<input name="montoXejercerCto" type="hidden" id="montoXejercerCto" value="<?php echo $contrato['monto_x_ejercer'] ?>" />
<input name="vTotal" type="hidden" id="vTotal" value="0" />
<input name="vReten" type="hidden" id="vReten" value="0" />
<input name="vSubto" type="hidden" id="vSubto" value="0" />
<input name="totalAntesRetenciones" type="hidden" id="totalAntesRetenciones" value="0" />

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td class="tituloVentana" colspan="5">FACTURA</td>
  </tr>
  <tr>
  	<td colspan="3">&nbsp;</td>
  	<td class="nombrePaciente" align="right">FACTURA <input name="factura" id="factura" type="text" size="10" maxlength="10" /></td>
  </tr>
  <tr>
  	<td colspan="2">&nbsp;</td>
  	<td colspan="2" class="nombrePaciente" align="right">FECHA EN FACTURA 
  	  <input name="fecha" id="fecha" type="text" size="10" maxlength="10" value="<?php echo date("d/m/Y"); ?>" readonly="readonly" /><a onclick="displayCalendar(document.forms[0].fecha,'dd/mm/yyyy',this,false)" title="Click para seleccionar la fecha de elaboraci&oacute;n de la factura" href="javascript: vacia();"><img src="diseno/calendario.png" border="0" /></a></td>
  </tr>
  <tr>
  	<td colspan="4" class="nombrePaciente" align="right">FECHA DE INICIO (COMPROMISO) <input name="fechaInicioCom" id="fechaInicioCom" type="text" size="10" maxlength="10" value="<?php echo formatoDia($contrato['fecha_inicio'],"fechaI"); ?>" readonly="readonly" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA DE TERMINO (COMPROMISO) <input name="fechaTerminoCom" id="fechaTerminoCom" type="text" size="10" maxlength="10" value="<?php echo formatoDia($contrato['fecha_terminacion'],"fechaI"); ?>" readonly="readonly" /></td>
  </tr>
  <tr>
  	<td colspan="4" class="nombrePaciente" align="right">FECHA DE INICIO (REAL) <input name="fechaInicioReal" id="fechaInicioReal" type="text" size="10" maxlength="10" value="" readonly="readonly" /><a onclick="displayCalendar(document.forms[0].fechaInicioReal,'dd/mm/yyyy',this,false)" title="Click para seleccionar la fecha de inicio real" href="javascript: vacia();"><img src="diseno/calendario.png" border="0" /></a>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA DE TERMINO (REAL) <input name="fechaTerminoReal" id="fechaTerminoReal" type="text" size="10" maxlength="10" value="" readonly="readonly" /><a onclick="displayCalendar(document.forms[0].fechaTerminoReal,'dd/mm/yyyy',this,false)" title="Click para seleccionar la fecha de termino real" href="javascript: vacia();"><img src="diseno/calendario.png" border="0" /></a></td>
  </tr>
  <tr>
  	<td colspan="4" class="nombrePaciente" align="right">DIAS DE RETRASO <input name="diasRetraso" id="diasRetraso" type="text" size="10" maxlength="10" value="0" /></td>
  </tr>
  <tr>
  	<td class="nombrePaciente" width="80">CLIENTE</td><td><?php echo $proveedor['razon_social']; ?></td><td class="nombrePaciente" width="80">R.F.C.</td><td width="150"><?php echo $proveedor['rfc']; ?></td>
  </tr>
  <tr>
  	<td class="nombrePaciente" width="80">DOMICILIO</td><td><?php echo $proveedor['direccion']; ?></td><td class="nombrePaciente" width="80">TELEFONO</td><td width="150"><?php echo $proveedor['telefono']; ?></td>
  </tr>
  <tr>
  	<td class="nombrePaciente" width="80">CIUDAD</td><td><?php echo $proveedor['ciudad'] . ", " . $proveedor['estado']; ?></td><td class="nombrePaciente" width="80">C.P.</td><td width="150"><?php echo $proveedor['cp']; ?></td>
  </tr>
  <tr>
    <td class="tituloVentana" colspan="5">&nbsp;</td>
  </tr>
  <tr>
  	<td class="nombrePaciente">CANTIDAD</td><td class="nombrePaciente">DESCRIPCION</td><td class="nombrePaciente">P. UNITARIO</td><td class="nombrePaciente" align="center">IMPORTE</td>
  </tr>
  <tr>
  	<td colspan="5">
<?php $tConceptos = count($conceptos);
	$j=1;
	$renglones = "";
	for ($i=0; $i<$tConceptos; $i++) {	
		$renglones .= "
    	<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">
          <tr>
            <td align=\"center\" valign=\"top\" width=\"80\"><input name=\"concepto" . $j . "\" type=\"hidden\" value=\"" . $conceptos[$i]['id_concepto'] . "\" id=\"concepto" . $j . "\" /><input name=\"cant" . $j . "\" type=\"text\" size=\"6\" maxlength=\"6\" onkeyup=\"javascript: calcularRenglon(event,this);\" id=\"cant" . $j . "\" /><span class=\"conteoCitasVerde\"><br>" . $conceptos[$i]['cantidad'] . " Disponibles</span></td><td align=\"left\" valign=\"top\"><input name=\"desc" . $j . "\" type=\"text\" size=\"65\" maxlength=\"200\" id=\"desc" . $j . "\" readonly=\"readonly\" value=\"" . $conceptos[$i]['descripcion'] . "\" /></td><td align=\"right\" valign=\"top\" width=\"80\"><input name=\"precio" . $j . "\" type=\"text\" size=\"11\" maxlength=\"8\" id=\"precio" . $j . "\" readonly=\"readonly\" value=\"" . $conceptos[$i]['precio_unitario'] . "\" /></td><td align=\"center\" valign=\"top\" width=\"150\"><input name=\"importe" . $j . "\" type=\"text\" size=\"11\" maxlength=\"11\" readonly=\"readonly\" value=\"0\" id=\"importe" . $j . "\" /></td>
          </tr>
        </table>";
		$j++;
	}
	echo $renglones;
?>

  <tr>
    <td colspan="3" align="right" class="nombrePaciente">SUBTOTAL</td><td align="right"><div id="subtotal">$ 0.00</div></td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">IVA</td><td align="right"><div id="divIva">$ 0.00</div></td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">TOTAL</td><td align="right"><div id="total">$ 0.00</div></td>
  </tr>
  <tr>
    <td colspan="4" align="left" class="conteoCitasVerde"><span class="nombrePaciente">RETENCIONES:</span><br /><input name="7601-116-00" id="7601-116-00" type="checkbox" value="7601-116-00" onblur="javascript: calcularTotales();" onclick="javascript: calcularTotales();" onkeypress="javascript: calcularTotales();" onchange="javascript: calcularTotales();" /> Honorarios(10%) <input name="7601-118-00" id="7601-118-00" type="checkbox" value="7601-118-00" onblur="javascript: calcularTotales();" onclick="javascript: calcularTotales();" onkeypress="javascript: calcularTotales();"  onchange="javascript: calcularTotales();" /> Construcci&oacute;n sfp(0.5%) <input name="7601-128-00" id="7601-128-00" type="checkbox" value="7601-128-00" onblur="javascript: calcularTotales();" onclick="javascript: calcularTotales();" onkeypress="javascript: calcularTotales();"  onchange="javascript: calcularTotales();" /> Personas F&iacute;sicas Act. Emp.(16%) <input name="7601-130-00" id="7601-130-00" type="checkbox" value="7601-130-00" onblur="javascript: calcularTotales();" onclick="javascript: calcularTotales();" onkeypress="javascript: calcularTotales();"  onchange="javascript: calcularTotales();" /> Fletes(4%) <input name="7601-126-00" id="7601-126-00" type="checkbox" value="7601-126-00" onblur="javascript: calcularTotales();" onclick="javascript: calcularTotales();" onkeypress="javascript: calcularTotales();"  onchange="javascript: calcularTotales();" /> Personas F&iacute;sicas Indep.(10%) <br /><input name="7601-124-00" id="7601-124-00" type="checkbox" value="7601-124-00" onblur="javascript: calcularTotales();" onclick="javascript: calcularTotales();" onkeypress="javascript: calcularTotales();"  onchange="javascript: calcularTotales();" /> 
    Sanci&oacute;n por Incumplimiento de contrato(2.5% diarios) <span class="nombrePaciente">
    <input name="sancion" type="text" id="sancion" size="6" maxlength="5" value="2.5" onkeyup="javascript: calcularSancion(event, this)" />
    </span></td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">RETENCION</td><td align="right"><div id="retencion">$ 0.00</div></td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">TOTAL</td><td align="right"><div id="granTotal">$ 0.00</div></td>
  </tr>
  <tr>
    <td class="tituloVentana" colspan="5">PARTIDA(S) AFECTADA(S)</td>
  </tr>
  <tr>
  	<td class="nombrePaciente">PARTIDA</td><td class="nombrePaciente">DESCRIPCION</td><td class="nombrePaciente" align="center">SALDO</td><td class="nombrePaciente" align="left">CARGAR</td>
  </tr>
<?php echo $datosPartidas ?>
  <tr>
    <td colspan="5" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicioGen.php');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Capturar Factura" class="botones" />
  <br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>

</body>
</html>
