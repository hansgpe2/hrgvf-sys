<?php include_once('lib/misFunciones.php');
require('fpdf/fpdf.php');

class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{
		//Logo
		$this->Image('diseno/logoEncabezado.png',10,8,33);
		//Arial bold 15
		$this->SetFont('Arial','B',13);
		//Movernos a la derecha
		$this->Cell(30,6,'',0,1,'L');
		$this->Cell(60);
		$this->Cell(30,8,html_entity_decode('Subdirecci&oacute;n General de Obras y Mantenimiento'),0,1,'L');
		$this->Cell(60);
		$this->Cell(30,8,html_entity_decode('Subdirecci&oacute;n de Conservaci&oacute;n y Mantenimiento'),0,1,'L');
		$this->Cell(60);
		$this->Cell(30,8,html_entity_decode('Hospital Regional Dr. Valent&iacute;n G&oacute;mez Farias'),0,1,'L');
		$this->Cell(100);
		$this->Cell(30,18,html_entity_decode('ACTA DE ENTREGA RECEPCION'),0,1,'C');
	}
	
	//Pie de página
	function Footer()
	{
		//Posición: a 1,5 cm del final
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
//		$this->Cell(0,10,'Page '.$this->PageNo().'/ {nb}',0,0,'C');
	}

/*	function Tabla1($header,$data)
	{
		//Colores, ancho de línea y fuente en negrita
		$this->SetFillColor(0,0,0);
		$this->SetTextColor(255);
		$this->SetDrawColor(0,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('Arial','B',8);
		//Cabecera
		$w=array(7,13,15,17,19);
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],7,$header[$i],1,0,'C',1);
		$this->Ln();
		//Restauración de colores y fuentes
		$this->SetFillColor(255,255,255);
		$this->SetTextColor(0);
		$this->SetFont('Arial','B',8);
		//Datos
		$fill=false;
		foreach($data as $row)
		{
			$this->Cell(71,3,$row[6],'LR',1,'R');
			$this->Cell(71,3,$row[5],'LR',1,'C');
			$this->Cell($w[0],6,$row[0],'L',0,'L',$fill);
			$this->Cell($w[1],6,$row[1],0,0,'L',$fill);
			$this->Cell($w[2],6,$row[2],0,0,'L',$fill);
			$this->Cell($w[3],6,$row[3],0,0,'R',$fill);
			$this->Cell($w[4],6,$row[4],'R',0,'R',$fill);
			$this->Ln();
			$this->Cell(71,3,$row[7],'LR',1,'L');
			$this->Cell(71,3,"",'LR',1,'L');
			$fill=!$fill;
		}
		$this->Cell(array_sum($w),0,'','T');
	}
*/	
}

function actaEntregaRecepcion($contrato, $compania, $direccion, $rfc, $telefono, $diasRet, $id_factura, $nombre1, $nombre2) {
	global $unidad;
	global $ubicacion;
	global $entidad;
	$pdf=new PDF('P','mm','letter');
// -------- 	 TICKET PARA CLIENTE
	//Títulos de las columnas
	$header=array('QTY.','MEDIDA','MATERIAL','P.UNITARIO','IMPORTE');
	//Carga de datos
//	$tam = array(80,$alto+40);
	$pdf->SetMargins(10,1,10);
	$pdf->SetDisplayMode("real");
	$pdf->AddPage("p");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(170,5,"Hoja No. " . $pdf->PageNo(),0,1,"R");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,"Unidad:",'LTR',0,"L");
	$pdf->Cell(70,5,"Contrato No. " . $contrato . "             AD-491",'LTR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,$unidad,'LBR',0,"L");
	$pdf->Cell(70,5,"",'LBR',1,"L");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,html_entity_decode("Ubicaci&oacute;n:"),'LTR',0,"L");
	$pdf->Cell(70,5,"",'LTR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($ubicacion),'LBR',0,"L");
	$pdf->Cell(70,5,"Compromiso                           Real     ",'LR',1,"C");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,"Entidad:",'LTR',0,"L");
	$pdf->Cell(70,5,"Fechas: ",'LR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($entidad),'LBR',0,"L");
	$pdf->Cell(70,5,html_entity_decode("D&iacute;a        Mes        A&ntilde;o        D&iacute;a        Mes        A&ntilde;o"),'LR',1,"R");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,html_entity_decode("Contratista y/o compa&ntilde;ia:"),'LTR',0,"L");
	$pdf->Cell(70,5,"Inicio      ",'LR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($compania),'LBR',0,"L");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(70,5,html_entity_decode("T&eacute;rmino  "),'LR',1,"L");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,html_entity_decode("Direcci&oacute;n:"),'LTR',0,"L");
	$pdf->Cell(70,5,"",'LR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($direccion),'LBR',0,"L");
	$pdf->Cell(70,5,"",'LR',1,"R");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(60,5,html_entity_decode("R.F.C."),'LTR',0,"L");
	$pdf->Cell(60,5,html_entity_decode("Tel&eacute;fono (s):"),'LTR',0,"L");
	$pdf->Cell(70,5,html_entity_decode("D&iacute;as de retraso:"),'LTR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(60,5,html_entity_decode($rfc),'LBR',0,"L");
	$pdf->Cell(60,5,html_entity_decode($telefono),'LBR',0,"L");
	$pdf->Cell(70,5,$diasRet,'LBR',1,"L");
	$pdf->Ln();
//  -----------   aquí se trabaja con los conceptos de la factura enviada	
	$detallesFactura = getConceptosXfactura($id_factura);
	$tDet = count($detallesFactura);
	$descripcion = "";
	for ($i=0; $i<$tDet; $i++) {
		$descripcion .= $detallesFactura[$i]['descripcion'] . "\n";
	}
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(190,5,html_entity_decode("Descripci&oacute;n de trabajos a realizar:"),'LTR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->MultiCell(190,5,html_entity_decode($descripcion),'LBR',"J");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(10,5,html_entity_decode("Cant"),'LTB',0,"C");
	$pdf->Cell(120,5,html_entity_decode("Descripci&oacute;n"),'TB',0,"L");
	$pdf->Cell(30,5,html_entity_decode("P. Unitario"),'TB',0,"R");
	$pdf->Cell(30,5,html_entity_decode("Importe"),'TRB',1,"R");
	$pdf->SetFont('Arial','B',7);
	for ($i=0; $i<$tDet; $i++) {
		$pdf->Cell(10,5,html_entity_decode($detallesFactura[$i]['cantidad']),'L',0,"L");
		$pdf->Cell(120,5,html_entity_decode($detallesFactura[$i]['descripcion']),0,0,"L");
		$pdf->Cell(30,5,"$ " . number_format($detallesFactura[$i]['precio_unitario'],2,".",","),0,0,"R");
		$pdf->Cell(30,5,"$ " . number_format($detallesFactura[$i]['importe'],2,".",","),'R',1,"R");
	}
	$pdf->Cell(10,5,"",'T',0,"L");
	$pdf->Cell(120,5,"",'T',0,"L");
	$pdf->Cell(30,5,"",'T',0,"L");
	$pdf->Cell(30,5,"",'T',1,"L");
	$factura = getFacturasXid($id_factura);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,html_entity_decode("Observaciones"),'LT',0,"L");
	$pdf->Cell(40,5,html_entity_decode("Subtotal"),'T',0,"R");
	$pdf->Cell(30,5,"$ " . number_format($factura['subtotal'],2,".",","),'TR',1,"R");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($rfc),'L',0,"L");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(40,5,html_entity_decode("IVA"),0,0,"R");
	$pdf->Cell(30,5,"$ " . number_format($factura['iva'],2,".",","),'R',1,"R");
	$pdf->Cell(120,5,"",'L',0,"L");
	$pdf->Cell(40,5,html_entity_decode("Retenci&oacute;n"),0,0,"R");
	$pdf->Cell(30,5,"$ " . number_format($factura['retenciones'],2,".",","),'R',1,"R");
	$pdf->Cell(120,5,"",'LB',0,"L");
	$pdf->Cell(40,5,html_entity_decode("Total"),'B',0,"R");
	$pdf->Cell(30,5,"$ " . number_format($factura['total'],2,".",","),'RB',1,"R");
	$pdf->Ln();
	$pdf->Cell(63,5,html_entity_decode("Se autoriza el pago de los trabajos por"),'LTR',0,"C");
	$pdf->Cell(63,5,html_entity_decode("Nombre y firma del usuario"),'T',0,"C");
	$pdf->Cell(64,5,html_entity_decode("Nombre y firma del contratista"),'LTR',1,"C");
	$pdf->Cell(63,5,html_entity_decode("cumplir en tiempo y calidad"),'LR',0,"C");
	$pdf->Cell(63,5,html_entity_decode("del equipo de unidad"),0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,"",'LR',0,"C");
	$pdf->Cell(63,5,"",0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,"",'LR',0,"C");
	$pdf->Cell(63,5,"",0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,"",'LR',0,"C");
	$pdf->Cell(63,5,"",0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,"Subdirector Administrativo",'LR',0,"C");
	$pdf->Cell(63,5,"",0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,html_entity_decode($nombre1),'LBR',0,"C");
	$pdf->Cell(63,5,html_entity_decode($nombre2),'B',0,"C");
	$pdf->Cell(64,5,html_entity_decode($compania),'LBR',1,"C");
	
	$pdf->nombre = 'borrar';
	$pdf->Output();
}

function suficiencia($contrato,$nombre1, $nombre2, $nombre3) { // subdirector, encargado, recuros financieros
	global $unidad;
	global $ubicacion;
	global $entidad;
	$pdf=new PDF('P','mm','letter');
// -------- 	 TICKET PARA CLIENTE
	//Títulos de las columnas
	$header=array('QTY.','MEDIDA','MATERIAL','P.UNITARIO','IMPORTE');
	//Carga de datos
//	$tam = array(80,$alto+40);
	$pdf->SetMargins(10,1,10);
	$pdf->SetDisplayMode("real");
	$pdf->AddPage("p");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(170,5,"Hoja No. " . $pdf->PageNo(),0,1,"R");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,"Unidad:",'LTR',0,"L");
	$pdf->Cell(70,5,"Contrato No. " . $contrato . "             AD-491",'LTR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,$unidad,'LBR',0,"L");
	$pdf->Cell(70,5,"",'LBR',1,"L");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,html_entity_decode("Ubicaci&oacute;n:"),'LTR',0,"L");
	$pdf->Cell(70,5,"",'LTR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($ubicacion),'LBR',0,"L");
	$pdf->Cell(70,5,"Compromiso                           Real     ",'LR',1,"C");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,"Entidad:",'LTR',0,"L");
	$pdf->Cell(70,5,"Fechas: ",'LR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($entidad),'LBR',0,"L");
	$pdf->Cell(70,5,html_entity_decode("D&iacute;a        Mes        A&ntilde;o        D&iacute;a        Mes        A&ntilde;o"),'LR',1,"R");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,html_entity_decode("Contratista y/o compa&ntilde;ia:"),'LTR',0,"L");
	$pdf->Cell(70,5,"Inicio      ",'LR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($compania),'LBR',0,"L");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(70,5,html_entity_decode("T&eacute;rmino  "),'LR',1,"L");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,html_entity_decode("Direcci&oacute;n:"),'LTR',0,"L");
	$pdf->Cell(70,5,"",'LR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($direccion),'LBR',0,"L");
	$pdf->Cell(70,5,"",'LR',1,"R");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(60,5,html_entity_decode("R.F.C."),'LTR',0,"L");
	$pdf->Cell(60,5,html_entity_decode("Tel&eacute;fono (s):"),'LTR',0,"L");
	$pdf->Cell(70,5,html_entity_decode("D&iacute;as de retraso:"),'LTR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(60,5,html_entity_decode($rfc),'LBR',0,"L");
	$pdf->Cell(60,5,html_entity_decode($telefono),'LBR',0,"L");
	$pdf->Cell(70,5,$diasRet,'LBR',1,"L");
	$pdf->Ln();
//  -----------   aquí se trabaja con los conceptos de la factura enviada	
	$detallesFactura = getConceptosXfactura($id_factura);
	$tDet = count($detallesFactura);
	$descripcion = "";
	for ($i=0; $i<$tDet; $i++) {
		$descripcion .= $detallesFactura[$i]['descripcion'] . "\n";
	}
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(190,5,html_entity_decode("Descripci&oacute;n de trabajos a realizar:"),'LTR',1,"L");
	$pdf->SetFont('Arial','B',7);
	$pdf->MultiCell(190,5,html_entity_decode($descripcion),'LBR',"J");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(10,5,html_entity_decode("Cant"),'LTB',0,"C");
	$pdf->Cell(120,5,html_entity_decode("Descripci&oacute;n"),'TB',0,"L");
	$pdf->Cell(30,5,html_entity_decode("P. Unitario"),'TB',0,"R");
	$pdf->Cell(30,5,html_entity_decode("Importe"),'TRB',1,"R");
	$pdf->SetFont('Arial','B',7);
	for ($i=0; $i<$tDet; $i++) {
		$pdf->Cell(10,5,html_entity_decode($detallesFactura[$i]['cantidad']),'L',0,"L");
		$pdf->Cell(120,5,html_entity_decode($detallesFactura[$i]['descripcion']),0,0,"L");
		$pdf->Cell(30,5,"$ " . number_format($detallesFactura[$i]['precio_unitario'],2,".",","),0,0,"R");
		$pdf->Cell(30,5,"$ " . number_format($detallesFactura[$i]['importe'],2,".",","),'R',1,"R");
	}
	$pdf->Cell(10,5,"",'T',0,"L");
	$pdf->Cell(120,5,"",'T',0,"L");
	$pdf->Cell(30,5,"",'T',0,"L");
	$pdf->Cell(30,5,"",'T',1,"L");
	$factura = getFacturasXid($id_factura);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(120,5,html_entity_decode("Observaciones"),'LT',0,"L");
	$pdf->Cell(40,5,html_entity_decode("Subtotal"),'T',0,"R");
	$pdf->Cell(30,5,"$ " . number_format($factura['subtotal'],2,".",","),'TR',1,"R");
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(120,5,html_entity_decode($rfc),'L',0,"L");
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(40,5,html_entity_decode("IVA"),0,0,"R");
	$pdf->Cell(30,5,"$ " . number_format($factura['iva'],2,".",","),'R',1,"R");
	$pdf->Cell(120,5,"",'L',0,"L");
	$pdf->Cell(40,5,html_entity_decode("Retenci&oacute;n"),0,0,"R");
	$pdf->Cell(30,5,"$ " . number_format($factura['retenciones'],2,".",","),'R',1,"R");
	$pdf->Cell(120,5,"",'LB',0,"L");
	$pdf->Cell(40,5,html_entity_decode("Total"),'B',0,"R");
	$pdf->Cell(30,5,"$ " . number_format($factura['total'],2,".",","),'RB',1,"R");
	$pdf->Ln();
	$pdf->Cell(63,5,html_entity_decode("Se autoriza el pago de los trabajos por"),'LTR',0,"C");
	$pdf->Cell(63,5,html_entity_decode("Nombre y firma del usuario"),'T',0,"C");
	$pdf->Cell(64,5,html_entity_decode("Nombre y firma del contratista"),'LTR',1,"C");
	$pdf->Cell(63,5,html_entity_decode("cumplir en tiempo y calidad"),'LR',0,"C");
	$pdf->Cell(63,5,html_entity_decode("del equipo de unidad"),0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,"",'LR',0,"C");
	$pdf->Cell(63,5,"",0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,"",'LR',0,"C");
	$pdf->Cell(63,5,"",0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,"",'LR',0,"C");
	$pdf->Cell(63,5,"",0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,"Subdirector Administrativo",'LR',0,"C");
	$pdf->Cell(63,5,"",0,0,"C");
	$pdf->Cell(64,5,"",'LR',1,"C");
	$pdf->Cell(63,5,html_entity_decode($nombre1),'LBR',0,"C");
	$pdf->Cell(63,5,html_entity_decode($nombre2),'B',0,"C");
	$pdf->Cell(64,5,html_entity_decode($compania),'LBR',1,"C");
	
	$pdf->nombre = 'borrar';
	$pdf->Output();
}

$unidad = "HOSPITAL REGIONAL VALENTIN GOMEZ FARIAS";
$ubicacion = "SOLEDAD OROZCO No. 203 COLONIA EL CAPULLO    C.P. 45100";
$entidad = "ZAPOPAN, JALISCO";
$subdirector = "LIC. JOSE LUIS LOPEZ MALDONADO";
$encargado = "ING. JUAN RAMON GARCIA LOPEZ";
$recursosFinancieros = "LIC. MA. MIKOTAC MEDRANO GOMEZ";
//$ret = actaEntregaRecepcion("1234", "aqui ira el proveedor", "dir del proveedor", "rfc del prove", "tel" , "x dias", "1", $subdirector, $encargado);
$ret = suficiencia("1234", $subdirector, $encargado, $recursosFinancieros);

?>
