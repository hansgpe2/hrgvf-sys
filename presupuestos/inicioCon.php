<?php
include_once('lib/misFunciones.php');
session_start ();

$contratos = getContratos();
$Ncontratos = count($contratos);
$out = "<table width=\"800\">
      	<tr><td><div id=\"menu\">
        			<table border=\"0\" width=\"100%\" class=\"tablaPrincipal\"><tr>
                    	<td width=\"150\"><a href=\"javascript: inicio('inicioCon.php');\" title=\"Inicio\" class=\"botones_menu\"><img src=\"diseno/_medHome.gif\" width=\"40\" height=\"40\" border=\"0\" /><br>Inicio</a></td>
                    	<td width=\"100\">&nbsp;</td>
                    	<td width=\"250\">&nbsp;</td>
                    	<td width=\"100\" align=\"center\"><a href=\"javascript: reportes();\" title=\"Reportes\" class=\"botones_menu\"><img src=\"diseno/printer.png\" width=\"40\" height=\"40\" border=\"0\" /><br>Reportes</a></td>
                        <td width=\"100\"><a href=\"javascript:ayuda();\" title=\"Ayuda\" class=\"botones_menu\"><img src=\"diseno/ayuda.png\" width=\"40\" height=\"40\" border=\"0\" /><br>Ayuda</a></td>
                        <td width=\"100\" align=\"right\"><a href=\"javascript:logout();\" title=\"Salir\" class=\"botones_menu\"><img src=\"diseno/logout.png\" width=\"40\" height=\"40\" border=\"0\" /><br>Salir</a></td>
                    </tr></table>
              	</div>
        </td></tr>
        <tr><td align=\"center\"><br /><div id=\"centro_contenido\">";
$temp = "<table width=\"100%\" cellspaccing=\"0\" cellpadding=\"0\"><tr><th>N. Contrato</th><th>Descripci&oacute;n</th><th>F. Inicio</th><th>F. Termino</th><th>Monto Total</th><th>Monto Ejercido</th></tr>";

for($i=0; $i<$Ncontratos; $i++) {
	$temp .= "<tr onmouseover=\"this.style.backgroundColor = '#EEEEEE'\" onmouseout=\"this.style.backgroundColor = '#FFFFFF'\"><td>" . $contratos[$i]["num_contrato"] . "</td><td>" . $contratos[$i]["descripcion_contrato"] . "</td><td>" . formatoDia($contratos[$i]["fecha_inicio"],"fechaI") . "</td><td>" . formatoDia($contratos[$i]["fecha_terminacion"],"fechaI") . "</td><td align=\"right\">$" . number_format($contratos[$i]["monto_total"],2,".",",") . "</td><td align=\"right\">$" . number_format($contratos[$i]["monto_ejercido"],2,".",",") . " &nbsp;&nbsp;<a class=\"botones_menu\" href=\"javascript: masInfoContrato(" . $contratos[$i]["id_contrato"] . ");\">+ info</a></td></tr>";
}
$temp .= "</table>";

$out.= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"center\">" ;
$out.= "<table width=\"720\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">CONTRATOS EN EL SISTEMA</td>
        </tr>
        <tr>
          <td align=\"left\">" . $temp . "
		  </td>
        </tr>
		<tr>
			<td align=\"right\"><br><br><a href=\"javascript:agregarContrato();\" class=\"linkIndex\">Agregar Contrato<img src=\"diseno/Symbol-Add.png\"width=\"42\" border=\"0\"></a>
			</td>
		</tr>
      </table>
	  </td></tr></table></div>
	</td></tr>
  </table>";
print($out);
?>