<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documentos</title>
<style type="text/css">
	@import url("lib/impresion.css") print;
	@import url("lib/reportes.css") screen;
</style>
</head>

<body>
<?php include_once('lib/misFunciones.php');
$settings = getSettings("1");
$factura = getFacturasXid($_GET['id_factura']);
$facturaConceptos = getConceptosXfactura($_GET['id_factura']);
$contrato = getContratoXid($factura['id_contrato']);
$proveedor = getProveedorXid($factura['id_proveedor']);
$partidas = getPartidasXcontrato($factura['id_contrato']);

$subdireccion1 = "Subdirecci&oacute;n General de Obras y Mantenimiento";
$subdireccion2 = "Subdirecci&oacute;n de Conservaci&oacute;n y Mantenimiento";
$unidad = $settings['unidad'];
$ubicacion = $settings['ubicacion'];
$entidad = $settings['entidad'];
$contratista = $proveedor['razon_social'];
$direccion = $proveedor['direccion'] . " COL. " .  $proveedor['colonia'] . " C.P. " .  $proveedor['cp'];
$rfc = $proveedor['rfc'];
$curp = $proveedor['extra'];
$telefono = $proveedor['telefono'];
$fechaFactura = $factura['fecha_entrega'];
$folio = $factura['folio'];
$mesFactura = substr($fechaFactura,4,2);
$mesFactura=(string)(int)$mesFactura;
$anioFactura = substr($fechaFactura,0,4);
$fechaInicioCom = $contrato['fecha_inicio'];
$fechaTerminoCom = $contrato['fecha_terminacion'];
$partida = "";
$long = count($partidas);
for ($i=0; $i<$long; $i++) {
	$datosPartida = getPartidaXid($partidas[$i]['id_partida']);
	$partida .= $datosPartida['partida'] . "-" . $datosPartida['subpartida'] . "<br />";
}
$tipoContrato = $contrato['tipo_contrato'];
$fechas = explode("|",$factura['extra']);
$fechaInicioReal = $fechas[0];
$fechaTerminoReal = $fechas[1];
$diasRetraso = $fechas[2];
$conceptos = "";
$detalles = "";
$long = count($facturaConceptos);
for ($i=0; $i<$long; $i++) {
	$conceptos = $facturaConceptos[$i]['descripcion'] . "<br>";
	$detalles = "
		  <tr>
            <td class=\"tituloEncabezado\" align=\"left\" valign=\"top\">" . $facturaConceptos[$i]['cantidad'] . "</td>
            <td class=\"tituloEncabezado\" align=\"left\" valign=\"top\">" . $facturaConceptos[$i]['descripcion'] . "</td>
            <td class=\"tituloEncabezado\" align=\"right\" valign=\"top\">$ " . number_format($facturaConceptos[$i]['precio_unitario'],2,".",",") . "</td>
            <td class=\"tituloEncabezado\" align=\"right\" valign=\"top\">$ " . number_format($facturaConceptos[$i]['importe'],2,".",",") . "</td>
          </tr>";
}
$subtotal = number_format($factura['subtotal'],2,".",",");
$iva = number_format($factura['iva'],2,".",",");
$retencion = number_format($factura['retenciones'],2,".",",");
$total = number_format($factura['total'],2,".",",");
$contrato = $contrato['num_contrato'];
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="74" align="left"><img src="diseno/logoEncabezado.png" width="74" height="74" /></td>
        <td width="80" class="tituloIssste" align="left">Instituto de<br />
          Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
        <td align="center"><?php echo $subdireccion1 ?><br /><?php echo $subdireccion2 ?><br /><?php echo $unidad ?><br /><br />ACTA DE ENTREGA RECEPCION</td>
        <td width="150" valign="bottom" align="right"><table width="150" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezado" align="center">Hoja No. 1</td>
            <td class="tituloEncabezado" align="center">De 1</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="65%"><span class="contenido8bold">Unidad:</span><br /><?php echo $unidad ?></td>
    <td align="left" class="tituloEncabezadoConBorde" width="35%"><span class="contenido8bold">Contrato No. </span><?php echo $contrato ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="65%"><span class="contenido8bold">Ubicaci&oacute;n:</span><br /><?php echo $ubicacion ?></td>
    <td align="left" class="tituloEncabezadoConBorde" width="35%" rowspan="4"><span class="contenido8bold"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezado" align="left" width="20%"><span class="contenido8bold">FECHAS:</span></td>
            <td class="tituloEncabezado" align="center" width="40%" colspan="3"><span class="contenido8bold">COMPROMISO:</span></td>
            <td class="tituloEncabezado" align="center" width="40%" colspan="3"><span class="contenido8bold">REAL:</span></td>
          </tr>
          <tr>
            <td class="tituloEncabezado" align="left">&nbsp;</td>
            <td class="tituloEncabezado" align="left">DIA</td>
            <td class="tituloEncabezado" align="left">MES</td>
            <td class="tituloEncabezado" align="left">A&Ntilde;O</td>
            <td class="tituloEncabezado" align="left">DIA</td>
            <td class="tituloEncabezado" align="left">MES</td>
            <td class="tituloEncabezado" align="left">A&Ntilde;O</td>
          </tr>
          <tr>
            <td class="contenido8" align="left">INICIO</td>
            <td class="contenido8" align="left"><?php echo substr($fechaInicioCom,6,2) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaInicioCom,4,2) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaInicioCom,0,4) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaInicioReal,0,2) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaInicioReal,3,2) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaInicioReal,6,4) ?></td>
          </tr>
          <tr>
            <td class="contenido8" align="left">TERMINO</td>
            <td class="contenido8" align="left"><?php echo substr($fechaTerminoCom,6,2) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaTerminoCom,4,2) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaTerminoCom,0,4) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaTerminoReal,0,2) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaTerminoReal,3,2) ?></td>
            <td class="contenido8" align="left"><?php echo substr($fechaTerminoReal,6,4) ?></td>
          </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="65%"><span class="contenido8bold">Entidad:</span><br /><?php echo $entidad ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="65%"><span class="contenido8bold">Contratista y/o compa&ntilde;ia:</span><br /><?php echo $contratista ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="65%"><span class="contenido8bold">Direcci&oacute;n:</span><br /><?php echo $direccion ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="65%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezadoConBorde" align="left" width="50%"><span class="contenido8bold">R.F.C.:</span><br /><?php echo $rfc ?></td>
            <td class="tituloEncabezadoConBorde" align="left" width="50%"><span class="contenido8bold">Tel&eacute;fono(s):</span><br /><?php echo $telefono ?></td>
          </tr>
    </table></td>
    <td align="left" class="tituloEncabezadoConBorde" width="35%"><span class="contenido8bold">D&iacute;as de retraso: </span><br /><?php echo $diasRetraso ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezado" width="100%" colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="100%" colspan="2"><span class="contenido8bold">Descripci&oacute;n de los trabajos a realizar:</span><br /><?php echo $conceptos ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="100%" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezado" align="left" width="10%" valign="top"><span class="contenido8bold">CANT.</span></td>
            <td class="tituloEncabezado" align="left" width="60%" valign="top"><span class="contenido8bold">DESCRIPCION</span></td>
            <td class="tituloEncabezado" align="center" width="15%" valign="top"><span class="contenido8bold">P. UNITARIO</span></td>
            <td class="tituloEncabezado" align="center" width="15%" valign="top"><span class="contenido8bold">IMPORTE</span></td>
            <?php echo $detalles ?>
          </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="100%" colspan="2"><span class="contenido8bold">Observaciones:</span><br /><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezado" align="right" width="70%" valign="top"><span class="contenido8bold">SUBTOTAL</span></td>
            <td class="tituloEncabezado" align="right" width="15%" valign="top">$ <?php echo $subtotal; ?></td>
            <td class="tituloEncabezado" align="right" width="15%" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td class="tituloEncabezado" align="right" width="70%" valign="top"><span class="contenido8bold">IVA</span></td>
            <td class="tituloEncabezado" align="right" width="15%" valign="top">$ <?php echo $iva; ?></td>
            <td class="tituloEncabezado" align="right" width="15%" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td class="tituloEncabezado" align="right" width="70%" valign="top"><span class="contenido8bold">RETENCION</span></td>
            <td class="tituloEncabezado" align="right" width="15%" valign="top">$ <?php echo $retencion; ?></td>
            <td class="tituloEncabezado" align="right" width="15%" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td class="tituloEncabezado" align="right" width="70%" valign="top"><span class="contenido8bold">TOTAL</span></td>
            <td class="tituloEncabezado" align="right" width="15%" valign="top">$ <?php echo $total; ?></td>
            <td class="tituloEncabezado" align="right" width="15%" valign="top">&nbsp;</td>
          </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="100%" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezadoConBorde" align="center" width="33%" valign="top">Se autoriza el pago de los trabajos por cumplir en tiempo y calidad<br /><br />Subdirector Administrativo<br /><br /><br /><br /><?php echo $settings['subdirector_advo'] ?></td>
            <td class="tituloEncabezadoConBorde" align="center" width="34%" valign="top">Nombre y firma del usuario del equipo de unidad<br /><br /><br /><br /><br /><br /><?php echo  $settings['encargado_area'] ?></td>
            <td class="tituloEncabezadoConBorde" align="center" width="33%" valign="top">Nombre y firma del contratista<br /><br /><br /><br /><br /><br /><br /><?php echo  $proveedor['razon_social'] ?></td>
          </tr>
    </table></td>
  </tr>
</table>
<h1 class="SaltoDePagina" >&nbsp;</h1>



<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tituloEncabezadoConBorde">
  <tr>
    <td colspan="5"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="74" align="left" rowspan="2"><img src="diseno/logoEncabezado.png" width="74" height="74" /></td>
        <td width="80" class="tituloIssste" align="left" rowspan="2">Instituto de<br />
          Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
        <td align="center">CONSTANCIA DE PERCEPCIONES Y RETENCIONES</td>
        <td width="150" valign="bottom" align="right" class="tituloEncabezado">No. Documento</td>
      </tr>
      <tr>
        <td align="center" colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezado" align="left" width="30%" rowspan="2"><span class="contenido8bold">PERIODO QUE AMPARA LA CONSTANCIA</span></td>
            <td class="tituloEncabezado" align="center" width="15%"><span class="contenido8bold">MES</span></td>
            <td class="tituloEncabezado" align="center" width="15%"><span class="contenido8bold">A&Ntilde;O</span></td>
            <td class="tituloEncabezado" align="center" width="15%"><span class="contenido8bold">MES</span></td>
            <td class="tituloEncabezado" align="center" width="15%"><span class="contenido8bold">A&Ntilde;O</span></td>
          </tr>
          <tr>
            <td class="tituloEncabezadoConBorde" align="center"><?php echo tituloMes($mesFactura) ?></td>
            <td class="tituloEncabezadoConBorde" align="center"><?php echo $anioFactura; ?></td>
            <td class="tituloEncabezadoConBorde" align="center"><?php echo tituloMes($mesFactura) ?></td>
            <td class="tituloEncabezadoConBorde" align="center"><?php echo $anioFactura; ?></td>
          </tr>
    	  </table>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde"><span class="contenido8bold">DATOS DEL CONTRIBUYENTE A QUIEN SE LE EXPIDE LA CONSTANCIA</span></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="20%"><span class="contenido8bold">APELLIDO PATERNO, MATERNO Y NOMBRE(S)<br />DENOMINACION, RAZON SOCIAL</span></td>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="80%"><?php echo $contratista ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="20%"><span class="contenido8bold">REGISTRO FEDERAL DE CONTRIBUYENTES</span></td>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="80%"><?php echo $rfc ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="20%"><span class="contenido8bold">CLAVE UNICO REGISTRO DE POBLACION</span></td>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="80%"><?php echo $curp ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="20%"><span class="contenido8bold">DOMICILIO FISCAL</span></td>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="80%"><?php echo $direccion ?></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="20%"><span class="contenido8bold">TELEFONO</span></td>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="80%"><?php echo $telefono ?></td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde"><span class="contenido8bold">TIPO DE INGRESO O ACTIVIDAD</span></td>
  </tr>
  <tr>
    <td colspan="5" align="left" class="tituloEncabezadoConBorde">MARQUE CON "X" EL RECUADRO QUE CORRESPONDA<br /><br /><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezado" align="right" width="20%" valign="top"><span class="contenido8">HONORARIOS</span></td>
            <td class="tituloEncabezadoConBorde" align="center" width="10%" valign="top">&nbsp;</td>
            <td class="tituloEncabezado" align="right" width="20%" valign="top"><span class="contenido8">&nbsp;</span></td>
            <td class="tituloEncabezadoConBorde" align="center" width="10%" valign="top">&nbsp;</td>
            <td class="tituloEncabezado" align="right" width="30%" valign="top"><span class="contenido8">ENAJENACION DE BIENES</span></td>
            <td class="tituloEncabezadoConBorde" align="center" width="10%" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="6">&nbsp;</td>
          </tr>
          <tr>
            <td class="tituloEncabezado" align="right" width="20%" valign="top"><span class="contenido8">INTERESES</span></td>
            <td class="tituloEncabezadoConBorde" align="center" width="10%" valign="top">&nbsp;</td>
            <td class="tituloEncabezado" align="right" width="20%" valign="top"><span class="contenido8">&nbsp;</span></td>
            <td class="tituloEncabezadoConBorde" align="center" width="10%" valign="top">&nbsp;</td>
            <td class="tituloEncabezado" align="right" width="30%" valign="top"><span class="contenido8">OTROS INGRESOS</span></td>
            <td class="tituloEncabezadoConBorde" align="center" width="10%" valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="5" align="right"><span class="contenido8">(INCLUYENDOLOS DEL CAPITULO X L ISR)</span></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="6">&nbsp;</td>
          </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde"><span class="contenido8bold">IMPUESTO SOBRE LA RENTA</span></td>
  </tr>
  <tr>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="64%"><span class="contenido8bold">A.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MONTO TOTAL PAGADO</span></td>
    <td align="left" class="tituloEncabezadoConBorde" width="33%">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="64%"><span class="contenido8bold">B.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPUESTO RETENIDO Y ENTERADO</span></td>
    <td align="left" class="tituloEncabezadoConBorde" width="33%">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde"><span class="contenido8bold">IMPUESTO AL VALOR AGREGADO</span></td>
  </tr>
  <tr>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="64%"><span class="contenido8bold">C.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MONTO DEL VALOR DE LOS ACTOS O ACTIVIDADES GRAVADAS</span></td>
    <td align="right" class="tituloEncabezadoConBorde" width="33%">$ <?php echo $subtotal ?></td>
  </tr>
  <tr>
    <td colspan="4" align="left" class="tituloEncabezadoConBorde" width="64%"><span class="contenido8bold">D.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;IMPUESTO RETENIDO Y ENTERADO</span></td>
    <td align="right" class="tituloEncabezadoConBorde" width="33%">$ <?php echo $iva ?></td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde"><span class="contenido8bold">DATOS DEL RETENEDOR</span></td>
  </tr>
  <tr>
    <td colspan="3" align="left" class="tituloEncabezadoConBorde" width="40%"><span class="contenido8bold">REGISTRO FEDERAL DE CONTRIBUYENTES</span></td>
    <td colspan="2" align="left" class="tituloEncabezadoConBorde" width="60%"><?php echo $settings['rfc_unidad']; ?></td>
  </tr>
  <tr>
    <td colspan="3" align="left" class="tituloEncabezadoConBorde" width="40%"><span class="contenido8bold">NOMBRE, DENOMINACION O RAZON SOCIAL</span></td>
    <td colspan="2" align="left" class="tituloEncabezadoConBorde" width="60%"><?php echo $settings['nombre_unidad']; ?></td>
  </tr>
  <tr>
    <td colspan="3" align="left" class="tituloEncabezadoConBorde" width="40%"><span class="contenido8bold">DOMICILIO FISCAL</span></td>
    <td colspan="2" align="left" class="tituloEncabezadoConBorde" width="60%"><?php echo $settings['domicilio_unidad']; ?></td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde"><span class="contenido8bold">DATOS DEL REPRESENTANTE LEGAL</span></td>
  </tr>
  <tr>
    <td colspan="2" align="left" class="tituloEncabezadoConBorde" width="33%"><span class="contenido8bold">APELLIDO PATERNO, MATERNO Y NOMBRE</span></td>
    <td colspan="3" align="left" class="tituloEncabezadoConBorde" width="67%"><?php echo $settings['director']; ?></td>
  </tr>
  <tr>
    <td colspan="2" align="left" class="tituloEncabezadoConBorde" width="33%"><span class="contenido8bold">REGISTRO FEDERAL DE CONTRIBUYENTES</span></td>
    <td colspan="3" align="left" class="tituloEncabezadoConBorde" width="67%"><?php echo $settings['rfc_director']; ?></td>
  </tr>
  <tr>
    <td colspan="2" align="left" class="tituloEncabezadoConBorde" width="33%"><span class="contenido8bold">C.U.R.P.</span></td>
    <td colspan="3" align="left" class="tituloEncabezadoConBorde" width="67%"><?php echo $settings['curp_director']; ?></td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezadoConBorde">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="100%" colspan="5"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="tituloEncabezadoConBorde" align="center" width="33%" valign="top" height="100">FIRMA DEL RETENEDOR O REPRESENTANTE LEGAL</td>
            <td class="tituloEncabezadoConBorde" align="center" width="34%" valign="top">SELLO (EN CASO DE TENERLO)</td>
            <td class="tituloEncabezadoConBorde" align="center" width="33%" valign="top">FIRMA DE RECIBIDO POR EL CONTRIBUYENTE</td>
          </tr>
    </table></td>
  </tr>
</table>
<h1 class="SaltoDePagina" >&nbsp;</h1>



<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="6"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="74" align="left" rowspan="2"><img src="diseno/logoEncabezado.png" width="74" height="74" /></td>
        <td width="80" class="tituloIssste" align="left">Instituto de<br />
          Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
        <td align="center">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezadoConBorde"></td>
  </tr>
  <tr>
    <td colspan="5" align="center" class="tituloEncabezado">&nbsp;<br />&nbsp;<br />&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" align="left" class="tituloEncabezado"><?php echo strtoupper($unidad); ?><br />JEFATURA DE INFROMATICA Y TELECOMUNICACIONES<br />OFICIO NO. XXXXX</td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezado">&nbsp;<br />&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" align="left" class="tituloEncabezado"><?php echo strtoupper($entidad); ?></td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezado">&nbsp;<br />&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" align="left" class="tituloEncabezado"><?php echo strtoupper($settings['recursos_financieros']); ?><br /> COORD. DE RECURSOS FINANCIEROS<br />P R E S E N T E :</td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezado">&nbsp;<br />&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" align="left" class="tituloEncabezado">Por este conducto informo a usted, acerca de las siguientes facturas, que de acuerdo al proceso de contrataci&oacute;n por <?php echo $arregloTipoContrato[$tipoContrato] ?> NO _____, SI_____ se aplica sanci&oacute;n a las siguientes facturas:<br /><?php echo $contratista ?></td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezado">&nbsp;<br />&nbsp;</td>
  </tr>
  <tr>
    <td align="center" class="tituloEncabezadoConBorde" width="10%"><span class="contenido8bold">No. PROG.</span></td>
    <td align="center" class="tituloEncabezadoConBorde" width="15%"><span class="contenido8bold">PARTIDA</span></td>
    <td align="center" class="tituloEncabezadoConBorde" width="30%"><span class="contenido8bold">DESCRIPCI&Oacute;N</span></td>
    <td align="center" class="tituloEncabezadoConBorde" width="15%"><span class="contenido8bold">COSTO</span></td>
    <td align="center" class="tituloEncabezadoConBorde" width="15%"><span class="contenido8bold">SANCI&Oacute;N</span></td>
    <td align="center" class="tituloEncabezadoConBorde" width="15%"><span class="contenido8bold">TOTAL A PAGAR</span></td>
  </tr>
  <tr>
    <td align="left" class="tituloEncabezadoConBorde" width="10%"><?php echo $folio; ?></td>
    <td align="left" class="tituloEncabezadoConBorde" width="15%"><?php echo $partida; ?></td>
    <td align="left" class="tituloEncabezadoConBorde" width="30%"><?php echo $conceptos ?></td>
    <td align="left" class="tituloEncabezadoConBorde" width="15%">$<?php echo $total ?></td>
    <td align="left" class="tituloEncabezadoConBorde" width="15%">$</td>
    <td align="left" class="tituloEncabezadoConBorde" width="15%">$<?php echo $total ?></td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezado">&nbsp;<br />&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" align="left" class="tituloEncabezado">Lo anterior para su atenci&oacute;n y tr&aacute;mite procedente.</td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezado">&nbsp;<br />&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" align="left" class="tituloEncabezado">Lo anterior para su atenci&oacute;n y tr&aacute;mite procedente.</td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezado">&nbsp;<br />&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" align="center" class="tituloEncabezado">A T E N T A M E N T E<br /><br /><br /><br /><br /><?php echo $settings['encargado_area'] ?><br />Jefe de Inform&aacute;tica y Telecomunicaciones</td>
  </tr>
</table>
</body>
</html>
