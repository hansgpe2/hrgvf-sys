<?php
include_once('lib/misFunciones.php');
session_start ();

$contratos = getContratos();
$Ncontratos = count($contratos);
$out = "<table width=\"800\">
      	<tr><td><div id=\"menu\">
        			<table border=\"0\" width=\"100%\" class=\"tablaPrincipal\"><tr>
                    	<td width=\"150\"><a href=\"javascript: inicio('inicioGen.php');\" title=\"Inicio\" class=\"botones_menu\"><img src=\"diseno/_medHome.gif\" width=\"40\" height=\"40\" border=\"0\" /><br>Inicio</a></td>
                    	<td width=\"100\">&nbsp;</td>
                    	<td width=\"250\">&nbsp;</td>
                    	<td width=\"100\" align=\"center\"><a href=\"javascript: reportes();\" title=\"Reportes\" class=\"botones_menu\"><img src=\"diseno/printer.png\" width=\"40\" height=\"40\" border=\"0\" /><br>Reportes</a></td>
                        <td width=\"100\"><a href=\"javascript:ayuda();\" title=\"Ayuda\" class=\"botones_menu\"><img src=\"diseno/ayuda.png\" width=\"40\" height=\"40\" border=\"0\" /><br>Ayuda</a></td>
                        <td width=\"100\" align=\"right\"><a href=\"javascript:logout();\" title=\"Salir\" class=\"botones_menu\"><img src=\"diseno/logout.png\" width=\"40\" height=\"40\" border=\"0\" /><br>Salir</a></td>
                    </tr></table>
              	</div>
        </td></tr>
        <tr><td align=\"center\"><br /><div id=\"centro_contenido\">";
$temp = "<table width=\"100%\" cellspaccing=\"0\" cellpadding=\"0\"><tr style=\"font-size:11px;\"><th align=\"center\">Contrato</th><th align=\"center\" width=\"200\">Descripci&oacute;n</th><th align=\"center\">F. Inicio</th><th align=\"center\">F. Termino</th><th align=\"center\">Monto <br>Original</th><th align=\"center\">Monto en <br>Proceso</th><th align=\"center\">Monto <br>Ejercido</th></tr>";

$clase = "celdaBlanca";
for($i=0; $i<$Ncontratos; $i++) {
	$montoProceso = $contratos[$i]["monto_total"] - $contratos[$i]["monto_x_ejercer"];
	$facturas = getFacturasXcontrato($contratos[$i]['id_contrato']);
	$tFacturas = count($facturas);
	$detFacturas = "";
	if ($tFacturas > 0) {
		$detFacturas = "<td colspan=\"7\">
			<table cellpadding=\"0\" cellspaccing=\"0\" style=\"border-style:solid; border-width:1px; font-size:9px;\">
				<tr><th width=\"70\" align=\"center\">Factura</th><th width=\"120\" align=\"center\">Fecha Ingreso</th><th width=\"120\" align=\"center\">Subtotal</th><th width=\"100\" align=\"center\">I.V.A.</th><th width=\"100\" align=\"center\">Retenciones</th><th width=\"120\" align=\"center\">Total</th><th width=\"50\" align=\"center\">&nbsp;</th></tr>";
		for ($j=0; $j<$tFacturas; $j++) {
			$detFacturas .= "<tr><td align=\"center\">" . $facturas[$j]['folio'] . "</td><td align=\"center\">" . formatoDia($facturas[$j]['fecha_entrega'],"fechaI") . "</td><td align=\"right\">" . number_format($facturas[$j]['subtotal'],2,".",",") . "</td><td align=\"right\">" . number_format($facturas[$j]['iva'],2,".",",") . "</td><td align=\"right\">" . number_format($facturas[$j]['retenciones'],2,".",",") . "</td><td align=\"right\">" . number_format($facturas[$j]['total'],2,".",",") . "</td><td align=\"center\"><a class=\"botones_menu\" href=\"javascript: masInfoFactura(" . $facturas[$j]["id_factura"] . ");\">detalles<br>Factura</a></td></tr>";
		}
		$detFacturas .= "</table></td>";
	}
	$temp .= "<tr class=\"" . $clase . "\" style=\"font-size:9px;\"><td>" . $contratos[$i]["num_contrato"] . "</td><td>" . $contratos[$i]["descripcion_contrato"] . "</td><td>" . formatoDia($contratos[$i]["fecha_inicio"],"fechaI") . "</td><td>" . formatoDia($contratos[$i]["fecha_terminacion"],"fechaI") . "</td><td align=\"right\">$" . number_format($contratos[$i]["monto_total"],2,".",",") . "</td><td align=\"right\">$" . number_format($montoProceso,2,".",",") . "</td><td align=\"right\">$" . number_format($contratos[$i]["monto_ejercido"],2,".",",") . " &nbsp;&nbsp;<a class=\"botones_menu\" href=\"javascript: masInfoContratoGen(" . $contratos[$i]["id_contrato"] . ");\">+ info</a></td></tr>
		<tr class=\"" . $clase . "\">" . $detFacturas . "</tr>
			
		<tr>
			<td align=\"center\" colspan=\"7\" style=\"border-bottom-width:1px; border-bottom-color:#36F; border-bottom-style:solid;\"><a href=\"javascript:capturarFactura(" . $contratos[$i]["id_contrato"] . ");\" class=\"linkIndex\">Capturar Factura del contrato " . $contratos[$i]["num_contrato"] . " - " . $contratos[$i]["descripcion_contrato"] . "</a><br><br></td>
		</tr>";
	if ($clase == "celdaBlanca") $clase = "celdaColor"; else $clase = "celdaBlanca";
}
$temp .= "</table>";

$out.= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"center\">" ;
$out.= "<table width=\"720\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">CONTRATOS DE TU DEPARTAMENTO</td>
        </tr>
        <tr>
          <td align=\"left\">" . $temp . "
		  </td>
        </tr>
      </table>
	  </td></tr></table></div>
	</td></tr>
  </table>";
print($out);
?>