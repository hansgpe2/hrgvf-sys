<?php session_start ();
include_once('lib/misFunciones.php');
/*
$contrato = getContratoXid($_GET['id_contrato']);
$proveedor = getProveedorXid($contrato['id_proveedor']);
$partidas = getPartidasXcontrato($contrato['id_contrato']);
$tPartidas = count($partidas);
$datosPartidas = "";
$ids_partidas = "";
for($i=0; $i<$tPartidas; $i++) {
	$partida = getPartidaXid($partidas[$i]['id_partida']);
	$datosPartidas .= "<tr><td class=\"conteoCitasVerde\">" . $partida['partida'] . "-" .$partida['subpartida'] . "-" . $partida['subsubpartida'] . "</td><td class=\"conteoCitasVerde\">" . $partida['nombre'] . "</td><td class=\"conteoCitasVerde\" align=\"right\">$" . number_format($partidas[$i]['monto_x_ejercer'],2,".",",") . "</td><td><input name=\"saldo_" . $partidas[$i]['id_partida'] . "\" type=\"hidden\" id=\"saldo_" . $partidas[$i]['id_partida'] . "\" value=\"" . $partidas[$i]['monto_x_ejercer'] . "\" /><input name=\"afectar_" . $partidas[$i]['id_partida'] . "\" id=\"afectar_" . $partidas[$i]['id_partida'] . "\" type=\"text\" size=\"10\" maxlenght=\"10\" onkeyup=\"javascript: saldosKeyUp(event, this);\" value=\"0\" /></td></tr>";
	$ids_partidas .= $partidas[$i]['id_partida'] . "|";
}
$ids_partidas = substr($ids_partidas,0,strlen($ids_partidas)-1);
*/

$factura = getFacturasXid($_GET['id_factura']);
$proveedor = getProveedorXid($factura['id_proveedor']);
$conceptos = getConceptosXfactura($factura['id_factura']);
$tConceptos = count($conceptos);
$textoDescripciones = "";
$subtotal = 0;
for ($i=0; $i<$tConceptos; $i++) {
	$textoDescripciones .= "<tr>";
	$textoDescripciones .= "<td align=\"center\" width=\"80\" style=\"border-bottom-style:solid; border-bottom-width:1px;\">" . $conceptos[$i]['cantidad'] . "</td>";
	$textoDescripciones .= "<td align=\"left\" style=\"border-bottom-style:solid; border-bottom-width:1px;\">" . $conceptos[$i]['descripcion'] . "</td>";
	$textoDescripciones .= "<td align=\"right\" width=\"80\" style=\"border-bottom-style:solid; border-bottom-width:1px;\">$ " . number_format($conceptos[$i]['precio_unitario'],2,".",",") . "</td>";
	$textoDescripciones .= "<td align=\"right\" width=\"150\" style=\"border-bottom-style:solid; border-bottom-width:1px;\">$ " . number_format($conceptos[$i]['importe'],2,".",",") . "</td>";
	$textoDescripciones .= "</tr>";
	$subtotal += $conceptos[$i]['importe'];
}

$iva = $subtotal * $ivaVigente;
$total = $subtotal + $iva;
$granTotal = $total - $factura['retenciones'];


$partidas = getPartidasXcontrato($factura['id_contrato']);
$tPartidas = count($partidas);
$datosPartidas = "";
for($i=0; $i<$tPartidas; $i++) {
	$partida = getPartidaXid($partidas[$i]['id_partida']);
	$cargadoApartida = getCargadoApartida($factura['id_contrato'], $factura['id_proveedor'], $factura['id_factura'], $partida['id_partida']);
	$datosPartidas .= "<tr><td class=\"conteoCitasVerde\">" . $partida['partida'] . "-" .$partida['subpartida'] . "-" . $partida['subsubpartida'] . "</td><td class=\"conteoCitasVerde\">" . $partida['nombre'] . "</td><td class=\"conteoCitasVerde\" align=\"right\">$ " . number_format($partidas[$i]['monto_x_ejercer'],2,".",",") . "</td><td class=\"conteoCitasVerde\" align=\"right\">$ " . number_format($cargadoApartida['cantidad'],2,".",",") . "</td></tr>";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarCapturarFactura();">
<input name="id_contrato" type="hidden" id="id_contrato" value="<?php echo $_GET['id_contrato'] ?>" />
<input name="id_proveedor" type="hidden" id="id_proveedor" value="<?php echo $contrato['id_proveedor'] ?>" />
<input name="ids_partidas" type="hidden" id="ids_partidas" value="<?php echo $ids_partidas ?>" />
<input name="montoXejercerCto" type="hidden" id="montoXejercerCto" value="<?php echo $contrato['monto_x_ejercer'] ?>" />
<input name="vTotal" type="hidden" id="vTotal" value="0" />
<input name="vReten" type="hidden" id="vReten" value="0" />
<input name="vSubto" type="hidden" id="vSubto" value="0" />

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td class="tituloVentana" colspan="5">FACTURA</td>
  </tr>
  <tr>
  	<td colspan="3">&nbsp;</td>
  	<td class="nombrePaciente" align="right">FOLIO <span class="horaVerde"><?php echo $factura['folio'] ?></span></td>
  </tr>
  <tr>
  	<td colspan="2">&nbsp;</td>
  	<td colspan="2" class="nombrePaciente" align="right">FECHA EN FACTURA <span class="horaVerde"><?php echo formatoDia($factura['fecha_elaboracion'],"fecha") ?></span></td>
  </tr>
  <tr>
  	<td class="nombrePaciente" width="80">CLIENTE</td><td><?php echo $proveedor['razon_social']; ?></td><td class="nombrePaciente" width="80">R.F.C.</td><td width="150"><?php echo $proveedor['rfc']; ?></td>
  </tr>
  <tr>
  	<td class="nombrePaciente" width="80">DOMICILIO</td><td><?php echo $proveedor['direccion']; ?></td><td class="nombrePaciente" width="80">TELEFONO</td><td width="150"><?php echo $proveedor['telefono']; ?></td>
  </tr>
  <tr>
  	<td class="nombrePaciente" width="80">CIUDAD</td><td><?php echo $proveedor['ciudad'] . ", " . $proveedor['estado']; ?></td><td class="nombrePaciente" width="80">C.P.</td><td width="150"><?php echo $proveedor['cp']; ?></td>
  </tr>
  <tr>
    <td class="tituloVentana" colspan="5">CONCEPTOS</td>
  </tr>
  <tr>
  	<td class="nombrePaciente">CANTIDAD</td><td class="nombrePaciente">DESCRIPCION</td><td class="nombrePaciente">P. UNITARIO</td><td class="nombrePaciente" align="center">IMPORTE</td>
  </tr>
  <tr>
  	<td colspan="5">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
            <?php echo $textoDescripciones ?>
        </table>
        <div id="renglon2" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant2" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant2" /></td><td align="left"><input name="desc2" type="text" size="65" maxlength="200" id="desc2" /></td><td align="right" width="80"><input name="precio2" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio2" /></td><td align="center" width="150"><input name="importe2" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe2" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon3" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant3" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant3" /></td><td align="left"><input name="desc3" type="text" size="65" maxlength="200" id="desc3" /></td><td align="right" width="80"><input name="precio3" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio3" /></td><td align="center" width="150"><input name="importe3" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe3" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon4" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant4" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant4" /></td><td align="left"><input name="desc4" type="text" size="65" maxlength="200" id="desc4" /></td><td align="right" width="80"><input name="precio4" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio4" /></td><td align="center" width="150"><input name="importe4" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe4" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon5" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant5" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant5" /></td><td align="left"><input name="desc5" type="text" size="65" maxlength="200" id="desc5" /></td><td align="right" width="80"><input name="precio5" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio5" /></td><td align="center" width="150"><input name="importe5" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe5" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon6" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant6" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant6" /></td><td align="left"><input name="desc6" type="text" size="65" maxlength="200" id="desc6" /></td><td align="right" width="80"><input name="precio6" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio6" /></td><td align="center" width="150"><input name="importe6" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe6" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon7" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant7" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant7" /></td><td align="left"><input name="desc7" type="text" size="65" maxlength="200" id="desc7" /></td><td align="right" width="80"><input name="precio7" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio7" /></td><td align="center" width="150"><input name="importe7" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe7" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon8" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant8" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant8" /></td><td align="left"><input name="desc8" type="text" size="65" maxlength="200" id="desc8" /></td><td align="right" width="80"><input name="precio8" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio8" /></td><td align="center" width="150"><input name="importe8" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe8" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon9" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant9" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant9" /></td><td align="left"><input name="desc9" type="text" size="65" maxlength="200" id="desc9" /></td><td align="right" width="80"><input name="precio9" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio9" /></td><td align="center" width="150"><input name="importe9" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe9" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon10" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant10" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant10" /></td><td align="left"><input name="desc10" type="text" size="65" maxlength="200" id="desc10" /></td><td align="right" width="80"><input name="precio10" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio10" /></td><td align="center" width="150"><input name="importe10" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe10" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon11" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant11" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant11" /></td><td align="left"><input name="desc11" type="text" size="65" maxlength="200" id="desc11" /></td><td align="right" width="80"><input name="precio11" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio11" /></td><td align="center" width="150"><input name="importe11" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe11" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon12" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant12" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant12" /></td><td align="left"><input name="desc12" type="text" size="65" maxlength="200" id="desc12" /></td><td align="right" width="80"><input name="precio12" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio12" /></td><td align="center" width="150"><input name="importe12" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe12" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon13" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant13" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant13" /></td><td align="left"><input name="desc13" type="text" size="65" maxlength="200" id="desc13" /></td><td align="right" width="80"><input name="precio13" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio13" /></td><td align="center" width="150"><input name="importe13" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe13" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon14" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant14" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant14" /></td><td align="left"><input name="desc14" type="text" size="65" maxlength="200" id="desc14" /></td><td align="right" width="80"><input name="precio14" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio14" /></td><td align="center" width="150"><input name="importe14" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe14" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon15" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant15" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglon(event,this);" id="cant15" /></td><td align="left"><input name="desc15" type="text" size="65" maxlength="200" id="desc15" /></td><td align="right" width="80"><input name="precio15" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonP(event,this);" id="precio15" /></td><td align="center" width="150"><input name="importe15" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe15" /></td>
          </tr>
        </table>
        </div>
  	</td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">SUBTOTAL</td><td align="right">$ <?php echo number_format($subtotal,2,".",",") ?></td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">IVA</td><td align="right">$ <?php echo number_format($iva,2,".",",") ?></td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">TOTAL</td><td align="right">$ <?php echo number_format($total,2,".",",") ?></td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">RETENCION:</td><td align="right">$ <?php echo number_format($factura['retenciones'],2,".",",") ?></td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">TOTAL</td><td align="right" class="horaVerde">$ <?php echo number_format($granTotal,2,".",",") ?></td>
  </tr>
  <tr>
    <td class="tituloVentana" colspan="5">PARTIDA(S) AFECTADA(S)</td>
  </tr>
  <tr>
  	<td class="nombrePaciente">PARTIDA</td><td class="nombrePaciente">DESCRIPCION</td><td class="nombrePaciente" align="center">SALDO</td>
  	<td class="nombrePaciente" align="center">CARGADO A LA PARTIDA</td>
  </tr>
<?php echo $datosPartidas ?>
  <tr>
    <td colspan="5" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicioGen.php');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="button" name="agregar" id="agregar" value="Reimprimir Documentos" class="botones" onclick="javascript:reimprimirDocumentos(<?php echo $_GET['id_factura'] ?>);" />
  <br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>

</body>
</html>
