<?php require_once('Connections/bdissste.php'); ?>
<?php
session_start ();

function getIps() {
	$res = "";
	if($_SERVER["HTTP_X_FORWARDED_FOR"])
	{
		if($pos=strpos($_SERVER["HTTP_X_FORWARDED_FOR"]," "))
		{
			$res = "IP local: ".substr($_SERVER["HTTP_X_FORWARDED_FOR"],0,$pos)." - IP Publica: ".substr($_SERVER["HTTP_X_FORWARDED_FOR"],$pos+1);
			$hostlocal=substr($_SERVER["HTTP_X_FORWARDED_FOR"],$pos+1);
		}else{
			$res = "IP Publica: ".$_SERVER["HTTP_X_FORWARDED_FOR"];
			$hostlocal=$_SERVER["HTTP_X_FORWARDED_FOR"];
		}
		if($_SERVER["REMOTE_ADDR"])
			$res .= " - Proxy: ".$_SERVER["REMOTE_ADDR"];
	}else{
		$res = "IP Publica: ".$_SERVER["REMOTE_ADDR"];
		$hostlocal=$_SERVER["REMOTE_ADDR"];
		if($hostlocal!=$_SERVER["REMOTE_ADDR"])
			$res .= " - Hostname: ".$hostlocal;
	}
	$hostname=gethostbyaddr($hostlocal);
	if($hostlocal!=$hostname)
		$res .= " - Hostname: ".$hostname;
	return $res;
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$usuario_hacerLogin = "-1";
if (isset($_POST['usuario'])) {
  $usuario_hacerLogin = $_POST['usuario'];
}
$password_hacerLogin = "-1";
if (isset($_POST['pass'])) {
  $password_hacerLogin = $_POST['pass'];
}
$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
mysql_select_db($database_bdissste, $bdissste);
$query_hacerLogin = sprintf("SELECT * FROM usuarios WHERE login = %s and pass = %s and status = '1'", GetSQLValueString($usuario_hacerLogin, "text"),GetSQLValueString($password_hacerLogin, "text"));
$hacerLogin = mysql_query($query_hacerLogin, $bdissste) or die(mysql_error());
$row_hacerLogin = mysql_fetch_assoc($hacerLogin);
$totalRows_hacerLogin = mysql_num_rows($hacerLogin);


mysql_free_result($hacerLogin);
//mysql_close($dbissste);
header("Content-type:text/xml");
if ($totalRows_hacerLogin == 1) {
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "INSERT INTO logs values('" . date("Ymd") . "','" . date("Hi") . "','" . $row_hacerLogin['id_usuario'] . "','" . getIps() . "','ingreso al sistema')";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$_SESSION['idUsuario'] = $row_hacerLogin['id_usuario'];
	$_SESSION['tipoUsuario'] = $row_hacerLogin['tipo_usuario'];
	switch($row_hacerLogin['tipo_usuario']) {
		case '0':$out = "inicioGen.php";  //generador de gastos
				break;
		case '1':$out = "inicioPre.php";  //presupuestos
				break;
		case '2':$out = "inicioCaj.php";  //cajas
				break;
		case '3':$out = "Sera direccionado a http://www.issstezapopan.net/admin para poder ingresar al sistema";  //administrador
				break;
		case '4':$out = "inicioVis.php";  //visor
				break;
		case '5':$out = "inicioCon.php";  //el que hace administra los contratos
				break;
	}
	
	print "1|" . $row_hacerLogin['nombre'] . "|" . $row_hacerLogin['tipo_usuario'] . "|" . $out;
} else {
	$query_query = "INSERT INTO logs values('" . date("Ymd") . "','" . date("Hi") . "','-1','" . getIps() . "','intento de ingreso al sistema fallido')";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	print "0|-1|-1|Nombre de Usuario o Contraseña Incorrectos";
}
?>