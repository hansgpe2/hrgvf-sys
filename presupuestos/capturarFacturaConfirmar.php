<?php session_start ();
include_once('lib/misFunciones.php');



$sqls = array();
$sqlsMontos = array();
$haySaldo = true;
$id_contrato = $_GET['id_contrato'];
for ($i = 0; $i < 10; $i++) {
	if (isset($_GET['partida'.$i])) {
		$id_partida = $_GET['partida'.$i];
		$cargo_partida = $_GET['cargoPartida'.$i];
		$partida = getPartidaAcargar($id_contrato, $id_partida);
		if ($partida['monto_x_ejercer'] < $cargo_partida) { // no se puede cargar una partida por falta de saldo
			$haySaldo = false;
		} else { // se genera el sql para cargar las partidas mas adelante, no se cargan aqu� porque debemos comprobar que en todas las partidas haya saldo
			$saldo_restante = $partida['monto_x_ejercer'] - $cargo_partida;
			$sqls[] = "UPDATE contratos_partidas SET monto_x_ejercer='" . $saldo_restante . "' WHERE id_partida='" . $id_partida . "' AND id_contrato='" . $id_contrato . "'";
			$sqlsMontos[] = "INSERT INTO partidas_montos_facturados VALUES('','" . $id_contrato . "','" . $_GET['id_proveedor'] . "','***id_factura***','" . $id_partida . "','" . $cargo_partida . "','')";
		}
	}
}

if ($haySaldo) {
	$duplica = existeDuplica("SELECT * FROM facturas WHERE id_proveedor='" . $_GET['id_proveedor'] . "' AND folio='" . $_GET['factura'] . "'");
	if (!$duplica) { // NO hay una factura con el mismo numero del proveedor
		$fechaEnFactura = formatoFechaBD($_GET['fecha']);
		$fecha = date("Ymd");
		$iva = $_GET['subtotal'] * .16;
		$sql = "INSERT INTO facturas VALUES('','" . $id_contrato . "','" . $_GET['id_proveedor'] . "','" . $_GET['factura'] . "','" . $fechaEnFactura . "','" . $fecha . "','" . $_GET['subtotal'] . "','" . $_GET['vretenciones'] . "','" . $_GET['retenciones'] . "','" . $iva . "','" . $_GET['total'] . "','" . $_GET['fechaInicioReal'] . "|" . $_GET['fechaTerminoReal'] . "|" . $_GET['diasRetraso'] . "');";
		$res = ejecutarSQL($sql);
		$id_factura = getFacturaRecianAgregada($id_contrato, $_GET['id_proveedor'], $_GET['factura']);
		for ($i = 1; $i <= 15; $i++) {
			if ((is_numeric($_GET['cant'.$i])) && (strlen($_GET['desc'.$i])>0) && (is_numeric($_GET['prec'.$i])) && (is_numeric($_GET['impo'.$i]))) { // es un rengl�n lleno v�lido
				$sqls[] = "INSERT INTO facturas_conceptos VALUES('','" . $id_factura . "','" . $_GET['cant'.$i] . "','" . $_GET['desc'.$i] . "','" . $_GET['prec'.$i] . "','" . $_GET['impo'.$i] . "','');";
				$concepto = getConceptoContratoXid($_GET['concepto'.$i],"actuales");
				$canti = $concepto['cantidad'] - $_GET['cant'.$i];
				$impo = $concepto['importe'] - $_GET['impo'.$i];
				$sqls[] = "UPDATE contratos_conceptos_actuales SET cantidad='" . $canti . "', importe='" . $impo . "' WHERE id_concepto='" . $_GET['concepto'.$i] . "' LIMIT 1";
			}
		}
		$montoXejercerCto = $_GET['montoXejercerCto'];
		$montoRestanteXcto = $montoXejercerCto - $_GET['total'];
		$sqls[] = "UPDATE contratos SET monto_x_ejercer='" . $montoRestanteXcto . "' WHERE id_contrato='" . $id_contrato . "' LIMIT 1";
		$sqls[] = "INSERT INTO facturas_movimientos VALUES('','" . $id_factura . "','0','1','" . date("Ymd") . "','" . date("Hi") . "','" . $_SESSION['idUsuario'] . "','');";
		$tSqls = count($sqls);
		for($i=0; $i < $tSqls; $i++) {
			$res = ejecutarSQL($sqls[$i]);
		}
		$tsqls = count($sqlsMontos);
		for($i=0; $i < $tSqls; $i++) {
			$sqlActual = str_replace("***id_factura***",$id_factura,$sqlsMontos[$i]);
			$res = ejecutarSQL($sqlActual);
		}
	print($id_factura);
	} else { // existe duplica del n. de factura para id_proveedor
		print("Ya existe una factura del proveedor con el mismo n&uacute;mero");
	}
}


/*

$duplica = existeDuplica("SELECT * FROM contratos WHERE num_contrato='" . $_GET['n_contrato'] . "'");
if (!$duplica) {
	$query_query = "INSERT INTO logs values('" . date("Ymd") . "','" . date("Hi") . "','" . $_SESSION['idUsuario'] . "','" . getIps() . "','agreg� contrato n. " . $_GET['n_contrato'] . "')";
	$res = ejecutarSQL($query_query);
	
	$query_query = "INSERT INTO contratos VALUES(NULL,'" .  $_GET['n_contrato'] . "','" .  $_GET['contrato'] . "','" . $_GET['tipo_contrato'] . "','" . $_GET['id_proveedor'] . "','" . $_GET['f_i'] . "','" . $_GET['f_t'] . "','" . $_GET['descripcion'] . "','" . $_GET['monto'] . "','0','" . $_GET['monto'] . "','" . $_GET['id_departamento'] . "','');";
	$res = ejecutarSQL($query_query);
	if ($res[0] == 0) {// no hay error
		$id_contrato = getContratoRecienAgregado($_GET['n_contrato']);
		if ($_GET['partida1'] <> "-1") {
			$aPartida = explode("|",$_GET["partida1"]);
			$cantidad = $_GET["cantidad1"];
			$query_query = "INSERT INTO contratos_partidas VALUES('" . $aPartida[0] . "','" .  $id_contrato . "','" .  $cantidad . "','" .  $cantidad . "','');";
			$res = ejecutarSQL($query_query);
			$partida = getPartidaXid($aPartida[0]);
			$resto = $partida['extra'] + $cantidad;
			$query_query = "UPDATE partidas SET extra='" . $resto . "' WHERE id_partida='" . $aPartida[0] . "' LIMIT 1;";
			$res = ejecutarSQL($query_query);
		}
		if ($_GET['partida2'] <> "-1") {
			$aPartida = explode("|",$_GET["partida2"]);
			$cantidad = $_GET["cantidad2"];
			$query_query = "INSERT INTO contratos_partidas VALUES('" . $aPartida[0] . "','" .  $id_contrato . "','" .  $cantidad . "','" .  $cantidad . "','');";
			$res = ejecutarSQL($query_query);
			$partida = getPartidaXid($aPartida[0]);
			$resto = $partida['extra'] + $cantidad;
			$query_query = "UPDATE partidas SET extra='" . $resto . "' WHERE id_partida='" . $aPartida[0] . "' LIMIT 1;";
			$res = ejecutarSQL($query_query);
		}
		if ($_GET['partida3'] <> "-1") {
			$aPartida = explode("|",$_GET["partida3"]);
			$cantidad = $_GET["cantidad3"];
			$query_query = "INSERT INTO contratos_partidas VALUES('" . $aPartida[0] . "','" .  $id_contrato . "','" .  $cantidad . "','" .  $cantidad . "','');";
			$res = ejecutarSQL($query_query);
			$partida = getPartidaXid($aPartida[0]);
			$resto = $partida['extra'] + $cantidad;
			$query_query = "UPDATE partidas SET extra='" . $resto . "' WHERE id_partida='" . $aPartida[0] . "' LIMIT 1;";
			$res = ejecutarSQL($query_query);
		}
		print("1");
	
	} else { // hay error en el mysql
		print($query_query);
	}
} else {
		print("-2");
}

*/
?>
