<?php
include_once('lib/misFunciones.php');
session_start ();

$contrato = getContratoXid($_GET['id_contrato']);
$proveedor = getProveedorXid($contrato['id_proveedor']);
$departamento = getDepartamentoXid($contrato['id_departamento']);
$partidas = getPartidasXcontrato($contrato['id_contrato']);
$tPartidas = count($partidas);
$datosPartidas = "";
for($i=0; $i<$tPartidas; $i++) {
	$partida = getPartidaXid($partidas[$i]['id_partida']);
	$datosPartidas .= $partida['partida'] . "-" .$partida['subpartida'] . "-" . $partida['subsubpartida'] . "-" . $partida['nombre'] . " --- AFECTADA CON: $" . number_format($partidas[$i]['monto_total'],2,".",",") . "<br>";
}
$conceptosIniciales = getConceptosContrato($contrato['id_contrato'],"originales");
$conceptosActuales = getConceptosContrato($contrato['id_contrato'],"actuales");
$tConceptos = count($conceptosIniciales);
$vIniciales = "<table width=\"100%\" border=\"0\">";
$vActuales = "<table width=\"100%\" border=\"0\">";
for($i=0; $i<$tConceptos; $i++) {
	$vIniciales .= "<tr><td>" . $conceptosIniciales[$i]['cantidad'] . "</td><td>" . $conceptosIniciales[$i]['descripcion'] . "</td><td>" . $conceptosIniciales[$i]['precio_unitario'] . "</td><td>" . $conceptosIniciales[$i]['importe'] . "</td></tr>";
	$vActuales .= "<tr><td>" . $conceptosActuales[$i]['cantidad'] . "</td><td>" . $conceptosActuales[$i]['descripcion'] . "</td><td>" . $conceptosActuales[$i]['precio_unitario'] . "</td><td>" . $conceptosActuales[$i]['importe'] . "</td></tr>";
}
$vIniciales .= "</table><br>";
$vActuales .= "</table>";

$montoProceso = $contrato["monto_total"] - $contrato["monto_x_ejercer"];
$montoXejercer = $contrato["monto_total"] - $montoProceso;
$temp = "<table width=\"100%\" cellspaccing=\"0\" cellpadding=\"0\" style=\"border-color:#333; border-style:solid; border-width:1px\"><tr><td class=\"tituloVentana\" height=\"23\" colspan=\"2\">DETALLES DEL CONTRATO: " . $contrato['num_contrato'] . "</td></tr>";
$temp .= "<tr><th width=\"220\" align=\"right\">DESCRIPCION:</th><td>" . $contrato["descripcion_contrato"] . "</td></tr>
		  <tr><th align=\"right\">CONTRATO:</th><td>" . $arregloContrato[$contrato["contrato"]] . "</td></<tr>
		  <tr><th align=\"right\">TIPO DE CONTRATO:</th><td>" . $arregloTipoContrato[$contrato["tipo_contrato"]] . "</td></<tr>
		  <tr><th align=\"right\">DATOS DEL<br>PROVEEDOR:</th><td>" . $proveedor['razon_social'] . "<br>" . $proveedor['nombre_contacto'] . " - " . $proveedor['telefono_contacto'] . "<br>" . $proveedor['email_contacto'] . "</td></<tr>
		  <tr><th align=\"right\">DEPARTAMENTO:</th><td> " . $departamento["nombre"] . "</td></tr>
		  <tr><th align=\"right\">FECHA DE INICIO:</th><td>" . formatoDia($contrato["fecha_inicio"],"fechaI") . "</td></<tr>
		  <tr><th align=\"right\">FECHA DE TERMINACION:</th><td>" . formatoDia($contrato["fecha_terminacion"],"fechaI") . "</td></tr>
		  <tr><th align=\"right\">MONTO TOTAL:</th><td>$ " . number_format($contrato["monto_total"],2,".",",") . "</td></tr>
		  <tr><th align=\"right\">MONTO EN PROCESO:</th><td>$ " . number_format($montoProceso,2,".",",") . "</td></tr>
		  <tr><th align=\"right\">MONTO EJERCIDO:</th><td>$ " . number_format($contrato["monto_ejercido"],2,".",",") . "</td></tr>
		  <tr><th align=\"right\">MONTO POR EJERCER:</th><td>$ " . number_format($montoXejercer,2,".",",") . "</td></tr>
		  <tr><th align=\"right\">PARTIDAS AFECTADAS:</th><td>" . $datosPartidas . "</td></tr>
		  <tr><th align=\"right\">CONCEPTOS ORIGINALES DEL CONTRATO:</th><td>" . $vIniciales . "</td></tr>
		  <tr><th align=\"right\">CONCEPTOS ACTUALES DEL CONTRATO:</th><td>" . $vActuales . "</td></tr>";
$temp .= "</table><br><p><a href=\"javascript:inicio('inicioGen.php');\"><img src=\"diseno/flechaIzq.png\" border=\"0\"></a></p><br>aqui poner datos de las facturas del contrato";
print($temp);
?>