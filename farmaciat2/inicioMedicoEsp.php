<?php

set_time_limit(180);
date_default_timezone_set('America/Mexico_City');
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');
if (!isset($_SESSION[$galleta_citas])) {
	header('Location: index.php');
}

$out = '';
if (count($_SESSION[$galleta_enca]) == 0) {
	$datosUsuario = getUsuarioXid($_SESSION['idUsuario']);
//	setcookie("idMedico", $_SESSION['idUsuario']);
	$datosMedico = getMedicoXid($datosUsuario["id_medico"]);
//	$dts = regresarServicioConsultorio($datosUsuario["id_medico"]);
	$id_servicio = $_SESSION['idServ'];
	$id_consultorio = $_SESSION['IdCon'];
	$servicio = getServicioXid($id_servicio);
	$consultorio = getConsultorioXid($id_consultorio);
	$_SESSION['idDr'] = $datosUsuario["id_medico"];
	$_SESSION['idServ'] = $id_servicio;
	$_SESSION['IdCon'] = $id_consultorio;
	$out = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"center\" class=\"tituloVentana2013\">";
	$out.= "CITAS DE " . ponerAcentos(strtoupper($datosMedico['titulo'] . " " . $datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']) . "<br>");
	$out.= "Consultorio: " . strtoupper($consultorio['nombre']) . " | Servicio: " . strtoupper($servicio);
	$out .= "</td></tr></table><br>";
	$_SESSION[$galleta_enca] = $out;
//	setcookie($galleta_enca, $out, time() + 24 * 60 * 60, '/'); 
} else {
	$out = $_SESSION[$galleta_enca];
}

//$contra = true;
print($out);

$hoy = date('Ymd');
?>
<center>
    <table width="800" border="0">
        <tr>
            <td width="687" valign="top">
                <table width="630" border="0" cellspacing="0" cellpadding="0" class="ventana">
                    <tr>
                        <td class="tituloVentana" height="23"><?php echo formatoDia($hoy, 'tituloCitasXdia'); ?></td>
                    </tr>
                    <tr>
                        <td align="center"><br />
							<?php
								$out = "";
								$datosCita = getDetallesCitaAgendaInicio($hoy, $_SESSION['idDr'], 'N');
								$tCitas = count($datosCita);
								if ($tCitas > 0) {
									$out.= "<table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
									for ($i=0; $i<$tCitas; $i++) {
										$clasSI = '';
										$clasNO = '';
										$disableNO = '';
										$asistio = $_SESSION[$galleta_citas]['N'.$datosCita[$i]['id_cita']]['asistio'];
										$id_horario = $_SESSION[$galleta_citas]['N'.$datosCita[$i]['id_cita']]['id_horario'];
										$claseParaDia = $_SESSION[$galleta_citas]['N'.$datosCita[$i]['id_cita']]['clase'];
										$hora_inicio = $_SESSION[$galleta_citas]['N'.$datosCita[$i]['id_cita']]['hora_inicio'];
										$hora_fin = $_SESSION[$galleta_citas]['N'.$datosCita[$i]['id_cita']]['hora_fin'];
										$nombre = $_SESSION[$galleta_citas]['N'.$datosCita[$i]['id_cita']]['nombre'];
										$datos = $_SESSION[$galleta_citas]['N'.$datosCita[$i]['id_cita']]['datos'];
										if ($asistio == 'SI') {
											$clasSI = ' r_on';
											$clasNO = 'dis';
											if (($datosCita[$i]['hizo_contra'] != '') || ($datosCita[$i]['fecha_cita'] != '') || ($datosCita[$i]['hizo_constancia'] != '')) {
												$disableNO = ' disabled="disabled" ';
											}
										}
										if ($asistio == 'NO') {
											$clasNO = ' r_off';
										}
										$botones = 'ASISTIO: 
											<label class="label_radio' . $clasSI . '" for="asistio_SI_N_' . $datosCita[$i]['id_cita'] . '">
												<input name="sino_' . $datosCita[$i]['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $datosCita[$i]['id_derecho'] . '_' . $id_horario . '" id="asistio_SI_N_' . $datosCita[$i]['id_cita'] . '" value="SI" type="radio" class="si_no" />
												SI
											</label>
											<label class="label_radio' . $clasNO . '" for="asistio_NO_N_' . $datosCita[$i]['id_cita'] . '">
												<input name="sino_' . $datosCita[$i]['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $datosCita[$i]['id_derecho'] . '_' . $id_horario . '" id="asistio_NO_N_' . $datosCita[$i]['id_cita'] . '" value="NO" type="radio" class="si_no"' . $disableNO . ' />
											   NO
											</label>';
										$out.="
											  <tr>
												<td class=\"" . $claseParaDia . "\">
													<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
													  <tr>
														<td width=\"100\" class=\"citaXdiaHora\">" . formatoHora($hora_inicio) . " - " . formatoHora($hora_fin) . "</td>
														<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
														<td width=\"180\" rowspan=\"2\" class=\"citaXdiaHora\"><div id=\"div_btnN_" . $datosCita[$i]['id_cita'] . "\" style=\"position:relative; top:10px;\">" . $botones . "</div></td>
													  </tr>
													  <tr>
														<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($datos) . "</td>
													  </tr>
													</table>
												</td>
											  </tr>";
									}
									$out .= '</table>';
								} else {
									$out .= '<p align="center" class="textosParaInputs">NO HAY CITAS DISPONIBLES PARA ESTE DIA<br><br></p>';
								}
                            echo $out;
							?>
                            <br />
                            <div id="estadoEliminando"></div></td>
                    </tr>
                </table>
                <p align="center"><input type="button" value="CERRAR CONSULTA E IMPRIMIR SM-1-10" class="boton g naranja" onclick="cerrarAgendaMedico('<?php echo $_SESSION['idUsuario'] ?>', '<?php echo $_SESSION['idServ'] ?>');" /></p>
                <table width="500" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">
                    <tr>
                        <td width="35" height="25" class="citaXdiaPRV"></td>
                        <td width="129">Citas Primera Vez</td>
                        <td width="35" class="citaXdiaSUB"></td>
                        <td width="138">Citas Subsecuentes</td>
                        <td width="35" class="citaXdiaPRO"></td>
                        <td width="138">Citas Procedimientos</td>
                        <td width="100"><div id="CanCitas">
                                <a href="javascript:EliminarCitas();" class="botones_menu" title="Cancelar Cita"><img src="diseno/cancelarCitaMedico.png"  width="48" height="48"/>Cancelar Cita</a></div></td>
                    </tr>
                </table>
                </p>
<?php

$tCitas = 0;
$out = "<br>";
$out.= "
<table width=\"630\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
<tr>
  <td class=\"tituloVentana\" height=\"23\">" . formatoDia($hoy, 'tituloCitasXdia') . " - EXTEMPORANEAS</td>
</tr>
";


								$datosCita = getDetallesCitaAgendaInicio($hoy, $_SESSION['idDr'], 'E');
								$tCitas = count($datosCita);
								if ($tCitas > 0) {
									for ($i=0; $i<$tCitas; $i++) {
										$clasSI = '';
										$clasNO = '';
										$disableNO = '';
										$asistio = $_SESSION[$galleta_citasE]['E'.$datosCita[$i]['id_cita']]['asistio'];
										$id_horario = $_SESSION[$galleta_citasE]['E'.$datosCita[$i]['id_cita']]['id_horario'];
										$claseParaDia = $_SESSION[$galleta_citasE]['E'.$datosCita[$i]['id_cita']]['clase'];
										$hora_inicio = $_SESSION[$galleta_citasE]['E'.$datosCita[$i]['id_cita']]['hora_inicio'];
										$hora_fin = $_SESSION[$galleta_citasE]['E'.$datosCita[$i]['id_cita']]['hora_fin'];
										$nombre = $_SESSION[$galleta_citasE]['E'.$datosCita[$i]['id_cita']]['nombre'];
										$datos = $_SESSION[$galleta_citasE]['E'.$datosCita[$i]['id_cita']]['datos'];
										if ($asistio == 'SI') {
											$clasSI = ' r_on';
											$clasNO = 'dis';
											if (($datosCita[$i]['hizo_contra'] != '') || ($datosCita[$i]['fecha_cita'] != '') || ($datosCita[$i]['hizo_constancia'] != '')) {
												$disableNO = ' disabled="disabled" ';
											}
										}
										if ($asistio == 'NO') {
											$clasNO = ' r_off';
										}
										$botones = 'ASISTIO: 
											<label class="label_radio' . $clasSI . '" for="asistio_SI_N_' . $datosCita[$i]['id_cita'] . '">
												<input name="sino_' . $datosCita[$i]['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $datosCita[$i]['id_derecho'] . '_' . $id_horario . '" id="asistio_SI_N_' . $datosCita[$i]['id_cita'] . '" value="SI" type="radio" class="si_noE" />
												SI
											</label>
											<label class="label_radio' . $clasNO . '" for="asistio_NO_N_' . $datosCita[$i]['id_cita'] . '">
												<input name="sino_' . $datosCita[$i]['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $datosCita[$i]['id_derecho'] . '_' . $id_horario . '" id="asistio_NO_N_' . $datosCita[$i]['id_cita'] . '" value="NO" type="radio" class="si_noE"' . $disableNO . ' />
											   NO
											</label>';
										$out.="
											  <tr>
												<td class=\"" . $claseParaDia . "\">
													<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
													  <tr>
														<td width=\"100\" class=\"citaXdiaHora\">" . formatoHora($hora_inicio) . " - " . formatoHora($hora_fin) . "</td>
														<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
														<td width=\"180\" rowspan=\"2\" class=\"citaXdiaHora\"><div id=\"div_btnN_" . $datosCita[$i]['id_cita'] . "\" style=\"position:relative; top:10px;\">" . $botones . "</div></td>
													  </tr>
													  <tr>
														<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($datos) . "</td>
													  </tr>
													</table>
												</td>
											  </tr>";
									}
								} else {
									$out .= '<p align="center" class="textosParaInputs">NO HAY CITAS DISPONIBLES PARA ESTE DIA<br><br></p>';
								}
$out .= "</table>";
if ($tCitas > 0)
    echo $out;
	
?>
            </td>
        </tr>
    </table>
</center>
