<!Doctype html>
<html>
    <head>
        <link href="lib/misEstilos.css" rel="stylesheet" type="text/css">
        <script src="lib/arreglos.js"></script>
    <head>
    </head>
    <body>
        <input type="hidden" id="diagnostico_seleccionado" name="diagnostico_seleccionado" value="" />
        <?php
        include('lib/misFunciones.php');
        if ($_GET['tipo_cita'] == 'N') {
            $id_horario = $_GET['idHorario'];
            $id_cita = getCita($id_horario, date('Ymd'));
            $derecho = getDatosDerecho($id_cita["id_derecho"]);
            $id_servicio = regresarIdServicio($_SESSION["idDr"]);
            $servicio = getServicioXid($id_servicio);
            $medico = getMedicoXid($_SESSION["idDr"]);
        ?>
        <div align="center">
                <table width="800" border="0" class="tablaPrincipal">   
                    <tr>
                        <td colspan="2" class="tituloVentana">Diagnóstico del paciente</td>

                    </tr>
                    <tr>
                        <td width="300" align="right" class="textosParaInputs">C&eacute;dula</td>
                        <td width="488"><?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo']; ?></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Nombre</td>
                        <td><?php echo ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']); ?>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Fecha</td>
                        <td><?php echo date("d/m/Y"); ?>&nbsp;
                            <input name="fecha" type="hidden" id="hiddenField3" value="<?php echo date("Ymd"); ?>"></td>
                    </tr>
                    <tr>
                      <td height="25" class="textosParaInputs" align="right" valign="top">DIAGNOSTICO:</td>
                      <td align="left" valign="top"><input type="text" cols="150" id="diagnostico" name="diagnostico" value="" onkeydown="return sinTab(event, this);" ></td>
                    </tr>
                    <tr>
                      <td height="25" class="textosParaInputs" align="right" valign="top"><small><small><a href='#' onclick="javascript:$('#div_otro_diagnostico').css('display','block')">OTRO DIAGNOSTICO:</a>:</small></small></td>
                      <td align="left" valign="top"><div id="div_otro_diagnostico" style="display:none;"><input type="text" id="diagnostico2" name="diagnostico2" cols="150" value="" onkeydown="return sinTab(event, this);" ></div></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Medico</td>
                        <td><?php echo $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']; ?><input name="medico" type="hidden" value="<?php echo $medico['id_medico']; ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input name="idDerecho" type="hidden" id="idDerecho" value="<?php echo $id_cita['id_derecho']; ?>" ><input name="idServicio" type="hidden" id="idServicio" value="<?php echo $id_servicio; ?>" ></td>

                    </tr>
                    <tr>
                        <td>
                           
                    </tr>
        </div>
<?php 

        } else if (isset($_GET['id_cita'])) {
            $id_cita = $_GET['id_cita'];
            $cita = getCitaExt($id_cita);
            $derecho = getDatosDerecho($cita["id_derecho"]);
            $servicio = getServicioXid($cita['id_servicio']);
            $medico = getMedicoXid($cita["id_medico"]);
            ?>
        <div align="center">
                <table width="800" border="0" class="tablaPrincipal">   
                    <tr>
                        <td colspan="2" class="tituloVentana">Diagnóstico del paciente</td>

                    </tr>
                    <tr>
                        <td width="300" align="right" class="textosParaInputs">C&eacute;dula</td>
                        <td width="488"><?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo']; ?></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Nombre</td>
                        <td><?php echo ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']); ?>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Fecha</td>
                        <td><?php echo date("d/m/Y"); ?>&nbsp;
                            <input name="fecha" type="hidden" id="hiddenField3" value="<?php echo date("Ymd"); ?>"></td>
                    </tr>
                    <tr>
                      <td height="25" class="textosParaInputs" align="right" valign="top">DIAGNOSTICO:</td>
                      <td align="left" valign="top"><input type="text" cols="150" id="diagnostico" name="diagnostico" value="" onkeydown="return sinTab(event, this);" ></td>
                    </tr>
                    <tr>
                      <td height="25" class="textosParaInputs" align="right" valign="top"><small><small><a href='#' onclick="javascript:$('#div_otro_diagnostico').css('display','block')">OTRO DIAGNOSTICO:</a>:</small></small></td>
                      <td align="left" valign="top"><div id="div_otro_diagnostico" style="display:none;"><input type="text" id="diagnostico2" name="diagnostico2" cols="150" value="" onkeydown="return sinTab(event, this);" ></div></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Medico</td>
                        <td><?php echo $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']; ?><input name="medico" type="hidden" value="<?php echo $medico['id_medico']; ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input name="idDerecho" type="hidden" id="idDerecho" value="<?php echo $cita['id_derecho']; ?>" ><input name="idServicio" type="hidden" id="idServicio" value="<?php echo $cita['id_servicio']; ?>" ></td>

                    </tr>
                    <tr>
                        <td>
                            <?php    
        }
                            if ($_SESSION['tipoUsuario'] == 8) {
                                echo "  <input type='button' name='regresar' id='regresar' value='Regresar' class='botones'  onclick='javascript: inicio(\"inicioMedicoEsp.php\");' tabindex='11' />&nbsp;&nbsp;&nbsp;&nbsp;";
                            } else if ($_SESSION['tipoUsuario'] == 4) {
                                echo "<input type='button' name='regresar' id='regresar' value='Regresar' class='botones'  onclick='javascript: inicio(\"inicioMedico.php\");' tabindex='11' />&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                            ?>
                            <input type="button" onClick="genDiagnostico('<?php echo $_REQUEST['id_cita'] ?>','<?php echo $_REQUEST['tipo_cita'] ?>');" id="agregar" class="botones" value="Guardar"></td>
                    </tr>
                </table>
        </div>
       

    </body>
</html>