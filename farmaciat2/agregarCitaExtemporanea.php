<?php
include_once('lib/misFunciones.php');
if($_SESSION['idUsuario'] > 0) { 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarAgregarCitaExtemporanea2014();">
<input type="hidden" id="diagnostico_seleccionado" name="diagnostico_seleccionado" value="" />
<input name="id_derecho" id="id_derecho" type="hidden" value="<?php echo $_SESSION['paciente']; ?>" />
<input name="fechaCita" id="fechaCita" type="hidden" value="<?php echo $_GET['getdate']; ?>" />
<input name="id_cita" id="id_cita" type="hidden" value="" />
<input name="tipo_reprogramacion" id="tipo_reprogramacion" type="hidden" value="" />

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CITA EXTEMPORANEA PARA EL <?php echo formatoDia($_GET['getdate'], 'tituloCitasXdia'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">HORA INICIO:</td>
    <td align="left"><input name="hora_inicio" type="text" id="hora_inicio" size="10" maxlength="5" />
    <span class="textosParaInputs">HORA FIN:</span> <input name="hora_fin" type="text" id="hora_fin" size="10" maxlength="5" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CODIGO DE BARRAS: </td>
    <td align="left"><input id="cod_bar" name="cod_bar" type="text" value="<?php echo ponerCeros($_SESSION['paciente'],8); ?>" maxlength="8" readonly="readonly" /></td>
  </tr>
  <tr>
  <?php 
  		$derecho=getDatosDerecho($_SESSION['paciente']);
		?>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><input name="cedula" type="text" id="cedula" maxlength="13" readonly="readonly" value="<?php echo $derecho['cedula']."/".$derecho['cedula_tipo']; ?>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_p" type="text" id="ap_p" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($derecho['ap_p']); ?>" />
      <input name="ap_m" type="text" id="ap_m" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($derecho['ap_m']); ?>" />
      <input name="nombre" type="text" id="nombre" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($derecho['nombres']); ?>"/></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><input name="telefono" type="text" id="telefono" size="20" maxlength="10" readonly="readonly" value="<?php echo $derecho['telefono']; ?>"/><span class="textosParaInputs"> FECHA NAC. </span><input name="fecha_nac" type="text" id="fecha_nac" size="20" maxlength="10" readonly="readonly" value="<?php echo $derecho['fecha_nacimiento'];?>" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccion" type="text" id="direccion" size="64" maxlength="50" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($derecho['direccion']); ?>" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><input type="text" name="estado" id="estado" value="<?php echo ponerAcentos($derecho['estado']); ?>" readonly="readonly"/> 
    <span class="textosParaInputs">MUNICIPIO: </span><input type="tel" name="municipio" id="municipio" value="<?php echo ponerAcentos($derecho['municipio']); ?>" readonly="readonly"/>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><div id="modificarDH" style="display:'';"><input type="button" name="modificar" id="modificar" value="Modificar Datos del Derechohabiente" class="botones" disabled="disabled" onclick="javascript: habilitarParaModificarDH();" /></div><div id="modificarDHGuardar" style="display:none"><input name="guardarMod" type="button" value="Guardar Modificaciones" class="botones" onclick="javascript: guardarModificacionesDH();" /> <input name="cancelarMod" type="button" value="Cancelar" class="botones" onclick="javascript: cancelarModificacionesDH();" /></div></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">TIPO DE CITA:</td>
    <td align="left"><select name="tipo_cita" id="tipo_cita">
      <option value="-1" selected="selected"></option>
      <!-- <option value="0">Primera Vez</option> -->
      <option value="1">Subsecuente</option>
      <option value="2">Procedimiento</option>
      <!-- <option value="3">Administrativo</option>
      <option value="4">Telemedicina</option> -->
    </select></td>
  </tr>
      <tr>
        <td class="textosParaInputs" align="right" colspan="2">&nbsp;</td>
      </tr>
<tr>
  <td height="25" class="textosParaInputs" align="right" valign="top">DIAGNOSTICO:</td>
  <td align="left" valign="top"><input type="text" cols="150" id="diagnostico" name="diagnostico" value="" onkeydown="return sinTab(event, this);" ></td>
</tr>
<tr>
  <td height="25" class="textosParaInputs" align="right" valign="top"><small><small><a href='#' onclick="javascript:$('#div_otro_diagnostico').css('display','block')">OTRO DIAGNOSTICO:</a>:</small></small></td>
  <td align="left" valign="top"><div id="div_otro_diagnostico" style="display:none;"><input type="text" id="diagnostico2" name="diagnostico2" cols="150" value="" onkeydown="return sinTab(event, this);" ></div></td>
</tr>
  <tr>
    <td class="textosParaInputs" align="right">OBSERVACIONES:</td>
    <td align="left"><input type="text" name="observaciones" id="observaciones" onkeyup="this.value = this.value.toUpperCase();" size="80" maxlength="150" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="selDia('<?php echo $_GET['getdate']; ?>','<?php echo $_GET['getdate']; ?>');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Agregar Cita" class="botones" />
  <br /><br /></td>
  </tr>
  <tr>
    <td colspan="2" align="left">
  <input type="button" name="imprimir_cb" id="imprimir_cb" value="Imprimir C&oacute;digo de barras" class="botones" onclick="javascript: imprimir_exp();" />
  </tr>
</table>
</form>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="reprogramar" style="display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">REPROGRAMAR CITA</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
            <td colspan="2">
                <form id="selDH" method="POST" action="javascript: buscarDH(document.getElementById('cedulaBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">CITA: </td>
                  <td align="left"><div id="listaCitasAreprogramar">&nbsp;</div></td>
                </tr>
                <tr>
                  <td colspan="2" align="center"><input name="cerrarReprogramar" type="button" class="botones" id="cerrarReprogramar" onclick="javascript: ocultarDiv('reprogramar');" value="Cerrar" />
                  &nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarCitaReprogramar" type="button" class="botones" id="seleccionarCitaReprogramar" onclick="javascript: cargarDatosCitaReprogramar();" value="Seleccionar Cita" />
                    <br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>


<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="buscar" style=" display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">BUSCAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
              <td><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();">
                <option value="cedula" selected="selected">C&eacute;dula</option>
                <option value="nombre">Nombre</option>
              </select>
              </td>
            </tr>
            <tr>
            <td colspan="2">
            	<div id="buscarPorCedula">
                <form id="selDH" method="POST" action="javascript: buscarDH(document.getElementById('cedulaBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                  <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                    <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="2" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
              </div>

                <div id="buscarPorNombre" style="display:none;">
                <form id="selDHN" method="POST" action="javascript: buscarDHN(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                  <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                  <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                  <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                    <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="4" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="4" align="center"></td>
                </tr>
              </table>
              </form>
              </div>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>
<form id="agregarDH" method="POST" action="javascript: agregarDHenCitaForma();">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="divAgregarDH" style=" display:none; height:0px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
              <td align="left"><input type="text" name="cedulaAgregar" id="cedulaAgregar" maxlength="10" /> / <input type="text" name="cedulaTipoAgregar" id="cedulaTipoAgregar" maxlength="2" size="5" /></td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_pAgregar" type="text" id="ap_pAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="ap_mAgregar" type="text" id="ap_mAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="nombreAgregar" type="text" id="nombreAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><input name="telefonoAgregar" type="text" id="telefonoAgregar" size="20" maxlength="10" /><span class="textosParaInputs"> FECHA NAC. </span><input name="fecha_nacAgregar" type="text" id="fecha_nacAgregar" size="20" maxlength="10" />
    </td>
</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" size="64" maxlength="50" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estadoAgregar" id="estadoAgregar" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');"> 
    </select><span class="textosParaInputs">MUNICIPIO: </span><select name="municipioAgregar" id="municipioAgregar">
    </select>
    </td>
  </tr>
        
            <tr>
              <td colspan="2" align="center"><div id="divBotones_EstadoAgregarDH"></div>
              </td>
            </tr>
        
            <tr>
              <td colspan="2" align="center"></td>
            </tr>
          </table>
        </div>
	</td>
</tr>
</table>
</form>

</body>
</html>
<?php
} else {
	$_SESSION['idUsuario'] = "-1";
	$_SESSION['tipoUsuario'] = "-1";
	$_SESSION['IdCon'] = "-1";
	$_SESSION['idServ'] = "-1";
	$_SESSION['idDr'] = "-1";
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Tu sesion ha caducado'); location.replace('index.php');</script>";
}
?>