<?php
session_start ();
include_once('lib/misFunciones.php');
	
	$out = '';
	if ($_SESSION["idDr"] > 0) {
		$folios = getFolios($_SESSION["idDr"]);
		$tFolios = count($folios);
		if ($tFolios > 0) {
			$out .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
				$out .= '<tr><th>Serie</th><th>Folio Inicial</th><th>Folio Final</th><th>Folio Actual</th><th>&nbsp;</th></tr>';
			for($i=0; $i<$tFolios; $i++) {
//				$botones = '<a title="Modificar Folios" href="javascript: ModificarFolios(' . $folios[$i]['id_folio'] . ');" class="linkReportes"><img src="diseno/Symbol-Refresh.png" border="0" alt="Modificar Folios"></a>&nbsp;&nbsp; <a title="Eliminar Folios" href="javascript: EliminarFolios(' . $folios[$i]['id_folio'] . ');" class="linkReportes"><img src="diseno/Symbol-Delete.png" border="0" alt="Eliminar Folios"></a>';
				$botones = '';
				$out .= '<tr><td align="center">' . $folios[$i]['serie'] . '</td><td align="right">' . ponerCeros($folios[$i]['folio_inicial'],7) . '</td><td align="right">' . ponerCeros($folios[$i]['folio_final'],7) . '</td><td align="right">' . ponerCeros($folios[$i]['folio_actual'],7) . '</td><td  align="center">' . $botones . '</td></tr>';
			}
			$out .= '</table>';
		} else {
			$out .= 'No hay folios asignados al Médico';
		}
?>
<center>
<br />
  <table width="600" border="0" cellspacing="0" cellpadding="0" class="ventana">
    <tr>
      <td class="tituloVentana" height="23">FOLIOS ASIGNADOS:</td>
    </tr>
    <tr>
      <td align="center" valign="top"><?php echo $out ?>
      </td>
    </tr>
    <tr>
      <td align="right" valign="top"><br><a title="Agregar Folios" href="javascript: AgregarFolios();" class="linkReportes"><img src="diseno/Symbol-Add.png" border="0" alt="Agregar Folios"></a>
      </td>
    </tr>
  </table>
</center>
<?php 
	}
?>