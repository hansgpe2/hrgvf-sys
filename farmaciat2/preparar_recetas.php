<?php
error_reporting(E_ERROR|E_PARSE);
set_time_limit(600);
include_once('lib/misFunciones.php');
@session_start ();

function getRecetasXsurtir() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $bdissste);
    $query_query = "SELECT * FROM recetas WHERE fecha='" . date('Ymd') . "' AND status='1' ORDER BY fecha,hora DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            /*$ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );*/
			$ret[]=$row_query;
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

$datosRecetas = getRecetasXsurtir();
$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="780">';
$out .= '<tr><td colspan="6" class="tituloVentana">Recetas por preparar</td></tr>';
$out .= '<tr><th>Fecha y Hora</th><th></th><th>Cod. Barras</th><th>Derechohabiente</th></tr>';
$tDatos = count($datosRecetas);

$colores = array("#FFFFFF", "#DDDDDD");
for ($i=0; $i<$tDatos; $i++) {
//	$datosMedico = getMedicoXid($datosRecetas[$i]['id_medico']);
//	$datosServicio = getServicioXid($datosRecetas[$i]['id_servicio']);
	$datosDerecho = getDatosDerecho($datosRecetas[$i]['id_derecho']);
    $aTipo = explode("_", $datosRecetas[$i]['entidad_federativa']);
    $cual = $i%2;
	$out .= '<tr class="botones_menu" style="background: ' . $colores[$cual] . ';"><td>' . formatoDia($datosRecetas[$i]['fecha'],"fecha") . ' - ' . $datosRecetas[$i]['hora'] . '</td><td>' . $aTipo[1] . '</td><td align="center">' . $datosRecetas[$i]['codigo_barras'] . '</td><td>' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td></tr>';
    $conceptos =  getDatosMedicamentoEnReceta($datosRecetas[$i]["id_receta"]);
    $out .= '<tr class="botones_menu" style="background: ' . $colores[$cual] . ';"><td colspan="6"><table width="100%" border="1" cellspacing="0" cellpadding="3"><tr><th width="30">Cantidad</th><th>Medicamento</th></tr>';
    foreach ($conceptos as $indice => $concepto) {
        $medicamento = getDatosMedicamentos($concepto["id_medicamento"]);
        $out .= '<tr><td align="center">' . $concepto["cantidad"] . '</td><td class="botones_menu">' . $medicamento["descripcion"] . '</td></tr>';
    }
    $out .= '<tr><td align="center" colspan="2" style="padding: 15px;"><a class="boton g amarillo" href="javascript: void(0);" onclick="javascript: preparar_recetas_do(\'' . $datosRecetas[$i]["id_receta"] . '\');">Preparada para entregar</a></td></tr>';
    $out .= '</table></td></tr>';
}
$out .= '</table></center>';

print($out);

?>

