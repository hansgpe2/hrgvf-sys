<?php
error_reporting(E_ALL^E_NOTICE);
include_once('lib/misFunciones.php');

    $datosReceta = getRecetaXid($_GET["id_receta"]);
    $derecho = getDatosDerecho($datosReceta["id_derecho"]);
//    $conceptos = getDatosMedicamentoEnReceta($datosReceta["id_receta"]);

	$nombre = ponerAcentos($derecho["ap_p"] . ' ' . $derecho["ap_m"] . ' ' . $derecho["nombres"]);
	$fecha = formatoDia($datosReceta['fecha'],"imprimirCita");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Etiqueta Receta Preparada</title>
<link href="lib/impresionEtiquetas.css" media="print" rel="stylesheet" type="text/css" />
</head>

<body onload="javascript: window.print();" style="width:2in; font-family:Arial, Helvetica, sans-serif; font-size:9px">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" width="45"><img src="diseno/logoEncabezado.png" width="30" height="30" /></td>
    <td class="issste" align="center">HOSP. REGIONAL “DR. VALENT&Iacute;N G&Oacute;MEZ FAR&Iacute;AS”<br /><span>CONSULTA EXTERNA</span></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2" align="center"><b><?php echo  ponerAcentos($nombre) ?></b></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%" align="left"><b>D&Iacute;A: </b><?php echo $fecha ?></td><td align="left"><b>Hora:</b> <?php echo $datosReceta['hora'] ?> Hrs.</td></tr></table></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2" align="left"><img src="barcode/barcode.php?code=<?php echo $datosReceta["codigo_barras"]; ?>&tam=1" alt="barcode" /></td>
  </tr>
</table>
</body>
</html>
