<?php
error_reporting(E_ALL^E_NOTICE);
include_once('lib/misFunciones.php');

function getLicenciaXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = array();
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
	mysql_query("SET CHARACTER SET utf8"); 
	mysql_query("SET NAMES utf8"); 
    $query_query = "SELECT * FROM licencias WHERE id='" . $id . "' AND status='1' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query == 1) {
        $ret = mysql_fetch_assoc($query);
 	}
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}


function getLicencia($paraQuien, $serie, $licencia, $medico, $dia, $mes, $ano, $diaI, $mesI, $anoI, $diaT, $mesT, $anoT) {
	$motivos_licencia = array("PRT" => "Posible Riesgo de Trabajo", "RT" => "Riesgo de Trabajo", "EG" => "Enfermedad General", "MPRE" => "Maternidad Pre-parto" , "MPOS" => "Maternidad Post-parto");
    if ($licencia["caracter_licencia"] == "excepcional") $licencia["caracter_licencia"] .= ' - ' . $licencia["extra"];
    $fechaCita = explode(" ", $licencia["fecha_creacion"]);
    $fechaCita = str_replace("-", "", $fechaCita[0]);
    $consultaOcama = getCita($licencia["idHorario"], $fechaCita);
    $consultaOcama = $consultaOcama["id_cita"];
    if ($consultaOcama == "-1") {
        $consultaOcama = getCitaExt($licencia["idHorario"]);
        $consultaOcama = $consultaOcama["id_cita"] . 'e';
    }
	$ret = '';
	$ret .= '
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">' . $paraQuien . '</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td width="222" align="center" height="57"><img src="diseno/issste2013.png" width="151" height="38" /></td>
                	<td align="center"><span class="tituloIssste">DIRECCION MEDICA</span><br /><span class="tituloEncabezado">LICENCIA MEDICA</span></td>
                	<td width="200" align="left">Serie No. <img src="barcode/barcode.php?code=' . $serie . '&tam=1" alt="barcode" /></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6">ENTIDAD FEDERATIVA<br /><span class="contenido8">' . $licencia["entidad_federativa"] . '</span></td>
                	<td align="left" width="130" class="contenido6BordeIzq">UNIDAD MEDICA<br /><span class="contenido8">' . $licencia["unidad_medica"] . '</span></td>
                	<td align="left" class="contenido6BordeIzq">CLAVE<br /><span class="contenido8">' . $licencia["clave"] . '</span></td>
                	<td width="100" align="center" class="contenido6BordeIzq">
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" colspan="3">FECHA</td>
                            </tr>
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center">' . $dia . '</td><td align="center">' . $mes . '</td><td align="center">' . $ano . '</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="left" class="contenido6">NOMBRE DEL PACIENTE<br /><span class="contenido8">' . ponerAcentos($licencia["paciente"]) . '</span></td>
                	<td align="left" width="100" class="contenido6BordeIzq">CEDULA<br /><span class="contenido8">' . $licencia["cedula"] . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="left" class="contenido6">DEPENDENCIA<br /><span class="contenido8">' . $licencia["dependencia"] . '</span></td>
                	<td align="left" width="50%" class="contenido6BordeIzq">UNIDAD ADMINISTRATIVA<br /><span class="contenido8">' . $licencia["unidad"] . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="left" class="contenido6">DIAGNOSTICO<br /><span class="contenido8">' . $licencia["diagnostico"] . '</span></td>
                </tr>
            </table>
        </td>
	</tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="left" class="contenido6BordeIzq">Días Otorgados con letra<br /><span class="contenido8">' . $licencia["dias_letra"] . '</span></td>
                	<td align="left" width="130" class="contenido6BordeIzq">Días Otorgados<br />con número<br /><span class="contenido8">' . $licencia["dias"] . '</span></td>
                	<td width="100" align="center" class="contenido6BordeIzq">
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" colspan="3">Inicio</td>
                            </tr>
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center">' . $diaI . '</td><td align="center">' . $mesI . '</td><td align="center">' . $anoI . '</td>
                        	</tr>
                        </table>
                    </td>
                	<td width="100" align="center" class="contenido6BordeIzq">
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" colspan="3">Término</td>
                            </tr>
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center">' . $diaT . '</td><td align="center">' . $mesT . '</td><td align="center">' . $anoT . '</td>
                        	</tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="25%" class="contenido6BordeIzq" valign="top">MOTIVOS DE LA LICENCIA<br /><span class="contenido8">' . $motivos_licencia[$licencia["motivo_licencia"]] . '</span></td>
                	<td align="center" width="25%" class="contenido6BordeIzq" valign="top">CARACTER DE LA LICENCIA<br /><span class="contenido8">' . $licencia["caracter_licencia"] . '</span></td>
                	<td align="center" width="25%" class="contenido6BordeIzq" valign="top">TIPO DE SERVICIO OTORGADO<br /><span class="contenido8">' . $licencia["servicio_otorgado"] . '</span></td>
                	<td align="center" width="25%" class="contenido6BordeIzq" valign="top">NUMERO DE CONSULTA Y/O CAMA<br /><span class="contenido8">' . $consultaOcama . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                    <td align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br /><span class="contenido8">' . $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]) . '</span></td>
                	<td width="170" align="left" class="contenido6BordeIzq" valign="top">NOMBRE Y FIRMA DEL PACIENTE</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="contenido6">
            <b>CLAVE SM3-1</b><br />ESTE DOCUMENTO ES DE CARÁCTER PUBLICO, POR LO QUE SE FALSIFICACION O MAL USO CONTITUYE UN DELETI FEDERAK Y SERA RESPONSABILIDAD DE QUIEN LOS SUSCRIBA, DE ACUERDO A LA LEY FEDERAL DE RESPONSABILIDADES ADMINISTRATIVAS DE LOS SERVIDORES PUBLICOS Y CODIGO PENAL EN MATERIA FEDERAK PARA TODA LA REPUBLICA
        </td>
    </tr>
</table>
	';
	return $ret;
}

if (isset($_GET["id"])) {
	$licencia = getLicenciaXid($_GET["id"]);
	if (count($licencia) > 0) {
		$medico = getMedicoXid($licencia["idDr"]);
		$serie = '034LM' . ponerCeros($_GET["id"], 7);
		$aFecha = explode(" ", $licencia["fecha_creacion"]);
		$aFecha = explode("-", $aFecha[0]);
		$dia = $aFecha[2];
		$mes = $aFecha[1];
		$ano = $aFecha[0];
		$aFechaI = explode("-", $licencia["fecha_inicio"]);
		$aFechaT = explode("-", $licencia["fecha_termino"]);
		$diaI = $aFechaI[2];
		$mesI = $aFechaI[1];
		$anoI = $aFechaI[0];
		$diaT = $aFechaT[2];
		$mesT = $aFechaT[1];
		$anoT = $aFechaT[0];
		$copia1 = getLicencia('O R I G I N A L &nbsp; - &nbsp; D E P E N D E N C I A', $serie, $licencia, $medico, $dia, $mes, $ano, $diaI, $mesI, $anoI, $diaT, $mesT, $anoT);
		$copia2 = getLicencia('C O P I A &nbsp; - &nbsp; I N T E R E S A D O', $serie, $licencia, $medico, $dia, $mes, $ano, $diaI, $mesI, $anoI, $diaT, $mesT, $anoT);
		$copia3 = getLicencia('C O P I A &nbsp; - &nbsp; E X P E D I E N T E', $serie, $licencia, $medico, $dia, $mes, $ano, $diaI, $mesI, $anoI, $diaT, $mesT, $anoT);
		$copia4 = getLicencia('C O P I A &nbsp; - &nbsp; E S T A D I S T I C A', $serie, $licencia, $medico, $dia, $mes, $ano, $diaI, $mesI, $anoI, $diaT, $mesT, $anoT);
		$out = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title>Licencia Médica: ' . $serie . '</title>
                <style type="text/css">
                    @import url("lib/impresion.css") print;
                    @import url("lib/reportes.css") screen;
                </style>
            </head>

            <body onload="javascript: window.print();">
            ' . $copia1 . '<div class="separador" style="margin-top:5pt; margin-bottom: 10pt;"></div>' . $copia2 . '<h1 class="SaltoDePagina"></h1>' . $copia3 . '<div class="separador" style="margin-top:5pt; margin-bottom: 10pt;"></div>' . $copia4 . '
           </body>
            </html>
			';

		print($out);
	} else {
		print("No existe la licencia M&eacute;dica");
	}
} else {
    print("Error en variable id licencia");
}

?>