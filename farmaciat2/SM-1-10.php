<?php
// Desactivar toda notificación de error
error_reporting(0);
session_start();
$alto_barcode = '37px;';
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');
function getEdadXfechaNac($fecha) {
    if (strlen($fecha) == 10) {
        $diaHoy = date('j');
        $mesHoy = date('n');
        $anoHoy = date('Y');

        $ano = substr($fecha, 6, 4);
        $mes = substr($fecha, 3, 2);
        $dia = substr($fecha, 0, 2);
        if (($mes == $mesHoy) && ($dia > $diaHoy)) {
            $anoHoy = ($anoHoiy - 1);
        }
        //si el mes es superior al actual tampoco habr� cumplido a�os, por eso le quitamos un a�o al actual
        if ($mes > $mesHoy) {
            $anoHoy = ($anoHoy - 1);
        }
        //ya no habria mas condiciones, ahora simplemente restamos los a�os y mostramos el resultado como su edad
        $edad = ($anoHoy - $ano);
    } else {
        $edad = "ND";
    }
    return $edad;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>SM-1-10</title>
        <style type="text/css">
            @import url("../lib/impresion.css") print;
            @import url("../lib/reportes.css") screen;
        </style>
    </head>
</head>

<body>

    <?php
        $datosUsuario = getUsuarioXid($_GET['idMedico']);
        $datosMedico = getMedicoXid($datosUsuario['id_medico']);
        $datosServicio = getServicioXidSM10($_GET['idServicio']);
 		$hoy = date('Ymd');
        $hoyCB = date('d/m/Y');
        $fechaI = $hoy;
        $fechaF = $hoy;
        $citas = citasXrangoFechas($fechaI, $fechaF);
        $totalCitas = count($citas);
        $out = "";
        //$j = 0;
        $num = 1;
        $bandera = false;
        for ($i = 0; $i < $totalCitas; $i++) {//Citas programadas
            if ($citas[$i]['id_derecho'] > 0) {
                $horario = getHorarioXid($citas[$i]['id_horario']);
				  $med = '';
				  if (isset($horario['id_medico'])) $med=$horario['id_medico'];
                if ($med == $datosUsuario['id_medico']) {
                    if ($bandera == false)
                        $out = "";
                    $bandera = true;
                    $datosDH = getDatosDerecho($citas[$i]['id_derecho']);
                    //		$j++;
                    $nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
                    $expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
                    $vigenciaSi = "&nbsp;";
                    $vigenciaNo = "&nbsp;";
                    $sexoM = "&nbsp;";
                    $sexoF = "&nbsp;";
                    $edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
                    $sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
                    if ($sexo == "M")
                        $sexoM = $edad;
                    if ($sexo == "F")
                        $sexoF = $edad;
                    $foraneo = tipoDH($datosDH['municipio']);
                    $tipoCita = "&nbsp;";
                    $observa = "&nbsp;";
                    if ($horario['tipo_cita'] == 0) {//tipo de cita 0 primera vez
                        $citaDetalles = getDetallesCitaAgenda($citas[$i]['id_cita'], 'N');
                        $observa = $citaDetalles['campo1'];
                        $tipoCita = "1ra.";
                    }
                    if ($horario['tipo_cita'] == 1)//tipo de cita 1 subsecuente
                        $tipoCita = "Subs.";
                    if ($horario['tipo_cita'] == 2) {//tipo de cita 2 procedimiento
                        $tipoCita = "Proc.";
                        $observa = $citas[$i]['observaciones'];
                    }
					$citaDetallesFarmacia = getDetallesCita($citas[$i]['id_cita'], 'N');
					$cant_medicamento = '';
					$n_medicamento = '';
					$codigo = '';
					$descripcion = '';
                    $diagnosticos = '';
					$asistio = '';
					$contra = '';
					if ($citaDetallesFarmacia['fecha_medicamento'] != '') {
						$recetas = getRecetasXderechoXdia($citas[$i]['id_derecho'], $datosUsuario['id_medico'], $_GET['idServicio'], $hoy);
						$tRecetas = count($recetas);
						if ($tRecetas>0) {
							for ($z=0; $z<$tRecetas; $z++) {
								$medicamentos = getDatosMedicamentoEnReceta($recetas[$z]['id_receta']);
								$tMedicamentos = count ($medicamentos);
								for ($y=0; $y<$tMedicamentos; $y++) {
									$cant_medicamento += $medicamentos[$y]['cantidad'];
//                                    $cant_medicamento .= $medicamentos[$y]['cantidad'] . '<br />';
									$n_medicamento .= $medicamentos[$y]['id_medicamento'] . '<br />';
									$medicamento = getDatosMedicamentos($medicamentos[$y]['id_medicamento']);
								}
//								$descripcion .= substr($recetas[$z]['numero_vale'],0,25) . '<br />';
							}
						}
					}
					if ($citaDetallesFarmacia['hizo_contra'] == 'SI') {
						$contra = 'C';
					}
					$diagnosticos = getDiagnostico2014($citaDetallesFarmacia);
					if ($citaDetallesFarmacia['asistio'] != 'SI') {
						$vigenciaNo = 'X';
						$diagnosticos = 'Cita no otorgada<br>paciente no asisti&oacute;';
					}
					if ($citaDetallesFarmacia['asistio'] == 'SI') {
						$vigenciaSi = 'X';
					}
					/* <td align=\"center\" class=\"contenido8\">" . formatoDia($citas[$i]['fecha_cita'], "imprimirCita") . "<br>" . formatoHora($horario['hora_inicio']) . "a" . formatoHora($horario['hora_fin']) . "</td> */
					$out .= "<tr height=\"30\">
					<td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\">" . $num . "</td>
					<td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\"><img src=\"barcode/barcode.php?code=" . $datosDH['cedula'] . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
					<td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\">" . ponerAcentos($nombre) . "</td>
					<td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\"><img src=\"barcode/barcode.php?code=" . $datosDH['cedula_tipo'] . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
					<td align=\"left\" class=\"contenido8\" style=\"border-left:0px;\">&nbsp;</td>
					<td align=\"center\" class=\"contenido8\">" . $sexoM . "</td>
					<td align=\"center\" class=\"contenido8\">" . $sexoF . "</td>
					<td align=\"center\" class=\"contenido8\">" . $foraneo . "</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
						<td align=\"center\" class=\"contenido8\">" . $contra . "</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
					<td align=\"left\" class=\"contenido8\" style=\"border-right:0px;\">&nbsp;</td>
					<td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\"><img src=\"barcode/barcode.php?code=" . $cant_medicamento . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
					<td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\">" . $diagnosticos . "</td>
					<td align=\"center\" class=\"contenido8\" style=\"border-left:0px;\">" . $tipoCita . "</td>
				    </tr>";
					
					$num++;
                }
            }
        }
        $citas2 = citasExtemporaneasXrangoFechas($fechaI, $fechaF);
        $totalCitas2 = count($citas2);
        $num2 = $num;
        $bandera2 = false;
        $out2 = "";
        for ($i = 0; $i < $totalCitas2; $i++) {//citas extemporaneas
            if ($citas2[$i]['id_derecho'] > 0) {
;
                if ($citas2[$i]['id_medico'] == $datosUsuario['id_medico']) {
                    if ($citas2[$i]['tipo_cita'] != 3) {//tipo de cita 3 administrativa, esta no se toma en el sm-1-10
                        if ($bandera2 == false)
                            $out2 = "";
                        $bandera2 = true;
                        $datosDH = getDatosDerecho($citas2[$i]['id_derecho']);
                        //		$j++;
                        $nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
                        $expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
                        $vigenciaSi = "&nbsp;";
                        $vigenciaNo = "&nbsp;";
                        $sexoM = "&nbsp;";
                        $sexoF = "&nbsp;";
                        $edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
                        $sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
                        if ($sexo == "M")
                            $sexoM = $edad;
                        if ($sexo == "F")
                            $sexoF = $edad;
                        $foraneo = tipoDH($datosDH['municipio']);
                        $tipoCita = '&nbsp;';
                        $observa = "&nbsp;";
                        if ($citas2[$i]['tipo_cita'] == 0) {
                            $citaDetalles = getDetallesCitaAgenda($citas2[$i]['id_cita'], 'E');
                            $observa = $citaDetalles['campo1'];
                            $tipoCita = "1ra.";
                        }
                        if ($citas2[$i]['tipo_cita'] == 1)
                            $tipoCita = "Subs.";
                        if ($citas2[$i]['tipo_cita'] == 2) {
                            $tipoCita = "Proc.";
                            $observa = $citas2[$i]['observaciones'];
                        }
						$citaDetallesFarmacia = getDetallesCita($citas2[$i]['id_cita'], 'E');
						$cant_medicamento = '';
						$n_medicamento = '';
						$codigo = '';
						$descripcion = '';
                        $diagnosticos = '';
						$asistio = '';
						$contra = '';
						if ($citaDetallesFarmacia['fecha_medicamento'] != '') {
							$recetas = getRecetasXderechoXdia($citas2[$i]['id_derecho'], $datosUsuario['id_medico'], $_GET['idServicio'], $hoy);
							$tRecetas = count($recetas);
							if ($tRecetas>0) {
								for ($z=0; $z<$tRecetas; $z++) {
									$medicamentos = getDatosMedicamentoEnReceta($recetas[$z]['id_receta']);
									$tMedicamentos = count ($medicamentos);
									for ($y=0; $y<$tMedicamentos; $y++) {
                                        $cant_medicamento += $medicamentos[$y]['cantidad'];
//										$cant_medicamento .= $medicamentos[$y]['cantidad'] . '<br />';
										$n_medicamento .= $medicamentos[$y]['id_medicamento'] . '<br />';
										$medicamento = getDatosMedicamentos($medicamentos[$y]['id_medicamento']);
//										$descripcion .= substr($medicamentos[$y]['indicaciones'],0,25) . '<br />';
									}
//									$descripcion .= substr($recetas[$z]['numero_vale'],0,25) . '<br />';
								}
							}
						}
						if ($citaDetallesFarmacia['hizo_contra'] == 'SI') {
							$contra = 'C';
						}
						$diagnosticos = getDiagnostico2014($citaDetallesFarmacia);
						if ($citaDetallesFarmacia['asistio'] != 'SI') {
							$vigenciaNo = 'X';
							$diagnosticos = 'Cita no otorgada - paciente no asisti&oacute;';
						}
						if ($citaDetallesFarmacia['asistio'] == 'SI') {
							$vigenciaSi = 'X';
						}
                       $out2 .= "<tr height=\"30\">
                    <td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\">" . $num2 . "E</td>
                    <td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\"><img src=\"barcode/barcode.php?code=" . $datosDH['cedula'] . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
                    <td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\">" . ponerAcentos($nombre) . "</td>
                    <td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\"><img src=\"barcode/barcode.php?code=" . $datosDH['cedula_tipo'] . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
                    <td align=\"left\" class=\"contenido8\" style=\"border-left:0px;\">&nbsp;</td>
					<td align=\"center\" class=\"contenido8\">" . $sexoM . "</td>
					<td align=\"center\" class=\"contenido8\">" . $sexoF . "</td>
					<td align=\"center\" class=\"contenido8\">" . $foraneo . "</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
					<td align=\"center\" class=\"contenido8\">" . $contra . "</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
					<td align=\"left\" class=\"contenido8\">&nbsp;</td>
                    <td align=\"left\" class=\"contenido8\" style=\"border-right:0px;\">&nbsp;</td>
                    <td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\"><img src=\"barcode/barcode.php?code=" . $cant_medicamento . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
                    <td align=\"center\" class=\"contenido8\" style=\"border-left:0px; border-right:0px;\">" . $diagnosticos . "</td>
                    <td align=\"center\" class=\"contenido8\" style=\"border-left:0px;\">" . $tipoCita . "</td>
				  </tr>";
                        $num2++;
                    }
                }
            }
        }
        if ($num2 < 12) {
            for ($i = $num2; $i < 12; $i++) {
//                $out2 .= "<tr height=\"30\"><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td></tr>";
            }
        } else {
            for ($i = $num2; $i < 30; $i++) {
//                $out2 .= "<tr height=\"30\"><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"left\" class=\"contenido8\">&nbsp;</td><td align=\"center\" class=\"contenido8\">&nbsp;</td></tr>";
            }
        }

        $claveUnidadMedica = "034203";
        $unidadMedica = "H.R VALENTIN GOMEZ FARIAS, ZAPOPAN";
        $delegacion = "H.R VALENTIN GOMEZ FARIAS, ZAPOPAN";
        $claveDelMedico = $datosMedico['n_empleado'];
        $nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
        $servicio = "CONSULTA EXTERNA";
        $claveDeLaEspecialidad = $datosServicio['clave'];
        $especialidad = ponerAcentos($datosServicio['nombre']);
        $horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
        $reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"74\" align=\"left\"><img src=\"diseno/logoEncabezado2.jpg\" width=\"74\" height=\"74\" /></td>
        <td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
          Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
        <td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />Informe Diario de Labores del Médico<br />SM-1-10</td>
        <td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td class=\"tituloEncabezado\" align=\"center\" colspan=\"2\">Fecha</td>
          </tr>
          <tr>
            <td align=\"center\" class=\"contenido8bold\" colspan=\"2\"><img src=\"barcode/barcode.php?code=" . $hoyCB . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
    <td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del Médico</td>
  </tr>
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td align=\"left\" class=\"contenido8bold\" width=\"50\">Clave del Médico:</td>
        <td align=\"left\" class=\"contenido8\" width=\"100\"><img src=\"barcode/barcode.php?code=" . $claveDelMedico . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
        <td align=\"left\" class=\"contenido8bold\" width=\"100\">Nombre del Médico:</td>
        <td align=\"left\" class=\"contenido8\" width=\"300\">" . $nombreDelMedico . "</td>
        <td align=\"left\" class=\"contenido8bold\" width=\"50\">Servicio:</td>
        <td align=\"left\" class=\"contenido8\" width=\"300\">" . $servicio . "</td>
      </tr>
      <tr>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Clave de la Especialidad:</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8\"><img src=\"barcode/barcode.php?code=" . $claveDeLaEspecialidad . "&tam=1\" style=\"height:" . $alto_barcode . "\"></td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Especialidad:</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $especialidad . "</td>
        <td align=\"left\" class=\"contenido8bold\">Horario:</td>
        <td align=\"left\" class=\"contenido8\">" . $horario . "</td>
      </tr>
      <tr>
        <td align=\"left\" class=\"contenido8bold\">Firma:</td>
        <td align=\"left\" class=\"contenido8\" height=\"40\">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
    <td><table width=\"100%\" border=\"2\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"15\" rowspan=\"2\" align=\"center\" class=\"contenido8bold\">Cita</td>
        <td width=\"50\" rowspan=\"2\" align=\"center\" class=\"contenido8bold\">Expediente</td>
        <td rowspan=\"2\" width=\"150\" align=\"center\" class=\"contenido8bold\">Nombre</td>
        <td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo de<br />Expediente</td>
        <td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Sub-<br />rogado</td>
        <td colspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Edad<br />y<br />Sexo</td>
        <td rowspan=\"2\" width=\"15\" align=\"center\" class=\"contenido8bold\">Fo-<br />rá-<br />neo</td>
        <td colspan=\"2\" width=\"60\" align=\"center\" class=\"contenido8bold\">Solicitud a:</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">Pase<br />a<br />Uni.</td>
        <td colspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Motivo<br />Licencia</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">No.<br />Serie<br />Lic.</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">Días<br />de<br />Lic.</td>
        <td rowspan=\"2\" width=\"100\" align=\"center\" class=\"contenido8bold\">No.<br />de<br />Medic.</td>
        <td width=\"180\" align=\"center\" class=\"contenido8bold\">Diagnóstico</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">Tipo<br>Cita</td>
      </tr>
      <tr>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">M</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">F</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">LAB</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">RX.</td>
        <td width=\"30\" align=\"center\" class=\"contenido8bold\">PRT</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">RT</td>
        <td width=\"30\" align=\"center\" class=\"contenido8bold\">Código</td>
        </tr>" . $out . $out2 . "
    </table></td>
  </tr>
</table>";
        echo $reporte;
    ?>
</body>
</html>
