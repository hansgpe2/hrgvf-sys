function ponerAcentos2013(Text) {
	var cadena=""; 
	var temp = "";
	var total = Text.length;
	for (var j = 0; j < total; j++)  
	{  
		var cara=Text.charAt(j);
		if (cara == "(") {
			temp = Text.substring(j,j+4);
			switch (temp) {
				case "(/a)": cadena += "\u00e1";
				j = j + 3;
				break;
				case "(/A)": cadena += "\u00c1";
				j = j + 3;
				break;
				case "(/e)": cadena += "\u00e9";
				j = j + 3;
				break;
				case "(/E)": cadena += "\u00c9";
				j = j + 3;
				break;
				case "(/i)": cadena += "\u00ed";
				j = j + 3;
				break;
				case "(/I)": cadena += "\u00cd";
				j = j + 3;
				break;
				case "(/o)": cadena += "\u00f3";
				j = j + 3;
				break;
				case "(/O)": cadena += "\u00d3";
				j = j + 3;
				break;
				case "(/u)": cadena += "\u00fa";
				j = j + 3;
				break;
				case "(/U)": cadena += "\u00da";
				j = j + 3;
				break;
				case "(/n)": cadena += "\u00f1";
				j = j + 3;
				break;
				case "(/N)": cadena += "\u00d1";
				j = j + 3;
				break;
				default:  
				cadena+=Text.charAt(j);  
				break;  
			}
		} else {
			cadena+=Text.charAt(j);  
		}
	}  
	return cadena;
}

function verRecetas2013(id_derecho, nmed) {
	var contenedor;
	contenedor = document.getElementById('div_recetas'+nmed);
	var objeto= new AjaxGET();
	objeto.open("GET", "citasXbusqueda.php?id_derecho="+id_derecho,true);
	objeto.onreadystatechange=function()
	{
		if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor.innerHTML = objeto.responseText;
		}
		if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
		{
			contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	objeto.send(null)
}

function genDiagnostico(id_cita, tipo_cita) {
	var diag = document.getElementById('diagnostico').value;
	var diagSel = document.getElementById('diagnostico_seleccionado').value;
	if (diagSel == '') {
		alert('El diagnostico es requerido');
		return;
	}
	if (diagSel != diag) {
		alert('No existe: ' + diag);
		return;
	}
	var diag2 = document.getElementById('diagnostico2').value;
	if (diag2 != '') {
		diag = diag + " || " + diag2;
	}
	var contenedor;
	contenedor = document.getElementById('contenido');
	var objeto= new AjaxGET();
	objeto.open("GET", "generarDiagnosticoConfirmar.php?id_cita="+id_cita+"&tipo_cita="+tipo_cita+"&diagnostico="+diag,true);
	objeto.onreadystatechange=function()
	{
		if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			alert('Diagnostico agregado correctamente');
			regresarARecetas();
		}
		if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
		{
			document.getElementById('agregar').disabled = 'disabled';
			document.getElementById('regresar').disabled = 'disabled';
			contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	objeto.send(null)
}

function hacerDiagnostico(id_horario, id_cita, tipo_cita) {
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "generarDiagnostico.php?idHorario="+id_horario+"&id_cita="+id_cita+"&tipo_cita="+tipo_cita,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null);
}

function quitarEnter2013(evt, obj) {
	var charCode = (evt.which) ? evt.which : event.keyCode;	
	if (charCode == 10) return false;
}

function sinTab(evt, obj) {
	var charCode = (evt.which) ? evt.which : event.keyCode;	
	if (charCode == 9) return false;
}

function validarAgregarCita2013() {
	var err = false;
	var errTxt = 'Cedula y nombre completo es requerido';
    var id_derecho = document.getElementById('id_derecho').value;
    var id_cita = document.getElementById('id_cita').value;
    var tipo_reprogramacion = document.getElementById('tipo_reprogramacion').value;
    var ap_p = document.getElementById('ap_p').value;
    var ap_m = document.getElementById('ap_m').value;
    var nombre = document.getElementById('nombre').value;
    var telefono = document.getElementById('telefono').value;
    var direccion =	document.getElementById('direccion').value;
    var estado = document.getElementById('estado').value;
    var municipio =	document.getElementById('municipio').value;
    var observaciones =	document.getElementById('observaciones').value;
    if (document.getElementById('diagnostico') != undefined) {
        var diagnostico = document.getElementById('diagnostico').value;
		var diagSel = document.getElementById('diagnostico_seleccionado').value;
		if (diagSel == '') {
			alert('El diagnostico es requerido');
			err = true;
		}
		if (diagSel != diagnostico) {
			alert('No existe: ' + diagnostico);
			err = true;
		}
        var diagnostico2 = document.getElementById('diagnostico2').value;
		if (diagnostico2 != undefined) {
			if (diagnostico2 != '') {
				diagnostico = diagnostico + ' || ' + diagnostico2;
			}
		}
    } else {
        var diagnostico = "";
    }
    if (document.getElementById('concertada') != undefined) {
        if (document.getElementById('concertada').checked == true)
            var concertada = "si";
        else 
            var concertada = "no";
    } else {
        var concertada = "";
    }
    if ((id_derecho.length > 0) && (err == false)) {
        var contenedor;
        contenedor = document.getElementById('contenido');
        var contenedor2;
        contenedor2 = document.getElementById('enviando');
        var objeto= new AjaxGET();
        objeto.open("GET", "agregarCitaConfirmar.php?id_derecho="+id_derecho+"&idHorario="+document.getElementById('id_horario').value+"&fechaCita="+document.getElementById('fechaCita').value+"&obs="+observaciones+"&id_cita="+id_cita+"&tipo_reprogramacion="+tipo_reprogramacion+"&diagnostico="+diagnostico+"&concertada="+concertada,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor2.innerHTML = "&nbsp;";
                document.getElementById('agregar').disabled = '';
                document.getElementById('regresar').disabled = '';
                citaAgregada(objeto.responseText);
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                document.getElementById('agregar').disabled = 'disabled';
                document.getElementById('regresar').disabled = 'disabled';
                contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert(errTxt);
    }
}

function cerrarAgendaMedico(id_medico, idServicio) {
    var contenedor2;
    var contenedorTmp;
    contenedor2 = document.getElementById('contenido');
    contenedorTmp = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "validarCierreAgenda.php",true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
			if(agenda.responseText == 'ok') {
				window.open('SM-1-10.php?idMedico='+id_medico+'&idServicio='+idServicio);
			} else {
				alert(ponerAcentos2013(agenda.responseText.replace(/-/g,'\n')));
			}
        }
        if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
        {
            //contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
	
/*	
    var contenedor;
    contenedor = document.getElementById('contenido');
    var seleccion= new AjaxGET();
    seleccion.open("GET", "adminAgendaMedico.php?idUsuario="+id_usuario,true);
    seleccion.onreadystatechange=function()
    {
        if (seleccion.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = seleccion.responseText;
//            cargarAgenda()
			$('#ex2').jqmHide(); 
        }
        if ((seleccion.readyState==1) ||(seleccion.readyState==2)||(seleccion.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    seleccion.send(null)
*/
}

function crearReferencia2013(horario, id_cita, tipo_cita)
{
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "generarContraReferencia.php?id_horario="+horario+"&id_cita="+id_cita+"&tipo_cita="+tipo_cita,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnosticoRef').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
			$('#diagnosticoRef2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null);
}
function crearCalendario2013(id)
{
    var contenedor;
    contenedor = document.getElementById('seleccion');
    var seleccion= new AjaxGET();
    seleccion.open("GET", "cargarCalendario.php?idHorario="+id,true);
    seleccion.onreadystatechange=function()
    {
        if (seleccion.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = seleccion.responseText;
            cargarAgenda()
			$('#ex2').jqmHide(); 
        }
        if ((seleccion.readyState==1) ||(seleccion.readyState==2)||(seleccion.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    seleccion.send(null)
}

function agregarReceta2013(id_horario) {
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "agregarReceta.php?id_horario=" + id_horario,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null)
}

function agregarReceta2013doble(id_horario) {
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "agregarRecetaDoble.php?id_horario=" + id_horario,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null)
}

function agregarReceta2013triple(id_horario) {
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "agregarRecetaTriple.php?id_horario=" + id_horario,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null)
}

function agregarConstancia2013(id_horario,idDr,idUsuario) {
    $("#contenido").load("constancias/index.php?idHorario="+id_horario+"&idDr="+idDr+"&idUser="+idUsuario);
	$('#ex2').jqmHide(); 
}


function crearReferenciaExt2013(cita, id_cita, tipo_cita)
{
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "generarContraReferencia.php?idCita="+cita+"&id_cita="+id_cita+"&tipo_cita="+tipo_cita,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnosticoRef').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
			$('#diagnosticoRef2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null);
}

function crearCalendarioExt2013(id)
{
    var contenedor;
    contenedor = document.getElementById('seleccion');
    var seleccion= new AjaxGET();
    seleccion.open("GET", "cargarCalendarioExt.php?idCita="+id,true);
    seleccion.onreadystatechange=function()
    {
        if (seleccion.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = seleccion.responseText;
            cargarAgenda()
			$('#ex2').jqmHide(); 
        }
        if ((seleccion.readyState==1) ||(seleccion.readyState==2)||(seleccion.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    seleccion.send(null)
}

function agregarRecetaCitaE2013(id_cita) {
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "agregarRecetaExt.php?id_cita="+id_cita,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null)
}

function agregarReceta2013dobleE(id_cita) {
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "agregarRecetaDobleE.php?id_cita=" + id_cita,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null)
}

function agregarReceta2013tripleE(id_cita) {
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "agregarRecetaTripleE.php?id_cita=" + id_cita,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null)
}

function agregarConstanciaE2013(id_cita,id_medico,id_usuario) {
    var contenedor2;
    contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "constancias/index.php?id_cita="+id_cita+"&idUser="+id_usuario+"&idMedico="+id_medico,true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
        }
        if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}


function getcookies()
{
    var obj = new Object();
    var lineas = document.cookie.split(";");
    for (var i=0; i<lineas.length; i++)
    {
        var partes = lineas[i].split("=");
        var nombre = partes[0];
        var valor = decodeURIComponent(partes[1]).replace("'", "\\'");
    if (nombre.length > 0)
        eval("obj." + nombre + "='" + valor + "'");
    }
    return obj;
}

function setupLabel() {
	if ($('.label_radio input').length) {
		$('.label_radio').each(function(){ 
			$(this).removeClass('r_on');
			$(this).removeClass('r_off');
		});
		$('.label_radio input:checked').each(function(){
			if($(this).val() == 'SI') {
				$(this).parent('label').addClass('r_on');
			} else {
				$(this).parent('label').addClass('r_off');
			}
		});
	};
};


    $(document).ready(function(){
        $('body').addClass('has-js');
/*        $('.label_radio').live("click", function(){
			alert('va');
            setupLabel();
        });
*/
		$('.si_no').live('click', function() {
			if($(this).val() == 'SI') {
				var tmp = $(this).attr('name');
				tmp = tmp.split('_');
				$('#ex2').jqm({ajax: 'botonesDerecho.php?id_cita='+tmp[1] + '&id_servicio='+tmp[2] + '&id_derecho='+tmp[3] + '&id_horario='+tmp[4], modal: false});
				$('#ex2').jqmShow();
				$('#asistio_NO_N_'+tmp[1]).attr('disabled', true);
			} else {
				var tmp = $(this).attr('name');
				tmp = tmp.split('_');
				$('#ex2').jqm({ajax: 'botonesNo.php?id_cita='+tmp[1] + '&id_servicio='+tmp[2] + '&id_derecho='+tmp[3] + '&id_horario='+tmp[4], modal: false});
				$('#ex2').jqmShow();
				$('#ex2').jqmHide(); 
			}
			inicio('inicioMedicoEsp.php');
		});
		$('.si_noE').live('click', function() {
			if($(this).val() == 'SI') {
				var tmp = $(this).attr('name');
				tmp = tmp.split('_');
				$('#ex2').jqm({ajax: 'botonesDerechoE.php?id_cita='+tmp[1] + '&id_servicio='+tmp[2] + '&id_derecho='+tmp[3] + '&id_horario='+tmp[4], modal: false});
				$('#ex2').jqmShow();
			} else {
				var tmp = $(this).attr('name');
				tmp = tmp.split('_');
				$('#ex2').jqm({ajax: 'botonesNoE.php?id_cita='+tmp[1] + '&id_servicio='+tmp[2] + '&id_derecho='+tmp[3] + '&id_horario='+tmp[4], modal: false});
				$('#ex2').jqmShow();
				$('#ex2').jqmHide(); 
			}
			inicio('inicioMedicoEsp.php');
		});
        setupLabel();

        $(".agregarReceta").live('click', function(){
        	var aId = $(this).attr("id").split("_");
			$('#ex2').jqmHide();
			$('#ex2').jqm({ajax: 'botonesDerechoPreguntaRecetaTriple.php?id_horario='+aId[1], modal: false});
			$('#ex2').jqmShow();

 //       	agregarReceta2013(aId[1]);
        });
		
        $(".agregarRecetaE").live('click', function(){
        	var aId = $(this).attr("id").split("_");
			$('#ex2').jqmHide();
			$('#ex2').jqm({ajax: 'botonesDerechoPreguntaRecetaTripleE.php?id_horario='+aId[1], modal: false});
			$('#ex2').jqmShow();

 //       	agregarReceta2013(aId[1]);
        });

	  });

function actualizarPeriodo2014(n_medicamento) {
		document.getElementById("periodo_"+n_medicamento).innerHTML = document.getElementById("frecuencia_"+n_medicamento).value;
		document.getElementById("diasTratamiento_"+n_medicamento).value = getFrecuencia2014(n_medicamento);
		actualizarCantidad2014(n_medicamento);
}

function actualizarCantidad2014(n_medicamento) {
	if (document.getElementById("medi_"+n_medicamento).value != '') {
		var dosis_temp = parseFloat(document.getElementById("dosis_"+n_medicamento).value);
		var dias_temp = parseInt(document.getElementById("dias_"+n_medicamento).value);
		var unidad_temp = parseFloat(document.getElementById("uni_"+n_medicamento).value);
		var cant_temp = Math.ceil((dosis_temp * dias_temp) / unidad_temp);
		document.getElementById("divcantidad_"+n_medicamento).innerHTML = cant_temp.toString();
		document.getElementById("cantidad_"+n_medicamento).value = cant_temp.toString();
		document.getElementById("unidad_"+n_medicamento).value = document.getElementById("dosis_"+n_medicamento).value + '-' + document.getElementById("frecuencia_"+n_medicamento).value + '-' + document.getElementById("dias_"+n_medicamento).value;
		
	} else {
		document.getElementById("divcantidad_"+n_medicamento).innerHTML = '&nbsp;';
	}
	
}

function getFrecuencia2014(n_medicamento) {
	var valor = document.getElementById("frecuencia_"+n_medicamento).value
	var maximo = 0;
	var ret = 1;
	switch (valor) {
		case 'DIAS':
			ret = 1;
			maximo = 30;
			break;
		case 'SEMANAS':
			ret = 7;
			maximo = 4;
			break;
		case 'MESES':
			ret = 30;
			maximo = 1;
			break;
	}
	var tab = 9 + parseInt(n_medicamento);
	var sel = '<select name="dias_' + n_medicamento + '" id="dias_' + n_medicamento + '" tabindex="' + tab.toString() + '" onchange="javascript: actualizarCantidad2014(\'' + n_medicamento + '\')"><option selected="selected" value="0"></option>';
	for (var i = 1; i<=maximo; i++) {
		sel += '<option value="' + i.toString() + '">' + i.toString() + '</option>';
	}
	sel += '</select>';
	document.getElementById('divTrat_' + n_medicamento).innerHTML = sel;
	return ret;
}

function validarAgregarRecetaTriple() {
	var diagnostico = document.getElementById("diagnostico").value;
	var diagnostico2 = document.getElementById("diagnostico2").value;
	var id_derecho = document.getElementById("id_derecho").value;
	if (diagnostico == '') {
		alert("El Diagnostico es Requerido");
	} else {
		if (id_derecho == '') {
			alert("El derechohabiente es Requerido");
		} else {
			if (diagnostico2 != '') diagnostico = diagnostico + ' | ' + diagnostico2;
			var forma = document.getElementById("formaReceta");
			var tForma = forma.elements.length;
			var cantidades = new Array();
			var dias = new Array();
			var diasTratamiento = new Array();
			var unidades = new Array();
			var medicamentos = new Array();
			var indicaciones = new Array();
			var grupos = new Array();
			var dosis = new Array();
			cantidades[0] = 0;
			unidades[0] = 0;
			medicamentos[0] = 0;
			dias[0] = 0;
			diasTratamiento[0] = 0;
			indicaciones[0] = 0;
			grupos[0] = 0;
			dosis[0] = 0;
			for (i=0; i<tForma; i++) {
				var id = forma.elements[i].id;
				var aId = id.split("_");
				if (aId[0] == "cantidad") cantidades[aId[1]] = forma.elements[i].value;
				if (aId[0] == "unidad") unidades[aId[1]] = forma.elements[i].value;
				if (aId[0] == "indicaciones") indicaciones[aId[1]] = forma.elements[i].value;
				if (aId[0] == "medi") medicamentos[aId[1]] = forma.elements[i].value;
				if (aId[0] == "dias") dias[aId[1]] = forma.elements[i].value;
				if (aId[0] == "diasTratamiento") diasTratamiento[aId[1]] = forma.elements[i].value;
				if (aId[0] == "grupo") grupos[aId[1]] = forma.elements[i].value;
				if (aId[0] == "dosis") dosis[aId[1]] = forma.elements[i].value;
			}
			tMed = cantidades.length;
			var cantidadesEnviar = new Array();
			var unidadesEnviar = new Array();
			var diasEnviar = new Array();
			var diasTratamientoEnviar = new Array();
			var medicamentosEnviar = new Array();
			var indicacionesEnviar = new Array();
			var gruposEnviar = new Array();
			var numerosMed = new Array();
			var error = "";
			for (i=0; i<tMed; i++) {
				if ((cantidades[i] > 0) || (dias[i] > 0) || (medicamentos[i] > 0) || (indicaciones[i] != "") || (dias[i] > 0) || (dosis[i] > 0)) {
					if ((cantidades[i] > 0) && (dias[i] > 0) && (medicamentos[i] > 0) && (indicaciones[i] != "") && (dias[i] > 0) && (dosis[i] > 0)) {
						cantidadesEnviar.push(cantidades[i]);
						unidadesEnviar.push(unidades[i]);
						diasEnviar.push(dias[i]);
						diasTratamientoEnviar.push(diasTratamiento[i]);
						medicamentosEnviar.push(medicamentos[i]);
						indicacionesEnviar.push(quitarComas(indicaciones[i]));
						gruposEnviar.push(grupos[i]);
						numerosMed.push(i);
						if (isNaN(dosis[i])) error += '- La dosis debe ser un numero\n\r';
						if ((grupos[i] == "2") && (cantidades[i] > 2)) error += '- Los medicamentos del grupo 2 no se pueden recetar por mas de 2 cajas\n\r';
					} else {
						error += "- Complete los datos del medicamento " + i + "\n\r";
					}
					if (medicamentos[i] > 0) {
						if (medicamentos[i] == medicamentos[i+1]) {
							error += "- Seleccione medicamentos diferentes\n\r"
						}
					}
				}
				
			}
			if (error == "") {
				tMedicamentos = medicamentosEnviar.length;
				if (tMedicamentos > 0) {
					serie = document.getElementById("serie").value;
					folio = document.getElementById("folio").value;
					tipo_receta = document.getElementById("tipo_receta").value;
					var contenedor2;
					contenedor2 = document.getElementById('enviando');
					var objeto= new AjaxGET();
					objeto.open("GET", quitarAcentos("agregarRecetaTripleConfirmar.php?tipo_receta=" + tipo_receta + "&cantidades=" + cantidadesEnviar.toString() + "&unidades=" + unidadesEnviar.toString() + "&dias=" + diasEnviar.toString() + "&diasTratamiento=" + diasTratamientoEnviar.toString() + "&medicamentos=" + medicamentosEnviar.toString() + "&indicaciones=" + indicacionesEnviar.toString() + "&numerosMed=" + numerosMed.toString() + "&grupos=" + gruposEnviar.toString() + "&id_derecho=" + id_derecho + "&serie="+serie+"&folio="+folio+"&diagnostico="+diagnostico),true);
					objeto.onreadystatechange=function()
					{
						if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
						{
							resp = objeto.responseText.split("|");
							contenedor2.innerHTML = resp[0];
							if (resp[1] == '') {
								document.getElementById('agregar').disabled = '';
								document.getElementById('regresar').disabled = '';
							} else {
								
								if (resp[2] != '0') ventana2 = window.open("recetasPRINTnew.php?id_receta1=" + resp[1] + "&id_receta2=" + resp[2],"ventanita2"); else ventana = window.open("recetasPRINTnew.php?id_receta1=" + resp[1],"ventanita");
								document.getElementById('regresar').disabled = '';
							}
						}
						if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
						{
							document.getElementById('agregar').disabled = 'disabled';
							document.getElementById('regresar').disabled = 'disabled';
							contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
						}
					}
					objeto.send(null)
				} else {
					alert('- Llene al menos los datos de un medicamento');
				}
			}else {
				alert(error);
			}
		}
	}
}

function validarAgregarRecetaDoble() {
	var diagnostico = document.getElementById("diagnostico").value;
	var diagnostico2 = document.getElementById("diagnostico2").value;
	var id_derecho = document.getElementById("id_derecho").value;
	if (diagnostico == '') {
		alert("El Diagnostico es Requerido");
	} else {
		if (id_derecho == '') {
			alert("El derechohabiente es Requerido");
		} else {
			if (diagnostico2 != '') diagnostico = diagnostico + ' | ' + diagnostico2;
			var forma = document.getElementById("formaReceta");
			var tForma = forma.elements.length;
			var cantidades = new Array();
			var dias = new Array();
			var diasTratamiento = new Array();
			var unidades = new Array();
			var medicamentos = new Array();
			var indicaciones = new Array();
			var grupos = new Array();
			var dosis = new Array();
			cantidades[0] = 0;
			unidades[0] = 0;
			medicamentos[0] = 0;
			dias[0] = 0;
			diasTratamiento[0] = 0;
			indicaciones[0] = 0;
			grupos[0] = 0;
			dosis[0] = 0;
			for (i=0; i<tForma; i++) {
				var id = forma.elements[i].id;
				var aId = id.split("_");
				if (aId[0] == "cantidad") cantidades[aId[1]] = forma.elements[i].value;
				if (aId[0] == "unidad") unidades[aId[1]] = forma.elements[i].value;
				if (aId[0] == "indicaciones") indicaciones[aId[1]] = forma.elements[i].value;
				if (aId[0] == "medi") medicamentos[aId[1]] = forma.elements[i].value;
				if (aId[0] == "dias") dias[aId[1]] = forma.elements[i].value;
				if (aId[0] == "diasTratamiento") diasTratamiento[aId[1]] = forma.elements[i].value;
				if (aId[0] == "grupo") grupos[aId[1]] = forma.elements[i].value;
				if (aId[0] == "dosis") dosis[aId[1]] = forma.elements[i].value;
			}
			tMed = cantidades.length;
			var cantidadesEnviar = new Array();
			var unidadesEnviar = new Array();
			var diasEnviar = new Array();
			var diasTratamientoEnviar = new Array();
			var medicamentosEnviar = new Array();
			var indicacionesEnviar = new Array();
			var gruposEnviar = new Array();
			var numerosMed = new Array();
			var error = "";
			for (i=0; i<tMed; i++) {
				if ((cantidades[i] > 0) || (dias[i] > 0) || (medicamentos[i] > 0) || (indicaciones[i] != "") || (dias[i] > 0) || (dosis[i] > 0)) {
					if ((cantidades[i] > 0) && (dias[i] > 0) && (medicamentos[i] > 0) && (indicaciones[i] != "") && (dias[i] > 0) && (dosis[i] > 0)) {
						cantidadesEnviar.push(cantidades[i]);
						unidadesEnviar.push(unidades[i]);
						diasEnviar.push(dias[i]);
						diasTratamientoEnviar.push(diasTratamiento[i]);
						medicamentosEnviar.push(medicamentos[i]);
						indicacionesEnviar.push(quitarComas(indicaciones[i]));
						gruposEnviar.push(grupos[i]);
						numerosMed.push(i);
						if (isNaN(dosis[i])) error += '- La dosis debe ser un numero\n\r';
						if ((grupos[i] == "2") && (cantidades[i] > 2)) error += '- Los medicamentos del grupo 2 no se pueden recetar por mas de 2 cajas\n\r';
					} else {
						error += "- Complete los datos del medicamento " + i + "\n\r";
					}
					if (medicamentos[i] > 0) {
						if (medicamentos[i] == medicamentos[i+1]) {
							error += "- Seleccione medicamentos diferentes\n\r"
						}
					}
				}
				
			}
			if (error == "") {
				tMedicamentos = medicamentosEnviar.length;
				if (tMedicamentos > 0) {
					serie = document.getElementById("serie").value;
					folio = document.getElementById("folio").value;
					tipo_receta = document.getElementById("tipo_receta").value;
					var contenedor2;
					contenedor2 = document.getElementById('enviando');
					var objeto= new AjaxGET();
					objeto.open("GET", quitarAcentos("agregarRecetaDobleConfirmar.php?tipo_receta=" + tipo_receta + "&cantidades=" + cantidadesEnviar.toString() + "&unidades=" + unidadesEnviar.toString() + "&dias=" + diasEnviar.toString() + "&diasTratamiento=" + diasTratamientoEnviar.toString() + "&medicamentos=" + medicamentosEnviar.toString() + "&indicaciones=" + indicacionesEnviar.toString() + "&numerosMed=" + numerosMed.toString() + "&grupos=" + gruposEnviar.toString() + "&id_derecho=" + id_derecho + "&serie="+serie+"&folio="+folio+"&diagnostico="+diagnostico),true);
					objeto.onreadystatechange=function()
					{
						if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
						{
							resp = objeto.responseText.split("|");
							contenedor2.innerHTML = resp[0];
							if (resp[1] == '') {
								document.getElementById('agregar').disabled = '';
								document.getElementById('regresar').disabled = '';
							} else {
								
								if (resp[2] != '0') ventana2 = window.open("recetasPRINTnew.php?id_receta1=" + resp[1] + "&id_receta2=" + resp[2],"ventanita2"); else ventana = window.open("recetasPRINTnew.php?id_receta1=" + resp[1],"ventanita");
								document.getElementById('regresar').disabled = '';
							}
						}
						if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
						{
							document.getElementById('agregar').disabled = 'disabled';
							document.getElementById('regresar').disabled = 'disabled';
							contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
						}
					}
					objeto.send(null)
				} else {
					alert('- Llene al menos los datos de un medicamento');
				}
			}else {
				alert(error);
			}
		}
	}
}

function capturarRec2014() {
	var contenedor;
	contenedor = document.getElementById('contenido');
	var objeto= new AjaxGET();
	objeto.open("GET", "agregarRecetaApacente.php",true);
	objeto.onreadystatechange=function()
	{
		if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor.innerHTML = objeto.responseText;
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
		{
			contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	objeto.send(null)
}


function citaExtemporanea2014(fecha) {
	var contenedor2;
	contenedor2 = document.getElementById('contenido');
	var agenda= new AjaxGET();
	agenda.open("GET", "agregarCitaExtemporanea.php?getdate="+fecha,true);
	agenda.onreadystatechange=function()
	{
		if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			contenedor2.innerHTML = agenda.responseText;
			cargarEstados('estado');
			cargarEstados('estadoAgregar');
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
			$('#diagnostico2').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3
			}); 	
		}
		if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
		{
			contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	agenda.send(null)
}

function validarAgregarCitaExtemporanea2014() {
	var id_derecho = document.getElementById('id_derecho').value;
	var id_cita = document.getElementById('id_cita').value;
	var hora_inicio = document.getElementById('hora_inicio').value;
	var hora_fin = document.getElementById('hora_fin').value;
	var cedula = document.getElementById('cedula').value;
	var ap_p = document.getElementById('ap_p').value;
	var ap_m = document.getElementById('ap_m').value;
	var nombre = document.getElementById('nombre').value;
	var telefono = document.getElementById('telefono').value;
	var direccion =	document.getElementById('direccion').value;
	var estado = document.getElementById('estado').value;
	var municipio =	document.getElementById('municipio').value;
	var tipo_cita =	document.getElementById('tipo_cita').value;
	var diagnostico =	document.getElementById('diagnostico').value;
	var diagnostico2 =	document.getElementById('diagnostico2').value;
	var observaciones =	document.getElementById('observaciones').value;
	var validarHoraInicio = esHoraValida(hora_inicio,'Hora Inicio');
	if (diagnostico == '') {
		alert('El diagnóstico es requerido');
		return;
	}
	if (tipo_cita == "-1") {
		alert('Tipo de Cita es requerido');
	} else {
		if(validarHoraInicio == "") {
			var validarHoraFin = esHoraValida(hora_fin,'Hora Fin');
			if(validarHoraFin == "") {
				if (diagnostico2 != '') diagnostico = diagnostico + ' | ' + diagnostico2;
					var contenedor;
					contenedor = document.getElementById('contenido');
					var objeto= new AjaxGET();
					objeto.open("GET", "agregarCitaExtemporaneaConfirmar.php?id_derecho="+id_derecho+"&hora_inicio="+hora_inicio+"&hora_fin="+hora_fin+"&fechaCita="+document.getElementById('fechaCita').value+"&obs="+observaciones+"&diagnostico="+diagnostico+"&id_cita="+id_cita+"&tipo_cita="+tipo_cita,true);
					objeto.onreadystatechange=function()
					{
						if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
						{
							aRespuesta = objeto.responseText.split('|');
							tRespuesta = aRespuesta.length;
							if (tRespuesta> 2)
								citaExtemporaneaAgregada(objeto.responseText);
							else alert(objeto.responseText);
						}
						if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
						{
//							contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
						}
					}
					objeto.send(null);
			} else {
				alert(validarHoraFin);
			}
		} else {
				alert(validarHoraInicio);
		}
	}
}
