<?php

function regresarServicioConsultorio($idDr) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM servicios_x_consultorio WHERE id_medico='" . $idDr . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
	$ret = array(
		'id_consultorio' => $row_query['id_consultorio'],
		'id_servicio' => $row_query['id_servicio']
	);
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function claseParaDia($tipo) {
	$ret = '';
	if ($tipo == 0)
		$ret = "citaXdiaPRV";
	if ($tipo == 1)
		$ret = "citaXdiaSUB";
	if ($tipo == 2)
		$ret = "citaXdiaPRO";
	return $ret;
}
function getContraRef2013($idDerecho, $servicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
 
    $sql = "SELECT * FROM `referencia_contrarreferencia` WHERE `id_derecho`='$idDerecho' and `id_servicio`='$servicio' order by id_referencia DESC LIMIT 1";
    $query = mysql_query($sql);
    $row_query = mysql_fetch_assoc($query);
	$ret=array();		
    if (mysql_num_rows($query) > 0) {
        $ret = array(
            'id_referencia' => $row_query['id_referencia'],
            'id_derecho' => $row_query['id_derecho'],
            'id_servicio' => $row_query['id_servicio'],
            'cita_primera' => $row_query['cita_primera'],
            'citas_subsecuentes' => $row_query['citas_subsecuentes'],
            'status_cita' => $row_query['status_cita']
        );
	}
    return $ret;
}

function delContraRef2013($idDerecho, $servicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
 
    $sql = "DELETE FROM `referencia_contrarreferencia` WHERE `id_derecho`='$idDerecho' and `id_servicio`='$servicio'";
    $query = mysql_query($sql);
	return 'ok';
}

function getDetallesCita($id_cita, $tipo_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $conn = mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdissste);
	mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $conn);

 
    $sql = "SELECT * FROM `citas_detalles` WHERE `id_cita`='$id_cita' and `tipo_cita`='$tipo_cita' LIMIT 1";
    $query = mysql_query($sql);
    $row_query = mysql_fetch_assoc($query);
	$ret=array();		
    if (mysql_num_rows($query) > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_derecho' => $row_query['id_derecho'],
            'id_servicio' => $row_query['id_servicio'],
            'n_citas' => $row_query['n_citas'],
            'fecha_estudio' => $row_query['fecha_estudio'],
            'fecha_medicamento' => $row_query['fecha_medicamento'],
            'fecha_cita' => $row_query['fecha_cita'],
            'hizo_contra' => $row_query['hizo_contra'],
            'hizo_constancia' => $row_query['hizo_constancia'],
            'asistio' => $row_query['asistio'],
            'fecha' => $row_query['fecha'],
            'id_medico' => $row_query['id_medico'],
            'tipo_cita' => $row_query['tipo_cita'],
            'diagnostico' => $row_query['diagnostico']
        );
	}
    return $ret;
}

function LlenarContraReferencias2013($idDerecho, $idServicio, $fecha, $resumen, $diagnosticoRef, $diagnostico, $evolucion, $tratamiento, $recomendaciones, $idUnidad, $idMedico, $cont) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
//    $res = ejecutarSQL("UPDATE citas_detalles SET n_citas='0' WHERE id_derecho='" . $idDerecho . "' AND id_servicio='" . $idServicio . "' AND fecha='" . $fecha . "' LIMIT 1");
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
    $fechaPrv = ObtenerPrimeraCita($idDerecho, $idServicio);
    $referencia = getContraRef2013($idDerecho, $idServicio);
    $contra = ObtenerUltimaContra($idDerecho,$idServicio);
    if(count($contra)==0 || $contra['fecha_contrarreferencia']!=date("Ymd")){
        $sql = "insert into contrarreferencias values(NULL," . $referencia['id_referencia'] . ",$idDerecho,$idServicio,'" . $fecha . "','" . $fechaPrv['fecha_cita'] . "',1,'".quitarAcentos($resumen)."','".quitarAcentos($diagnosticoRef)."','".quitarAcentos($diagnostico)."','".quitarAcentos($evolucion)."','".quitarAcentos($tratamiento)."','".quitarAcentos($recomendaciones)."','$idUnidad','$idMedico','$cont',0,'');";
        $query = mysql_query($sql);
        $error = mysql_errno();
    	if(count($contra)==0 || $contra['fecha_contrarreferencia']!=date("Ymd")){
            $sql = "delete from referencia_contrarreferencia where id_derecho=" . $idDerecho . " AND id_servicio=" . $idServicio;
        }
        else
            $sql = "update referencia_contrarreferencia set citas_subsecuentes=0 where id_derecho=" . $idDerecho . " AND id_servicio=" . $idServicio;

/*
        $sql = "insert into contrarreferencias values(NULL," . $referencia['id_referencia'] . ",$idDerecho,$idServicio,'" . $fecha . "','" . $fechaPrv['fecha_cita'] . "',1,'".quitarAcentos($resumen)."','".quitarAcentos($diagnosticoRef)."','".quitarAcentos($diagnostico)."','".quitarAcentos($evolucion)."','".quitarAcentos($tratamiento)."','".quitarAcentos($recomendaciones)."','$idUnidad','$idMedico','$cont',0,'');";
        $query = mysql_query($sql);
        $error = mysql_errno();
        if ($cont == 0) {
            $sql = "update referencia_contrarreferencia set cita_primera=0,citas_subsecuentes=0 where id_derecho=" .$idDerecho . " AND id_servicio=" . $idServicio;
        }
        else
            $sql = "update referencia_contrarreferencia set citas_subsecuentes=0 where id_derecho=" . $idDerecho . " AND id_servicio=" . $idServicio;
*/
        $query = mysql_query($sql);
        $error = mysql_errno();
        return $error;
    }
    else
        return 1;
}

function actualizaFechaTratamiento($dias, $diasTrat) {
	$misDet = getDetallesCita($_SESSION['id_cita'], $_SESSION['tipo_cita']);
	$diasTrat = (int)$dias*(int)$diasTrat;
	$fechaTermino= date('Ymd', strtotime('+' . $diasTrat . ' day')) ;
	if ($fechaTermino > $misDet['fecha_medicamento']) {
		$res = ejecutarSQL("UPDATE citas_detalles SET fecha_medicamento='" . $fechaTermino . "' WHERE id_cita='" . $_SESSION['id_cita'] . "' AND tipo_cita='" . $_SESSION['tipo_cita'] . "' LIMIT 1");
		$res = ejecutarSQL("UPDATE citas_detalles2 SET hora_receta='" . date('Y-m-d H:i:s') . "' WHERE id_cita='" . $_SESSION['id_cita'] . "' AND tipo_cita='" . $_SESSION['tipo_cita'] . "' LIMIT 1");
	}
}

function citasXrangoFechas($fechaI, $fechaF) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE fecha_cita>='" . $fechaI . "' and fecha_cita<='" . $fechaF . "' ORDER BY fecha_cita ASC, id_horario ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_cita'],
                'id_horario' => $row_query['id_horario'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasExtemporaneasXrangoFechas($fechaI, $fechaF) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE fecha_cita>='" . $fechaI . "' and fecha_cita<='" . $fechaF . "' ORDER BY fecha_cita ASC, hora_inicio ASC, hora_fin ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_cita'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesCitaAgenda($id_cita, $tipo) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_detalles WHERE id_cita='" . $id_cita . "' AND tipo='" . $tipo . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'tipo' => $row_query['tipo'],
            'campo1' => $row_query['campo1'],
            'campo2' => $row_query['campo2'],
            'campo3' => $row_query['campo3']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesCitaAgendaInicio($fecha, $id_medico, $tipo) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_detalles WHERE fecha='" . $fecha . "' AND id_medico='" . $id_medico . "' AND tipo_cita='" . $tipo . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
          do {
			  $ret[] = array(
					'id_cita' => $row_query['id_cita'],
					'id_derecho' => $row_query['id_derecho'],
					'n_citas' => $row_query['n_citas'],
					'fecha_estudio' => $row_query['fecha_estudio'],
					'fecha_medicamento' => $row_query['fecha_medicamento'],
					'fecha_cita' => $row_query['fecha_cita'],
					'hizo_contra' => $row_query['hizo_contra'],
					'hizo_constancia' => $row_query['hizo_constancia'],
					'asistio' => $row_query['asistio'],
					'fecha' => $row_query['fecha'],
					'id_medico' => $row_query['id_medico'],
					'tipo_cita' => $row_query['tipo_cita'],
					'diagnostico' => $row_query['diagnostico']
				);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesCitaAgendaValidar($fecha, $id_medico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_detalles WHERE fecha='" . $fecha . "' AND id_medico='" . $id_medico . "' AND asistio=''";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = 0;
	$ret = $totalRows_query;
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesCitaAgendaValidar2($fecha, $id_medico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_detalles WHERE fecha='" . $fecha . "' AND id_medico='" . $id_medico . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
          do {
			  $ret[] = array(
					'id_cita' => $row_query['id_cita'],
					'id_derecho' => $row_query['id_derecho'],
					'n_citas' => $row_query['n_citas'],
					'fecha_estudio' => $row_query['fecha_estudio'],
					'fecha_medicamento' => $row_query['fecha_medicamento'],
					'fecha_cita' => $row_query['fecha_cita'],
					'hizo_contra' => $row_query['hizo_contra'],
					'hizo_constancia' => $row_query['hizo_constancia'],
					'asistio' => $row_query['asistio'],
					'fecha' => $row_query['fecha'],
					'id_medico' => $row_query['id_medico'],
					'tipo_cita' => $row_query['tipo_cita'],
					'diagnostico' => $row_query['diagnostico']
				);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServicioXidSM10($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM servicios WHERE id_servicio='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_servicio' => $row_query['id_servicio'],
            'clave' => $row_query['clave'],
            'nombre' => $row_query['nombre']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXderechoXdia($id_derecho, $id_medico, $id_servicio, $fecha) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $ret = array();
    $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "' AND id_medico='" . $id_medico . "' AND id_servicio='" . $id_servicio . "' AND fecha='" . $fecha . "' ORDER BY fecha DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function existeDuplicaFarmacia($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query = mysql_query($query_query, $bdissste) ;//or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDiagnostico2013($detallesCita) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
	$ret = '';
	if ($detallesCita['asistio'] == "SI") {
		if ($detallesCita['diagnostico'] == '') {
			$bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
			mysql_select_db($database_bdisssteR, $bdissste);
			if ($detallesCita['fecha_cita'] != '') {
				$query_query = "SELECT * FROM citas WHERE fecha_cita='" . $detallesCita['fecha_cita'] . "' AND id_derecho='" . $detallesCita['id_derecho'] . "' AND status='1' LIMIT 1";
				$query = mysql_query($query_query, $bdissste) or die(mysql_error());
				$row_query = mysql_fetch_assoc($query);
				$ret = $row_query['observaciones'];
				if ($ret == '') {
					$query_query = "SELECT * FROM citas_extemporaneas WHERE fecha_cita='" . $detallesCita['fecha_cita'] . "' AND id_derecho='" . $detallesCita['id_derecho'] . "' AND status='1' LIMIT 1";
					$query = mysql_query($query_query, $bdissste) or die(mysql_error());
					$row_query = mysql_fetch_assoc($query);
					$ret = $row_query['observaciones'];
				}
			}
			if ($detallesCita['hizo_contra'] == 'SI') {
				$query_query = "SELECT * FROM contrarreferencias WHERE fecha_contrarreferencia='" . $detallesCita['fecha'] . "' AND id_derecho='" . $detallesCita['id_derecho'] . "' AND id_servicio='" . $_SESSION['idServ'] . "' AND id_medico='" . $_SESSION['idDr'] . "' LIMIT 1";
				$query = mysql_query($query_query, $bdissste) or die(mysql_error());
				$row_query = mysql_fetch_assoc($query);
				$ret = $row_query['diagnostico'];
			}
			@mysql_free_result($query);
			@mysql_close($dbissste);
		} else {
			$ret = $detallesCita['diagnostico'];
		}
	}
	return $ret;
}

function getDiagnostico2014($detallesCita) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    global $alto_barcode;
    $ret = '';
    if ($detallesCita['asistio'] == "SI") {
        if ($detallesCita['diagnostico'] == '') {
            $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
            mysql_select_db($database_bdisssteR, $bdissste);
            if ($detallesCita['fecha_cita'] != '') {
                $query_query = "SELECT * FROM citas WHERE fecha_cita='" . $detallesCita['fecha_cita'] . "' AND id_derecho='" . $detallesCita['id_derecho'] . "' AND status='1' LIMIT 1";
                $query = mysql_query($query_query, $bdissste) or die(mysql_error());
                $row_query = mysql_fetch_assoc($query);
                $ret = $row_query['dxCie10'];
                if ($ret == '') {
                    $query_query = "SELECT * FROM citas_extemporaneas WHERE fecha_cita='" . $detallesCita['fecha_cita'] . "' AND id_derecho='" . $detallesCita['id_derecho'] . "' AND status='1' LIMIT 1";
                    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
                    $row_query = mysql_fetch_assoc($query);
                    $ret = $row_query['observaciones'];
                }
            }
            if ($detallesCita['hizo_contra'] == 'SI') {
                $query_query = "SELECT * FROM contrarreferencias WHERE fecha_contrarreferencia='" . $detallesCita['fecha'] . "' AND id_derecho='" . $detallesCita['id_derecho'] . "' AND id_servicio='" . $_SESSION['idServ'] . "' AND id_medico='" . $_SESSION['idDr'] . "' LIMIT 1";
                $query = mysql_query($query_query, $bdissste) or die(mysql_error());
                $row_query = mysql_fetch_assoc($query);
                $ret = $row_query['diagnostico'];
            }
            @mysql_free_result($query);
            @mysql_close($dbissste);
        } else {
            $ret = $detallesCita['diagnostico'];
        }
    }
    $retBien = '';
    $aDiagnosticos = explode(" || ", $ret);
    $tDiagnosticos = count($aDiagnosticos);
    if ($tDiagnosticos > 0) {
        $aCodigos = explode(" - ", $aDiagnosticos[0]);
        $cod = str_replace(".", "", $aCodigos[0]);
        if (strlen($cod) == 3) $cod .= '0';
        $retBien .= "<img src=\"barcode/barcode.php?code=" . $cod . "&tam=1\" style=\"height:" . $alto_barcode . "\"><br>";

    }
/*    
    for ($i=0; $i<$tDiagnosticos; $i++) {
        $aCodigos = explode(" - ", $aDiagnosticos[$i]);
        $retBien .= "<img src=\"barcode/barcode.php?code=" . $aCodigos[0] . "&tam=1\" style=\"height:" . $alto_barcode . "\"><br>";
    }
*/
    return $retBien;
}

function getConsultaCierre($fecha, $id_usuario, $id_medico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdissste);
 
    $sql = "SELECT * FROM `consulta_cierre` WHERE `fecha`='$fecha' and `id_usuario`='$id_usuario' and `id_medico`='$id_medico' LIMIT 1";
    $query = mysql_query($sql);
    $row_query = mysql_fetch_assoc($query);
	$ret=array();		
    if (mysql_num_rows($query) > 0) {
        $ret = array(
            'fecha' => $row_query['fecha'],
            'id_usuario' => $row_query['id_usuario'],
            'id_medico' => $row_query['id_medico'],
            'inicio_consulta' => $row_query['inicio_consulta'],
            'termino_consulta' => $row_query['termino_consulta'],
            'total_cierres' => $row_query['total_cierres'],
            'extra1' => $row_query['extra1'],
            'extra2' => $row_query['extra2']
        );
	}
    return $ret;
}

function botonDiagnostico($fecha, $id_usuario, $id_medico, $hizo_contra, $hizo_cita, $id_horario, $id_cita, $tipo_cita, $diagnostico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdissste);
 
    $sql = "SELECT * FROM `consulta_cierre` WHERE `fecha`='$fecha' and `id_usuario`='$id_usuario' and `id_medico`='$id_medico' LIMIT 1";
    $query = mysql_query($sql);
    $row_query = mysql_fetch_assoc($query);
	$ret= '';
    if (mysql_num_rows($query) > 0) {
		if (($row_query['extra2'] >= 1) && ($hizo_contra == '') && ($hizo_cita == '') && ($diagnostico == '')) $ret = '<input type="button" class="botones" onclick="hacerDiagnostico(\'' . $id_horario . '\',\'' . $id_cita . '\',\'' . $tipo_cita . '\')" value="DIAGNOSTICO">';
	}
    return $ret;
}

// 2014
function getIdRecetaConcertacionAgregada($fecha, $hora, $id_medico, $id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT id_receta FROM recetas_concertacion WHERE fecha='" . $fecha . "' AND hora='" . $hora . "' AND id_medico='" . $id_medico . "' AND id_derecho='" . $id_derecho . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "0";
    if ($totalRows_query > 0) {
        $ret = $row_query['id_receta'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}


?>