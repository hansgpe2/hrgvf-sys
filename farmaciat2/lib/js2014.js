function buscarRecetasDe(){
	if (document.getElementById("dh") != undefined) {
		resp = document.getElementById("dh").value.split("|");
		var contenedor2;
		contenedor2 = document.getElementById('recetas');
		var agenda= new AjaxGET();
		agenda.open("POST", "liberarMedicamentoBuscar.php?id_derecho=" + resp[0],true);
		agenda.onreadystatechange=function()
		{
			if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor2.innerHTML = agenda.responseText;
			}
			if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
			{
				contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
			}
		}
		agenda.send(null)
	} else {
		alert("Selecciona un derechohabiente");
	}
}

function validarLiberarReceta(id_receta) {
	var motivo = document.getElementById("motivo_"+id_receta).value;
	if (motivo == "") {
		alert("Motivo de liberacion es requerido");
	} else {
		var contenedor;
		contenedor = document.getElementById('recetas');
		var objeto= new AjaxGET();
		objeto.open("GET", "liberarMedicamentoDO.php?id_receta=" + id_receta + "&motivo=" + motivo,true);
		objeto.onreadystatechange=function()
		{
			if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor.innerHTML = objeto.responseText;
			}
			if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
			{
				contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
			}
		}
		objeto.send(null);
	}

}

var o=new Array("diez", "once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve", "veinte", "veintiuno", "veintidos", "veintitres", "veinticuatro", "veinticinco", "veintiseis", "veintisiete", "veintiocho", "veintinueve");
var u=new Array("cero", "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve");
var d=new Array("", "", "", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa");
var c=new Array("", "ciento", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos");
 
function nn(n)
{
  var n=parseFloat(n).toFixed(2); /*se limita a dos decimales, no sabía que existía toFixed() :)*/
  var p=n.toString().substring(n.toString().indexOf(".")+1); /*decimales*/
  var m=n.toString().substring(0,n.toString().indexOf(".")); /*número sin decimales*/
  var m=parseFloat(m).toString().split("").reverse(); /*tampoco que reverse() existía :D*/
  var t="";
 
  /*Se analiza cada 3 dígitos*/
  for (var i=0; i<m.length; i+=3)
  {
    var x=t;
    /*formamos un número de 2 dígitos*/
    var b=m[i+1]!=undefined?parseFloat(m[i+1].toString()+m[i].toString()):parseFloat(m[i].toString());
    /*analizamos el 3 dígito*/
    t=m[i+2]!=undefined?(c[m[i+2]]+" "):"";
    t+=b<10?u[b]:(b<30?o[b-10]:(d[m[i+1]]+(m[i]=='0'?"":(" y "+u[m[i]]))));
    t=t=="ciento cero"?"cien":t;
    if (2<i&&i<6)
      t=t=="uno"?"mil ":(t.replace("uno","un")+" mil ");
    if (5<i&&i<9)
      t=t=="uno"?"un millón ":(t.replace("uno","un")+" millones ");
    t+=x;
    //t=i<3?t:(i<6?((t=="uno"?"mil ":(t+" mil "))+x):((t=="uno"?"un millón ":(t+" millones "))+x));
  }
 
  t;
  /*correcciones*/
  t=t.replace("  "," ");
  t=t.replace(" cero","");
  //t=t.replace("ciento y","cien y");
  //alert("Numero: "+n+"\nNº Dígitos: "+m.length+"\nDígitos: "+m+"\nDecimales: "+p+"\nt: "+t);
  //document.getElementById("esc").value=t;
  return t;
}

function crearLicencia(id)
{
    var contenedor2;
    contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "generarLicencia.php?idHorario="+id,true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
			$('#ex2').jqmHide(); 
			$('#diagnostico').autocomplete({
			    serviceUrl: 'cie_10_simef.php',
			    minChars:3,
			    onSelect: function (suggestion) {
			    	$("#diagnostico_seleccionado").val(suggestion.value);
			    }
			}); 	
        }
        if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function validarFechaMenorA(date, today){
      var x=new Date();
      var fecha = date.split("-");
      x.setFullYear(fecha[2],fecha[1]-1,fecha[0]);
 
      if (x >= today)
        return false;
      else
        return true;
}

restaFechas = function(f1,f2)
 {
 var aFecha1 = f1.split('-');
 var aFecha2 = f2.split('-');
 var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
 var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
 var dif = fFecha2 - fFecha1;
 var dias = Math.floor(dif / (1000 * 60 * 60 * 24));
 return dias+1;
 }

 function getDiasOtorgados() {
  	document.getElementById('dias_otorgados').value = restaFechas(document.getElementById('date1').value, document.getElementById('date2').value);
 	var diasConLetra = nn(document.getElementById('dias_otorgados').value);
	document.getElementById('dias_otorgados_letra').value = diasConLetra;
 	document.getElementById("div_dias_otorgados").innerHTML = document.getElementById('dias_otorgados').value;
 	document.getElementById("div_dias_otorgados_letra").innerHTML = diasConLetra;
 }

function genLicencia() {
	var idHorario = document.getElementById('idHorario').value;
	var idDerecho = document.getElementById('idDerecho').value;
	var idDr = document.getElementById('idDr').value;
	var idServicio = document.getElementById('idServicio').value;
	var dias_otorgados = document.getElementById('dias_otorgados').value;
	var dias_otorgados_letra = document.getElementById('dias_otorgados_letra').value;
	var fecha_inicio = document.getElementById('date1').value;
	var fecha_termino = document.getElementById('date2').value;
	var dependencia = document.getElementById('dependencia').value;
	var unidad = document.getElementById('unidad').value;
	var diagnostico = document.getElementById('diagnostico').value;
	var diagnostico_seleccionado = document.getElementById('diagnostico_seleccionado').value;
	var motivo_licencia = document.getElementById('motivo_licencia').value;
	var caracter_licencia = document.getElementById('caracter_licencia').value;
	var servicio_otorgado = document.getElementById('servicio_otorgado').value;
	var numero_oficio = document.getElementById('numero_oficio').value;
    var today = new Date();

    if (caracter_licencia == "retroactiva") {
		// restamos 5 días
		var unDia = 24*60*60*1000;
		var diaValido=new Date(today.getTime() - (unDia*5));
		if (validarFechaMenorA(fecha_inicio, diaValido)) {
			alert('Solo se permiten 5 dias retroactivos, favor de enviar a clinica de adscripcion o cambiar fecha de inicio');
			return;
		}    	
    } else {
		if (validarFechaMenorA(fecha_inicio, today)) {
			alert('La fecha debe ser mayor o igual a la fecha actual');
			return;
		}    	
    }
    if (caracter_licencia == "excepcional") {
		if (numero_oficio == '') {
			alert('Ingresa el numero de oficio');
			return;
		}    	
    }
	var x=new Date();
	var fecha = fecha_inicio.split("-");
	x.setFullYear(fecha[2],fecha[1]-1,fecha[0]);
	if (validarFechaMenorA(fecha_termino, today)) {
		alert('La fecha debe ser mayor o igual a la fecha de inicio');
		return;
	}
	if (motivo_licencia == '') {
		alert('Selecciona el motivo de la licencia');
		return;
	}
	if (diagnostico_seleccionado == '') {
		alert('Selecciona el diagnostico de la licencia');
		return;
	}
	if (caracter_licencia == '') {
		alert('Selecciona el carácter de la licencia');
		return;
	}
	if (servicio_otorgado == '') {
		alert('Selecciona el servicio otorgado');
		return;
	}
	var contenedor;
	contenedor = document.getElementById('enviando');
	var objeto= new AjaxGET();
	objeto.open("GET", "generarLicenciaConfirmar.php?idHorario="+idHorario+"&idDerecho="+idDerecho+"&idDr="+idDr+"&idServicio="+idServicio+"&dias_otorgados="+dias_otorgados+"&dias_otorgados_letra="+dias_otorgados_letra+"&fecha_inicio="+fecha_inicio+"&fecha_termino="+fecha_termino+"&dependencia="+dependencia+"&unidad="+unidad+"&diagnostico="+diagnostico+"&diagnostico_seleccionado="+diagnostico_seleccionado+"&motivo_licencia="+motivo_licencia+"&caracter_licencia="+caracter_licencia+"&servicio_otorgado="+servicio_otorgado+"&numero_oficio="+numero_oficio,true);
	objeto.onreadystatechange=function()
	{
		if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
		{
			resp = objeto.responseText.split("|");
			if (resp[0] == "ok") {
				alert('Licencia Medica agregada correctamente');
				ventana2 = window.open("licenciaMedicaPrint.php?id=" + resp[1],"ventanita2");
				inicio("inicioMedicoEsp.php");
			} else {
				alert(resp[1]);
			}
			document.getElementById('agregar').disabled = '';
			document.getElementById('regresar').disabled = '';
			contenedor.innerHTML = "";
		}
		if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
		{
			document.getElementById('agregar').disabled = 'disabled';
			document.getElementById('regresar').disabled = 'disabled';
			contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
		}
	}
	objeto.send(null)
}

function cambioCaracterLicencia(obj) {
	if(obj.value == "excepcional") {
		document.getElementById('div_num_oficio').style.display = "block";
	} else {
		document.getElementById('div_num_oficio').style.display = "none";
	}
}
