<?php
session_start ();
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');
?>
<?php

$ent_fed = 'Jalisco';
$uni_med = 'H.R.V.G.F';

error_reporting(E_ALL ^ E_NOTICE);

function tieneLicencia($idDerecho, $fecha_inicio, $fecha_termino) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = 0;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT fecha_termino FROM licencias WHERE idDerecho='" . $idDerecho . "' AND (fecha_inicio BETWEEN '" . $fecha_inicio . "' AND '" . $fecha_termino . "' OR fecha_termino BETWEEN '" . $fecha_inicio . "' AND '" . $fecha_termino . "' OR (fecha_inicio<'" . $fecha_inicio . "' AND fecha_termino>'" . $fecha_termino . "')) AND status='1' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query == 1) { // SI HAY UNA LICENCIA EN LAS FECHAS DADAS
        $row_query = mysql_fetch_assoc($query);
        if ($fecha_inicio != $row_query["fecha_termino"]) { // se valida que el registro en la bd en su fecha de termino sea igual al inicio enviado, ya que esto si sería válido (preguntar)
        	$ret = 1;
        }    	
	}
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getLicenciaRecienAgregada($idDerecho, $fecha) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = 0;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT id FROM licencias WHERE idDerecho='" . $idDerecho . "' AND fecha_creacion='" . $fecha . "' AND status='1' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query == 1) {
        $row_query = mysql_fetch_assoc($query);
       	$ret = $row_query["id"];
	}
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

if ((isset($_GET["caracter_licencia"])) && (isset($_GET["dias_otorgados"])) && (isset($_GET["dias_otorgados_letra"])) && (isset($_GET["fecha_inicio"])) && (isset($_GET["fecha_termino"])) && (isset($_GET["idDerecho"])) && (isset($_GET["idDr"])) && (isset($_GET["idHorario"])) && (isset($_GET["idServicio"])) && (isset($_GET["motivo_licencia"])) && (isset($_GET["servicio_otorgado"]))) {
	$afecha_inicio = explode('-',$_GET["fecha_inicio"]);
	$fecha_inicio = $afecha_inicio[2] . '-' . $afecha_inicio[1] . '-' . $afecha_inicio[0];
	$afecha_termino = explode('-',$_GET["fecha_termino"]);
	$fecha_termino = $afecha_termino[2] . '-' . $afecha_termino[1] . '-' . $afecha_termino[0];
	$tieneLicenciaActiva = tieneLicencia($_GET["idDerecho"], $fecha_inicio, $fecha_termino);
	if ($tieneLicenciaActiva == 1) {
		print("no|Ya existe una licencia que se traslapa con las fechas seleccionadas|");
	} else {
		$derecho = getDatosDerecho($_GET["idDerecho"]);
		$paciente = $derecho['ap_p'] . ' '. $derecho['ap_m'] . ' ' . $derecho['nombres'];
		$fechaInt = date("Y-m-d H:i:s");
		$sql = "INSERT INTO licencias VALUES(NULL, '', '" . $ent_fed . "','" . $uni_med . "','1420000000' ,'" . $derecho['cedula'] . "/" . $derecho['cedula_tipo'] . "','" . $paciente . "','" . $_GET["dependencia"] . "','" . $_GET["unidad"] . "','" . $_GET["diagnostico_seleccionado"] . "','" . $fechaInt . "','" . $fecha_inicio . "','" . $fecha_termino . "','" . $_GET["dias_otorgados"] . "','" . $_GET["dias_otorgados_letra"] . "','" . $_GET["motivo_licencia"] . "','" . $_GET["caracter_licencia"] . "','" . $_GET["servicio_otorgado"] . "','" . $_GET["idHorario"] . "','" . $_GET["idDerecho"] . "','" . $_GET["idDr"] . "','" . $_GET["idServicio"] . "','" . $_SESSION['idUsuario'] . "','1','" . $_GET["numero_oficio"] . "');";
		$res = ejecutarSQL($sql);
		$idLicencia = getLicenciaRecienAgregada($_GET["idDerecho"], $fechaInt);
		print("ok|" . $idLicencia . "|");
	}

} else {
	print("no|no existen las variables|");
}
?>
