<?php
error_reporting(E_ERROR|E_PARSE);
set_time_limit(600);
include_once('lib/misFunciones.php');
session_start ();

$datosRecetas = getRecetas("1");
$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="780">';
$out .= '<tr><td colspan="5" class="tituloVentana">Recetas sin surtir</td></tr>';
$out .= '<tr><th>Fecha y Hora</th><th>Folio</th><th>M&eacute;dico</th><th>Servicio</th><th>Derechohabiente</th></tr>';
$tDatos = count($datosRecetas);

for ($i=0; $i<$tDatos; $i++) {
	$datosMedico = getMedicoXid($datosRecetas[$i]['id_medico']);
	$datosServicio = getServicioXid($datosRecetas[$i]['id_servicio']);
	$datosDerecho = getDatosDerecho($datosRecetas[$i]['id_derecho']);
	$out .= '<tr class="botones_menu"><td>' . $datosRecetas[$i]['fecha'] . ' - ' . $datosRecetas[$i]['hora'] . '</td><td>' . $datosRecetas[$i]['serie'] . '' . $datosRecetas[$i]['folio'] . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td>' . ponerAcentos($datosServicio) . '</td><td>' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td></tr>';
}
$out .= '</table></center>';

print($out);

?>

