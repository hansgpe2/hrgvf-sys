<?php
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');

function getContraReferenciaValidar($id_derecho, $id_servicio, $id_medico, $fecha_contrarreferencia) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
    $sql = "select * from contrarreferencias where id_derecho='" . $id_derecho . "' AND id_servicio='" . $id_servicio . "' AND id_medico='" . $id_medico . "' AND fecha_contrarreferencia='" . $fecha_contrarreferencia . "' LIMIT 1";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_errno() == 0)
        if (mysql_num_rows($query)>0)
            $ret = mysql_fetch_array($query);

    return $ret;
}

date_default_timezone_set('America/Mexico_City');
$galleE = $_SESSION[$galleta_citasE];
$nombre = '';
$galle_actual = '';

foreach($galleE as $indice => $arregloActual) {
	if ($arregloActual['id_cita'] == $_GET['id_cita']) {
		$galle_actual = $indice;
		break;
	}
}
$hoyC = date('Ymd');

$referencia = getContraRef2013($_GET['id_derecho'], $_GET['id_servicio']);
$detalles = getDetallesCita($_GET['id_cita'], 'E');

$_SESSION['id_cita'] = $_GET['id_cita'];
$_SESSION['tipo_cita'] = 'E';
$nombre = $_SESSION[$galleta_citasE][$galle_actual]['nombre'];
if ($_SESSION[$galleta_citasE][$galle_actual]['asistio'] != 'SI') {
	$_SESSION[$galleta_citasE][$galle_actual]['asistio'] = 'SI';
	$res = ejecutarSQL("UPDATE citas_detalles SET asistio='SI', n_citas='" . $referencia['citas_subsecuentes'] . "' WHERE id_cita='" . $_GET['id_cita'] . "' AND tipo_cita='E' AND fecha='" . $hoyC . "' LIMIT 1");
	$res = ejecutarSQL("UPDATE citas_detalles2 SET hora_si_no='" . date('Y-m-d H:i:s') . "' WHERE id_cita='" . $_GET['id_cita'] . "' AND tipo_cita='E' LIMIT 1");
	$detalles = getDetallesCita($_GET['id_cita'], 'E');
}

$botonContra = '&nbsp;';
$botonCita = '&nbsp;';
$botonReceta = "&nbsp;";
$botonConstancia = "&nbsp;";

if ($detalles['n_citas'] >=4) { // CONTRAREFERENCIA AUTOMATICA
	if ($detalles['hizo_contra'] != 'SI') // SI NO HA HECHO SU CONTRAREFERENCIA AUTOMATICA LE APARECEMOS EL BOTON, UNA VEZ QUE HAGA LA CONTRAREFERENCIA YA NO APARECERÁ
		$botonContra = "<a href=\"javascript: crearReferenciaExt2013('" . $_GET['id_cita'] . "','" . $_GET['id_cita'] . "','E');\" title=\"Contrarreferencia\" class='botones_menu'><img src=\"images/advertencia.png\" border=\"0\" width='30' height='30'><br>Hacer Contrarreferencia</a>";
} else { // SE PUEDEN HACER LAS DEMAS COSAS
	if ($detalles['hizo_contra'] != 'SI') { // SI NO HA HECHO SU CONTRAREFERENCIA PUEDE TENER TODOS LOS BOTONES
		if ($detalles['fecha_cita'] == '') { // NO SE HA HECHO CONSTANCIA NI CITA SUBSECUENTE
			$botonContra = "<a href=\"javascript: crearReferenciaExt2013('" . $_GET['id_cita'] . "','" . $_GET['id_cita'] . "','E');\" title=\"Contrarreferencia\" class='botones_menu'><img src=\"images/Copy.png\" border=\"0\" width='30' height='30'><br>Hacer Contrarreferencia</a>";
			$botonCita = "<a href='javascript:crearCalendarioExt2013(\"" . $_GET['id_cita'] . "\");' class='botones_menu' title='Agendar Cita'><img src='diseno/agendarCita.png'  width='30' height='30'/><br>Agendar Cita Subsecuente</a>";
		}
	} else {
		$miContrarref = getContraReferenciaValidar($_GET['id_derecho'], $_GET['id_servicio'], $_SESSION['idDr'], $hoyC);
		if (count($miContrarref) > 0) {
			if ($miContrarref['status_citas'] == '1') $botonCita = "<a href='javascript:crearCalendario2013(\"" . $_GET['id_horario'] . "\");' class='botones_menu' title='Agendar Cita'><img src='diseno/agendarCita.png'  width='30' height='30'/><br>Agendar Cita Subsecuente</a>";
		}
	}
	$botonReceta = "<a href=\"javascript: void(0);\" title=\"Capturar Receta\" class='botones_menu agregarRecetaE' id=\"ag_" . $_GET['id_horario'] . "\"><img src=\"diseno/nuevaReceta.png\" border=\"0\" width='30' height='30'><br>Agregar Receta</a>";
	$botonConstancia = "<a href='javascript:agregarConstanciaE2013(" . $_GET['id_cita'] . ");' class='botones_menu'><img src='diseno/generarConstancia.png' width='30' height='30' /><br>Agregar Constancia</a>";
}

$botonDiagnostico = botonDiagnostico($hoyC, $_SESSION['idUsuario'], $_SESSION['idDr'], $detalles['hizo_contra'], $detalles['fecha_cita'], $_GET['id_horario'], $_GET['id_cita'], 'E', $detalles['diagnostico']);

echo '<h2 style="text-align:center;">' . ponerAcentos($nombre) . '</h2><p align="center">' . $botonDiagnostico . '</p>';
?>
<p align="center">
	<div style="float: left; margin-left:70px; margin-top:60px;"><a href="javascript: void(0);" onclick="crearLicencia('<?php echo $_GET['id_horario'] ?>');" class="botones_menu agregarReceta"><img src="diseno/licenciaMed.png" style="border: 0;" /><br>Licencia Médica</a></div>
	<div style="float: right; margin-right:170px;"><img src="lib/perfil.png" /></div>
<?php
echo "
		<table border='0' cellpadding='2' width='100%'><tr><td align='center' width='25%'>" . $botonContra . "</td><td align='center' width='25%'>" . $botonCita . "</td><td align='center' width='25%'>" . $botonReceta . "</td><td align='center' width='25%'>" . $botonConstancia . "</td></tr></table>
	";

?>
</p>
<input type="image" src="lib/close.gif" class="jqmClose" />