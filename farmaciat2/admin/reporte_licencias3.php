<?php
set_time_limit(600);
session_start ();
require_once("../lib/funcionesAdmin.php");

function getDerechohabienteXidOk($id) {
	global $hostname_bdissste;
	global $username_bdisssteR;
	global $password_bdisssteR;
	global $database_bdisssteR;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdisssteR, $bdissste);
	$query_query = "SELECT * FROM derechohabientes WHERE id_derecho='" . $id . "' AND st='1'";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	if ($totalRows_query>0){
			$ret=array(
					'id_derecho' => $row_query['id_derecho'],
					'cedula' => $row_query['cedula'],
					'cedula_tipo' => $row_query['cedula_tipo'],
					'ap_p' => $row_query['ap_p'],
					'ap_m' => $row_query['ap_m'],
					'nombres' => $row_query['nombres'],
					'fecha_nacimiento' => $row_query['fecha_nacimiento'],
					'telefono' => $row_query['telefono'],
					'direccion' => $row_query['direccion'],
					'estado' => $row_query['estado'],
					'municipio' => $row_query['municipio'],
					'status' => $row_query['status'],
					'st' => $row_query['st']
					);
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}

function getDatosMedicamentoEnReceta($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_concepto' => $row_query['id_concepto'],
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'cantidad' => $row_query['cantidad'],
                'unidad' => $row_query['unidad'],
                'dias' => $row_query['dias'],
                'indicaciones' => $row_query['indicaciones'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getLicenciasXRango($fecha_i, $fecha_f, $filtro, $orden) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "SELECT * FROM licencias WHERE fecha_inicio>='" . $fecha_i . "' AND fecha_inicio<='" . $fecha_f . "'" . $filtro . $orden;
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	$ret = array();
	if ($totalRows_query>0){
		do{
			$ret[]=$row_query;
		}while($row_query = mysql_fetch_assoc($query));
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return array($ret, $totalRows_query);
}


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if ((isset($_POST['mostrar'])) && (isset($_POST['fecha_de'])) && (isset($_POST['fecha_a'])) && (isset($_POST['caracter'])) && (isset($_POST['orden'])) && (isset($_POST['id']))) {
		$mostrar = $_POST['mostrar'];
		$fecha_de = $_POST['fecha_de'];
		$fecha_a = $_POST['fecha_a'];
		$caracter = $_POST['caracter'];
		$motivo = $_POST['motivo'];
		$tipo = $_POST['tipo'];
		$orden = $_POST['orden'];
		$id = $_POST['id'];
		
		$f_d = explode('/', $fecha_de);
		$fecha_de = $f_d[2] . $f_d[1] . $f_d[0];
		$f_a = explode('/', $fecha_a);
		$fecha_a = $f_a[2] . $f_a[1] . $f_a[0];
		
		$titulo = '';
		$filtro = "";
		switch ($mostrar) {
			case 'paciente':
				$derecho = getDerechohabienteXidOk($id);
				$titulo .= ponerAcentos($derecho['ap_p'] . ' ' . $derecho['ap_m'] . ' ' . $derecho['nombres']);
				$filtro .= " AND idDerecho='" . $id . "'";
				break;
			case 'servicio':
				$derecho = getServicioXid($id);
				$titulo .= ponerAcentos($derecho['nombre']);
				$filtro .= " AND idServicio='" . $id . "'";
				break;
			case 'medico':
				$derecho = getMedicoXid($id);
				$titulo .= ponerAcentos($derecho['ap_p'] . ' ' . $derecho['ap_m'] . ' ' . $derecho['nombres']);
				$filtro .= " AND idDr='" . $id . "'";
				break;
			case 'todas':
				$titulo .= 'todas las licencias';
				break;
		}
		switch ($caracter) {
			case 'inicial':
			case 'subsecuente':
			case 'retroactiva':
			case 'excepcional':
				$filtro .= " AND caracter_licencia='" . $caracter . "'";
				break;
		}
		switch ($motivo) {
			case 'PRT':
			case 'RT':
			case 'EG':
			case 'MPRE':
			case 'MPOS':
				$filtro .= " AND motivo_licencia='" . $motivo . "'";
				break;
		}
		switch ($tipo) {
			case 'consulta externa':
			case 'hospitalización':
			case 'urgencias':
				$filtro .= " AND servicio_otorgado='" . $tipo . "'";
				break;
		}
		$orden_txt = "";
		switch ($orden) {
			case 'fecha':
				$orden_txt .= " ORDER BY fecha_creacion";
				break;
			case 'servicio':
				$orden_txt .= " ORDER BY idServicio";
				break;
			case 'medico':
				$orden_txt .= " ORDER BY idDr";
				break;
			case 'dias':
				$orden_txt .= " ORDER BY " . $orden . " DESC";
				break;
			case 'dependencia':
			case 'diagnostico':
				$orden_txt .= " ORDER BY " . $orden;
				break;
			case 'unidad adva':
				$orden_txt .= " ORDER BY unidad_adva";
				break;
		}
		
//		$recetas = getRecetasXRango($fecha_de, $fecha_a, $filtro, $orden_txt);
		$licencias = getLicenciasXRango($fecha_de, $fecha_a, $filtro, $orden_txt);
		$out = '';
		$cont = 0;
		$colores = array('#FFFFFF', '#DDDDDD');
		if ($licencias[1] > 0) {
			$out .= '<table border="1" cellpadding="3" cellspacing="0" width="1300" style="font-size: 10px;">';
			$out .= '<tr><td colspan="14" class="tituloVentana">Licencias de: ' . $titulo . '</td></tr>';
			$out .= '<tr><th>F. Creación</th><th>Cédula</th><th>Derechohabiente</th><th>Dependencia</th><th>Unidad Adva.</th><th>Diagnóstico</th><th>F. Inicio</th><th>F. Termino</th><th>Días</th><th>Caracter</th><th>Motivo</th><th>Servicio O.</th><th>M&eacute;dico</th><th>Servicio</th></tr>';
			foreach ($licencias[0] as $index => $licencia) {
				
				$datosMedico = getMedicoXid($licencia['idDr']);
				$datosServicio = getServicioXid($licencia['idServicio']);
				$medico = $datosMedico["ap_p"] . ' ' . $datosMedico["pap_m"] . ' ' . $datosMedico["nombre"];
				$servicio = $datosServicio["nombre"];
				$out .= '<tr><td>' . $licencia['fecha_creacion'] . '</td><td>' . $licencia['cedula'] . '</td><td>' . $licencia['paciente'] . '</td><td>' . $licencia['dependencia'] . '</td><td>' . $licencia['unidad_adva'] . '</td><td>' . $licencia['diagnostico'] . '</td><td>' . $licencia['fecha_inicio'] . '</td><td>' . $licencia['fecha_termino'] . '</td><td>' . $licencia['dias'] . '</td><td>' . $licencia['caracter_licencia'] . '</td><td>' . $licencia['motivo_licencia'] . '</td><td>' . $licencia['servicio_otorgado'] . '</td><td>' . $medico . '</td><td>' . $servicio . '</td></tr>';
			}
			$out .= '</table>';
		}else {
			$out .= '<p>0 Licencias Médicas</p>';
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Recetas</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/datepicker.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
	.ul_reporte {
		list-style:none;
		padding:0px;
		margin:0px;
	}
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
	  <tr>
	    <td colspan="2" align="center">Seleccionaste Mostrar Licencias Médicas <b><?php echo $_POST['status'] ?></b> de <b><?php echo $_POST['mostrar'] ?></b> desde <b><?php echo $_POST['fecha_de'] ?></b> hasta <b><?php echo $_POST['fecha_a'] ?></b><br />
	      Carácter de la Licencia: <b><?php echo $_POST['caracter'] ?></b><br>
	      Motivo de la Licencia: <b><?php echo $_POST['motivo'] ?></b><br>
	      Tipo de Servicio Otorgado: <b><?php echo $_POST['tipo'] ?></b><br>
	      se ordenar&aacute; el reporte por <b><?php echo $_POST['orden'] ?></b></td>
	  </tr>
    <tr><td align="center">
		<?php echo $out ?>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php
	} else {
		echo "<script  language=\"javascript\" type=\"text/javascript\">alert('error en variables'); history.back();</script>";
	}
} ?>