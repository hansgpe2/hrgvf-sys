<?php
session_start ();
require_once("../lib/funcionesAdmin.php");

$tipoMedicamento = array("","GRUPO 1","GRUPO 2","GRUPO 3","GRUPO 4","GRUPO 5","GRUPO 6"); // grupo 5 medicamentos que se asignan a x paciente

if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if (isset($_GET['id_medicamento'])) {
		$res = getDatosMedicamentos($_GET['id_medicamento']);
		$codigo = $res['codigo_barras'];
		$nombre = $res['descripcion'];
		$presentacion = $res['presentacion'];
		$unidad = $res['unidad'];
		$caducidad = formatoHora($res['tiempo_cad_remanente']);
		$tipo = $res['extra1'];
		$total = count($tipoMedicamento);
		$opciones_tipo = "";
		for ($i=0; $i<$total; $i++) {
			$temp = "";
			if ($i == $tipo) $temp = " selected=\"selected\"";
			$opciones_tipo.= "<option value=\"" . $i . "\"" . $temp . ">" . $tipoMedicamento[$i] . "</option>";
		}


		$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
		mysql_select_db($database_bdissste, $bdissste);
		mysql_query("SET CHARACTER SET utf8"); 
		mysql_query("SET NAMES utf8"); 
		$query_query = "SELECT * FROM medicamentos_leyendas WHERE id_medicamento='" . $_GET['id_medicamento'] . "' LIMIT 1";
		$query = mysql_query($query_query, $bdissste) or die(mysql_error());
		$row_query = mysql_fetch_assoc($query);
		$totalRows_query = mysql_num_rows($query);
		$leyenda = "";
		if ($totalRows_query>0){
				$leyenda = $row_query['leyenda'];
		}
		@mysql_free_result($query);
		@mysql_close($dbissste);

	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No existe la variable id del servicio'); history.back();</script>";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Recetas</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="medicamentos_modificar_confirmar.php" onsubmit="MM_validateForm('codigo','','NisNum','presentacion','','RisNum','unidad','','R','nombre','','R');return document.MM_returnValue" method="post" name="forma">
        <input name="id_medicamento" type="hidden" id="id_medicamento" value="<?php echo $_GET['id_medicamento'] ?>" />
    	<table class="ventana">
          <tr class="tituloVentana">
            <td colspan="2">Modificar Medicamento</td>
          </tr>
		  <tr>
		    <td>Id. del Medicamento</td>
		    <td><b><?php echo $_GET['id_medicamento'] ?></b></td></tr>
		  <tr>
		    <td>Código de Barras:</td>
		    <td><input name="codigo" type="text" size="18" maxlength="18" id="codigo" value="<?php echo $codigo ?>" /></td></tr>
		  <tr><td>Nombre:</td>
		  <td><textarea name="nombre" cols="40" rows="5" id="nombre"><?php echo $nombre ?></textarea></td></tr>
		  <tr>
		    <td>Cantidad:</td>
		    <td><input name="presentacion" type="text" size="15" maxlength="15" id="presentacion" value="<?php echo $presentacion ?>" /></td></tr>
		  <tr>
		    <td>Unidad:</td>
		    <td><select name="unidad">
			  <option value="<?php echo $unidad ?>" selected="selected"><?php echo $unidad ?></option>
		      <option value="TABLETAS">TABLETAS</option>
		      <option value="MLS">MLS</option>
		      <option value="AMPOLLETAS">AMPOLLETAS</option>
		      <option value="PIEZAS">PIEZAS</option>
		      <option value="PARCHES">PARCHES</option>
		      <option value="DOSIS">DOSIS</option>
		      <option value="APLICADORES">APLICADORES</option>
		      <option value="JERINGAS">JERINGAS</option>
		    </select></td></tr>
		  <tr>
		    <td>Grupo de Medicamento:</td>
		    <td><select name="tipo" id="tipo"> <?php echo $opciones_tipo ?>
		      </select>
		    </td>
		  </tr>
		  <tr>
		    <td>Leyenda de Medicamento<br />(vacío si no lleva):</td>
		    <td><textarea name="leyenda" cols="60" rows="2" id="leyenda"><?php echo $leyenda ?></textarea></td></tr>
		  <tr><td align="center" colspan="2"><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" onclick="" value="Modificar Medicamento" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php 
} ?>