<?php
set_time_limit(600);
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Recetas</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/datepicker.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
	.ul_reporte {
		list-style:none;
		padding:0px;
		margin:0px;
	}
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
<script>
	function validar() {
		if ($('#fecha_de').val() == '') {
			alert('Debes seleccionar la fecha inicial');
			$('#fecha_de').focus();
			return false;
		}
		if ($('#fecha_a').val() == '') {
			alert('Debes seleccionar la fecha final');
			$('#fecha_a').focus();
			return false;
		}
		var aFecha_inicio = $('#fecha_de').val().split('/');
		var aFecha_fin = $('#fecha_a').val().split('/');
		var fec_ini = aFecha_inicio[2] + '' + aFecha_inicio[1] + '' + aFecha_inicio[0];
		var fec_fin = aFecha_fin[2] + '' + aFecha_fin[1] + '' + aFecha_fin[0];
		if (parseInt(fec_ini) > parseInt(fec_fin)) {
			alert('La fecha final debe ser mayor que la fecha inicial');
			$('#fecha_a').focus();
			return false;
		}
		var miRadio3= "";
		$("input[name='orden']").each(function(i){
			if ($(this).is(':checked')) {
				miRadio3 = $(this).val();
			}
		});
		if (miRadio3 == "") {
			alert('Debes seleccionar el orden de las licencias medicas que quieres mostrar');
			$("input[name='orden']").focus();
			return false;
		}
		var miRadio1= "";
		$("input[name='mostrar']").each(function(i){
			if ($(this).is(':checked')) {
				miRadio1 = $(this).val();
			}
		});
		if (miRadio1 == 'todas')
			$('#forma').attr('action','reporte_licencias3.php');
		else 
			$('#forma').attr('action','reporte_licencias2.php');
		$('#forma').submit();
	}

	$(document).ready(function() {
		$('.inputDate').DatePicker({
			format:'d/m/Y',
			date: '<?php echo date('d/m/Y'); ?>',
			current: '<?php echo date('d/m/Y'); ?>',
			starts: 1,
			position: 'right',
			onBeforeShow: function(){
				myId = $(this).attr('id');
			},
			onChange: function(formated, dates){
				$('#' + myId).val(formated);
			}
		});
	
	});
</script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="reporte_licencias2.php" method="post" name="forma" id="forma">
        	<input type="hidden" id="id" name="id" value="" />
        <br /><br /><br />
    	<table class="ventana" width="650">
          <tr class="tituloVentana">
            <td colspan="2">Configura el Reporte de Licencias Médicas</td>
          </tr>
		  <tr>
		    <td width="250" height="129">Mostrar Licencias Médicas de:</td>
		    <td>
            	<ul class="ul_reporte">
                	<li>
                    	<input type="radio" id="mostrar1" name="mostrar" value="paciente" />Paciente
                    </li>
                	<li>
                    	<input type="radio" id="mostrar2" name="mostrar" value="servicio" />Servicio
                    </li>
                	<li>
                    	<input type="radio" id="mostrar3" name="mostrar" value="medico" />M&eacute;dico
                    </li>
                	<li>
                    	<input type="radio" id="mostrar4" name="mostrar" value="todas" checked="checked" />Todas
                    </li>
                </ul>
            </td>
          </tr>
		  <tr>
		  	<td height="62">Rango de Fechas:</td>
		  	<td>
            	De <input type="text" name="fecha_de" id="fecha_de" value="" class="inputDate" readonly="readonly" /> a <input type="text" name="fecha_a" id="fecha_a" value="" class="inputDate" readonly="readonly" /> 
		  	</td>
          </tr>
		  <tr>
		    <td height="107">Carácter de la Licencia:</td>
		    <td>
            	<ul class="ul_reporte">
                	<li>
                    	<input type="radio" id="caracter1" name="caracter" value="inicial" />Inicial
                    </li>
                	<li>
                    	<input type="radio" id="caracter2" name="caracter" value="subsecuente" />Subsecuente
                    </li>
                	<li>
                    	<input type="radio" id="caracter3" name="caracter" value="retroactiva" />Retroactiva
                    </li>
                	<li>
                    	<input type="radio" id="caracter4" name="caracter" value="excepcional" />Excepcional
                    </li>
                	<li>
                    	<input type="radio" id="caracter5" name="caracter" value="todas" checked="checked" />Todas
                    </li>
                </ul>
            </td>
          </tr>
		  <tr>
		    <td height="107">Motivo de la Licencia:</td>
		    <td>
            	<ul class="ul_reporte">
                	<li>
                    	<input type="radio" id="motivo1" name="motivo" value="PRT" />Posible Riesgo de Trabajo
                    </li>
                	<li>
                    	<input type="radio" id="motivo2" name="motivo" value="RT" />Riesgo de Trabajo
                    </li>
                	<li>
                    	<input type="radio" id="motivo3" name="motivo" value="EG" />Enfermedad General
                    </li>
                	<li>
                    	<input type="radio" id="motivo4" name="motivo" value="MPRE" />Maternidad Pre-parto
                    </li>
                	<li>
                    	<input type="radio" id="motivo5" name="motivo" value="MPOS" />Maternidad Post-parto
                    </li>
                	<li>
                    	<input type="radio" id="motivo6" name="motivo" value="todas" checked="checked" />Todos
                    </li>
                </ul>
            </td>
          </tr>
		  <tr>
		    <td height="107">Tipo de Servicio Otorgado:</td>
		    <td>
            	<ul class="ul_reporte">
                	<li>
                    	<input type="radio" id="tipo1" name="tipo" value="consulta externa" />Consulta Externa
                    </li>
                	<li>
                    	<input type="radio" id="tipo2" name="tipo" value="hospitalización" />Hospitalización
                    </li>
                	<li>
                    	<input type="radio" id="tipo3" name="tipo" value="urgencias" />Urgencias
                    </li>
                	<li>
                    	<input type="radio" id="tipo4" name="tipo" value="todas" checked="checked" />Todos
                    </li>
                </ul>
            </td>
          </tr>
		  <tr>
		  	<td height="112">Ordenar por:</td>
		  	<td>
            	<ul class="ul_reporte">
                	<li>
                    	<input type="radio" id="orden1" name="orden" value="fecha" />Fecha
                    </li>
                	<li>
                    	<input type="radio" id="orden2" name="orden" value="servicio" />Servicio
                    </li>
                	<li>
                    	<input type="radio" id="orden3" name="orden" value="medico" />M&eacute;dico
                    </li>
                	<li>
                    	<input type="radio" id="orden4" name="orden" value="dias" />Días Otorgados
                    </li>
                	<li>
                    	<input type="radio" id="orden5" name="orden" value="dependencia" />Dependencia
                    </li>
                	<li>
                    	<input type="radio" id="orden6" name="orden" value="unidad adva" />Unidad Adva.
                    </li>
                	<li>
                    	<input type="radio" id="orden7" name="orden" value="diagnostico" />Diagnóstico
                    </li>
                </ul>
		  	</td>
          </tr>
		  <tr><td align="center" colspan="2"><br><br>
		       <input name="agregar" type="button" class="botones" id="agregar" onclick="javascript:validar();" value="Generar Reporte" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php 
} ?>