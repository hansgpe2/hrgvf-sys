<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Citas_model extends CI_Model {

	private $farmacia;

	public function __construct()
	{
		parent::__construct();
		$this->farmacia=$this->load->database('farmacia',true);
	}

	public function getCita($idHorario,$fecha_cita)
	{
		$this->db->where('id_horario', $idHorario);
		$this->db->where('fecha_cita', $fecha_cita);
		$query=$this->db->get_where('citas');
		return $query->row_array();
	}

	public function getCitaExt($idcita)
	{
		$this->db->where('id_cita', $idcita);
		$query=$this->db->get_where('citas_extemporaneas');
		return $query->row_array();	
	}

	public function getDerecho($idDerecho)
	{
		$this->db->where('id_derecho', $idDerecho);
		$dh=$this->db->get('derechohabientes');
		if($dh->num_rows()>0)
		{
			return $dh->row_array();
		}
		else
		{
			return array(
            'cedula' => "-1",
            'cedula_tipo' => "-1",
            'ap_p' => "-1",
            'ap_m' => "-1",
            'nombres' => "-1",
            'telefono' => "-1",
            'fecha_nacimiento' => "-1",
            'direccion' => "-1",
            'estado' => "-1",
            'municipio' => "-1",
            'status' => "-1");
		}
	}

	public function servicioXConsultorio($idDr)
	{
		$this->db->select('id_servicio');
		$this->db->where('id_medico', $idDr);
		$query=$this->db->get('servicios_x_consultorio');
		return $query->row_array();
	}

	public function datosServicio($idServicio)
	{
		$this->db->where('id_servicio', $idServicio);
		$query=$this->db->get('servicios');
		return $query->row_array();
	}

	public function datos_medico($idMed)
	{
		$this->db->where('id_medico', $idMed);
		$query=$this->db->get('medicos');
		return $query->row_array();
	}

	public function anexar_detalles()
	{
		$data = array('hizo_constancia'=>'SI');
		$where = array('id_cita' => $this->session->userdata('id_cita'),
		'tipo_cita'=>$this->session->userdata('tipo_cita'));
		$this->farmacia->update('citas_detalles', $data,$where);
		$data2 = array('hora_constancia' =>  date('Y-m-d H:i:s'));
		$this->farmacia->update('citas_detalles2', $data2,$where);
	}

	public function getCitasDetalles($idcita,$tipoCita)
	{
		$this->farmacia->where('id_cita', $idcita);
		$this->farmacia->where('tipo', $tipoCita);
		$query=$this->farmacia->get('citas_detalles');
		return $query->row_array();
	}

	public function a_servicios()
	{
		$qServicio=$this->db->get('servicios');
		$addw=array();
		foreach ($qServicio->result() as $row) {
			$addw[$row->id_servicio]=$row->nombre;
		}
		return $addw;
	}

	public function a_medicos()
	{
		$qMedicos=$this->db->get('medicos');
		$addw=array();
		foreach ($qMedicos->result() as $row) {
			$addw[$row->id_medico]=$row->titulo.' '.$row->nombres.' '.$row->ap_p.' '.$row->ap_m;
		}
		return $addw;
	}

	public function a_derechohabientes()
	{
		$qMedicos=$this->db->get('derechohabientes');
		$addw=array();
		foreach ($qMedicos->result() as $row) {
			$addw[$row->id_derecho]['cedula']=$row->cedula.'/'.$row->cedula_tipo;
			$addw[$row->id_derecho]['nombre']=$row->titulo.' '.$row->nombres.' '.$row->ap_p.' '.$row->ap_m;
		}
		return $addw;
	}

	public function datos_horario($idhorario)
	{
		$this->db->where('id_horario', $idhorario);
		$query=$this->db->get('horarios');
		return $query->row_array();
	}



}

/* End of file citas_model.php */
/* Location: .//C/xampp/htdocs/farmaciat2/constancias/app/models/citas_model.php */