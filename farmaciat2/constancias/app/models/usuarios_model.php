<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

private $farmacia;
public function __construct()
{
	parent::__construct();
	$this->farmacia=$this->load->database('farmacia',true);	
}

public function datos_usuario($idUsuario)
{
	$this->farmacia->where('id_usuario', $idUsuario);
	$query=$this->farmacia->get('usuarios');
	return $query->row_array();
}
	

}

/* End of file usuarios_model.php */
/* Location: .//C/xampp/htdocs/farmaciat2/constancias/app/models/usuarios_model.php */