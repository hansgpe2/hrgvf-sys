<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Constancias_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function datos_constancia($id)
	{
		$this->db->where('id_constancia', $id);
		return $this->db->get('constancia_cita')->row_array();
	}

	public function generar_folio($idServ,$idDh)
	{
		$this->db->where('id_servicio', $idServ);
		$qServicio=$this->db->get('servicios');
		$dServicio=$qServicio->row_array();
		$num=date('Ymdhmi');
		return $dServicio['clave'].$num;
	}

	public function constancia_insert($data)
	{
		$data['folio']=$this->generar_folio($data['id_servicio'],$data['id_derecho']);
		$this->db->insert('constancia_cita', $data);
		$aff=$this->db->affected_rows();
		if($aff>0)
		{
			return array('retorno' => 1,'registro'=>$this->db->insert_id());
		}
		else
		{
			return $this->db->error();
		}
	}

	public function constancias_derecho($idDh)
	{
		$this->db->where('id_derecho', $idDh);
		return $this->db->get('constancia_cita');
	}

	public function constancia_folio($folio)
	{
		$this->db->like('folio',$folio);
		return $this->db->get('constancia_cita');
	}

}

/* End of file constancias_model.php */
/* Location: ./application/models/constancias_model.php */