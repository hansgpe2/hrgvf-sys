<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Constancias extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('citas_model');
		$this->load->model('usuarios_model');
		$array = array(
			'idDr' => $this->input->get('idDr'),
			'idUsuario'=>$this->input->get('idUser')
		);
		$this->session->set_userdata( $array );
		$this->load->library('funciones');
		if($this->input->get('idHorario')){
			$id_horario = $this->input->get('idHorario');
			$horario=$this->citas_model->datos_horario($id_horario);
	        $data['cita'] = $this->citas_model->getCita($id_horario, date('Ymd'));
	        $data['id_cita']=$data['cita']['id_cita'];
	        $data['medico'] = $this->citas_model->datos_medico($horario['id_medico']);
	        $data['servicio']=$this->citas_model->datosServicio($horario['id_servicio']);
	    	$data['usuario'] = $this->usuarios_model->datos_usuario($this->session->userdata('idUsuario'));
    	}
    	else {
    		$data['id_cita'] = $this->input->get('id_cita');
            $data['cita'] = $this->citas_model->getCitaExt($data['id_cita']);
            $data['medico'] = $this->citas_model->datos_medico($data['cita']['id_medico']);
	        $data['servicio']=$this->citas_model->datosServicio($data['cita']['id_servicio']);
	    	$data['usuario'] = $this->usuarios_model->datos_usuario($this->session->userdata('idUsuario'));
    	}
    	$data['derecho'] = $this->citas_model->getDerecho($data['cita']["id_derecho"]);
	    $array = array(
	    	'idServicio' => $data['servicio']['id_servicio'],
	    	'id_cita'=>$data['id_cita'],
	    	'tipo_cita'=>'N'
	    );
	    $this->session->set_userdata( $array );
	    
	    
	    $array = array(
	    	'tipoUsuario' => $data['usuario']['tipo_usuario']
	    );
	    
	    $this->session->set_userdata( $array );
		$this->load->view('v_generar_constancia', $data);
	}

	public function guardar_constancia()
	{
		$this->load->model('constancias_model');
		$this->load->model('citas_model');
		$data = array('id_servicio' =>$this->input->get('idServicio'),
			'id_derecho'=>$this->input->get('idDerecho'),
			'id_medico'=>$this->input->get('medico'),
			'id_usuario'=>$this->session->userdata('idUsuario'),
			'fecha_cita'=>$this->input->get('fecha_cita'),
			'hora_entrada'=>$this->input->get('hEnt'),
			'hora_salida'=>$this->input->get('hSalida'),
			'acompanante'=>$this->input->get('Acomp'));
		$query=$this->constancias_model->constancia_insert($data);
		$this->citas_model->anexar_detalles();
		header('Location:'.site_url('constancias/constancia/'.$query['registro']));
	}

	public function constancia($id,$pdf=1)
	{
		$this->load->model('constancias_model');
		$this->load->model('citas_model');
		$this->load->model('usuarios_model');
		$this->load->library('funciones');
		$this->load->library('codebar');
		$data['constancia']=$this->constancias_model->datos_constancia($id);
		$data['derecho'] = $this->citas_model->getDerecho($data['constancia']["id_derecho"]);
		$data['servicio'] = $this->citas_model->datosServicio($data['constancia']['id_servicio']);
	    $data['medico'] = $this->citas_model->datos_medico($data['constancia']['id_medico']);
	    $data['usuario'] = $this->usuarios_model->datos_usuario($data['constancia']['id_usuario']);	
	    if($pdf==1)
	    {
	    	$this->load->library('pdf');
    		$pdf = $this->pdf->load();
	    	$html=$this->load->view('constancia', $data,true);
	    	$pdf->WriteHTML($html); // write the HTML into the PDF
    		$pdf->Output();

	    }
	    else {
	    		$this->load->view('constancia', $data);    	
	        }    
	    
	}

	public function buscar_constancias()
	{
		$this->load->view('v_buscar_constancias');
	}

	public function constancias_derecho($id)
	{
		$this->load->model('constancias_model');
		$this->load->model('citas_model');
		$this->load->library('funciones');
		
		$data['constancias']=$this->constancias_model->constancias_derecho($id);
		$data['servicios']=$this->citas_model->a_servicios();
		$data['medicos']=$this->citas_model->a_medicos();
		
		$this->load->view('v_constancias', $data);
	}

	public function buscar_constancias_folio($folio)
	{
		$this->load->model('constancias_model');
		$this->load->model('citas_model');
		$this->load->library('funciones');
		$data['constancias']=$this->constancias_model->constancia_folio($folio);
		$data['servicios']=$this->citas_model->a_servicios();
		$data['medicos']=$this->citas_model->a_medicos();
		$this->load->view('v_constancias', $data);	
	}


}

/* End of file constancias.php */
/* Location: .//C/xampp/htdocs/farmaciat2/constancias/app/controllers/constancias.php */