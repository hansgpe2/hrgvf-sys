<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CodigoBarras extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function codigo_barras($valor)
	{
		$this->load->library('codebar');
		$colorFront = new BCGColor(0, 0, 0);
		$colorBack = new BCGColor(255, 255, 255);
		$font = new BCGFontFile($this->codebar->font, 18);
		$code = new BCGcode128();

		$code = new BCGcode128();
		$code->setScale(2); // Resolution
		$code->setThickness(30); // Thickness
		$code->setForegroundColor($colorFront); // Color of bars
		$code->setBackgroundColor($colorBack); // Color of spaces
		$code->setFont($font); // Font (or 0)
		$code->parse($valor); // Text
		$drawing = new BCGDrawing(null,$colorBack);
		$drawing->setBarcode($code);
		$drawing->draw();
		header('Content-Type: image/png');
		return $drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
	}

}

/* End of file codigoBarras.php */
/* Location: ./application/controllers/codigoBarras.php */