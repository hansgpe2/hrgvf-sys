<?php 


/**
 * Codigo de barras
 */
class Codebar
{
    /**
     * Codigo de barras
     */
    public $font;
    public function __construct()
    {
        require_once(APPPATH.'third_party/barcode/class/BCGFontFile.php');
		require_once(APPPATH.'third_party/barcode/class/BCGColor.php');
		require_once(APPPATH.'third_party/barcode/class/BCGDrawing.php');
		require_once(APPPATH.'third_party/barcode/class/BCGcode128.barcode.php');
		$this->font=APPPATH.'third_party/barcode/font/Arial.ttf';
    }

}
 ?>
