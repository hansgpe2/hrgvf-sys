<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class CodigoBarras{

	public function __construct()
	{
		$dir=getcwd();
		$dir.='\\app\\third_party\\barcode\\php-barcode.php';
		include($dir);
	}
	public function generar_codigo($valor,$code='code128')
	{
		  $fontSize = 10;   // GD1 in px ; GD2 in point
		  $marge    = 10;   // between barcode and hri in pixel
		  $x        = 125;  // barcode center
		  $y        = 125;  // barcode center
		  $height   = 50;   // barcode height in 1D ; module size in 2D
		  $width    = 2;    // barcode height in 1D ; not use in 2D
		  $angle    = 90;   // rotation in degrees : nb : non horizontable barcode might not be usable because of pixelisation

		  $im     = imagecreatetruecolor(300, 300);
		  $black  = ImageColorAllocate($im,0x00,0x00,0x00);
		  $white  = ImageColorAllocate($im,0xff,0xff,0xff);
		  $red    = ImageColorAllocate($im,0xff,0x00,0x00);
		  $blue   = ImageColorAllocate($im,0x00,0x00,0xff);
		  imagefilledrectangle($im, 0, 0, 300, 300, $white);
		  $data = Barcode::gd($im, $black, $x, $y, $angle, $code, array('code'=>$valor), $width, $height);
		  return $data;
	}


}

/* End of file barcode.php */
/* Location: ./application/controllers/barcode.php */