<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funciones{

	function tituloDia($dia_num) {
    switch ($dia_num) {
        case 0: return "<abbr title=\"Domingo\">Domingo</abbr>";
            break;
        case 1: return "<abbr title=\"Lunes\">Lunes</abbr>";
            break;
        case 2: return "<abbr title=\"Martes\">Martes</abbr>";
            break;
        case 3: return "<abbr title=\"Miercoles\">Mi&eacute;rcoles</abbr>";
            break;
        case 4: return "<abbr title=\"Jueves\">Jueves</abbr>";
            break;
        case 5: return "<abbr title=\"Viernes\">Viernes</abbr>";
            break;
        case 6: return "<abbr title=\"Sabado\">S&aacute;bado</abbr>";
            break;
    }
}

function tituloMes($mes) {
    $meses = array(12);
    if ($mes[0] == "0")
        $mes = $mes[1];
    $meses[1] = "Enero";
    $meses[2] = "Febrero";
    $meses[3] = "Marzo";
    $meses[4] = "Abril";
    $meses[5] = "Mayo";
    $meses[6] = "Junio";
    $meses[7] = "Julio";
    $meses[8] = "Agosto";
    $meses[9] = "Septiembre";
    $meses[10] = "Octubre";
    $meses[11] = "Noviembre";
    $meses[12] = "Diciembre";
    return $meses[$mes];
}

function diaSemana($dia_num) {
    switch ($dia_num) {
        case 0: return "DOMINGO";
            break;
        case 7: return "DOMINGO";
            break;
        case 1: return "LUNES";
            break;
        case 2: return "MARTES";
            break;
        case 3: return "MIERCOLES";
            break;
        case 4: return "JUEVES";
            break;
        case 5: return "VIERNES";
            break;
        case 6: return "SABADO";
            break;
    }
}

function formatoHora($hora) {
    $hora=date('H:i',strtotime($hora));
    return $hora;
}

function quitarPuntosHora($hora) {
    return substr($hora, 0, 2) . substr($hora, 3, 2);
}

function compararFechas($hoy, $otra) { // formato yyyymmdd
    $hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
    $otraMK = mktime(0, 0, 0, substr($otra, 4, 2), substr($otra, 6, 2), substr($otra, 0, 4));
    if ($otraMK >= $hoyMK)
        return true; else
        return false;
}

function quitarAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "&") {
            $temp = substr($Text, $j, 8);
            switch ($temp) {
                case "&aacute;": $cadena .= "(/a)";
                    $j = $j + 7;
                    break;
                case "&Aacute;": $cadena .= "(/A)";
                    $j = $j + 7;
                    break;
                case "&eacute;": $cadena .= "(/e)";
                    $j = $j + 7;
                    break;
                case "&Eacute;": $cadena .= "(/E)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/i)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/I)";
                    $j = $j + 7;
                    break;
                case "&oacute;": $cadena .= "(/o)";
                    $j = $j + 7;
                    break;
                case "&Oacute;": $cadena .= "(/O)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/u)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/U)";
                    $j = $j + 7;
                    break;
                case "&ntilde;": $cadena .= "(/n)";
                    $j = $j + 7;
                    break;
                case "&Ntilde;": $cadena .= "(/N)";
                    $j = $j + 7;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            switch ($cara) {
                case "�": $cadena.="(/a)";
                    break;
                case "�": $cadena.="(/e)";
                    break;
                case "�": $cadena.="(/i)";
                    break;
                case "�": $cadena.="(/o)";
                    break;
                case "�": $cadena.="(/u)";
                    break;
                case "�": $cadena.="(/A)";
                    break;
                case "�": $cadena.="(/E)";
                    break;
                case "�": $cadena.="(/I)";
                    break;
                case "�": $cadena.="(/O)";
                    break;
                case "�": $cadena.="(/U)";
                    break;
                case "�": $cadena.="(/n)";
                    break;
                case "�": $cadena.="(/N)";
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        }
    }
    return $cadena;
}

function ponerAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "(") {
            $temp = substr($Text, $j, 4);
            switch ($temp) {
                case "(/a)": $cadena .= "&aacute;";
                    $j = $j + 3;
                    break;
                case "(/A)": $cadena .= "&Aacute;";
                    $j = $j + 3;
                    break;
                case "(/e)": $cadena .= "&eacute;";
                    $j = $j + 3;
                    break;
                case "(/E)": $cadena .= "&Eacute;";
                    $j = $j + 3;
                    break;
                case "(/i)": $cadena .= "&iacute;";
                    $j = $j + 3;
                    break;
                case "(/I)": $cadena .= "&Iacute;";
                    $j = $j + 3;
                    break;
                case "(/o)": $cadena .= "&oacute;";
                    $j = $j + 3;
                    break;
                case "(/O)": $cadena .= "&Oacute;";
                    $j = $j + 3;
                    break;
                case "(/u)": $cadena .= "&uacute;";
                    $j = $j + 3;
                    break;
                case "(/U)": $cadena .= "&Uacute;";
                    $j = $j + 3;
                    break;
                case "(/n)": $cadena .= "&ntilde;";
                    $j = $j + 3;
                    break;
                case "(/N)": $cadena .= "&Ntilde;";
                    $j = $j + 3;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            $cadena.=$Text[$j];
        }
    }
    return $cadena;
}

function tipoDH($municipio) {
    $ret = "X";
    if (($municipio == "Guadalajara") || ($municipio == "Tlajomulco de Z(/u)(/n)iga") || ($municipio == "Tonal(/a)") || ($municipio == "Zapopan") || ($municipio == "Tlaquepaque"))
        $ret = "&nbsp;";
    return $ret;
}

function queSexoTipoCedula($tipoCedula) {
    $ret = "";
    if (($tipoCedula == "10") || ($tipoCedula == "40") || ($tipoCedula == "41") || ($tipoCedula == "50") || ($tipoCedula == "51") || ($tipoCedula == "70") || ($tipoCedula == "71") || ($tipoCedula == "90") || ($tipoCedula == "92"))
        $ret = "M"; else
        $ret = "F";
    return $ret;
}

// ---------------------------------------------------------------------------------------------  N U E V A S    F U N C I O N E S  -------------


function formatoDia($fecha, $paraDonde) {
    $dia = substr($fecha, 6, 2);
    $mes = substr($fecha, 4, 2);
    $ano = substr($fecha, 0, 4);
    $diaSem = date("N", mktime(0, 0, 0, $mes, $dia, $ano));
    if ($paraDonde == 'tituloCitasXdia') {
        $fechaO = diaSemana($diaSem) . " " . $dia . " DE " . strtoupper(tituloMes($mes)) . " DE " . $ano;
    }
    if ($paraDonde == 'imprimirCita') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    if ($paraDonde == 'fecha') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    return $fechaO;
}



function formatoFechaBD($fecha) {
    $dia = substr($fecha, 0, 2);
    $mes = substr($fecha, 3, 2);
    $ano = substr($fecha, 6, 4);
    $fechaO = $ano . $mes . $dia;
    return $fechaO;
}

function diferenciaDeDiasEntreFechas($ano1, $mes1, $dia1, $ano2, $mes2, $dia2) {
    //defino fecha 1
    /* 	$ano1 = 2006;
      $mes1 = 10;
      $dia1 = 2;

      //defino fecha 2
      $ano2 = 2006;
      $mes2 = 10;
      $dia2 = 27;
     */
    //calculo timestam de las dos fechas
    $timestamp1 = mktime(0, 0, 0, $mes1, $dia1, $ano1);
    $timestamp2 = mktime(4, 12, 0, $mes2, $dia2, $ano2);

    //resto a una fecha la otra
    $segundos_diferencia = $timestamp1 - $timestamp2;
    //echo $segundos_diferencia;
    //convierto segundos en d�as
    $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

    //obtengo el valor absoulto de los d�as (quito el posible signo negativo)
    $dias_diferencia = abs($dias_diferencia);

    //quito los decimales a los d�as de diferencia
    $dias_diferencia = floor($dias_diferencia);

    return $dias_diferencia;
}

function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function ejecutarLOG($log) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "INSERT INTO bitacora VALUES('" . date('Ymd') . "','" . date('Hi') . "','" . getRealIpAddr() . "','" . $log . "','" . $_SESSION['idUsuario'] . "')";
    $query = mysql_query($query_query, $bdissste); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $error;
}


        function cambiarAcentosUMF($Text)
        {
        	 $cadena = "";
            $temp = "";
            $total = strlen($Text);
            for ($j = 0; $j < $total; $j++) {
                $cara = $Text[$j];
                if ($cara == "(") {
                    $temp = substr($Text, $j, 4);
                    switch ($temp) {
                        case "(/a)": $cadena .= "a";
                            $j = $j + 3;
                            break;
                        case "(/A)": $cadena .= "A";
                            $j = $j + 3;
                            break;
                        case "(/e)": $cadena .= "e";
                            $j = $j + 3;
                            break;
                        case "(/E)": $cadena .= "E";
                            $j = $j + 3;
                            break;
                        case "(/i)": $cadena .= "i";
                            $j = $j + 3;
                            break;
                        case "(/I)": $cadena .= "I";
                            $j = $j + 3;
                            break;
                        case "(/o)": $cadena .= "o";
                            $j = $j + 3;
                            break;
                        case "(/O)": $cadena .= "O";
                            $j = $j + 3;
                            break;
                        case "(/u)": $cadena .= "u";
                            $j = $j + 3;
                            break;
                        case "(/U)": $cadena .= "U";
                            $j = $j + 3;
                            break;
                        case "(/n)": $cadena .= "ñ";
                            $j = $j + 3;
                            break;
                        case "(/N)": $cadena .= "Ñ";
                            $j = $j + 3;
                            break;
                        default:
                            $cadena.=$Text[$j];
                            break;
                    }
                } else {
                    $cadena.=$Text[$j];
                }
            }
            return $cadena;
        }

        public function formato_fecha_constancia($fecha)
        {
            
            $fecha1=date('N/j/n/Y',strtotime($fecha));
            $diaSem = array(1 => 'Lunes',
            2=>'Martes',
            3=>'Miercoles',
            4=>'Jueves',
            5=>'Viernes',
            6=>'Sabado',
            7=>'Domingo');
            $meses = array(1 =>'Enero' ,
            2=>'Febrero',
            3=>'Marzo',
            4=>'Abril',
            5=>'Mayo',
            6=>'Junio',
            7=>'Julio',
            8=>'Agosto',
            9=>'Septiembre',
            10=>'Octubre',
            11=>'Noviembre',
            12=>'Diciembre');
            $fechas=explode('/', $fecha1);
            $fecha=$diaSem[$fechas[0]].' '.$fechas[1].' de '.$meses[$fechas[2]].' del '.$fechas[3];
           
            return $fecha;
        }

}

/* End of file funciones.php */
/* Location: .//C/xampp/htdocs/farmaciat2/constancias/app/libraries/funciones.php */