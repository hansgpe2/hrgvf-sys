<!DOCTYPE html>
<html>
<head>
	<style type="text/css" media="screen">
	.constancias
	{
		border-collapse: collapse;
		border:1px solid #000;
	}
		.constancias thead th
		{
			background: #d0021b;
			color: #fff;
		}
		.enlaceboton {    
		   font-family: verdana, arial, sans-serif; 
		   font-size: 9pt; 
		   font-weight: bold; 
		   padding: 1px; 
		   background-color: #ccc; 
		   color: #666666; 
		   text-decoration: none; 
		} 
		.enlaceboton:link, 
		.enlaceboton:visited { 
		   border-top: 1px solid #cccccc; 
		   border-bottom: 2px solid #666666; 
		   border-left: 1px solid #cccccc; 
		   border-right: 2px solid #666666; 
		} 
		.enlaceboton:hover { 
		    border-bottom: 1px solid #cccccc; 
		   border-top: 2px solid #666666; 
		   border-right: 1px solid #cccccc; 
		   border-left: 2px solid #666666; 
		}
		.constancias tr td
		{
			border-bottom: 1px solid #000;
			font-size: 9pt;
		}

		.constancias thead th
		{
			border: 1px solid #fff;
			font-size: 9pt;
		}
	</style>
</head>
<body>
<table class="constancias">
	<thead>
		<tr>
			<th rowspan="2">Ver</th>
			<th rowspan="2">Folio</th>
			<th colspan="2">Paciente</th>
			<th rowspan="2">Fecha</th>
			<th colspan="2">Asistencia</th>
			<th rowspan="2">Servicio</th>
			<th rowspan="2">Acompañante</th>
			<th rowspan="2">Médico</th>
		</tr>
		<tr>
			<th>Cedula</th>
			<th>Nombre</th>
			<th>Entrada</th>
			<th>Salida</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($constancias->result() as $row) { 
		$derechohabiente=$this->citas_model->getDerecho($row->id_derecho)
		?>
		<tr>
			<td><a href="<?php echo site_url('constancias/constancia/'.$row->id_constancia) ?>" target="_blank" class="enlaceboton"> Imprimir </a></td>
			<td><?php echo $row->folio; ?></td>
			<td><?php echo $derechohabiente['cedula'].'/'.$derechohabiente['cedula_tipo']; ?></td>
			<td><?php echo $derechohabiente['nombres'].' '.$derechohabiente['ap_p'].' '.$derechohabiente['ap_m']; ?></td>
			<td><?php echo $this->funciones->formatoDia($row->fecha_cita,'imprimirCita'); ?></td>
			<td><?php echo $row->hora_entrada; ?></td>
			<td><?php echo $row->hora_salida; ?></td>
			<td><?php echo $servicios[$row->id_servicio]; ?></td>
			<td><?php echo $row->acompanante; ?></td>
			<td><?php echo $medicos[$row->id_medico]; ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>


</body>
</html>