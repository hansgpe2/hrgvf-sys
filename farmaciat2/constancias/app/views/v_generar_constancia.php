<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link href=".././lib/misEstilos.css" rel="stylesheet" type="text/css">
    <script src="../../lib/arreglos.js"></script>
</head>
<body>
<div align="center">
            <form action="<?php echo site_url('constancias/guardar_constancia'); ?>" method="get" target="_blank" id="Constancia">
                <table width="800" border="0" class="tablaPrincipal">   
                    <tr>
                        <td colspan="2" class="tituloVentana">Constancia M&eacute;dica</td>

                    </tr>
                    <tr>
                        <td width="300" align="right" class="textosParaInputs">C&eacute;dula</td>
                        <td width="488"><?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo']; ?></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Nombre</td>
                        <td><?php echo $this->funciones->ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']); ?>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Fecha</td>
                        <td><?php echo date("d/m/Y"); ?>&nbsp;
                            <input name="fecha" type="hidden" id="hiddenField3" value="<?php echo date("Ymd"); ?>">
                            </td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Hora de llegada</td>
                        <td><input type="time" name="hEnt" id="hEnt">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Hora de Salida</td>
                        <td><label for="hSalida"></label>
                            <input type="time" name="hSalida" id="hSalida"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Acompa&ntilde;ante</td>
                        <td><input name="Acomp" type="text" id="Acomp"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Medico</td>
                        <td><?php echo $usuario['nombre'] ?><input name="medico" type="hidden" value="<?php echo $medico['id_medico']; ?>">
                        <input name="usuario" type="hidden" value="<?php echo $usuario['id_usuario']; ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        <?php
                             if ($_SESSION['tipoUsuario'] == 8) {
                                echo "  <input type='button' name='regresar' id='regresar' value='Regresar' class='botones'  onclick='javascript: inicio(\"inicioMedicoEsp.php\");' tabindex='11' />&nbsp;&nbsp;&nbsp;&nbsp;";
                            } else if ($_SESSION['tipoUsuario'] == 4) {
                                echo "<input type='button' name='regresar' id='regresar' value='Regresar' class='botones'  onclick='javascript: inicio(\"inicioMedico.php\");' tabindex='11' />&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                            ?>
                            <input type="button" onClick="genConstancia();" class="botones" value="Guardar"></td>
                        </td>
                    </tr>
                    </table>
                    
                        <input name="fecha_cita" type="hidden" id="fecha_cita" value="<?php echo $cita['fecha_cita']; ?>" >
                        <input name="idDerecho" type="hidden" id="idDerecho" value="<?php echo $derecho['id_derecho']; ?>" >
                        <input name="idServicio" type="hidden" id="idServicio" value="<?php echo $servicio['id_servicio']; ?>" >
            </form>
        </div>
    
</body>
</html>