<!DOCTYPE html>
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Untitled Document</title>
                <link href="../../../../lib/impresion.css" rel="stylesheet" type="text/css">
                <link href="../../../../lib/reportes.css" rel="stylesheet" type="text/css">
                <style type="text/css" media="screen">
                    .encabezados
                    {
                        width: 50px;
                        height: 50px;
                    }
                    .codigo
                    {
                        width: 150px;
                        height: 30px;
                    }
                </style>
            </head>

            <body>
                <table width="684" cellpadding="0" cellspacing="0" class="tituloEncabezadoConBorde" border="0">
                    <tr><td width="39%"></td></tr>
                    <tr>
                        <td class="tituloIssste" rowspan="3"><img src="../../../../diseno/logoEncabezado.jpg" class="encabezados"><br>
                        <img src="<?php echo site_url('codigoBarras/codigo_barras/'.$constancia['folio']); ?>" class="codigo"></td>
                        <td width="61%" align="center" class="tituloEncabezado">CONSTANCIA</td><td><img src="../../../../diseno/LogoHRVGF.jpg" class="encabezados"></td>
                    </tr></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td class="tituloEncabezado" align="left">Consulta Externa</td></tr>
                <tr>
                    <td colspan="2" align="right" class="tituloEncabezado">Zapopan Jalisco a <?php echo $this->funciones->formato_fecha_constancia($constancia['fecha_elaboracion']) ?>.</td>
                </tr>
                <tr>
                <td style="font-family:Arial, Helvetica, sans-serif;font-size:14px;" height="400" colspan="2"><p class="tituloEncabezado">A QUIEN CORRESPONDA:</p>
                    <p style="text-align:justify">Por medio de la presente se hace constar que <span class="tituloEncabezado"><?php echo $this->funciones->ponerAcentos($derecho['nombres'].' '.$derecho['ap_p'].' '.$derecho['ap_m']); ?></span> Asisti&oacute; el d&iacute;a de hoy a la Consulta Externa de este centro Hospitalario al Servicio de <span class="tituloEncabezado"><?php echo $servicio['nombre']; ?></span> entre las <span class="tituloEncabezado"><?php echo $this->funciones->formatoHora($constancia['hora_entrada']); ?></span> y las <span class="tituloEncabezado"><?php echo $this->funciones->formatoHora($constancia['hora_salida']); ?></span></p>
                    <p style="text-align:justify">A petici&oacute;n del interesado y para los fines que estime conveniente se expide la presente constancia que no surte efecto de licencia m&eacute;dica.</p>
                    <?php
                    if ($constancia['acompanante'] != "") {
                        print("<p>Acompa&ntilde;ante<span class='tituloEncabezado'> " . $this->funciones->ponerAcentos($constancia['acompanante']) . "</span></p>");
                    }
                    ?>
                    </td>
                </tr>
                <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif;font-size:14px;">
                    <br>Capturista: <?php echo $usuario['nombre']; ?></td><td style="font-family:Arial, Helvetica, sans-serif;font-size:14px;" align="right" class="tituloEncabezado"><?php print($medico['titulo']." ".$medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']); ?>
                        <br>DGP:<?php echo $medico['cedula_profesional']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height:14px"></td>
                    </tr>
            </table>
</body>
</html>
