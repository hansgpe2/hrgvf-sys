<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
				<title>
		</title>
		<style type="text/css">
			body { line-height:108%; font-family:Calibri; font-size:11pt }
			p { margin:0pt 0pt 8pt }
			.Header { margin-bottom:0pt; line-height:normal; font-size:11pt }
			.Footer { margin-bottom:0pt; line-height:normal; font-size:11pt }
			span.Hyperlink { text-decoration:underline; color:#0563c1 }
			span.PlaceholderText { color:#808080 }
		</style>
	</head>
	<body>
		<div>
			<p style="text-align:center; line-height:108%; font-size:18pt">
				<img src="../../../../diseno/logoEncabezado.jpg" width="101" height="67" alt="" style="margin:0pt 9pt; float:left" /><img src="../../../../diseno/LogoHRVGF.jpg" width="74" height="74" alt="" style="margin:0pt 9pt; float:right" /><strong><span style="font-size:18pt; ">CONSTANCIA</span><br><span style="font-size:16pt; ">Consulta externa</span></strong>
			</p>
			
				
			
			<p style="margin-left:318.6pt; text-indent:35.4pt; text-align:right; line-height:108%; font-size:14pt">
				<img src="<?php echo site_url('codigoBarras/codigo_barras/'.$constancia['folio']); ?>" width="116" height="29" alt="" style="margin:3.22pt 9pt 3.6pt 8.62pt; float:left" />
			</p>
			<p style="text-align:right">
				<strong><span style="font-size:10pt; ">Zapopan Jalisco a <?php echo $this->funciones->formato_fecha_constancia($constancia['fecha_elaboracion']); ?></span></strong>
			</p>
			<p style="text-align:center; line-height:108%; font-size:18pt">
				<strong><span style="font-size:18pt; ">&#xa0;</span></strong>
			</p>
			<p style="line-height:108%; font-size:12pt">
				<strong><span style="font-size:12pt; ">A QUIEN CORRESPONDA:</span></strong>
			</p>
			<p style="line-height:108%; font-size:12pt">
				<span style="font-size:12pt">Por medio de la presente se hace constar que <?php echo $this->funciones->ponerAcentos($derecho['nombres'].' '.$derecho['ap_p'].' '.$derecho['ap_m']); ?> asistió el día de hoy a la consulta externa de este centro hospitalario al servicio de <?php echo $this->funciones->ponerAcentos($servicio['nombre']); ?> entre las <?php echo $constancia['hora_entrada']; ?> y las <?php echo $constancia['hora_salida']; ?>.</span>
			</p>
			<p style="line-height:108%; font-size:12pt">
				<span style="font-size:12pt">A petición del interesado y para los fines que estime conveniente se expide la presente constancia que no surte efecto de licencia médica.</span>
			</p>
			<?php if(!empty($constancia['acompanante'])) {?>
			<p style="font-size:12pt;">
				Acompañante: <?php echo $constancia['acompanante']; ?>
			</p>
			<?php } ?>
			
				<p style="text-align:left;width:50%">
					Capturista: <?php echo $usuario['nombre']; ?>
				</p>
				<p style="text-align:right;">
					<?php echo $this->funciones->ponerAcentos($medico['titulo'].' '.$medico['nombres'].' '.$medico['ap_p'].' '.$medico['ap_m']); ?><br>
					D.G.P. <?php echo $medico['cedula_profesional']; ?>
				</p>
			
			<p style="font-size:8pt;text-align:center;">
				Podrá verificar esta constancia en la página </span><a style="color:#0563c1" href="http://issstezapopan.gob.mx/constancia">http://issstezapopan.gob.mx/constancia</a>.
			</p>
			<div>
		<hr>
				<p class="Footer" style="text-align:center">
					<span style="font-size:9pt">Hospital Regional “Dr. Valentín Gómez Farías”</span>
				</p>
				<p class="Footer" style="text-align:center">
					<span style="font-size:9pt">Avenida Soledad Orozco 203, Colonia el Capullo, Zapopan Jalisco</span>
				</p>
				<p class="Footer" style="text-align:center">
					<span style="font-size:9pt">Teléfono: 0133 38360650</span>
				</p>
			</div>
		</div>
	</body>
</html>