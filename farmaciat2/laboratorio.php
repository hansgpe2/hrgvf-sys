<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aviso</title>
	<!-- Latest compiled and minified CSS -->
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" media="screen" href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
	<link rel="stylesheet" href="lib/misEstilos2013.css" type="text/css">
	<link href="lib/impresion.css" media="print" rel="stylesheet" type="text/css" />
	<style type="text/css" media="screen">
		.container
		{
			padding-top: 120px;
			background-color: #fff;
		}
	</style>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$("#ir_a").click(function(event) {
			window.close();

		});
	});
	</script>
</head>
<body >
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<ul class="nav navbar-nav navbar-right">
		<li>
				<a href="http://192.161.21.31/login.aspx" class="btn btn-default" target="_blank" id="ir_a"> Ir al sitio <span class="glyphicon glyphicon-circle-arrow-right"></span></a>	
		</li>
	</ul>
</nav>
<div class="container">
		<header id="header" class="row">
			<table class="table table-responsive">
		        <tr>
		          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="diseno/logoEncabezado.jpg" width="74" height="74" class="img-responsive"></td>
		        </tr>
		        <tr>
		          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
		        </tr>
		      </table>
		</header><!-- /header -->
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="page-header">
				  <h1>Nueva Empresa de Laboratorio<small> a partir del 2016</small></h1>
				</div>
			</div>
		</div>
		<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="thumbnail">
						<img src="diseno/inicio_lab1.jpg" class="img-responsive" style="width:300px;height:200px">
						<div class="caption">
							<h3>Usuario y Contraseña</h3>
							<p>
								ISSSTE
							</p>
						</div>
					</div>
				</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="thumbnail">
					<img src="diseno/inicio_lab2.jpg" class="img-responsive" style="width:500px;height:200px">
					<div class="caption">
						<h3>Busqueda de Resultados</h3>
						<p>
							favor de colocar la cedula en el campo de afilación y los rangos de fecha correctos
						</p>
					</div>
				</div>
			</div>
		</div>
		
</div>


	
</body>
</html>