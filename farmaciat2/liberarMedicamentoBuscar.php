<?php
error_reporting(E_ALL^E_NOTICE);
include_once('lib/misFunciones.php');
session_start ();

function getMedicamentosEnRecetaParaLiberar($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'cantidad' => $row_query['cantidad'],
                'unidad' => $row_query['unidad'],
                'dias' => $row_query['dias'],
                'extra1' => $row_query['extra1'],
				'fecha' => $row_query['fecha'],
				'hora' => $row_query['hora'],
				'id_usuario_surtio' => $row_query['id_usuario_surtio'],				
                'indicaciones' => $row_query['indicaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXderechoParaLiberar($id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = array();
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
	$fecha_buscar = date("Ymd", strtotime("-30 day"));
    $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "' AND fecha>='" . $fecha_buscar . "' AND status !='0' ORDER BY fecha DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

$recetas = getRecetasXderechoParaLiberar($_GET['id_derecho']);
$out = '';
if (count($recetas) > 0) {
	foreach($recetas as $index => $receta) {
		$conceptos = getMedicamentosEnRecetaParaLiberar($receta['id_receta']);
		$boton = '';
		$input = '';
		foreach($conceptos as $indexConcepto => $concepto) {		
			$fecha_factura = substr($receta["fecha"], 6, 2) . "-" . substr($receta["fecha"], 4, 2) . "-" . substr($receta["fecha"], 0, 4);
			$hoy = date("d-m-Y");
			$fecha_termino = strtotime($fecha_factura . " +" . $concepto["dias"] . " day");
			$fecha_termino = date("Ymd", $fecha_termino);
			$hoy = date("Ymd");
			$hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
			$terminoMK = mktime(0, 0, 0, substr($fecha_termino, 4, 2), substr($fecha_termino, 6, 2), substr($fecha_termino, 0, 4));
			if ($terminoMK > $hoyMK) { // tiene tratamiento entonces si habilita el poder liberar medicamento
				$boton = '<tr><td colspan="5"><br><input name="cancelar" type="button" value="Liberar Receta" onclick="javascript: validarLiberarReceta(' . $receta["id_receta"] . ')" /></td></tr>';
				$input = '<tr><td colspan="5"><br>Motivo de Liberaci&oacute;n: <input type="text" name="motivo_' . $receta["id_receta"] . '" id="motivo_' . $receta["id_receta"] . '" value="" size="60"><br></td></tr>';
				break;
			}
		}
		$out .= '<center><br><br><table border="0" cellpadding="0" cellspacing="0" width="780">';
		$out .= '<tr><td colspan="5" class="tituloVentana">Receta ' . $receta["codigo_barras"] . '</td></tr>';
		$out .= '<tr><th>Fecha y Hora</th><th>Folio</th><th>M&eacute;dico</th><th>Servicio</th><th>Derechohabiente</th></tr>';
		$datosMedico = getMedicoXid($receta['id_medico']);
		$datosServicio = getServicioXid($receta['id_servicio']);
		$datosDerecho = getDatosDerecho($receta['id_derecho']);
		$out .= '<tr class="botones_menu"><td>' . $receta['fecha'] . ' - ' . $receta['hora'] . '</td><td>' . $receta['serie'] . '' . $receta['folio'] . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td>' . ponerAcentos($datosServicio) . '</td><td>' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td></tr>';
		$out .= '<tr><td colspan="5">&nbsp;</td></tr>';
		if ($conceptos != "") {
			$out .= '<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
			$tConceptos = count($conceptos);
			for ($i=0; $i < $tConceptos; $i++) {
	//			$Extistencia_medicamento = getExistenciasMedicamentos($conceptos[$i]["id_medicamento"]);
				$medicamento = getDatosMedicamentos($conceptos[$i]["id_medicamento"]);
				if ($conceptos[$i]['extra1'] == 's') {
					$usuario = getUsuarioXid($conceptos[$i]["id_usuario_surtio"]);
					$readonly = ' si ';
					$datosExtra = '<p class="textoRojo">Medicamento surtido por: ' . $usuario["nombre"] . ' | ' . formatoDia($conceptos[$i]["fecha"],"fecha") . ' - ' . $conceptos[$i]["hora"] . '</p>';
				} else {
					$readonly = ' no';
					$datosExtra = '';
				}
				$out .= '<td width="50%" align="center">
							<table width="350" border="1" cellspacing="0" cellpadding="3">
								<tr><td align="left">Cantidad: </td><td>' . $conceptos[$i]["cantidad"] . '</td></tr>
								<tr><td align="left">Tratamiento: </td><td>' . $conceptos[$i]["dias"] . ' D&iacute;as</td></tr>
								<tr><td align="left">Medicamento: </td><td class="botones_menu">' . $medicamento["descripcion"] . '</td></tr>
								<tr><td align="left">Indicaciones: </td><td>' . $conceptos[$i]["indicaciones"] . '</td></tr>
								<tr><td align="center" colspan="2">Se surti&oacute; el medicamento: ' . $readonly . $datosExtra . '</td></tr>
							</table>
						</td>';
			}
			$out .= '</tr></table>';
			$out .= $input . $boton;
		}
		$out .= '</table><div id="enviando"></div></center>';
	
	}
} else {
	$out = '<h2>No se encontraron recetas activas del derechohabiente</h2>';
}


print($out);

?>

