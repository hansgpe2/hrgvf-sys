<?php
include_once('lib/misFunciones.php');
@session_start ();

$datosRecetas = getRecetaXCodigoBarras($_REQUEST["cod_bar"]);
if ($datosRecetas == "") {
	$out = 'No existe receta con el c&oacute;digo de barras ingresado';
} else {
	$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="780">';
	$out .= '<tr><td colspan="5" class="tituloVentana">Receta</td></tr>';
	$out .= '<tr><th>Fecha y Hora</th><th>Folio</th><th>M&eacute;dico</th><th>Servicio</th><th>Derechohabiente</th></tr>';
	$datosMedico = getMedicoXid($datosRecetas['id_medico']);
	$datosServicio = getServicioXid($datosRecetas['id_servicio']);
	$datosDerecho = getDatosDerecho($datosRecetas['id_derecho']);
	$out .= '<tr class="botones_menu"><td>' . formatoDia($datosRecetas['fecha'],"fecha") . ' - ' . $datosRecetas['hora'] . '</td><td>' . $datosRecetas['serie'] . '' . $datosRecetas['folio'] . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td>' . ponerAcentos($datosServicio) . '</td><td>' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td></tr>';
	$out .= '<tr><td colspan="5">&nbsp;</td></tr>';
	$conceptos =  getDatosMedicamentoEnReceta($datosRecetas["id_receta"]);
	$boton = '';
	if ($datosRecetas["status"] == "2") $boton = ' disabled="disabled"';
	$hoy = date('Ymd');
	if ($conceptos != "") {
		$out .= '<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
		$tConceptos = count($conceptos);
		for ($i=0; $i < $tConceptos; $i++) {
//			$Extistencia_medicamento = getExistenciasMedicamentos($conceptos[$i]["id_medicamento"]);
			$medicamento = getDatosMedicamentos($conceptos[$i]["id_medicamento"]);
			if ($conceptos[$i]['extra1'] == 's') {
				$usuario = getUsuarioXid($conceptos[$i]["id_usuario_surtio"]);
				$readonly = ' disabled="disabled"';
				$datosExtra = '<p class="textoRojo">Medicamento surtido por: ' . @$usuario["nombre"] . ' | ' . formatoDia($conceptos[$i]["fecha"],"fecha") . ' - ' . $conceptos[$i]["hora"] . '</p>';
			} else {
				$readonly = '';
				$datosExtra = '';
			}
			if ((int)$datosRecetas['fecha']>(int)$hoy) { // si la fecha de la receta es futura no se puede surtir
				$boton = ' disabled="disabled"';
				$readonly = ' disabled="disabled"';
				$datosExtra = '<p class="textoRojo">No se puede surtir la receta porque tiene una fecha futura: ' . formatoDia($datosRecetas['fecha'],"fecha") . '</p>';
			}
			$out .= '<td width="50%" align="center">
						<table width="350" border="1" cellspacing="0" cellpadding="3">
							<tr><td align="left">Cantidad: </td><td>' . $conceptos[$i]["cantidad"] . '</td></tr>
							<tr><td align="left">Tratamiento: </td><td>' . $conceptos[$i]["dias"] . ' D&iacute;as</td></tr>
							<tr><td align="left">Medicamento: </td><td class="botones_menu">' . $medicamento["descripcion"] . '</td></tr>
							<tr><td align="left">Indicaciones: </td><td>' . $conceptos[$i]["indicaciones"] . '</td></tr>
							<tr><td align="center" colspan="2">Se surti&oacute; el medicamento: <input id="surtio_' . $i . '" name="surtio_' . $i . '" type="checkbox" value="' . $conceptos[$i]["id_medicamento"] . '" checked="checked"' . $readonly . ' />' . $datosExtra . '</td></tr>
						</table>
					</td>';
		}
		$out .= '</tr></table>';
		$out .= '<tr><td colspan="5"><br><input name="enviar" type="button" value="Guardar" onclick="javascript: validarSurtirReceta(' . $datosRecetas["id_receta"] . ')"' . $boton . ' /></td></tr>';
	}
	$out .= '</table><div id="enviando"></div></center>';
}
print($out);

?>

