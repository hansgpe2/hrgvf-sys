<?php
session_start ();
include_once('lib/misFunciones.php');
	
	$medico = getMedicoXid($_SESSION["idDr"]);
	$nombre = $medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"];
	$series = getSeries();
	$tSeries = count($series);
	$opcSeries = '';
	for ($i=0; $i<$tSeries; $i++) {
		$opcSeries .= '<option value="' . $series[$i]["serie"] . '">' . $series[$i]["serie"] . '</option>';
	}
?>
<br>
<form id="formaFolio" method="POST" action="javascript: validarAgregarFolios('<?php echo $_SESSION["idDr"] ?>');">

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">AGREGAR FOLIOS A MEDICO</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">MEDICO: </td>
    <td align="left"><input name="medico" type="text" id="medico"  readonly="readonly" size="35" value="<?php echo $nombre ?>" />
     </td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">SERIE:</td>
    <td align="left"><select id="serie" name="serie"><?php echo $opcSeries ?></select></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">FOLIO INICIAL:</td>
    <td align="left"><input name="folio_inicial" type="text" id="folio_inicial" size="20" maxlength="7" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">FOLIO FINAL:</td>
    <td align="left"><input name="folio_final" type="text" id="folio_final" size="20" maxlength="7" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: cargarFolios();" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Agregar Folios" class="botones" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>