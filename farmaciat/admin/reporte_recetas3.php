<?php
set_time_limit(600);
session_start ();
require_once("../lib/funcionesAdmin.php");

function getDerechohabienteXidOk($id) {
	global $hostname_bdissste;
	global $username_bdisssteR;
	global $password_bdisssteR;
	global $database_bdisssteR;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdisssteR, $bdissste);
	$query_query = "SELECT * FROM derechohabientes WHERE id_derecho='" . $id . "' AND st='1'";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	if ($totalRows_query>0){
			$ret=array(
					'id_derecho' => $row_query['id_derecho'],
					'cedula' => $row_query['cedula'],
					'cedula_tipo' => $row_query['cedula_tipo'],
					'ap_p' => $row_query['ap_p'],
					'ap_m' => $row_query['ap_m'],
					'nombres' => $row_query['nombres'],
					'fecha_nacimiento' => $row_query['fecha_nacimiento'],
					'telefono' => $row_query['telefono'],
					'direccion' => $row_query['direccion'],
					'estado' => $row_query['estado'],
					'municipio' => $row_query['municipio'],
					'status' => $row_query['status'],
					'st' => $row_query['st']
					);
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}

function getDatosMedicamentoEnReceta($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_concepto' => $row_query['id_concepto'],
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'cantidad' => $row_query['cantidad'],
                'unidad' => $row_query['unidad'],
                'dias' => $row_query['dias'],
                'indicaciones' => $row_query['indicaciones'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXRango($fecha_i, $fecha_f, $filtro, $orden) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "SELECT * FROM recetas WHERE fecha>='" . $fecha_i . "' AND fecha<='" . $fecha_f . "'" . $filtro . $orden;
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	$ret = array();
	if ($totalRows_query>0){
		do{
			$ret[]=array(
						'id_receta' => $row_query['id_receta'],
						'fecha' => $row_query['fecha'],
						'hora' => $row_query['hora'],
						'codigo_barras' => $row_query['codigo_barras'],
						'id_medico' => $row_query['id_medico'],
						'id_servicio' => $row_query['id_servicio'],
						'id_derecho' => $row_query['id_derecho'],
						'status' => $row_query['status'],
						'extra1' => $row_query['extra1']
						);
/*			$ret[]=array(
						'id_receta' => $row_query['id_receta'],
						'n_serie' => $row_query['n_serie'],
						'codigo_barras' => $row_query['codigo_barras'],
						'id_medico' => $row_query['id_medico'],
						'id_servicio' => $row_query['id_servicio'],
						'id_derecho' => $row_query['id_derecho'],
						'fecha_captura' => $row_query['fecha_captura'],
						'fecha_expedicion' => $row_query['fecha_expedicion'],
						'entidad' => $row_query['entidad'],
						'status' => $row_query['status'],
						'extra1' => $row_query['extra1']
						);
*/		}while($row_query = mysql_fetch_assoc($query));
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return array($ret, $totalRows_query);
}


if (@$_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if ((isset($_POST['mostrar'])) && (isset($_POST['fecha_de'])) && (isset($_POST['fecha_a'])) && (isset($_POST['status'])) && (isset($_POST['orden'])) && (isset($_POST['id']))) {
		$mostrar = $_POST['mostrar'];
		$fecha_de = $_POST['fecha_de'];
		$fecha_a = $_POST['fecha_a'];
		$status = $_POST['status'];
		$orden = $_POST['orden'];
		$id = $_POST['id'];
		
		$f_d = explode('/', $fecha_de);
		$fecha_de = $f_d[2] . $f_d[1] . $f_d[0];
		$f_a = explode('/', $fecha_a);
		$fecha_a = $f_a[2] . $f_a[1] . $f_a[0];
		
		$titulo = '';
		$filtro = "";
		switch ($mostrar) {
			case 'paciente':
				$derecho = getDerechohabienteXidOk($id);
				$titulo .= ponerAcentos($derecho['ap_p'] . ' ' . $derecho['ap_m'] . ' ' . $derecho['nombres']);
				$filtro .= " AND id_derecho='" . $id . "'";
				break;
			case 'servicio':
				$derecho = getServicioXid($id);
				$titulo .= ponerAcentos($derecho['nombre']);
				$filtro .= " AND id_servicio='" . $id . "'";
				break;
			case 'medico':
				$derecho = getMedicoXid($id);
				$titulo .= ponerAcentos($derecho['ap_p'] . ' ' . $derecho['ap_m'] . ' ' . $derecho['nombres']);
				$filtro .= " AND id_medico='" . $id . "'";
				break;
			case 'todas':
				$titulo .= 'todas las recetas';
				break;
		}
		switch ($status) {
			case 'surtidas':
				$filtro .= " AND status='2'";
				break;
			case 'sin_surtir':
				$filtro .= " AND status='1'";
				break;
			case 'ambas':
				$filtro .= " AND status!='0'";
				break;
		}
		$orden_txt = "";
		switch ($orden) {
			case 'fecha':
				$orden_txt .= " ORDER BY fecha ASC, hora ASC";
				break;
			case 'servicio':
				$orden_txt .= " ORDER BY id_servicio";
				break;
			case 'medico':
				$orden_txt .= " ORDER BY id_medico";
				break;
		}
		
		$recetas = getRecetasXRango($fecha_de, $fecha_a, $filtro, $orden_txt);
		$out = '';
		$cont = 0;
		$colores = array('#FFFFFF', '#DDDDDD');
		if ($recetas[1] > 0) {
			$out .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
			$out .= '<tr><td colspan="9" class="tituloVentana">Recetas de: ' . $titulo . '</td></tr>';
			$out .= '<tr><th>Fecha y Hora</th><th>Cod. Barras</th><th>Derechohabiente</th><th>M&eacute;dico</th><th>Servicio</th><th>Medicamentos</th><th>Cant.</th><th>D&iacute;as</th><th>Surtido</th></tr>';
			for ($z=0; $z<$recetas[1]; $z++) {
				$registro = $recetas[0][$z];
				$datosMedico = getMedicoXid($registro['id_medico']);
				$datosServicio = getServicioXid($registro['id_servicio']);
				$datosDerecho = getDerechohabienteXidOk($registro['id_derecho']);
				$conceptos =  getDatosMedicamentoEnReceta($registro["id_receta"]);
				if ($conceptos != "") {
					$tConceptos = count($conceptos);
					$medicamentos = '';
					$cantidades = '';
					$dias = '';
					$surtido = '';
					$med = array('0', '0');
					if ($mostrar != 'medicamento') { // si se van a mostrar todas las recetas filtradas
						for ($i=0; $i < $tConceptos; $i++) {
							$medicamento = getDatosMedicamentos($conceptos[$i]["id_medicamento"]);
							$med[$i] = $conceptos[$i]['id_medicamento'];
							if ($conceptos[$i]['extra1'] == 's') {
								@$usuario = getUsuarioXid($conceptos[$i]["id_usuario_surtio"]);
								@$surtido = 'Surtido por: ' . @$usuario["nombre"] . ' | ' . @$conceptos[$i]["fecha"] . ' - ' . @$conceptos[$i]["hora"];
							} else {
								@$surtido = 'no';
							}
			
							$medicamentos .= $conceptos[$i]["id_medicamento"] . ' - ' . $medicamento["descripcion"] . '<br />';
							$cantidades .= $conceptos[$i]["cantidad"] . '<br />';
							$dias .= $conceptos[$i]["dias"] . '<br />';
						}
						$cont++;
						$color = $colores[$cont%2];
						$out .= '<tr style="background:' . $color . '" class="botones_menu"><td>' . $registro['fecha'] . ' - ' . $registro['hora'] . '</td><td>' . $registro['codigo_barras'] . '</td><td>' . $datosDerecho['cedula'] . '/' . $datosDerecho['cedula_tipo'] . ' - ' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td>' . ponerAcentos($datosServicio['nombre']) . '</td><td>' . $medicamentos . '</td><td>' . $cantidades . '</td><td>' . $dias . '</td><td>' . $surtido . '</td></tr>';
					} else { // se mostraran solo las recetas de los medicamentos con id_medicamento == $id
						$seMuestra = false;
						for ($i=0; $i < $tConceptos; $i++) {
							if ($conceptos[$i]["id_medicamento"] == $id) $seMuestra = true;
							$medicamento = getDatosMedicamentos($conceptos[$i]["id_medicamento"]);
							$med[$i] = $conceptos[$i]['id_medicamento'];
							if ($conceptos[$i]['extra1'] == 's') {
								$usuario = getUsuarioXid($conceptos[$i]["id_usuario_surtio"]);
								$surtido = 'Surtido por: ' . $usuario["nombre"] . ' | ' . $conceptos[$i]["fecha"] . ' - ' . $conceptos[$i]["hora"];
							} else {
								$surtido = 'no';
							}
			
							$medicamentos .= $conceptos[$i]["id_medicamento"] . ' - ' . $medicamento["descripcion"] . '<br />';
							$cantidades .= $conceptos[$i]["cantidad"] . '<br />';
							$dias .= $conceptos[$i]["dias"] . '<br />';
						}
						if ($seMuestra == true) {
							$cont++;
							$color = $colores[$cont%2];
							$out .= '<tr style="background:' . $color . '" class="botones_menu"><td>' . $registro['fecha'] . ' - ' . $registro['hora'] . '</td><td>' . $registro['codigo_barras'] . '</td><td>' . $datosDerecho['cedula'] . '/' . $datosDerecho['cedula_tipo'] . ' - ' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td>' . ponerAcentos($datosServicio['nombre']) . '</td><td>' . $medicamentos . '</td><td>' . $cantidades . '</td><td>' . $dias . '</td><td>' . $surtido . '</td></tr>';
						}
					}
				}
			}
			$out .= '</table>';
		}else {
			$out .= '<p>0 recetas</p>';
		}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Recetas</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/datepicker.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
	.ul_reporte {
		list-style:none;
		padding:0px;
		margin:0px;
	}
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo @$menu[@$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr>
    	<td align="center">
Seleccionaste Mostrar Recetas <b><?php echo $_POST['status'] ?></b> de <b><?php echo $_POST['mostrar'] ?></b> desde <b><?php echo $_POST['fecha_de'] ?></b> hasta <b><?php echo $_POST['fecha_a'] ?></b> y se ordenar&aacute; el reporte por <b><?php echo $_POST['orden'] ?>        
        </td>
    </tr>
    <tr><td align="center">
		<?php echo $out ?>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php
	} else {
		echo "<script  language=\"javascript\" type=\"text/javascript\">alert('error en variables'); history.back();</script>";
	}
} ?>