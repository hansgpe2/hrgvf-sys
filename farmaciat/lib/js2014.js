function buscarRecetasDe(){
	if (document.getElementById("dh") != undefined) {
		resp = document.getElementById("dh").value.split("|");
		var contenedor2;
		contenedor2 = document.getElementById('recetas');
		var agenda= new AjaxGET();
		agenda.open("POST", "liberarMedicamentoBuscar.php?id_derecho=" + resp[0],true);
		agenda.onreadystatechange=function()
		{
			if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor2.innerHTML = agenda.responseText;
			}
			if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
			{
				contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
			}
		}
		agenda.send(null)
	} else {
		alert("Selecciona un derechohabiente");
	}
}

function validarLiberarReceta(id_receta) {
	var motivo = document.getElementById("motivo_"+id_receta).value;
	if (motivo == "") {
		alert("Motivo de liberacion es requerido");
	} else {
		var contenedor;
		contenedor = document.getElementById('recetas');
		var objeto= new AjaxGET();
		objeto.open("GET", "liberarMedicamentoDO.php?id_receta=" + id_receta + "&motivo=" + motivo,true);
		objeto.onreadystatechange=function()
		{
			if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor.innerHTML = objeto.responseText;
			}
			if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
			{
				contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
			}
		}
		objeto.send(null);
	}

}