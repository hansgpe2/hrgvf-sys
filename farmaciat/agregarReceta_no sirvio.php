<?php
session_start ();
include_once('lib/misFunciones.php');
$id_horario = $_GET["id_horario"];
if (is_numeric($id_horario)) {
	$cita = getCita($id_horario, date('Ymd'));
	$derecho =  getDatosDerecho($cita["id_derecho"]);
	$id_servicio = regresarIdServicio($_SESSION["idDr"]);
	$servicio = getServicioXid($id_servicio);
	$medico = getMedicoXid($_SESSION["idDr"]);
	
	$folio = getFolioActual($_SESSION["idDr"]);
	if (count($folio) == 0) {
		$boton = '<span class="textoRojo">No tiene folios disponibles</span>';
	} else {
		$boton = '<input type="submit" name="agregar" id="agregar" value="Guardar Receta" class="botones" tabindex="11" />';
	}
	$medicamentos = getMedicamentosXservicio($_SESSION['idServ']);
//	$medicamentos = getMedicamentos();
	$tMed = count($medicamentos);
	$liMedicamentos = '<li><a href="#" id="" title=""><span>&nbsp;</span></a></li>';
	$liMedicamentos2 = '<li><a href="#" id="" title=""><span>&nbsp;</span></a></li>';

	for ($i=0; $i<$tMed; $i++) {
		$seMuestra = true;
		if ($medicamentos[$i]["extra1"] == '5') { // si el grupo es 5 es un medicamento que se asigna solo a determinados pacientes
			if (!tieneMedicamentoAsignado($cita["id_derecho"], $medicamentos[$i]["id_medicamento"])) $seMuestra = false;
		}
		if ($seMuestra == true) {
			$existencias = getExistenciasMedicamentos($medicamentos[$i]["id_medicamento"]);
			if ($existencias["existencia_actual"] == 0) $clase = 'sinExistencias'; else $clase = 'conExistencias';
				$liMedicamentos .= '<li><a onclick="javascript:actualizarMedicamento(\'' . $medicamentos[$i]["presentacion"] . '\',\'' . $medicamentos[$i]["unidad"] . '\',\'' . $medicamentos[$i]["extra1"] . '\',\'1\');" href="#" id="' . $medicamentos[$i]["id_medicamento"] . '" title="' . $medicamentos[$i]["descripcion"] . '"><span class="' . $clase . '">' . $medicamentos[$i]["descripcion"] . '</span></a></li>';
				$liMedicamentos2 .= '<li><a onclick="javascript:actualizarMedicamento(\'' . $medicamentos[$i]["presentacion"] . '\',\'' . $medicamentos[$i]["unidad"] . '\',\'' . $medicamentos[$i]["extra1"] . '\',\'2\');" href="#" id="' . $medicamentos[$i]["id_medicamento"] . '" title="' . $medicamentos[$i]["descripcion"] . '"><span class="' . $clase . '">' . $medicamentos[$i]["descripcion"] . '</span></a></li>';
		}
	}

	$opCantidades = '<option value="0" selected="selected"></option>';
	for ($i=1; $i<=60; $i++) {
		$opCantidades .= '<option value="' . $i . '">' . $i . '</option>';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaReceta" method="POST" action="javascript: validarAgregarReceta();">
<input name="id_derecho" id="id_derecho" type="hidden" value="<?php echo $cita["id_derecho"]; ?>" />
<input name="serie" id="serie" type="hidden" value="<?php echo $folio["serie"]; ?>" />
<input name="folio" id="folio" type="hidden" value="<?php echo ponerCeros($folio["folio_actual"],7); ?>" />
<input name="unidad_1" id="unidad_1" type="hidden" value="" />
<input name="unidad_2" id="unidad_2" type="hidden" value="" />
<input name="uni_1" id="uni_1" type="hidden" value="" />
<input name="uni_2" id="uni_2" type="hidden" value="" />
<input name="cantidad_1" id="cantidad_1" type="hidden" value="0" />
<input name="cantidad_2" id="cantidad_2" type="hidden" value="0" />
<input name="tipo_receta" id="tipo_receta" type="hidden" value="" />
<input name="medi_1" id="medi_1" type="hidden" value="" />
<input name="medi_2" id="medi_2" type="hidden" value="" />
<input name="grupo_1" id="grupo_1" type="hidden" value="" />
<input name="grupo_2" id="grupo_2" type="hidden" value="" />
<input name="diasTratamiento_1" id="diasTratamiento_1" type="hidden" value="1" />
<input name="diasTratamiento_2" id="diasTratamiento_2" type="hidden" value="1" />
<table width="730" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CAPTURAR RECETA</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">ENTIDAD FEDERATIVA:</td>
    <td align="left"><?php echo ENTIDAD_FEDERATIVA; ?>
    &nbsp;&nbsp;&nbsp;<span class="textosParaInputs">FECHA DE EXPEDICION: </span>
    <input name="fecha" type="text" id="fecha" maxlength="10" value="<?php echo date('d/m/Y') ?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO:</td>
    <td align="left"><?php echo $servicio; ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MEDICO:</td>
    <td align="left"><?php echo $medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]; ?> - <?php echo $medico["ced_pro"] ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right" valign="bottom">CEDULA: </td>
    <td align="left" valign="bottom"><?php echo $derecho["cedula"] . "/" . $derecho["cedula_tipo"]; ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right" valign="top">NOMBRE:</td>
    <td align="left" valign="top"><?php echo ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]); ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right" valign="top">DIAGNOSTICO:</td>
    <td align="left" valign="top"><input type="text" size="40" id="diagnostico" name="diagnostico" value="" tabindex="1"></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">&nbsp;</td>
    <td align="left">
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    	<table width="97%" border="0" class="ventana">
               	<td class="tituloVentana" colspan="2">MEDICAMENTO 1</td>
            	<td class="tituloVentana" colspan="2">MEDICAMENTO 2</td>
            <tr>
            	<td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left">
                    <div class="mi_select">
                            <input type="text" name="medicamento_1" id="medicamento_1" value="" tabindex="2">
                            <ul id="med1">
                                <?php echo $liMedicamentos ?>
                            </ul>
                    </div>
                </td>
                <td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left">
                    <div class="mi_select2">
                            <input type="text" name="medicamento_2" id="medicamento_2" value="" tabindex="7">
                            <ul id="med2">
                                <?php echo $liMedicamentos2 ?>
                            </ul>
                    </div>
                </td>
            </tr>
    		<tr>
            	<td class="textosParaInputs" align="right">PERIODO:</td>
                <td align="left"><select name="frecuencia_1" id="frecuencia_1" tabindex="3" onchange="javascript: actualizarPeriodo('1');"><option value="DIAS" selected="selected">Diario</option><option value="SEMANAS">Semanal</option><option value="MESES">Mensual</option><option value="TRIMESTRES">Trimestral</option></select></td>
                <td class="textosParaInputs" align="right">PERIODO:</td>
                <td align="left"><select name="frecuencia_2" id="frecuencia_2" tabindex="3" onchange="javascript: actualizarPeriodo('2');">
                  <option value="DIAS" selected="selected">Diario</option>
                  <option value="SEMANAS">Semanal</option>
                  <option value="MESES">Mensual</option>
                  <option value="TRIMESTRES">Trimestral</option>
                </select></td>
            </tr>
    		<tr>
            	<td class="textosParaInputs" align="right">DOSIS:</td>
                <td align="left"><input type="text" name="dosis_1" id="dosis_1" tabindex="4" value="" size="5" onkeydown="javascript: return validarNumero(event, this.value);" onblur="javascript: return compruebaNumero(this,'1')"> &nbsp;&nbsp;<div style="display:inline" class="textosParaInputs" id="presentacion_1">&nbsp;</div></td>
                <td class="textosParaInputs" align="right">DOSIS:</td>
                <td align="left"><input type="text" name="dosis_2" id="dosis_2" tabindex="9" value="" size="5" onkeydown="javascript: return validarNumero(event, this.value);" onblur="javascript: return compruebaNumero(this,'2')"> &nbsp;&nbsp;<div style="display:inline" class="textosParaInputs" id="presentacion_2">&nbsp;</div></td>
            </tr>
    		<tr>
            	<td class="textosParaInputs" align="right">TRATAMIENTO:</td>
                <td align="left"><div style="display:inline" id="divTrat_1"><select name="dias_1" id="dias_1" tabindex="5" onchange="javascript: actualizarCantidad('1')"><?php echo $opCantidades ?></select></div> <div id="periodo_1" style="display:inline" class="textosParaInputs">DIAS</div></td>
                <td class="textosParaInputs" align="right">TRATAMIENTO:</td>
                <td align="left"><div style="display:inline" id="divTrat_2"><select name="dias_2" id="dias_2" tabindex="10" onchange="javascript: actualizarCantidad('2')"><?php echo $opCantidades ?></select></div> <div id="periodo_2" style="display:inline" class="textosParaInputs">DIAS</div></td>
            </tr>
			<tr>
            	<td class="textosParaInputs" align="right">CANTIDAD:</td>
                <td align="left"><div id="divcantidad_1">&nbsp;</div></td>
                <td class="textosParaInputs" align="right">CANTIDAD:</td>
                <td align="left"><div id="divcantidad_2">&nbsp;</div></td>
            </tr>
			<tr>
            	<td class="textosParaInputs" align="right">INIDICACIONES:</td>
                <td align="left"><textarea name="indicaciones_1" id="indicaciones_1" cols="20" rows="5" tabindex="6"></textarea></td>
                <td class="textosParaInputs" align="right">INIDICACIONES:</td>
                <td align="left"><textarea name="indicaciones_2" id="indicaciones_2" cols="20" rows="5" tabindex="11"></textarea></td>
          	</tr>
       </table>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><div id="masRecetas" style="width:100%"></div></td>
  </tr>
<!--  <tr>
    <td>&nbsp;</td>
    <td align="right"><div id="div_agregar"><a href="#" onclick="javascript:agregarOtraReceta(3, '<?php echo $folio["serie"]; ?>', <?php echo $folio["folio_actual"]; ?>);" class="linkIndex">Agregar Otra Receta</a></div></td>
  </tr>

!-->  <tr>
    <td colspan="2" align="center"><span class="sinExistencias"><br /><span class="textoRojo">Los medicamentos en rojo tienen CERO existencias<br />Favor de buscar un alternativo antes de guardar la receta</span><br />
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicioMedico.php');" tabindex="11" />&nbsp;&nbsp;&nbsp;&nbsp;
  <?php echo $boton; ?>
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>


</body>
</html>
<?php
}
?>
