<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<?php
if ((isset($_GET["cantidades"])) && (isset($_GET["unidades"])) && (isset($_GET["dias"])) && (isset($_GET["medicamentos"])) && (isset($_GET["indicaciones"])) && (isset($_GET["id_derecho"])) && (isset($_GET["serie"])) && (isset($_GET["folio"])) && (isset($_GET["grupos"]))) {
	$cant = $_GET["cantidades"];
	$unid= $_GET["unidades"];
	$di = $_GET["dias"];
	$diT = $_GET["diasTratamiento"];
	$medi = $_GET["medicamentos"];
	$indi = $_GET["indicaciones"];
	$nuMed = $_GET["numerosMed"];
	$grps = $_GET["grupos"];
	$diagnostico = $_GET["diagnostico"];
	$cantidades = explode(",",$cant);
	$unidades = explode(",",$unid);
	$dias = explode(",",$di);
	$diasTratamiento = explode(",",$diT);
	$medicamentos = explode(",",$medi);
	$indicaciones = explode(",",$indi);
	$grupos = explode(",",$grps);
	$numerosMed = explode(",",$nuMed);
	$id_derecho = $_GET["id_derecho"];
	$serie = $_GET["serie"];
//	$folio = $_GET["folio"];
	$id_medico = $_SESSION["idDr"];
	$medico = getMedicoXid($id_medico);
	$id_servicio = regresarIdServicio($id_medico);
	$cuantasRecetas = count($cantidades) / 2;
	$id_receta2 = 0;
	if (is_integer($cuantasRecetas)) $cuantasRecetas = $cuantasRecetas; else $cuantasRecetas = ceil($cuantasRecetas); // sube el valor de cuantas recetas cuando no es entero;
	$ret = '';
	$nMed = 0;
	$id_receta = '';
	for($i=0; $i<$cuantasRecetas; $i++) {
		$folio = getFolioActual($_SESSION["idDr"]); // traemos los valores del folio del dr
		if ($folio["folio_actual"] < $folio["folio_final"]) { // si está el folio actual dentro de los folios disponibles entonces si agregarmos la receta
			$hayExistencias = true;
			$existeMed1 = '';
			$existeMed2 = '';
			$enTratamiento = true;
			if (tieneTratamiento($medicamentos[$nMed], $id_derecho)) {
				$ret .= '<span style="color:#ff0000">- Tiene tratamiento vigente del medicamento 1</span><br>';
				$enTratamiento = false;
			}
			if (isset($cantidades[$nMed+1])) {
				if (tieneTratamiento($medicamentos[$nMed+1], $id_derecho)) {
					$ret .= '<span style="color:#ff0000">- Tiene tratamiento vigente del medicamento 2</span><br>';
					$enTratamiento = false;
				}
			}
// CODIGO PARA COMPROBAR EXISTENCIAS DE MEDICAMENTOS			
			if (!hayMedicamento($medicamentos[$nMed], $cantidades[$nMed])) { 
				$ret .= '<span style="color:#ff0000">- El medicamento 1 no tiene existencias y se generó como vale</span><br>';
				$hayExistencias = false;
				$existeMed1 = 'N';
			}
			if (isset($cantidades[$nMed+1])) {
				if (!hayMedicamento($medicamentos[$nMed+1], $cantidades[$nMed+1])) {
					$ret .= '<span style="color:#ff0000">- El medicamento 2 no tiene existencias y se generó como vale</span><br>';
					$hayExistencias = false;
					$existeMed2 = 'N';
				}
			}

// CODIGO PARA COMPROBAR EXISTENCIAS DE MEDICAMENTOS			
			if ($enTratamiento) {
				if (($grupos[$nMed] == "2") || ($grupos[$nMed+1] == "2") || ($grupos[$nMed] == "3") || ($grupos[$nMed+1] == "3")) { // si alguno de los medicamentos es de grupo 2 o 3 se deben generar en recetas separadas
								$folioSiguiente = $folio["folio_actual"] + 1;
								$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . date("Ymd") . "','" . date("H:i:s") . "','','" . ENTIDAD_FEDERATIVA . "','" . CLAVE_UNIDAD_MEDICA . "','" . $diagnostico . "','" . $medico["ced_pro"] . "','" . $_SESSION["idUsuario"] . "','0','1','" . $_GET["tipo_receta"] . "');";
								$res = ejecutarSQL($sql);
								$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
								$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
								$res = ejecutarSQL($sql);
								$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
								$res = ejecutarSQL($sql);
								$ret .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
								$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $medicamentos[$nMed] . "','" . $cantidades[$nMed] . "','" . $unidades[$nMed] . "','" . (int)$dias[$nMed] * (int)$diasTratamiento[$nMed] . "','" . $indicaciones[$nMed] . "','" . $existeMed1 . "','','','');";
								$res = ejecutarSQL($sql);
								if ($existeMed1 == '') {
									restaExistenciasMedicamentos($medicamentos[$nMed], $cantidades[$nMed]);
									$ret .= '- Medicamento ' . $numerosMed[$nMed] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
								} else {
				// aquí podriamos restar medicamento pero se iría al - (menos)
									$ret .= '- Medicamento SIN EXISTENCIAS ' . $numerosMed[$nMed] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
								}
					//	si la receta tiene el segundo medicamento se agrega
								if (isset($cantidades[$nMed+1])) {
									$folioSiguiente2 = $folioSiguiente + 1;
									$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folioSiguiente . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . date("Ymd") . "','" . date("H:i:s") . "','','" . ENTIDAD_FEDERATIVA . "','" . CLAVE_UNIDAD_MEDICA . "','" . 		$diagnostico . "','" . $medico["ced_pro"] . "','" . $_SESSION["idUsuario"] . "','0','1','" . $_GET["tipo_receta"] . "');";
									$res = ejecutarSQL($sql);
									$id_receta2 = getIdRecetaAgregada($folio["serie"], $folioSiguiente, $id_medico, $id_derecho);
									$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente2 . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
									$res = ejecutarSQL($sql);
									$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta2,12) . "' WHERE id_receta='" . $id_receta2 . "' LIMIT 1";
									$res = ejecutarSQL($sql);
									$ret .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folioSiguiente,7) . " generada correctamente<br>";
									$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta2 . "','" . $medicamentos[$nMed+1] . "','" . $cantidades[$nMed+1] . "','" . $unidades[$nMed+1] . "','" . (int)$dias[$nMed+1] * (int)$diasTratamiento[$nMed+1] . "','" . $indicaciones[$nMed+1] . "','" . $existeMed2 . "','','','');";
									$res = ejecutarSQL($sql);
									if ($existeMed2 == '') {
										restaExistenciasMedicamentos($medicamentos[$nMed+1], $cantidades[$nMed+1]);
										$ret .= '- Medicamento ' . $numerosMed[$nMed+1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folioSiguiente,7) . " agregado correctamente<br>";
									} else {
					// aquí podriamos restar medicamento pero se iría al - (menos)
										$ret .= '- Medicamento SIN EXISTENCIAS ' . $numerosMed[$nMed+1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folioSiguiente,7) . " agregado correctamente<br>";
									}
								}
				} else { // si son grupo 4 o 1 (que se supone que no tienen del grupo 1) por default hago lo del grupo 4: medicamentos en la misma receta
								$folioSiguiente = $folio["folio_actual"] + 1;
								$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . date("Ymd") . "','" . date("H:i:s") . "','','" . ENTIDAD_FEDERATIVA . "','" . CLAVE_UNIDAD_MEDICA . "','" . $diagnostico . "','" . $medico["ced_pro"] . "','" . $_SESSION["idUsuario"] . "','0','1','" . $_GET["tipo_receta"] . "');";
								$res = ejecutarSQL($sql);
								$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
								$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
								$res = ejecutarSQL($sql);
								$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
								$res = ejecutarSQL($sql);
								$ret .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
								$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $medicamentos[$nMed] . "','" . $cantidades[$nMed] . "','" . $unidades[$nMed] . "','" . (int)$dias[$nMed] * (int)$diasTratamiento[$nMed] . "','" . $indicaciones[$nMed] . "','" . $existeMed1 . "','','','');";
								$res = ejecutarSQL($sql);
								if ($existeMed1 == '') {
									restaExistenciasMedicamentos($medicamentos[$nMed], $cantidades[$nMed]);
									$ret .= '- Medicamento ' . $numerosMed[$nMed] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
								} else {
				// aquí podriamos restar medicamento pero se iría al - (menos)
									$ret .= '- Medicamento SIN EXISTENCIAS ' . $numerosMed[$nMed] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
								}
					//	si la receta tiene el segundo medicamento se agrega
								if (isset($cantidades[$nMed+1])) {
									$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $medicamentos[$nMed+1] . "','" . $cantidades[$nMed+1] . "','" . $unidades[$nMed+1] . "','" . (int)$dias[$nMed+1] * (int)$diasTratamiento[$nMed+1] . "','" . $indicaciones[$nMed+1] . "','" . $existeMed2 . "','','','');";
									$res = ejecutarSQL($sql);
									if ($existeMed2 == '') {
										restaExistenciasMedicamentos($medicamentos[$nMed+1], $cantidades[$nMed+1]);
										$ret .= '- Medicamento ' . $numerosMed[$nMed+1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
									} else {
					// aquí podriamos restar medicamento pero se iría al - (menos)
										$ret .= '- Medicamento SIN EXISTENCIAS ' . $numerosMed[$nMed+1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
									}
								}
				}
			} else {
				$ret .= '<span style="color:#ff0000">- No se generó la receta porque el derechohabiente tiene tratamiento en curso</span>';
			}
		} else { // mensaje de error por falta de folios
			if (isset($numerosMed[$nMed])) $med1 = $numerosMed[$nMed];
			if (isset($numerosMed[$nMed+1])) $med2 = " y Medicamento " . $numerosMed[$nMed+1]; else $med2 = "";
			$ret .= '<span style="color:#ff0000">- No se generó Receta para Medicamento ' . $numerosMed[$nMed] . $med2 . " por falta de folios por parte del médico, pida se le asignen mas folios</span><br>";
		}
		$nMed = $nMed + 2;
	}
	
	print($ret . "|" . $id_receta . "|" . $id_receta2);

} else {
	echo '<span style="color:#ff0000">No se pudo agregar la Receta, pongase en contacto con el administrador del sistema</span>';
}
?>
