<?php
session_start ();
include_once('lib/misFunciones.php');
if($_SESSION['idUsuario'] > 0 && $_SESSION['paciente']>0) { 
    $datosDH=  getDatosDerecho($_SESSION['paciente']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarAgregarCita();">
<input name="id_derecho" id="id_derecho" type="hidden" value="<?php echo $_SESSION['paciente']; ?>" />
<input name="id_horario" id="id_horario" type="hidden" value="<?php echo $_GET['idHorario']; ?>" />
<input name="fechaCita" id="fechaCita" type="hidden" value="<?php echo $_GET['getdate']; ?>" />
<input name="id_cita" id="id_cita" type="hidden" value="" />
<input name="tipo_reprogramacion" id="tipo_reprogramacion" type="hidden" value="" />

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CITA PARA EL <?php echo formatoDia($_GET['getdate'], 'tituloCitasXdia'); ?> DE <?php echo $_GET['horaInicio'] ?> A <?php echo $_GET['horaFin'] ?> HORAS</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td><input type="text" name="cedula" id="cedula" value="<?php echo $datosDH['cedula']."/".$datosDH['cedula_tipo']; ?>" readonly="readonly" /></td>
      </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_p" type="text" id="ap_p" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($datosDH['ap_p']); ?>" />
        <input name="ap_m" type="text" id="ap_m" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($datosDH['ap_m']); ?>" />
        <input name="nombre" type="text" id="nombre" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($datosDH['nombres']); ?>" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><input name="telefono" type="text" id="telefono" size="20" maxlength="10" readonly="readonly" value="<?php echo $datosDH['telefono'] ?>" /><span class="textosParaInputs"> FECHA NAC. </span><input name="fecha_nac" type="text" id="fecha_nac" size="20" maxlength="10" readonly="readonly" value="<?php echo $datosDH['fecha_nacimiento']; ?>" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccion" type="text" id="direccion" size="64" maxlength="50" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($datosDH['direccion']); ?>" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><input type="text" name="estado" id="estado" value="<?php echo ponerAcentos($datosDH['estado']) ?>" readonly="1" />
        </select><span class="textosParaInputs">MUNICIPIO: </span><input type="text" name="municipio" id="municipio" value="<?php echo ponerAcentos($datosDH['municipio']) ?>" readonly="1" />
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
 <?php  
 
 $horario = getHorarioXid($_GET['idHorario']);
 if ($horario['tipo_cita'] == 0) {  //cita primera vez
 	$anoHoy = date('Y', strtotime("now"));
 	$mesHoy = date('m', strtotime("now"));
 	$diaHoy = date('d', strtotime("now"));
	$fecha = $_GET['getdate'];
	$dia = substr($fecha,6,2);
	$mes = substr($fecha,4,2);
	$ano = substr($fecha,0,4);

	$dias = diferenciaDeDiasEntreFechas($anoHoy, $mesHoy, $diaHoy, $ano, $mes, $dia); //calcular dias de diferencia entre hoy y la fecha de la cita que se pretende dar
	if ($dias > 15) { // si la cita es primera vez y mayor a 15 dias de diferencia entre la fecha actual, pedimos que se cheque si es concertada por el derechohabiente
?>
<?php
	}
 	
?>
  <tr>
    <td class="textosParaInputs" align="right">DIAGNOSTICO:</td>
    <td align="left"><input type="text" name="diagnostico" id="diagnostico" onkeyup="this.value = this.value.toUpperCase();" size="80" maxlength="150" /></td>
  </tr>
<?php
 }
?>
  <tr>
    <td class="textosParaInputs" align="right">OBSERVACIONES:</td>
    <td align="left"><input type="text" name="observaciones" id="observaciones" onkeyup="this.value = this.value.toUpperCase();" size="80" maxlength="150" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="selDia('<?php echo $_GET['getdate']; ?>','<?php echo $_GET['getdate']; ?>');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Agregar Cita" class="botones" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
  <tr>
    <td colspan="2" align="left">
  <input type="button" name="imprimir_cb" id="imprimir_cb" value="Imprimir C&oacute;digo de barras" class="botones" onclick="javascript: imprimir_exp();" />
  </tr>
</table>
</form>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="reprogramar" style="display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">REPROGRAMAR CITA</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
            <td colspan="2">
                <form id="selDH" method="POST" action="javascript: buscarDH(document.getElementById('cedulaBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <div id="listaCitasAreprogramar">&nbsp;</div>
                <tr>
                  <td colspan="2" align="center"><input name="cerrarReprogramar" type="button" class="botones" id="cerrarReprogramar" onclick="javascript: ocultarDiv('reprogramar');" value="Cerrar" />
                  &nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarCitaReprogramar" type="button" class="botones" id="seleccionarCitaReprogramar" onclick="javascript: cargarDatosCitaReprogramar();" value="Seleccionar Cita" />
                    <br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>




</body>
</html>
<?php
} else {
	$_SESSION['idUsuario'] = "-1";
	$_SESSION['tipoUsuario'] = "-1";
	$_SESSION['IdCon'] = "-1";
	$_SESSION['idServ'] = "-1";
	$_SESSION['idDr'] = "-1";
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Tu sesion ha caducado'); location.replace('index.php');</script>";
}
?>