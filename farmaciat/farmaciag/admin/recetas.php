<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {

	$datos = getRecetas();
	$TDatos = count($datos);
	
	$out = "<table class=\"ventana\" width=\"790\"><tr><td>";
	
	for ($i=0;$i<$TDatos;$i++) {
		$paciente = getDatosDerecho($datos[$i]['id_derecho']);
		$nombrePac = ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']);
		$servicio = getServicioXid($datos[$i]['id_servicio']);
		$medico = getMedicoXid($datos[$i]['id_medico']);
		$nombreMed = ponerAcentos($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']);
		$conceptos = getMedicamentosEnReceta($datos[$i]['id_receta']);
		$Tconceptos = count($conceptos);
		$medicamento = array();
		for($j=0; $j<$Tconceptos; $j++) {
			$med = getDatosMedicamentos($conceptos[$j]['id_medicamento']);
			$medicamento[$j] = "<table class=\"ventana\" width=\"100%\"><tr><td><b>Medicamento:</b><br>" . ponerAcentos($med['descripcion']) . "</td></tr><tr><td><b>Indicaciones:</b><br>" . ponerAcentos($conceptos[$j]['indicaciones']) . "</td></tr></table>";
		}
		$out.= "<table class=\"ventana\" width=\"100%\">
					<tr><td class=\"tituloVentana\" colspan=\"2\">N. Serie: " . $datos[$i]['n_serie'] . "</td></tr>
					<tr><td><b>Fecha Captura: </b>" . formatoDia($datos[$i]['fecha_captura'],"imprimirCita") . "</td><td><b>Fecha Expedición: </b>" . formatoDia($datos[$i]['fecha_expedicion'],"imprimirCita") . "</td></tr>
					<tr><td valign=\"top\"><b>Entidad: </b>" . ponerAcentos($datos[$i]['entidad']) . "</td><td><b>Servicio: </b>" . ponerAcentos($servicio['nombre']) . "</td></tr>
					<tr><td><b>Paciente: </b>" . $nombrePac . "</td><td><b>Cedula: </b>" . $paciente['cedula'] . "/" . $paciente['cedula_tipo'] . "</td></tr>
					<tr><td>" . $medicamento[0] . "</td><td>" . $medicamento[1] . "</td></tr>
					<tr><td colspan=\"2\" align=\"center\"><b>Medico: </b>" . $nombreMed . "</td></tr>";
//					<tr><td colspan=\"2\" align=\"right\"><input name=\"eli" . $i . "\" id=\"eli" . $i . "\" type=\"button\" value=\"Eliminar Receta\" class=\"botones\" onClick=\"javascript: location.href= 'recetas_eliminar.php?id_receta=" . $datos[$i]['id_receta'] . "'\" /></td></tr>
		$out.= "</table><br>";
	}
	$out.= "</td></tr></table>";


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Medicamentos Oncológicos</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Medicamentos Oncológicos&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><? echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center"><? echo $out; ?>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<? 
} ?>