<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {

	$selectDr = 'De M&eacute;dico: <select name="medico" id="medico">';
	$drs = getMedicos();
	$tDrs = count($drs);
	for ($i=0; $i<$tDrs; $i++) {
		$selectDr .= '<option value="' . $drs[$i]["id_medico"] . '">' . ponerAcentos(htmlentities($drs[$i]["ap_p"] . ' ' . $drs[$i]["ap_m"] . ' ' . $drs[$i]["nombres"])) . '</option>';
	}
	$selectDr .= '</select>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Recetas</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
<script language="javascript" type="text/javascript">
	function cambiaTipo(obj) {
		if ((obj.value == "1") || (obj.value == "8")) document.getElementById("div_dr").style.visibility = "visible"; else document.getElementById("div_dr").style.visibility = "hidden";
	}
</script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="usuarios_agregar_confirmar.php" onsubmit="MM_validateForm('usuario','','L','contra','','L','nombre','','R','cedula','','R');return document.MM_returnValue" method="post" name="forma">
    	<table class="ventana" width="550">
          <tr class="tituloVentana">
            <td colspan="2">Agregar Usuario</td>
          </tr>
		  <tr><td>Nombre de Usuario:</td><td><input name="usuario" type="text" size="15" maxlength="12" id="usuario" /></td></tr>
		  <tr>
		    <td>Contraseña:</td>
		    <td><input name="contra" type="text" size="15" maxlength="12" id="contra" /></td></tr>
		  <tr><td>Nombre:</td>
		  <td><input name="nombre" type="text" size="40" maxlength="50" id="nombre" /></td></tr>
		  <tr><td>REG. D.G.P.:</td>
		  <td><input name="cedula" type="text" size="40" maxlength="50" id="cedula" /></td></tr>
		  <tr>
		    <td valign="top">Tipo de Usuario:</td>
		    <td><select name="tipo_usuario" id="tipo_usuario" onchange="javascript: cambiaTipo(this);">
		      <option value="1">MEDICO</option>
		      <option value="2" selected="selected">FARMACIA</option>
		      <option value="3">CONSULTA EXTERNA</option>
		      <option value="8">USUARIO ESPECIAL</option>
		      <option value="0">ADMINISTRADOR</option>
		      </select><br /><br />
              <div id="div_dr" style="visibility:hidden;"><?php echo $selectDr; ?></div>
		    </td>
		  </tr>
		  <tr><td align="center" colspan="2"><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" onclick="" value="Agregar Usuario" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php 
} ?>