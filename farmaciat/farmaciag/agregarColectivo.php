<?php
session_start ();
include_once('lib/misFunciones.php');
$id_servicio = $_GET["id_servicio"];
if (is_numeric($id_servicio)) {
	$datosColectivo = getServicioXid($id_servicio);

	$cantidades = array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40");
	$tCantidades = count($cantidades);
	$opCantidades = '<option value="0" selected="selected"></option>';
	for ($i=0; $i<$tCantidades; $i++) {
		$opCantidades .= '<option value="' . $cantidades[$i] . '">' . $cantidades[$i] . '</option>';
	}

	$medicamentos = getMedicamentos();
	$tMed = count($medicamentos);
	$out = '';
	if ($tMed > 0) {
	$out .= '<table width="80%" border="0" class="ventana" cellpadding="2" cellspacing="0">
        	<tr><td colspan="3" align="center" height="35"><input type="button" class="botones" value="Generar..." onclick="javascript:agregarColectivoValidar();"></td></tr>
			<tr>
            	<td class="tituloVentana">EXIS.</td>
            	<td class="tituloVentana">MEDICAMENTO</td>
            	<td class="tituloVentana">CANT.</td>
            </tr>';
			$clase = 'tablaFondoAzul';
		for ($i=0; $i<$tMed; $i++) {
			$existencias = getExistenciasMedicamentos($medicamentos[$i]["id_medicamento"]);
			if ($existencias["existencia_actual"] > 0) {
				if ($clase == 'tablaFondoAzul') $clase = 'tablaFondoBco'; else $clase = 'tablaFondoAzul';
				$out .= '<tr class="' . $clase . '"><td align="center">' . $existencias["existencia_actual"] . '</td><td align="left" class="conExistencias">' . $medicamentos[$i]["descripcion"] . '</td><td><select id="' . $medicamentos[$i]["id_medicamento"] . '" name="' . $medicamentos[$i]["id_medicamento"] . '">' . $opCantidades . '</select><input type="hidden" id="e_' . $medicamentos[$i]["id_medicamento"] . '" name="e_' . $medicamentos[$i]["id_medicamento"] . '" value="' . $existencias["existencia_actual"] . '"></td></tr>';
			}
		}
		$out .= '<tr><td colspan="3" height="35" align="center"><input type="button" class="botones" value="Generar..." onclick="javascript:agregarColectivoValidar();"></td></tr></table>';
	} else {
		$out .= 'NO HAY MEDICAMENTOS EN LA TABLA';
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<div id="mascara" style="width:100%; height:100%; background-color:#CCC; color:#000000; position:absolute; top:1px; left:1px; z-index:100; visibility:hidden;">Generando colectivo...</div>
<form id="formaColectivo" method="POST">
<input name="id_servicio" id="id_servicio" type="hidden" value="<?php echo $id_servicio; ?>" />

<table width="730" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CAPTURAR COLECTIVO</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO: </td>
    <td align="left"><?php echo $datosColectivo; ?>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="textosParaInputs">FECHA DE EXPEDICION: </span>
    <input name="fecha" type="text" id="fecha" maxlength="10" value="<?php echo date('d/m/Y') ?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    	<?php echo $out ?>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><div id="masRecetas" style="width:100%"></div></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><span class="sinExistencias"><br />
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicioColectivos.php');" tabindex="24" />&nbsp;&nbsp;&nbsp;&nbsp;
  <?php echo $boton; ?>
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>


</body>
</html>
<?php
}
?>
