function ponerCeros2(cant, cuantos) {
	aCeros = new Array("","0","00","000","0000","00000","000000","0000000","00000000","000000000","0000000000","00000000000","000000000000");
	cantStr = cant.toString();
	tCant = cantStr.length;
	cuantos = cuantos - tCant;
	return aCeros[cuantos] + cantStr;
}

function recetasXpaciente() {
	$.ajax({
	  type: "POST",
	  url: "recetasXpacienteBuscar.php"
//	  data: { name: "John", location: "Boston" }
	}).done(function( msg ) {
		$("#contenido").html(msg);
		$("#codigoBuscar").focus();
	});
}

function recetasXpacienteTodas() {
	$.ajax({
	  type: "POST",
	  url: "recetasXpacienteBuscarTodas.php"
//	  data: { name: "John", location: "Boston" }
	}).done(function( msg ) {
		$("#contenido").html(msg);
		$("#cedulaBuscar").focus();
	});
}

function buscarPor2(valor) {
	switch (valor) {
		case 'nombre':
			ocultarDiv('buscarPorBarras');
			ocultarDiv('buscarPorCedula');
			mostrarDiv('buscarPorNombre');
			break;
		case 'cedula':
			ocultarDiv('buscarPorBarras');
			ocultarDiv('buscarPorNombre');
			mostrarDiv('buscarPorCedula');
			break;
		case 'barras':
			ocultarDiv('buscarPorCedula');
			ocultarDiv('buscarPorNombre');
			mostrarDiv('buscarPorBarras');
			break;
	}
}

function recetasSinSurtir(id_derecho) {
	if (id_derecho == '-1') {
		alert('Debe seleccionar un derechohabiente');
	} else {
		var datos = id_derecho.split('|');
		var id_d = ponerCeros2(datos[0], 12);
		buscarDHbarras(id_d);
	}
}

function buscarDHbarras(barras) {
	if (barras.length == 12) {
		$.ajax({
		  type: "POST",
		  url: "recetasXpacienteGET.php",
		  data: { cod_bar: barras }
		}).done(function( msg ) {
			$("#recetas").html(msg);
		});
	} else
	{
		alert("El codigo de barras debe ser de 12 digitos");
	}
}

function recetasTodas(id_derecho) {
	if (id_derecho == '-1') {
		alert('Debe seleccionar un derechohabiente');
	} else {
		var datos = id_derecho.split('|');
		var id_d = ponerCeros2(datos[0], 12);
		buscarDHbarrasTodas(id_d);
	}
}

function buscarDHbarrasTodas(barras) {
	if (barras.length == 12) {
		$.ajax({
		  type: "POST",
		  url: "recetasXpacienteTodasGET.php",
		  data: { cod_bar: barras }
		}).done(function( msg ) {
			$("#recetas").html(msg);
		});
	} else
	{
		alert("El codigo de barras debe ser de 12 digitos");
	}
}

function SurtirRecetaXpaciente(id_receta, med1, med2, id_derecho) {
	if (confirm('Desea Surtir esta receta')) {
		$.ajax({
		  type: "POST",
		  url: "recetasSURTIR.php",
		  data: { id_receta: id_receta, med1: med1, med2: med2 }
		}).done(function( msg ) {
			alert(msg);
			buscarDHbarras(id_derecho);
		});
	}
}
