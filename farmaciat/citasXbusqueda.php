<?php
error_reporting(E_ALL^E_NOTICE);
include_once('lib/misFunciones.php');

	$citas = citasXderechohabiente($_GET['id_derecho']);
	$tCitas = count($citas);
	$out = "";
	$out.= "<br><table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
	if ($tCitas > 0 ) {
		for($i=0;$i<$tCitas;$i++) {
			$horario = getHorarioXid($citas[$i]['id_horario']);
			$datosCita = citasOcupadasXdia($citas[$i]['id_horario'],$citas[$i]['fecha_cita']);
			$datosDerecho = getDatosDerecho($_GET['id_derecho']);
			$datosMedico = getMedicoXid($horario['id_medico']);
			$datosServicio = getServicioXid($horario['id_servicio']);
			$nombreMedico = strtoupper($datosMedico['titulo'] . " " . $datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
			$nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
			$datosUsuario = getUsuarioXid($datosCita['id_usuario']);
		
			$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'],0,12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
			$datosCita = $datosServicio . " - " . $nombreMedico;
			if($horario['tipo_cita'] == 0) $claseParaDia = "citaXdiaPRV"; 
			if($horario['tipo_cita'] == 1) $claseParaDia = "citaXdiaSUB"; 
			if($horario['tipo_cita'] == 2) $claseParaDia = "citaXdiaPRO"; 
			$fecha = formatoDia($citas[$i]['fecha_cita'],"imprimirCita");
//			$recetas = getRecetasXderecho($_GET['id_derecho'], $horario['id_medico'], $horario['id_servicio']);
			$receta = getRecetasXderechoXcita($_GET['id_derecho'], $horario['id_medico'], $horario['id_servicio'], $citas[$i]['fecha_cita']);
			$datosReceta = '';
			$tRecetas = count($receta);
			if ($tRecetas > 0) {
				for ($j = 0; $j<$tRecetas; $j++) {
					$conceptos =  getDatosMedicamentoEnReceta($receta[$j]["id_receta"]);
					$medicamento1 = getDatosMedicamentos($conceptos[0]["id_medicamento"]);
					$medicamento2 = '';
					$nMed2 = '';
					$dMed2 = '';
					$cMed2 = '';
					$datosExtra1 = '';
					$datosExtra0 = '';
					if(count($conceptos)>1) {
						$medicamento2 = getDatosMedicamentos($conceptos[1]["id_medicamento"]);
						$nMed2 = $medicamento2["id_medicamento"] . ' - ' . $medicamento2["descripcion"];
						$dMed2 = 'Cant.Cajas: ' . $conceptos[1]["cantidad"] . '&nbsp;&nbsp;&nbsp;Dias_Tratamiento. ' . $conceptos[1]["dias"];
						$cMed2 = 'Indic.: ' . $conceptos[1]["indicaciones"];						
						if ($conceptos[1]['extra1'] == 's') {
							$usuario = getUsuarioXid($conceptos[1]["id_usuario_surtio"]);
							$datosExtra1 = 'Medicamento surtido por: ' . $usuario["nombre"] . ' | ' . formatoDia($conceptos[1]["fecha"],"fecha") . ' - ' . $conceptos[1]["hora"];
						} else {
							$datosExtra1 = 'Medicamento NO surtido';
						}
					}
					if ($conceptos[0]['extra1'] == 's') {
						$usuario = getUsuarioXid($conceptos[0]["id_usuario_surtio"]);
						$datosExtra0 = 'Medicamento surtido por: ' . $usuario["nombre"] . ' | ' . formatoDia($conceptos[0]["fecha"],"fecha") . ' - ' . $conceptos[0]["hora"];
					} else {
						$datosExtra0 = 'Medicamento NO surtido';
					}


					$datosReceta.= '<table width="100%" cellpadding="2" cellspacing="0" border="1" class="citaXdiaInfo"><tr><td colspan="2" align="center">No_Receta:' . $receta[$j]["codigo_barras"] . '</td></tr>';
					$datosReceta.= '<tr><td align="left" width="50%">' .  $medicamento1["id_medicamento"] . ' - ' . $medicamento1["descripcion"] . '</td><td align="left" width="50%">' . $nMed2 . '</td></tr>';
					$datosReceta.= '<tr><td align="left" width="50%">Cant.Cajas: ' . $conceptos[0]["cantidad"] . '&nbsp;&nbsp;&nbsp;Dias_Tratamiento. ' . $conceptos[0]["dias"] . '</td><td align="left" width="50%">' . $dMed2 . '</td></tr>';
					$datosReceta.= '<tr><td align="left" width="50%">Indic.: ' . $conceptos[0]["indicaciones"] . '</td><td align="left" width="50%">' . $cMed2 . '</td></tr>';
					$datosReceta.= '<tr><td align="center">' . $datosExtra0 . '</td><td align="center">' . $datosExtra1 . '</td></tr>';
					$datosReceta.= '</table>';
				}
			}
			$out.="
				  <tr>
					<td class=\"" . $claseParaDia . "\">
						<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td width=\"180\" class=\"citaXdiaHora\">" . $fecha . " - " . formatoHora($horario['hora_inicio']) . " - " . formatoHora($horario['hora_fin']) . "</td>
							<td align=\"left\" width=\"520\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
						  </tr>
						  <tr>
							<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($datos) . "</td>
						  </tr>
						  <tr>
						  	<td>&nbsp;</td>
							<td align=\"left\" class=\"citaXdiaInfo\">" . ponerAcentos($datosCita) . "</td>
						  </tr>
						  <tr>
						  	<td colspan=\"2\">" . $datosReceta . "
							</td>
						  </tr>
						</table>
					</td>
				  </tr>";
	
	  }
	} else {
	$out .= "
				<tr>
					<td align=\"center\">
						NO EXISTEN CITAS DEL DERECHOHABIENTE
					</td>
				  </tr>	";
	}

	$out.= "</table>";
	$out.="<p align=\"center\"><table width=\"500\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:10px;\">
<tr>
<td width=\"35\" height=\"25\" class=\"citaXdiaPRV\"></td><td width=\"129\">Citas Primera Vez</td>
<td width=\"35\" class=\"citaXdiaSUB\"></td><td width=\"138\">Citas Subsecuentes</td>
<td width=\"35\" class=\"citaXdiaPRO\"></td><td width=\"138\">Citas Procedimientos</td>
</tr>
</table></p>";
//	print($out);
	
	
	
	$citas = citasExtXderechohabiente($_GET['id_derecho']);
	$tCitas = count($citas);
//	$out = "";
	$out.= "<br><table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
	if ($tCitas > 0 ) {
		for($i=0;$i<$tCitas;$i++) {
//			$horario = getHorarioXid($citas[$i]['id_horario']);
//			$datosCita = citasOcupadasXdia($citas[$i]['id_horario'],$citas[$i]['fecha_cita']);
			$datosDerecho = getDatosDerecho($_GET['id_derecho']);
			$datosMedico = getMedicoXid($citas[$i]['id_medico']);
			$datosServicio = getServicioXid($citas[$i]['id_servicio']);
			$nombreMedico = strtoupper($datosMedico['titulo'] . " " . $datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
			$nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
			$datosUsuario = getUsuarioXid($citas[$i]['id_usuario']);
		
			$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'],0,12) . "... " . $citas[$i]['extra1'] . " - " . $citas[$i]['observaciones'];
			$datosCita = $datosServicio . " - " . $nombreMedico;
			if($horario['tipo_cita'] == 0) $claseParaDia = "citaXdiaPRV"; 
			if($horario['tipo_cita'] == 1) $claseParaDia = "citaXdiaSUB"; 
			if($horario['tipo_cita'] == 2) $claseParaDia = "citaXdiaPRO"; 
			$fecha = formatoDia($citas[$i]['fecha_cita'],"imprimirCita");
                        $receta = getRecetasXderechoXcita($_GET['id_derecho'], $citas[$i]['id_medico'], $citas[$i]['id_servicio'], $citas[$i]['fecha_cita']);
			$datosReceta = '';
			$tRecetas = count($receta);
			if ($tRecetas > 0) {
				for ($j = 0; $j<$tRecetas; $j++) {
					$conceptos =  getDatosMedicamentoEnReceta($receta[$j]["id_receta"]);
					$medicamento1 = getDatosMedicamentos($conceptos[0]["id_medicamento"]);
					$medicamento2 = '';
					$nMed2 = '';
					$dMed2 = '';
					$cMed2 = '';
					$datosExtra1 = '';
					$datosExtra0 = '';
					if(count($conceptos)>1) {
						$medicamento2 = getDatosMedicamentos($conceptos[1]["id_medicamento"]);
						$nMed2 = $medicamento2["id_medicamento"] . ' - ' . $medicamento2["descripcion"];
						$dMed2 = 'Cant.Cajas: ' . $conceptos[1]["cantidad"] . '&nbsp;&nbsp;&nbsp;Dias_Tratamiento. ' . $conceptos[1]["dias"];
						$cMed2 = 'Indic.: ' . $conceptos[1]["indicaciones"];						
						if ($conceptos[1]['extra1'] == 's') {
							$usuario = getUsuarioXid($conceptos[1]["id_usuario_surtio"]);
							$datosExtra1 = 'Medicamento surtido por: ' . $usuario["nombre"] . ' | ' . formatoDia($conceptos[1]["fecha"],"fecha") . ' - ' . $conceptos[1]["hora"];
						} else {
							$datosExtra1 = 'Medicamento NO surtido';
						}
					}
					if ($conceptos[0]['extra1'] == 's') {
						$usuario = getUsuarioXid($conceptos[0]["id_usuario_surtio"]);
						$datosExtra0 = 'Medicamento surtido por: ' . $usuario["nombre"] . ' | ' . formatoDia($conceptos[0]["fecha"],"fecha") . ' - ' . $conceptos[0]["hora"];
					} else {
						$datosExtra0 = 'Medicamento NO surtido';
					}
					$datosReceta.= '<table width="100%" cellpadding="2" cellspacing="0" border="1" class="citaXdiaInfo"><tr><td colspan="2" align="center">No_Receta:' . $receta[$j]["codigo_barras"] . '</td></tr>';
					$datosReceta.= '<tr><td align="left" width="50%">' .  $medicamento1["id_medicamento"] . ' - ' . $medicamento1["descripcion"] . '</td><td align="left" width="50%">' . $nMed2 . '</td></tr>';
					$datosReceta.= '<tr><td align="left" width="50%">Cant.Cajas: ' . $conceptos[0]["cantidad"] . '&nbsp;&nbsp;&nbsp;Dias_Tratamiento. ' . $conceptos[0]["dias"] . '</td><td align="left" width="50%">' . $dMed2 . '</td></tr>';
					$datosReceta.= '<tr><td align="left" width="50%">Indic.: ' . $conceptos[0]["indicaciones"] . '</td><td align="left" width="50%">' . $cMed2 . '</td></tr>';
					$datosReceta.= '<tr><td align="center">' . $datosExtra0 . '</td><td align="center">' . $datosExtra1 . '</td></tr>';
					$datosReceta.= '</table>';
				}
			}
			$out.="
				  <tr>
					<td class=\"" . $claseParaDia . "\">
						<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td width=\"180\" class=\"citaXdiaHora\">" . $fecha . " - " . formatoHora($horario['hora_inicio']) . " - " . formatoHora($horario['hora_fin']) . "</td>
							<td align=\"left\" width=\"520\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
						  </tr>
						  <tr>
							<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($datos) . "</td>
						  </tr>
						  <tr>
						  	<td>&nbsp;</td>
							<td align=\"left\" class=\"citaXdiaInfo\">" . ponerAcentos($datosCita) . "</td>
						  </tr>
                                                  <tr>
						  	<td colspan=\"2\">" . $datosReceta . "
							</td>
						  </tr>
						</table>
					</td>
				  </tr>";
	
	  }
	} else {
	$out .= "
				<tr>
					<td align=\"center\">
						NO EXISTEN CITAS EXTEMPORANEAS DEL DERECHOHABIENTE
					</td>
				  </tr>	";
	}

	$out.= "</table>";
	$out.="<p align=\"center\"><table width=\"500\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:10px;\">
<tr>
<td width=\"35\" height=\"25\" class=\"citaXdiaPRV\"></td><td width=\"129\">Citas Primera Vez</td>
<td width=\"35\" class=\"citaXdiaSUB\"></td><td width=\"138\">Citas Subsecuentes</td>
<td width=\"35\" class=\"citaXdiaPRO\"></td><td width=\"138\">Citas Procedimientos</td>
</tr>
</table></p>";
	print($out);
	
	
?>
