<?php
session_start();
include_once('lib/misFunciones.php');
$fecha=  time();
$horas=2;
$fecha+=($horas*60);
$fechaYhora=date("d/m/Y - H:i",$fecha);
$fechaCita=$_GET['fechaCita'];
$idDer=$_GET['id_derecho'];
$serv=$_SESSION['idServ'];
$cons=$_SESSION['IdCon'];
$med=$_SESSION['idDr'];
$horaI=$_GET['hora_inicio'];
$horaF=$_GET['hora_fin'];
$obs=$_GET['obs'];
$tipoCita=$_GET['tipo_cita'];
$sql="SELECT id_cita FROM citas_extemporaneas WHERE fecha_cita=".$fechaCita." AND id_derecho=".$idDer." AND id_servicio=".$serv."
 UNION
SELECT c.id_cita FROM citas AS c JOIN horarios AS h ON(h.id_horario=c.id_horario) WHERE c.fecha_cita=".$fechaCita." AND c.id_derecho=".$idDer." AND h.id_servicio=".$serv;
$duplicada=  existeDuplica($sql);
if($duplicada)
    echo "alert('Ya existe la cita');";
else
{
    $sql="SELECT fecha_cita FROM citas_extemporaneas WHERE id_derecho=".$idDer." AND id_servicio=".$serv."
 UNION
SELECT c.fecha_cita FROM citas AS c JOIN horarios AS h ON(h.id_horario=c.id_horario) WHERE c.id_derecho=".$idDer." AND h.id_servicio=".$serv." order by fecha_cita DESC limit 1";
    $res1=  ejecutarSQLAgenda($sql);
    $fechaUltimaCita=$res1['fecha_cita'];
    if($fechaCita==$fechaUltimaCita)
        echo "alert('la fecha seleccionada es la misma de hoy');";
    else if($fechaCita<$fechaUltimaCita)
        echo "alert('la fecha seleccionada es menor a la fecha de la ultima cita".date("d/m/Y",  strtotime($fechaUltimaCita))."');";
    else
    {
        $fechaCitaU=  strtotime($fechaCita);
        $fechaUltimaCitaU=  strtotime($fechaUltimaCita);
        $fecha6meses=$fechaUltimaCitaU+((30*24*60*60)*6);
        if($fechaCitaU>$fecha6meses)
        echo "alert('La fecha no puede ser mayor al ".date("d/m/Y",$fecha6meses)."');";
        else
        {
            $sql="INSERT INTO citas_extemporaneas VALUES(NULL,".$cons.",".$serv.",".$med.",".$idDer.",".quitarPuntosHora($horaI).",".quitarPuntosHora($horaF).",".$tipoCita.",".$fechaCita.",1,'".$obs."',".$med.",'".$fechaYhora."',2);";
            $res=  ejecutarSQLR($sql);
            if($res[0]==0)
            {
                $sql="SELECT id_cita,`id_consultorio`,id_servicio,id_medico,id_derecho,hora_inicio,hora_fin,tipo_cita,fecha_cita FROM citas_extemporaneas WHERE id_derecho=".$idDer." AND id_servicio=".$serv." ORDER BY id_cita DESC limit 1";
                $citaEGen=  ejecutarSQLAgenda($sql);
                $idCita=$citaEGen['id_cita'];
                $sql="INSERT INTO citas_medico VALUES(NULL,".date("Ymd").",".$idCita.",".$idDer.",".$med.",TRUE);";
                $res=  ejecutarSQLR($sql);
                if($res[0]==0)
                {
                    ejecutarLOG("Cita extemporanea nueva por Medico|id_con:".$cons."|id_serv:".$serv."|id_med:".$med."|dh:".$idDer."|fecha:".$fechaCita."|status:1|observaciones:".$obs."|hora_inicio:".quitarPuntosHora($horaI)."|hora_fin:".quitarPuntosHora($horaF));
					if($_GET['tipo_cita']==0)
				{
					$sql="SELECT * FROM `referencia_contrarreferencia` where id_derecho".$_GET['id_derecho']." And id_servicio=".$_SESSION['idServ'];
					$queryCon=ejecutarSQLAgenda($sql);
					if(count($queryCon)==0)
						$sql="insert into referencia_contrarreferencia VALUES(NULL,".$_GET['id_derecho'].",".$_SESSION['idServ'].",1,0,1);";
					else
						$sql="update referencia_contrarreferencia set citas_subsecuentes=citas_subsecuentes+1 where id_derecho=".$_GET['id_derecho']." AND id_servicio=".$_SESSION['idServ'];
						$query=ejecutarSQLR($sql);
					}
					else
					if($_GET['tipo_cita']==1)
					{
						$sql="update referencia_contrarreferencia set citas_subsecuentes=citas_subsecuentes+1 where id_derecho=".$_GET['id_derecho']." AND id_servicio=".$_SESSION['idServ'];
						$query=ejecutarSQLR($sql);
					}
                    print($cons."|".$serv."|".$idDer."|".$fechaCita."|".$med."|".$horaI."|".$horaF."|no");
                }
                else
                    echo "alert('Error en insercion de cita,\n Contacte al administrador del sistema');";
            }
            else
                echo "alert('Error en insercion de cita,\n Contacte al administrador del sistema');";
        }
    }
}

?>