<?php
session_start ();
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');
?>
<?php
error_reporting(E_ALL ^ E_NOTICE);

function agregarReceta1medicamento3meses2medicamentos ($id_medico, $id_servicio, $id_derecho, $entidad_federativa, $clave_uni_med, $diagnostico, $ced_prof, $id_usuario, $tipo_receta, $id_medicamentos, $cantidades, $unidades, $dias, $diasTratamientos, $indicaciones, $extra, $n_meds) {
	$ret = array(
		'id_receta' => '',
		'txt' => '',
		'extra' => ''
	);
	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
	$fecha = date("Y-m-d");
	$hora = date("H:i:s");
	$fecha1 = date('Ymd');
	$fecha2 = date('Ymd', strtotime('+30 days'));
	$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha1 . "','" . $hora . "','','" . $entidad_federativa . "_D','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
	$ret['id_receta'] = $id_receta."_D";
	$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
	$res = ejecutarSQL($sql);
			$sql_concepto = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamentos[0] . "','" . $cantidades[0] . "','" . $unidades[0] . "','" . (int)$dias[0] * (int)$diasTratamientos[0] . "','" . $indicaciones[0] . "','" . $extra . "','','','');";
			$res = ejecutarSQL($sql_concepto);
			if ($extra == '') {
				restaExistenciasMedicamentos($id_medicamentos[0], $cantidades[0]);
				$ret['txt'] .= '- Medicamento ' . $n_meds[0] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
			} else {
				// aquí podriamos restar medicamento pero se iría al - (menos)
				$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_meds[0] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
			}
			$sql_concepto = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamentos[1] . "','" . $cantidades[1] . "','" . $unidades[1] . "','" . (int)$dias[1] * (int)$diasTratamientos[1] . "','" . $indicaciones[1] . "','" . $extra . "','','','');";
			$res = ejecutarSQL($sql_concepto);
			if ($extra == '') {
				restaExistenciasMedicamentos($id_medicamentos[1], $cantidades[1]);
				$ret['txt'] .= '- Medicamento ' . $n_meds[1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
			} else {
				// aquí podriamos restar medicamento pero se iría al - (menos)
				$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_meds[1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
			}
	$folioSiguiente = $folio["folio_actual"] + 1;
	$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folioSiguiente . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha2 . "','" . $hora . "','','" . $entidad_federativa . "_D','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaAgregada($folio["serie"], $folioSiguiente, $id_medico, $id_derecho);
	$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta2 . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$folioSiguiente = $folio["folio_actual"] + 2;
	$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$ret['txt'] .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
			$sql_concepto = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamentos[0] . "','" . $cantidades[0] . "','" . $unidades[0] . "','" . (int)$dias[0] * (int)$diasTratamientos[0] . "','" . $indicaciones[0] . "','" . $extra . "','','','');";
			$res = ejecutarSQL($sql_concepto);
			if ($extra == '') {
				restaExistenciasMedicamentos($id_medicamentos[0], $cantidades[0]);
				$ret['txt'] .= '- Medicamento ' . $n_meds[0] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
			} else {
				// aquí podriamos restar medicamento pero se iría al - (menos)
				$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_meds[0] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
			}
			$sql_concepto = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamentos[1] . "','" . $cantidades[1] . "','" . $unidades[1] . "','" . (int)$dias[1] * (int)$diasTratamientos[1] . "','" . $indicaciones[1] . "','" . $extra . "','','','');";
			$res = ejecutarSQL($sql_concepto);
			if ($extra == '') {
				restaExistenciasMedicamentos($id_medicamentos[1], $cantidades[1]);
				$ret['txt'] .= '- Medicamento ' . $n_meds[1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
			} else {
				// aquí podriamos restar medicamento pero se iría al - (menos)
				$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_meds[1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
			}
	return ($ret);
}

function agregarReceta1medicamento3meses2medicamentos2014 ($fecha, $hora, $id_medico, $id_servicio, $id_derecho, $entidad_federativa, $clave_uni_med, $diagnostico, $ced_prof, $id_usuario, $tipo_receta, $id_medicamentos, $cantidades, $unidades, $dias, $diasTratamientos, $indicaciones, $extra, $n_meds) {
	$ret = array(
		'id_receta' => '',
		'txt' => '',
		'extra' => ''
	);
	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
	$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha . "','" . $hora . "','','" . $entidad_federativa . "_D','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
	$ret['id_receta'] = $id_receta."_D";
	$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$sql_concepto = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamentos[0] . "','" . $cantidades[0] . "','" . $unidades[0] . "','" . (int)$dias[0] * (int)$diasTratamientos[0] . "','" . $indicaciones[0] . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql_concepto);
	if ($extra == '') {
		restaExistenciasMedicamentos($id_medicamentos[0], $cantidades[0]);
		$ret['txt'] .= '- Medicamento ' . $n_meds[0] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	} else {
		// aquí podriamos restar medicamento pero se iría al - (menos)
		$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_meds[0] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	}
	$sql_concepto = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamentos[1] . "','" . $cantidades[1] . "','" . $unidades[1] . "','" . (int)$dias[1] * (int)$diasTratamientos[1] . "','" . $indicaciones[1] . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql_concepto);
	if ($extra == '') {
		restaExistenciasMedicamentos($id_medicamentos[1], $cantidades[1]);
		$ret['txt'] .= '- Medicamento ' . $n_meds[1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	} else {
		// aquí podriamos restar medicamento pero se iría al - (menos)
		$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_meds[1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	}
	$folioSiguiente = $folio["folio_actual"] + 1;
	$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	return ($ret);
}



function tieneTratamiento2014($id_med, $id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = false;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_esp WHERE id_medicamento='" . $id_med . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    $fecha = date("Y-m-d");
    $fecha_inicio = date("Ymd", strtotime($fecha . " - 90 day"));
    $fecha_fin =  date("Ymd", strtotime($fecha . " + 90 day"));
    if ($totalRows_query == 0) { // SI EL MEDICAMENTO NO ESTA EN LA TABLA DE MEDICAMENTOS SIN RESTRICCIONES, SE VERIFICA SI TIENE O NO TRATAMIENTO
	    $query_query = "SELECT serie, folio, id_medico, id_servicio, id_derecho, recetas.fecha, recetas.hora, status, recetas.id_receta, id_concepto, id_medicamento, dias FROM recetas JOIN recetas_conceptos on recetas.id_receta=recetas_conceptos.id_receta WHERE recetas.fecha >='" . $fecha_inicio . "' AND recetas.fecha <='" . $fecha_fin . "' AND recetas.id_derecho='" . $id_derecho . "' AND recetas_conceptos.id_medicamento='" . $id_med . "' AND recetas.status!='0' ";
//        $query_query = "SELECT * FROM recetas WHERE fecha >='" . $fecha_inicio . "' AND fecha <='" . $fecha_fin . "' AND id_derecho='" . $id_derecho . "' AND status!='0'";
        $query = mysql_query($query_query, $bdissste) or die(mysql_error());
        $row_query = mysql_fetch_assoc($query);
        $totalRows_query = mysql_num_rows($query);
        if ($totalRows_query > 0) {
            do {
                $fecha_factura = substr($row_query["fecha"], 6, 2) . "-" . substr($row_query["fecha"], 4, 2) . "-" . substr($row_query["fecha"], 0, 4);
                $hoy = date("d-m-Y");
                $fecha_termino = strtotime($fecha_factura . " +" . $row_query["dias"] . " day");
                $fecha_termino = date("Ymd", $fecha_termino);
                $hoy = date("Ymd");
                $hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
                $terminoMK = mktime(0, 0, 0, substr($fecha_termino, 4, 2), substr($fecha_termino, 6, 2), substr($fecha_termino, 0, 4));
                if ($terminoMK > $hoyMK)
                    $ret = true;
            }while ($row_query = mysql_fetch_assoc($query));
        }
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}



function statusParaReceta($id_medico, $id_derecho, $id_medicamento, $cantidad, $n_med) {
	$ret = array(
		'status' => '0', // 0=si se puede usar el medicamento para la receta, 1=error tiene tratamiento, 2=error no hay medicamento, 9=error no hay folios
		'txt' => '',
		'extra' => ''
	);
//	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
//	if ($folio["folio_actual"] < $folio["folio_final"]) { // si está el folio actual dentro de los folios disponibles entonces si podríamos agregar la receta
		if (tieneTratamiento2014($id_medicamento, $id_derecho)) {
			$ret['status'] = 1;
			$ret['txt'] = '<span style="color:#ff0000">- Tiene tratamiento vigente del medicamento ' . $n_med . '</span> <a class="boton m azul" href="javascript:void(0);" onclick="javascript:verRecetas2013(\'' . $id_derecho . '\',\'' . $n_med . '\');">ver recetas</a><div id="div_recetas' . $n_med . '"></div><br>';
		}
		if (!hayMedicamento($id_medicamento, $cantidad)) { 
			$ret['status'] = 2;
			$ret['txt'] = '<span style="color:#ff0000">- El medicamento ' . $n_med . ' no tiene existencias y se va a generar como vale</span><br>';
			$ret['extra'] = 'N';
		}
//	} else { // mensaje de error por falta de folios
//		$ret['status'] = 9;
//		$ret['txt'] = '<span style="color:#ff0000">- No se generó Receta para Medicamento ' . $n_med . ' por falta de folios por parte del médico, pida se le asignen mas folios</span><br>';
//	}
	return $ret;	
}

function agregarReceta1medicamento3meses ($id_medico, $id_servicio, $id_derecho, $entidad_federativa, $clave_uni_med, $diagnostico, $ced_prof, $id_usuario, $tipo_receta, $id_medicamento, $cantidad, $unidad, $dias, $diasTratamiento, $indicaciones, $extra, $n_med) {
	$ret = array(
		'id_receta' => '',
		'txt' => '',
		'extra' => ''
	);
	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
	$fecha = date("Y-m-d");
	$hora = date("H:i:s");
	$fecha1 = date('Ymd');
	$fecha2 = date('Ymd', strtotime('+30 days'));
	$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha1 . "','" . $hora . "','','" . $entidad_federativa . "_D','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
	$ret['id_receta'] = $id_receta."_D";
	$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamento . "','" . $cantidad . "','" . $unidad . "','" . (int)$dias * (int)$diasTratamiento . "','" . $indicaciones . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql);
	$folioSiguiente = $folio["folio_actual"] + 1;
	$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folioSiguiente . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha2 . "','" . $hora . "','','" . $entidad_federativa . "_D','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaAgregada($folio["serie"], $folioSiguiente, $id_medico, $id_derecho);
	$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamento . "','" . $cantidad . "','" . $unidad . "','" . (int)$dias * (int)$diasTratamiento . "','" . $indicaciones . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql);
	$folioSiguiente = $folio["folio_actual"] + 2;
	$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$ret['txt'] .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
	if ($extra == '') {
		restaExistenciasMedicamentos($id_medicamento, $cantidad);
		$ret['txt'] .= '- Medicamento ' . $n_med . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	} else {
		// aquí podriamos restar medicamento pero se iría al - (menos)
		$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_med . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	}
	return ($ret);
}

function agregarReceta1medicamento3meses2014 ($fecha, $hora, $id_medico, $id_servicio, $id_derecho, $entidad_federativa, $clave_uni_med, $diagnostico, $ced_prof, $id_usuario, $tipo_receta, $id_medicamento, $cantidad, $unidad, $dias, $diasTratamiento, $indicaciones, $extra, $n_med) {
	$ret = array(
		'id_receta' => '',
		'txt' => '',
		'extra' => ''
	);
//	$folio = array("serie" => '', "folio_actual" => 1); // traemos los valores del folio del dr
	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
	$folioSiguiente = $folio["folio_actual"] + 1;
	$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha . "','" . $hora . "','','" . $entidad_federativa . "_D','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
	$ret['id_receta'] = $id_receta."_D";
	$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$ret['txt'] .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
	$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamento . "','" . $cantidad . "','" . $unidad . "','" . (int)$dias * (int)$diasTratamiento . "','" . $indicaciones . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql);
	if ($extra == '') {
		restaExistenciasMedicamentos($id_medicamento, $cantidad);
		$ret['txt'] .= '- Medicamento ' . $n_med . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	} else {
		// aquí podriamos restar medicamento pero se iría al - (menos)
		$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_med . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	}
	return ($ret);
}




function agregarConcertacion1medicamento ($id_medico, $id_servicio, $id_derecho, $entidad_federativa, $clave_uni_med, $diagnostico, $ced_prof, $id_usuario, $tipo_receta, $id_medicamento, $cantidad, $unidad, $dias, $diasTratamiento, $indicaciones, $extra, $n_med) {
	$ret = array(
		'id_receta' => '',
		'txt' => '',
		'extra' => ''
	);
	$folio = array("serie" => "", "folio_actual" => "");
//	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
//	$folioSiguiente = $folio["folio_actual"] + 1;
	$fecha = date("Ymd");
	$hora = date("H:i:s");
	$sql = "INSERT INTO recetas_concertacion VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha . "','" . $hora . "','','" . $entidad_federativa . "','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaConcertacionAgregada($fecha, $hora, $id_medico, $id_derecho);
	$ret['id_receta'] = $id_receta;
//	$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
//	$res = ejecutarSQL($sql);
	$sql = "UPDATE recetas_concertacion SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$sql = "INSERT INTO recetas_concertacion_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamento . "','" . $cantidad . "','" . $unidad . "','" . (int)$dias * (int)$diasTratamiento . "','" . $indicaciones . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql);
	return ($ret);
}

function agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, $entidad_federativa, $clave_uni_med, $diagnostico, $ced_prof, $id_usuario, $tipo_receta, $id_medicamento, $cantidad, $unidad, $dias, $diasTratamiento, $indicaciones, $extra, $n_med) {
	$ret = array(
		'id_receta' => '',
		'txt' => '',
		'extra' => ''
	);
	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
	$folioSiguiente = $folio["folio_actual"] + 1;
	$fecha = date("Ymd");
	$hora = date("H:i:s");
	$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha . "','" . $hora . "','','" . $entidad_federativa . "','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
	$ret['id_receta'] = $id_receta;
	$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$ret['txt'] .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
	$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamento . "','" . $cantidad . "','" . $unidad . "','" . (int)$dias * (int)$diasTratamiento . "','" . $indicaciones . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql);
	if ($extra == '') {
		restaExistenciasMedicamentos($id_medicamento, $cantidad);
		$ret['txt'] .= '- Medicamento ' . $n_med . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	} else {
		// aquí podriamos restar medicamento pero se iría al - (menos)
		$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_med . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	}
	return ($ret);
}

function agregar2doMedicamento ($id_receta, $id_medicamento, $cantidad, $unidad, $dias, $diasTratamiento, $indicaciones, $extra, $n_med) {
	$ret = '';
	$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamento . "','" . $cantidad . "','" . $unidad . "','" . (int)$dias * (int)$diasTratamiento . "','" . $indicaciones . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql);
	if ($extra == '') {
		restaExistenciasMedicamentos($id_medicamento, $cantidad);
		$ret .= '- Medicamento ' . $n_med . ' agregado correctamente<br>';
	} else {
		// aquí podriamos restar medicamento pero se iría al - (menos)
		$ret .= '- Medicamento SIN EXISTENCIAS ' . $n_med . '  agregado correctamente<br>';
	}
	return $ret;
}

if ((isset($_GET["cantidades"])) && (isset($_GET["unidades"])) && (isset($_GET["dias"])) && (isset($_GET["medicamentos"])) && (isset($_GET["indicaciones"])) && (isset($_GET["id_derecho"])) && (isset($_GET["serie"])) && (isset($_GET["folio"])) && (isset($_GET["grupos"]))) {
	$cant = $_GET["cantidades"];
	$unid= $_GET["unidades"];
	$di = $_GET["dias"];
	$diT = $_GET["diasTratamiento"];
	$medi = $_GET["medicamentos"];
	$indi = $_GET["indicaciones"];
	$nuMed = $_GET["numerosMed"];
	$tipo_receta = $_GET["tipo_receta"];
//	$folio = $_GET["folio"];
	$grps = $_GET["grupos"];
	$diagnostico = $_GET["diagnostico"];
	$id_derecho = $_GET["id_derecho"];
	$serie = $_GET["serie"];
	$id_medico = $_SESSION["idDr"];

	$medicamentos = explode(",",$medi);
	$tmedicamentos = count($medicamentos);
	$ret = '';
	$id_receta = '';
	$id_receta2 = 0;
	$medico = getMedicoXid($id_medico);
	$id_servicio = regresarIdServicio($id_medico);

	$fecha1 = date('Ymd');
	$fecha2 = date('Ymd', strtotime('+30 days'));
	$hora = date("H:i:s");

	switch ($tmedicamentos) {
		case '1': // un solo medicamento se recibió
			$status = statusParaReceta($id_medico, $id_derecho, $medi, $cant, $nuMed);
			$grupos = explode(",",$grps);
			$numerosMed = explode(",",$nuMed);
			switch ($status['status']) {
				case 0 : // todo bien para que se genere la receta
					$agregar = agregarReceta1medicamento3meses2014 ($fecha1, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medi, $cant, $unid, $di, $diT, $indi, $status['extra'], $nuMed);
					$ret = $agregar['txt'];
					$id_receta = $agregar['id_receta'];
					$agregar = agregarReceta1medicamento3meses2014 ($fecha2, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medi, $cant, $unid, $di, $diT, $indi, $status['extra'], $nuMed);
					$ret .= $agregar['txt'];
					$up = actualizaFechaTratamiento($di, $diT);
					break;
				case 2 : // no se genera receta por falta de existencias del medicamento 1
//					$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medi, $cant, $unid, $di, $diT, $indi, $status['extra'], $nuMed);
					$agregarConc = agregarConcertacion1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medi, $cant, $unid, $di, $diT, $indi, $status['extra'], $nuMed);
					$ret = 'Medicamento 1 sin existencias. NO se generó receta';
					$id_receta = '';
					$up = actualizaFechaTratamiento($di, $diT);
					break;
				case 1 : // tiene tratamiento vigiente
				case 9 : // no tiene folios el dr
					$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status['txt'];
					break;
			}
			break;
		case '2': // 2 medicamentos se recibieron
			$cantidades = explode(",",$cant);
			$unidades = explode(",",$unid);
			$dias = explode(",",$di);
			$diasTratamiento = explode(",",$diT);
			$indicaciones = explode(",",$indi);
			$grupos = explode(",",$grps);
			$numerosMed = explode(",",$nuMed);
			if (($grupos[0] == "2") || ($grupos[1] == "2") || ($grupos[0] == "3") || ($grupos[1] == "3") || ($grupos[0] == "6") || ($grupos[1] == "6")) { // si alguno de los medicamentos es de grupo 2, 3 o 6 se deben generar en recetas separadas
			// receta del medicamento 1
				$status = statusParaReceta($id_medico, $id_derecho, $medicamentos[0], $cantidades[0], $numerosMed[0]);
				switch ($status['status']) {
					case 0 : // todo bien para que se genere la receta
							$agregar = agregarReceta1medicamento3meses2014 ($fecha1, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
							$ret = $agregar['txt'];
							$id_receta = $agregar['id_receta'];
							$agregar = agregarReceta1medicamento3meses2014 ($fecha2, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
							$ret .= $agregar['txt'];
							$up = actualizaFechaTratamiento($dias[0], $diasTratamiento[0]);
						break;
					case 2 : // no se genera receta por falta de existencias del medicamento 1
//							$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
							$agregarConc = agregarConcertacion1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
							$ret = 'Medicamento 1 sin existencias. NO se generó receta';
							$id_receta = '';
							$up = actualizaFechaTratamiento($dias[0], $diasTratamiento[0]);
						break;
					case 1 : // tiene tratamiento vigiente
					case 9 : // no tiene folios el dr
						$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status['txt'];
						break;
				}
			// receta del medicamento 2
				$status = statusParaReceta($id_medico, $id_derecho, $medicamentos[1], $cantidades[1], $numerosMed[1]);
				switch ($status['status']) {
					case 0 : // todo bien para que se genere la receta
							$agregar = agregarReceta1medicamento3meses2014 ($fecha1, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
							$ret .= $agregar['txt'];
							$id_receta2 = $agregar['id_receta'];
							$agregar = agregarReceta1medicamento3meses2014 ($fecha2, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
							$ret .= $agregar['txt'];
							$up = actualizaFechaTratamiento($dias[1], $diasTratamiento[1]);
						break;
					case 2 : // no se genera receta por falta de existencias del medicamento 2
//							$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
							$agregarConc = agregarConcertacion1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
							$ret .= 'Medicamento 2 sin existencias. NO se generó receta';
							$id_receta2 = '';
							$up = actualizaFechaTratamiento($dias[1], $diasTratamiento[1]);
						break;
					case 1 : // tiene tratamiento vigiente
					case 9 : // no tiene folios el dr
						$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status['txt'];
						break;
				}
			} else { // si son grupo 4 o 1 (que se supone que no tienen del grupo 1) por default hago lo del grupo 4: medicamentos en la misma receta si son con existencia o recetas separadas si uno tiene existencias y otro no
				$status = statusParaReceta($id_medico, $id_derecho, $medicamentos[0], $cantidades[0], $numerosMed[0]);
				switch ($status['status']) {
					case 0 : // todo bien para que se genere la receta
						$status2 = statusParaReceta($id_medico, $id_derecho, $medicamentos[1], $cantidades[1], $numerosMed[1]);
						switch ($status2['status']) {
							case 0: // los 2 tienen existencia
//								$agregar = agregarReceta1medicamento3meses ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
//								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
//								$ret = $agregar['txt'];
								$agregar2do = agregarReceta1medicamento3meses2medicamentos2014 ($fecha1, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos, $cantidades, $unidades, $dias, $diasTratamiento, $indicaciones, $status['extra'], $numerosMed);
								$id_receta = $agregar2do['id_receta'];
//								$agregar2do = agregarReceta1medicamento3meses ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
//								$agregar2do = agregar2doMedicamento($id_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status2['extra'], $numerosMed[1]);
								$up = actualizaFechaTratamiento($dias[0], $diasTratamiento[0]);
								$up = actualizaFechaTratamiento($dias[1], $diasTratamiento[1]);
								$ret .= $agregar2do['txt'];
								$agregar2do = agregarReceta1medicamento3meses2medicamentos2014 ($fecha2, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos, $cantidades, $unidades, $dias, $diasTratamiento, $indicaciones, $status['extra'], $numerosMed);
								$ret .= $agregar2do['txt'];
								break;
							case 2: // el medicamento 1 tiene existencia pero el 2 no, no se genera receta para el 2
								$agregar = agregarReceta1medicamento3meses2014 ($fecha1, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
//								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$ret = $agregar['txt'];
								$id_receta = $agregar['id_receta'];
								$agregar = agregarReceta1medicamento3meses2014 ($fecha2, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$ret .= $agregar['txt'];
//								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
								$agregarConc = agregarConcertacion1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
								$up = actualizaFechaTratamiento($dias[0], $diasTratamiento[0]);
								$up = actualizaFechaTratamiento($dias[1], $diasTratamiento[1]);
								$ret .= 'Medicamento 2 sin existencias. NO se generó receta';
								$id_receta2 = '';
								break;
							case 1 : // tiene tratamiento vigiente el medicamento 2
							case 9 : // no tiene folios el dr
								$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status2['txt'];
								break;
						}
						break;
					case 2 : // EL medicamento 1 no tiene existencia
						$status2 = statusParaReceta($id_medico, $id_derecho, $medicamentos[1], $cantidades[1], $numerosMed[1]);
						switch ($status2['status']) {
							case 0: // el medicamento 1 no tiene existencia pero el medicamento 2 si, se genera receta solo para el 2
//								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$agregarConc = agregarConcertacion1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$ret = 'Medicamento 1 sin existencias. NO se generó receta<br>';
								$id_receta = '';
								$agregar = agregarReceta1medicamento3meses2014 ($fecha1, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
//								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
								$up = actualizaFechaTratamiento($dias[0], $diasTratamiento[0]);
								$up = actualizaFechaTratamiento($dias[1], $diasTratamiento[1]);
								$ret .= $agregar['txt'];
								$id_receta = $agregar['id_receta'];
								$agregar = agregarReceta1medicamento3meses2014 ($fecha2, $hora, $id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
								$ret .= $agregar['txt'];
								$id_receta2 = '';
								break;
							case 2: // el medicamento 1 no tiene existencia ni el medicamento 2, se generan en una sola receta
//								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$agregarConc = agregarConcertacion1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$agregarConc = agregarConcertacion1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
								$ret = 'Medicamento 1 y 2 sin existencias. NO se generó receta';
								$id_receta = '';
//								$agregar2do = agregar2doMedicamento($id_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status2['extra'], $numerosMed[1]);
								$up = actualizaFechaTratamiento($dias[0], $diasTratamiento[0]);
								$up = actualizaFechaTratamiento($dias[1], $diasTratamiento[1]);
								$ret .= '';
								break;
							case 1 : // tiene tratamiento vigiente el medicamento 2
							case 9 : // no tiene folios el dr
								$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status2['txt'];
								break;
						}
						break;
					case 1 : // tiene tratamiento vigiente
					case 9 : // no tiene folios el dr
						$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status['txt'];
						break;
				}
			}
			break;
	}

	print($ret . "|" . $id_receta . "|" . $id_receta2);

} else {
	echo '<span style="color:#ff0000">No se pudo agregar la Receta, pongase en contacto con el administrador del sistema</span>';
}
?>
