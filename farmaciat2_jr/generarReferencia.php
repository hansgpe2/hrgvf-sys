<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="lib/misEstilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php
session_start(); 
include "lib/misFunciones.php";
if(isset($_GET['id_horario'])){
$horario=$_GET['id_horario'];
$cita=getCita($horario,date("Ymd"));
}
else
{
	$cita=getCitaExt($_GET['idCita']);
}
$derecho=getDatosDerecho($cita['id_derecho']);
$idserv=$_SESSION['idServ'];
$serv=getServicioXid($idserv);
$medi=getMedicoXid($_SESSION['idDr']);
$diagRef=ObtenerDiagnosticoReferencia($cita['id_derecho'],$idserv);
?>
<div align="center">
<form action="formatoContrarreferencia.php" name="contraReferencia" method="post" target="_blank">
<table width="100%" height="100%">
<tr><td colspan="2" class="tituloVentana">FORMATO DE CONTRARREFERENCIA</td></tr>
<tr><td class="textosParaInputs">CEDULA</td><td><?php echo $derecho['cedula']."/".$derecho['cedula_tipo']; ?><input type="hidden" name="idDerecho" id="idDerecho" value="<?php echo $cita['id_derecho'] ?>" /></td></tr>
<tr><td class="textosParaInputs">NOMBRE</td><td><?php echo ponerAcentos($derecho['ap_p']." ".$derecho['ap_m']." ".$derecho['nombres']); ?></td></tr>
<tr><td class="textosParaInputs">SERVICIO</td><td><?php echo $serv ?><input type="hidden" name="servicio" id="servicio" value="<?php echo $idserv; ?>"  /><input type="hidden" name="fecha" id="fecha" value="<?php echo date("Ymd"); ?>"  /></td></tr>
<tr><td class="textosParaInputs">DIAGNOSTICO DE REFERENCIA</td><td><textarea  id="diagnosticoRef" cols="50" rows="10" name="diagnosticoRef" wrap="hard" maxlength="1000"  value="<?php echo $diagRef['campo1']; ?>" /></td></tr>
<?php if($derecho['unidad_medica']==0){ ?>
<tr><td class="textosParaInputs">Unidad Medica</td><td><select name="Unidad" id="Unidad" onfocus="ObtenerUnidadesMedicas('<?php echo $derecho['estado']; ?>');"></select></td></tr>
<?php } ?>    
<tr><td colspan="2" align="center"><input type="button" name="regresar" id="regresar" value="regresar" onclick="regresarARecetas()" class="botones" />&nbsp;  <input type="submit" value="Guardar Referencia" class="botones" /></td></tr>
</table></form></div>
</body>
</html>