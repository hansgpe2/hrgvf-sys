<?php
include_once('lib/misFunciones.php');
session_start ();

if (isset($_REQUEST["id_receta"])) {
	$datosReceta = getRecetaXid($_REQUEST["id_receta"]);
	$servicio = getServicioXid($datosReceta["id_servicio"]);
	$derecho =  getDatosDerecho($datosReceta["id_derecho"]);
	$medico = getMedicoXid($datosReceta["id_medico"]);
	$conceptos =  getDatosMedicamentoEnReceta($datosReceta["id_receta"]);
	$tablaMed = array();
	$tablaMedF = array();
	$tablaMedCant = array();
	$tablaVale = array();
	$tablaMed[0] = '';
	$tablaMedF[0] = '';
	$tablaMedCant[0] = '';
	$tablaVale[0] = '';
	$tablaMed[1] = '';
	$tablaMedF[1] = '';
	$tablaMedCant[1] = '';
	$tablaVale[1] = '';
	if ($conceptos != "") {
		$medicamento = getDatosMedicamentos($conceptos[0]["id_medicamento"]);
		if ($conceptos[0]["extra1"] == "N") { // es medicamento sin existencia
			$tablaVale[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
							<tr>
								<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
							</tr>
							<tr>
								<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[0]["indicaciones"])) . '&nbsp;</td>
							</tr>
					</table>';
			if ($medicamento["id_medicamento"] == 0) $medicamento["id_medicamento"] = "";
			$tablaValeF[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
							<tr>
								<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
							</tr>
							<tr>
								<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[0]["indicaciones"])) . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top">
									<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
											<tr>
												<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">DIAS DE TRATAMIENTO.</td>
											</tr>
											<tr>
												<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[0]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[0]["dias"] . '&nbsp;<br><br><span class="contenido8Bold" style="text-decoration: underline;">TELEFONO PACIENTE</span></td>
											</tr>
											<tr>
												<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top"><strong></strong></td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
											</tr>
									</table>
								</td>
							</tr>
					</table>';
		} else { 
			if ($medicamento["id_medicamento"] != 0) {
				$tablaMed[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
								</tr>
								<tr>
									<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[0]["indicaciones"])) . '&nbsp;</td>
								</tr>
						</table>';
				if ($medicamento["id_medicamento"] == 0) $medicamento["id_medicamento"] = "";
				$tablaMedF[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
								</tr>
								<tr>
									<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[0]["indicaciones"])) . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top">
										<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
												<tr>
													<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">DIAS TRATAMIENTO.</td>
												</tr>
												<tr>
													<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[0]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[0]["dias"] . '&nbsp;<br><br><span class="contenido8Bold" style="text-decoration: underline;">TELEFONO PACIENTE</span></td>
												</tr>
												<tr>
													<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top"><strong></strong></td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
												</tr>
										</table>
									</td>
								</tr>
						</table>';
				$tablaMedCant[0] = '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido6" valign="top">CLAVE</td><td align="center" class="contenido6BordeIzq" valign="top">CANT.</td><td align="center" class="contenido6BordeIzq" valign="top">DIAS TRATAMIENTO.</td>
								</tr>
								<tr>
									<td align="center" class="contenido8bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[0]["cantidad"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[0]["dias"] . '&nbsp;</td>
								</tr>
						</table>';
			}
		}
		$medicamento = getDatosMedicamentos($conceptos[1]["id_medicamento"]);
		if ($conceptos[1]["extra1"] == "N") { // es medicamento sin existencia
			$tablaVale[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
							<tr>
								<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
							</tr>
							<tr>
								<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[1]["indicaciones"])) . '&nbsp;</td>
							</tr>
						</table>';
			if ($medicamento["id_medicamento"] == 0) $medicamento["id_medicamento"] = "";
			$tablaValeF[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
							<tr>
								<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
							</tr>
							<tr>
								<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[1]["indicaciones"])) . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top">
									<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
											<tr>
												<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">DIAS TRATAMIENTO.</td>
											</tr>
											<tr>
												<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[1]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["dias"] . '&nbsp;<br><br><span class="contenido8Bold" style="text-decoration: underline;">NOMBRE PACIENTE</span></td>
											</tr>
											<tr>
												<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["cantidad"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
											</tr>
									</table>
								</td>
							</tr>
					</table>';
		} else { 
			if ($medicamento["id_medicamento"] != 0) {
				$tablaMed[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
								</tr>
								<tr>
									<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[1]["indicaciones"])) . '&nbsp;</td>
								</tr>
							</table>';
				if ($medicamento["id_medicamento"] == 0) $medicamento["id_medicamento"] = "";
				$tablaMedF[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
								</tr>
								<tr>
									<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[1]["indicaciones"])) . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top">
										<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
												<tr>
													<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">DIAS TRATAMIENTO.</td>
												</tr>
												<tr>
													<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[1]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["dias"] . '&nbsp;<br><br><span class="contenido8Bold" style="text-decoration: underline;">NOMBRE PACIENTE</span></td>
												</tr>
												<tr>
													<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["cantidad"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
												</tr>
										</table>
									</td>
								</tr>
						</table>';
				$tablaMedCant[1] = '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido6" valign="top">CLAVE</td><td align="center" class="contenido6BordeIzq" valign="top">CANT.</td><td align="center" class="contenido6BordeIzq" valign="top">DIAS TRATAMIENTO.</td>
								</tr>
								<tr>
									<td align="center" class="contenido8bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[1]["cantidad"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[1]["dias"] . '&nbsp;</td>
								</tr>
						</table>';
			}
		}
	}
//	$medicamento = getDatosMedicamentos();
	if ($datosReceta != "") {
		$titulo = $datosReceta["serie"] . ponerCeros($datosReceta["folio"],7);
		$dia = substr($datosReceta["fecha"],6,2);
		$mes = substr($datosReceta["fecha"],4,2);
		$ano = substr($datosReceta["fecha"],0,4);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Receta: <?php echo $titulo ?></title>
<style type="text/css">
	@import url("lib/impresion.css") print;
	@import url("lib/reportes.css") screen;
</style>
</head>

<body onload="javascript: window.print();">
<?php
	if (($tablaMed[0] != '') || ($tablaMed[1] != '')) {
?>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">&nbsp;P A C I E N T E</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td width="222" align="center" height="80"><img src="diseno/logoEncabezado.jpg" width="160" /></td>
                	<td align="center"><span class="tituloIssste">DIRECCION MEDICA</span><br /><span class="tituloEncabezado">RECETA MEDICA</span></td>
                	<td width="200" align="left"><img src="barcode/barcode.php?code=<?php echo $datosReceta["codigo_barras"] ?>&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado"><?php echo $titulo ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6">ENTIDAD FEDERATIVA<br /><span class="contenido8"><?php echo $datosReceta["entidad_federativa"] ?></span></td>
                	<td width="100" align="center" class="contenido6BordeIzq">
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center"><?php echo $dia ?></td><td align="center"><?php echo $mes ?></td><td align="center"><?php echo $ano ?></td>
                        	</tr>
                        </table>
                    </td>
                	<td align="left" width="130" class="contenido6BordeIzq">CLAVE DE LA UNIDAD MEDICA<br /><span class="contenido8"><?php echo $datosReceta["clave_unidad_medica"] ?></span></td>
                	<td align="left" class="contenido6BordeIzq">SERVICIO<br /><span class="contenido8"><?php echo $servicio ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="left" class="contenido6">NOMBRE DEL PACIENTE<br /><span class="contenido8"><?php echo ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]); ?></span></td>
                    <td align="left" class="contenido6BordeIzq"><span class="contenido8bold">DIAGNOSTICO</span><br /><span class="contenido8"><?php echo ponerAcentos($datosReceta["numero_vale"]); ?></span></td>
                	<td align="left" width="100" class="contenido6BordeIzq">CEDULA<br /><span class="contenido8"><?php echo $derecho["cedula"] . "/" . $derecho["cedula_tipo"]; ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMed[0] ?></td>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMed[1] ?></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8"><?php echo $datosReceta["dgp"] ?></span></td>
                    <td align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br /><span class="contenido8"><?php echo $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]); ?></span></td>
                	<td width="70" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMedCant[0] ?></td>
                	<td align="center" width="50%" class="contenido6BordeIzq" valign="top"><?php echo $tablaMedCant[1] ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br /><p class="separador">&nbsp;</p>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">&nbsp;F A R M A C I A</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="230" align="left" class="contenido6">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center"><?php echo $dia ?></td><td align="center"><?php echo $mes ?></td><td align="center"><?php echo $ano ?></td>
                        	</tr>
                        </table>
                        <br />
                        NOMBRE DEL PACIENTE<br /><span class="contenido8bold"><?php echo ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]); ?></span>
                        <br />
                        CEDULA<br /><span class="contenido8"><?php echo $derecho["cedula"] . "/" . $derecho["cedula_tipo"]; ?></span>
                    </td>
                	<td align="center">
                    	<table class="codigoMedico">
                        	<tr>
                            	<td align="center">CODIGO<br />BARRAS<br />MEDICO</td>
                            </tr>
                        </table>
                    </td>
                	<td width="200" align="left"><img src="barcode/barcode.php?code=<?php echo $datosReceta["codigo_barras"] ?>&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado"><?php echo $titulo ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8"><?php echo $datosReceta["dgp"] ?></span></td>
                    <td align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br /><span class="contenido8"><?php echo $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]); ?></span></td>
                    <td align="left" class="contenido6BordeIzq"><span class="contenido8bold">DIAGNOSTICO</span><br /><span class="contenido8"><?php echo ponerAcentos($datosReceta["numero_vale"]); ?></span></td>
                	<td width="70" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMedF[0] ?></td>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMedF[1] ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php
	}
	if (($tablaVale[0] != '') || ($tablaVale[1] != '')) {
?>
<br /><br /><br />
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">&nbsp;P A C I E N T E</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td width="222" align="center" height="80"><img src="diseno/logoEncabezado.jpg" width="160" /></td>
                	<td align="center"><span class="tituloIssste">DIRECCION MEDICA</span><br /><span class="tituloEncabezado">RECETA MEDICA</span></td>
                	<td width="200" align="left"><img src="barcode/barcode.php?code=<?php echo $datosReceta["codigo_barras"] ?>&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado"><?php echo $titulo ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6">ENTIDAD FEDERATIVA<br /><span class="contenido8"><?php echo $datosReceta["entidad_federativa"] ?></span></td>
                	<td width="100" align="center" class="contenido6BordeIzq">
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center"><?php echo $dia ?></td><td align="center"><?php echo $mes ?></td><td align="center"><?php echo $ano ?></td>
                        	</tr>
                        </table>
                    </td>
                	<td align="left" width="130" class="contenido6BordeIzq">CLAVE DE LA UNIDAD MEDICA<br /><span class="contenido8"><?php echo $datosReceta["clave_unidad_medica"] ?></span></td>
                	<td align="left" class="contenido6BordeIzq">SERVICIO<br /><span class="contenido8"><?php echo $servicio ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="left" class="contenido6">NOMBRE DEL PACIENTE<br /><span class="contenido8"><?php echo ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]); ?></span></td>
                    <td align="left" class="contenido6BordeIzq"><span class="contenido8bold">DIAGNOSTICO</span><br /><span class="contenido8"><?php echo ponerAcentos($datosReceta["numero_vale"]); ?></span></td>
                	<td align="left" width="100" class="contenido6BordeIzq">CEDULA<br /><span class="contenido8"><?php echo $derecho["cedula"] . "/" . $derecho["cedula_tipo"]; ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaVale[0] ?></td>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaVale[1] ?></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8"><?php echo $datosReceta["dgp"] ?></span></td>
                    <td width="965" align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br />
                  <span class="contenido8"><?php echo $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]); ?></span></td>
                	<td width="311" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMedCant[0] ?></td>
                	<td align="center" width="50%" class="contenido6BordeIzq" valign="top"><?php echo $tablaMedCant[1] ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br /><p class="separador">&nbsp;</p>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">&nbsp;F A R M A C I A</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="230" align="left" class="contenido6">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center"><?php echo $dia ?></td><td align="center"><?php echo $mes ?></td><td align="center"><?php echo $ano ?></td>
                        	</tr>
                        </table>
                        <br />
                        NOMBRE DEL PACIENTE<br /><span class="contenido8bold"><?php echo ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]); ?></span>
                        <br />
                        CEDULA<br /><span class="contenido8"><?php echo $derecho["cedula"] . "/" . $derecho["cedula_tipo"]; ?></span>
                    </td>
                	<td align="center">
                    	<table class="codigoMedico">
                        	<tr>
                            	<td align="center">CODIGO<br />BARRAS<br />MEDICO</td>
                            </tr>
                        </table>
                    </td>
                	<td width="200" align="left"><img src="barcode/barcode.php?code=<?php echo $datosReceta["codigo_barras"] ?>&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado"><?php echo $titulo ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8"><?php echo $datosReceta["dgp"] ?></span></td>
                    <td width="688" align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br />
                  <span class="contenido8"><?php echo $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]); ?></span></td>
                    <td width="271" align="left" class="contenido6BordeIzq"><span class="contenido8bold">DIAGNOSTICO</span><br />
                  <span class="contenido8"><?php echo ponerAcentos($datosReceta["numero_vale"]); ?></span></td>
                	<td width="307" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
            	  <td align="center" class="contenido6" valign="top"><?php echo $tablaValeF[0] ?></td>
            	  <td align="center" class="contenido6" valign="top"><?php echo $tablaValeF[1] ?></td>
          	  </tr>
            </table>
        </td>
    </tr>
</table>

<?php		
	}
?>

</body>
</html>
<?php		
	} else {
		print("No existe la receta");
	}
} else {
	print("Error en variable id receta");
}

/*
$datosRecetas = getRecetaXCodigoBarras($_REQUEST["cod_bar"]);
if ($datosRecetas == "") {
	$out = 'No existe receta con el c&oacute;digo de barras ingresado';
} else {
	$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="780">';
	$out .= '<tr><td colspan="5" class="tituloVentana">Receta</td></tr>';
	$out .= '<tr><th>Fecha y Hora</th><th>Folio</th><th>M&eacute;dico</th><th>Servicio</th><th>Derechohabiente</th></tr>';
	$datosMedico = getMedicoXid($datosRecetas['id_medico']);
	$datosServicio = getServicioXid($datosRecetas['id_servicio']);
	$datosDerecho = getDatosDerecho($datosRecetas['id_derecho']);
	$out .= '<tr class="botones_menu"><td>' . $datosRecetas['fecha'] . ' - ' . $datosRecetas['hora'] . '</td><td>' . $datosRecetas['serie'] . '' . $datosRecetas['folio'] . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td>' . ponerAcentos($datosServicio) . '</td><td>' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td></tr>';
	$out .= '<tr><td colspan="5">&nbsp;</td></tr>';
	$conceptos =  getDatosMedicamentoEnReceta($datosRecetas["id_receta"]);
	if ($datosRecetas["status"] == "2") $boton = ' disabled="disabled"'; else $boton = '';
	if ($conceptos != "") {
		$out .= '<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
		$tConceptos = count($conceptos);
		for ($i=0; $i < $tConceptos; $i++) {
//			$Extistencia_medicamento = getExistenciasMedicamentos($conceptos[$i]["id_medicamento"]);
			$medicamento = getDatosMedicamentos($conceptos[$i]["id_medicamento"]);
			if ($conceptos[$i]['extra1'] == 's') $readonly = ' disabled="disabled"'; else $readonly = '';
			$out .= '<td width="50%" align="center">
						<table width="350" border="1" cellspacing="0" cellpadding="3">
							<tr><td align="left">Cantidad: </td><td>' . $conceptos[$i]["cantidad"] . '</td></tr>
							<tr><td align="left">Tratamiento: </td><td>' . $conceptos[$i]["dias"] . ' D&iacute;as</td></tr>
							<tr><td align="left">Medicamento: </td><td class="botones_menu">' . $medicamento["descripcion"] . '</td></tr>
							<tr><td align="left">Indicaciones: </td><td>' . $conceptos[$i]["indicaciones"] . '</td></tr>
							<tr><td align="center" colspan="2">Se surti&oacute; el medicamento: <input id="surtio_' . $i . '" name="surtio_' . $i . '" type="checkbox" value="' . $conceptos[$i]["id_medicamento"] . '" checked="checked"' . $readonly . ' /></td></tr>
						</table>
					</td>';
		}
		$out .= '</tr></table>';
		$out .= '<tr><td colspan="5"><br><input name="enviar" type="button" value="Guardar" onclick="javascript: validarSurtirReceta(' . $datosRecetas["id_receta"] . ')"' . $boton . ' /></td></tr>';
	}
	$out .= '</table><div id="enviando"></div></center>';
}
print($out);
*/
?>

