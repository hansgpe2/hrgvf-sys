<?php
include_once('lib/misFunciones.php');
session_start ();

if (isset($_REQUEST["id_receta"])) {
	$datosReceta = getRecetaXid($_REQUEST["id_receta"]);
	$servicio = getServicioXid($datosReceta["id_servicio"]);
	$derecho =  getDatosDerecho($datosReceta["id_derecho"]);
	$medico = getMedicoXid($datosReceta["id_medico"]);
	$conceptos =  getDatosMedicamentoEnReceta($datosReceta["id_receta"]);
	$tablaMed = array();
	$tablaMedF = array();
	$tablaMedCant = array();
	if ($conceptos != "") {
		$medicamento = getDatosMedicamentos($conceptos[0]["id_medicamento"]);
		$tablaMed[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
						<tr>
							<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
						</tr>
						<tr>
							<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
						</tr>
						<tr>
							<td align="left" class="contenido8" valign="top" height="50">' . strtoupper($conceptos[0]["indicaciones"]) . '&nbsp;</td>
						</tr>
				</table>';
		if ($medicamento["id_medicamento"] == 0) $medicamento["id_medicamento"] = "";
		$tablaMedF[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
						<tr>
							<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
						</tr>
						<tr>
							<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
						</tr>
						<tr>
							<td align="left" class="contenido8" valign="top" height="50">' . strtoupper($conceptos[0]["indicaciones"]) . '&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class="contenido8" valign="top">
								<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
										<tr>
											<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">T.D.</td>
										</tr>
										<tr>
											<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[0]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[0]["dias"] . '&nbsp;</td>
										</tr>
										<tr>
											<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[0]["cantidad"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
										</tr>
								</table>
							</td>
						</tr>
				</table>';
		$tablaMedCant[0] = '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
						<tr>
							<td align="center" class="contenido6" valign="top">CLAVE</td><td align="center" class="contenido6BordeIzq" valign="top">CANT.</td><td align="center" class="contenido6BordeIzq" valign="top">T.D.</td>
						</tr>
						<tr>
							<td align="center" class="contenido8bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[0]["cantidad"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[0]["dias"] . '&nbsp;</td>
						</tr>
				</table>';
		$medicamento = getDatosMedicamentos($conceptos[1]["id_medicamento"]);
		$tablaMed[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
						<tr>
							<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
						</tr>
						<tr>
							<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
						</tr>
						<tr>
							<td align="left" class="contenido8" valign="top" height="50">' . strtoupper($conceptos[1]["indicaciones"]) . '&nbsp;</td>
						</tr>
					</table>';
		if ($medicamento["id_medicamento"] == 0) $medicamento["id_medicamento"] = "";
		$tablaMedF[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
						<tr>
							<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
						</tr>
						<tr>
							<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
						</tr>
						<tr>
							<td align="left" class="contenido8" valign="top" height="50">' . strtoupper($conceptos[1]["indicaciones"]) . '&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class="contenido8" valign="top">
								<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
										<tr>
											<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">T.D.</td>
										</tr>
										<tr>
											<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[1]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["dias"] . '&nbsp;</td>
										</tr>
										<tr>
											<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["cantidad"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
										</tr>
								</table>
							</td>
						</tr>
				</table>';
		$tablaMedCant[1] = '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
						<tr>
							<td align="center" class="contenido6" valign="top">CLAVE</td><td align="center" class="contenido6BordeIzq" valign="top">CANT.</td><td align="center" class="contenido6BordeIzq" valign="top">T.D.</td>
						</tr>
						<tr>
							<td align="center" class="contenido8bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[1]["cantidad"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[1]["dias"] . '&nbsp;</td>
						</tr>
				</table>';
	}
//	$medicamento = getDatosMedicamentos();
	if ($datosReceta != "") {
		$titulo = $datosReceta["serie"] . ponerCeros($datosReceta["folio"],7);
		$dia = substr($datosReceta["fecha"],6,2);
		$mes = substr($datosReceta["fecha"],4,2);
		$ano = substr($datosReceta["fecha"],0,4);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Receta: <?php echo $titulo ?></title>
<style type="text/css">
	@import url("lib/impresion.css") print;
	@import url("lib/reportes.css") screen;
</style>
</head>

<body onload="javascript: window.print();">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">C O P I A &nbsp;&nbsp;&nbsp;P A R A &nbsp;&nbsp;&nbsp;E L &nbsp;&nbsp;&nbsp;P A C I E N T E</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td width="222" align="center" height="80"><img src="diseno/logoEncabezado.jpg" width="160" /></td>
                	<td align="center"><span class="tituloIssste">DIRECCION MEDICA</span><br /><span class="tituloEncabezado">RECETA MEDICA</span></td>
                	<td width="200" align="left"><img src="barcode/barcode.php?code=<?php echo $datosReceta["codigo_barras"] ?>&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado"><?php echo $titulo ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6">ENTIDAD FEDERATIVA<br /><span class="contenido8"><?php echo $datosReceta["entidad_federativa"] ?></span></td>
                	<td width="100" align="center" class="contenido6BordeIzq">
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center"><?php echo $dia ?></td><td align="center"><?php echo $mes ?></td><td align="center"><?php echo $ano ?></td>
                        	</tr>
                        </table>
                    </td>
                	<td align="left" width="130" class="contenido6BordeIzq">CLAVE DE LA UNIDAD MEDICA<br /><span class="contenido8"><?php echo $datosReceta["clave_unidad_medica"] ?></span></td>
                	<td align="left" class="contenido6BordeIzq">SERVICIO<br /><span class="contenido8"><?php echo $servicio ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="left" class="contenido6">NOMBRE DEL PACIENTE<br /><span class="contenido8"><?php echo ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]); ?></span></td>
                	<td align="left" width="100" class="contenido6">CEDULA<br /><span class="contenido8"><?php echo $derecho["cedula"] . "/" . $derecho["cedula_tipo"]; ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMed[0] ?></td>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMed[1] ?></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8"><?php echo $datosReceta["dgp"] ?></span></td>
                    <td align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br /><span class="contenido8"><?php echo $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]); ?></span></td>
                	<td width="70" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMedCant[0] ?></td>
                	<td align="center" width="50%" class="contenido6BordeIzq" valign="top"><?php echo $tablaMedCant[1] ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br /><br /><br /><p class="separador">&nbsp;</p><br /><br />
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">C O P I A &nbsp;&nbsp;&nbsp;P A R A &nbsp;&nbsp;F A R M A C I A</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="230" align="left" class="contenido6">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center"><?php echo $dia ?></td><td align="center"><?php echo $mes ?></td><td align="center"><?php echo $ano ?></td>
                        	</tr>
                        </table>
                        <br />
                        NOMBRE DEL PACIENTE<br /><span class="contenido8bold"><?php echo ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]); ?></span>
                        <br />
                        CEDULA<br /><span class="contenido8"><?php echo $derecho["cedula"] . "/" . $derecho["cedula_tipo"]; ?></span>
                    </td>
                	<td align="center">
                    	<table class="codigoMedico">
                        	<tr>
                            	<td align="center">CODIGO<br />BARRAS<br />MEDICO</td>
                            </tr>
                        </table>
                    </td>
                	<td width="200" align="left"><img src="barcode/barcode.php?code=<?php echo $datosReceta["codigo_barras"] ?>&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado"><?php echo $titulo ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                    <td align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br /><span class="contenido8"><?php echo $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]); ?></span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMedF[0] ?></td>
                	<td align="center" width="50%" class="contenido6" valign="top"><?php echo $tablaMedF[1] ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>
<?php		
	} else {
		print("No existe la receta");
	}
} else {
	print("Error en variable id receta");
}
?>

