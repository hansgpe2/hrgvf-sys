<!Doctype html>
<html>
    <head>
        <link href="lib/misEstilos.css" rel="stylesheet" type="text/css">
        <script src="lib/arreglos.js"></script>
    <head>
    </head>
    <body>
        <?php
        include('lib/misFunciones.php');
        if (isset($_GET['idHorario'])) {
            $id_horario = $_GET['idHorario'];
            $id_cita = getCita($id_horario, date('Ymd'));
            $derecho = getDatosDerecho($id_cita["id_derecho"]);
            $id_servicio = regresarIdServicio($_SESSION["idDr"]);
            $servicio = getServicioXid($id_servicio);
            $medico = getMedicoXid($_SESSION["idDr"]);
        ?>
        <div align="center">
            <form id="forma" name="forma">
                <input type="hidden" name="idHorario" id="idHorario" value="<?php echo $_GET['idHorario'] ?>" />
                <input type="hidden" name="idDerecho" id="idDerecho" value="<?php echo $id_cita["id_derecho"] ?>" />
                <input type="hidden" name="idDr" id="idDr" value="<?php echo $_SESSION["idDr"] ?>" />
                <input type="hidden" name="idServicio" id="idServicio" value="<?php echo $id_servicio ?>" />
                <input type="hidden" name="dias_otorgados" id="dias_otorgados" value="1" />
                <input type="hidden" name="dias_otorgados_letra" id="dias_otorgados_letra" value="uno" />
                <table width="800" border="0" class="tablaPrincipal">   
                    <tr>
                        <td colspan="4" class="tituloVentana">Licencia M&eacute;dica</td>

                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">C&eacute;dula</td>
                        <td ><?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo']; ?></td>
                        <td align="right" class="textosParaInputs">Nombre</td>
                        <td><?php echo ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']); ?>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Fecha Inicio</td>
                        <td><INPUT type="text" name="date1" id="date1" readonly="readonly" onfocus="doShow('datechooser1','forma','date1')" value="<?php echo date("d-m-Y"); ?>"><div enabled="false" id="datechooser1" style="display:inline;" onclick="getDiasOtorgados();"></div>
                        </td>
                        <td align="right" class="textosParaInputs">Fecha Término</td>
                        <td><INPUT type="text" name="date2" id="date2" readonly="readonly" onfocus="doShow('datechooser2','forma','date2')" value="<?php echo date("d-m-Y"); ?>"><div enabled="false" id="datechooser2" style="display:inline;" onclick="getDiasOtorgados();"></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Días Otorgados</td>
                        <td><div id="div_dias_otorgados">1</div></td>
                        <td align="right" class="textosParaInputs">Días Otorgados<br>(con letra)</td>
                        <td><div id="div_dias_otorgados_letra">uno</div></td>
                    </tr>
                   <tr>
                        <td align="right" class="textosParaInputs">Dependencia</td>
                        <td><input type="text" name="dependencia" id="dependencia" style="width:200px"></td>
                        <td align="right" class="textosParaInputs">Unidad Adva.</td>
                        <td><input type="text" name="unidad" id="unidad" style="width:200px"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Diagnóstico</td>
                        <td colspan="3"><input name="diagnostico" type="text" id="diagnostico" style="width:500px"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Motivo de la Licencia</td>
                        <td colspan="3">
                            <select id="motivo_licencia" name="motivo_licencia">
                                <option value="" selected>Selecciona...</option>
                                <option value="PRT">Posible Riesgo de Trabajo</option>
                                <option value="RT">Riesgo de Trabajo</option>
                                <option value="EG">Enfermedad General</option>
                                <option value="MPRE">Maternidad Pre-parto</option>
                                <option value="MPOS">Maternidad Post-parto</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Carácter de la Licencia</td>
                        <td colspan="3">
                            <select id="caracter_licencia" name="caracter_licencia">
                                <option value="" selected>Selecciona...</option>
                                <option value="inicial">Inicial</option>
                                <option value="subsecuente">Subsecuente</option>
                                <option value="retroactiva">Retroactiva</option>
                                <option value="excepcional">Excepcional</option>
                             </select>                            
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Tipo de Servicio Otorgado</td>
                        <td colspan="3">
                            <select id="servicio_otorgado" name="servicio_otorgado">
                                <option value="" selected>Selecciona...</option>
                                <option value="consulta externa">Consulta Externa</option>
                                <option value="hospitalización">Hospitalización</option>
                                <option value="urgencias">Urgencias</option>
                             </select>                            
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Medico</td>
                        <td colspan="3"><?php echo $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']; ?><input name="medico" type="hidden" value="<?php echo $medico['id_medico']; ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input name="idDerecho" type="hidden" id="idDerecho" value="<?php echo $id_cita['id_derecho']; ?>" ><input name="idServicio" type="hidden" id="idServicio" value="<?php echo $id_servicio; ?>" ></td>

                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                           
<?php 

        } else {
            echo 'error';
        }
                            if ($_SESSION['tipoUsuario'] == 8) {
                                echo "  <input type='button' name='regresar' id='regresar' value='Regresar' class='botones'  onclick='javascript: inicio(\"inicioMedicoEsp.php\");' tabindex='11' />&nbsp;&nbsp;&nbsp;&nbsp;";
                            } else if ($_SESSION['tipoUsuario'] == 4) {
                                echo "<input type='button' name='regresar' id='regresar' value='Regresar' class='botones'  onclick='javascript: inicio(\"inicioMedico.php\");' tabindex='11' />&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                            ?>
                            <input type="button" onClick="genLicencia();" id="agregar" class="botones" value="Guardar"> <br /><br /><div id="enviando">&nbsp;</div></td>
                    </tr>
                </table>
            </form>
        </div>
       

    </body>
</html>