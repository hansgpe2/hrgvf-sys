<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {

	$orden = 'descripcion';
	$sentido = 'ASC';
	if (isset($_REQUEST["orden"])) {
		$orden = $_REQUEST["orden"];
	}
	if (isset($_REQUEST["sentido"])) {
		if ($_REQUEST["sentido"] == 'DESC') $sentido = 'ASC'; else $sentido = 'DESC';
	}
	
	$datos = getMedicamentosEspOrden($orden . ' ' . $sentido);
	$TDatos = count($datos);
	
	$out = '<table class="ventana">
          <tr class="tituloVentana">
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.id_medicamento&sentido=' . $sentido . '">Id.</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.descripcion&sentido=' . $sentido . '">Nombre</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.presentacion&sentido=' . $sentido . '">Cant.</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.unidad&sentido=' . $sentido . '">Unidad</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.sin_restricciones&sentido=' . $sentido . '">Restricciones</a></td>
            <td>Acci&oacute;n</td>
          </tr>';
	
	for ($i=0;$i<$TDatos;$i++) {
		$out.= "<tr class='filas'><td>" . $datos[$i]['id_medicamento'] . "</td><td>" . htmlentities($datos[$i]['descripcion']) . "</td><td>" . $datos[$i]['presentacion'] . "</td><td>" . $datos[$i]['unidad'] . "</td><td>" . $datos[$i]['sin_restricciones'] . "</td><td><input name=\"mod" . $i . "\" id=\"mod" . $i . "\" type=\"button\" value=\"Eliminar\" class=\"botones\" onClick=\"javascript: location.href= 'medicamentos_especiales_eliminar.php?id_medicamento=" . $datos[$i]['id_medicamento'] . "'\" /></td></tr>";
	}
	$out.= "<tr><td align=\"center\" colspan=\"6\"><br><br><input name=\"agregar\" id=\"agregar\" type=\"button\" value=\"Agregar Medicamento sin Restricciones\" class=\"botones\" onClick=\"javascript: location.href= 'medicamentos_especiales_agregar.php'\" /></td></tr>";
	$out.= "</table>";


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Recetas</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center"><?php echo $out; ?>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php 
} ?>