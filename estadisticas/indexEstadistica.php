<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Formato de Reportes</title>
<title>Generar reporte de Medico</title>
        <!--importar css-->
        <link rel="stylesheet" href="js/jquery_ui/themes/base/jquery.ui.all.css" />
        <link rel="stylesheet" href="js/jquery_ui/themes/base/jquery.ui.datepicker.css"  />
 <link href="misEstilos.css" rel="stylesheet" type="text/css" />      
        <!--Scripts del calendario Popup-->
        <script src="js/jquery_ui/jquery-1.7.2.js"></script>
        <script src="js/jquery_ui/ui/jquery.ui.core.js"></script>
        <script src="js/jquery_ui/ui/jquery.ui.widget.js"></script>
        <script src="js/jquery_ui/ui/jquery.ui.datepicker.js"></script>
        <script src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
        
        <!--Funcionamiento del calendario-->
        <script type="text/javascript">
                $(function (){
                $("#date1").datepicker();
            });
            $(document).ready(function() {
                $("#date1").focus();
                $("#genRep").validate({rules:{
                        date1:{
                            required: true,
                            date:true
                        }
                }});
            });
        </script>
</head>

<body>
<table width="1024" height="74" class="ventana" align="center">
<tr>
<td rowspan="2" width="76" height="74" class="tituloVentana"><img src="_images/logoEncabezado.png" width="74" height="74" /></td><td width="936" height="44" align="center" class="tituloVentana"><span class="tituloEncabezado">Cita Medica Electronica</span></td></tr>
<tr><td height="21" width="936" align="center" class="tituloVentana"><span class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado</span></td></tr>
<tr><td height="100" colspan="2"></td></tr>
<tr><td height="188" colspan="3" align="center">
<form action="resultado.php" method="post" name="genRep" target="_blank" id="genRep">
<table width="329" height="188" class="tablaPrincipal"><tr><td colspan="3" align="center" class="tituloVentana"><span class="subtituloEncabezado">Consulta de estadisticos de citas</span></td></tr><tr><td height="21" colspan="3">&nbsp;</td></tr>
<tr><td colspan="3">
        <label for="date1"><span class="Encabezado">Fecha del reporte</span></label><input type="text" name="date1" id="date1" /></td></tr>
<tr><td colspan="3">Turno</td></tr>
<tr><td width="100"><input type="radio" name="turno" value="M" />Matutino
    </td><td width="134"><input type="radio" name="turno" value="V" />Vespertino</td><td width="79"><input type="radio" name="turno" checked/>Ambos</td></td></tr><tr>
<tr><td colspan="3">Si no se selecciona ninguno da el general</td></tr><td colspan="3"><input type="submit" value="Generar Reporte" /></td></tr>
</table></form>
</td></tr><tr><td height="406" colspan="2"></td></tr>
</table>
</body>
</html>
