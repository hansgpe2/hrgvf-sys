<?php require_once('Connections/archjaja.php'); ?>
<?php require_once('Connections/expedi.php'); ?>
<?php

function ponerCeros($cant,$cuantos) {
	$aCeros = array("","0","00","000","0000","00000","000000","0000000","00000000","000000000","0000000000","00000000000","000000000000","0000000000000","00000000000000","000000000000000");
	$tCant = strlen($cant);
	$cuantos = $cuantos - $tCant;
	return $aCeros[$cuantos] . $cant;
}

function crearSolicitud($fecha, $hora, $tipo, $status, $idUsuario, $idConsultorio, $idServicio, $idMedico) {
	global $hostname_bdisssteEx;
	global $database_bdisssteEx;
	global $username_bdisssteEx;
	global $password_bdisssteEx;
	$ret = 0;
	$bdissste = mysql_pconnect($hostname_bdisssteEx, $username_bdisssteEx, $password_bdisssteEx) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdisssteEx, $bdissste);
	$query_query = "INSERT INTO solicitudes VALUES('','" . $fecha . "','" . $hora . "','','','" .$tipo . "','" . $status . "','" . $idUsuario . "','','" . $idConsultorio . "','" . $idServicio . "','" . $idMedico . "','')";
	$query = mysql_query($query_query, $bdissste); //or die(mysql_error());
	$ret = mysql_insert_id($bdissste);
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}

function crearSolicitudDetalle($id_sol, $id_derecho, $id_cita, $fecha, $hora, $status, $extra) {
	global $hostname_bdisssteEx;
	global $database_bdisssteEx;
	global $username_bdisssteEx;
	global $password_bdisssteEx;
	$ret = 0;
	$bdissste = mysql_pconnect($hostname_bdisssteEx, $username_bdisssteEx, $password_bdisssteEx) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdisssteEx, $bdissste);
	$query_query = "INSERT INTO solicitudes_detalles VALUES('','" . $id_sol . "','" . $id_derecho . "','" . $id_cita . "','" . $fecha . "','" . $hora . "','" . $status . "','" . $extra . "')";
	$query = mysql_query($query_query, $bdissste); //or die(mysql_error());
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}


function orderMultiDimensionalArrayF ($toOrderArray, $field, $inverse = false) {
    $position = array();
    $newRow = array();
    foreach ($toOrderArray as $key => $row) {
            $position[$key]  = $row[$field];
            $newRow[$key] = $row;
    }
    if ($inverse) {
        arsort($position);
    }
    else {
        asort($position);
    }
    $returnArray = array();
    foreach ($position as $key => $pos) {     
        $returnArray[] = $newRow[$key];
    }
    return $returnArray;
}



function getSolicitudExpedientes($id_solicitud, $idConsultorio, $idServicio, $idMedico, $idUsuario, $tipoReporte, $fechaI, $fechaF, $saltoLinea) {
	$arregloDatos = array();
	$out = '';
	$fechaInicio = $fechaI;
	$fechaFin = $fechaF;
	$claveUnidadMedica = "&nbsp;";
	$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
	$delegacion = "DELEGACION ESTATAL EN JALISCO";
	$servicio = "CONSULTA EXTERNA";
	
	$datosMedico = getMedicoXid($idMedico);		
	$datosServicio = getServicioXid($idServicio);
	$fechaI = substr($fechaI,6,4) . substr($fechaI,3,2) . substr($fechaI,0,2);
	$fechaF = substr($fechaF,6,4) . substr($fechaF,3,2) . substr($fechaF,0,2);
	$citas = citasXrangoFechas($fechaI,$fechaF);
	$totalCitas = count($citas);
	$bandera = false;
	for($i=0;$i<$totalCitas;$i++) {
		if($citas[$i]['id_derecho'] > 0) {;
			$horario = getHorarioXid($citas[$i]['id_horario']);
			if ($horario['id_medico'] == $idMedico) {
				if ($bandera == false) $out = "";
				$bandera = true;
				$datosDH = getDatosDerecho($citas[$i]['id_derecho']);
		//		$j++;
				$nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
				$expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
				$vigenciaSi = "X";
				$vigenciaNo = "&nbsp;";
				$sexoM = "&nbsp;";
				$sexoF = "&nbsp;";
				$edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
				$sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
				if ($sexo == "M") $sexoM = $edad;
				if ($sexo == "F") $sexoF = $edad;
				$foraneo = tipoDH($datosDH['municipio']);
				$prv = "&nbsp;";
				$sub = "&nbsp;";
				if ($horario['tipo_cita'] == 0) $prv= "X"; else $sub = "X";
				$arregloDatos[] = array("id_cita" => $citas[$i]['id_cita'], "id_horario" => $horario['id_horario'], "id_consultorio" => $horario['id_consultorio'], "id_servicio" => $horario['id_servicio'], "id_medico" => $horario['id_medico'], "fecha_cita" => $citas[$i]['fecha_cita'],"hora_cita" => $horario['hora_inicio'], "id_derecho" => $citas[$i]['id_derecho'], "cedula" => $expediente, "nombre" => ponerAcentos($nombre), "observaciones" => ponerAcentos($citas[$i]['observaciones']));
			}
		}
	}
	$citas = citasExtemporaneasXrangoFechas($fechaI,$fechaF);
	$totalCitas = count($citas);
	for($i=0;$i<$totalCitas;$i++) {
		if($citas[$i]['id_derecho'] > 0) {;
			if ($citas[$i]['id_medico'] == $idMedico) {
				if ($bandera == false) $out = "";
				$bandera = true;
				$datosDH = getDatosDerecho($citas[$i]['id_derecho']);
		//		$j++;
				$nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
				$expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
				$vigenciaSi = "X";
				$vigenciaNo = "&nbsp;";
				$sexoM = "&nbsp;";
				$sexoF = "&nbsp;";
				$edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
				$sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
				if ($sexo == "M") $sexoM = $edad;
				if ($sexo == "F") $sexoF = $edad;
				$foraneo = tipoDH($datosDH['municipio']);
				$prv = "&nbsp;";
				$sub = "&nbsp;";
				if ($citas[$i]['tipo_cita'] == 0) $prv= "X"; else $sub = "X";
				$arregloDatos[] = array("id_cita" => $citas[$i]['id_cita'], "id_horario" => "E", "id_consultorio" => $citas[$i]['id_consultorio'], "id_servicio" => $citas[$i]['id_servicio'], "id_medico" => $citas[$i]['id_medico'], "fecha_cita" => $citas[$i]['fecha_cita'],"hora_cita" => $citas[$i]['hora_inicio'], "id_derecho" => $citas[$i]['id_derecho'], "cedula" => $expediente, "nombre" => ponerAcentos($nombre) . " <b>E</b>", "observaciones" => ponerAcentos($citas[$i]['observaciones']));
			}
		}
	}
	$arregloDatos = orderMultiDimensionalArrayF($arregloDatos, "cedula", false);
	$tArrDatos = count($arregloDatos);
	if ($tArrDatos > 0) {
		$fechaSis = date('Ymd');
		$horaSis = date('H:i:s');
		$id_sol = crearSolicitud($fechaSis, $horaSis, "0", "1", $idUsuario, $idConsultorio, $idServicio, $idMedico); // 0 = tipo consulta externa; 1 = status solicitada
		for ($i=0; $i<$tArrDatos; $i++) {
			$detalle = crearSolicitudDetalle($id_sol, $arregloDatos[$i]['id_derecho'], $arregloDatos[$i]['id_cita'], $arregloDatos[$i]['fecha_cita'], $arregloDatos[$i]['hora_cita'], "0", $arregloDatos[$i]['id_horario']); // 0 = status por buscar el expediente
			$out .= "<tr height=\"60\">
				<td align=\"center\" class=\"contenido8BordeCB\"><img src=\"barcode/barcode.php?code=" . ponerCeros($arregloDatos[$i]["id_derecho"],8) . "&tam=1\" alt=\"barcode\" /></td>
				<td align=\"center\" class=\"contenido10BordeSI\">" . $arregloDatos[$i]["cedula"] . "</td>
				<td align=\"left\" class=\"contenido8Borde\">" . $arregloDatos[$i]["nombre"] . "</td>
				<td align=\"left\" class=\"contenido8Borde\">" . $arregloDatos[$i]["observaciones"] . "</td>
			  </tr>";
		}
	} else {
		$out = "<tr height=\"30\"><td colspan=\"5\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . formatoDia($fechaI,"imprimirCita") . " A " . formatoDia($fechaF,"imprimirCita") . "</td></tr>";
	}
	$claveDelMedico = $datosMedico['n_empleado'];
	$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
	$claveDeLaEspecialidad = $datosServicio['clave'];
	$especialidad = ponerAcentos($datosServicio['nombre']);
	$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td width=\"74\" align=\"left\"><img src=\"diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
				<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
				  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
				<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />Ficha para Archivo</td>
				<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"180\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
				  	<td colspan=\"2\" align=\"center\"><img src=\"barcode/barcode.php?code=" . ponerCeros($id_sol,12) . "&tam=1\" border=\"0\" alt=\"barcode\" /></td>
				  </tr>
				  </table>
				  <table width=\"180\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
					<td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
				  </tr>
				  <tr>
					<td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
					<td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
				  </tr>
				</table></td>
			  </tr>
			</table></td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
		  </tr>
		  <tr>
			<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del Médico</td>
		  </tr>
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td align=\"left\" class=\"contenido8bold\" width=\"50\">Clave del Médico:</td>
				<td align=\"left\" class=\"contenido8\" width=\"100\">" . $claveDelMedico . "</td>
				<td align=\"left\" class=\"contenido8bold\" width=\"100\">Nombre del Médico:</td>
				<td align=\"left\" class=\"contenido8\" width=\"300\">" . $nombreDelMedico . "</td>
				<td align=\"left\" class=\"contenido8bold\" width=\"50\">Servicio:</td>
				<td align=\"left\" class=\"contenido8\" width=\"300\">" . $servicio . "</td>
			  </tr>
			  <tr>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Clave de la Especialidad:</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $claveDeLaEspecialidad . "</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Especialidad:</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $especialidad . "</td>
				<td align=\"left\" class=\"contenido8bold\">Horario:</td>
				<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
			  </tr>
			  <tr>
				<td align=\"left\" class=\"contenido8bold\">Firma:</td>
				<td align=\"left\" class=\"contenido8\" height=\"40\">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		  <tr>
			<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
		  </tr>
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td width=\"120\" align=\"center\" class=\"contenido8boldBorde\">Expediente</td>
				<td width=\"110\" align=\"center\" class=\"contenido8boldBorde\">C&eacute;dula</td>
				<td width=\"200\" align=\"center\" class=\"contenido8boldBorde\">Nombre Paciente</td>
				<td align=\"center\" class=\"contenido8boldBorde\">Observaciones</td>
				</tr>" . $out . "
			</table></td>
		  </tr>
		</table>" . $saltoLinea;
	return $reporte;
}

?>