<?php
include_once('../lib/config.inc.php');
include_once('../lib/date_functions.php');
include_once('../lib/funcionesAdmin.php');
@session_start ();
?>


<?php


	if(!isset($_GET['getdate'])) {
		$unix_time = strtotime(date('Ymd'));
	} else {
		$unix_time  = strtotime($_GET['getdate']);
	}
	$display_date = tituloMes(date('n', $unix_time)) . " " . date('Y', $unix_time);
	$meses = "<select id=\"meses\" name=\"meses\" onchange=\"javascript: cambiarMes(this.value);\">";
	for ($i=6;$i>0;$i--) {
		$fechaCompleta =  date('Ymd', strtotime(date("Y/m/d",$unix_time) . " - " . $i . " month"));
		$mes = date('n', strtotime(date("Y/m/d",$unix_time) . " - " . $i . " month"));
		$anio = date('Y', strtotime(date("Y/m/d",$unix_time) . " - " . $i . " month"));
		$meses .= "<option value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
	}
	for ($i=0;$i<7;$i++) {
		$fechaCompleta =  date('Ymd', strtotime(date("Y/m/d",$unix_time) . " + " . $i . " month"));
		$mes = date('n', strtotime(date("Y/m/d",$unix_time) . " + " . $i . " month"));
		$anio = date('Y', strtotime(date("Y/m/d",$unix_time) . " + " . $i . " month"));
		if ($i==0) {
			$meses .= "<option selected value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
		} else {
			$meses .= "<option value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
		}
	}
	$meses .= "</select>";
	$display_date = $meses;

	$MC = array();
	$MC['title'] = $display_date;
	$MC['webcals'] = array();

/*	foreach($MonketWebCals as $webcal) {
		addWebCal($webcal);
	}
*/

echo str_pad(' ', 4095);
flush();
ob_flush();

$current_view = "month";




if (!isset($getdate)) {
	if (isset($_GET['getdate']) && ($_GET['getdate'] !== '')) {
		$getdate = $_GET['getdate'];
	} else {
		$getdate = date('Ymd', strtotime("now + $second_offset seconds"));
	}
}
if ($minical_view == 'current') $minical_view = 'month';

@ereg ("([0-9]{4})([0-9]{2})([0-9]{2})", $getdate, $day_array2);
$this_day                               = $day_array2[3];
$this_month                             = $day_array2[2];
$this_year                              = $day_array2[1];

$unix_time                              = strtotime($getdate);
$today_today                    = date('Ymd', strtotime("now + $second_offset seconds"));
$tomorrows_date                 = date( "Ymd", strtotime("+1 day",  $unix_time));
$yesterdays_date                = date( "Ymd", strtotime("-1 day",  $unix_time));

// find out next month
$next_month_month               = ($this_month+1 == '13') ? '1' : ($this_month+1);
$next_month_day                 = $this_day;
$next_month_year                = ($next_month_month == '1') ? ($this_year+1) : $this_year;
while (!checkdate($next_month_month,$next_month_day,$next_month_year)) $next_month_day--;
$next_month_time                = mktime(0,0,0,$next_month_month,$next_month_day,$next_month_year);

// find out last month
$prev_month_month               = ($this_month-1 == '0') ? '12' : ($this_month-1);
$prev_month_day                 = $this_day;
$prev_month_year                = ($prev_month_month == '12') ? ($this_year-1) : $this_year;
while (!checkdate($prev_month_month,$prev_month_day,$prev_month_year)) $prev_month_day--;
$prev_month_time                = mktime(0,0,0,$prev_month_month,$prev_month_day,$prev_month_year);

$next_month                     = date("Ymd", $next_month_time);
$prev_month                     = date("Ymd", $prev_month_time);
$display_date                   = localizeDate (@$dateFormat_month, $unix_time);
$parse_month                    = date ("Ym", $unix_time);
$first_of_month                 = $this_year.$this_month."01";
$start_month_day                = dateOfWeek($first_of_month, $week_start_day);
$thisday2                               = localizeDate(@$dateFormat_week_list, $unix_time);
$num_of_events2                         = 0;


function getCalInitHTML() {
	$init = '';

 
        $init .= '<style type="text/css" media="screen">';
        $init .= '<!--';
        $init .= '  @import "../lib/calendar.css";';
        $init .= '  @import "../lib/logging.css";';
        $init .= '  @import "../lib/dom-tt.css";';
        $init .= '-->';
        $init .= '</style>';


        $init .= '<!-- compliance patch for microsoft browsers -->';
        $init .= '<!--[if lt IE 7]>';
        $init .= '<script type="text/javascript">';
        $init .= 'IE7_PNG_SUFFIX = ".png";';
        $init .= '</script>';
        $init .= '<script src="'. @MONKET_BASE .'js/ie7/ie7-standard.js" type="text/javascript">';
        $init .= '</script>';
        $init .= '<![endif]-->';


	return $init;
}

function addWebCal($webcal) {
	global $MC;
	$MC['webcals'][] = $webcal;
}


function displayCalendarNames() {
        global $master_array;
        $calendars = $master_array['calendars'];
        echo '<div class="calendars" id="calendars">';
        for ($ct1 = 1; $ct1 <= count($calendars); $ct1++) {
                $calendar = $calendars[$ct1];
                $inputId = 'view-' . idEncode($calendar['id']);

                $isWebCal = '';
                if ($calendar['isWebCal']) {
                        $isWebCal = ' webcal';
                }

		if (!defined(DEFAULT_CALENDAR)) {
			define(DEFAULT_CALENDAR, $calendar['name']);
		}
		$isDefault = '';
		if ($calendar['name'] == DEFAULT_CALENDAR) {
			$isDefault = ' default';
		}

                echo '<div class="calendar-info color-' . $ct1 . $isWebCal . $isDefault . '">';

                echo '<span class="cal-details">';
                echo '<input type="checkbox" name="' . $inputId . '" id="' . $inputId . '" checked="checked" />';
                echo '<label class="name" for="' . $inputId . '">';
                echo $calendar['name'];
                echo '</label>';
                echo '</span> ';

                echo '<span class="links">';
                if ($calendar['isWebCal']) {
                        echo '<a href="' . $calendar['filename'] . '">ics</a>';
                        echo ' <span class="faded">rss</span>';
                } else {
                        echo '<a href="phpicalendar-karl/calendars/' . $calendar['id'] . '.ics' . '">ics</a>';
                        echo ' <a href="phpicalendar-karl/rss/rss.php?cal=' . $calendar['id'] . '&amp;rssview=month">rss</a>';
                }
                echo '</span>';

                echo '</div>';
        }
        echo '</div>';

}

function idEncode($text) {
        $temp = preg_replace('/ +/', '-', $text);
        return preg_replace('/[^A-Za-z0-9-_:]/', '_', $temp);
}


function displayCalendar() {
	global $master_array;
	global $prev_month;
	global $next_month;
	global $week_start_day;
	global $daysofweek_lang;
	global $start_month_day;
	global $today_today;
	
/*<div style="text-align: center; margin: 20px;"><a href="<?php echo SITE_DIR ?>">Today</a></div> */



?>

<div id="baseURL" title="<?php echo SITE_DIR ?>" class="hidden"></div>


<div id="calendar" style="width:500px; height:590px;">

<?php echo '<div class="prev-month"><a href="javascript: cambiarMes(' . $prev_month . ');"
title="Mes Anterior">&laquo;</a></div>'; ?>

<?php echo '<div class="next-month"><a href="javascript: cambiarMes(' . $next_month . ');"
title="Mes Siguiente">&raquo;</a></div>'; ?>



<a name="top"></a>
<div class="day-header">
<table cellspacing="0"  class="header" summary="Days of Weeks (these act as titles for follwing week tables)">
        <tr>
        <?php
                // loops through 7 times, starts with $week_start_day
                $start_day = strtotime($week_start_day);
                for ($i=0; $i<7; $i++) {
                        $day_num = date("w", $start_day);
                        $day = $daysofweek_lang[$day_num];
                        echo '<th>' . tituloDia($day_num) . '</th>';
                        $start_day = strtotime("+1 day", $start_day);
                }

        ?>
        </tr>
</table>
</div>
        <?php
                $sunday = strtotime("$start_month_day");
                $whole_month = TRUE;
                $weekNum = 0;
				echo '<form action="incidenciasAgregarDias.php" method="post" name="forma" id="forma">';
                do {
                        $weekNum++;
                        echo '<div class="week">';
                        echo '<table cellspacing="0" summary="' . date('jS F Y', $sunday) . '">';

                        echo '<tr>';
                        for ($ct1 = 0; $ct1 < 7; $ct1++) {
                                $thisDate = strtotime('+' . $ct1 . ' day', $sunday);
                                $day = date ("j", $thisDate);
                                $daylink = date ("Ymd", $thisDate);
								$temp = "";
                                $temp.= "<td align=\"right\" height=\"80\"";

                                if ($today_today == $daylink) {
                                        $temp.= "  background: url(images/today-fade.png) top center repeat-x;";
                                }

                                $temp.= '">';

                                $temp.= '<span class="numeroDia">' . $day . '</span>';

                                if ($day == '1') {
                                        $month = substr(tituloMes(date('n', $thisDate)),0,3);
                                        $temp.= '<span class="textoMes">' . $month . '</span>';
                                }

                                $temp.= "<input name=\"bloq" . $daylink . "\" id=\"bloq" . $daylink . "\" value=\"" . $daylink . "\" type=\"checkbox\" value=\"\" /><br><a class=\"linkSelDia\" href=\"javascript:selDia('" . $daylink . "','" . $today_today . "');\">";
								$temp.= recuperarCantidadCitas('PRV',$daylink,$_SESSION['idUsuario'],$_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']);
								$temp.= recuperarCantidadCitas('SUB',$daylink,$_SESSION['idUsuario'],$_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']);
								$temp.= recuperarCantidadCitas('PRO',$daylink,$_SESSION['idUsuario'],$_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']);
                                $temp.= '</a></td>';
								echo $temp;
                        }
                        echo '</tr>';
                        echo '</table>';
                        echo '</div>';

                        $sunday = strtotime("+7 day", $sunday);
                        $checkagain = date ("m", $sunday);
                        if ($checkagain != @$this_month) $whole_month = FALSE;
                } while ($weekNum < 6); // while ($whole_month == TRUE);    
				echo '<br /><input class="botones" type="button" name="eliminarIncidencias" id="eliminarIncidencias" value="Eliminar Incidencias de D&iacute;as Seleccionados" onclick="javascript: eliminarIncidencias2();" /> <input class="botones" type="button" onclick="javascript: agregarIncidencias();" name="programarIncidencias" id="programarIncidencias" value="Programar Incidencias en D&iacute;as Seleccionados" /></form>';                                                                                 
       ?>

</div>
<p align="center"><table width="500" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">
<tr>
<td width="15" style="background-color:#006600"></td><td width="129">Mas del 50% de citas disponibles</td>
<td width="15" style="background-color:#FF9900"></td><td width="138">Menos del 50% de citas disponibles</td>
<td width="15" style="background-color:#CC0000"></td><td width="81">0% de citas disponibles</td>
<td width="107" align="left">PRV = Primera Vez<br>SUB = Subsecuentes<br>PRO = Procedimiento</td>
</tr>
</table></p>
<?php
}


?>
