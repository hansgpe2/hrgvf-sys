<?php
session_start ();
$_SESSION['idUsuario'] = "-1";
$_SESSION['tipoUsuario'] = "-1";
$_SESSION['IdCon'] = "-1";
$_SESSION['idServ'] = "-1";
$_SESSION['idDr'] = "-1";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Cita M&eacute;dica Electr&oacute;nica</title>

<link rel="stylesheet" href="lib/misEstilos.css" type="text/css" />
<script src="lib/jquery-1.7.1.js"></script>
<script src="lib/gus_calendar.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="lib/arreglos2014.js" type="text/javascript"></script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="lib/calendar.css" rel="stylesheet" type="text/css" />
<link href="lib/dom-tt.css" rel="stylesheet" type="text/css" />
<link href="lib/login.css" rel="stylesheet" type="text/css" />
<link href="lib/impresion.css" media="print" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="lib/medicamentos.css" type="text/css" />

  <link rel="stylesheet" href="lib/css/cupertino/jquery-ui-1.10.4.custom.css">

  <script src="lib/jquery-ui-1.10.4.custom.min.js"></script>
  <script>
  (function( $ ) {
    $.widget( "custom.combobox", {
      _create: function() {
        this.wrapper = $( "<span>" )
          .addClass( "custom-combobox" )
          .insertAfter( this.element );
 
        this.element.hide();
        this._createAutocomplete();
        this._createShowAllButton();
      },
 
      _createAutocomplete: function() {
        var selected = this.element.children( ":selected" ),
          value = selected.val() ? selected.text() : "";
 
        this.input = $( "<input>" )
          .appendTo( this.wrapper )
          .val( value )
          .attr( "title", "" )
          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
          .autocomplete({
            delay: 0,
            minLength: 0,
            source: $.proxy( this, "_source" )
          })
          .tooltip({
            tooltipClass: "ui-state-highlight"
          });
 
        this._on( this.input, {
          autocompleteselect: function( event, ui ) {
            ui.item.option.selected = true;
            this._trigger( "select", event, {
              item: ui.item.option
            });
          },
 
          autocompletechange: "_removeIfInvalid"
        });
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $( "<a>" )
          .attr( "tabIndex", -1 )
          .attr( "title", "Show All Items" )
          .tooltip()
          .appendTo( this.wrapper )
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass( "ui-corner-all" )
          .addClass( "custom-combobox-toggle ui-corner-right" )
          .mousedown(function() {
            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if ( wasOpen ) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete( "search", "" );
          });
      },
 
      _source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        response( this.element.children( "option" ).map(function() {
          var text = $( this ).text();
          if ( this.value && ( !request.term || matcher.test(text) ) )
            return {
              label: text,
              value: text,
              option: this
            };
        }) );
      },
 
      _removeIfInvalid: function( event, ui ) {
 
        // Selected an item, nothing to do
        if ( ui.item ) {
          return;
        }
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.element.children( "option" ).each(function() {
          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
            this.selected = valid = true;
            return false;
          }
        });
 
        // Found a match, nothing to do
        if ( valid ) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val( "" )
          .attr( "title", value + " didn't match any item" )
          .tooltip( "open" );
        this.element.val( "" );
        this._delay(function() {
          this.input.tooltip( "close" ).attr( "title", "" );
        }, 2500 );
        this.input.data( "ui-autocomplete" ).term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      }
    });
  })( jQuery );
 
  </script>
    <style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
    /* support: IE7 */
    *height: 1.2em;
    *top: 0.1em;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 0.1em;
  }
  .custom-combobox-input * {
	  font-size:12px;
  }

  </style>

</head>

<body bgcolor="#EEEEEE" onload="javascript: obtenerLogin();">
<center>
<script language="javascript" type="text/javascript">
	document.write("<script type='text/javascript' src='lib/arreglos.js'></script"+">");
</script>
<div class="tablaPrincipalAgenda" id="tablaPrincipal">
	<div class="encabezado" id="encabezado" width="800">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="diseno/logoEncabezado.jpg" width="104" height="108" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
              </div>
    <div id="centro" class="centro" width="100%" height="100%">
    <div id="seleccion"></div>
      <table width="100%">
      <tr><td valign="top"><div id="menu" style="display:none"><a href="javascript: reportes();" title="Reportes" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reportes</a>
      			<a href="javascript: mostrarDiv('listaCitasReprogramar');" title="Reprogramar Citas" class="botones_menu"><div id="citasReprogramar"><img src="diseno/reprogramar.png" width="40" height="40" border="0" /></div><br>a Reprogramar</a><br>
                <div style="z-index:100; width:500px; height:200px; float:left; overflow:auto; position:absolute; background-color:#EEEEEE; border:solid; border-width:1px; border-color:#005AB3; display:none;" id="listaCitasReprogramar">
                </div><br>

      			<a href="javascript: mostrarDiv('listaCitasRezago');" title="Citas con Rezago" class="botones_menu"><div id="citasRezago"><img src="diseno/rezago.png" width="40" height="40" border="0" /></div><br>con Rezago</a><br>
                <div style="z-index:100; width:500px; height:200px; float:left; overflow:auto; position:absolute; background-color:#EEEEEE; border:solid; border-width:1px; border-color:#005AB3; display:none;" id="listaCitasRezago">
                </div>
                <a href="javascript: buscar();" title="Buscar Cita" class="botones_menu"><img src="diseno/buscar.png" width="40" height="40" border="0" /><br>Buscar Cita</a><br><br><br><br><br>
                <a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a><br><br>
                <a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a>
              </div></td>
      <td align="center">
      <div id="contenido">
      </div>
      </td></tr></table>
    </div>
        </div>
</center>
 
</body>
</html>
