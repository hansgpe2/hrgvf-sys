<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>

<body>
<header class="tituloVentana">Eliminar Servicios Asignados a las clinicas</header>
<p class="textosParaInputs">Buscar por:		
  <label for="buscarPor"></label>
  <select name="buscarPor" id="buscarPor" onchange="buscarPorServicios(this.id)
">
    <option value="0">Servicios</option>
    <option value="1">Clinicas</option>
    <option value="-1" selected="selected">Todos</option>
  </select>
</p>
<div id="servicios" style="display:none">
<p class="tiuloVentana">Filtrar por Servicio</p>
  <form id="form1" name="form1" action="javascript:BuscarClinicasXServicioEliminar()">
  <p><span class="textosParaInputs">
  Servicio:</span>&nbsp;&nbsp;&nbsp;
    <select name="servicio" id="servicio">
      <?php 
  	error_reporting(E_ERROR);		
      include("lib/funcionesAdmin.php");
			$sql="select * from servicios where id_grupo_servicio=1";
			$servicios=ConsultaMultiple($sql);
			foreach($servicios as $key=>$servicio)
			{
				echo "<option value='".$servicio['id_servicio']."'>".$servicio['nombre']."</option>";
			}
  ?>	
    </select>
  </p>
  <p align="center">
    <input type="submit" name="button" id="button" value="Filtrar" />
  </p>
  </form>
</div>
<div id="clinicas" style="display:none"><form action="javascript:BuscarServiciosXClinicaEliminar()" method="get" name="clinicas">
  <p>Filtrar por Clinica</p>
  <table width="100%" border="0">
    <tr>
      <td class="textosParaInputs" align="right">Estado</td>
      <td><label for="Unidad"></label>
        <select name="estado" id="estado" onfocus="cargarEstados(this.id);" onchange="cargarMunicipios(this.value,'municipio');ObtenerUnidadesMedicas(this.value);">
        </select></td>
    </tr>
    <tr>
      <td class="textosParaInputs" align="right">Municipio</td>
      <td><label for="Unidad"></label>
        <select name="municipio" id="municipio" onchange="ObtenerUnidadesMedicasMunicipio('estado',this.value);">
        </select></td>
    </tr>
    <tr>
      <td class="textosParaInputs" align="right">Unidad Médica</td>
      <td><label for="Unidad"></label>
        <select name="Unidad" id="Unidad" >
          <option value="-1"></option>
          <option value="0">Todas</option>
        </select></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p align="center">
    <input type="submit" name="button2" id="button2" value="Filtrar" />
  </p>
</form></div>
<div id="serviciosAsignados"><?php include_once("eliminarServicios.php"); ?></div>
</body>
</html>