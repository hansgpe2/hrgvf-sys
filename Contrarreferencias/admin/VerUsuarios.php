<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Documento sin título</title>
        
    </head>

    <body>
        <header class="tituloVentana">Ver Usuarios Del Sistema</header>
        <!---<p class="textosParaInputs">Buscar por:		
            <label for="buscarPor"></label>
            <select name="buscarPor" id="buscarPor" onchange="buscarPorServicios(this.id)
                    ">
                <option value="0"><a onclick="VerTodosUsuarios();">Tipo de Usuario</a></option>
                <option value="1">Clinicas</option>
                <option value="-1" selected="selected">Todos</option>
            </select>
        </p>
        <div id="servicios" style="display:none">
            <p class="tiuloVentana">Filtrar por Servicio</p>
            <form id="form1" name="form1" action="javascript:BuscarUsuariosXTipo()">
                <p><span class="textosParaInputs">
                        Tipo de Usuario:</span>&nbsp;&nbsp;&nbsp;
                    <select name="servicio" id="servicio">
                        <option value="-1"></option>
                        <option value="1">Usuario Contrarreferencia</option>
                        <option value="2">Usuario de Clinica</option>
                        <option value="3">Visor de Contrarreferencias</option>
                        <option value="4">Visor Clinicas</option>
                        <option value="5">Administrador Contrarreferencia</option>
                        <option value="6">Administrador de Clinicas</option>
                        <option value="7">Jefe de Servicio</option>
                    </select>
                </p>
                <p align="center">
                    <input type="submit" name="button" id="button" value="Filtrar" />
                </p>
            </form>
        </div>
        <div id="clinicas" style="display:none"><form action="javascript:BuscarUsuarioXUnidad()" method="get" name="clinicas">
                <p>Filtrar por Clinica</p>
                <table width="100%" border="0">
                    <tr>
                        <td class="textosParaInputs" align="right">Estado</td>
                        <td><label for="Unidad"></label>
                            <select name="estado" id="estado" onfocus="cargarEstados(this.id);" onchange="cargarMunicipios(this.value, 'municipio');
                ObtenerUnidadesMedicas(this.value);">
                            </select></td>
                    </tr>
                    <tr>
                        <td class="textosParaInputs" align="right">Municipio</td>
                        <td><label for="Unidad"></label>
                            <select name="municipio" id="municipio" onchange="ObtenerUnidadesMedicasMunicipio('estado', this.value);">
                            </select></td>
                    </tr>
                    <tr>
                        <td class="textosParaInputs" align="right">Unidad Médica</td>
                        <td><label for="Unidad"></label>
                            <select name="Unidad" id="Unidad" >
                                <option value="-1"></option>
                                <option value="0">Hospital Regional Valentin Gomez Farias</option>
                            </select></td>
                    </tr>
                </table>
                <p>&nbsp;</p>
                <p align="center">
                    <input type="submit" name="button2" id="button2" value="Filtrar" />
                </p>
            </form></div>---->
        <div id="serviciosAsignados">
            <table id="usuarios" border="0" width="100%" class="dataTable">
                <thead class="tituloVentana">
                <tr>
                    <td>Usuario</td>
                    <td>Nombre</td>
                    <td>Clinica</td>
                    <td>Estado</td>
                    <td>Servicio</td>
                    <td>Modificar</td>
                </tr></thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
               var uTable=$("#usuarios").dataTable({
                    'bProcessing': true,
                    'sScrollY': '400px',
                    'sScrollX': '100%',
                    'bJQueryUI': true,
                    'bPaginate': false,
                    'bAutoWidth': false,
                    'bScrollCollapse': false,
                    'ordercolumn':'',
                    'aaSorting': [[0, 'desc']],
                    'aoColumns': [
                        {'sClass': 'dt_right','bSearchable':true,'bSortable':true},
                        {'sClass': 'dt_right','bSearchable':true,'bSortable':true},
                        {'bSearchable':true,'bSortable':true},
                        {'bSearchable':true,'bSortable':true},
                        {'bSearchable':true,'bSortable':true},
                        {'sClass': 'dt_center'}
                    ],
                    'oLanguage': {
                        'sProcessing': 'Procesando...',
                        'sLengthMenu': 'Mostrar _MENU_ registros',
                        'sZeroRecords': 'No se encontraron resultados',
                        'sInfo': 'Mostrando desde _START_ hasta _END_ de _TOTAL_ registros',
                        'sInfoEmpty': 'Mostrando desde 0 hasta 0 de 0 registros',
                        'sInfoFiltered': '(filtrado de _MAX_ registros en total)',
                        'sInfoPostFix': '',
                        'sSearch': 'Buscar:',
                        'sUrl': '',
                        'oPaginate': {
                            'sFirst': '&nbsp;Primero&nbsp;',
                            'sPrevious': '&nbsp;Anterior&nbsp;',
                            'sNext': '&nbsp;Siguiente&nbsp;',
                            'sLast': '&nbsp;&Uacute;ltimo&nbsp;'
                        }
                    },
                    'sAjaxSource': 'ajaxnullsource.txt'
                });
                sUrl_source_ajax = '';
		uTable.fnReloadAjax('ConsultaUsuarios.php');
                uTable.fnAdjustColumnSizing();
            });
        </script>

    </body>
</html>