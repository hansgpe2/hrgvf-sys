<?php
include_once 'lib/funcionesAdmin.php';
if (isset($_REQUEST['Servicio']))
    $sql = "select * from usuarios_contrarreferencias where tipo_usuario=" . $_REQUEST['Servicio'] . " order by id_clinica ASC";
else if (isset($_REQUEST['unidad']))
    $sql = "select * from usuarios_contrarreferencias where id_clinica=" . $_REQUEST['unidad'] . " order by id_clinica ASC";
else
    $sql = "select * from usuarios_contrarreferencias order by id_clinica ASC";
$usuarios = ConsultaMultiple($sql);
$i = 0;
$e=0;
foreach ($usuarios as $key => $usuario) {
    $row=array();
    if ($usuario['id_clinica'] > 0) {
        $clinica = obtenerDatosClinica($usuario['id_clinica']);
        $nomclinica = $clinica['nombre'];
        $estadoClinica = $clinica['Estado'];
        $munClinica = $clinica['Municipio'];
    } else {
        $nomclinica = "H.R Valentín Gomez Farías";
        $estadoClinica = "Jalisco";
        $munClinica = "Zapopan";
    }


    $row[] = $usuario['login'];
    $row[] = $usuario['nombre'];
    $row[] = $nomclinica;
    $row[]=$estadoClinica;
    if ($usuario['extra1'] > 0) {
        $servicio = NombreServicio($usuario['extra1']);
        $row[] = $servicio;
    } else
        $row[] = "";

    $row[] = '<input type="button" value="Modificar/Eliminar Usuario" onclick="ModificarUsuario('.$usuario['id_usuario']. ');" />';
    if(!json_encode($row,JSON_UNESCAPED_UNICODE|JSON_HEX_TAG|JSON_HEX_QUOT))
            $e++;
    else
    $output['aaData'][] = $row;
    $i++;
}
//header('Cache-Control: no-cache, must-revalidate');
//header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
//header('Content-type: application/json');
//$joutput=  json_encode($output,JSON_UNESCAPED_UNICODE|JSON_HEX_TAG|JSON_HEX_QUOT);
echo json_encode($output,JSON_UNESCAPED_UNICODE|JSON_HEX_TAG|JSON_HEX_QUOT);
//echo json_encode($output);

?>