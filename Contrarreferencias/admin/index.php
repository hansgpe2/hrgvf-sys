<?php
if ($_COOKIE['idUsuario'] > 0) {
    include('../lib/funciones.php');
    session_start();
    $_SESSION['idUsuario'] = $_COOKIE['idUsuario'];
    $datosUsuario = obtenerDatosUsuario($_SESSION['idUsuario']);
    $_SESSION['idClinica'] = $datosUsuario['id_clinica'];
    $clinica = obtenerDatosClinica($_SESSION['idClinica']);
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>Administrador de Clinica</title>
            <?php if (!strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) { ?>
                <link rel="stylesheet" href="lib/estilosAdmin.css">
            <?php } else {
                ?>
                <link rel="stylesheet" href="lib/estilosAdmin_ie.css">
            <?php } ?>
            <link rel="stylesheet" href="../lib/misEstilos.css">
            <link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
            <script src="lib/jquery-1.8.3.js"></script>
            <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
            <script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
            <script src="lib/jquery-ui-1.10.4.custom/js/jquery-1.10.2.js"></script>
            <script src="lib/jquery.cookie.js"></script>

            <!----DataTable---->
            <link href="lib/DataTables-1.9.4/media/css/jquery.dataTables.css" />
            <script src="lib/DataTables-1.9.4/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
            <script src="lib/DataTables-1.9.4/media/js/jquery.dataTables.ex.js"></script>

            <!---Jquery UI---->
            <link href="lib/jquery-ui-1.10.4.custom/css/issste2014/jquery-ui-1.10.4.custom.css" />
            <script src="lib/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
            
            <!----Validacion--->
            <script src="lib/jquery-validate/dist/jquery.validate.min.js"></script>
            <script src="lib/jquery-validate/dist/additional-methods.min.js"></script>

            <!---Funciones del sistema---->
            <script src="lib/funcionesAdmin.js"></script>
        </head>

        <body onunload="Salir()">
            <header>
                <div class="encabezado" align="center"><table align="center">
                        <tr>
                            <td class="tituloEncabezado">Sistema de Administraci&oacute;n de Referencia y Contrarreferencia</td>
                            <td rowspan="2"><img src="../diseno/logoEncabezado.jpg" width="104" height="108" /></td>
                        </tr>
                        <tr><td class="subtituloEncabezado">INSTITUTO DE SEGURIDAD SOCIAL Y SERVICIOS PARA LOS TRABAJADORES DEL ESTADO</td></tr>
                    </table></div>
            </header>
            <nav align="center"><!---
                <ul class="menu">
                <li class="parent"><a href="">Servicios</a>
                <ul>
                <li><a href="">Alta de Servicios</a></li>
                <li><a href="">Baja de Servicios</a></li>
                </ul>
                </li>
                <li class="parent"><a href="">Unidades M&eacute;dicas</a>
                <ul>
                <li><a href="">Alta</a></li>
                <li><a href="">Baja</a></li>
                <li><a href="">Consulta y modificaci&oacute;n</a></li>
                </ul></li>
                <li><a href="">Salir</a></li>
                </ul>--->
                <ul id="MenuBar1" class="MenuBarHorizontal">
                    <li><a class="MenuBarItemSubmenu" href="#">Servicios</a>
                        <ul>
                            <li><a href="javascript:NuevoServicio()">Alta</a></li>
                            <li><a href="javascript:EliminarServicios()">Baja</a></li>
                            <li><a href="javascript:BuscarServicios()">Buscar Servicio</a></li>
                        </ul>
                    </li>
                    <li><a  class="MenuBarItemSubmenu" href="#">Usuarios</a>
                        <ul>
                            <li><a onclick="NuevoUsuario();" href="#">Alta</a></li>

                            <li><a onclick="VerUsuarios();" href="#">Consulta y modificaci&oacute;n</a></li>
                        </ul>
                    </li>
                    <li><a href="Salir.php">Salir</a>
                </ul>
            </nav>
            <script type="text/javascript">
                var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown: "../SpryAssets/SpryMenuBarDownHover.gif", imgRight: "../SpryAssets/SpryMenuBarRightHover.gif"});
            </script>
            <br>
            <br>
            <br>
            <article id="contenido"></article>
            <div id="ficha" class="issste2014 .ui-widget-content" style="background: #fffffff;opaque:0.9%;display:none;"></div>
        </body>
    </html>
    <?php
} else
    echo "Inicie Sesión";
?>