<?php
include './lib/funcionesAdmin.php';
$idUsuario = $_REQUEST['idUsuario'];
$usuario = NombreUsuario($idUsuario);
$clinica = obtenerDatosClinica($usuario['id_clinica']);
if (!isset($clinica['Estado']))
    $clinica['Estado'] = "Jalisco";
?>
<div class="ui-dialog-content">
    <form id="datosUsuario" name="Usuario" action="ModificarUsuario.php" method="GET" target="_blank">
        <table width="100%" border="0" style="background: white;">
            <tr>
                <td class="textosParaInputs" align="right">Usuario</td>
                <td><label for="usuario">
                        <input name="login" type="text" id="login" onblur="this.value = this.value.toUpperCase();" maxlength="12" value="<?php echo $usuario['login']; ?>" required title="Nombre de Usuario requerido" />
                    </label></td>
            </tr>
            <tr>
                <td class="textosParaInputs" align="right">Contrase&ntilde;a</td>
                <td><input type="password" name="password" id="password" value="<?php echo $usuario['pass']; ?>" required title="Password requerido" /></td>
            </tr>
            <tr>
                <td class="textosParaInputs" align="right">Nombre del Usuario</td>
                <td><input type="hidden" value="<?php echo $usuario['id_usuario']; ?>" name="idUsuario" id="idUsuario" >
                    <label for="nombre"></label>
                    <input type="text" name="nombre" id="nombre" value="<?php echo $usuario['nombre'] ?>" required title="Nombre de la persona requerido" /></td>
            </tr>
            <tr>
                <td class="textosParaInputs" align="right">Tipo de Usuario</td>
                <td><label for="tipoUsuario"></label>
                    <select name="tipoUsuario" id="tipoUsuario" onchange="<?php echo "mostrarSelectClinicaMod('" . $usuario['id_clinica'] . "', '" . $clinica['Estado'] . "', '" . $usuario['extra1'] . "')"; ?>">
                        <option value="-1"></option>
                        <option value="6" <?php if($usuario['tipo_usuario']==6) echo "selected"; ?>>Administrador Clinica</option>
                        <option value="2" <?php if($usuario['tipo_usuario']==2) echo "selected"; ?>>Usuario Clinica</option>
                        <option value="4" <?php if($usuario['tipo_usuario']==4) echo "selected"; ?>>Visor de clinica</option>
                        <option value="1" <?php if($usuario['tipo_usuario']==1) echo "selected"; ?>>Usuario Contrarreferencia</option>
                        <option value="3" <?php if($usuario['tipo_usuario']==3) echo "selected"; ?>>Visor Contrarreferencias</option>
                        <option value="5" <?php if($usuario['tipo_usuario']==5) echo "selected"; ?>>Administrador Contrarreferencia</option>
                        <option value="7" <?php if($usuario['tipo_usuario']==7) echo "selected"; ?>>Jefe de Servicio</option>
                    </select></td>
            </tr><tr><td colspan="2">


                    <div id="clinica" style="<?php
                    if ($usuario['tipo_usuario'] == "6" || $usuario['tipo_usuario'] == "2" || $usuario['tipo_usuario'] == "4")
                        echo "display:block";
                    else
                        echo "display:none";
                    ?>">
                        <table width="100%">
                            <tr>
                                <td width="40%" align="right" class="textosParaInputs">Estado</td>
                                <td width="60%"><label for="Unidad"></label>
                                    <select name="estado" id="estado" onfocus="cargarEstados(this.id);" onchange="cargarMunicipios(this.value, 'municipio');
                                            ObtenerUnidadesMedicas(this.value);">
                                    </select>&nbsp;Municipio
                                    <select name="municipio" id="municipio" onchange="ObtenerUnidadesMedicasMunicipio1('estado', this.value, 'Unidad');">
                                    </select></td>
                            </tr>
                            <tr>
                                <td class="textosParaInputs" align="right">Unidad Médica de Adscripción</td>
                                <td><label for="Unidad"></label>
                                    <select name="Unidad" id="Unidad" >

                                    </select></td>
                            </tr></table></div>
                    <div id="JefeServicio" style="display: none">
                        <table width="100%">
                            <tr>
                                <td width="40%" align="right" class="textosParaInputs">Servicio</td>
                                <td width="60%"><label for="Unidad"></label>
                                    <?php
                                    include_once 'lib/funcionesAdmin.php';
                                    $sql = "select * from servicios order by nombre ASC";
                                    $servicios = ConsultaMultiple($sql);
                                    $select = "<select id='servicio' name='servicio'><option value='-1'>&nbsp;</option>";
                                    foreach ($servicios as $key => $value) {
                                        $select.="<option value='" . $value['id_servicio'] . "'>" . $value['nombre'] . "</option>";
                                    }
                                    $select.="</select>";
                                    echo $select;
                                    ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div></td></tr>
            <tr>
                <td colspan="2" align="right" class="textosParaInputs">&nbsp;</td>
            </tr>
            <tr>
            </tr>
        </table></form>
</div>

<script type="text/javascript">
    $("#datosUsuario").on('submit',function() {
        $.validator.addMethod('valueNotEquals', function(value, element, arg) {
            return arg != value;
        }, 'Value must not equal arg.');
        switch ($("#tipoUsuario option:selected").val()) {
            case 2:
            case 4:
            case 6:
                $("#datosUsuario").validate({
                    rules: {
                        tipoUsuario: {
                            valueNotEquals: -1
                        },
                        Unidad: {
                            valueNotEquals: -1
                        },
                        estado: {
                            valueNotEquals: -1
                        }
                    },
                    messages: {
                        tipoUsuario: {
                            valueNotEquals: "Elija el tipo de Usuario"
                        },
                        Unidad: {
                            valueNotEquals: "Elija la Clinica de adscripcion"
                        },
                        estado: {
                            valueNotEquals: "Elija el estado de la clinica"
                        }
                    }
                });
                break;
            case 7:
                $("#datosUsuario").validate({
                    rules: {
                        tipoUsuario: {
                            valueNotEquals: -1
                        },
                        servicio: {
                            valueNotEquals: -1
                        }
                    },
                    messages:
                            {
                                tipoUsuario: {
                                    valueNotEquals: "Tipo de Usuario"
                                },
                                servicio: {
                                    valueNotEquals: "Servicio requerido"
                                }
                            }
                });
                break;
            default:
                $("#datosUsuario").validate({
                    rules:
                            {
                                tipoUsuario: {
                                    valueNotEquals: -1
                                }
                            },
                    messages: {
                        tipoUsuario: {
                            valueNotEquals: "Elija el tipo de Usuario"
                        }
                    }
                });
        }
    });
</script>