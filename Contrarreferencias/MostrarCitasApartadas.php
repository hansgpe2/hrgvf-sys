<div id="VentanaFlotante" class="flotante"></div>
<div id="Resultados">
    <?php
    session_start();
    //include 'lib/misFunciones.php';
    include './lib/funciones.php';
    $tpoFiltro = $_REQUEST['filtro'];
    if ($_SESSION['tipoUsuario'] == 7) {
        $servicio = $_SESSION['idServ'];
        switch ($tpoFiltro) {
            case 3:
                $sql = "select * from citas_apartadas where not (aceptada=0 or aceptada=3) and id_servicio=$servicio order by fecha_apartada DESC";
                break;
            case 2:
                $clinica = $_REQUEST['clinica'];
                $sql = "select * from citas_apartadas where id_unidad=" . $clinica . " and not (aceptada=3 and aceptada=0) and id_servicio=$servicio order by fecha_apartada DESC";
                break;
            case 1:
                $fecha1 = $_REQUEST['fecha1'];
                $fecha2 = $_REQUEST['fecha2'];
                $sql = "select * from citas_apartadas where not (aceptada=3 and aceptada=0) and id_servicio=$servicio and fecha_apartada>=$fecha1 and fecha_apartada<=$fecha2 order by fecha_apartada DESC";
                break;
        }
    } else {
        switch ($tpoFiltro) {
            case 3:
                $sql = "select * from citas_apartadas where (aceptada=-1 or aceptada=3) order by id_unidad,fecha_apartada ASC";
                break;
            case 2:
                $clinica = $_REQUEST['clinica'];
                $sql = "select * from citas_apartadas where id_unidad=" . $clinica . " and (aceptada=-1 or aceptada=3) order by fecha_apartada ASC";
                break;
            case 1:
                $fecha1 = $_REQUEST['fecha1'];
                $fecha2 = $_REQUEST['fecha2'];
                $sql = "select * from citas_apartadas where (aceptada=-1 or aceptada=3) and fecha_apartada>=$fecha1 and fecha_apartada<=$fecha2 order by id_unidad,fecha_apartada ASC";
                break;
        }
    }
    echo "<table class='Ventana' border='1'>
        <tr><th class='tituloVentana'>Solicitud</th>
        <th class='tituloVentana'>Cedula</th>
        <th class='tituloVentana'>Nombre</th>
        <th class='tituloVentana'>Servicio</th>
        <th class='tituloVentana'>Medico</th>";
    if ($_SESSION['tipoUsuario'] == 7)
        echo "<th class='tituloVentana'>Respuesta de Contrarreferencia</th>
        <th class='tituloVentana'>Diagnostico</th>";
    else
        echo "<th class='tituloVentana'>Respuesta del Jefe de Servicio</th>
        <th class='tituloVentana'>Motivo de Rechazo del Jefe de Servicio</th>";
    echo "<th class='tituloVentana'>Fecha Solicitada</th>
        <th class='tituloVentana'>Fecha y hora de la cita</th>
        <th class='tituloVentana'>Clinica de referencia</th>
        <th class='tituloVentana'>Archivos</th>";
    $citas = consultaMultipleSQL($sql);
    $regitros = count($citas);
    foreach ($citas as $key => $cita) {
        $datosCita = datosCita($cita['id_cita']);
        //$horario = getHorarioXid($datosCita['id_horario']);
        $datosDer = getDatosDerecho($cita['id_derecho']);
        $datosServicio = getServicioXid($cita['id_servicio']);
        $clinica = obtenerDatosClinica($cita['id_unidad']);
        $medico = getMedicoXid($cita['id_medico']);
        $sql = "select * from archivos_referencia where id_cita=" . $cita['carpeta'];
        $files = consultaMultipleSQL($sql);
        $tablaCitas = "<tr><td>" . ponerCeros($cita['id_cita_ap'], 8) . "</td>
        <td>" . $datosDer['cedula'] . "/" . $datosDer['cedula_tipo'] . "</td>
            <td>" . ponerAcentos($datosDer['ap_p'] . " " . $datosDer['ap_m'] . " " . $datosDer['nombres']) . "</td>
                <td>" . $datosServicio . "</td>
                    <td>" . ponerAcentos($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']) . "</td>";
        $tablaCitas.="<td>";
        if ($_SESSION['tipoUsuario'] == 7)
        {
            
            switch ($cita['aceptada'])
            {
                case "-1":
                    $tablaCitas.="&nbsp;";
                    break;
                case "0":
                    $tablaCitas.="Rechazada por Contrarreferencia";
                    break;
                case "1":
                    $tablaCitas.="Aceptada por Contrarreferencia";
                    break;
                case "2":
                    $tablaCitas.="Reprogramada";
                    break;
            }
        }
        else
        {
            switch ($cita['aceptada'])
            {
                case "-1":
                    $tablaCitas.="&nbsp;";
                    break;
                case "3":
                    //$medico=  getMedicoXid($_SESSION['idMedico']);
                    $tablaCitas.="Aceptada por Jefe de Servicio";
                    break;
            }
        }
        $tablaCitas.="</td><td>";
        if($cita['aceptada']==0 || $cita['aceptada']==4)
        {
            $tablaCitas.=$cita['motivo_rechazo'];
        }
        if($_SESSION['tipoUsuario']==7)
        $tablaCitas.=$cita['diagnostico'];
        $tablaCitas.="</td><td>" . date("d/m/Y H:i", strtotime($cita['fecha_apartada'])) . "</td>
                            <td>" . date("d/m/Y", strtotime($datosCita['fecha_cita'])) . "-" . date("H:i", strtotime($cita['hora_cita'])) . "</td>
                                <td>" . $clinica['nombre'] . "</td><td>";
        $tablaArchivos = "<table border='1'><tr><td>archivo</td><td>&nbsp;</td></tr>";
        foreach ($files as $key => $archivo) {
            $nombre = strtok($archivo['archivo'], ".");
            $ubicacion = $archivo['ubicacion'];
            //$ubicacion=strtok($archivo['ubicacion']);
            switch ($nombre) {
                case "Referencia_cita":
                    $tablaArchivos.="<tr><td>Referencia</td><td><a href='http://" . $_SERVER['HTTP_HOST'] . "/" . $ubicacion . "' target='_blank' ><img src='diseno/printer.png'></td></tr>";
                    break;

                case "estudios_laboratorio":
                    $tablaArchivos.="<tr><td>estudios de laboratorio</td><td><a href='http://" . $_SERVER['HTTP_HOST'] . "/" . $ubicacion . "' target='_blank' ><img src='diseno/printer.png'></td></tr>";
                    break;

                case "Placas_tx":
                    $tablaArchivos.="<tr><td>Rayos X</td><td><a href='http://" . $_SERVER['HTTP_HOST'] . "/" . $ubicacion . "' target='_blank'><img src='diseno/printer.png'></td></tr>";
                    break;

                case "Ecos":
                    $tablaArchivos.="<tr><td>Ecos</td><td><a href='http://" . $_SERVER['HTTP_HOST'] . "/" . $ubicacion . "' target='_blank'><img src='diseno/printer.png'></td></tr>";
                    break;
            }
        }
        $tablaArchivos.="</table>";
        if ($_SESSION['tipoUsuario'] == 7)
            $tablaCitas.=$tablaArchivos . "</td><td><input type='button' value='Aceptar/Rechazar Cita' class='cita' onclick='contestarSolicitud(" . $cita['id_cita_ap'] . ");'></td></tr>";
        else
            $tablaCitas.=$tablaArchivos . "</td><td><input type='button' value='Aceptar/Rechazar Cita' class='cita' onclick='contestarSolicitud(" . $cita['id_cita_ap'] . ");'><input type='button' value='Cambiar Fecha de la Cita' onclick='reprogramarCita(" . $cita['id_cita'] . "," . $cita['id_cita_ap'] . ");' id='reprogramar'></td></tr>";
        echo $tablaCitas;
    }
    echo "</table>";
    echo "<br>Total de Registros:" . $regitros;
    ?>
</div>