<?php
include_once('lib/misFunciones.php');
include_once('Connections/bdissste.php');
mysql_connect($hostname_bdissste,$username_bdissste,$password_bdissste);
mysql_select_db($database_bdissste);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

    <body>
<form id="datechooser" name="datechooser" method="POST" action="javascript:VerContrarreferencia();">
  <table width="400" border="0" cellspacing="0" cellpadding="0" class="ventana">
    <tr>
      <td class="tituloVentana" height="23">VER LISTA DE CONTRARREFERENCIAS</td>
    </tr>
    <tr>
      <td align="left" valign="top"><br />Reporte por: 
        <table width="200">
        <tr>
        <td>
        <input type="radio" name="RadioGroup1" value="todas" onclick="ocultarDiv('clinicas');" />
        Todas las referencias de la Unidad
        </td>
          <tr>
            <td><p>
              <label>
                <input type="radio" name="RadioGroup1" value="unidad" id="RadioGroup1_0" onclick="seleccionarEstado()" />
                Unidad de Referencia</label>
            </p>
              <div id="clinicas" style="display:none">
                <p>
                  <label for="unidades_medicas">Estado</label>
                  <label for="estado"></label>
                  <select name="estado" id="estado">
                  </select><br />
                  <input type="radio" name="filClinica" id="filClinica" value="0" />
                  <label for="filClinica">Todas</label>
                  las clinicas</p>
                <p>
                  <input type="radio" name="filClinica" id="filClinica" value="1" onclick="ObtenerUnidadesMedicas()" />
                  Por Clinica </p>
                <p> &nbsp;Clinica
                  <select name="unidades_medicas" id="Unidad">
                  </select>
                </p>
              </div>
            <p>&nbsp; </p></td>
          </tr>
          <tr><td>Servicio y M&eacute;dico</td></tr>
          <tr>
            <td><label>
              <input type="radio" name="RadioGroup1" value="servicio" id="RadioGroup1" onclick="ocultarDiv('clinicas');" />
              Servicio</label><select name="MSer" id="MSer">
              <?php
			 $servicios=getServiciosXConsultorio();
			 $i=0;

			  while($i<count($servicios))
			  {
				echo "<option value='".$servicios[$i]['id_servicio']."'>".$servicios[$i]['clave']." ".$servicios[$i]['nombre']."</option>";
				$i++;
			  }
			  ?>
              </select></td>
          </tr>
          <tr>
            <td><label>
              <input name="RadioGroup1" type="radio" id="RadioGroup1" value="medico" checked="checked" onclick="mostrarDiv('clinicas');MedicosXServicio('MSer');" />
              Médico
              </label>
              <label for="medico"></label>
              <select name="medico" id="medico" onfocus="MedicosXServicio('MSer');">
              </select>
            <label> </label></td>
          </tr>
          <tr>
            <td>Referencias 
              <select name="TipoRespuesta" id="TipoRespuesta">
                <option value="2">Todas</option>
                <option value="1">Enviadas</option>
                <option value="0">No Enviadas</option>
            </select></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
    	<td><input type="hidden" name="idServicio" id="hiddenField" />
            <br />Fecha:<br /><div id="padre"  style="position:relative">de
              
                <label for="mes1">mes</label>
                <select name="mes1" id="mes1" onchange="calendarios('1');">
                  <option value="01" <?php if (!(strcmp(01, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>ene</option>
                  <option value="02" <?php if (!(strcmp(02, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>feb</option>
                  <option value="03" <?php if (!(strcmp(03, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>mar</option>
                  <option value="04" <?php if (!(strcmp(04, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>abr</option>
                  <option value="05" <?php if (!(strcmp(05, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>may</option>
                  <option value="06" <?php if (!(strcmp(06, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>jun</option>
                  <option value="07" <?php if (!(strcmp(07, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>jul</option>
                  <option value="08" <?php if (!(strcmp(08, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>ago</option>
                  <option value="09" <?php if (!(strcmp(09, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>sept</option>
                  <option value="10" <?php if (!(strcmp(10, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>oct</option>
                  <option value="11" <?php if (!(strcmp(11, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>nov</option>
                  <option value="12" <?php if (!(strcmp(12, "<?php echo date(\"m\");"))) {echo "selected=\"selected\"";} ?>>dic</option>
                </select>
                 <label for="dia1">dia</label>
              <select name="dia1" id="dia1">
                <option value="01" <?php if (!(strcmp(01, date("d")))) {echo "selected=\"selected\"";} ?>>1</option>
                <option value="02" <?php if (!(strcmp(02, date("d")))) {echo "selected=\"selected\"";} ?>>2</option>
                <option value="03" <?php if (!(strcmp(03, date("d")))) {echo "selected=\"selected\"";} ?>>3</option>
                <option value="04" <?php if (!(strcmp(04, date("d")))) {echo "selected=\"selected\"";} ?>>4</option>
                <option value="05" <?php if (!(strcmp(05, date("d")))) {echo "selected=\"selected\"";} ?>>5</option>
                <option value="06" <?php if (!(strcmp(06, date("d")))) {echo "selected=\"selected\"";} ?>>6</option>
                <option value="07" <?php if (!(strcmp(07, date("d")))) {echo "selected=\"selected\"";} ?>>7</option>
                <option value="08" <?php if (!(strcmp(08, date("d")))) {echo "selected=\"selected\"";} ?>>8</option>
                <option value="09" <?php if (!(strcmp(09, date("d")))) {echo "selected=\"selected\"";} ?>>9</option>
                <option value="10" <?php if (!(strcmp(10, date("d")))) {echo "selected=\"selected\"";} ?>>10</option>
                <option value="11" <?php if (!(strcmp(11, date("d")))) {echo "selected=\"selected\"";} ?>>11</option>
                <option value="12" <?php if (!(strcmp(12, date("d")))) {echo "selected=\"selected\"";} ?>>12</option>
                <option value="13" <?php if (!(strcmp(13, date("d")))) {echo "selected=\"selected\"";} ?>>13</option>
                <option value="14" <?php if (!(strcmp(14, date("d")))) {echo "selected=\"selected\"";} ?>>14</option>
                <option value="15" <?php if (!(strcmp(15, date("d")))) {echo "selected=\"selected\"";} ?>>15</option>
                <option value="16" <?php if (!(strcmp(16, date("d")))) {echo "selected=\"selected\"";} ?>>16</option>
                <option value="17" <?php if (!(strcmp(17, date("d")))) {echo "selected=\"selected\"";} ?>>17</option>
                <option value="18" <?php if (!(strcmp(18, date("d")))) {echo "selected=\"selected\"";} ?>>18</option>
                <option value="19" <?php if (!(strcmp(19, date("d")))) {echo "selected=\"selected\"";} ?>>19</option>
                <option value="20" <?php if (!(strcmp(20, date("d")))) {echo "selected=\"selected\"";} ?>>20</option>
                <option value="21" <?php if (!(strcmp(21, date("d")))) {echo "selected=\"selected\"";} ?>>21</option>
                <option value="22" <?php if (!(strcmp(22, date("d")))) {echo "selected=\"selected\"";} ?>>22</option>
                <option value="23" <?php if (!(strcmp(23, date("d")))) {echo "selected=\"selected\"";} ?>>23</option>
                <option value="24" <?php if (!(strcmp(24, date("d")))) {echo "selected=\"selected\"";} ?>>24</option>
                <option value="25" <?php if (!(strcmp(25, date("d")))) {echo "selected=\"selected\"";} ?>>25</option>
                <option value="26" <?php if (!(strcmp(26, date("d")))) {echo "selected=\"selected\"";} ?>>26</option>
                <option value="27" <?php if (!(strcmp(27, date("d")))) {echo "selected=\"selected\"";} ?>>27</option>
                <option value="28" <?php if (!(strcmp(28, date("d")))) {echo "selected=\"selected\"";} ?>>28</option>
                <option value="29" <?php if (!(strcmp(29, date("d")))) {echo "selected=\"selected\"";} ?>>29</option>
                <option value="30" <?php if (!(strcmp(30, date("d")))) {echo "selected=\"selected\"";} ?>>30</option>
                <option value="31" <?php if (!(strcmp(31, date("d")))) {echo "selected=\"selected\"";} ?>>31</option>
                 </select>
              <label for="anio1">año</label>
                 <input name="anio1" type="text" id="anio1" value="<?php echo date("Y"); ?>" size="4" maxlength="4" />
<br />&nbsp;&nbsp;a

    	<select name="mes2" id="mes2" onchange="calendarios('2');">
    	  <option value="01" <?php if (!(strcmp(01, date("m")))) {echo "selected=\"selected\"";} ?>>ene</option>
    	  <option value="02" <?php if (!(strcmp(02, date("m")))) {echo "selected=\"selected\"";} ?>>feb</option>
    	  <option value="03" <?php if (!(strcmp(03, date("m")))) {echo "selected=\"selected\"";} ?>>mar</option>
    	  <option value="04" <?php if (!(strcmp(04, date("m")))) {echo "selected=\"selected\"";} ?>>abr</option>
    	  <option value="05" <?php if (!(strcmp(05, date("m")))) {echo "selected=\"selected\"";} ?>>may</option>
    	  <option value="06" <?php if (!(strcmp(06, date("m")))) {echo "selected=\"selected\"";} ?>>jun</option>
    	  <option value="07" <?php if (!(strcmp(07, date("m")))) {echo "selected=\"selected\"";} ?>>jul</option>
    	  <option value="08" <?php if (!(strcmp(08, date("m")))) {echo "selected=\"selected\"";} ?>>ago</option>
    	  <option value="09" <?php if (!(strcmp(09, date("m")))) {echo "selected=\"selected\"";} ?>>sept</option>
    	  <option value="10" <?php if (!(strcmp(10, date("m")))) {echo "selected=\"selected\"";} ?>>oct</option>
    	  <option value="11" <?php if (!(strcmp(11, date("m")))) {echo "selected=\"selected\"";} ?>>nov</option>
    	  <option value="12" <?php if (!(strcmp(12, date("m")))) {echo "selected=\"selected\"";} ?>>dic</option>
  	  </select>
        <label for="dia1">dia</label>
        <select name="dia2" id="dia2">
          <option value="01" <?php if (!(strcmp(01, date("d")))) {echo "selected=\"selected\"";} ?>>1</option>
          <option value="02" <?php if (!(strcmp(02, date("d")))) {echo "selected=\"selected\"";} ?>>2</option>
          <option value="03" <?php if (!(strcmp(03, date("d")))) {echo "selected=\"selected\"";} ?>>3</option>
          <option value="04" <?php if (!(strcmp(04, date("d")))) {echo "selected=\"selected\"";} ?>>4</option>
          <option value="05" <?php if (!(strcmp(05, date("d")))) {echo "selected=\"selected\"";} ?>>5</option>
          <option value="06" <?php if (!(strcmp(06, date("d")))) {echo "selected=\"selected\"";} ?>>6</option>
          <option value="07" <?php if (!(strcmp(07, date("d")))) {echo "selected=\"selected\"";} ?>>7</option>
          <option value="08" <?php if (!(strcmp(08, date("d")))) {echo "selected=\"selected\"";} ?>>8</option>
          <option value="09" <?php if (!(strcmp(09, date("d")))) {echo "selected=\"selected\"";} ?>>9</option>
          <option value="10" <?php if (!(strcmp(10, date("d")))) {echo "selected=\"selected\"";} ?>>10</option>
          <option value="11" <?php if (!(strcmp(11, date("d")))) {echo "selected=\"selected\"";} ?>>11</option>
          <option value="12" <?php if (!(strcmp(12, date("d")))) {echo "selected=\"selected\"";} ?>>12</option>
          <option value="13" <?php if (!(strcmp(13, date("d")))) {echo "selected=\"selected\"";} ?>>13</option>
          <option value="14" <?php if (!(strcmp(14, date("d")))) {echo "selected=\"selected\"";} ?>>14</option>
          <option value="15" <?php if (!(strcmp(15, date("d")))) {echo "selected=\"selected\"";} ?>>15</option>
          <option value="16" <?php if (!(strcmp(16, date("d")))) {echo "selected=\"selected\"";} ?>>16</option>
          <option value="17" <?php if (!(strcmp(17, date("d")))) {echo "selected=\"selected\"";} ?>>17</option>
          <option value="18" <?php if (!(strcmp(18, date("d")))) {echo "selected=\"selected\"";} ?>>18</option>
          <option value="19" <?php if (!(strcmp(19, date("d")))) {echo "selected=\"selected\"";} ?>>19</option>
          <option value="20" <?php if (!(strcmp(20, date("d")))) {echo "selected=\"selected\"";} ?>>20</option>
          <option value="21" <?php if (!(strcmp(21, date("d")))) {echo "selected=\"selected\"";} ?>>21</option>
          <option value="22" <?php if (!(strcmp(22, date("d")))) {echo "selected=\"selected\"";} ?>>22</option>
          <option value="23" <?php if (!(strcmp(23, date("d")))) {echo "selected=\"selected\"";} ?>>23</option>
          <option value="24" <?php if (!(strcmp(24, date("d")))) {echo "selected=\"selected\"";} ?>>24</option>
          <option value="25" <?php if (!(strcmp(25, date("d")))) {echo "selected=\"selected\"";} ?>>25</option>
          <option value="26" <?php if (!(strcmp(26, date("d")))) {echo "selected=\"selected\"";} ?>>26</option>
          <option value="27" <?php if (!(strcmp(27, date("d")))) {echo "selected=\"selected\"";} ?>>27</option>
          <option value="28" <?php if (!(strcmp(28, date("d")))) {echo "selected=\"selected\"";} ?>>28</option>
          <option value="29" <?php if (!(strcmp(29, date("d")))) {echo "selected=\"selected\"";} ?>>29</option>
          <option value="30" <?php if (!(strcmp(30, date("d")))) {echo "selected=\"selected\"";} ?>>30</option>
          <option value="31" <?php if (!(strcmp(31, date("d")))) {echo "selected=\"selected\"";} ?>>31</option>
        </select>
        <label for="anio2">año</label>
        <input name="anio2" type="text" id="anio2" value="<?php echo date("Y"); ?>" size="4" maxlength="4" />
            </div><br /><p align="center">&nbsp;&nbsp;&nbsp;
<input name="boton" type="submit" value="Generar Reporte" class="botones" id="boton"  /></p>
    	<br />
    	</td>
    </tr>
  </table>
</form>
</body>
</html>
