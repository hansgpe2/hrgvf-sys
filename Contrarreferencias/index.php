<?php
session_start();
$_SESSION['idUsuario'] = "-1";
$_SESSION['tipoUsuario'] = "-1";
$_SESSION['Activo']=false;
$_SESSION['IdCon'] = "-1";
$_SESSION['idServ'] = "-1";
$_SESSION['idDr'] = "-1";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>ISSSTE - Control de Referencia y Contrarreferencia</title>

        <link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
            <link href="lib/jquery-ui-1.9.2.custom/css/ui-lightness/jquery-ui-1.9.2.custom.css">
                <script src="lib/js/jquery-1.8.3.js"></script>        
                <script src="lib/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js"></script>
                <script src="lib/jquery.cookie.js"></script>
                <script src="lib/arreglos.js"></script>
                
                </head>

                <body bgcolor="#EEEEEE" onload="obtenerLogin();">
                    <center>
                        <table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
                                    <table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
                                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="diseno/logoEncabezado.jpg" width="104" height="108" /></td>
                                                        <td height="48" class="tituloEncabezado" valign="middle">Control de Contrarreferencias</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td></tr></table>

                                    <table id="centro" class="centro" width="800" height="499">
                                        <tr><td align="center" valign="top">
                                                <table width="800">
                                                    <tr><td colspan="2">
                                                            <menu id="menu">
                                                                <div id="menuContrarreferencia" style="display:none;">
                                                                    <table border="0" width="100%" class="tablaPrincipal"><tr>
                                                                            <td width="150"><a href="javascript: inicio('inicioContrarref.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                                                                            <td width="100"><a href="javascript:verReferencias()" class="botones_menu"><img src="diseno/contrarreferencia.png" width="40" height="40" /><br />Ver Contrarreferencias</a></td>
                                                                            <td width="200"><a href="javascript:buscarReferencias()" class="botones_menu"><img src="diseno/buscar.png" /><br class="botones_menu" />Buscar por Derechohabiente</a></td>
                                                                            <td width="200"><a href="javascript:buscarCitas()" class="botones_menu"><img src="diseno/basignar.gif" /><br  />Revisar citas provenientes de <br />Clinicas</a></td>
                                                                            <td width="100" align="center"><a href="javascript:buscarCitasPrv()" class="botones_menu"><img src="diseno/agenda_icono.jpg" width="40" height="40" /><br/>Desbloquear Citas <br/>Primera Vez</a></td>
                                                                            <td width="100"><a href="javascript:ConsultarReferencias()" class="botones_menu"><img src="diseno/cita.png" /><br />Referencias de clinicas <br>contestadas</a></td>
                                                                            <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                                                                        </tr></table>
                                                                </div>
                                                                <div id="menuClinica" style="display:none;">
                                                                    <table border="0" width="100%" class="tablaPrincipal"><tr>
                                                                            <td width="150"><a href="javascript: inicio('inicioVisor.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                                                                            <td width="100"><a href="javascript:verReferencias()"><img src="diseno/contrarreferencia.png" width="40" height="40" /><br class="botones_menu" />Ver Contrarreferencias</a></td>
                                                                            <td width="200"><a href="javascript:buscarReferencias()" class="botones_menu"><img src="diseno/buscar.png" /><br class="botones_menu" />Buscar por Derechohabiente</a></td>
                                                                            <td width="200">&nbsp;</td>
                                                                            <td width="100" align="center">&nbsp;</td>
                                                                            <td width="100">&nbsp;</td>
                                                                            <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                                                                        </tr></table>
                                                                </div>
                                                                <div id="menuJefeServicio" style="display:none;">
                                                                    <table border="0" width="100%" class="tablaPrincipal"><tr>
                                                                            <td width="150"><a href="javascript: inicio('inicioJefeServicio.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                                                                            <td width="100"><a href="javascript:verReferencias()" class="botones_menu"><img src="diseno/contrarreferencia.png" width="40" height="40" /><br />Ver Contrarreferencias</a></td>
                                                                            <td width="200"><a href="javascript:buscarReferencias()" class="botones_menu"><img src="diseno/buscar.png" /><br class="botones_menu" />Buscar por Derechohabiente</a></td>
                                                                            <td width="200"><a href="javascript:buscarCitas()" class="botones_menu"><img src="diseno/basignar.gif" /><br  />Revisar citas provenientes de <br />Clinicas</a></td>
                                                                            <td width="100" align="center">&nbsp;</td>
                                                                            <td width="100"><a href="javascript:ConsultarReferencias()" class="botones_menu"><img src="diseno/cita.png" /><br />Referencias de clinicas <br>contestadas</a></td>
                                                                            <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                                                                        </tr></table>
                                                                </div>
                                                            </menu>
                                                        </td></tr>
                                                    <tr>
                                                        <td width="788" align="center" id="content"><br />
                                                            <div id="contenido">
                                                            </div>
                                                        </td></tr>
                                                </table>
                                            </td></tr></table>
                                </td></tr></table>
                    </center>
                </body>
                </html>
