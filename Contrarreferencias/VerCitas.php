<?php
$mes=date("n");
$dia=date("d");
$meses=array("ene","feb","marzo","abril","mayo","junio","julio","ago","sept","oct","dic","");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>

<body>
<table width="100%" border="0">
  <tr>
    <td class="tituloVentana">ACEPTAR CITAS DE CLINICAS</td>
  </tr>
  <tr>
    <td><table width="100%" border="0">
      <tr>
      <td>Filtrar Busqueda
        <select name="selectFiltro" id="selectFiltro" onChange="filtrarCitas()">
          <option value="3">Todas</option>
          <option value="2">clinica</option>
          <option value="1">fecha</option>
        </select></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        
      </tr>
      <tr>
        <td><div id="busquedaClinica" style="display:none">
        <table width="100%"><tr><td>
        <input type="radio" name="clinica" value="0" >Estado: <select id="estado" onFocus="cargarEstados(this.id)" onChange="ObtenerUnidadesMedicas()" ></select>
        <p>
        <input type="radio" name="clinica" value="0" >
        Clinica: <select id="Unidad"></select></p>
        </td></tr><tr><td align="center"><input type="button" id="VerCitas" value="Ver Citas Agendadas" onClick="mostrarCitas();" ></tr></table></div>
        <div id="busquedaFecha" style="display:none">
        <table width="100%">
        <tr>
          <td><span style="position:relative">de
              <label for="mes1">mes</label>
              <select name="mes1" id="mes1" onchange="calendarios('1');">
                <?php
                    for($i=1;$i<=12;$i++)
                    {
                        if($i==$mes)
                        {
                            if($i<10)
                                echo "<option value=0".$i." selected>".$meses[$i-1]."</option>";
                            else
                                echo "<option value=".$i." selected>".$meses[$i-1]."</option>";
                        }
                        else {
                            if($i<10)
                                echo "<option value=0".$i.">".$meses[$i-1]."</option>";
                            else
                                echo "<option value=".$i.">".$meses[$i-1]."</option>";
                        }
                    }
                  ?>
              </select>
              <label for="dia1">dia</label>
              <select name="dia1" id="dia1">
                <option value="01" <?php if (!(strcmp(01, date("d")))) {echo "selected=\"selected\"";} ?>>1</option>
                <option value="02" <?php if (!(strcmp(02, date("d")))) {echo "selected=\"selected\"";} ?>>2</option>
                <option value="03" <?php if (!(strcmp(03, date("d")))) {echo "selected=\"selected\"";} ?>>3</option>
                <option value="04" <?php if (!(strcmp(04, date("d")))) {echo "selected=\"selected\"";} ?>>4</option>
                <option value="05" <?php if (!(strcmp(05, date("d")))) {echo "selected=\"selected\"";} ?>>5</option>
                <option value="06" <?php if (!(strcmp(06, date("d")))) {echo "selected=\"selected\"";} ?>>6</option>
                <option value="07" <?php if (!(strcmp(07, date("d")))) {echo "selected=\"selected\"";} ?>>7</option>
                <option value="08" <?php if (!(strcmp(08, date("d")))) {echo "selected=\"selected\"";} ?>>8</option>
                <option value="09" <?php if (!(strcmp(09, date("d")))) {echo "selected=\"selected\"";} ?>>9</option>
                <option value="10" <?php if (!(strcmp(10, date("d")))) {echo "selected=\"selected\"";} ?>>10</option>
                <option value="11" <?php if (!(strcmp(11, date("d")))) {echo "selected=\"selected\"";} ?>>11</option>
                <option value="12" <?php if (!(strcmp(12, date("d")))) {echo "selected=\"selected\"";} ?>>12</option>
                <option value="13" <?php if (!(strcmp(13, date("d")))) {echo "selected=\"selected\"";} ?>>13</option>
                <option value="14" <?php if (!(strcmp(14, date("d")))) {echo "selected=\"selected\"";} ?>>14</option>
                <option value="15" <?php if (!(strcmp(15, date("d")))) {echo "selected=\"selected\"";} ?>>15</option>
                <option value="16" <?php if (!(strcmp(16, date("d")))) {echo "selected=\"selected\"";} ?>>16</option>
                <option value="17" <?php if (!(strcmp(17, date("d")))) {echo "selected=\"selected\"";} ?>>17</option>
                <option value="18" <?php if (!(strcmp(18, date("d")))) {echo "selected=\"selected\"";} ?>>18</option>
                <option value="19" <?php if (!(strcmp(19, date("d")))) {echo "selected=\"selected\"";} ?>>19</option>
                <option value="20" <?php if (!(strcmp(20, date("d")))) {echo "selected=\"selected\"";} ?>>20</option>
                <option value="21" <?php if (!(strcmp(21, date("d")))) {echo "selected=\"selected\"";} ?>>21</option>
                <option value="22" <?php if (!(strcmp(22, date("d")))) {echo "selected=\"selected\"";} ?>>22</option>
                <option value="23" <?php if (!(strcmp(23, date("d")))) {echo "selected=\"selected\"";} ?>>23</option>
                <option value="24" <?php if (!(strcmp(24, date("d")))) {echo "selected=\"selected\"";} ?>>24</option>
                <option value="25" <?php if (!(strcmp(25, date("d")))) {echo "selected=\"selected\"";} ?>>25</option>
                <option value="26" <?php if (!(strcmp(26, date("d")))) {echo "selected=\"selected\"";} ?>>26</option>
                <option value="27" <?php if (!(strcmp(27, date("d")))) {echo "selected=\"selected\"";} ?>>27</option>
                <option value="28" <?php if (!(strcmp(28, date("d")))) {echo "selected=\"selected\"";} ?>>28</option>
                <option value="29" <?php if (!(strcmp(29, date("d")))) {echo "selected=\"selected\"";} ?>>29</option>
                <option value="30" <?php if (!(strcmp(30, date("d")))) {echo "selected=\"selected\"";} ?>>30</option>
                <option value="31" <?php if (!(strcmp(31, date("d")))) {echo "selected=\"selected\"";} ?>>31</option>
              </select>
              <label for="anio1">año</label>
              <input name="anio1" type="text" id="anio1" value="<?php echo date("Y"); ?>" size="4" maxlength="4" />
              <br />
              &nbsp;&nbsp;a
              <select name="mes2" id="mes2" onchange="calendarios('2');">
                <?php
                    for($i=1;$i<=12;$i++)
                    {
                        if($i==$mes)
                        {
                            if($i<10)
                                echo "<option value=0".$i." selected>".$meses[$i-1]."</option>";
                            else
                                echo "<option value=".$i." selected>".$meses[$i-1]."</option>";
                        }
                        else {
                            if($i<10)
                                echo "<option value=0".$i.">".$meses[$i-1]."</option>";
                            else
                                echo "<option value=".$i.">".$meses[$i-1]."</option>";
                        }
                    }
                  ?>
              </select>
              <label for="dia1">dia</label>
              <select name="dia2" id="dia2">
                <option value="01" <?php if (!(strcmp(01, date("d")))) {echo "selected=\"selected\"";} ?>>1</option>
                <option value="02" <?php if (!(strcmp(02, date("d")))) {echo "selected=\"selected\"";} ?>>2</option>
                <option value="03" <?php if (!(strcmp(03, date("d")))) {echo "selected=\"selected\"";} ?>>3</option>
                <option value="04" <?php if (!(strcmp(04, date("d")))) {echo "selected=\"selected\"";} ?>>4</option>
                <option value="05" <?php if (!(strcmp(05, date("d")))) {echo "selected=\"selected\"";} ?>>5</option>
                <option value="06" <?php if (!(strcmp(06, date("d")))) {echo "selected=\"selected\"";} ?>>6</option>
                <option value="07" <?php if (!(strcmp(07, date("d")))) {echo "selected=\"selected\"";} ?>>7</option>
                <option value="08" <?php if (!(strcmp(08, date("d")))) {echo "selected=\"selected\"";} ?>>8</option>
                <option value="09" <?php if (!(strcmp(09, date("d")))) {echo "selected=\"selected\"";} ?>>9</option>
                <option value="10" <?php if (!(strcmp(10, date("d")))) {echo "selected=\"selected\"";} ?>>10</option>
                <option value="11" <?php if (!(strcmp(11, date("d")))) {echo "selected=\"selected\"";} ?>>11</option>
                <option value="12" <?php if (!(strcmp(12, date("d")))) {echo "selected=\"selected\"";} ?>>12</option>
                <option value="13" <?php if (!(strcmp(13, date("d")))) {echo "selected=\"selected\"";} ?>>13</option>
                <option value="14" <?php if (!(strcmp(14, date("d")))) {echo "selected=\"selected\"";} ?>>14</option>
                <option value="15" <?php if (!(strcmp(15, date("d")))) {echo "selected=\"selected\"";} ?>>15</option>
                <option value="16" <?php if (!(strcmp(16, date("d")))) {echo "selected=\"selected\"";} ?>>16</option>
                <option value="17" <?php if (!(strcmp(17, date("d")))) {echo "selected=\"selected\"";} ?>>17</option>
                <option value="18" <?php if (!(strcmp(18, date("d")))) {echo "selected=\"selected\"";} ?>>18</option>
                <option value="19" <?php if (!(strcmp(19, date("d")))) {echo "selected=\"selected\"";} ?>>19</option>
                <option value="20" <?php if (!(strcmp(20, date("d")))) {echo "selected=\"selected\"";} ?>>20</option>
                <option value="21" <?php if (!(strcmp(21, date("d")))) {echo "selected=\"selected\"";} ?>>21</option>
                <option value="22" <?php if (!(strcmp(22, date("d")))) {echo "selected=\"selected\"";} ?>>22</option>
                <option value="23" <?php if (!(strcmp(23, date("d")))) {echo "selected=\"selected\"";} ?>>23</option>
                <option value="24" <?php if (!(strcmp(24, date("d")))) {echo "selected=\"selected\"";} ?>>24</option>
                <option value="25" <?php if (!(strcmp(25, date("d")))) {echo "selected=\"selected\"";} ?>>25</option>
                <option value="26" <?php if (!(strcmp(26, date("d")))) {echo "selected=\"selected\"";} ?>>26</option>
                <option value="27" <?php if (!(strcmp(27, date("d")))) {echo "selected=\"selected\"";} ?>>27</option>
                <option value="28" <?php if (!(strcmp(28, date("d")))) {echo "selected=\"selected\"";} ?>>28</option>
                <option value="29" <?php if (!(strcmp(29, date("d")))) {echo "selected=\"selected\"";} ?>>29</option>
                <option value="30" <?php if (!(strcmp(30, date("d")))) {echo "selected=\"selected\"";} ?>>30</option>
                <option value="31" <?php if (!(strcmp(31, date("d")))) {echo "selected=\"selected\"";} ?>>31</option>
              </select>
              <label for="anio2">año</label>
              <input name="anio2" type="text" id="anio2" value="<?php echo date("Y"); ?>" size="4" maxlength="4" />
          </span></td></tr>
          <tr><td align="center"><input type="button" value="Ver Citas" class="botones" onClick="mostrarCitas();"></td></tr>
        </table></div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><div id="citas"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>