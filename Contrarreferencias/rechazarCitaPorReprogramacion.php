<link rel="stylesheet" href="lib/misEstilos.css" />
<?php
include 'lib/misFunciones.php';
$id_citaApartada=$_REQUEST['citaApartada'];
$sql="select * from citas_apartadas where id_cita_ap=".$id_citaApartada;
$citaApartada=  ejecutarSQLAgenda($sql);
$cita=  datosCita($citaApartada['id_cita']);
$datosCita=  datosCita($citaApartada['id_cita']);
$horario=getHorarioXid($datosCita['id_horario']);
$derecho=getDatosDerecho($datosCita['id_derecho']);
$servicio=  getServicioXid($horario['id_servicio']);
$medico=  getMedicoXid($horario['id_medico']);
$clinica=  obtenerUnidadMedica($derecho['unidad_medica']);

?>
<script src="lib/jquery-1.7.2.min.js"></script>
<script src="lib/arreglos.js"></script>
<table class="Ventana" width="800px">
<tr>
  <td class="tituloVentana">Rechazar Cita</td><td align="right" class="tituloVentana"><div id="cerrar"><button class="botones" onclick="cerrar()">X</button></div></td></tr>
<tr><td height="624" colspan="2">
<table width="100%" border="0">
  <tr>
    <td class="tituloVentana">Solicitud No. <?php echo $id_citaApartada; ?>
      <form id="form1" name="form1" method="post" action="">
        <input name="solicitud" type="hidden" id="solicitud" value="<?php echo $id_citaApartada; ?>" />
        <input name="cita" type="hidden" id="cita" value="<?php echo $datosCita['id_cita']; ?>" />
      </form></td>
  </tr>
  <tr>
    <td><table width="100%" border="0">
      <tr>
        <td colspan="2">Datos de la cita</td>
      </tr>
      <tr>
        <td width="48%"><span class='textosParaInputs'>Fecha:</span>&nbsp;<?php echo date("d/m/Y",strtotime($datosCita['fecha_cita'])); ?></td>
        <td width="52%"><span class="textosParaInputs">Horario:</span> &nbsp;<?php echo date("H:i",strtotime($horario['hora_inicio']))."-".date("H:i",strtotime($horario['hora_fin'])); ?></td>
      </tr>
      <tr>
          <td><span class="textosParaInputs">Servicio:</span> &nbsp;<?php echo $servicio; ?></td>
          <td><span class="textosParaInputs">Medico:</span> &nbsp;<?php echo $medico['ap_p']." ".$medico['ap_m']." ".$medico['nombres']; ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="479" co><table width="100%" border="0">
      <tr>
        <td colspan="2" align="center">Datos del Derechohabiente</td>
        
      </tr>
      <tr>
          <td width="49%"><span class="textosParaInputs">Cedula:</span>&nbsp;<?php echo $derecho['cedula']."/".$derecho['cedula_tipo']; ?></td>
          <td width="51%"><span class="textosParaInputs">Nombre:</span>&nbsp;<?php echo $derecho['nombres']." ".$derecho['ap_p']." ".$derecho['ap_m']; ?></td>
      </tr>
      <tr>
          <td><span class="textosParaInputs">Direcci&oacute;n:</span><?php echo $derecho['direccion']." Codigo Postal ".$derecho['Codigo_Postal']; ?></td>
          <td><span class="textosParaInputs">Clinica</span>&nbsp;<?php echo $clinica['nombre']; ?></td>
      </tr>
      <tr>
        <td colspan="2"><?php
    $sql = "select * from archivos_referencia where id_cita=" . $cita['id_cita'];
    $files = consultaMultipleSQL($sql);
    $tablaArchivos = "<table border='1'><tr><td>archivo</td><td><img src='diseno/printer.png'></td></tr>";
    foreach ($files as $key => $archivo) {
        $nombre=strtok($archivo['archivo'],".");
        $ubicacion = $archivo['ubicacion'];
        //$ubicacion=strtok($archivo['ubicacion']);
        
        if (substr_compare("Referencia_cita", $nombre, 0) == 0) {
            $tablaArchivos.="<tr><td>Referencia</td><td><a href='http://" . $_SERVER['HTTP_HOST']."/" . $ubicacion . "' target='_blank' ><img src='diseno/printer.png'></td></tr>";
        }

        if (substr_compare("estudios_laboratorio", $nombre, 0) == 0) {
            $tablaArchivos.="<tr><td>estudios de laboratorio</td><td><a href='http://" .$_SERVER['HTTP_HOST']."/" . $ubicacion . "' target='_blank' ><img src='diseno/printer.png'></td></tr>";
        }

        if (substr_compare("Placas_tx", $nombre, 0) == 0) {
            $tablaArchivos.="<tr><td>Rayos X</td><td><a href='http://" . $_SERVER['HTTP_HOST'] ."/" . $ubicacion . "' target='_blank'><img src='diseno/printer.png'></td></tr>";
        }

        if (substr_compare("Ecos", $nombre, 0) == 0) {
            $tablaArchivos.="<tr><td>Ecos</td><td><a href='http://" . $_SERVER['HTTP_HOST'] ."/" . $ubicacion . "' target='_blank'><img src='diseno/printer.png'></td></tr>";
        }
    }
    $tablaArchivos.="</table>";
    echo $tablaArchivos;
    ?></td></tr>
      <tr>
        <td height="380" colspan="2"><table width="100%">
          <tr>
              <td class="textosParaInputs">Exprese el motivo del rechazo de la cita</td>
            </tr>
            <tr>
              <td><textarea name="rechazo" rows="7" id="rechazo" maxlength="300" cols="100"></textarea></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
              <td align="center"><input type="button" value="Aceptar" onclick="rechazarCitaConfirmar()" />
                <input type="button" value="Cancelar" onclick="window.close()" /></td>
            </tr>
        </table></td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</td></tr></table>