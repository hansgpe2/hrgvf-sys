<?php

session_start();
//require_once '/lib/mpdf/mpdf.php';
include_once('lib/misFunciones.php');
//$mpdf = new mPDF('utf-8','Letter');
//ob_clean();
function getContraReferencia1Pablo($idRef) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR);
    mysql_select_db($database_bdisssteR);
    $sql = "select * from contrarreferencias where id_contra=" . $idRef;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_errno() == 0)
        if (mysql_num_rows($query)>0)
            $ret = mysql_fetch_array($query);

    return $ret;
}
$idContra = $_GET['folioContrarrefencia'];
$ref = getContraReferencia1($idContra);
$idDerecho = $ref['id_derecho'];
$doc = $ref['id_medico'];
$derecho = getDatosDerecho($idDerecho);
$medico = getMedicoXid($doc);
$servicio = getServicioXid($ref['id_servicio']);
$unidad = obtenerUnidadMedica($ref['id_unidad']);
$medicinas = ObtenerMedicamentosXReferencia($idContra);
$count = count($medicinas);
if (count($ref) > 0) {
    $evo = $ref['evolucion'];
    $datclin = $ref['resumen'];
    $diag = $ref['diagnostico'];
    $diagRef = $ref['diagnostico_ref'];
    $tratamientos = $ref['tratamiento'];
    $recom = $ref['recomendaciones'];
    $fecha = $ref['fecha_contrarreferencia'];
    $fecha = date("d/m/Y", strtotime($fecha));
    $cont = $ref['status_citas'];
}


if ($cont == 0)
    $contTrat = "El paciente  continuar&aacute; su tratamiento con su m&eacute;dico familiar";
else
    $contTrat = "El paciente  continuar&aacute; tratamiento con el m&eacute;dico especialista";
$medicamentos = "";
if (count($medicinas)) {
    $medicamentos.='<table width="100%" border="1">
        <tr>
          <td class="encabezado">Clave</td>
          <td class="encabezado">Medicamento</td>
          <td class="encabezado">Tipo Tratamiento</td>
          <td class="encabezado">Tratamiento en Dias</td>
          <td class="encabezado">Cantidad de cajas</td>
          <td class="encabezado">Indicaciones</td>
        </tr>';
    $i = 0;

    while ($i < $count) {
        $medicina = getDatosMedicamentos($medicinas[$i]['id_medicamento']);
        $desc = strtok($medicina['descripcion'], "/");
        if ($medicinas[$i]['cronico'] == 1)
            $Ttrat = "Critico";
        else
            $Ttrat = "Temporal";
        $medicamentos .= '<tr class="medicamentos">
        <td class="medicamentos">' . $medicina['id_medicamento'] . '</td>
          <td class="medicamentos">' .
                $desc . '</td>
          <td>' . $Ttrat . '</td>
          <td class="medicamentos">' . $medicinas[$i]['dias'] . '</td>
          <td class="medicamentos">' . $medicinas[$i]['cajas'] . '</td>
          <td class="medicamentos">' . nl2br($medicinas[$i]['indicaciones']) . '</td>
        </tr>';

        $i++;
    }
    $medicamentos.='</table>';
}
$html = '
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>CONTRARREFERENCIA</title>
        <style type="text/css">
            @import url("lib/impresion2.css") print;
        </style>
        <link href="lib/impresion2.css" rel="stylesheet" type="text/css">
    </head>
</head>

<body>

<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="270" align="center"><img src="diseno/logoEncabezado.jpg" alt="" width="90" height="107" /></td>
    <td width="566" align="center" class="encabezado"><p>HOSPITAL REGIONAL </p>
      <p>"DR. VALENTÍN GÓMEZ FARÍAS" <br />
        FORMATO DE CONTRARREFERENCIA</p></td>
    <td width="221"><p><img src="diseno/LogoHRVGF.jpg" alt="" width="141" height="104" /></p></td>
  </tr>
</table>
    <p><span class="titulos">CLINICA DE ADSCRIPCION</span><span class="datos">' . ponerAcentos($unidad['nombre']) . '</span>
  <br><span class="titulos">LOCALIDAD</span><span class="datos">' . $derecho['municipio'] . ',' . $derecho['estado'] . '</span> &nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">&nbsp;&nbsp;&nbsp;&nbsp; FECHA</span><span class="datos"> <?php echo $fecha; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">CEDULA</span><span class="datos">:' . $derecho['cedula'] . '/' . $derecho['cedula_tipo'] . '</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">NOMBRE</span>&nbsp;&nbsp;&nbsp;<span class="datos">' . ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']) . '</span> <span class="titulos">SERVICIO QUE ENVIA:</span>' . $servicio . '<br>
                        
                        </p>
						<p>' . $contTrat . '</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><p align="center" class="encabezado">INFORME DEL MEDICO CONSULTADO</p></td>
  </tr>
  <tr>
    <td class="titulos">RESUMEN DE DATOS CLINICOS: </strong></td>
  </tr>
  <tr>
    <td class="datos">' . ponerAcentos($datclin) . '</td>
  </tr>
  <tr>
    <td class="titulos">DIAGNOSTICOS DE:</td>
  </tr>
  <tr>
    <td><table width="100%" class="tablaDiag">
      <tr>
        <td class="titulos">Referencia</td>
        <td class="titulos">Contrarreferencia</td>
      </tr>
      <tr>
        <td class="datos">' . ponerAcentos($diagRef) . '</td>
        <td class="datos">' . ponerAcentos($diag) . '</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="titulos">SINTESIS DE LA EVOLUCION:</td>
  </tr>
  <tr>
    <td class="datos">' . ponerAcentos($evo) . '</td>
  </tr>
  <tr>
    <td class="titulos">TRATAMIENTO INSTITUIDO:</td>
  </tr>
  <tr>
    <td class="datos">' . ponerAcentos($tratamientos) . '</td></tr> 
    <tr><td>' . $medicamentos . '</td></tr>
      <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td class="titulos">RECOMENDACIONES:</td>
  </tr>
  <tr>
    <td class="datos">' . ponerAcentos($recom) . '</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="30pt" align="center">_______________________________________________________</td>
  </tr>
  <tr>
    <td align="center"><strong>' . $medico['titulo'] . ' ' . $medico['ap_p'] . ' ' . $medico['ap_m'] . ' ' . $medico['nombres'] . '<br /> DGP: ' . $medico['ced_pro'] . '<br>' . $servicio . '</strong></td>
  </tr>
  <tr>
    <td colspan="4" align="center" class="nota">Para Revisar las recetas proporcionadas al paciente<br />
      Dirijase
      a la siguiente Direccion http://192.165.95.30/farmaciag/, ingrese con el usuario visorvisor, contrase&ntilde;a 123456</td>
  </tr>
  <tr>
    <td></td>
  </tr>
</table>
<span class="nota">
<p><strong>INDICACIONES:</strong><br>
<ol>
  <li>ESTE FORMATO DEBERA IMPRIMIRSE UNICAMENTE EN <strong>UN TANTO </strong>PARA SU RESGUARDO EN EL EXPEDIENTE <strong>CON SELLO Y FIRMA DEL MEDICO TRATANTE </strong> .</li>
  <li>LAS UNIDADES MEDICAS PODRAN CONSULTAR Y/O IMPRIMIR ESTE DOCUMENTO DESDE EL SISTEMA DE REFERENCIA ELECTRONICA <strong>http://192.165.95.30/clinicas </strong> PARA REALIZAR EL CONTROL Y SEGUIMIENTO DE LOS PACIENTES DESPUÉS DE 48 HRS DE EMITIDO </li>
</body>
</html>';
//ob_end_clean();
/* $mpdf->WriteHTML($html);
  $mpdf->Output(); */
echo $html;
?>
