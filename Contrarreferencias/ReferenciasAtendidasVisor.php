        <?php error_reporting(E_ERROR); ?>
<table width="100%" border="1" class="Ventana">
<tr class="tituloVentana">
        <th>Solicitud</th>
        <th>Cedula</th>
        <th>Nombre del Derechohabiente</th>
        <th>Servicio</th>
        <th>Medico</th>
        <th>Fecha de Solicitada</th>
        <th>Fecha y Hora de la Cita</th>
        <th>Respuesta</th>
        <th>Clinica</th>
        <th>Referencia</th>
</tr>
<?php 
include './lib/misFunciones.php';
$sql=$_REQUEST['sql'];
$lim1=$_REQUEST['l1'];
$lim2=$_REQUEST['l2'];
$sql.=" Limit $lim1,$lim2";
$citas=  consultaMultipleSQL($sql);
foreach ($citas as $key => $cita) {
    $horario = getHorarioXid($cita['id_horario']);
    $servicio = getServicioXid($horario['id_servicio']);
    $medico = getMedicoXid($horario['id_medico']);
    $derecho = getDatosDerecho($cita['id_derecho']);
    $citaag = datosCita($cita['id_cita']);
    $clinica=  obtenerUnidadMedica($cita['id_unidad']);
    if($cita['aceptada']==2)
    $sql = "select * from archivos_referencia where id_cita=" . $cita['carpeta'] . " AND archivo like 'Referencia_cita.pdf'";
    else
        $sql = "select * from archivos_referencia where id_cita=" . $cita['id_cita'] . " AND archivo like 'Referencia_cita.pdf'";
    $archivo = ejecutarSQLAgenda($sql);
    ?>
        <tr>
            <td><?php echo $cita['id_cita_ap'] ?></td>
            <td><?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo'] ?></td>
            <td><?php echo $derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres'] ?></td>
            <td><?php echo $servicio; ?></td>
            <td><?php echo $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']; ?></td>
            <td><?php echo date("d-m-Y", strtotime($cita['fecha_apartada'])); ?></td>
            <td><?php if($cita['aceptada']!=0 && count($citaag)>0)
            echo date("d-m-Y", strtotime($citaag['fecha_cita'])) . "-" . date("H:i", strtotime($horario['hora_inicio'])); ?></td>
            <td>
    <?php
    switch ($cita['aceptada']) {
        case 0:
            echo "Rechazada por " . $cita['motivo_rechazo'];
            break;
        case 1:
            echo "Aceptada";
            break;
        case 2:
            echo "Reprogramada";
    }
    ?>
            </td>
            <td><?php echo $clinica['nombre'] ?></td>
            <td><?php
                if (count($archivo) > 0) {
                    $ubicacion = $archivo['ubicacion'];
                    echo "Referencia <a href='http://" . $_SERVER['HTTP_HOST'] . "/" . $ubicacion . "' target='_blank' ><img src='diseno/printer.png'>";
                }
                ?></td>
        </tr>
                <?php
            }
            ?>
</table>