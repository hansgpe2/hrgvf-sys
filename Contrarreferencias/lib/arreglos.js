/*Funciones de Contrarreferencias
 *Version 1.6
 *ISSSTE Derechos Reservados
 */

function ponerAcentos2(Text) {
    var cadena = "";
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)
    {
        var cara = Text.charAt(j);
        if (cara == "(") {
            temp = Text.substring(j, j + 4);
            switch (temp) {
                case "(/a)":
                    cadena += "u00e1";
                    j = j + 3;
                    break;
                case "(/A)":
                    cadena += "\u00c1";
                    j = j + 3;
                    break;
                case "(/e)":
                    cadena += "\u00e9";
                    j = j + 3;
                    break;
                case "(/E)":
                    cadena += "\u00c9";
                    j = j + 3;
                    break;
                case "(/i)":
                    cadena += "\u00ed";
                    j = j + 3;
                    break;
                case "(/I)":
                    cadena += "\u00cd";
                    j = j + 3;
                    break;
                case "(/o)":
                    cadena += "\u00f3";
                    j = j + 3;
                    break;
                case "(/O)":
                    cadena += "\u00d3";
                    j = j + 3;
                    break;
                case "(/u)":
                    cadena += "\u00fa";
                    j = j + 3;
                    break;
                case "(/U)":
                    cadena += "\u00da";
                    j = j + 3;
                    break;
                case "(/n)":
                    cadena += "\u00f1";
                    j = j + 3;
                    break;
                case "(/N)":
                    cadena += "\u00d1";
                    j = j + 3;
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        } else {
            cadena += Text.charAt(j);
        }
    }
    return cadena;
}


function codigoBarras(e) {
    var keynum
    var keychar
    var numcheck

    if (window.event) // IE
    {
        keynum = e.keyCode
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which
    }
    keychar = String.fromCharCode(keynum)
    numcheck = /[\d\b]/
    return numcheck.test(keychar)
}

tipo_cedulas = new Array("10", "20", "30", "31", "32", "40", "41", "50", "51", "60", "61", "70", "71", "80", "81", "90", "91", "92", "99");

var estados = new Array();
estados['Zacatecas'] = new Array('Apozol', 'Apulco', 'Atolinga', 'Benito Juarez', 'Calera', 'Canitas de Felipe Pescador', 'Concepcion del Oro', 'Cuauhtemoc', 'Chalchihuites', 'Fresnillo', 'Trinidad Garcia de la Cadena', 'Genaro Codina', 'General Enrique Estrada', 'General Francisco R. Murguia', 'El Plateado de Joaquin Amaro', 'General Panfilo Natera', 'Guadalupe', 'Huanusco', 'Jalpa', 'Jerez', 'Jimenez del Teul', 'Juan Aldama', 'Juchipila', 'Loreto', 'Luis Moya', 'Mazapil', 'Melchor Ocampo', 'Mezquital del Oro', 'Miguel Auza', 'Momax', 'Monte Escobedo', 'Morelos', 'Moyahua de Estrada', 'Nochistlan de Mejia', 'Noria de angeles', 'Ojocaliente', 'Panuco', 'Pinos', 'Rio Grande', 'Sain Alto', 'El Salvador', 'Sombrerete', 'Susticacan', 'Tabasco', 'Tepechitlan', 'Tepetongo', 'Teul de Gonzalez Ortega', 'Tlaltenango de Sanchez Roman', 'Valparaiso', 'Vetagrande', 'Villa de Cos', 'Villa Garcia', 'Villa Gonzalez Ortega', 'Villa Hidalgo', 'Villanueva', 'Zacatecas', 'Trancoso', 'Santa Maria de la Paz', 'Otro');

estados['Yucatan'] = new Array('Abala', 'Acanceh', 'Akil', 'Baca', 'Bokoba', 'Buctzotz', 'Cacalchen', 'Calotmul', 'Cansahcab', 'Cantamayec', 'Celestun', 'Cenotillo', 'Conkal', 'Cuncunul', 'Cuzama', 'Chacsinkin', 'Chankom', 'Chapab', 'Chemax', 'Chicxulub Pueblo', 'Chichimila', 'Chikindzonot', 'Chochola', 'Chumayel', 'Dzan', 'Dzemul', 'Dzidzantun', 'Dzilam de Bravo', 'Dzilam Gonzalez', 'Dzitas', 'Dzoncauich', 'Espita', 'Halacho', 'Hocaba', 'Hoctun', 'Homun', 'Huhi', 'Hunucma', 'Ixil', 'Izamal', 'Kanasin', 'Kantunil', 'Kaua', 'Kinchil', 'Kopoma', 'Mama', 'Mani', 'Maxcanu', 'Mayapan', 'Merida', 'Mococha', 'Motul', 'Muna', 'Muxupip', 'Opichen', 'Oxkutzcab', 'Panaba', 'Peto', 'Progreso', 'Quintana Roo', 'Rio Lagartos', 'Sacalum', 'Samahil', 'Sanahcat', 'San Felipe', 'Santa Elena', 'Seye', 'Sinanche', 'Sotuta', 'Sucila', 'Sudzal', 'Suma', 'Tahdziu', 'Tahmek', 'Teabo', 'Tecoh', 'Tekal de Venegas', 'Tekanto', 'Tekax', 'Tekit', 'Tekom', 'Telchac Pueblo', 'Telchac Puerto', 'Temax', 'Temozon', 'Tepakan', 'Tetiz', 'Teya', 'Ticul', 'Timucuy', 'Tinum', 'Tixcacalcupul', 'Tixkokob', 'Tixmehuac', 'Tixpehual', 'Tizimin', 'Tunkas', 'Tzucacab', 'Uayma', 'Ucu', 'Uman', 'Valladolid', 'Xocchel', 'Yaxcaba', 'Yaxkukul', 'Yobain', 'Otro');

estados['Veracruz'] = new Array('Acajete', 'Acatlan', 'Acayucan', 'Actopan', 'Acula', 'Acultzingo', 'Agua Dulce', 'Alpatlahuac', 'Alto Lucero de Gutierrez Barrios', 'Altotonga', 'Alvarado', 'Amatitlan', 'Amatitlan de los Reyes', 'angel R. Cabada', 'Apazapan', 'Aquila', 'Astacinga', 'Atlahuilco', 'Atoyac', 'Atzacan', 'Atzalan', 'Ayahualulco', 'Banderilla', 'Benito Juarez', 'Boca del Rio', 'Calcahualco', 'Camaron de Tejada', 'Camerino Z. Mendoza', 'Carlos A. Carrillo', 'Carrillo Puerto', 'Castillo de Teayo', 'Catemaco', 'Cazones de Herrera', 'Cerro Azul', 'Chacaltianguis', 'Chalma', 'Chiconamel', 'Chiconquiaco', 'Chicontepec', 'Chinameca', 'Chinampa de Gorostiza', 'Chocoman', 'Chontla', 'Chumatlan', 'Citlaltepetl', 'Coacoatzintla', 'Coahuitlan (Progreso de Zaragoza)', 'Coatepec', 'Coatzacoalcos', 'Coatzintla', 'Coetzala', 'Colipa', 'Comapa', 'Cordoba', 'Cosamaloapan', 'Cosautlan de Carvajal', 'Coscomatepec', 'Cosoleacaque', 'Cotaxtla', 'Coxquihi', 'Coyutla', 'Cuichapa', 'Cuitlahuac', 'El Higo', 'Emiliano Zapata', 'Espinal', 'Filomeno Mata', 'Fortin', 'Gutierrez Zamora', 'Hidalgotitlan', 'Huatusco', 'Huayacocotla', 'Hueyapan de Ocampo', 'Huiloapan de Cuauhtemoc', 'Igancio de la Llave', 'Ilamatlan', 'Isla', 'Ixcatepec', 'Ixhuacan de los Reyes', 'Ixhuatlan de Madero', 'Ixhuatlan del Cafe', 'Ixhuatlan del Sureste', 'Ixhuatlancillo', 'Ixmatlahuacan', 'Ixtaczoquitlan', 'Jalancingo', 'Jalcomulco', 'Jaltipan', 'Jamapa', 'Jesus Carranza', 'Jilotepec', 'Jose Azueta', 'Juan Rodriguez Clara', 'Juchique de Ferrer', 'La Antigua', 'La Perla', 'Landero y Coss', 'Las Choapas', 'Las Minas', 'Las Vigas de Ramirez', 'Lerdo de Tejada', 'Los Reyes', 'Magdalena', 'Maltrata', 'Manlio Fabio Altamirano', 'Mariano Escobedo', 'Martinez de la Torre', 'Mecatlan', 'Mecayapan', 'Medellin', 'Mihuatlan', 'Minatitlan', 'Misantla', 'Mixtla de Altamirano', 'Moloacan', 'Nanchital de Lazaro Cardenas del Rio', 'Naolinco', 'Naranjal', 'Naranjos-Amatlan', 'Nautla', 'Nogales', 'Oluta', 'Omealca', 'Orizaba', 'Otatitlan', 'Oteapan', 'Ozuluama', 'Pajapan', 'Panuco', 'Papantla', 'Paso de Ovejas', 'Paso del Macho', 'Perote', 'Platon Sanchez', 'Playa Vicente', 'Poza Rica de Hidalgo', 'Pueblo Viejo', 'Puente Nacional', 'Rafael Delgado', 'Rafael Lucio', 'Rio Blanco', 'Saltabarranca', 'San Andres Tenejapan', 'San Andres Tuxtla', 'San Juan Evangelista', 'San Rafael', 'Santiago Sochiapan', 'Santiago Tuxtla', 'Sayula de Aleman', 'Sochiapa', 'Soconusco', 'Soledad Atzompa', 'Soledad de Doblado', 'Soteapan', 'Tamalin', 'Tamiahua', 'Tampico Alto', 'Tancoco', 'Tantima', 'Tantoyuca', 'Tatahuicapan de Juarez', 'Tatatila', 'Tecolutla', 'Tehuipango', 'Temapache', 'Tempoal', 'Tenampa', 'Tenochtitlan', 'Teocelo', 'Tepatlaxco', 'Tepetlan', 'Tepetzintla', 'Tequila', 'Texcatepec', 'Texhuacan', 'Texistepec', 'Tezonapa', 'Tierra Blanca', 'Tihuatlan', 'Tlachichilco', 'Tlacojalpan', 'Tlacolulan', 'Tlacotalpan', 'Tlacotepec de Mejia', 'Tlalixcoyan', 'Tlalnelhuayocan', 'Tlaltetela', 'Tlapacoyan', 'Tlaquilpan', 'Tlilapan', 'Tomatlan', 'Tonayan', 'Totutla', 'Tres Valles', 'Tuxpan', 'Tuxtilla', 'ursulo Galvan', 'Uxpanapa', 'Vega de Alatorre', 'Veracruz', 'Villa Aldama', 'Xalapa', 'Xico', 'Xoxocotla', 'Yanga', 'Yecuatla', 'Zacualpan', 'Zaragoza', 'Zentla', 'Zongolica', 'Zontecomatlan', 'Zozocolco', 'Otro');

estados['Tlaxcala'] = new Array('Amaxac de Guerrero', 'Apetatitlan de Antonio Carvajal', 'Atlangatepec', 'Altzayanca', 'Apizaco', 'Benito Juarez', 'Calpulalpan', 'El Carmen Tequexquitla', 'Cuapiaxtla', 'Cuaxomulco', 'Emiliano Zapata', 'Santa Ana Chiautempan', 'Munoz de Domingo Arenas', 'Espanita', 'Huamantla', 'Hueyotlipan', 'Ixtacuixtla de Mariano Matamoros', 'Ixtenco', 'Mazatecochco de Jose Maria Morelos', 'Contla de Juan Cuamatzi', 'Tepetitla de Lardizabal', 'Sanctorum de Lazaro Cardenas', 'Nanacamilpa de Mariano Arista', 'Acuamanala de Miguel Hidalgo', 'Nativitas', 'Panotla', 'Papalotla de Xicohtencatl', 'San Pablo del Monte', 'Santa Cruz Tlaxcala', 'Tenancingo', 'Teolocholco', 'Tepeyanco', 'Terrenate', 'Tetla de la Solidaridad', 'Tetlatlahuca', 'Tlaxcala de Xicohtencatl', 'Tlaxco', 'Tocatlan', 'Totolac', 'Tzompantepec', 'Xaloztoc', 'Xicohtzinco', 'Yauhquemecan', 'Zacatelco', 'Zitlaltepec de Trinidad Sanchez Santos', 'Lazaro Cardenas', 'La Magdalena Tlaltelulco', 'San Damian Texoloc', 'San Francisco Tetlanohcan', 'San Jeronimo Zacualpan', 'San Jose Teacalco', 'San Juan Huactzinco', 'San Lorenzo Axocomanitla', 'San Lucas Tecopilco', 'Santa Ana Nopalucan', 'Santa Apolonia Teacalco', 'Santa Catarina Ayometla', 'Santa Cruz Quilehtla', 'Santa Isabel Xiloxoxtla', 'Otro');

estados['Tamaulipas'] = new Array('Abasolo', 'Aldama', 'Altamira', 'Antiguo Morelos', 'Burgos', 'Bustamante', 'Camargo', 'Casas', 'Ciudad Madero', 'Cruillas', 'Gomez Farias', 'Gonzalez', 'Güemez', 'Guerrero', 'Gustavo Diaz Ordaz', 'Hidalgo', 'Jaumave', 'Jimenez', 'Llera', 'Mainero', 'El Mante', 'Matamoros', 'Mendez', 'Mier', 'Miguel Aleman', 'Miquihuana', 'Nuevo Laredo', 'Nuevo Morelos', 'Ocampo', 'Padilla', 'Palmillas', 'Reynosa', 'Rio Bravo', 'San Carlos', 'San Fernando', 'San Nicolas', 'Soto la Marina', 'Tampico', 'Tula', 'Valle Hermoso', 'Victoria', 'Villagran', 'Xicotencatl', 'Otro');

estados['Tabasco'] = new Array('Balancan', 'Cardenas', 'Centla', 'Centro', 'Comalcalco', 'Cunduacan', 'Emiliano Zapata', 'Huimanguillo', 'Jalapa', 'Jalpa de Mendez', 'Jonuta', 'Macuspana', 'Nacajuca', 'Paraiso', 'Tacotalpa', 'Teapa', 'Tenosique', 'Otro');

estados['Sonora'] = new Array('Aconchi', 'Agua Prieta', 'alamos', 'Altar', 'Arivechi', 'Arizpe', 'Atil', 'Bacadehuachi', 'Bacanora', 'Bacerac', 'Bacoachi', 'Bacum', 'Banamichi', 'Baviacora', 'Bavispe', 'Benito Juarez', 'Benjamin Hill', 'Caborca', 'Cajeme', 'Cananea', 'Carbo', 'Cucurpe', 'Cumpas', 'Divisaderos', 'Empalme', 'Etchojoa', 'Fronteras', 'General Plutarco Elias Calles', 'Granados', 'Guaymas', 'Hermosillo', 'Huachinera', 'Huasabas', 'Huatabampo', 'Huepac', 'imuris', 'La Colorada', 'Magdalena', 'Mazatan', 'Moctezuma', 'Naco', 'Nacori Chico', 'Nacozari', 'Navojoa', 'Nogales', 'Onavas', 'Opodepe', 'Oquitoa', 'Pitiquito', 'Puerto Penasco', 'Quiriego', 'Rayon', 'Rosario', 'Sahuaripa', 'San Felipe de Jesus', 'San Ignacio Rio Muerto', 'San Javier', 'San Luis Rio Colorado', 'San Miguel de Horcasitas', 'San Pedro de la Cueva', 'Santa Ana', 'Santa Cruz', 'Saric', 'Soyopa', 'Suaqui Grande', 'Tepache', 'Trincheras', 'Tubutama', 'Ures', 'Villa Hidalgo', 'Villa Pesqueira', 'Yecora', 'Otro');

estados['Sinaloa'] = new Array('Ahome', 'Angostura', 'Badiraguato', 'Choix', 'Concordia', 'Cosala', 'Culiacan', 'El Fuerte', 'Elota', 'Escuinapa', 'Guasave', 'Mazatlan', 'Mocorito', 'Navolato', 'Rosario', 'Salvador Alvarado', 'San Ignacio', 'Sinaloa', 'Otro');

estados['San Luis Potosi'] = new Array('Ahualulco', 'Alaquines', 'Aquismon', 'Armadillo de los Infante', 'Axtla de Terrazas', 'Cardenas', 'Catorce', 'Cedral', 'Cerritos', 'Cerro de San Pedro', 'Charcas', 'Ciudad del Maiz', 'Ciudad Fernandez', 'Ciudad Valles', 'Coxcatlan', 'ebano', 'El Naranjo', 'Guadalcazar', 'Huehuetlan', 'Lagunillas', 'Matehuala', 'Matlapa', 'Mexquitic de Carmona', 'Moctezuma', 'Rayon', 'Rioverde', 'Salinas', 'San Antonio', 'San Ciro de Acosta', 'San Luis Potosi', 'San Martin Chalchicuautla', 'San Nicolas de Tolentino', 'San Vicente Tancuayalab', 'Santa Catarina', 'Santa Maria del Rio', 'Santo Domingo', 'Soledad de Graciano Sanchez', 'Tamasopo', 'Tamazunchale', 'Tampacan', 'Tampamolon', 'Tamuin', 'Tancahuitz', 'Tanlajas', 'Tanquian de Escobedo', 'Tierra Nueva', 'Vanegas', 'Venado', 'Villa de Arista', 'Villa de Arriaga', 'Villa de Guadalupe', 'Villa de la Paz', 'Villa de Ramos', 'Villa de Reyes', 'Villa Hidalgo', 'Villa Juarez', 'Xilitla', 'Zaragoza', 'Otro');

estados['Quintana Roo'] = new Array('Benito Juarez', 'Cozumel', 'Felipe Carrillo Puerto', 'Isla Mujeres', 'Jose Maria Morelos', 'Lazaro Cardenas', 'Othon P. Blanco', 'Solidaridad', 'Tulum', 'Otro');

estados['Queretaro'] = new Array('Amealco de Bonfil', 'Arroyo Seco', 'Cadereyta de Montes', 'Colon', 'Corregidora', 'El Marques', 'Ezequiel Montes', 'Huimilpan', 'Jalpan de Serra', 'Landa de Matamoros', 'Pedro Escobedo', 'Penamiller', 'Pinal de Amoles', 'Queretaro', 'San Joaquin', 'San Juan del Rio', 'Tequisquiapan', 'Toliman', 'Otro');

estados['Puebla'] = new Array('Acajete', 'Acateno', 'Acatlan de Osorio', 'Acatzingo', 'Acteopan', 'Ahuacatlan', 'Ahuatlan', 'Ahuazotepec', 'Ahuehuetitla', 'Ajalpan', 'Albino Zertuche', 'Aljojuca', 'Altepexi', 'Amixtlan', 'Amozoc', 'Aquixtla', 'Atempan', 'Atexcal', 'Atlequizayan', 'Atlixco', 'Atoyatempan', 'Atzala', 'Atzitzihuacan', 'Atzitzintla', 'Axutla', 'Ayotoxco de Guerrero', 'Calpan', 'Caltepec', 'Camocuautla', 'Canada Morelos', 'Caxhuacan', 'Chalchicomula de Sesma', 'Chapulco', 'Chiautla', 'Chiautzingo', 'Chichiquila', 'Chiconcuautla', 'Chietla', 'Chigmecatitlan', 'Chignahuapan', 'Chignautla', 'Chila de la Sal', 'Chila de las Flores', 'Chilchotla', 'Chinantla', 'Coatepec', 'Coatzingo', 'Cohetzala', 'Cohuecan', 'Coronango', 'Coxcatlan', 'Coyomeapan', 'Coyotepec', 'Cuapiaxtla de Madero', 'Cuautempan', 'Cuautinchan', 'Cuautlancingo', 'Cuayuca de Andrade', 'Cuetzalan del Progreso', 'Cuyoaco', 'Domingo Arenas', 'Eloxochitlan', 'Epatlan', 'Esperanza', 'Francisco Z. Mena', 'General Felipe angeles', 'Guadalupe', 'Guadalupe Victoria', 'Hermenegildo Galeana', 'Honey', 'Huaquechula', 'Huatlatlauca', 'Huauchinango', 'Huehuetla', 'Huehuetlan el Chico', 'Huehuetlan el Grande', 'Huejotzingo', 'Hueyapan', 'Hueytamalco', 'Hueytlalpan', 'Huitzilan de Serdan', 'Huitziltepec', 'Ixcamilpa de Guerrero', 'Ixcaquixtla', 'Ixtacamaxtitlan', 'Ixtepec', 'Izucar de Matamoros', 'Jalpan', 'Jolalpan', 'Jonotla', 'Jopala', 'Juan C. Bonilla', 'Juan Galindo', 'Juan N. Mendez', 'La Magdalena Tlatlauquitepec', 'Lafragua', 'Libres', 'Los Reyes de Juarez', 'Mazapiltepec de Juarez', 'Mixtla', 'Molcaxac', 'Naupan', 'Nauzontla', 'Nealtican', 'Nicolas Bravo', 'Nopalucan', 'Ocotepec', 'Ocoyucan', 'Olintla', 'Oriental', 'Pahuatlan', 'Palmar de Bravo', 'Pantepec', 'Petlalcingo', 'Piaxtla', 'Puebla de Zaragoza', 'Quecholac', 'Quimixtlan', 'Rafael Lara Grajales', 'San Andres Cholula', 'San Antonio Canada', 'San Diego La Mesa Tochimiltzingo', 'San Felipe Teotlalcingo', 'San Felipe Tepatlan', 'San Gabriel Chilac', 'San Gregorio Atzompa', 'San Jeronimo Tecuanipan', 'San Jeronimo Xayacatlan', 'San Jose Chiapa', 'San Jose Miahuatlan', 'San Juan Atenco', 'San Juan Atzompa', 'San Martin Texmelucan', 'San Martin Totoltepec', 'San Matias Tlalancaleca', 'San Miguel Ixitlan', 'San Miguel Xoxtla', 'San Nicolas Buenos Aires', 'San Nicolas de los Ranchos', 'San Pablo Anicano', 'San Pedro Cholula', 'San Pedro Yeloixtlahuaca', 'San Salvador el Seco', 'San Salvador el Verde', 'San Salvador Huixcolotla', 'San Sebastian Tlacotepec', 'Santa Catarina Tlaltempan', 'Santa Ines Ahuatempan', 'Santa Isabel Cholula', 'Santiago Miahuatlan', 'Santo Tomas Hueyotlipan', 'Soltepec', 'Tecali de Herrera', 'Tecamachalco', 'Tecomatlan', 'Tehuacan', 'Tehuitzingo', 'Tenampulco', 'Teopantlan', 'Teotlalco', 'Tepanco de Lopez', 'Tepango de Rodriguez', 'Tepatlaxco de Hidalgo', 'Tepeaca', 'Tepemaxalco', 'Tepeojuma', 'Tepetzintla', 'Tepexco', 'Tepexi de Rodriguez', 'Tepeyahualco', 'Tepeyahualco de Cuauhtemoc', 'Tetela de Ocampo', 'Teteles de avila Castillo', 'Teziutlan', 'Tianguismanalco', 'Tilapa', 'Tlachichuca', 'Tlacotepec de Benito Juarez', 'Tlacuilotepec', 'Tlahuapan', 'Tlalnepantla', 'Tlaltenango', 'Tlaola', 'Tlapacoya', 'Tlapanala', 'Tlatlauquitepec', 'Tlaxco', 'Tochimilco', 'Tochtepec', 'Totoltepec de Guerrero', 'Tulcingo', 'Tuzamapan de Galeana', 'Tzicatlacoyan', 'Venustiano Carranza', 'Vicente Guerrero', 'Xayacatlan de Bravo', 'Xicotepec', 'Xicotlan', 'Xiutetelco', 'Xochiapulco', 'Xochiltepec', 'Xochitlan de Vicente Suarez', 'Xochitlan Todos Santos', 'Yaonahuac', 'Yehualtepec', 'Zacapala', 'Zacapoaxtla', 'Zacatlan', 'Zapotitlan', 'Zapotitlan de Mendez', 'Zaragoza', 'Zautla', 'Zihuateutla', 'Zinacatepec', 'Zongozotla', 'Zoquiapan', 'Zoquitlan', 'Otro');

estados['Oaxaca'] = new Array('Abejones', 'Acatlan de Perez Figueroa', 'animas Trujano', 'Asuncion Cacalotepec', 'Asuncion Cuyotepeji', 'Asuncion Ixtaltepec', 'Asuncion Nochixtlan', 'Asuncion Ocotlan', 'Asuncion Tlacolulita', 'Ayoquezco de Aldama', 'Ayotzintepec', 'Calihuala', 'Candelaria Loxicha', 'Capulalpam de Mendez', 'Chahuites', 'Chalcatongo de Hidalgo', 'Chiquihuitlan de Benito Juarez', 'Cienega de Zimatlan', 'Ciudad Ixtepec', 'Coatecas Altas', 'Coicoyan de las Flores', 'Concepcion Buenavista', 'Concepcion Papalo', 'Constancia del Rosario', 'Cosolapa', 'Cosoltepec', 'Cuilapam de Guerrero', 'Cuyamecalco Villa de Zaragoza', 'Ejutla de Crespo', 'El Barrio de la Soledad', 'El Espinal', 'Eloxochitlan de Flores Magon', 'Fresnillo de Trujano', 'Guadalupe de Ramirez', 'Guadalupe Etla', 'Guelatao de Juarez', 'Guevea de Humboldt', 'Heroica Ciudad de Tlaxiaco', 'Huajuapan de Leon', 'Huautepec', 'Huautla de Jimenez', 'Ixpantepec Nieves', 'Ixtlan de Juarez', 'Juchitan de Zaragoza', 'La Compania', 'La Pe', 'La Reforma', 'La Trinidad Vista Hermosa', 'Loma Bonita', 'Magdalena Apasco', 'Magdalena Jaltepec', 'Magdalena Mixtepec', 'Magdalena Ocotlan', 'Magdalena Penasco', 'Magdalena Teitipac', 'Magdalena Tequisistlan', 'Magdalena Tlacotepec', 'Magdalena Yodocono de Porfirio Diaz', 'Magdalena Zahuatlan', 'Mariscala de Juarez', 'Martires de Tacubaya', 'Matias Romero', 'Mazatlan Villa de Flores', 'Mesones Hidalgo', 'Miahuatlan de Porfirio Diaz', 'Mixistlan de la Reforma', 'Monjas', 'Natividad', 'Nazareno Etla', 'Nejapa de Madero', 'Nuevo Zoquiapam', 'Oaxaca de Juarez', 'Ocotlan de Morelos', 'Pinotepa de Don Luis', 'Pluma Hidalgo', 'Putla Villa de Guerrero', 'Reforma de Pineda', 'Reyes Etla', 'Rojas de Cuauhtemoc', 'Salina Cruz', 'San Agustin Amatengo', 'San Agustin Atenango', 'San Agustin Chayuco', 'San Agustin de las Juntas', 'San Agustin Etla', 'San Agustin Loxicha', 'San Agustin Tlacotepec', 'San Agustin Yatareni', 'San Andres Cabecera Nueva', 'San Andres Dinicuiti', 'San Andres Huaxpaltepec', 'San Andres Huayapam', 'San Andres Ixtlahuaca', 'San Andres Lagunas', 'San Andres Nuxino', 'San Andres Paxtlan', 'San Andres Sinaxtla', 'San Andres Solaga', 'San Andres Teotilalpam', 'San Andres Tepetlapa', 'San Andres Yaa', 'San Andres Zabache', 'San Andres Zautla', 'San Antonino Castillo Velasco', 'San Antonino el Alto', 'San Antonino Monte Verde', 'San Antonio Acutla', 'San Antonio de la Cal', 'San Antonio Huitepec', 'San Antonio Nanahuatipam', 'San Antonio Sinicahua', 'San Antonio Tepetlapa', 'San Baltazar Chichicapam', 'San Baltazar Loxicha', 'San Baltazar Yatzachi el Bajo', 'San Bartolo Coyotepec', 'San Bartolo Soyaltepec', 'San Bartolo Yautepec', 'San Bartolome Ayautla', 'San Bartolome Loxicha', 'San Bartolome Quialana', 'San Bartolome Yucuane', 'San Bartolome Zoogocho', 'San Bernardo Mixtepec', 'San Blas Atempa', 'San Carlos Yautepec', 'San Cristobal Amatlan', 'San Cristobal Amoltepec', 'San Cristobal Lachirioag', 'San Cristobal Suchixtlahuaca', 'San Dionisio del Mar', 'San Dionisio Ocotepec', 'San Dionisio Ocotlan', 'San Esteban Atatlahuca', 'San Felipe Jalapa de Diaz', 'San Felipe Tejalapam', 'San Felipe Usila', 'San Francisco Cahuacua', 'San Francisco Cajonos', 'San Francisco Chapulapa', 'San Francisco Chindua', 'San Francisco del Mar', 'San Francisco Huehuetlan', 'San Francisco Ixhuatan', 'San Francisco Jaltepetongo', 'San Francisco Lachigolo', 'San Francisco Logueche', 'San Francisco Nuxano', 'San Francisco Ozolotepec', 'San Francisco Sola', 'San Francisco Telixtlahuaca', 'San Francisco Teopan', 'San Francisco Tlapancingo', 'San Gabriel Mixtepec', 'San Ildefonso Amatlan', 'San Ildefonso Sola', 'San Ildefonso Villa Alta', 'San Jacinto Amilpas', 'San Jacinto Tlacotepec', 'San Jeronimo Coatlan', 'San Jeronimo Silacayoapilla', 'San Jeronimo Sosola', 'San Jeronimo Taviche', 'San Jeronimo Tecoatl', 'San Jeronimo Tlacochahuaya', 'San Jorge Nuchita', 'San Jose Ayuquila', 'San Jose Chiltepec', 'San Jose del Penasco', 'San Jose del Progreso', 'San Jose Estancia Grande', 'San Jose Independencia', 'San Jose Lachiguiri', 'San Jose Tenango', 'San Juan Achiutla', 'San Juan Atepec', 'San Juan Bautista Atatlahuca', 'San Juan Bautista Coixtlahuaca', 'San Juan Bautista Cuicatlan', 'San Juan Bautista Guelache', 'San Juan Bautista Jayacatlan', 'San Juan Bautista lo de Soto', 'San Juan Bautista Suchitepec', 'San Juan Bautista Tlachichilco', 'San Juan Bautista Tlacoatzintepec', 'San Juan Bautista Tuxtepec', 'San Juan Bautista Valle Nacional', 'San Juan Cacahuatepec', 'San Juan Chicomezuchil', 'San Juan Chilateca', 'San Juan Cieneguilla', 'San Juan Coatzospam', 'San Juan Colorado', 'San Juan Comaltepec', 'San Juan Cotzocon', 'San Juan de los Cues', 'San Juan del Estado', 'San Juan del Rio', 'San Juan Diuxi', 'San Juan Evangelista Analco', 'San Juan Guelavia', 'San Juan Guichicovi', 'San Juan Ihualtepec', 'San Juan Juquila Mixes', 'San Juan Juquila Vijanos', 'San Juan Lachao', 'San Juan Lachigalla', 'San Juan Lajarcia', 'San Juan Lalana', 'San Juan Mazatlan', 'San Juan Mixtepec Distrito 08', 'San Juan Mixtepec Distrito 26', 'San Juan Ozolotepec', 'San Juan Petlapa', 'San Juan Quiahije', 'San Juan Quiotepec', 'San Juan Sayultepec', 'San Juan Tabaa', 'San Juan Tamazola', 'San Juan Teita', 'San Juan Teitipac', 'San Juan Tepeuxila', 'San Juan Teposcolula', 'San Juan Yaee', 'San Juan Yatzona', 'San Juan Yucuita', 'San Juan Yumi', 'San Lorenzo', 'San Lorenzo Albarradas', 'San Lorenzo Cacaotepec', 'San Lorenzo Cuaunecuiltitla', 'San Lorenzo Texmelucan', 'San Lorenzo Victoria', 'San Lucas Camotlan', 'San Lucas Ojitlan', 'San Lucas Quiavini', 'San Lucas Zoquiapam', 'San Luis Amatlan', 'San Marcial Ozolotepec', 'San Marcos Arteaga', 'San Martin de los Cansecos', 'San Martin Huamelulpam', 'San Martin Itunyoso', 'San Martin Lachila', 'San Martin Peras', 'San Martin Tilcajete', 'San Martin Toxpalan', 'San Martin Zacatepec', 'San Mateo Cajonos', 'San Mateo del Mar', 'San Mateo Etlatongo', 'San Mateo Nejapam', 'San Mateo Penasco', 'San Mateo Pinas', 'San Mateo Rio Hondo', 'San Mateo Sindihui', 'San Mateo Tlapiltepec', 'San Mateo Yoloxochitlan', 'San Melchor Betaza', 'San Miguel Achiutla', 'San Miguel Ahuehuetitlan', 'San Miguel Aloapam', 'San Miguel Amatitlan', 'San Miguel Amatlan', 'San Miguel Chicahua', 'San Miguel Chimalapa', 'San Miguel Coatlan', 'San Miguel del Puerto', 'San Miguel del Rio', 'San Miguel Ejutla', 'San Miguel el Grande', 'San Miguel Huautla', 'San Miguel Mixtepec', 'San Miguel Panixtlahuaca', 'San Miguel Peras', 'San Miguel Piedras', 'San Miguel Quetzaltepec', 'San Miguel Santa Flor', 'San Miguel Soyaltepec', 'San Miguel Suchixtepec', 'San Miguel Tecomatlan', 'San Miguel Tenango', 'San Miguel Tequixtepec', 'San Miguel Tilquiapam', 'San Miguel Tlacamama', 'San Miguel Tlacotepec', 'San Miguel Tulancingo', 'San Miguel Yotao', 'San Nicolas', 'San Nicolas Hidalgo', 'San Pablo Coatlan', 'San Pablo Cuatro Venados', 'San Pablo Etla', 'San Pablo Huitzo', 'San Pablo Huixtepec', 'San Pablo Macuiltianguis', 'San Pablo Tijaltepec', 'San Pablo Villa de Mitla', 'San Pablo Yaganiza', 'San Pedro Amuzgos', 'San Pedro Amuzgos', 'San Pedro Atoyac', 'San Pedro Cajonos', 'San Pedro Comitancillo', 'San Pedro Coxcaltepec Cantaros', 'San Pedro el Alto', 'San Pedro Huamelula', 'San Pedro Huilotepec', 'San Pedro Ixcatlan', 'San Pedro Ixtlahuaca', 'San Pedro Jaltepetongo', 'San Pedro Jicayan', 'San Pedro Jocotipac', 'San Pedro Juchatengo', 'San Pedro Martir', 'San Pedro Martir Quiechapa', 'San Pedro Martir Yucuxaco', 'San Pedro Mixtepec - Distrito 22 -', 'San Pedro Mixtepec - Distrito 26 -', 'San Pedro Molinos', 'San Pedro Nopala', 'San Pedro Ocopetatillo', 'San Pedro Ocotepec', 'San Pedro Pochutla', 'San Pedro Quiatoni', 'San Pedro Sochiapam', 'San Pedro Tapanatepec', 'San Pedro Taviche', 'San Pedro Teozacoalco', 'San Pedro Teutila', 'San Pedro Tidaa', 'San Pedro Topiltepec', 'San Pedro Totolapa', 'San Pedro y San Pablo Ayutla', 'San Pedro y San Pablo Teposcolula', 'San Pedro y San Pablo Tequixtepec', 'San Pedro Yaneri', 'San Pedro Yolox', 'San Pedro Yucunama', 'San Raymundo Jalpan', 'San Sebastian Abasolo', 'San Sebastian Coatlan', 'San Sebastian Ixcapa', 'San Sebastian Nicananduta', 'San Sebastian Rio Hondo', 'San Sebastian Tecomaxtlahuaca', 'San Sebastian Teitipac', 'San Sebastian Tutla', 'San Simon Almolongas', 'San Simon Zahuatlan', 'San Vicente Coatlan', 'San Vicente Lachixio', 'San Vicente Nunu', 'Santa Ana', 'Santa Ana Ateixtlahuaca', 'Santa Ana Cuauhtemoc', 'Santa Ana del Valle', 'Santa Ana Tavela', 'Santa Ana Tlapacoyan', 'Santa Ana Yareni', 'Santa Ana Zegache', 'Santa Catalina Quieri', 'Santa Catarina Cuixtla', 'Santa Catarina Ixtepeji', 'Santa Catarina Juquila', 'Santa Catarina Lachatao', 'Santa Catarina Loxicha', 'Santa Catarina Mechoacan', 'Santa Catarina Minas', 'Santa Catarina Quiane', 'Santa Catarina Quioquitani', 'Santa Catarina Tayata', 'Santa Catarina Ticua', 'Santa Catarina Yosonotu', 'Santa Catarina Zapoquila', 'Santa Cruz Acatepec', 'Santa Cruz Amilpas', 'Santa Cruz de Bravo', 'Santa Cruz Itundujia', 'Santa Cruz Mixtepec', 'Santa Cruz Nundaco', 'Santa Cruz Papalutla', 'Santa Cruz Tacache de Mina', 'Santa Cruz Tacahua', 'Santa Cruz Tayata', 'Santa Cruz Xitla', 'Santa Cruz Xoxocotlan', 'Santa Cruz Zenzontepec', 'Santa Gertrudis', 'Santa Ines de Zaragoza', 'Santa Ines del Monte', 'Santa Ines Yatzeche', 'Santa Lucia del Camino', 'Santa Lucia Miahuatlan', 'Santa Lucia Monteverde', 'Santa Lucia Ocotlan', 'Santa Magdalena Jicotlan', 'Santa Maria Alotepec', 'Santa Maria Apazco', 'Santa Maria Atzompa', 'Santa Maria Camotlan', 'Santa Maria Chachoapam', 'Santa Maria Chilchotla', 'Santa Maria Chimalapa', 'Santa Maria Colotepec', 'Santa Maria Cortijo', 'Santa Maria Coyotepec', 'Santa Maria del Rosario', 'Santa Maria del Tule', 'Santa Maria Ecatepec', 'Santa Maria Guelace', 'Santa Maria Guienagati', 'Santa Maria Huatulco', 'Santa Maria Huazolotitlan', 'Santa Maria Ipalapa', 'Santa Maria Ixcatlan', 'Santa Maria Jacatepec', 'Santa Maria Jalapa del Marques', 'Santa Maria Jaltianguis', 'Santa Maria la Asuncion', 'Santa Maria Lachixio', 'Santa Maria Mixtequilla', 'Santa Maria Nativitas', 'Santa Maria Nduayaco', 'Santa Maria Ozolotepec', 'Santa Maria Papalo', 'Santa Maria Penoles', 'Santa Maria Petapa', 'Santa Maria Quiegolani', 'Santa Maria Sola', 'Santa Maria Tataltepec', 'Santa Maria Tecomavaca', 'Santa Maria Temaxcalapa', 'Santa Maria Temaxcaltepec', 'Santa Maria Teopoxco', 'Santa Maria Tepantlali', 'Santa Maria Texcatitlan', 'Santa Maria Tlahuitoltepec', 'Santa Maria Tlalixtac', 'Santa Maria Tonameca', 'Santa Maria Totolapilla', 'Santa Maria Xadani', 'Santa Maria Yalina', 'Santa Maria Yavesia', 'Santa Maria Yolotepec', 'Santa Maria Yosoyua', 'Santa Maria Yucuhiti', 'Santa Maria Zacatepec', 'Santa Maria Zaniza', 'Santa Maria Zoquitlan', 'Santiago Amoltepec', 'Santiago Apoala', 'Santiago Apostol', 'Santiago Astata', 'Santiago Atitlan', 'Santiago Ayuquililla', 'Santiago Cacaloxtepec', 'Santiago Camotlan', 'Santiago Chazumba', 'Santiago Choapam', 'Santiago Comaltepec', 'Santiago del Rio', 'Santiago Huajolotitlan', 'Santiago Huauclilla', 'Santiago Ihuitlan Plumas', 'Santiago Ixcuintepec', 'Santiago Ixtayutla', 'Santiago Jamiltepec', 'Santiago Jocotepec', 'Santiago Juxtlahuaca', 'Santiago Lachiguiri', 'Santiago Lalopa', 'Santiago Laollaga', 'Santiago Laxopa', 'Santiago Llano Grande', 'Santiago Matatlan', 'Santiago Miltepec', 'Santiago Minas', 'Santiago Nacaltepec', 'Santiago Nejapilla', 'Santiago Niltepec', 'Santiago Nundiche', 'Santiago Nuyoo', 'Santiago Pinotepa Nacional', 'Santiago Suchilquitongo', 'Santiago Tamazola', 'Santiago Tapextla', 'Santiago Tenango', 'Santiago Tepetlapa', 'Santiago Tetepec', 'Santiago Texcalcingo', 'Santiago Textitlan', 'Santiago Tilantongo', 'Santiago Tillo', 'Santiago Tlazoyaltepec', 'Santiago Xanica', 'Santiago Xiacui', 'Santiago Yaitepec', 'Santiago Yaveo', 'Santiago Yolomecatl', 'Santiago Yosondua', 'Santiago Yucuyachi', 'Santiago Zacatepec', 'Santiago Zoochila', 'Santo Domingo Albarradas', 'Santo Domingo Armenta', 'Santo Domingo Chihuitan', 'Santo Domingo de Morelos', 'Santo Domingo Ingenio', 'Santo Domingo Ixcatlan', 'Santo Domingo Nuxaa', 'Santo Domingo Ozolotepec', 'Santo Domingo Petapa', 'Santo Domingo Roayaga', 'Santo Domingo Tehuantepec', 'Santo Domingo Teojomulco', 'Santo Domingo Tepuxtepec', 'Santo Domingo Tlatayapam', 'Santo Domingo Tomaltepec', 'Santo Domingo Tonala', 'Santo Domingo Tonaltepec', 'Santo Domingo Xagacia', 'Santo Domingo Yanhuitlan', 'Santo Domingo Yodohino', 'Santo Domingo Zanatepec', 'Santo Tomas Jalieza', 'Santo Tomas Mazaltepec', 'Santo Tomas Ocotepec', 'Santo Tomas Tamazulapan', 'Santos Reyes Nopala', 'Santos Reyes Papalo', 'Santos Reyes Tepejillo', 'Santos Reyes Yucuna', 'Silacayoapam', 'Sitio de Xitlapehua', 'Soledad Etla', 'Tamazulapam del Espiritu Santo', 'Tanetze de Zaragoza', 'Taniche', 'Tataltepec de Valdes', 'Teococuilco de Marcos Perez', 'Teotitlan de Flores Magon', 'Teotitlan del Valle', 'Teotongo', 'Tepelmeme Villa de Morelos', 'Tezoatlan de Segura y Luna', 'Tlacolula de Matamoros', 'Tlacotepec Plumas', 'Tlalixtac de Cabrera', 'Totontepec Villa de Morelos', 'Trinidad Zaachila', 'Union Hidalgo', 'Valerio Trujano', 'Villa de Chilapa de Diaz', 'Villa de Etla', 'Villa de Tamazulapam del Progreso', 'Villa de Tututepec de Melchor Ocampo', 'Villa de Zaachila', 'Villa Diaz Ordaz', 'Villa Hidalgo', 'Villa Sola de Vega', 'Villa Talea de Castro', 'Villa Tejupam de la Union', 'Yaxe', 'Yogana', 'Yutanduchi de Guerrero', 'Zapotitlan del Rio', 'Zapotitlan Lagunas', 'Zapotitlan Palmas', 'Zimatlan de Alvarez', 'Otro');

estados['Nuevo Leon'] = new Array('Abasolo', 'Agualeguas', 'Allende', 'Anahuac', 'Apodaca', 'Aramberri', 'Bustamante', 'Cadereyta Jimenez', 'Carmen', 'Cerralvo', 'China', 'Cienega de Flores', 'Doctor Arroyo', 'Doctor Coss', 'Doctor Gonzalez', 'Galeana', 'Garcia', 'General Bravo', 'General Escobedo', 'General Teran', 'General Trevino', 'General Zaragoza', 'General Zuazua', 'Guadalupe', 'Hidalgo', 'Higueras', 'Hualahuises', 'Iturbide', 'Juarez', 'Lampazos de Naranjo', 'Linares', 'Los Aldamas', 'Los Herreras', 'Los Ramones', 'Marin', 'Melchor Ocampo', 'Mier y Noriega', 'Mina', 'Montemorelos', 'Monterrey', 'Paras', 'Pesqueria', 'Rayones', 'Sabinas Hidalgo', 'Salinas Victoria', 'San Nicolas de los Garza', 'San Pedro Garza Garcia', 'Santa Catarina', 'Santiago', 'Vallecillo', 'Villaldama', 'Otro');


estados['Nayarit'] = new Array('Acaponeta', 'Ahuacatlan', 'Amatlan de Canas', 'Bahia de Banderas', 'Compostela', 'Del Nayar', 'Huajicori', 'Ixtlan del Rio', 'Jala', 'La Yesca', 'Rosamorada', 'Ruiz', 'San Blas', 'San Pedro Lagunillas', 'Santa Maria del Oro', 'Santiago Ixcuintla', 'Tecuala', 'Tepic', 'Tuxpan', 'Xalisco', 'Otro');

estados['Morelos'] = new Array('Amacuzac', 'Atlatlahucan', 'Axoyiapan', 'Ayala', 'Coatlan del Rio', 'Cuatla', 'Cuernavaca', 'Emiliano Zapata', 'Huitzilac', 'Jantetelco', 'Jiutepec', 'Jojutla', 'Jonacatepec', 'Mazatepec', 'Miacatlan', 'Ocuituco', 'Puente de Ixtla', 'Temixco', 'Temoac', 'Tepalcingo', 'Tepoztlan', 'Tetecala', 'Tetela del Volcan', 'Tlalnepantla', 'Tlaltizapan', 'Tlaquiltenango', 'Tlayacapan', 'Totolapan', 'Xochitepec', 'Yautepec', 'Yecapixtla', 'Zacatepec de Hidalgo', 'Zacualpan de Amilpas', 'Otro');

estados['Michoacan'] = new Array('Acuitzio', 'Aguililla', 'alvaro Obregon', 'Angamacutiro', 'Angangueo', 'Apatzingan', 'Aporo', 'Aquila', 'Ario', 'Arteaga', 'Brisenas', 'Buenavista', 'Caracuaro', 'Chaparan', 'Charo', 'Chavinda', 'Cheran', 'Chilchota', 'Chinicuila', 'Chirintzio', 'Chucandiro', 'Churumuco', 'Coahuayana', 'Coalcoman de Vazquez Pallares', 'Coeneo', 'Cojumatlan de Regules', 'Contepec', 'Copandaro', 'Cotija', 'Cuitzeo', 'Ecuandureo', 'Epitacio Huerta', 'Erongaricuaro', 'Gabriel Zamora', 'Hidalgo', 'Huandacareo', 'Huaniqueo', 'Huetamo', 'Huiramba', 'Indaparapeo', 'Irimbo', 'Ixtlan', 'Jacona', 'Jimenez', 'Jiquilpan', 'Jose Sixto Verduzco', 'Juarez', 'Jungapeo', 'La Huacana', 'La Piedad', 'Lagunillas', 'Lazaro Cardenas', 'Los Reyes', 'Madero', 'Maravatio', 'Marcos Castellanos', 'Morelia', 'Morelos', 'Mugica', 'Nahuatzen', 'Nocupetaro', 'Nuevo Parangaricutiro', 'Nuevo Urecho', 'Numaran', 'Ocampo', 'Pajacuaran', 'Panindicuaro', 'Paracho', 'Paracuaro', 'Patzcuaro', 'Penjamillo', 'Periban', 'Purepero', 'Puruandiro', 'Querendaro', 'Quiroga', 'Salvador Escalante', 'San Lucas', 'Santa Ana Maya', 'Senguio', 'Sahuayo', 'Susupuato', 'Tacambaro', 'Tancitaro', 'Tangamandapio', 'Tangancicuaro', 'Tanhuato', 'Taretan', 'Tarimbaro', 'Tepalcatepec', 'Tingambato', 'Tingüindin', 'Tiquicheo de Nicolas Romero', 'Tlalpujahua', 'Tlazazalca', 'Tocumbo', 'Tumbiscatio', 'Turicato', 'Tuxpan', 'Tuzantla', 'Tzintzuntzan', 'Tzitzio', 'Uruapan', 'Venustiano Carranza', 'Villamar', 'Vista Hermosa', 'Yurecuaro', 'Zacapu', 'Zamora', 'Zinaparo', 'Zinapecuaro', 'Ziracuaretiro', 'Zitacuaro', 'Otro');

estados['Estado de Mexico'] = new Array('Acambay', 'Acolman', 'Aculco', 'Almoloya de Alquisiras', 'Almoloya de Juarez', 'Almoloya del Rio', 'Amanalco', 'Amatepec', 'Amemeca', 'Apaxco', 'Atenco', 'Atizapan', 'Atizapan de Zaragoza', 'Atlacomulco', 'Atlautla', 'Axapusco', 'Ayapango', 'Calimaya', 'Capulhuac', 'Chalco', 'Chapa de Mota', 'Chapultepec', 'Chiautla', 'Chicoloapan', 'Chiconcuac', 'Chimalhuacan', 'Coacalco de Berriozabal', 'Coatepec Harinas', 'Cocotitlan', 'Coyotepec', 'Cuautitlan', 'Cuautitlan Izcalli', 'Donato Guerra', 'Ecatepec de Morelos', 'Ecatzingo', 'El Oro', 'Huehuetoca', 'Hueypoxtla', 'Huixquilucan', 'Isidro Fabela', 'Ixtapaluca', 'Ixtapan de la Sal', 'Ixtapan del Oro', 'Ixtlahuaca', 'Jaltenco', 'Jilotepec', 'Jilotzingo', 'Jiquipilco', 'Jocotitlan', 'Joquicingo', 'Juchitepec', 'La Paz', 'Lerma', 'Luvianos', 'Malinalco', 'Melchor Ocampo', 'Metepec', 'Mexicaltzingo', 'Morelos', 'Naucalpan de Juarez', 'Nextlalpan', 'Nezahualcoyotl', 'Nicolas Romero', 'Nopaltepec', 'Ocoyoacac', 'Ocuilan', 'Otumba', 'Otzoloapan', 'Otzolotepec', 'Ozumba', 'Papalotla', 'Polotitlan', 'Rayon', 'San Antonio la Isla', 'San Felipe del Progreso', 'San Jose del Rincon', 'San Martin de las Piramides', 'San Mateo Atenco', 'San Simon de Guerrero', 'Santo Tomas', 'Soyaniquilpan de Juarez', 'Sultepec', 'Tecamac', 'Tejupilco', 'Temamatla', 'Temascalapa', 'Temascalcingo', 'Temascaltepec', 'Temoaya', 'Tenancingo', 'Tenango del Aire', 'Tenango del Valle', 'Teoloyucan', 'Teotihuacan', 'Tepetlaoxtoc', 'Tepetlixpa', 'Tepotzotlan', 'Tequixquiac', 'Texcaltitlan', 'Texcalyacac', 'Texcoco', 'Tezoyuca', 'Tianguistenco', 'Timilpan', 'Tlalmanalco', 'Tlalnepantla de Baz', 'Tlatlaya', 'Toluca', 'Tonanitla', 'Tonatico', 'Tultepec', 'Tultitlan', 'Valle de Bravo', 'Valle de Chalco Solidaridad', 'Villa de Allende', 'Villa del Carbon', 'Villa Guerrero', 'Villa Victoria', 'Xalatlaco', 'Xonacatlan', 'Zacazonapan', 'Zacualpan', 'Zinacantepec', 'Zumpahuacan', 'Zumpango', 'Otro');

estados['Jalisco'] = new Array('Guadalajara', 'Tlajomulco de Zuniga', 'Tlaquepaque', 'Tonala', 'Zapopan', 'Acatic', 'Acatlan de Juarez', 'Ahualulco de Mercado', 'Amacueca', 'Amatitan', 'Ameca', 'Arandas', 'Atemajac de Brizuela', 'Atengo', 'Atenguillo', 'Atotonilco el Alto', 'Atoyac', 'Autlan de Navarro', 'Ayotlan', 'Ayutla', 'Bolanos', 'Cabo Corrientes', 'Canadas de Obregon', 'Capilla de Guadalupe', 'Casimiro Castillo', 'Chapala', 'Chimaltitan', 'Chiquilistlan', 'Cihuatlan', 'Cocula', 'Colotlan', 'Concepcion de Buenos Aires', 'Cuautitlan de Garcia Barragan', 'Cuautla', 'Cuquio', 'Degollado', 'Ejutla', 'El Arenal', 'El Grullo', 'El Limon', 'El Salto', 'Encarnacion de Diaz', 'Etzatlan', 'Gomez Farias', 'Guachinango', 'Hostotipaquillo', 'Huejucar', 'Huejuquilla el Alto', 'Ixtlahuacan de los Membrillos', 'Ixtlahuacan del Rio', 'Jalostotitlan', 'Jamay', 'Jesus Maria', 'Jilotlan de los Dolores', 'Jocotepec', 'Juanacatlan', 'Juchitlan', 'La Barca', 'La Huerta', 'La Manzanilla de la Paz', 'Lagos de Moreno', 'Magdalena', 'Mascota', 'Mazamitla', 'Mexticacan', 'Mezquitic', 'Mixtlan', 'Ocotlan', 'Ojuelos de Jalisco', 'Pihuamo', 'Poncitlan', 'Puerto Vallarta', 'Quitupan', 'San Cristobal de la Barranca', 'San Diego de Alejandria', 'San Gabriel', 'San Ignacio Cerro Gordo', 'San Juan de los Lagos', 'San Juanito de Escobedo', 'San Julian', 'San Marcos', 'San Martin de Bolanos', 'San Martin de Hidalgo', 'San Miguel el Alto', 'San Sebastian del Oeste', 'Santa Maria de los angeles', 'Santa Maria del Oro', 'Sayula', 'Tala', 'Talpa de Allende', 'Tamazula de Gordiano', 'Tapalpa', 'Tecalitlan', 'Techaluta de Montenegro', 'Tecolotlan', 'Tenamaxtlan', 'Teocaltiche', 'Teocuitatlan de Corona', 'Tepatitlan de Morelos', 'Tequila', 'Teuchitlan', 'Tizapan el Alto', 'Toliman', 'Tomatlan', 'Tonaya', 'Tonila', 'Totatiche', 'Tototlan', 'Tuxcacuesco', 'Tuxcueca', 'Tuxpan', 'Union de San Antonio', 'Union de Tula', 'Valle de Guadalupe', 'Valle de Juarez', 'Villa Corona', 'Villa Guerrero', 'Villa Hidalgo', 'Villa Purificacion', 'Yahualica de Gonzalez Gallo', 'Zacoalco de Torres', 'Zapotiltic', 'Zapotitlan de Vadillo', 'Zapotlan del Rey', 'Zapotlan El Grande', 'Zapotlanejo', 'Otro');

estados['Hidalgo'] = new Array('Acatlan', 'Acaxochitlan', 'Actopan', 'Agua Blanca de Iturbide', 'Ajacuba', 'Alfajayucan', 'Almoloya', 'Apan', 'Atitalaquia', 'Atlapexco', 'Atotonilco de Tula', 'Atotonilco el Grande', 'Calnali', 'Cardonal', 'Chapantongo', 'Chapulhuacan', 'Chilcuautla', 'Cuautepec de Hinojosa', 'El Arenal', 'Eloxochitlan', 'Emiliano Zapata', 'Epazoyucan', 'Francisco I. Madero', 'Huasca de Ocampo', 'Huautla', 'Huazalingo', 'Huehuetla', 'Huejutla de Reyes', 'Huichapan', 'Ixmiquilpan', 'Jacala de Ledezma', 'Jaltocan', 'Juarez Hidalgo', 'La Mision', 'Lolotla', 'Metepec', 'Metztitlan', 'Mineral de la Reforma', 'Mineral del Chico', 'Mineral del Monte', 'Mixquiahuala de Juarez', 'Molango de Escamilla', 'Nicolas Flores', 'Nopala de Villagran', 'Omitlan de Juarez', 'Pachuca de Soto', 'Pacula', 'Pisaflores', 'Progreso de Obregon', 'San Agustin Metzquititlan', 'San Agustin Tlaxiaca', 'San Bartolo Tutotepec', 'San Felipe Orizatlan', 'San Salvador', 'Santiago de Anaya', 'Santiago Tulantepec de Lugo Guerrero', 'Singuilucan', 'Tasquillo', 'Tecozautla', 'Tenango de Doria', 'Tepeapulco', 'Tepehuacan de Guerrero', 'Tepeji del Rio de Ocampo', 'Tepetitlan', 'Tetepango', 'Tezontepec de Aldama', 'Tianguistengo', 'Tizayuca', 'Tlahuelilpan', 'Tlahuiltepa', 'Tlanalapa', 'Tlanchinol', 'Tlaxcoapan', 'Tolcayuca', 'Tula de Allende', 'Tulancingo de Bravo', 'Villa de Tezontepec', 'Xochiatipan', 'Xochicoatlan', 'Yahualica', 'Zacualtipan de Angeles', 'Zapotlan de Juarez', 'Zempoala', 'Zimapan', 'Otro');

estados['Guerrero'] = new Array('Acapulco de Juarez', 'Acatepec', 'Ahuacuotzingo', 'Ajuchitlan del Progreso', 'Alcozauca de Guerrero', 'Alpoyeca', 'Apaxtla', 'Arcelia', 'Atenango del Rio', 'Atlamajalcingo del Monte', 'Atlixtac', 'Atoyac de alvarez', 'Ayutla de los Libres', 'Azoyu', 'Benito Juarez', 'Buenavista de Cuellar', 'Chilapa de alvarez', 'Chilpancingo de los Bravo', 'Coahuayutla de Jose Maria Izazaga', 'Cochoapa el Grande', 'Cocula', 'Copala', 'Copalillo', 'Copanatoyac', 'Coyuca de Benitez', 'Coyuca de Catalan', 'Cuajinicuilapa', 'Cualac', 'Cuautepec', 'Cuetzala del Progreso', 'Cutzamala de Pinzon', 'Eduardo Neri', 'Florencio Villarreal', 'General Canuto A. Neri', 'General Heliodoro Castillo', 'Huamuxtitlan', 'Huitzuco de los Figueroa', 'Iguala de la Independencia', 'Igualapa', 'Iliatenco', 'Ixcateopan de Cuauhtemoc', 'Jose Azueta', 'Jose Joaquin Herrera', 'Juan R. Escudero', 'Juchitan', 'La Union de Isidoro Montes de Oca', 'Leonardo Bravo', 'Malinaltepec', 'Marquelia', 'Martir de Cuilapan', 'Metlatonoc', 'Mochitlan', 'Olinala', 'Ometepec', 'Pedro Ascencio Alquisiras', 'Petatlan', 'Pilcaya', 'Pungarabato', 'Quechultenango', 'San Luis Acatlan', 'San Marcos', 'San Miguel Totolapan', 'Taxco de Alarcon', 'Tecoanapa', 'Tecpan de Galeana', 'Teloloapan', 'Tepecoacuilco de Trujano', 'Tetipac', 'Tixtla de Guerrero', 'Tlacoachistlahuaca', 'Tlacoapa', 'Tlalchapa', 'Tlalixtaquilla de Maldonado', 'Tlapa de Comonfort', 'Tlapehuala', 'Xalpatlahuac', 'Xochihuehuetlan', 'Xochistlahuaca', 'Zapotitlan Tablas', 'Zirandaro', 'Zitlala', 'Otro');

estados['Guanajuato'] = new Array('Abasolo', 'Acambaro', 'Allende', 'Apaseo el Alto', 'Apaseo el Grande', 'Atarjea', 'Celaya', 'Comonfort', 'Coroneo', 'Cortazar', 'Cueramaro', 'Doctor Mora', 'Dolores Hidalgo', 'Guanajuato', 'Huanimaro', 'Irapuato', 'Jaral del Progreso', 'Jerecuaro', 'Leon', 'Manuel Doblado', 'Moroleon', 'Ocampo', 'Penjamo', 'Pueblo Nuevo', 'Purisima del Rincon', 'Romita', 'Salamanca', 'Salvatierra', 'San Diego de la Union', 'San Felipe', 'San Francisco del Rincon', 'San Jose Iturbide', 'San Luis de la Paz', 'Santa Catarina', 'Santa Cruz de Juventino Rosas', 'Santiago Maravatio', 'Silao', 'Tarandacuao', 'Tarimoro', 'Tierra Blanca', 'Uriangato', 'Valle de Santiago', 'Victoria', 'Villagran', 'Xichu', 'Yuriria', 'Otro');

estados['Durango'] = new Array('Canatlan', 'Canelas', 'Coneto de Comonfort', 'Cuencame', 'Durango', 'El Oro', 'General Simon Bolivar', 'Gomez Palacio', 'Guadalupe Victoria', 'Guanacevi', 'Hidalgo', 'Inde', 'Lerdo', 'Mapimi', 'Mezquital', 'Nazas', 'Nombre de Dios', 'Nuevo Ideal', 'Ocampo', 'Otaez', 'Panuco de Coronado', 'Penon Blanco', 'Poanas', 'Pueblo Nuevo', 'Rodeo', 'San Bernardo', 'San Dimas', 'San Juan de Guadalupe', 'San Juan del Rio', 'San Luis del Cordero', 'San Pedro del Gallo', 'Santa Clara', 'Santiago Papasquiaro', 'Suchil', 'Tamazula', 'Tepehuanes', 'Tlahualilo', 'Topia', 'Vicente Guerrero', 'Otro');

estados['Colima'] = new Array('Armeria', 'Colima', 'Comala', 'Coquimatlan', 'Cuauhtemoc', 'Ixtlahuacan', 'Manzanillo', 'Minatitlan', 'Tecoman', 'Villa de alvarez', 'Otro');

estados['Coahuila'] = new Array('Abasolo', 'Acuna', 'Allende', 'Arteaga', 'Candela', 'Castanos', 'Cuatrocienegas', 'Escobedo', 'Francisco I. Madero', 'Frontera', 'General Cepeda', 'Guerrero', 'Hidalgo', 'Jimenez', 'Juarez', 'Lamadrid', 'Matamoros', 'Monclova', 'Morelos', 'Muzquiz', 'Nadadores', 'Nava', 'Ocampo', 'Parras', 'Piedras Negras', 'Progreso', 'Ramos Arizpe', 'Sabinas', 'Sacramento', 'Saltillo', 'San Buenaventura', 'San Juan de Sabinas', 'San Pedro', 'Sierra Mojada', 'Torreon', 'Viesca', 'Villa Union', 'Zaragoza', 'Otro');

estados['Chihuahua'] = new Array('Ahumada', 'Aldama', 'Allende', 'Aquiles Serdan', 'Ascension', 'Bachiniva', 'Balleza', 'Batopilas', 'Bocoyna', 'Buenaventura', 'Camargo', 'Carichi', 'Casas Grandes', 'Chihuahua', 'Chinipas', 'Coronado', 'Coyame del Sotol', 'Cuauhtemoc', 'Cusihuiriachi', 'Delicias', 'Dr. Belisario Dominguez', 'El Tule', 'Galeana', 'Gomez Farias', 'Gran Morelos', 'Guachochi', 'Guadalupe', 'Guadalupe y Calvo', 'Guazapares', 'Guerrero', 'Hidalgo del Parral', 'Huejotitan', 'Ignacio Zaragoza', 'Janos', 'Jimenez', 'Juarez', 'Julimes', 'La Cruz', 'Lopez', 'Madera', 'Maguarichi', 'Manuel Benavides', 'Matachi', 'Matamoros', 'Meoqui', 'Morelos', 'Moris', 'Namiquipa', 'Nonoava', 'Nuevo Casas Grandes', 'Ocampo', 'Ojinaga', 'Praxedis G. Guerrero', 'Riva Palacio', 'Rosales', 'Rosario', 'San Francisco de Borja', 'San Francisco de Conchos', 'San Francisco del Oro', 'Santa Barbara', 'Santa Isabel', 'Satevo', 'Saucillo', 'Temosachi', 'Urique', 'Uruachi', 'Valle de Zaragoza', 'Otro');

estados['Chiapas'] = new Array('Acacoyagua', 'Acala', 'Acapetahua', 'Aldama', 'Altamirano', 'Amatan', 'Amatenango de la Frontera', 'Amatenango del Valle', 'angel Albino Corzo', 'Arriaga', 'Bejucal de Ocampo', 'Bella Vista', 'Benemerito de las Americas', 'Berriozabal', 'Bochil', 'Cacahoatan', 'Capainala', 'Catazaja', 'Chalchihuitan', 'Chamula', 'Chanal', 'Chapultenango', 'Chenalho', 'Chiapa de Corzo', 'Chiapilla', 'Chicoasen', 'Chicomuselo', 'Chilon', 'Cintalapa', 'Coapilla', 'Comitan de Dominguez', 'El Bosque', 'El Porvenir', 'Escuintla', 'Francisco Leon', 'Frontera Comalapa', 'Frontera Hidalgo', 'Huehuetan', 'Huitiupan', 'Huixtan', 'Huixtla', 'Ixhuatan', 'Ixtacomitan', 'Ixtapa', 'Ixtapangajoya', 'Jiquipilas', 'Jitotol', 'Juarez', 'La Concordia', 'La Grandeza', 'La Independencia', 'La Libertad', 'La Trinataria', 'Larrainzar', 'Las Margaritas', 'Las Rosas', 'Mapastepec', 'Maravilla Tenejapa', 'Marques de Comillas', 'Mazapa de Madero', 'Mazatan', 'Metapa', 'Mitontic', 'Montecristo de Guerrero', 'Motozintla', 'Nicolas Ruiz', 'Ocosingo', 'Ocotepec', 'Ocozocoautla de Espinosa', 'Ostuacan', 'Osumacinta', 'Oxchuc', 'Palenque', 'Pantelho', 'Pantepec', 'Pichucalco', 'Pijijiapan', 'Pueblo Nuevo Solistahuacan', 'Rayon', 'Reforma', 'Sabanilla', 'Salto de Agua', 'San Andres Duraznal', 'San Cristobal de las Casas', 'San Fernando', 'San Juan Cancuc', 'San Lucas', 'Santiago el Pinar', 'Siltepec', 'Simojovel', 'Sitala', 'Socoltenango', 'Solosuchiapa', 'Soyalo', 'Suanuapa', 'Suchiapa', 'Suchiate', 'Tapachula', 'Tapalapa', 'Tapilula', 'Tecpatan', 'Tenejapa', 'Teopisca', 'Tila', 'Tonala', 'Totolapa', 'Tumbala', 'Tuxtla Chico', 'Tuxtla Gutierrez', 'Tuzantan', 'Tzimol', 'Union Juarez', 'Venustiano Carranza', 'Villa Comaltitlan', 'Villa Corzo', 'Villaflores', 'Yajalon', 'Zinacantan', 'Otro');

estados['Campeche'] = new Array('Calakmul', 'Calkini', 'Campeche', 'Candelaria', 'Carmen', 'Champoton', 'Escarcega', 'Hecelchakan', 'Hopelchen', 'Palizada', 'Tenabo', 'Otro');

estados['Baja California Sur'] = new Array('Comondu', 'La Paz', 'Loreto', 'Los Cabos', 'Mulege', 'Otro');

estados['Baja California'] = new Array('Ensenada', 'Mexicali', 'Tecate', 'Tijuana', 'Playas de Rosarito', 'Otro');

estados['Aguascalientes'] = new Array('Aguascalientes', 'Asientos', 'Calvillo', 'Cosio', 'El Llano', 'Jesus Maria', 'Pabellon de Arteaga', 'Rincon de Romos', 'San Francisco de los Romo', 'San Jose de Gracia', 'Tepezala', 'Otro');

estados['Distrito Federal'] = new Array('Distrito Federal', 'Otro');

listaEstados = new Array('Aguascalientes', 'Baja California', 'Baja California Sur', 'Campeche', 'Chiapas', 'Chihuahua', 'Coahuila', 'Colima', 'Distrito Federal', 'Durango', 'Guanajuato', 'Guerrero', 'Hidalgo', 'Jalisco', 'Estado de Mexico', 'Michoacan', 'Morelos', 'Nayarit', 'Nuevo Leon', 'Oaxaca', 'Puebla', 'Queretaro', 'Quintana Roo', 'San Luis Potosi', 'Sinaloa', 'Sonora', 'Tabasco', 'Tamaulipas', 'Tlaxcala', 'Veracruz', 'Yucatan', 'Zacatecas', 'Otro');

function cargarEstados(elemento) {
    sel = document.getElementById(elemento);
    totalEstados = listaEstados.length;
    opcion = document.createElement("OPTION");
    opcion.innerHTML = '';
    opcion.value = '';
    sel.appendChild(opcion);
    for (i = 0; i < totalEstados; i++) {
        opcion = document.createElement("OPTION");
        opcion.innerHTML = listaEstados[i];
        opcion.value = listaEstados[i];
        sel.appendChild(opcion);
    }
}

function cargarMunicipios(estado, elemento) {
    sel = document.getElementById(elemento);
    total = sel.options.length - 1;
    for (j = total; j >= 0; j--) {
        sel.options[j] = null;
    }

    totalMunicipios = estados[estado].length;
    opcion = document.createElement("OPTION");
    opcion.innerHTML = '';
    opcion.value = '';
    sel.appendChild(opcion);
    for (i = 0; i < totalMunicipios; i++) {
        opcion = document.createElement("OPTION");
        opcion.innerHTML = estados[estado][i];
        opcion.value = estados[estado][i];
        sel.appendChild(opcion);
    }

}

function quitarAcentos(Text)
{
    var cadena = "";
    var codigo = "";
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)
    {
        var Char = Text.charCodeAt(j);
        var cara = Text.charAt(j);
        if (cara == "&") {
            temp = Text.substring(j, j + 8);
            switch (temp) {
                case "&aacute;":
                    cadena += "(/a)";
                    j = j + 7;
                    break;
                case "&Aacute;":
                    cadena += "(/A)";
                    j = j + 7;
                    break;
                case "&eacute;":
                    cadena += "(/e)";
                    j = j + 7;
                    break;
                case "&Eacute;":
                    cadena += "(/E)";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "(/i)";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "(/I)";
                    j = j + 7;
                    break;
                case "&oacute;":
                    cadena += "(/o)";
                    j = j + 7;
                    break;
                case "&Oacute;":
                    cadena += "(/O)";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "(/u)";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "(/U)";
                    j = j + 7;
                    break;
                case "&ntilde;":
                    cadena += "(/n)";
                    j = j + 7;
                    break;
                case "&Ntilde;":
                    cadena += "(/N)";
                    j = j + 7;
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        } else {
            switch (Char)
            {
                case 225:
                    cadena += "(/a)";
                    break;
                case 233:
                    cadena += "(/e)";
                    break;
                case 237:
                    cadena += "(/i)";
                    break;
                case 243:
                    cadena += "(/o)";
                    break;
                case 250:
                    cadena += "(/u)";
                    break;
                case 193:
                    cadena += "(/A)";
                    break;
                case 201:
                    cadena += "(/E)";
                    break;
                case 205:
                    cadena += "(/I)";
                    break;
                case 211:
                    cadena += "(/O)";
                    break;
                case 218:
                    cadena += "(/U)";
                    break;
                case 241:
                    cadena += "(/n)";
                    break;
                case 209:
                    cadena += "(/N)";
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        }
        codigo += "_" + Text.charCodeAt(j);
    }
    return cadena;
}

function ponerAcentos(Text) {
    var cadena = "";
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)
    {
        var cara = Text.charAt(j);
        if (cara == "(") {
            temp = Text.substring(j, j + 4);
            switch (temp) {
                case "(/a)":
                    cadena += "&aacute;";
                    j = j + 3;
                    break;
                case "(/A)":
                    cadena += "&Aacute;";
                    j = j + 3;
                    break;
                case "(/e)":
                    cadena += "&eacute;";
                    j = j + 3;
                    break;
                case "(/E)":
                    cadena += "&Eacute;";
                    j = j + 3;
                    break;
                case "(/i)":
                    cadena += "&iacute;";
                    j = j + 3;
                    break;
                case "(/I)":
                    cadena += "&Iacute;";
                    j = j + 3;
                    break;
                case "(/o)":
                    cadena += "&oacute;";
                    j = j + 3;
                    break;
                case "(/O)":
                    cadena += "&Oacute;";
                    j = j + 3;
                    break;
                case "(/u)":
                    cadena += "&uacute;";
                    j = j + 3;
                    break;
                case "(/U)":
                    cadena += "&Uacute;";
                    j = j + 3;
                    break;
                case "(/n)":
                    cadena += "&ntilde;";
                    j = j + 3;
                    break;
                case "(/N)":
                    cadena += "&Ntilde;";
                    j = j + 3;
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        } else {
            cadena += Text.charAt(j);
        }
    }
    return cadena;
}


/* El objetivo de este fichero es crear la clase objetoAjax (en Javascript a las �clases� se les llama �prototipos�) */

function AjaxGET()
{
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();
    return xmlhttp;
}

var cursores = new Array("auto", "hand", "crosshair", "default", "pointer", "move", "e-resize", "ne-resize", "nw-resize", "n-resize", "se-resize", "sw-resize", "s-resize", "w-resize", "text", "wait");
function setCursor() {
    document.body.style.cursor = 'hand'
}
function restoreCursor() {
    document.body.style.cursor = 'default'
}
function mostrarDiv(nombre) {
    capa = document.getElementById(nombre);
    capa.style.display = '';
}

function ocultarDiv(nombre) {
    capa = eval('document.getElementById("' + nombre + '").style');
    //	div = document.getElementById(nombre);
    capa.display = 'none';
}

function validarCedula(val) {
    out = "";
    if (val.length != 10)
        out = 'La cedula debe ser de 10 caracteres';
    else {
        l1 = val.charAt(0);
        l2 = val.charAt(1);
        l3 = val.charAt(2);
        l4 = val.charAt(3);
        ano = val.charAt(4) + val.charAt(5);
        mes = val.charAt(6) + val.charAt(7);
        dia = val.charAt(8) + val.charAt(9);
        if (!esConsonante(l1))
            out = 'El primer caracter de la cedula debe ser una consonante.\n';
        else if (!esVocal(l2))
            out = 'El segundo caracter de la cedula debe ser una vocal.\n';
        else if (!esConsonante(l3))
            out = 'El tercer caracter de la cedula debe ser una consonante.\n';
        else if (!esConsonante(l4))
            out = 'El cuarto caracter de la cedula debe ser una consonante.\n';
        else if (!esFechaValida(dia + "/" + mes + "/19" + ano))
            out = 'La fecha de la cedula es invalida.\n';
    }
    return out;
}

function buscarDH(cedula) {
    //	cedulaCorrecta = validarCedula(cedula);
    if (cedula.length > 3) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes');
        var objeto = new AjaxGET();
        objeto.open("GET", "buscarDHparaCita.php?cedula=" + cedula, true);
        objeto.onreadystatechange = function()
        {
            if (objeto.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
            }
            if ((objeto.readyState == 1) || (objeto.readyState == 2) || (objeto.readyState == 3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe teclear al menos 4 caracteres');
    }
}

function buscarDHN(ap_p, ap_m, nombre) {
    if ((ap_p.length > 0) || (ap_m.length > 0) || (nombre.length > 0)) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes2');
        var objeto = new AjaxGET();
        objeto.open("GET", "buscarDHNparaCita.php?ap_p=" + ap_p + "&ap_m=" + ap_m + "&nombre=" + nombre, true);
        objeto.onreadystatechange = function()
        {
            if (objeto.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
            }
            if ((objeto.readyState == 1) || (objeto.readyState == 2) || (objeto.readyState == 3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe ingresar al menos un campo de busqueda (apellido paterno, apellido materno o nombre)');
    }
}

function buscarDHbusqueda(cedula) {
    if (cedula.length > 3) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes');
        var objeto = new AjaxGET();
        objeto.open("GET", "buscarDHparaBusqueda.php?cedula=" + cedula, true);
        objeto.onreadystatechange = function()
        {
            if (objeto.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
            }
            if ((objeto.readyState == 1) || (objeto.readyState == 2) || (objeto.readyState == 3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe teclear al menos 4 caracteres');
    }
}

function buscarDHNbusqueda(ap_p, ap_m, nombre) {
    if ((ap_p.length > 0) || (ap_m.length > 0) || (nombre.length > 0)) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes2');
        var objeto = new AjaxGET();
        objeto.open("GET", "buscarDHNparaBusqueda.php?ap_p=" + quitarAcentos(ap_p) + "&ap_m=" + quitarAcentos(ap_m) + "&nombre=" + quitarAcentos(nombre), true);
        objeto.onreadystatechange = function()
        {
            if (objeto.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
            }
            if ((objeto.readyState == 1) || (objeto.readyState == 2) || (objeto.readyState == 3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe ingresar al menos un campo de busqueda (apellido paterno, apellido materno o nombre)');
    }
}

function esHoraValida(val, nm) {
    errors = "";
    a = val.charAt(0);
    b = val.charAt(1);
    c = val.charAt(2);
    d = val.charAt(3);
    e = val.charAt(4);
    if (val.length != 5)
        errors += '- ' + nm + ' debe contener una hora valida. ej 08:30\n';
    else if (isNaN(a))
        errors += '- ' + nm + ' debe contener una hora valida. ej 08:30\n';
    else if (isNaN(b))
        errors += '- ' + nm + ' debe contener una hora valida. ej 08:30\n';
    else if (c != ':')
        errors += '- ' + nm + ' debe contener una hora valida. ej 08:30\n';
    else if (isNaN(d))
        errors += '- ' + nm + ' debe contener una hora valida. ej 08:30\n';
    else if (isNaN(e))
        errors += '- ' + nm + ' debe contener una hora valida. ej 08:30\n';
    else if ((a == 2 && b > 3) || (a > 2))
        errors += '- ' + nm + ' debe contener una hora valida. ej 08:30\n';
    else if (d > 5)
        errors += '- ' + nm + ' debe contener una hora valida. ej 08:30\n';
    return errors;
}

function DetectaBloqueoPops()
{
    var popup
    try
    {
        //Se crea una nueva ventana para probar si esta o no activo
        // el bloqueador de ventanas emergentes.
        //Si esta activo, se lanza el error, de lo contrario s�lo se cierra la ventana creada
        if (!(popup = window.open('about:blank', '_blank', 'width=1,height=1')))
            throw "ErrPop"
        msj = "La ventana se cre� con �xito"
        popup.close()
    }
    catch (err)
    {
        //Se captura el error, si fue por motivo de bloqueo, se muestra el mensaje de advertencia
        //Si no fue por bloque, entonces se muestra la descripci�n del error ocurrido.
        if (err == "ErrPop")
            msj = "tA T E N C I � Nnn�El bloqueo de popups esta activo!"
        else
        {
            msj = "Hubo un erro en la p�gina.nn"
            msj += "Descripci�n del error: " + err.description + "nn"
        }
    }
    alert(msj)

}


function esFechaValida(fecha) {
    if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha)) {
        return false;
    }
    var dia = parseInt(fecha.substring(0, 2), 10);
    var mes = parseInt(fecha.substring(3, 5), 10);
    var anio = parseInt(fecha.substring(6), 10);

    switch (mes) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            numDias = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            numDias = 30;
            break;
        case 2:
            numDias = 29;
            break;
        default:
            return false;
    }

    if (dia > numDias || dia == 0) {
        return false;
    }
    return true;
}

function esConsonante(valor) {
    var conso = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', '&ntilde;', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    tConso = conso.length;
    seEncontro = false;
    for (i = 0; i < tConso; i++)
        if (conso[i] == valor.toUpperCase())
            seEncontro = true;
    return seEncontro;
}

function esVocal(valor) {
    var vocal = new Array('A', 'E', 'I', 'O', 'U');
    tVocal = vocal.length;
    seEncontro = false;
    for (i = 0; i < tVocal; i++)
        if (vocal[i] == valor.toUpperCase())
            seEncontro = true;
    return seEncontro;
}

function hacerLogin(usuario, pass) {
    //	document.getElementById('ingresar')focus();
    var contenido = document.getElementById('contenido');
    var objeto = AjaxGET();
    objeto.open("GET", "loginAcceder.php?usuario=" + usuario + "&pass=" + pass, true);
    objeto.onreadystatechange = function()
    {
        if (objeto.readyState == 4)
        {
            aResp = objeto.responseText.split("|");
            if (aResp[1] == "-1") {
                alert("Usuario o contrase\u00f1a incorrectas");
                $("#contenido").load("login.php");
            }
            else
            {
                aResp = objeto.responseText.split("|");
                switch (aResp[3])
                {
                    case "inicioContrarref.php":
                        mostrarDiv('menuContrarreferencia');
                        $("#contenido").load(aResp[3]);
                        break;
                    case "inicioVisor.php":
                        mostrarDiv('menuClinica');
                        $("#contenido").load(aResp[3]);
                        break;
                    case "Sera direccionado al modulo Administrartivo":
                        alert(aResp[3]);
                        $.cookie("idUsuario",aResp[1]);
                        location.href = "admin/index.php";
                        break;
                    case "inicioJefeServicio.php":
                        mostrarDiv("menuJefeServicio");
                        $("#contenido").load(aResp[3]);
                        break;
                }
            }
        } else
        if (objeto.readyState == 2)
        {
            contenido.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    };
    objeto.send(null);
}


function refrescarVariables() {
    fresh.coger('variables.php', '', true)
}

function verificarLogin() {
    var usuario = document.getElementById('usuario').value;
    var pass = document.getElementById('pass').value;
    var correcto = true;
    if (usuario.length < 6) {
        alert('Introduce un Nombre de Usuario Correcto');
        correcto = false;
    }
    if ((pass.length < 6) && (correcto == true)) {
        alert('Introduce una Contrase&ntilde;a Correcta');
        correcto = false;
    }
    if (correcto == true)
        hacerLogin(usuario, pass);
}

function obtenerLogin() {
    var contenedor2 = document.getElementById('contenido');
    var agenda = new AjaxGET();
    agenda.open("GET", "login.php", true);
    agenda.onreadystatechange = function()
    {
        if (agenda.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState == 2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)

}

function buscarPor() {
    if (document.getElementById('tipo_busqueda').value == 'nombre') {
        ocultarDiv('buscarPorCedula');
        mostrarDiv('buscarPorNombre');
    } else {
        ocultarDiv('buscarPorNombre');
        mostrarDiv('buscarPorCedula');
    }
}

function reportes() {
    var contenedor2 = document.getElementById('contenido');
    var agenda = new AjaxGET();
    agenda.open("GET", "reportes.php", true);
    agenda.onreadystatechange = function()
    {
        if (agenda.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState == 2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function reportesCargar(liga) {
    var contenedor2 = document.getElementById('contenido');
    var agenda = new AjaxGET();
    agenda.open("GET", liga, true);
    agenda.onreadystatechange = function()
    {
        if (agenda.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState == 2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function validarReporte(liga, idUsuario, idServicio, idMedico) {
    var fechaI = document.getElementById('date1');
    var fechaF = document.getElementById('date2');
    f1 = fechaI.value.length;
    f2 = fechaF.value.length;
    if (f1 == 0 || f2 == 0) {
        alert("Ambas fechas deben estar establecidas!");
        return false;
    }

    var fechaIn = fechaI.value;
    var fechaFi = fechaF.value;
    var diaIn = fechaIn.substring(0, 2);
    var mesIn = (fechaIn.substring(3, 5)) * 1 - 1;
    var anoIn = fechaIn.substring(6, 10);
    var fechaInU = new Date(anoIn, mesIn, diaIn);
    var diaFi = fechaFi.substring(0, 2);
    var mesFi = (fechaFi.substring(3, 5)) * 1 - 1;
    var anoFi = fechaFi.substring(6, 10);
    var fechaFiU = new Date(anoFi, mesFi, diaFi);
    if (fechaInU > fechaFiU) {
        alert("La fecha final no puede ser menor a la fecha inicial");
    } else {
        var rg1 = document.getElementById('RadioGroup1_0');
        var rg2 = document.getElementById('RadioGroup1_1');
        var rg3 = document.getElementById('RadioGroup1_2');
        var rg4 = document.getElementById('RadioGroup1_3');
        if (rg1.checked)
            tipoReporte = "unidad";
        //if (rg2.checked) tipoReporte = "consultorio";
        if (rg3.checked)
            tipoReporte = "servicio";
        if (rg4.checked)
            tipoReporte = "medico";

    }
    return false;
}

function logout() {
    var contenedor2 = document.getElementById('contenido');
    var agenda = new AjaxGET();
    agenda.open("GET", "logout.php", true);
    agenda.onreadystatechange = function()
    {
        if (agenda.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            //			contenedor2.innerHTML = agenda.responseText;
            document.getElementById('menu').style.display = "none";
            obtenerLogin();
        }
        if (agenda.readyState == 2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}
function ayuda() {
    var contenedor2 = document.getElementById('contenido');
    var agenda = new AjaxGET();
    agenda.open("GET", "ayuda.php", true);
    agenda.onreadystatechange = function()
    {
        if (agenda.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState == 2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function buscar() {
    var contenedor2 = document.getElementById('contenido');
    var agenda = new AjaxGET();
    agenda.open("GET", "buscar.php", true);
    agenda.onreadystatechange = function()
    {
        if (agenda.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState == 2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function verCitasDH() {
    if (!document.getElementById('dh')) {
        alert("Ingrese la cedula del derechohabiente y haga click en Buscar...");
    } else {
        valor = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
        valor = ponerAcentos(valor);
        if (valor == -1) {
            alert('No existe derechohabiente con la cedula introducida');
        } else {
            aValor = valor.split('|');
            mostrarDiv("citas");
            var contenedor2 = document.getElementById('citas');
            var agenda = new AjaxGET();
            agenda.open("GET", "citasXbusqueda.php?id_derecho=" + aValor[0], true);
            agenda.onreadystatechange = function()
            {
                if (agenda.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
                {
                    contenedor2.innerHTML = agenda.responseText;
                }
                if (agenda.readyState == 2)
                {
                    contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            }
            agenda.send(null)
        }
    }
}
//Funciones para contraReferencias en arreglos.js

function crearReferencia(horario)
{
    $("#contenido").load("generarContraReferencia.php?id_horario=" + horario);
}

function crearReferenciaExt(cita)
{
    $("#contenido").load("generarContraReferencia.php?idCita=" + cita);
}

function ObtenerUnidadesMedicas()
{
    var est = $("#estado option:selected").html();
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function() {
        if (xml.readyState == 4)
        {
            document.getElementById('Unidad').innerHTML = quitarAcentos(xml.responseText);
        }
        if (xml.readyState == 2)
        {
            document.getElementById('Unidad').innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    };
    xml.open("GET", "ObtenerUMF.php?estado=" + est, true);
    xml.send();

}

function ObtenerContraRef()
{
    if (!document.getElementById('dh')) {
        alert("Ingrese la cedula del derechohabiente y haga click en Buscar...");
    } else {
        valor = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
        valor = ponerAcentos(valor);
        if (valor == -1) {
            alert('No existe derechohabiente con la cedula introducida');
        } else {
            aValor = valor.split('|');
            mostrarDiv("citas");
            var idDer = aValor[0];
            var contenedor = document.getElementById('citas');
            var ref = new AjaxGET();
            ref.open("GET", "mostrarReferencias.php?id_derecho=" + idDer, true);
            ref.onreadystatechange = function() {
                if (ref.readyState == 4)
                {
                    contenedor.innerHTML = ref.responseText;
                }
                if (ref.readyState == 2)
                {
                    contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            };
            ref.send(null);
        }
    }
}

function BuscarContraRef()
{
    $("#contenido").load("buscarReferencias.php");
}

function vaciarSelect(elemeto)
{
    opciones = document.getElementById(elemeto).length;
    lista = document.getElementById(elemeto);
    while (opciones > 0) {
        lista.options[opciones - 1] = null;
        opciones = document.getElementById(elemeto).length;
    }
}

function VerContrarreferencia()
{
    var idServicio = document.getElementById('MSer').value;
    var idMedico = document.getElementById('medico').value;
    var tipoBusq = document.getElementsByName('RadioGroup1');
    var tipoBusqClin = $("input[name='filClinica']:checked").val();
    var estadoSelect = $("#estado option:selected").html();
    for (i = 0; i < tipoBusq.length; i++)
        if (tipoBusq[i].checked)
            tipBusqSel = tipoBusq[i].value;
    var unidad = document.getElementById('Unidad').value;
    dia = $("#dia1 option:selected").val().toString();
    mes = $("#mes1 option:selected").val().toString();
    var fecha1 = $("#anio1").val().toString() + mes + dia;
    dia = $("#dia2 option:selected").val().toString();
    mes = $("#mes2 option:selected").val().toString();
    var fecha2 = $("#anio2").val() + mes + dia;
	var enviada=$("#TipoRespuesta option:selected").val();
    var ref = new AjaxGET();
    ref.onreadystatechange = function()
    {
        if (ref.readyState == 4)
        {
            $("#contenido").html(ref.responseText);
        }
        else
        {
            $("#contenido").html("<img src=\"diseno/loading.gif\">");
        }
    };
    ref.open("GET", "VerContrarreferencias.php?id_servicio=" + idServicio + "&idMedico=" + idMedico + "&tipoBusq=" + tipBusqSel + "&unidad=" + unidad + "&fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&estado=" + estadoSelect + "&filClinica=" + tipoBusqClin+"&enviado="+enviada, true);
    ref.send(null);
}

function regresar()
{
    $("#contenido").load("ConsRef.php");
}


function calendarios(elemento)
{
    var mes = $("#mes" + elemento + " option:selected").val();
    vaciarSelect("dia" + elemento);
    switch (mes)
    {
        case '01':
            dias = 31;
            break;
        case '02':
            anio = document.getElementById('anio' + elemento).value;
            if ((anio % 4) == 0)
                dias = 29;
            else
                dias = 28;
            break;
        case '03':
            dias = 30;
            break;
        case '04':
            dias = 30;
            break;
        case '05':
            dias = 31;
            break;
        case '06':
            dias = 30;
            break;
        case '07':
            dias = 31;
            break;
        case '08':
            dias = 31;
            break;
        case '09':
            dias = 30;
            break;
        case '10':
            dias = 31;
            break;
        case '11':
            dias = 30;
            break;
        case '12':
            dias = 31;
            break;
    }
    for (i = 1; i <= dias; i++)
    {
        if (i < 10)
            opcion = new Option(i, "0" + i);
        else
            opcion = new Option(i, i);
        document.getElementById("dia" + elemento).options[i - 1] = opcion;
    }

}

function VerReferencia(numE)
{
    referencia = document.getElementById("ref" + numE).value;
    window.open("VerFormatoContrarreferencia.php?folioContrarrefencia=" + referencia);
}

function enviarRefaClin(numE)
{
    var contenedor = document.getElementById('env' + numE);
    referencia = document.getElementById("ref" + numE).value;
    var objecto = new AjaxGET();
    objecto.open("GET", "EnvioClinica.php?ref=" + referencia, true);
    objecto.onreadystatechange = function()
    {
        if (objecto.readyState == 4)
        {
            eval(objecto.responseText);
            if (objecto.responseText == "alert('La referencia fue enviada a clinica');")
                contenedor.innerHTML = "enviada";
        }
    };
    objecto.send(null);
}

function inicio(liga) {
    var contenedor = document.getElementById('contenido');

    var seleccion = new AjaxGET();
    seleccion.open("GET", liga, true);
    seleccion.onreadystatechange = function()
    {
        if (seleccion.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = seleccion.responseText;
        }
        if ((seleccion.readyState == 1) || (seleccion.readyState == 2) || (seleccion.readyState == 3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    seleccion.send(null);
    switch (liga)
    {
        case "inicioContrarref.php":
            mostrarDiv('menuContrarreferencia');
            break;
        case "inicioVisor.php":
            mostrarDiv('menuClinica');
            break;
        case "Sera direccionado al modulo Administrartivo":
            alert(liga);
            location.href = "admin/index.php"
    }
}

function buscarReferencias()
{
    $("#contenido").load("buscarReferencias.php");
}

function verReferencias()
{
    $("#contenido").load("ConsRef.php");
}

function buscarCitas()
{
    var contenedor = document.getElementById('contenido');
    var ajax = new AjaxGET();
    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
            mostrarCitas();
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", "VerCitas.php", true);
    ajax.send(null);
}

function filtrarCitas()
{
    ind = document.getElementById('selectFiltro').selectedIndex;
    tipo = document.getElementById('selectFiltro').options[ind].value;
    if (tipo == 3)
    {
        ocultarDiv('busquedaClinica');
        ocultarDiv('busquedaFecha');
        mostrarCitas();
    }
    if (tipo == 2)
    {
        ocultarDiv('busquedaFecha');
        mostrarDiv('busquedaClinica');
    }
    if (tipo == 1)
    {
        mostrarDiv('busquedaFecha');
        ocultarDiv('busquedaClinica');
    }
}

function mostrarCitas()
{
    ind = document.getElementById('selectFiltro').selectedIndex;
    var tipo = document.getElementById('selectFiltro').options[ind].value;
    var fecha1 = "", fecha2 = "", umf = "";
    if (tipo == 2)
    {
        ind = document.getElementById('Unidad').selectedIndex;
        umf = document.getElementById('Unidad').options[ind].value;
    }
    if (tipo == 1)
    {
        dia1 = document.getElementById('dia1').options[document.getElementById('dia1').selectedIndex].value;
        mes1 = document.getElementById('dia1').options[document.getElementById('mes1').selectedIndex].value;
        anio1 = document.getElementById('anio1').value;
        fecha1 = anio1 + mes1 + dia1 + "";
        dia2 = document.getElementById('dia2').options[document.getElementById('dia2').selectedIndex].value;
        mes2 = document.getElementById('dia2').options[document.getElementById('mes2').selectedIndex].value;
        anio2 = document.getElementById('anio2').value;
        fecha2 = anio1 + mes1 + dia1 + "";
    }
    var contenedor = document.getElementById('citas');
    var citas = new AjaxGET();
    citas.onreadystatechange = function()
    {
        if (citas.readyState == 4)
        {
            contenedor.innerHTML = citas.responseText;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    citas.open("GET", "MostrarCitasApartadas.php?filtro=" + tipo + "&clinica=" + umf + "&fecha1=" + fecha1 + "&fecha2=" + fecha2);
    citas.send(null);
}

function contestarSolicitud(idSolicitud)
{

    window.open("AceptarConsulta.php?citaApartada=" + idSolicitud);
    /*var contenedor=document.getElementById('VentanaFlotante');
     $("#VentanaFlotante").fadeTo(1000, 0.4, function(){
     mostrarDiv('VentanaFlotante');
     var ajax=new AjaxGET();
     ajax.onreadystatechange=function()
     {
     if(ajax.readyState==4)
     {
     contenedor.innerHTML=ajax.responseText;
     }
     else
     contenedor.innerHTML="<img src=\"diseno/loading.gif\">";
     }
     ajax.open("GET","AceptarConsulta.php?citaApartada="+idSolicitud);
     ajax.send(null);
     });*/
}
function cerrar()
{
    window.close();
}

function aceptarCita()
{
    var cita = document.getElementById('solicitud').value;
    $.ajax({
        url: "aceptarCitaConfirmar.php",
        data: "idSolicitud=" + cita,
        type: "GET",
        dataType: "html"
    }).done(function(datos) {
        if (datos == "ok") {
            alert("Cita Aceptada");
            mostrarCitasPadre();


        }
        else
            alert("error en aceptar la cita, intente de nuevo");
    });
}

function rechazarCita()
{
    mostrarDiv('rech');
    ocultarDiv('botones');
}

function rechazarCitaConfirmar()
{
    var cita = $("#cita").val();
    var solicitud = $('#solicitud').val();
    var motivo = $('#rechazo').val();
    if (motivo == "")
        alert("Ingrese el motivo del rechazo de la cita");
    else {
        $.ajax({
            url: "cancelarCitaConfirmar.php",
            data: "motivo=" + motivo + "&idCita=" + cita + "&citaAp=" + solicitud,
            dataType: "text",
            type: "GET"
        }).done(function(data) {
            if (data == "ok")
            {
                alert("Cita rechazada");
                mostrarCitasPadre();
            }
            else
                alert("Error en el proceso, intente de nueva cuenta");
        });
    }
}

function cancelarRechazo()
{
    document.getElementById('rech').innerHTML = "";
    ocultarDiv('rech');
    mostrarDiv('botones');
}

function reprogramarCita(idCita, idSol)
{
    aceptar = confirm("Acepta la cita con numero de solicitud" + idSol);
    if (aceptar) {
        window.open("buscarCitas.php?idCita=" + idCita + "&idSol=" + idSol);
    }
    else
    {
        window.open("rechazarCitaPorReprogramacion.php?citaApartada=" + idSol);
    }
}

function elegirCita(idDerecho, idhorario, fecha, horario, idCitaAnt, idSolicitud)
{
    var contenedor = document.getElementById('citas');
    var conf = confirm("Desea elegir la cita el dia de " + fecha + " con horario de " + horario);
    if (conf) {
        var objeto = new AjaxGET();
        objeto.open("GET", "agregarCitaConfirmar.php?idDerecho=" + idDerecho + "&fecha=" + fecha + "&horario=" + idhorario + "&idCitaAnt=" + idCitaAnt + "&idSol=" + idSolicitud, true);
        objeto.onreadystatechange = function() {
            if (objeto.readyState == 4)
            {

                var aResp = objeto.responseText.split("|");
                if (aResp.length > 2) {
                    alert("cita Reprogramada");
                    mostrarCitasPadre();
                }
                else
                {
                    alert(aResp[1]);
                    apartarCita();
                }
            }
            else
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
        objeto.send(null);
    }
    else
        apartarCita();
}

function MedicosXServicio(servicio)
{
    var contenedor = document.getElementById('medico');
    var id = document.getElementById(servicio).options[document.getElementById(servicio).selectedIndex].value;
    var ajax = new AjaxGET();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", "MedicosServicio.php?servicio=" + id);
    ajax.send(null);
}

function mostrarCitasPadre()
{


    var contenedor = opener.document.getElementById('contenido');
    var citas = new AjaxGET();
    citas.onreadystatechange = function()
    {
        if (citas.readyState == 4)
        {
            contenedor.innerHTML = citas.responseText;
            buscarCitas();
            cerrar();
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    citas.open("GET", "VerCitas.php");
    citas.send(null);
}

function cancelCita()
{
    var cita = $("#cita").val();
    var solicitud = $("#cita").val();
    var motivo = $('#rechazo').val();
    if (motivo == "")
        alert("Ingrese el motivo del rechazo de la cita");
    else {
        $.ajax({
            url: "cancelarCitaConfirmar.php",
            data: "motivo=" + motivo + "&idCita=" + cita + "&citaAp=" + solicitud,
            dataType: "text",
            type: "GET"
        }).done(function(data) {
            if (data == "ok")
            {
                alert("Cita rechazada");
                mostrarCitasPadre();
                //cerrar();
            }
            else
                alert("Error en el proceso, intente de nueva cuenta");
        });
    }
}

function seleccionarEstado()
{
    mostrarDiv('clinicas');
    vaciarSelect('estado');
    cargarEstados('estado');

}

function BuscarCitasPor()
{
    var filtro = $("#filtrarPor option:selected").val();
    
    if(filtro=="C")
        mostrarDiv("buscarClinica");
    else
        ocultarDiv("buscarClinica");
}

function ConsultarReferencias()
{
    $("#contenido").load("VerReferenciasContestadas.php");
}


function cambiarFecha(campo)
{
    var dia = $("#dia" + campo + " option:selected").val()
    var mes = $("#mes" + campo + " option:selected").val();
    var anio = $("#anio" + campo).val();
    var fecha = anio + "" + mes + "" + dia
    $("#fecha" + campo).val(fecha);
}

function ChecarConsultas()
{
    var filtro = $("#filtrarPor option:selected").val();
    var filtroFecha = $("input:radio[name=filtrarFecha]:checked").val();
    if (filtro == "C")
        var Clinica = $("#Unidad option:selected").val();
    if (filtroFecha == "F")
    {
        var fecha1 = $("#fecha1").val();
        var fecha2 = $("#fecha2").val();
    }
    if (filtro == "C" && filtroFecha == "F")
        $.ajax({
            url: "ReferenciasAtendidas.php",
            data: {
                fConsulta: filtro,
                fFecha: filtroFecha,
                clinica: Clinica,
                fecha1: fecha1,
                fecha2: fecha2
            },
            success: function(data)
            {
                $("#citas").html(data);
            }
        });
    else
    if (filtro == "C" && filtroFecha == "T")
        $.ajax({
            url: "ReferenciasAtendidas.php",
            data: {
                fConsulta: filtro,
                fFecha: filtroFecha,
                clinica: Clinica
            },
            success: function(data)
            {
                $("#citas").html(data);
            },
            beforeSend:function()
            {
                $("#citas").html("<img src=\"diseno/loading.gif\">");
            }
        });
    else
        if(filtro!="C" && filtroFecha=="F")
            $.ajax({
            url: "ReferenciasAtendidas.php",
            data: {
                fConsulta: filtro,
                fFecha: filtroFecha,
                fecha1: fecha1,
                fecha2: fecha2
            },
            success: function(data)
            {
                $("#citas").html(data);
            },
            beforeSend:function()
            {
                $("#citas").html("<img src=\"diseno/loading.gif\">");
            }
        });
        else
        $.ajax({
            url: "ReferenciasAtendidas.php",
            data: {
                fConsulta: filtro,
                fFecha: filtroFecha
            },
            success: function(data)
            {
                $("#citas").html(data);
            },
            beforeSend:function()
            {
                $("#citas").html("<img src=\"diseno/loading.gif\">");
            }
        });

}

function avanzarConsultaCitas(div, sql, cont)
{
    var lim1 = $("#lim1").val();
    var lim2 = $("#lim2").val();
    $.ajax({
        url: "ReferenciasAtendidasVisor.php",
        data: {
            l1: lim1,
            l2: lim2,
            sql: sql
        },
        success: function(data) {
            $("#resultadosCitas").html(data);
        }

    });
    lim1 = parseInt(lim1) + parseInt(div);
    lim2 = parseInt(lim2) + parseInt(div);
    $("#lim1").val(lim1);
    $("#lim2").val(lim2);
    if (lim2 >= cont)
        $("#av").attr("disabled", true);
    else
        $("#av").removeAttr("disabled");
}

function retroConsultaCitas(div, sql)
{
    var lim1 = $("#lim1").val();
    var lim2 = $("#lim2").val();
    $.ajax({
        url: "ReferenciasAtendidasVisor.php",
        data: {
            l1: lim1,
            l2: lim2,
            sql: sql
        },
        success: function(data) {
            $("#resultadosCitas").html(data);
        }

    });
    lim1 -= div;
    lim2 -= div;
    $("#lim1").val(lim1);
    $("#lim2").val(lim2);
    if (lim1 <= 0)
        $("#ret").attr("disabled", true);
    else
        $("#ret").removeAttr("disabled");
}

function inicioConsultaCitas(sql)
{
    var lim1 = 0;
    var lim2 = 25;
    $.ajax({
        url: "ReferenciasAtendidasVisor.php",
        data: {
            l1: lim1,
            l2: lim2,
            sql: sql
        },
        success: function(data) {
            $("#resultadosCitas").html(data);
        }

    });
    $("#ret").attr("disabled", true);
    $("#av").removeAttr("disabled");
    $("#lim1").val(lim1);
    $("#lim2").val(lim2);
}

function finConsultaCitas(cont, div, sql)
{
    var lim1 = cont - div;
    var lim2 = cont;
    $.ajax({
        url: "ReferenciasAtendidasVisor.php",
        data: {
            l1: lim1,
            l2: lim2,
            sql: sql
        },
        success: function(data) {
            $("#resultadosCitas").html(data);
        }

    });
    $("#av").attr("disabled", true);
    $("#ret").removeAttr("disabled");
    $("#lim1").val(lim1);
    $("#lim2").val(lim2);
}

function buscarCitasPrv()
{
    $("#contenido").load("BuscarDHDesbloqueo.php");
}

function BuscarCitasPRV()
{
    if (!document.getElementById('dh')) {
        alert("Ingrese la cedula del derechohabiente y haga click en Buscar...");
    } else {
        valor = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
        valor = ponerAcentos(valor);
        if (valor == -1) {
            alert('No existe derechohabiente con la cedula introducida');
        } else {
            aValor = valor.split('|');
            mostrarDiv("citas");
            var idDer = aValor[0];
            var contenedor = document.getElementById('citas');
            var ref = new AjaxGET();
            ref.open("GET", "BuscarCitasDes.php?idDer=" + idDer, true);
            ref.onreadystatechange = function() {
                if (ref.readyState == 4)
                {
                    contenedor.innerHTML = ref.responseText;
                }
                if (ref.readyState == 2)
                {
                    contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            };
            ref.send(null);
        }
    }
}

function desbloquearCita(idReferencia)
{
    var motivo=prompt("Escriba el Motivo de Liberacion","");
    var ref=new AjaxGET();
    ref.onreadyStateChange=function ()
    {
        if(ref.readyState==4)
            {
                resp=ref.responseText;
                if(resp=="1"){
                    alert("cita Desbloqueada, puede agendar de nuevo");
                    BuscarCitasPRV();
                }
                else
                    alert("error, intente nuevamente");
            }
                
    }
    ref.open("GET","DesbloquearCita.php?idRef="+idReferencia+"&motivo="+motivo,true);
    ref.send(null);
}
