<?php
error_reporting(E_ALL^E_NOTICE);
define("WP_MEMORY_LIMIT", "512M");
$baseDatos_issste = 'sistema_agenducha';
$host_issste = 'localhost';
$usuario_issste = 'root';
$pass_issste = '1234567890';

function obtenerDatosUsuario($idUsuario) {
    global $baseDatos_issste;
    global $host_issste;
    global $pass_issste;
    global $usuario_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from usuarios_contrarreferencias where id_usuario=" . $idUsuario;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function obtenerDatosClinica($idClinica) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from vgf where id_unidad=" . $idClinica;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function obtenerReferenciasEnviadas($idClin) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from contrarreferencias where id_unidad=" . $idClin . " And envio_clinica=1 Order by fecha_envio_clinica DESC LIMIT 50";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query))
        while ($row = mysql_fetch_array($query)) {
            $ret[] = $row;
        }
    return $ret;
}

function obtenerReferenciasCont($idClin) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from contrarreferencias where id_unidad=" . $idClin . " And envio_clinica=1 AND status_citas=1 Order by fecha_envio_clinica DESC LIMIT 50";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query))
        while ($row = mysql_fetch_array($query)) {
            $ret[] = $row;
        }
    return $ret;
}

function obtenerReferenciaCitaSinCont($idClin) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from contrarreferencias where id_unidad=" . $idClin . " And envio_clinica=1 AND status_citas=0 Order by fecha_envio_clinica DESC LIMIT 50";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query))
        while ($row = mysql_fetch_array($query)) {
            $ret[] = $row;
        }
    return $ret;
}

function insertarSQL($sql) {
    global $host_issste;
    global $baseDatos_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    mysql_query($sql);
    $ret[0] = mysql_errno();
    $ret[1] = mysql_error();
    return $ret;
}

function consultaSQL($sql) {
    global $host_issste;
    global $baseDatos_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or die(mysql_error());
    mysql_select_db($baseDatos_issste) or die(mysql_error());
    $query = mysql_query($sql) or die(mysql_error());
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function DatosDerechoHabiente($idDer) {
    global $host_issste;
    global $baseDatos_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or die(mysql_error());
    mysql_select_db($baseDatos_issste);
    $sql = "select * from derechohabientes where id_derecho=" . $idDer;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function DatosServicio($idServ) {
    global $host_issste;
    global $baseDatos_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or die(mysql_error());
    mysql_select_db($baseDatos_issste);
    $sql = "select * from servicios where id_servicio=" . $idServ;
    $query = mysql_query($sql);
    $error=  mysql_error();
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function ponerAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "(") {
            $temp = substr($Text, $j, 4);
            switch ($temp) {
                case "(/a)": $cadena .= "&aacute;";
                    $j = $j + 3;
                    break;
                case "(/A)": $cadena .= "&Aacute;";
                    $j = $j + 3;
                    break;
                case "(/e)": $cadena .= "&eacute;";
                    $j = $j + 3;
                    break;
                case "(/E)": $cadena .= "&Eacute;";
                    $j = $j + 3;
                    break;
                case "(/i)": $cadena .= "&iacute;";
                    $j = $j + 3;
                    break;
                case "(/I)": $cadena .= "&Iacute;";
                    $j = $j + 3;
                    break;
                case "(/o)": $cadena .= "&oacute;";
                    $j = $j + 3;
                    break;
                case "(/O)": $cadena .= "&Oacute;";
                    $j = $j + 3;
                    break;
                case "(/u)": $cadena .= "&uacute;";
                    $j = $j + 3;
                    break;
                case "(/U)": $cadena .= "&Uacute;";
                    $j = $j + 3;
                    break;
                case "(/n)": $cadena .= "&ntilde;";
                    $j = $j + 3;
                    break;
                case "(/N)": $cadena .= "&Ntilde;";
                    $j = $j + 3;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            $cadena.=$Text[$j];
        }
    }
    return $cadena;
}

function ObtenerDHS($sql) {
    global $baseDatos_issste;
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    $con = mysqli_connect($host_issste, $usuario_issste, $pass_issste, $baseDatos_issste);
    $query = mysqli_query($con, $sql);
    $ret = array();
    if (mysqli_num_rows($query) > 0) {
        while ($row = mysqli_fetch_assoc($query)) {
            $ret[] = $row;
        }
    }
    return $ret;
}

function buscarDHxCedulaParaBusqueda($ced) {

    $sql = "select * from derechohabientes where cedula like '" . $ced . "%' order by cedula_tipo ASC";
    $dhs = ObtenerDHS($sql);
    $tdh = count($dhs);
    $ret = "<select id='dh' name='dh' onchange='activarInc()'>";
    if ($tdh > 0)
        foreach ($dhs as $key => $value) {
            $ret.="<option value='" . $value['id_derecho'] . "|" . $value['cedula_tipo'] . "'>" . $value['cedula'] . "/" . $value['cedula_tipo'] . " " . ponerAcentos($value['ap_p']) . " " . ponerAcentos($value['ap_m']) . " " . ponerAcentos($value['nombres']) . "</option>";
        }
    else
        $ret.="<option value='-1'>No existe derechohabiente con la Cedula " . strtoupper($ced) . "</option>";
    $ret.="</select>";
    return $ret;
}

function buscarDHxNombreParaBusqueda($app, $apm, $nombres) {
    $sql = "select * from derechohabientes where ap_p like '%" . $app . "%' and ap_m like '%$apm%' and nombres like '%$nombres%' order by cedula_tipo ASC";
    $dhs = ObtenerDHS($sql);
    $tdh = count($dhs);
    $ret = "<select id='dh' name='dh' onchange='activarInc()'>";
    if ($tdh > 0)
        foreach ($dhs as $key => $value) {
            $ret.="<option value='" . $value['id_derecho'] . "|" . $value['cedula_tipo'] . "'>" . $value['cedula'] . "/" . $value['cedula_tipo'] . " " . ponerAcentos($value['ap_p']) . " " . ponerAcentos($value['ap_m']) . " " . ponerAcentos($value['nombres']) . "</option>";
        }
    else
        $ret.="<option value='-1'>No existe derechohabiente con la Cedula " . strtoupper($ced) . "</option>";
    $ret.="</select>";
    return $ret;
}

function getMedicoXid($idMed) {
    global $baseDatos_issste;
    global $host_issste;
    global $pass_issste;
    global $usuario_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from medicos where id_medico=$idMed";
    $result = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($result) > 0)
        $ret = mysql_fetch_assoc($result);
    return $ret;
}

function getContraReferencia($idRef) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from contrarreferencias where id_contra=$idRef";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);

    return $ret;
}

function nombreServicio($idServ) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from servicios where id_servicio=$idServ";
    $query = mysql_query($sql);
    $ret = "";
    if (mysql_num_rows($query) > 0) {
        $rows = mysql_fetch_array($query);
        $ret = $rows['nombre'];
    }
    return $ret;
}

function datosServicioXCita($idCita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from citas as c join horarios as h on(c.id_horario=h.id_horario) where c.id_cita=" . $idCita;
    $result = mysql_query($sql);
    $servicio = array();
    if (mysql_num_rows($result) > 0) {
        $horarioCita = mysql_fetch_array($result);
        $servicio = DatosServicio($horarioCita['id_servicio']);
    }
    return $servicio;
}

function datosCita($idCita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from citas where id_cita=" . $idCita;
    $result = mysql_query($sql);
    $res = array();
    if (mysql_num_rows($result) > 0)
        $res = mysql_fetch_array($result);
    return $res;
}

function datosHorarioXcita($idHorario) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from horarios where id_horario=" . $idHorario;
    $result = mysql_query($sql);
    $res = array();
    if (mysql_num_rows($result) > 0)
        $res = mysql_fetch_array($result);
    return $res;
}

function citasXHorario($idHorario, $fecha) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from citas where id_horario=" . $idHorario . " and fecha_cita=" . $fecha;
    $result = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($result) > 0) {
        $ret = mysql_fetch_array($result);
    }
    return $ret;
}

function diaSemana($dia_num) {
    switch ($dia_num) {
        case 0: return "DOMINGO";
            break;
        case 7: return "DOMINGO";
            break;
        case 1: return "LUNES";
            break;
        case 2: return "MARTES";
            break;
        case 3: return "MIERCOLES";
            break;
        case 4: return "JUEVES";
            break;
        case 5: return "VIERNES";
            break;
        case 6: return "SABADO";
            break;
    }
}

function obtenerHorarioPrv($serv, $fecha/* ,$doctor */) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $dia = diaSemana(date("N", strtotime($fecha)));
    $sql = "select * from horarios where tipo_cita=0 and id_servicio=" . $serv . " and dia='$dia'  "/* and id_medico=$doctor */ . " order by hora_inicio ASC";
    $result = mysql_query($sql);
    $res = array();
    if (mysql_num_rows($result) > 0)
        while ($row = mysql_fetch_array($result)) {
            $horarioElim = obtenerHrEliminado($row['id_consultorio'], $row['id_servicio'], $row['id_medico']);
            if ($horarioElim)
                $res[] = $row;
        }
    return $res;
}

function ponerCeros($cant, $cuantos) {
    $aCeros = array("", "0", "00", "000", "0000", "00000", "000000", "0000000", "00000000", "000000000", "0000000000", "00000000000", "000000000000", "0000000000000", "00000000000000", "000000000000000");
    $tCant = strlen($cant);
    $cuantos = $cuantos - $tCant;
    return $aCeros[$cuantos] . $cant;
}

function existeDuplicaCita($query_query) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query = mysql_query($query_query) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    return $ret;
}

function existeDuplica($query_query) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query = mysql_query($query_query) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    return $ret;
}

function getCitaRecienAgregada($id_horario, $id_derecho, $fecha_cita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $id_horario . "' AND id_derecho='" . $id_derecho . "' AND fecha_cita='" . $fecha_cita . "' LIMIT 1";
    $query = mysql_query($query_query) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_horario' => $row_query['id_horario'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    return $ret;
}

function ejecutarLOG($log) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query_query = "INSERT INTO bitacora VALUES('" . date('Ymd') . "','" . date('Hi') . "','" . getRealIpAddr() . "','" . $log . "','" . $_SESSION['idUsuario'] . "')";
    $query = mysql_query($query_query); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    return $error;
}

function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function obtenerHrEliminado($idconsultorio, $idServicio, $idMedico) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from servicios_x_consultorio where id_consultorio=$idconsultorio and id_servicio=$idServicio and id_medico=$idMedico";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0)
        $ret = true;
    else {
        $ret = false;
    }
    return $ret;
}

function quitarAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "&") {
            $temp = substr($Text, $j, 8);
            switch ($temp) {
                case "&aacute;": $cadena .= "(/a)";
                    $j = $j + 7;
                    break;
                case "&Aacute;": $cadena .= "(/A)";
                    $j = $j + 7;
                    break;
                case "&eacute;": $cadena .= "(/e)";
                    $j = $j + 7;
                    break;
                case "&Eacute;": $cadena .= "(/E)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/i)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/I)";
                    $j = $j + 7;
                    break;
                case "&oacute;": $cadena .= "(/o)";
                    $j = $j + 7;
                    break;
                case "&Oacute;": $cadena .= "(/O)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/u)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/U)";
                    $j = $j + 7;
                    break;
                case "&ntilde;": $cadena .= "(/n)";
                    $j = $j + 7;
                    break;
                case "&Ntilde;": $cadena .= "(/N)";
                    $j = $j + 7;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            switch ($cara) {
                case "á": $cadena.="(/a)";
                    break;
                case "é": $cadena.="(/e)";
                    break;
                case "í": $cadena.="(/i)";
                    break;
                case "ó": $cadena.="(/o)";
                    break;
                case "ú": $cadena.="(/u)";
                    break;
                case "Á": $cadena.="(/A)";
                    break;
                case "É": $cadena.="(/E)";
                    break;
                case "Í": $cadena.="(/I)";
                    break;
                case "Ó": $cadena.="(/O)";
                    break;
                case "Ú": $cadena.="(/U)";
                    break;
                case "ñ": $cadena.="(/n)";
                    break;
                case "Ñ": $cadena.="(/N)";
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        }
    }
    return $cadena;
}

function HayMasMedicosXServicio($idServ) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select* from servicios_x_consultorio where id_servicio=$idServ group by id_medico";
    $query = mysql_query($sql);
    if (mysql_num_rows($query) > 1)
        $ret = true;
    else
        $ret = false;
    return $ret;
}

/* function getMedicoXid($id) {

  global $host_issste;
  global $usuario_issste;
  global $pass_issste;
  global $baseDatos_issste;
  mysql_connect($host_issste, $usuario_issste, $pass_issste);
  mysql_select_db($baseDatos_issste);
  $query_query = "SELECT * FROM medicos WHERE id_medico='" . $id . "'";
  $query = mysql_query($query_query) or die(mysql_error());
  $row_query = mysql_fetch_assoc($query);
  $totalRows_query = mysql_num_rows($query);
  if ($totalRows_query > 0) {
  $ret = array(
  'id_medico' => $row_query['id_medico'],
  'cedula' => $row_query['cedula'],
  'cedula_tipo' => $row_query['cedula_tipo'],
  'n_empleado' => $row_query['n_empleado'],
  'ced_pro' => $row_query['cedula_profesional'],
  'titulo' => $row_query['titulo'],
  'ap_p' => $row_query['ap_p'],
  'ap_m' => $row_query['ap_m'],
  'nombres' => $row_query['nombres'],
  'turno' => $row_query['turno'],
  'telefono' => $row_query['telefono'],
  'direccion' => $row_query['direccion'],
  'tipo_medico' => $row_query['tipo_medico'],
  'id_servicio1' => $row_query['id_servicio1'],
  'id_servicio2' => $row_query['id_servicio2'],
  'hora_entrada' => $row_query['hora_entrada'],
  'hora_salida' => $row_query['hora_salida'],
  'intervalo_citas0' => $row_query['intervalo_citas0'],
  'intervalo_citas1' => $row_query['intervalo_citas1'],
  'intervalo_citas2' => $row_query['intervalo_citas2'],
  'observaciones' => $row_query['observaciones'],
  'st' => $row_query['st']
  );
  }
  return $ret;
  } */

function DatosUsuario($idUsuario) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from usuarios_contrarreferencias where id_usuario=$idUsuario";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_assoc($query);
    return $ret;
}

function ListaDiags() {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    $cie_bd = "sistema_cie10";
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($cie_bd);
    $sql = "select * from diagnostico_cie10";
    $ret = "";
    $res = mysql_query($sql);
    if (mysql_num_rows($res) > 0) {
        while ($row = mysql_fetch_assoc($res)) {
            $ret.="<option><a onclick='javascript:regresarDiagnostico(\"" . $row['Clave'] . "-" . ponerAcentos($row['Nombre']) . "\");' class='botones'>" . ponerAcentos($row['Nombre']) . "</a></option>";
        }
    }
    $ret.="<option><a href='javascript:regresarDiagnostico(\"otro\")'>Otro diagnostico</option>";
    return $ret;
}

function ExisteMedico($idMedico) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from medicos where id_medico=" . $idMedico;
    $query = mysql_query($sql);
    if (mysql_num_fields($query) > 0)
        $ret = true;
    else
        $ret = false;
    return $ret;
}

function consultaMultipleSQL($sql) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0) {
        while ($row = mysql_fetch_array($query)) {
            $ret[] = $row;
        }
    }
    return $ret;
}

function getHorarioXid($id) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query_query = "SELECT * FROM horarios WHERE id_horario='" . $id . "' ORDER BY id_horario";
    $query = mysql_query($query_query) or die($sql . "<br>" . mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_horario' => $row_query['id_horario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'dia' => $row_query['dia'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita']
        );
    }
    return $ret;
}

function getDatosDerecho($id_derecho) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query_query = "SELECT * FROM derechohabientes WHERE id_derecho='" . $id_derecho . "'";
    $query = mysql_query($query_query) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'cedula' => $row_query['cedula'],
            'cedula_tipo' => $row_query['cedula_tipo'],
            'ap_p' => $row_query['ap_p'],
            'ap_m' => $row_query['ap_m'],
            'nombres' => $row_query['nombres'],
            'fecha_nacimiento' => $row_query['fecha_nacimiento'],
            'telefono' => $row_query['telefono'],
            'direccion' => $row_query['direccion'],
            'estado' => $row_query['estado'],
            'municipio' => $row_query['municipio'],
            'status' => $row_query['status'],
            'unidad_medica' => $row_query['unidad_medica'],
            'Codigo_Postal' => $row_query['Codigo_Postal']
        );
    } else {
        $ret = array(
            'cedula' => "-1",
            'cedula_tipo' => "-1",
            'ap_p' => "-1",
            'ap_m' => "-1",
            'nombres' => "-1",
            'telefono' => "-1",
            'fecha_nacimiento' => "-1",
            'direccion' => "-1",
            'estado' => "-1",
            'municipio' => "-1",
            'status' => "-1"
        );
    }
    return $ret;
}

function getServicioXid($id) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query_query = "SELECT * FROM servicios WHERE id_servicio='" . $id . "'";
    $query = mysql_query($query_query) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = $row_query['nombre'];
    }
    return $ret;
}

function VistaCitasContestadas($nombre) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    $con = mysqli_connect($host_issste, $usuario_issste, $pass_issste, $baseDatos_issste) or die(mysqli_connect_error());
    mysqli_autocommit($con, FALSE);
    $sql = "SELECT ca.id_cita_ap,d.cedula,d.cedula_tipo,d.ap_p,d.ap_m,d.nombres,s.nombre AS servicio,dr.ap_p AS apDoc,dr.ap_m AS amDoc,dr.nombres AS nomDoc,ca.fecha_apartada,c.fecha_cita,ca.hora_cita,ca.aceptada,ca.motivo_rechazo,ca.carpeta,cl.nombre AS clinica,ca.id_unidad,ar.archivo,ar.ubicacion
FROM citas_apartadas AS ca,citas AS c,derechohabientes AS d,medicos AS dr,servicios AS s,vgf AS cl,archivos_referencia AS ar
WHERE ca.`id_derecho`=d.`id_derecho` AND ca.`id_servicio`=s.`id_servicio` AND ca.`id_unidad`=cl.`id_unidad` AND ca.`id_medico`=dr.`id_medico` AND (ca.`id_cita`=ar.`id_cita` OR ca.`carpeta`=ar.`id_cita`) and c.id_cita=ca.id_cita";
    $sq1 = "Create or Replace view $nombre as " . $sql;
    $query = mysqli_query($con, $sq1);
    $ret = array(mysqli_errno($con), mysqli_errno($con));
    return $ret;
}

function ObtenerSQLCitasContestadas($fecha1, $fecha2, $clinica, $filtroFecha, $filtro) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    /*$sql = "SELECT ca.id_cita_ap,d.cedula,d.cedula_tipo,d.ap_p,d.ap_m,d.nombres,s.nombre AS servicio,dr.ap_p AS apDoc,dr.ap_m AS amDoc,dr.nombres AS nomDoc,ca.fecha_apartada,c.fecha_cita,ca.hora_cita,ca.aceptada,ca.motivo_rechazo,ca.carpeta,cl.nombre AS clinica,ca.id_unidad,ar.archivo,ar.ubicacion
FROM citas_apartadas AS ca INNER JOIN derechohabientes AS d ON(ca.`id_derecho`=d.`id_derecho`)
LEFT JOIN servicios AS s ON(ca.`id_servicio`=s.id_servicio)
INNER JOIN medicos AS dr ON(ca.`id_medico`=dr.`id_medico`)
INNER JOIN (SELECT id_cita,id_horario,fecha_cita FROM citas UNION SELECT id_cita,id_horario,fecha_cita FROM citas_rechazadas) AS c ON(ca.`id_cita`=c.id_cita)
INNER JOIN vgf AS cl ON(ca.`id_unidad`=cl.`id_unidad`)
INNER JOIN archivos_referencia AS ar ON(ca.`id_cita`=ar.`id_cita` OR ca.`carpeta`=ar.`id_cita`)
";*/
    $sql="select * from citas_apartadas as ca";
    switch ($filtro) {
        case 'T':
            $sql.=" where not ca.aceptada=-1";
            break;
        case 'C':
            $sql.=" where not ca.aceptada=-1 and ca.id_unidad=$clinica";
            break;
        case 'A':
            $sql.=" where ca.aceptada=1";
            break;
        case 'R':
            $sql.=" where ca.aceptada=0";
            break;
        case 'Rep':
            $sql.=" where ca.aceptada=2";
            break;
    }
    if ($filtroFecha == "F") {
        $sql.=" and ca.fecha_apartada>=" . $fecha1 . " and ca.fecha_apartada<=" . $fecha2;
    }
    //$sql.=" GROUP BY ca.id_cita_ap";
    /*echo $sql;
    $con = mysqli_connect($host_issste, $usuario_issste, $pass_issste, $baseDatos_issste) or die(mysqli_connect_error($con));
    mysqli_autocommit($con, FALSE);
    $query =  /*mysqli_query($con, $sql);mysql_query($sql) or die(mysql_error());
    $ret = array();
    while ($row = mysql_fetch_assoc($query)) {
        $ret[] = $row;
    }
    return $ret;*/
 return $sql;
}

function ObtenerConsultasCitasContestadas($sql,$l1,$c) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql.=" LIMIT $l1,$c";
    //$con = mysqli_connect($host_issste, $usuario_issste, $pass_issste, $baseDatos_issste) or die(mysqli_connect_error($con));
    //mysqli_autocommit($con, FALSE);
    $query = /* mysqli_query($con, $sql) */mysql_query($sql) or die(mysql_error());
    return $query;
}

function obtenerUnidadMedica($idUnidadMedica) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdissste);
    $sql = "select * from vgf where id_unidad='$idUnidadMedica'";
    $query = mysql_query($sql);
    if (mysql_num_rows($query) > 0) {
        $ret = mysql_fetch_array($query);
    }
    else
        $ret = array();
    return $ret;
}
?>
