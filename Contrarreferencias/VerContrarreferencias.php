<!Doctype html>
<html>
    <head>
        <style type="text/css">
            @import url("lib/busqueda.css") print;
        </style>
    </head>
    <body>
        <table class="contrarreferencias" border="1"><tr>
                <th class="Enccontra">Folio Contrarreferencia</th>
                <th class="Enccontra">Fecha de elaboraci&oacute;n</th>
                <th class="Enccontra">Servicio</th>
                <th class="Enccontra">C&eacute;dula</th>
                <th class="Enccontra">Nombre del Derechohabiente</th>
                <th class="Enccontra">M&eacute;dico</th>
                <th class="Enccontra">Clinica de Referencia</th>
                <th class="Enccontra">Fecha de Envio a Clinica</th>
                <th class="Enccontra">Enviar a Clinica</th>
                <th class="Enccontra">Imprimir Formato</th>
            </tr>
            <?php
            error_reporting(E_ALL^E_NOTICE);
            set_time_limit(1000);
            include 'Connections/bdissste.php';
            include 'lib/misFunciones.php';
            $con=mysqli_connect($hostname_bdissste, $username_bdissste, $password_bdissste,$database_bdissste);
            $opcion = $_REQUEST['tipoBusq'];
            $fecha_iniB = $_REQUEST['fecha1'];
            $fecha_ini = date("Ymd", strtotime($fecha_iniB));
            $fecha_finB = $_REQUEST['fecha2'];
            $fecha_fin = date("Ymd", strtotime($fecha_finB));
            $sql="SELECT con.id_contra,con.folio_contrarreferencia,dh.cedula,dh.cedula_tipo,dh.ap_p,dh.ap_m,dh.nombres,s.nombre AS servicio,m.ap_p AS apMed,m.ap_m AS amMed,m.nombres AS nomMed,cl.nombre AS clinica,con.fecha_contrarreferencia,
con.fecha_envio_clinica,con.envio_clinica FROM contrarreferencias AS con,derechohabientes AS dh,servicios AS s,medicos AS m,vgf AS cl 
WHERE (con.`id_unidad`=cl.`id_unidad` AND con.`id_derecho`=dh.`id_derecho` AND con.`id_servicio`=s.`id_servicio` AND con.`id_medico`=m.`id_medico`) AND ";
            switch ($opcion){
                case "todas":
                $sql.= " con.fecha_contrarreferencia<=$fecha_fin And con.fecha_contrarreferencia>=$fecha_ini";
                    break;
                case "unidad":
                $clinica .= $_REQUEST['filClinica'];
                if ($clinica == 0)
                    $sql .= " cl.`Estado`='".$_REQUEST['estado']."' AND con.fecha_contrarreferencia<=$fecha_fin AND con.fecha_contrarreferencia>=$fecha_ini";
                else{
                    $umf = $_REQUEST['unidad'];
                    $sql .= "con.id_unidad=" . $umf . " AND con.fecha_contrarreferencia<=$fecha_fin And con.fecha_contrarreferencia>=$fecha_ini";
                }
            break;
                case "servicio":
                $id_servicio = $_REQUEST['id_servicio'];
                $sql .= " con.id_servicio=" . $id_servicio . " AND con.fecha_contrarreferencia<=$fecha_fin And con.fecha_contrarreferencia>=$fecha_ini";
                break;
                case "medico":
                $idMedico = $_REQUEST['idMedico'];
                $sql .= " con.id_medico=" . $idMedico . " AND con.fecha_contrarreferencia<=$fecha_fin And con.fecha_contrarreferencia>=$fecha_ini";
                break;
            }
            switch ($_REQUEST['enviado'])
            {
                case 0:
                    $sql.=" and con.envio_clinica=0";
                    break;
                case 1:
                    $sql.=" and con.envio_clinica=1";
            }
            $query = mysqli_query($con,$sql);
            if (mysqli_errno($con) == 0) {
                if (mysqli_num_rows($query) > 0) {
                    $ret = "";
                    $ref=0;
                    while ($contraref = mysqli_fetch_assoc($query)) {
                        if($contraref['envio_clinica']==0)
                       $ret.="<tr><td>" . $contraref['id_contra'] . "<input type='hidden' id='ref$ref' value='".$contraref['id_contra']."' /></td>
                            <td class='Azul'>" . date("d/m/y", strtotime($contraref['fecha_contrarreferencia'])) . "</td>
                            <td class='Azul'>".$contraref['servicio']."</td>
                            <td>" . $contraref['cedula'] . "/" . $contraref['cedula_tipo'] . "</td>
                            <td>" . ponerAcentos($contraref['ap_p'] . " " . $contraref['ap_m'] . " " . $contraref['nombres']) . "</td>
                            <td class='Azul'>" . ponerAcentos($contraref['apMed'] . " " . $contraref['amMed'] . " " . $contraref['nomMed']) . "</td>
                            <td>" . htmlentities(quitarAcentos($contraref['clinica'])) . "</td>
                            <td>&nbsp;</td>
                            <td><div id='env$ref'><button id='enviar$ref' onclick='enviarRefaClin($ref)' >Enviar</button></div></td>
                            <td><button id='imprimir$ref' onclick='VerReferencia($ref)'>imprimir</button></td></tr>";
                   else {
                       $ret.="<tr><td>" . $contraref['id_contra'] . "<input type='hidden' id='ref$ref' value='".$contraref['id_contra']."' /></td>
                            <td class='Azul'>" . date("d/m/y", strtotime($contraref['fecha_contrarreferencia'])) . "</td>
                            <td class='Azul'>".$contraref['servicio']."</td>
                            <td>" . $contraref['cedula'] . "/" . $contraref['cedula_tipo'] . "</td>
                            <td>" . ponerAcentos($contraref['ap_p'] . " " . $contraref['ap_m'] . " " . $contraref['nombres']) . "</td>
                            <td class='Azul'>" . ponerAcentos($contraref['apMed'] . " " . $contraref['amMed'] . " " . $contraref['nomMed']) . "</td>
                            <td>" . htmlentities(quitarAcentos($contraref['clinica'])) . "</td>
                            <td>" . date("d/m/Y", strtotime($contraref['fecha_envio_clinica'])) . "</td>
                            <td><div id='env$ref'>Enviado</div></td>
                            <td><button id='imprimir$ref' onclick='VerReferencia($ref)'>imprimir</button></td></tr>";
                   }
                       $ref++;
                    }
                }
                else
                    $ret = "<tr><td colspan='10'>No se encontraron referencias con los datos especificados</td></tr>";
            }
            print_r($ret);
            ?>
        </table>
        <br/><a href='javascript:regresar();'><img src='diseno/flechaIzq.png' /></a>
    </body>
</html>