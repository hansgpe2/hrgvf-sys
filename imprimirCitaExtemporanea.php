<?php
error_reporting(E_ALL^E_NOTICE);
session_start ();
include_once('lib/misFunciones.php');

function getCitaExtemporaneaParaImprimir($id_consultorio, $id_servicio, $id_derecho, $fecha_cita, $id_usuario, $hora_inicio, $hora_fin) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "SELECT * FROM citas_extemporaneas WHERE id_consultorio='" . $id_consultorio . "' AND id_servicio='" . $id_servicio . "' AND id_derecho='" . $id_derecho . "' AND fecha_cita='" . $fecha_cita . "' AND id_usuario='" . $id_usuario . "' AND hora_inicio='" . $hora_inicio . "' AND hora_fin='" . $hora_fin . "' LIMIT 1";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	if ($totalRows_query>0){
		$ret=array(
					'id_cita' => $row_query['id_cita'],
					'id_consultorio' => $row_query['id_consultorio'],
					'id_servicio' => $row_query['id_servicio'],
					'id_medico' => $row_query['id_medico'],
					'id_derecho' => $row_query['id_derecho'],
					'hora_inicio' => $row_query['hora_inicio'],
					'hora_fin' => $row_query['hora_fin'],
					'tipo_cita' => $row_query['tipo_cita'],
					'fecha_cita' => $row_query['fecha_cita'],
					'status' => $row_query['status'],
					'observaciones' => $row_query['observaciones'],
					'id_usuario' => $row_query['id_usuario']
				);
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}



	$datosCita = getCitaExtemporaneaParaImprimir($_GET['id_consultorio'], $_GET['id_servicio'], $_GET['id_derecho'], $_GET['fecha_cita'], $_GET['id_usuario'], str_replace(":", "", $_GET['fecha_inicio']), str_replace(":", "", $_GET['fecha_fin']));
	$datosDH = getDatosDerecho($_GET['id_derecho']);
//	$datosHorario = getHorarioXid($_GET['id_horario']);
	$datosServicio = getServicioXid($_GET['id_servicio']);
	$datosOperador = getUsuarioXid($_GET['id_usuario']);
	$datosDr = getMedicoXid($datosCita["id_medico"]);
	$fecha = formatoDia($_GET['fecha_cita'],"imprimirCita");
	
	$fechaHora = $fecha . " " . formatoHora($_GET['hora_inicio']) . " hrs.";
	$servicioConsultorio = $datosServicio['nombre'];
	$nombre = strtoupper($datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']);
	$nombreDr = strtoupper($datosDr['ap_p'] . " " . $datosDr['ap_m'] . " " . $datosDr['nombres']);
	$datosAgendo = strtoupper($datosOperador['nombre']) . "-" . date('d/m/Y - H:i:s');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Cita</title>
<link href="lib/impresionEtiquetas.css" media="print" rel="stylesheet" type="text/css" />
</head>

<body onload="javascript: window.print();" style="width:2in; font-family:Arial, Helvetica, sans-serif; font-size:9px">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" width="45"><img src="diseno/logoEncabezado.png" width="30" height="30" /></td>
    <td class="issste" align="center">HOSP. REGIONAL “DR. VALENT&Iacute;N G&Oacute;MEZ FAR&Iacute;AS”<br /><span>CONSULTA EXTERNA</span></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2" align="center"><b><?php echo  ponerAcentos($nombre) ?></b></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%" align="left"><b>D&Iacute;A: </b><?php echo $fecha ?></td><td align="left"><b>Hora:</b> <?php echo formatoHora($datosCita['hora_inicio']) ?> Hrs.</td></tr></table></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2" align="left"><b>Serv.</b> <?php echo ponerAcentos($servicioConsultorio)  ?></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2" align="left"><b>Dr.</b> <?php echo ponerAcentos($nombreDr)  ?></td>
  </tr>
  <tr>
    <td class="quienAgenda" colspan="2"><b>Capt. </b><?php echo ponerAcentos($datosAgendo) ?></td>
  </tr>
</table>
</body>
</html>
