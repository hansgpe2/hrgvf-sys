<?php
session_start();
include_once('lib/misFunciones.php');
if($_SESSION['idUsuario'] > 0) { 

function getEstudios($id_servicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM laboratorio_catalogo WHERE id_familia='" . $id_servicio . "' AND status='1' ORDER BY clave";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
               $ret[] = array(
                   'id_estudio' => $row_query['id_estudio'],
                   'clave' => $row_query['clave'],
                   'descripcion' => $row_query['descripcion'],
                   'id_familia' => $row_query['id_familia'],
                   'dias_entrega' => $row_query['dias_entrega'],
                   'status' => $row_query['status'],
                   'extra1' => $row_query['extra1'],
                   'extra2' => $row_query['extra2']
               );
        } while ($row_query = mysql_fetch_assoc($query));
    }

    return $ret;
}

function getServiciosEstudios() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM laboratorio_servicios WHERE status='1' ORDER BY clave";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
               $ret[] = array(
                   'id_servicioL' => $row_query['id_servicioL'],
                   'clave' => $row_query['clave'],
                   'descripcion' => $row_query['descripcion'],
                   'status' => $row_query['status'],
                   'extra' => $row_query['extra']
               );
        } while ($row_query = mysql_fetch_assoc($query));
    }

    return $ret;
}

$datosEstudio = '';
if ($_SESSION['IdCon'] == "118") { // 118 es el id de consultorio de radiología e imagen
	$estudios = getEstudios($_SESSION['idServ']);
	$liEstudios = '<option value="">Selecciona...</option>';
	$tEstudios = count($estudios);
	if ($tEstudios > 0) {
		for ($i=0; $i<$tEstudios; $i++) {
			$descripcion = $estudios[$i]['clave'] . ' - ' . $estudios[$i]['descripcion'];
			$liEstudios .= '<option value="' . $estudios[$i]['id_estudio'] . '">' . $descripcion . '</option>';
		}
	}

	$services = getServiciosEstudios();
	$tservices = count($services);
	$liServices = '<option value="">Selecciona...</option>';
	if ($tservices > 0) {
		for ($i=0; $i<$tservices; $i++) {
			$descripcion = $services[$i]['clave'] . ' - ' . $services[$i]['descripcion'];
			$liServices .= '<option value="' . $services[$i]['id_servicioL'] . '">' . $descripcion . '</option>';
		}
	}
	$datosEstudio .= '
  <tr>
  	<td height="25" class="textosParaInputs" align="right" valign="top">ESTUDIO A REALIZAR:</td>
	<td align="left">
<div class="ui-widget">
  <select id="combobox">
                                ' . $liEstudios . '
  </select>
</div>
	</td>
  </tr>
  <tr>
  	<td height="25" class="textosParaInputs" align="right" valign="top">SERVICIO SOLICITA:</td>
	<td align="left">
<div class="ui-widget">
  <select id="combobox2">
                                ' . $liServices . '
  </select>
</div>
	</td>
  </tr>
	';
}


function opcionesCitas() {
	$btns = '';
	switch ($_SESSION['tipoUsuario']) {
		case '3':
		case '1':
			$btns = '
					<option value="0">Primera Vez</option>
					<option value="1">Subsecuente</option>
					<option value="2">Procedimiento</option>
					<option value="3">Administrativo</option>
					<option value="4">Telemedicina</option>
					<option value="5">Cita Medicamento</option>
			';
			break;
		case '5':
			$btns = '
					<option value="0">Primera Vez</option>
					<option value="4">Telemedicina</option>
			';
			break;
		case '6':
			$btns = '
					<option value="1">Subsecuente</option>
					<option value="2">Procedimiento</option>
			';
			break;
		case '7':
			$btns = '
					<option value="3">Administrativo</option>
			';
			break;
		case '8':
			$btns = '
					<option value="2">Procedimiento</option>
			';
			break;
	}
	return $btns;
}

$opcionesCita = opcionesCitas();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarAgregarCitaExtemporanea2014();">
<input name="id_derecho" id="id_derecho" type="hidden" value="" />
<input name="fechaCita" id="fechaCita" type="hidden" value="<?php echo $_GET['getdate']; ?>" />
<input name="id_cita" id="id_cita" type="hidden" value="" />
<input name="tipo_reprogramacion" id="tipo_reprogramacion" type="hidden" value="" />
<input name="estudi_1" id="estudi_1" type="hidden" value="" />

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CITA EXTEMPORANEA PARA EL <?php echo formatoDia($_GET['getdate'], 'tituloCitasXdia'); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">HORA INICIO:</td>
    <td align="left"><input name="hora_inicio" type="text" id="hora_inicio" size="10" maxlength="5" />
    <span class="textosParaInputs">HORA FIN:</span> <input name="hora_fin" type="text" id="hora_fin" size="10" maxlength="5" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CODIGO DE BARRAS: </td>
    <td align="left"><input id="cod_bar" name="cod_bar" type="text" value="" maxlength="8" onkeypress="javascript: return codigoBarras(event);" onkeyup="javascript: buscarCodigoBarrasExp();"  /> <input name="buscar" type="button" value="Buscar C&oacute;digo..." onclick="javascript: buscarCodigoBarrasExp();" class="botones" /><div id="buscando" style="display:inline" class="error">introduce 8 digitos</div></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><input name="cedula" type="text" id="cedula" maxlength="13" readonly="readonly" />
     <input name="seleccionar" type="button" class="botones" id="seleccionar" onClick="javascript:  ocultarDiv('divAgregarDH'); ocultarDiv('reprogramar'); mostrarDiv('buscar'); getElementById('cedulaBuscar').focus(); document.getElementById('buscar').style.height = '150px';
" value="Buscar Paciente">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <input name="botonReprogramar" type="button" class="botones" id="botonReprogramar" onclick="javascript:  ocultarDiv('divAgregarDH'); ocultarDiv('buscar'); mostrarDiv('reprogramar'); cargarCitasAReprogramar();" value="Reprogramar Cita Aqu&iacute;" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SEXO</td>
    <td><select name="sexo1" disabled="disabled" id="sexo1" onchange="ObtenerTpoDerXSexo(this.id,'cedulaTipoCita');">
      <option value="H">Hombre</option>
      <option value="M">Mujer</option>
    </select>
      &nbsp;<span class="textosParaInputs">TIPO DE DERECHOHABIENTE</span>&nbsp;
      <select name="cedulaTipoCita" id="cedulaTipoCita" disabled="disabled">
      </select></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_p" type="text" id="ap_p" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="ap_m" type="text" id="ap_m" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="nombre" type="text" id="nombre" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><input name="telefono" type="text" id="telefono" size="20" maxlength="10" readonly="readonly" /><span class="textosParaInputs"> FECHA NAC. </span><input name="fecha_nac" type="text" id="fecha_nac" size="20" maxlength="10" readonly="readonly" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccion" type="text" id="direccion" size="64" maxlength="50" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right"> COLONIA</td>
    <td><input name="col1" type="text" id="col1" onkeyup="this.value=this.value.toUpperCase();" readonly="readonly"  />
      <span class="textosParaInputs">CODIGO POSTAL</span>
      <input name="cp1" type="text" id="cp1" size="5" maxlength="5" readonly="readonly"  /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">E-MAIL:</td>
    <td align="left"><input name="emailCita" type="text" id="emailCita" value="" size="30" readonly="readonly"  />
      <span class="textosParaInputs">CELULAR</span>
      <input name="cel1" type="text" id="cel1" value="" size="10" maxlength="10" readonly="readonly" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estado" id="estado" onchange="javascript: cargarMunicipios(this.value,'municipio');" disabled="disabled"> 
    </select><span class="textosParaInputs">MUNICIPIO: </span><select name="municipio" id="municipio" disabled="disabled">
    </select>
    </td>
  </tr>
    <tr><td align="right"><span class="textosParaInputs">UNIDAD MEDICA DE ADSCRIPCION:</span></td><td align="left"><select id="UnidadCita" name="UnidadCita" disabled></select>
            </td></tr>
  <tr>
    <td>&nbsp;</td>
    <td align="left"><div id="modificarDH" style="display:'';"><input type="button" name="modificar" id="modificar" value="Modificar Datos del Derechohabiente" class="botones" disabled="disabled" onclick="javascript: habilitarParaModificarDH();" /></div><div id="modificarDHGuardar" style="display:none"><input name="guardarMod" type="button" value="Guardar Modificaciones" class="botones" onclick="javascript: guardarModificacionesDH();" /> <input name="cancelarMod" type="button" value="Cancelar" class="botones" onclick="javascript: cancelarModificacionesDH();" /></div></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">TIPO DE CITA:</td>
    <td align="left"><select name="tipo_cita" id="tipo_cita"><?php echo $opcionesCita; ?>
    </select></td>
  </tr>
      <tr>
        <td class="textosParaInputs" align="right"><input name="concertada" type="checkbox" id="concertada" value="concertada" /></td>
        <td align="left">Cita concertada por el derechohabiente (solo en 1ra vez)</td>
      </tr>
  <tr>
    <td class="textosParaInputs" align="right">DIAGNOSTICO:</td>
    <td align="left"><input type="text" name="diagnostico" id="diagnostico" onkeyup="this.value = this.value.toUpperCase();" size="80" maxlength="150" /><br />Llenar Diagn&oacute;stico solo si es cita de 1ra vez</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">OBSERVACIONES:</td>
    <td align="left"><input type="text" name="observaciones" id="observaciones" disabled="disabled" onkeyup="this.value = this.value.toUpperCase();" size="80" maxlength="150" /></td>
  </tr>
  <?php echo $datosEstudio; ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="selDia('<?php echo $_GET['getdate']; ?>','<?php echo $_GET['getdate']; ?>');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Agregar Cita" class="botones" disabled="disabled" />
  <br /><br /></td>
  </tr>
  <tr>
    <td colspan="2" align="left">
  <input type="button" name="imprimir_cb" id="imprimir_cb" value="Imprimir C&oacute;digo de barras" class="botones" onclick="javascript: imprimir_exp();" />
  </tr>
</table>
</form>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="reprogramar" style="display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">REPROGRAMAR CITA</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
            <td colspan="2">
                <form id="selDH" method="POST" action="javascript: buscarDH(document.getElementById('cedulaBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">CITA: </td>
                  <td align="left"><div id="listaCitasAreprogramar">&nbsp;</div></td>
                </tr>
                <tr>
                  <td colspan="2" align="center"><input name="cerrarReprogramar" type="button" class="botones" id="cerrarReprogramar" onclick="javascript: ocultarDiv('reprogramar');" value="Cerrar" />
                  &nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarCitaReprogramar" type="button" class="botones" id="seleccionarCitaReprogramar" onclick="javascript: cargarDatosCitaReprogramar();" value="Seleccionar Cita" />
                    <br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>


<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="buscar" style=" display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">BUSCAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
              <td><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();">
                <option value="cedula" selected="selected">C&eacute;dula</option>
                <option value="nombre">Nombre</option>
              </select>
              </td>
            </tr>
            <tr>
            <td colspan="2">
            	<div id="buscarPorCedula">
                <form id="selDH" method="POST" action="javascript: buscarDH(document.getElementById('cedulaBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                  <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                    <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="2" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
              </div>

                <div id="buscarPorNombre" style="display:none;">
                <form id="selDHN" method="POST" action="javascript: buscarDHN(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                  <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                  <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                  <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                    <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="4" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="4" align="center"></td>
                </tr>
              </table>
              </form>
              </div>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>
<form id="agregarDH" method="POST" action="javascript: agregarDHenCitaForma();">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
       <div id="divAgregarDH" style="display:none; height:0px; margin-top:10px;">
          <table width="800" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
              <td align="left"><input type="text" name="cedulaAgregar" id="cedulaAgregar" maxlength="10" /></td>
            </tr>
            <tr><td  align="right" class="textosParaInputs">SEXO</td><td><select name="sexo" id="sexo" onchange="ObtenerTpoDerXSexo(this.id,'cedulaTipoAgregar')">
              <option value="-1" selected="selected"></option>
              <option value="M">Mujer</option>
              <option value="H">Hombre</option>
              </select>
                        &nbsp;<span class="textosParaInputs">TIPO DE CEDULA</span>&nbsp;<select name="cedulaTipoAgregar" id="cedulaTipoAgregar" onfocus="ObtenerTpoDerXSexo('sexo', this.id);">
              </select></td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right" colspan="4">NOMBRE DE DERECHOHABIENTE</td></tr>
    <tr height="25"><td class="textosParaInputs" align="right">APELLIDO PATERNO</td>
    <td align="left"><input name="ap_pAgregar" type="text" id="ap_pAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" value="Apellido Paterno" onfocus="javascript:borrarReferencias(this);" /><span class="textosParaInputs">APELLIDO MATERNO</span>
      <input name="ap_mAgregar" type="text" id="ap_mAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" value="Apellido Materno" onfocus="javascript:borrarReferencias(this);" /></td></tr>
     <tr><td class="textosParaInputs" align="right">NOMBRES</td><td>
      <input name="nombreAgregar" type="text" id="nombreAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" value="Nombre(s)" onfocus="javascript:borrarReferencias(this);" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><input name="telefonoAgregar" type="text" id="telefonoAgregar" size="20" maxlength="10" value="" /><span class="textosParaInputs"> F. NAC. </span><span style="position:relative">
        <label for="mes1">mes</label>
        <select name="mes1" id="mes1" tabindex="8" onchange="calendarios('1');">
          <option value="-1"></option>
          <option value="01">ene</option>
          <option value="02">feb</option>
          <option value="03">mar</option>
          <option value="04">abr</option>
          <option value="05">may</option>
          <option value="06">jun</option>
          <option value="07">jul</option>
          <option value="08">ago</option>
          <option value="09">sept</option>
          <option value="10">oct</option>
          <option value="11">nov</option>
          <option value="12">dic</option>
        </select>
        <label for="dia1">dia</label>
        <select name="dia1" id="dia1" tabindex="9">
          <option value="-1"></option>
          <option value="01">1</option>
          <option value="02">2</option>
          <option value="03">3</option>
          <option value="04">4</option>
          <option value="05">5</option>
          <option value="06">6</option>
          <option value="07">7</option>
          <option value="08">8</option>
          <option value="09">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
        </select>
        <label for="anio1">año</label>
        <select name="anio1" id="anio1" tabindex="10">
        <?php
			$ini=date("Y");
			$fin=$ini-150;
			for($i=$ini;$i>$fin;$i--)
			{
				echo "<option value='$i'>$i</option>";
			}
		?></select>
      </span>
    </td>
</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" size="64" maxlength="50" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
 <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estadoAgregar" id="estadoAgregar" tabindex="14" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');">
      <script type="text/javascript">
                cargarEstados('estadoAgregar');
            </script>
    </select>
      <span class="textosParaInputs">MUNICIPIO: </span>
      <select name="municipioAgregar" id="municipioAgregar" tabindex="15" onchange="vaciarSelect('col');vaciarSelect('cp');obtenerCodigoPostal('estadoAgregar',this.id,'col');cargarColonias('col','cp', 'municipioAgregar','estadoAgregar');">
      </select></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right"> COLONIA</td>
    <td><select name="col" id="col" tabindex="16" onchange="obtenerCodigoPostal('estadoAgregar','municipioAgregar',this.id);">
    </select>
      <span class="textosParaInputs">CODIGO POSTAL</span>
      <select name="cp" id="cp" tabindex="17" onchange="cargarColonias('col',this.id, 'municipioAgregar','estadoAgregar')"  value="0" >
      </select></td>
      </tr>
  <tr>
    <td><span class="textosParaInputs">UNIDAD MEDICA DE ADSCRIPCIÓN:</span></td>
    <td align="left"><select name="Unidad" id="Unidad" tabindex="18">
    </select></td>
    </tr>
        <tr>
          <td class="textosParaInputs" align="right">E-MAIL:</td>
          <td align="left"><input name="emailAgregar" type="text" id="emailAgregar" size="30" tabindex="12" />
            <span class="textosParaInputs">CELULAR</span>
            <input type="text" name="cel" value="" maxlength="10" size="10" id="cel" tabindex="13" /></td>
          </tr>
        
            <tr>
              <td class="textosParaInputs" align="right">&nbsp;</td>
              <td align="left">&nbsp;</td>
              </tr>
            <tr>
              <td colspan="2" align="center"><div id="divBotones_EstadoAgregarDH"></div>
                </td>
              </tr>
        
            <tr>
              <td colspan="2" align="center"></td>
            </tr>
          </table>
        </div>
	</td>
</tr>
</table>
</form>

</body>
</html>
<?php
} else {
	$_SESSION['idUsuario'] = "-1";
	$_SESSION['tipoUsuario'] = "-1";
	$_SESSION['IdCon'] = "-1";
	$_SESSION['idServ'] = "-1";
	$_SESSION['idDr'] = "-1";
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Tu sesion ha caducado'); location.replace('index.php');</script>";
}
?>