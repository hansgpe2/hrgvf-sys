<?php

session_start();
include_once('lib/misFunciones.php');
if ($_SESSION['idUsuario'] > 0) {
?>
    <?php

//Obtenemos la fecha actual:
    $fecha = time();
//Queremos sumar 2 horas a la fecha actual:
    $horas = 0;
// Convertimos las horas a segundos y las sumamos:
    $fecha += ($horas * 60 * 60);
// Le damos al resultado el formato deseado:
    $fechaYhora = date('d/m/Y - H:i', $fecha);
    if ($_GET['id_cita'] != "") { // SE VA PASAR UNA CITA POR REPROGRAMAR A CITA REPROGRAMADA
        $citaMismoDia = existeDuplica("SELECT * FROM citas WHERE fecha_cita='" . $_GET['fechaCita'] . "' AND id_derecho='" . $_GET['id_derecho'] . "'");
        if (!$citaMismoDia) {
            $duplica = existeDuplica("SELECT * FROM citas WHERE id_horario='" . $_GET['idHorario'] . "' AND fecha_cita='" . $_GET['fechaCita'] . "'");
            // agregar en citas
            if (!$duplica) {
                // VER SI ES CITA A REPROGRAMAR O CITA A REPROGRAMAR REZAGADA
                if ($_GET['tipo_reprogramacion'] == "RP") { // si es a reprogramar normal
                    $query_query = "INSERT INTO citas VALUES (NULL,'" . $_GET['idHorario'] . "','" . $_GET['id_derecho'] . "','" . $_GET['fechaCita'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',0);";
                    $res = ejecutarSQL($query_query);
                    $log = ejecutarLOG("cita reprogramada | horario: " . $_GET['idHorario'] . " | dh: " . $_GET['id_derecho'] . " | fecha: " . $_GET['fechaCita'] . " | status: 1 | observaciones: " . $_GET['obs']);
                    if ($res[0] == 0) {// no hay error
                        // marcar en citas por reprogramar en status = 9
                        $query_query = "UPDATE citas_por_reprogramar SET status='9' WHERE id_cita='" . $_GET['id_cita'] . "'";
                        $log = ejecutarLOG("update de citas por reprogramar | cita : " . $_GET['id_cita']);
                        $res2 = ejecutarSQL($query_query);
                        if ($res2[0] == 0) {// no hay error
                            // agregar en citas reprogramadas incluyendo los datos de la cita original y la cita nueva
                            $cita = getCitaRecienAgregada($_GET['idHorario'], $_GET['id_derecho'], $_GET['fechaCita']);
                            $citaAnterior = getcitasAreprogramarXid($_GET['id_cita']);
                            $query_query = "INSERT INTO citas_reprogramadas VALUES ('" . $cita['id_cita'] . "','" . $_GET['idHorario'] . "','" . $_GET['id_derecho'] . "','" . $_GET['fechaCita'] . "','" . $_GET['id_cita'] . "','" . $citaAnterior['id_horario'] . "','" . $citaAnterior['fecha_cita'] . "','" . $citaAnterior['status'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','');";
                            $res3 = ejecutarSQL($query_query);
                            $horario = getHorarioXid($_GET['idHorario']);
                            if ($horario['tipo_cita'] == 0) { // si el tipo de cita es 1ra vez
                                // si es 1ra vez agregar campo diagnostico N = normal E = extraordinaria
                                $query_query = "INSERT INTO citas_detalles VALUES ('" . $cita['id_cita'] . "','N','" . $_GET['diagnostico'] . "','" . $_GET['concertada'] . "','');";
                                $res = ejecutarSQL($query_query);
                            }
                            print($cita['id_cita'] . "|" . $_GET['idHorario'] . "|" . $_GET['id_derecho'] . "|" . $_GET['fechaCita'] . "|" . $_SESSION['idUsuario'] . "|RP");
                        } else { // hay error en el mysql
                            echo "alert('No se pudo poner en 9 el status');";
                        }
                    } else { // hay error en el mysql
                        echo "alert('No se pudo agregar la cita'); ";
                    }
                } else { //SI ES CITA A REPROGRAMAR POR REZAGO
                    $horario = getHorarioXid($_GET['idHorario']);
                    if ($horario['tipo_cita'] == "0") { //SI ES CITA DE PRIMERA VEZ SE DA DE ALTA, SINO, NO SE PUEDE DAR DE ALTA
                        $query_query = "INSERT INTO citas VALUES (NULL,'" . $_GET['idHorario'] . "','" . $_GET['id_derecho'] . "','" . $_GET['fechaCita'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',1,'');";
                        $res = ejecutarSQL($query_query);
                        $log = ejecutarLOG("cita reprogramada por rezago | horario: " . $_GET['idHorario'] . " | dh: " . $_GET['id_derecho'] . " | fecha: " . $_GET['fechaCita'] . " | status: 1 | observaciones: " . $_GET['obs']);
                        if ($res[0] == 0) {// no hay error
                            // marcar en citas por reprogramar en status = 9
                            $query_query = "UPDATE citas_por_reprogramar_rezago SET status='9' WHERE id_cita='" . $_GET['id_cita'] . "'";
                            $res2 = ejecutarSQL($query_query);
                            $log = ejecutarLOG("update de citas por reprogramar rezago | cita : " . $_GET['id_cita']);
                            if ($res2[0] == 0) {// no hay error
                                // agregar en citas reprogramadas rezagada incluyendo los datos de la cita original y la cita nueva
                                $cita = getCitaRecienAgregada($_GET['idHorario'], $_GET['id_derecho'], $_GET['fechaCita']);
                                $citaAnterior = getcitasAreprogramarXid($_GET['id_cita']);
                                $query_query = "INSERT INTO citas_reprogramadas_rezago VALUES ('" . $cita['id_cita'] . "','" . $_GET['idHorario'] . "','" . $_GET['id_derecho'] . "','" . $_GET['fechaCita'] . "','" . $_GET['id_cita'] . "','" . $citaAnterior['id_horario'] . "','" . $citaAnterior['fecha_cita'] . "','" . $citaAnterior['status'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','');";
                                $res3 = ejecutarSQL($query_query);
                                if ($horario['tipo_cita'] == 0) { // si el tipo de cita es 1ra vez
                                    // si es 1ra vez agregar campo diagnostico N = normal E = extraordinaria
                                    $query_query = "INSERT INTO citas_detalles VALUES ('" . $cita['id_cita'] . "','N','" . $_GET['diagnostico'] . "','" . $_GET['concertada'] . "','');";
                                    $res = ejecutarSQL($query_query);
                                }
                                print($cita['id_cita'] . "|" . $_GET['idHorario'] . "|" . $_GET['id_derecho'] . "|" . $_GET['fechaCita'] . "|" . $_SESSION['idUsuario'] . "|RZ");
                            } else { // hay error en el mysql
                                echo "alert('No se pudo poner en 9 el status');";
                            }
                        } else { // hay error en el mysql
                            echo "alert('No se pudo agregar la cita'); ";
                        }
                    } else { // SI NO ES CITA DE PRIMERA VEZ MANDA ERROR
                        echo "alert('Debe seleccionar una cita de primera vez para poder reprogramar una cita rezagada');";
                    }
                }
            } else {
                echo "alert('No se encontro la id_cita');";
            }
        } else {
            echo "alert('Ya existe una cita este dia para el paciente seleccionado');";
        }
    } else { // SOLO SE VA A AGREGAR UNA CITA
//	$citaMismoDia = existeDuplica("SELECT * FROM citas WHERE fecha_cita='" . $_GET['fechaCita'] . "' AND id_derecho='" . $_GET['id_derecho'] . "'");

		if ((isset($_GET['estudio'])) && ($_GET['estudio'] != '')) { // la cita es para laboratorio
			$query_query = "INSERT INTO citas VALUES (NULL,'" . $_GET['idHorario'] . "','" . $_GET['id_derecho'] . "','" . $_GET['fechaCita'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',0,'');";
           $res = ejecutarSQL($query_query);
           $log = ejecutarLOG("cita lab nueva | horario: " . $_GET['idHorario'] . " | dh: " . $_GET['id_derecho'] . " | fecha: " . $_GET['fechaCita'] . " | status: 1 | observaciones: " . $_GET['obs']);
           if ($res[0] == 0) {// no hay error
               $cita = getCitaRecienAgregada($_GET['idHorario'], $_GET['id_derecho'], $_GET['fechaCita']);
               $contraReferencia = "no";
               if ($horario['tipo_cita'] == 0 ) { // si el tipo de cita es 1ra vez
                   // si es 1ra vez agregar campo diagnostico N = normal E = extraordinaria
                   $query_query = "INSERT INTO citas_detalles VALUES ('" . $cita['id_cita'] . "','N','" . $_GET['diagnostico'] . "','" . $_GET['concertada'] . "','');";
                   $res = ejecutarSQL($query_query);
               }

				if ($_GET['estudio'] != '') {
					$datosHorario = getHorarioXid($_GET['idHorario']);
					$query_queryLab = "INSERT INTO laboratorio_citas VALUES (NULL,'" . $_SESSION['IdCon'] . "','" . $_SESSION['idServ'] . "','" . $_SESSION['idDr'] . "','" . $_GET['id_derecho'] . "','" . $datosHorario['hora_inicio'] . "','" . $datosHorario['hora_fin'] . "','" . $datosHorario['tipo_cita'] . "','" . $_GET['fechaCita'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',1,'" . $cita['id_cita'] . "','" . $_GET['estudio'] . "','" . $_GET['servEstudio'] . "');";
				   $res = ejecutarSQL($query_queryLab);
				}
               print($cita['id_cita'] . "|" . $_GET['idHorario'] . "|" . $_GET['id_derecho'] . "|" . $_GET['fechaCita'] . "|" . $_SESSION['idUsuario'] . "||" . $contraReferencia . "|");
			}
		} else { // cita que no es para laboratorio
        $citaMismoDia = existeDuplica("SELECT cita.id_cita FROM citas cita, horarios horario WHERE cita.id_horario=horario.id_horario AND cita.fecha_cita='" . $_GET['fechaCita'] . "' AND cita.id_derecho='" . $_GET['id_derecho'] . "' AND horario.id_consultorio='" . $_SESSION['IdCon'] . "' AND horario.id_servicio='" . $_SESSION['idServ'] . "' AND horario.id_medico='" . $_SESSION['idDr'] . "'
            UNION select id_cita from citas_extemporaneas WHERE fecha_cita='" . $_GET['fechaCita'] . "' AND id_derecho='" . $_GET['id_derecho'] . "' AND id_consultorio='" . $_SESSION['IdCon'] . "' AND id_servicio='" . $_SESSION['idServ'] . "' AND id_medico='" . $_SESSION['idDr'] . "'");
        if (!$citaMismoDia) {
            $duplica = existeDuplica("SELECT * FROM citas WHERE id_horario='" . $_GET['idHorario'] . "' AND fecha_cita='" . $_GET['fechaCita'] . "'");
            if (!$duplica) {
                // checar si la cita es de primera vez y una fecha mayor a 3 semanas para mandarla a rezagadas
                $unix_time = strtotime(date('Ymd'));
                $fecha3semanas = date('Ymd', strtotime(date("Y/m/d", $unix_time) . " + 21 day"));
                $horario = getHorarioXid($_GET['idHorario']);
                $sql = "select * from referencia_contrarreferencia where id_derecho=" . $_GET['id_derecho'] . " and id_servicio=" . $_SESSION['idServ'];
                $referencia = ejecutarConsulta($sql);
                if (count($referencia) == 0 && $horario['tipo_cita'] == 1) {
                    $sql="select c.fecha_cita from citas as c join horarios as h on(c.id_horario=h.id_horario) where c.id_derecho=".$_GET['id_derecho']." And h.id_servicio=".$_SESSION['idServ']." And h.tipo_cita=0 and c.fecha_cita>=".date("Ymd", strtotime("-3 year"))."
                        UNION
                        select fecha_cita from citas_extemporaneas where id_derecho=".$_GET['id_derecho']." And id_servicio=".$_SESSION['idServ']." And tipo_cita=0 and fecha_cita>=".date("Ymd", strtotime("-1 year"))." ORDER BY fecha_cita DESC Limit 1";
                    $prv=ejecutarConsulta($sql);
                    if(count($prv)>0)
                    {
                        $sql="select c.id_cita from citas as c join horarios as h on(c.id_horario=h.id_horario) where c.id_derecho=".$_GET['id_derecho']." And h.id_servicio=".$_SESSION['idServ']." And h.tipo_cita=1 and c.fecha_cita>=".$prv['fecha_cita']."
                        UNION
                        select id_cita from citas_extemporaneas where id_derecho=".$_GET['id_derecho']." And id_servicio=".$_SESSION['idServ']." And tipo_cita=1 and fecha_cita>=".$prv['fecha_cita'];
                        $citasSub=  contarCitas($sql);
                        if(count($prv)>0 || $citasSub>0)
                        {
                            $sql="insert into referencia_contrarreferencia values(NULL,".$_GET['id_derecho'].",".$_SESSION['idServ'].",1,".$citasSub.",1);";
                            $ref=ejecutarSQL($sql);
                        }
                        else
                        {
                            echo "alert('No cuenta con cita de primera Vez, agende de primera vez');";
                            exit();
                        }
                    }
                    else
                    {
                        $sql="select c.id_cita from citas as c join horarios as h on(c.id_horario=h.id_horario) where c.id_derecho=".$_GET['id_derecho']." And h.id_servicio=".$_SESSION['idServ']." And h.tipo_cita=1 and c.fecha_cita>=".date("Ymd", strtotime("-3 year"))."
                        UNION
                        select id_cita from citas_extemporaneas where id_derecho=".$_GET['id_derecho']." And id_servicio=".$_SESSION['idServ']." And tipo_cita=1 and fecha_cita>=".date("Ymd", strtotime("-1 year"));
                        $prv= contarCitas($sql);
                        if($prv>0)
                        {
                            $sql="insert into referencia_contrarreferencia values(NULL,".$_GET['id_derecho'].",".$_SESSION['idServ'].",1,".$prv.",1);";
                            $ref=ejecutarSQL($sql);
                        }
                        else
                        {
                            echo "alert('No cuenta con cita de primera Vez, agende de primera vez');";
                            exit();
                        }
                    }
                } else if ($horario['tipo_cita'] == 1 && $referencia['cita_primera'] == 0) {
                    echo "alert('No cuenta con cita de primera Vez, agende de primera vez');";
                    exit();
                } else
//                if (($referencia['cita_primera'] == 1 && $horario['tipo_cita'] == 0) &&$referencia['citas_subsecuentes']>0) {
                if (($referencia['cita_primera'] == 1 && $horario['tipo_cita'] == 0)) {
                    echo "alert('Ya cuenta con cita de primera Vez, agende otro tipo de cita');";
                    exit();
                }

// --------------------------   DESCOMENTAR LINEAS PARA QUE VUELVA A FUNCIONAR EL RAZAGO DE CITAS

                /* 			if(($fecha3semanas <= $_GET['fechaCita']) && ($horario['tipo_cita'] == '0')) {// la fecha de la cita es mayor a 3 semanas de hoy y es de primera vez, se manda cita a rezagadas
                  $query_query = "INSERT INTO citas_por_reprogramar_rezago VALUES (NULL,'" .  $_GET['idHorario'] . "','" . $_GET['id_derecho'] . "','" . $_GET['fechaCita'] . "','0','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','');";
                  $res = ejecutarSQL($query_query);
                  if ($res[0] == 0) {// no hay error
                  echo "alert('Debido a que la cita es de Primera Vez y la fecha de la cita es mayor a 3 semanas de la fecha actual, esta cita se ha enviado a citas con rezago y se debera reprogramar'); cargarAgenda();";
                  }
                  } else { // la cita se agrega normalita
                 */ $query_query = "INSERT INTO citas VALUES (NULL,'" . $_GET['idHorario'] . "','" . $_GET['id_derecho'] . "','" . $_GET['fechaCita'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',0,'');";
// echo  $query_query;
                $res = ejecutarSQL($query_query);
                $log = ejecutarLOG("cita nueva | horario: " . $_GET['idHorario'] . " | dh: " . $_GET['id_derecho'] . " | fecha: " . $_GET['fechaCita'] . " | status: 1 | observaciones: " . $_GET['obs']);
                if ($res[0] == 0) {// no hay error
                    $cita = getCitaRecienAgregada($_GET['idHorario'], $_GET['id_derecho'], $_GET['fechaCita']);
                    $contraReferencia = "no";
                    if ($horario['tipo_cita'] == 0 ) { // si el tipo de cita es 1ra vez
                        // si es 1ra vez agregar campo diagnostico N = normal E = extraordinaria
                        $query_query = "INSERT INTO citas_detalles VALUES ('" . $cita['id_cita'] . "','N','" . $_GET['diagnostico'] . "','" . $_GET['concertada'] . "','');";
                        $res = ejecutarSQL($query_query);
                    }
                    //$horario=getHorarioXid($_GET['id_horario']);
                    if (($horario['tipo_cita'] == 0 && $referencia['cita_primera']==0)|| count($referencia)==0) {
                        $sql = "insert into referencia_contrarreferencia VALUES(NULL,'" . $_GET['id_derecho'] . "','" . $horario['id_servicio'] . "',1,0,1);";
                        $querypri = ejecutarSQL($sql);
                    } else
                    if ($horario['tipo_cita'] == 1) {
                        $sql = "select * from referencia_contrarreferencia where id_derecho=" . $_GET['id_derecho'] . " AND id_servicio=" . $horario['id_servicio']." order by id_referencia DESC LIMIT 1";
                        $query = ejecutarConsulta($sql);
                        if (count($query) > 0) {
                            $sql = "update referencia_contrarreferencia SET citas_subsecuentes=citas_subsecuentes+1 Where id_derecho=" . $_GET['id_derecho'] . " AND id_servicio=" . $horario['id_servicio'];
                            $query = ejecutarSQL($sql);
                        }
                    }
                    print($cita['id_cita'] . "|" . $_GET['idHorario'] . "|" . $_GET['id_derecho'] . "|" . $_GET['fechaCita'] . "|" . $_SESSION['idUsuario'] . "||" . $contraReferencia . "|");
                    //	print_r($res);
                }
				
//			}
            } else {
                echo "alert('El horario ya ha sido llenado con otra cita');";
            }
        } else {
            echo "alert('Ya existe una cita este dia para el paciente seleccionado');";
        }
		} // fin de cita que no es para laboratorio
    }
    ?>
    <?php

} else {
    $_SESSION['idUsuario'] = "-1";
    $_SESSION['tipoUsuario'] = "-1";
    $_SESSION['IdCon'] = "-1";
    $_SESSION['idServ'] = "-1";
    $_SESSION['idDr'] = "-1";
    echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Tu sesion ha caducado'); location.replace('index.php');</script>";
}
    ?>