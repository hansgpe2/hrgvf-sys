<?php
session_start();
set_time_limit(600);
include_once 'lib/funciones.php';
$solicitud=$_REQUEST['idCitaAp'];
$aceptada=$_REQUEST['estado'];
$cita=DatosCitaApartada($solicitud);
$idCita=$cita['id_cita'];
$medico=$cita['id_medico'];
if($aceptada==0)
$servicio=$_REQUEST['idServ'];
else
$servicio=$cita['id_servicio'];
$nomMedico=  getMedicoXid($medico);
$nomServicio=  nombreServicio($servicio);
$idDer=$cita['id_derecho'];
$derecho=  DatosDerechoHabiente($idDer);
?>
<div class="tituloVentana">Reprogramar Cita</div>
<div class="subtituloVentana"><table width="100%" border="0">
  <tr>
    <td colspan="2" class="tituloVentana">Datos del DerechoHabiente</td>
    
  </tr>
  <tr>
    <td width="51%"><span class="textosParaInputs">Cedula:&nbsp;</span><?php echo $derecho['cedula']."/".$derecho['cedula_tipo']; ?></td>
    <td width="49%"><span class="textosParaInputs">Nombre:&nbsp;</span><?php echo ponerAcentos($derecho['ap_p']." ".$derecho['ap_m']." ".$derecho['nombres']) ?></td>
  </tr>
  <tr>
    <td colspan="2" class="tituloVentana">Datos de la Cita</td>
    
  </tr>
  <tr>
    <td><span class="textosParaInputs">Hora y Fecha:&nbsp;</span><?php echo date("d/m/Y",strtotime($cita['fecha_cita']))." ".date("H:i",strtotime($cita['hora_cita'])); ?></td>
    <td class="textosParaInputs">Servicio: <?php echo ponerAcentos($nomMedico['ap_p']." ".$nomMedico['ap_m']." ".$nomMedico['nombres']); ?></td>
  </tr>
</table>
</div>
<?php
if(date("Ymd")<=$cita['fecha_cita'])
$fechaIni = date("Ymd",strtotime($cita['fecha_cita']));
else
    $fechaIni=date("Ymd");
$fechaFin = date("Ymd", strtotime("+60 day", strtotime($fechaIni)));
$fecha = date("Ymd", strtotime("+1 day", strtotime($fechaIni)));
$contMed = 0;
?>
<table border='1'><tr><th>fecha</th><th>Dia</th><th>hora</th><th>Servicio</th><th>Medico</th><th>Apartar Cita</th></tr>
<?php
while ($fecha <= $fechaFin) {
    $citas= CitasXMedicoDia($medico, $fecha);
    foreach ($citas as $key => $cita) {
        ?>
        <tr><td><?php echo date("d/m/Y",  strtotime($cita['fecha']));?></td>
                <td><?php echo $cita['dia']?></td>
                <td><?php echo date("H:i",strtotime($cita['horaI']))."-".date("H:i",strtotime($cita['horaF']))?></td>
                <td><?php echo ponerAcentos($nomServicio); ?></td>
                <td><?php echo ponerAcentos($cita['medico']); ?></td>
                <td><input type="button" value="elegir cita" onclick="elegirCitaRep('<?php echo $idDer ?>','<?php echo  $cita['id_horario'] ?>','<?php echo $cita['fecha'] ?>','<?php echo date("H:i", strtotime($cita['horaI'])) ?>','<?php echo $solicitud; ?>','<?php echo $aceptada; ?>');" /></td></tr>
        <?php
    $contMed++;        
    }
    $fecha=date("Ymd",  strtotime("$fecha +1 day"));
}
?>
</table>
<?php echo "Total de Citas Disponibles: ".$contMed; ?>
