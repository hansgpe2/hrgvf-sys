/*
SQLyog Ultimate v9.63 
MySQL - 5.0.67-community-nt : Database - sistema_agenducha
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistema_agenducha` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistema_agenducha`;

/*Table structure for table `usuarios_contrarreferencias` */

DROP TABLE IF EXISTS `usuarios_contrarreferencias`;

CREATE TABLE `usuarios_contrarreferencias` (
  `id_usuario` int(11) NOT NULL auto_increment,
  `login` varchar(36) default NULL,
  `pass` varchar(36) default NULL,
  `nombre` varchar(150) default NULL,
  `tipo_usuario` varchar(3) default NULL COMMENT '1-Contrarreferencia 2-Clinica 3-Visor Contrarreferencia 4-Visor Clinica 5-Admin Contrarreferencias 6-admin Clinica',
  `id_clinica` int(11) default NULL,
  `status` varchar(3) default NULL,
  `sesionId` varchar(120) default NULL,
  `extra1` varchar(60) default NULL,
  `st` varchar(3) default NULL,
  PRIMARY KEY  (`id_usuario`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `usuarios_contrarreferencias` */

insert  into `usuarios_contrarreferencias`(`id_usuario`,`login`,`pass`,`nombre`,`tipo_usuario`,`id_clinica`,`status`,`sesionId`,`extra1`,`st`) values (1,'CONTRAREF','123456','Usuario de contrarreferencia','1',0,'0','','','1'),(2,'clinica1','123456','usuario de clinica de prueba','2',1,'0','','','1'),(3,'adminclin1','123456','administrador Clinica 1','6',1,'0','','','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
