/*
SQLyog Ultimate v9.63 
MySQL - 5.0.67-community-nt : Database - sistema_agenducha
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistema_agenducha` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistema_agenducha`;

/*Table structure for table `servicios_x_clinica` */

DROP TABLE IF EXISTS `servicios_x_clinica`;

CREATE TABLE `servicios_x_clinica` (
  `id_clinica` int(11) default NULL,
  `id_servicio` int(11) default NULL,
  `id_usuario` int(45) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `servicios_x_clinica` */

insert  into `servicios_x_clinica`(`id_clinica`,`id_servicio`,`id_usuario`) values (1,63,3),(1,10,3),(1,1,3),(1,2,3),(1,3,3),(1,4,3),(19,9,4);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
