/*
SQLyog Ultimate v9.63 
MySQL - 5.0.67-community-nt : Database - sistema_agenducha
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistema_agenducha` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistema_agenducha`;

/*Table structure for table `archivos_referencia` */

DROP TABLE IF EXISTS `archivos_referencia`;

CREATE TABLE `archivos_referencia` (
  `id_archivo` int(11) NOT NULL auto_increment,
  `id_derecho` int(11) default NULL,
  `id_cita` int(11) default NULL,
  `archivo` varchar(30) default NULL,
  `ubicacion` varchar(100) default NULL,
  PRIMARY KEY  (`id_archivo`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Table structure for table `citas_apartadas` */

DROP TABLE IF EXISTS `citas_apartadas`;

CREATE TABLE `citas_apartadas` (
  `id_cita_ap` int(11) NOT NULL auto_increment,
  `id_cita` int(11) NOT NULL,
  `id_unidad` int(11) NOT NULL,
  `aceptada` tinyint(1) default NULL,
  `diagnostico` varchar(400) default NULL,
  `observaciones` varchar(400) default NULL,
  `motivo_rechazo` varchar(300) default NULL,
  `fecha_apartada` date default NULL,
  `fecha_revisada` date default NULL,
  `id_derecho` int(11) default NULL,
  `id_horario` int(11) default NULL,
  PRIMARY KEY  (`id_cita_ap`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
