<!DOCTYPE html>
<html>
    <head>
        <title>Comprobante de Cita</title>
        <link href="lib/impresion2.css">
        <link href="lib/impresion2.css" rel="stylesheet" type="text/css">
    </head>
    <body onLoad="window.print();">
        <p>
            <?php
            session_start();
            include 'lib/funciones.php';
            if (isset($_FILES)) {
                $referencia = $_FILES['referencia'];
                $tipoRef = $_FILES['referencia']['type'];
                $nomRef = $_FILES['referencia']['name'];
                $error_stringRef = $_FILES['referencia']['error'];
                $laboratorio = $_FILES['laboratorio'];
                $tipoLab = $_FILES['laboratorio']['type'];
                $nomLab = $_FILES['laboratorio']['name'];
                $error_stringLab = $_FILES['laboratorio']['error'];
                $placas = $_FILES['Placas'];
                $tipoPlacas = $_FILES['Placas']['type'];
                $nomPlacas = $_FILES['Placas']['name'];
                $error_stringPlacas = $_FILES['Placas']['error'];
                $ecos = $_FILES['Ecos'];
                $tipoEcos = $ecos['type'];
                $nombreEcos = $ecos['name'];
                $errorEcos = $ecos['error'];
                $fechaCita = $_REQUEST['fechaCita'];
                $servicio = $_REQUEST['servicio'];
                $medico = $_REQUEST['medico'];
                $horaIni = $_REQUEST['horaIni'];
                $raiz = "../documentos_cita/";
                $cita = $_REQUEST['idCita'];
                if (!file_exists($raiz)) {
                    mkdir($raiz);
                }
                $carpeta = $raiz . "/" . $cita . "/";
                $idDerecho = $_REQUEST['dh'];
                $arch = 0;

                mkdir($carpeta);
                if ((strpos($tipoRef, "pdf") || (strpos($tipoRef, "jpeg") || strpos($tipoRef, "jpg"))) && empty($error_stringRef)) {
                    $tmp = $referencia['tmp_name'];
                    $nomArch = explode(".", $nomRef);
                    $ext = $nomArch[1];
                    $nombreNuevo = "Referencia_cita." . $ext;
                    $ubicacion = $carpeta . $nombreNuevo;
                    if (move_uploaded_file($tmp, $ubicacion)) {
                        $sql = "insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
                        $ej = insertarSQL($sql);
                        if ($ej[0] == 0) {
                            $arch+=1;
                        }
                    }
                }
                if ((strpos($tipoLab, "pdf") || (strpos($tipoLab, "jpeg") || strpos($tipoLab, "jpg"))) && empty($error_stringLab)) {
                    $tmp = $laboratorio['tmp_name'];
                    $nomArch = explode(".", $nomLab);
                    $ext = $nomArch[1];
                    $nombreNuevo = "estudios_laboratorio." . $ext;
                    $ubicacion = $carpeta . $nombreNuevo;
                    if (move_uploaded_file($tmp, $ubicacion)) {
                        $sql = "insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
                        $ej = insertarSQL($sql);
                        if ($ej[0] == 0) {
                            $arch+=1;
                        }
                    }
                }

                if ((strpos($tipoEcos, "pdf") || (strpos($tipoEcos, "jpeg") || strpos($tipoEcos, "jpg"))) && empty($errorEcos)) {
                    $tmp = $ecos['tmp_name'];
                    $nomArch = explode(".", $nombreEcos);
                    $ext = $nomArch[1];
                    $nombreNuevo = "Ecos" . "." . $ext;
                    $ubicacion = $carpeta . $nombreNuevo;
                    if (move_uploaded_file($tmp, $ubicacion)) {
                        $sql = "insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
                        $ej = insertarSQL($sql);
                        if ($ej[0] == 0) {
                            $arch+=1;
                        }
                    }
                }

                if ((strpos($tipoPlacas, "pdf") || (strpos($tipoPlacas, "jpeg") || strpos($tipoPlacas, "jpg"))) && empty($error_stringPlacas)) {
                    $tmp = $placas['tmp_name'];
                    $nomArch = explode(".", $nomPlacas);
                    $ext = $nomArch[1];
                    $nombreNuevo = "Placas_tx." . $ext;
                    $ubicacion = $carpeta . $nombreNuevo;
                    if (move_uploaded_file($tmp, $ubicacion)) {
                        $sql = "insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
                        $ej = insertarSQL($sql);
                        if ($ej[0] == 0) {
                            $arch+=1;
                        }
                    }
                }
                if ($arch > 0) {
                    $idUnidad = $_SESSION['idClinica'];
                    $diagnostico = $_REQUEST['diagnostico'];
                    if ($diagnostico == "otro")
                        $diagnostico = $_REQUEST['diagnostico1'];
                    $usuario = $_SESSION['idUsuario'];
                    $sql = "insert into citas_apartadas values(NULL,$cita,$idUnidad,-1,'".$diagnostico."','".$_REQUEST['observaciones']."','','".date("Y-m-d H:i:s") ."',NULL," . $_REQUEST['idDh'] . ",$usuario,$cita,$servicio,$medico,'$horaIni','$fechaCita');";
                    $ej = insertarSQL($sql);
                    if ($ej[0] == 0) {
                        $sql = "select * from citas as c join horarios as h on(h.id_horario=c.id_horario) where c.fecha_cita='$fechaCita' and h.hora_inicio='$horaIni'";
                        $sql = "select id_cita_ap from citas_apartadas where id_cita=" . $cita;
                        $citaAp = consultaSQL($sql);
                        $existeCita = existeDuplica($sql);
                        if (isset($_COOKIE['cita']))
                            setcookie($_COOKIE['cita'], "", time() - 3600);
                        if ($existeCita)
                            header("location:formatoCita.php?idCita=" . $cita . "&idCitaApartada=" . $citaAp['id_cita_ap']);
                    }
                }
                else {
                    header("location:cancelarCita.php?idCita=" . $cita);
                }
            }
            ?>
            <input type="hidden" id="cita">
            <br />
        </p>
    </body>
</html>