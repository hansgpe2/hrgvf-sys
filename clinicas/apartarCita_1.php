<?php
session_start();
include 'lib/funciones.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body onload="window.print();">
<?php
if(isset($_FILES))
{
	$referencia=$_FILES['referencia'];
	$laboratorio=$_FILES['laboratorio'];
	$placas=$_FILES['Placas'];
	$ecos=$_FILES['Ecos'];
	$cita=$_REQUEST['idCita'];
	$idDerecho=$_REQUEST['id_derecho'];
    $raiz="../documentos_cita/";
    if(!file_exists($raiz))
    {
        mkdir($raiz);
    }
    $carpeta=$raiz."/".$cita."/";    
    $idDerecho=$_REQUEST['dh'];
    $arch=0;
	$archivo=strtok($referencia['name'],".");
	$ext=strtok($referencia['name']);
	 mkdir($carpeta);
	if(strtolower($ext)=="pdf" || strtolower($ext)=="jpg" || strtolower($ext)=="jpeg")
	{
		$tmp=$referencia['tmp_name'];
        $nomArch= explode(".", $nomRef);
        $ext=$nomArch[1];
        $nombreNuevo="Referencia_cita.".$ext;
        $ubicacion=$carpeta.$nombreNuevo;
        if(move_uploaded_file($tmp,$ubicacion))
        {
            $sql="insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
            $ej=  insertarSQL($sql);
            if($ej[0]==0)
            {
                $arch+=1;
            }
        }
	}
	$archivo=strtok($ecos['name'],'.');
	$ext=strtok($ecos['name']);
	if(strtolower($ext)=="pdf" || strtolower($ext)=="jpg"|| strtolower($ext)=="jpeg")
	{
		$tmp=$ecos['tmp_name'];
		$nomArch=$archivo;
		$nombreNuevo=$nomArch.".".$ext;
		$ubicacion=$carpeta.$nombreNuevo;
		if(move_uploaded_file($tmp,$ubicacion))
		{
			$sql="insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
			$ej=  insertarSQL($sql);
            if($ej[0]==0)
            {
                $arch+=1;
            }
		}
	}
	
	
	?>
<table width="57%" height="71" border="0" class="tablaDiag">
  <tr>
    <td><img src="diseno/logoEncabezado.jpg" alt="" width="112" height="49" /></td>
    <td class="encabezado" align="center">Cita Apartada</td>
  </tr>
  <tr>
    <td width="39%" class="encabezado">Cedula</td>
    <td width="61%" class="datos"><?php echo $datosDerecho['cedula']."/".$datosDerecho['cedula_tipo']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">Nombre</td>
    <td><?php echo ponerAcentos($datosDerecho['ap_p']." ".$datosDerecho['ap_m']." ".$datosDerecho['nombres']); ?></td>
  </tr>
  <tr>
    <td class="encabezado">Telefono</td>
    <td><?php echo $datosDerecho['telefono']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">Correo Electronico</td>
    <td><?php echo $datosDerecho['email']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">Celular</td>
    <td><?php echo $datosDerecho['celular']; ?></td>
  </tr>
  <tr>
    <td class="encabezado" colspan="2" align="center">Datos de la Cita</td>
  </tr>
  <tr>
    <td class="encabezado">Fecha</td>
    <td><?php echo date("d/m/Y",strtotime($datosCita['fecha_cita']))." ".date("H:i",strtotime($horario['hora_inicio']))."-".date("H:i",strtotime($horario['hora_fin'])); ?></td>
  </tr>
  <tr>
    <td class="encabezado">Servicio</td>
    <td><?php echo $servicio['nombre']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">M&eacute;dico</td>
    <td><?php echo $medico['titulo']." ".$medico['ap_p']." ".$medico['ap_m']." ".$medico['nombres']; ?></td>
  </tr>
  <tr>
    <td class="nota" colspan="2">Cualquier cambio en su cita se le avisara en los medios de contactos proporcionados</td>
  </tr>
</table>
<?php
}
else
 echo "<script>alert('No hay ningun archivo Selecionado');location.href='cancelarCita.php?idCita=".$cita."';window.close();</script>";
?>
</body>
</html>