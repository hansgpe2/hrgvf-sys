<?php
session_start();
include './lib/funciones.php';
$fechas = explode("|", $_REQUEST['meses']);
$fecha1 = $fechas[0];
$fecha2 = $fechas[1];
$clinica = $_SESSION['idClinica'];
$fechaIni = date("Ymd", strtotime($fecha1));
$fechaFin = date("Ymd", strtotime($fecha2));
$medCrit=  ObtenerTratamientosCriticos($_SESSION['idClinica'],$fechaIni,$fechaFin);
$clin = obtenerDatosClinica($clinica);
?>
<link href="lib/impresion2.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0">
    <tr>
        <td width="16%" rowspan="2"><img src="diseno/LOGO-ISSSTE.jpg" width="104" height="108" /></td>
        <td height="53" class="encabezado"><?php echo $clin['nombre']; ?></td>
    </tr>
    <tr>
        <td class="encabezado">Reporte de medicamentos del mes de <?php
            $mes = date("m", strtotime($fecha1));
            switch ($mes) {
                case "01":
                    echo "Enero";
                    break;
                case "02":
                    echo "Febrero";
                    break;
                case "03":
                    echo "Marzo";
                    break;
                case "04":
                    echo "Abril";
                    break;
                case "05":
                    echo "Mayo";
                    break;
                case "06":
                    echo "Junio";
                    break;
                case "07":
                    echo "Julio";
                    break;
                case "08":
                    echo "Agosto";
                    break;
                case "09":
                    echo "Septiembre";
                    break;
                case "10":
                    echo "Octubre";
                    break;
                case "11":
                    echo "Noviembre";
                    break;
                case "12":
                    echo "Diciembre";
                    break;
            }
            ?> <?php echo date("Y", strtotime($fecha1)) ?></td>
    </tr>
</table>


<p>&nbsp;</p>
<table width="800" border="1" class="tablaDiag">
    <tr class="titulos">
        <td width="15%" align="center">Clave del medicamento</td>
        <td width="71%" align="center">Descripción</td>
        <td width="14%" align="center">Cajas Requeridas</td>
    </tr>
    <?php
    $cont=  count($medCrit);
for($i=0;$i<$cont;$i++) {
    $medicina=  getMedicamentoXId($medCrit[$i]);
    $pacientes=  ObtenerPacientesXMed($medCrit[$i],$_SESSION['idClinica'],$fechaIni,$fechaFin);
    $totalMed=0;
    /*$printPacientes="<table class='Ventana'><tr>
        <th>Cedula</th><th>Nombre</th><th>Tratamiento Critico</th><th>Cajas</th>";*/
    foreach ($pacientes as $key1=>$paciente)
    {
        $totalMed+=$paciente['cajas'];
        /*$printPacientes.="<tr>
            <td>".$paciente['cedula']."</td>
                <td>".$paciente['nombreDH']."</td>
                <td>".$paciente['cronico']."</td>
                <td>".$paciente['cajas']."</td></tr>";*/
    }
    //$printPacientes.="</table>";
    echo "<tr><td align='center' class='claveMed'>".$medicina['id_medicamento']."</td>
        <td class='datosDiag'>".substr($medicina['descripcion'], 0,60)."</td>
            <td align='center'>".$totalMed."</td>
                </tr>";
           
}
    ?>
</table>
<p class="nota">Si desea ver el detallado de los pacientes puede consultarla en el apartado de Listas de Medicamentos Referidos del Hospital</p>
<p>&nbsp;</p>
<p class="firmaDirector">Firma del Director de la Clinica </p>
<p class="firmaDirector"><?php echo $clin['nombre']; ?></p>
