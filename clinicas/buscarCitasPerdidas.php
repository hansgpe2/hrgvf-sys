<?php
session_start();
include './lib/funciones.php';
$idCita=$_REQUEST['idSol'];
$datosCita=  DatosCitaApartada($idCita);
$estatusCita=$datosCita['aceptada'];
$usuario=$_SESSION['idUsuario'];
$estado=$_REQUEST['estado'];
if ($usuario > 0) {
    /* @var $fechaCitaR type */
    $fechaCitaR = $_REQUEST['fecha'];
   // echo "Nodo 1";
    $derecho = $_REQUEST['id_derecho'];
    $idhorario = $_REQUEST['horario'];
    $horario = datosHorarioXcita($_REQUEST['horario']);
//Obtenemos la fecha actual:
    $fecha = time();
//Queremos sumar 2 horas a la fecha actual:
    $horas = 0;
// Convertimos las horas a segundos y las sumamos:
    $fecha += ($horas * 60 * 60);
// Le damos al resultado el formato deseado:
    $fechaYhora = date('d/m/Y - H:i', $fecha);
    //Checa que no haya cita en el mismo servicio
    $citaMismoDia = existeDuplica("SELECT * FROM citas as c join horarios as h on(h.id_horario=c.id_horario) WHERE c.fecha_cita='" . $fechaCitaR . "' AND c.id_derecho='" . $derecho . "' AND h.id_servicio='" . $horario['id_servicio'] . "'");
    if (!$citaMismoDia) {
        //checa que no este ocupado el horario del serrvicio
        $duplica = existeDuplica("SELECT * FROM citas WHERE id_horario='" . $idhorario . "' AND fecha_cita='" . $fechaCitaR . "'");
        // agregar en citas
        if (!$duplica) {
//	$citaMismoDia = existeDuplica("SELECT * FROM citas WHERE fecha_cita='" . $fechaCitaR . "' AND id_derecho='" . $derecho . "'");
            $citaMismoDia = existeDuplica("SELECT cita.fecha_cita, cita.id_derecho, cita.id_horario, horario.id_horario, horario.id_consultorio, horario.id_servicio, horario.id_medico FROM citas cita, horarios horario WHERE cita.id_horario=horario.id_horario AND cita.fecha_cita='" . $fechaCitaR . "' AND cita.id_derecho='" . $derecho . "' AND horario.id_consultorio='" . $horario['id_consultorio'] . "' AND horario.id_servicio='" . $horario['id_servicio'] . "' AND horario.id_medico='" . $horario['id_medico'] . "'");
            if (!$citaMismoDia) {
                $duplica = existeDuplica("SELECT * FROM citas WHERE id_horario='" . $idhorario . "' AND fecha_cita='" . $fechaCitaR . "'");
                if (!$duplica) {
                    $horario = datosHorarioXcita($idhorario);
					$diagnostico=strtok($datosCita['diagnostico']);
                    $query_query = "INSERT INTO citas VALUES (NULL,'" . $idhorario . "','" . $derecho . "','" . $fechaCitaR . "','1','','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',3,'".$diagnostico."');";
                    $res = insertarSQL($query_query);
                    /* $log = */ ejecutarLOG("cita nueva | horario: " . $idhorario . " | dh: " . $derecho . " | fecha: " . $fechaCitaR . " | status: 1 | observaciones: cita agendada desde clinica");
                    if ($res[0] == 0) {// no hay error
                        $contraReferencia = "no";
                        $cita = getCitaRecienAgregada($idhorario, $derecho, $fechaCitaR);
                        if($estatusCita=="1")
                        $sql="update citas_apartadas SET id_cita=".$cita['id_cita'].",observaciones='Reprogramada por Clinica',fecha_apartada='".date("Y-m-d H:i:s")."',fecha_revisada='".date("Ymd")."',id_usuario=".$usuario.",hora_cita='".$horario['hora_inicio']."',fecha_cita='".$cita['fecha_cita']."' where id_cita_ap=".$datosCita['id_cita_ap'];
                        else
                          $sql="update citas_apartadas SET id_cita=".$cita['id_cita'].",aceptada=-1,observaciones='Reprogramada por Clinica',fecha_apartada='".date("Y-m-d H:i:s")."',fecha_revisada='',id_usuario=".$usuario.",hora_cita='".$horario['hora_inicio']."',fecha_cita='".$cita['fecha_cita']."',id_medico=".$horario['id_medico']." where id_cita_ap=".$idCita;  
                        $ejCita=  insertarSQL($sql);
                        if($ejCita[0]==0 && $estatusCita=="1")
                        {
                            $contraReferencia=  ObtenerRefContra($datosCita['id_servicio'], $datosCita['id_derecho']);
                            $sql="update referencia_contrarreferencia set citas_subsecuentes=0 where id_referencia=".$contraReferencia['id_referencia'];
                            $ejContra=  insertarSQL($sql);
                            echo '1|Cita Agregada|'.$cita['id_cita']."|".$datosCita['id_cita_ap'];
                        }
                        else
                            if($ejCita[0]==0)
                                echo '1|Cita Agregada|'.$cita['id_cita']."|".$datosCita['id_cita_ap'];
                    }
                    else{
                        echo "-1|'Cita no agregada'";
                        //echo "nodo 7";
                    }
                }
                else {
                    echo "-1|'El horario ya ha sido llenado con otra cita'";
                    //echo "nodo 8";
                }
            } else {
                echo "-1|'Ya existe una cita este horario para el paciente seleccionado'";
                //echo "nodo 9";
            }
        } else {
            echo "-1|'Ya existe una cita este dia para el paciente seleccionado'";
            //echo "nodo 10";
        }
    } else {
        echo "-1|'Ya tiene cita el paciente en este dia'";
        //echo "nodo 11";
    }
}
else{
echo "-1|'Tu sesion ha caducado')";
        //echo "nodo 12";
}   

?>