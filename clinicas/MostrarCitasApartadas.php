<?php

session_start();
include 'lib/funciones.php';
$data['idClinica'] = $_SESSION['idClinica'];
$tpoFiltro = $_REQUEST['filtro'];
$fech = date("Ymd", strtotime("-2 years"));
$data['filtro'] = $tpoFiltro;
switch ($tpoFiltro) {
    case 1:
        $data['fecha1'] = $_REQUEST['fecha1'];
        $data['fecha2'] = $_REQUEST['fecha2'];
        break;
    case 2:
        $data['estatus'] = "1";
        break;
    case 4:
        $data['estatus'] = "0";
        break;
    case 5:
        $data['estatus'] = "-1";
        break;
    case 6:
        $data['estatus'] = "2";
        break;
    case 7:
        $data['folio'] = $_REQUEST['folio'];
        break;
}
$citas = ObtenerCitas($data);
$output['aaData'] = array();
foreach ($citas as $key => $cita) {
    $row = array();
    switch ($cita['aceptada']) {
        case -1:
            $row[] = "<a href='formatoCita.php?idCita=" . $cita['id_cita'] . "&idCitaApartada=" . $cita['id_cita_ap'] . "' target='_blank' class='ui-button'>" . ponerCeros($cita['id_cita_ap'], 8) . "</a>";
            $row[] = $cita['cedula'] . "/" . $cita['cedula_tipo'];
            $row[] = ponerAcentos($cita['dh_app'] . " " . $cita['dh_apm'] . " " . $cita['dh_nom']);
            $row[] = $cita['servicio'];
            $row[] = "sin respuesta";
            $row[] = "";
            $row[] = date("d/m/Y", strtotime($cita['fecha_apartada']));
            $row[] = "";
            $row[] = date("d/m/Y", strtotime($cita['fecha_cita'])) . "-" . date("H:i", strtotime($cita['hora_cita']));
            $row[] = ponerAcentos($cita['medico'] . " " . $cita['doc_app'] . " " . $cita['doc_apm']);
            $row[] = $cita['usuario'];
            break;
        case 0:
            $row[] = ponerCeros($cita['id_cita_ap'], 8);
            $row[] = $cita['cedula'] . "/" . $cita['cedula_tipo'];
            $row[] = ponerAcentos($cita['dh_app'] . " " . $cita['dh_apm'] . " " . $cita['dh_nom']);
            $row[] = $cita['servicio'];
            $row[] = "Rechazada";
            $row[] = $cita['motivo_rechazo'];
            $row[] = date("d/m/Y", strtotime($cita['fecha_apartada']));
            $row[] = date("d/m/Y", strtotime($cita['fecha_revisada']));
            $row[] = date("d/m/Y", strtotime($cita['fecha_cita'])) . "-" . $cita['hora_cita'];
            $row[] = "<input type='button' value='Reprogramar cita' onclick='ReprogramarCitaRechazada(" . $cita['id_cita_ap'] . ");'>";
            $row[] = $cita['usuario'];
            break;
        case 1:
            $fechaCita = strtotime($cita['fecha_cita']);
            $hoy = strtotime(date("Ymd"));
            if ($fechaCita < $hoy) {
                $fechaCita = date("Ymd", $fechaCita);
                $contServicios = BuscarMovimientosPacientes($fechaCita, $cita['id_derecho'], $cita['id_servicio']);
                if ($contServicios == 0) {
                    $row[] = "<a href='formatoCita.php?idCita=" . $cita['id_cita'] . "&idCitaApartada=" . $cita['id_cita_ap'] . "' target='_blank'>" . ponerCeros($cita['id_cita_ap'], 8);
                    $row[] = $cita['cedula'] . "/" . $cita['cedula_tipo'];
                    $row[] = ponerAcentos($cita['dh_app'] . " " . $cita['dh_apm'] . " " . $cita['dh_nom']);
                    $row[] = $cita['servicio'];
                    $row[] = "Aceptada";
                    $row[] = "<input type='button' value='Reprogramar Cita' onclick='ReprogramarAceptada(" . $cita['id_cita_ap'] . ",1);' >";
                    $row[] = date("d/m/Y", strtotime($cita['fecha_apartada']));
                    $row[] = date("d/m/Y", strtotime($cita['fecha_revisada']));
                    $row[] = date("d/m/Y", strtotime($cita['fecha_cita'])) . "-" . $cita['hora_cita'];
                    $row[] = ponerAcentos($cita['medico'] . " " . $cita['doc_app'] . " " . $cita['doc_apm']);
                    $row[] = $cita['usuario'];
                } else {
                    $row[] = "<a href='formatoCita.php?idCita=" . $cita['id_cita'] . "&idCitaApartada=" . $cita['id_cita_ap'] . "' target='_blank'>" . ponerCeros($cita['id_cita_ap'], 8);
                    $row[] = $cita['cedula'] . "/" . $cita['cedula_tipo'];
                    $row[] = ponerAcentos($cita['dh_app'] . " " . $cita['dh_apm'] . " " . $cita['dh_nom']);
                    $row[] = $cita['servicio'];
                    $row[] = "Aceptada";
                    $row[] = "";
                    $row[] = date("d/m/Y", strtotime($cita['fecha_apartada']));
                    $row[] = date("d/m/Y", strtotime($cita['fecha_revisada']));
                    $row[] = date("d/m/Y", strtotime($cita['fecha_cita'])) . "-" . $cita['hora_cita'];
                    $row[] = ponerAcentos($cita['medico'] . " " . $cita['doc_app'] . " " . $cita['doc_apm']);
                    $row[] = $cita['usuario'];
                }
            } else {
                $row[] = "<a href='formatoCita.php?idCita=" . $cita['id_cita'] . "&idCitaApartada=" . $cita['id_cita_ap'] . "' target='_blank'>" . ponerCeros($cita['id_cita_ap'], 8) . "</a>";
                $row[] = $cita['cedula'] . "/" . $cita['cedula_tipo'];
                $row[] = ponerAcentos($cita['dh_app'] . " " . $cita['dh_apm'] . " " . $cita['dh_nom']);
                $row[] = $cita['servicio'];
                $row[] = "Aceptada";
                $row[] = "";
                $row[] = date("d/m/Y", strtotime($cita['fecha_apartada']));
                $row[] = date("d/m/Y", strtotime($cita['fecha_revisada']));
                $row[] = date("d/m/Y", strtotime($cita['fecha_cita'])) . "-" . $cita['hora_cita'];
                $row[] = ponerAcentos($cita['medico'] . " " . $cita['doc_app'] . " " . $cita['doc_apm']);
                $row[] = $cita['usuario'];
            }
            break;
        case 2:
            $row[] = "<a href='formatoCita.php?idCita=" . $cita['id_cita'] . "&idCitaApartada=" . $cita['id_cita_ap'] . "' target='_blank'>" . ponerCeros($cita['id_cita_ap'], 8) . "</a>";
            $row[] = $cita['cedula'] . "/" . $cita['cedula_tipo'];
            $row[] = ponerAcentos($cita['dh_app'] . " " . $cita['dh_apm'] . " " . $cita['dh_nom']);
            $row[] = $cita['servicio'];
            $row[] = "Reprogramada";
            $row[] = "";
            $row[] = date("d/m/Y", strtotime($cita['fecha_apartada']));
            $row[] = date("d/m/Y", strtotime($cita['fecha_revisada']));
            $row[] = date("d/m/Y", strtotime($cita['fecha_cita'])) . "-" . date("H:i", strtotime($cita['hora_cita']));
            $row[] = ponerAcentos($cita['medico'] . " " . $cita['doc_app'] . " " . $cita['doc_apm']);
            $row[] = $cita['usuario'];
            break;
    }
    if (!json_encode($row, JSON_HEX_TAG | JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE)) {
        $rowClean = array_map('utf8_clean', $row);
        if (serialize($row) != serialize($rowClean)) {
            $row = $rowClean;
            $error_msg = 'Your data is not valid UTF-8 and has been stripped.';
        }
    }
    $output['aaData'][] = $row;
}
$jout = json_encode($output, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG | JSON_HEX_QUOT);
if (!$jout)
    echo $output['aaData'][] = json_last_error_msg();
else
    echo $jout;
?>