<?php 
session_start();
include 'lib/funciones.php';
$idClinica=$_SESSION['idClinica'];
$idDerecho=$_REQUEST['idDer'];
$datosDer=  DatosDerechoHabiente($idDerecho);
$datosClinica=  obtenerDatosClinica($idClinica);
$datosClinicaDh=  obtenerDatosClinica($datosDer['unidad_medica']);    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="lib/misEstilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
<header class="tituloVentana">
Cambiar de Unidad Medica
</header><form action="cambioClinicaConfirmar.php" method="get">
<table width="100%" border="0">
  <tr>
    <td width="49%" align="right" class="textosParaInputs">Unidad Médica del DerechoHabiente</td>
    <td width="51%"><?php echo $datosClinicaDh['nombre'];?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">Clinica del DerechoHabiente</td>
    <td><?php 
    $sql="select * from vgf where clinica=".$datosClinicaDh['clinica']." and unidad=0";
    $clinica=  consultaSQL($sql);
    echo $clinica['nombre'];
    ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">Clinica de esta Unidad</td>
    <td><?php 
    $sql="select * from vgf where clinica=".$datosClinica['clinica']." and unidad=0";
    $clinica=  consultaSQL($sql);
    echo $clinica['nombre'];
    ?></td>
  </tr>
  <tr><?php
    if($datosClinica['unidad']==0){
  ?>
      <td class="textosParaInputs" align="right">Nueva Unidad de Adscripci&oacute;n</td>
    <td><?php $sql="select * from vgf where clinica=".$datosClinica['clinica']." and Estado='".$datosClinica['Estado']."'";
    $clinicas= ObtenerDHS($sql);
    echo '<select id="nuevaClinica" name="nuevaClinica">';
    foreach ($clinicas as $key => $clinica) {
        echo '<option value="'.$clinica['id_unidad'].'">'.ponerAcentos($clinica['nombre']).'</option>';
    }
    echo "</select>";
    ?></td>
    <?php
    }
	else
	{
	?>
    <tr><td class="textosParaInputs" align="right">Nueva Unidad de Adscripci&oacute;n</td><td><?php echo $datosClinica['nombre'] ?>
        <input name="nuevaClinica" type="hidden" id="nuevaClinica" value="<?php echo $datosClinica['id_unidad'] ?>" /></td></tr>
    <?php 
	}
	?>
  </tr>
  <tr>
    <td colspan="2" align="right" class="textosParaInputs">      <input name="dh" type="hidden" id="dh" value="<?php echo $datosDer['id_derecho']; ?>" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="textosParaInputs"><input type="submit" name="cambiar" id="cambiar" value="Enviar" /></td>
  </tr>
</table></form>
</body>
</html>