/*
SQLyog Ultimate v9.63 
MySQL - 5.0.67-community-nt 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `usuarios_contrarreferencias` (
	`id_usuario` int (11),
	`login` varchar (36),
	`pass` varchar (36),
	`nombre` varchar (150),
	`tipo_usuario` varchar (3),
	`id_clinica` int (11),
	`status` varchar (3),
	`sesionId` varchar (120),
	`extra1` varchar (60),
	`st` varchar (3)
); 
insert into `usuarios_contrarreferencias` (`id_usuario`, `login`, `pass`, `nombre`, `tipo_usuario`, `id_clinica`, `status`, `sesionId`, `extra1`, `st`) values('1','CONTRAREF','123456','Usuario de contrarreferencia','1','0','0','','','1');
insert into `usuarios_contrarreferencias` (`id_usuario`, `login`, `pass`, `nombre`, `tipo_usuario`, `id_clinica`, `status`, `sesionId`, `extra1`, `st`) values('2','clinica1','123456','usuario de clinica de prueba','2','1','0','','','1');
