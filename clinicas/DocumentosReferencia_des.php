<?php
session_start();
include_once 'lib/funciones.php';
$derechoHabiente = $_REQUEST['idDerecho'];
$cita = $_REQUEST['idCita'];
$fecha = $_REQUEST['fechaCita'];
$der = DatosDerechoHabiente($derechoHabiente);
$idhorario = $_REQUEST['idHorario'];
$datosCita = datosCita($cita);
$horario = datosHorarioXcita($idhorario);
$servicio = datosServicio($horario['id_servicio']);
$medico = getMedicoXid($horario['id_medico']);
//$listDiagnosticos=  ListaDiags();
?>
<!Doctype html>
<html>
    <head>

        <link href="lib/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" />
        <link href="lib/jquery-file-upload/css/jquery.fileupload-ui-noscript.css" rel="stylesheet" />
        <style type="text/css">
            .obligatorios {
                color: #D40000;
            }
            .bar {
                height: 18px;
                background: green;
            }
        </style>
    </head>
    <body>
        <table width="800" class="Ventana">
            <tr>
                <td class="tituloVentana">Cita del derechoHabiente <?php echo ponerAcentos($der['ap_p'] . " " . $der['ap_m'] . " " . $der['nombres']); ?> en el servicio <?php echo $servicio['nombre']; ?></td>
            </tr>
            <tr>
                <td>Fecha de la Cita apartada <?php echo date("d/m/Y", strtotime($datosCita['fecha_cita'])); ?> hora de la cita <?php echo date("H:i", strtotime($horario['hora_inicio'])) . " a " . date("H:i", strtotime($horario['hora_fin'])); ?></td>
            </tr>
            <tr>
                <td><form action="apartarCita.php" method="post" enctype="multipart/form-data" id="FormCita" name="FormCita" target="_blank"><input type="hidden" id="dh"  name="dh" value="<?php echo $derechoHabiente; ?>"  /><input type="hidden" id="idCita" name="idCita" value="<?php echo $cita; ?>"  />
                        <input name="clinica" type="hidden" id="clinica" value="<?php echo $_SESSION['idClinica']; ?>">
                        <input type="hidden" name="fechaCita" id="fechaCita" value="<?php echo $fecha; ?>" >
                        <input type="hidden" name="servicio" id="servicio" value="<?php echo $servicio['id_servicio']; ?>" >
                        <input type="hidden" name="medico" id="medico" value="<?php echo $medico['id_medico'] ?>" >
                        <input type="hidden" name="horaIni" id="horaIni" value="<?php echo $horario['hora_inicio'] ?>" >
                        <input type="hidden" name="idDh" id="idDH" value="<?php echo $derechoHabiente; ?>" >
                        <input type="hidden" name="idRef" id="idRef" value="" />
                        <input type="hidden" name="idPlacas" id="idPlacas" value="" />
                        <input type="hidden" name="idLab" id="idLab" value="" />
                        <input type="hidden" name="idEco" id="idEco" value="" />
                        <input name="citaRealizada" type="hidden" value="0" id="citaRealizada" />
                        <table>
                            <tr>
                                <td><label for="diagnostico">Diagnostico</label><span class="obligatorios"> *</span></td><td><div class="ui_widget" style="display:block"><input name="diagnostico" type="text" id="diagnostico" onKeyUp="listaDiagnosticos('<?php echo $der['sexo']; ?>', '<?php echo $der['fecha_nacimiento']; ?>', 'resultados', this.value);">
                                        <a href="<?php echo site_url('docs/CIE10_SIMEF.pdf'); ?>" target="_blank" class="botones">Catalogo CIE10</a><div id="resultados" style="display:compact"></div>
                                        <div id="oDiag" style="display:none">
                                            <textarea name="diagnostico1" id="diagnostico1" cols="45" rows="5"></textarea>
                                        </div>
                                    </div></td></tr>
                            <tr>
                                <td>Observaciones</td><td><label for="observaciones"></label>
                                    <textarea name="observaciones" id="observaciones" cols="45" rows="5"></textarea></td></tr>
                            <tr><td colspan="2" align="center"><p>
                                        <input type="button" name="regresar" id="regresar" value="regresar" onClick="cancelarCita();" />  
                                        &nbsp;      
                                        <input name="apartarCita" type="button" value="Apartar Cita" onClick="apartarCitaConfirmar()" />
                                        <input name="citaRealizada" type="hidden" value="0" id="citaRealizada" />
                                    </p>
                                    <p class="obligatorios">* campo obligatorio</p></td></tr>
                        </table></form>
                    <table width="100%" border="1">
                        <tr>
                            <td width="56%">Referencia Firmada<span class="obligatorios"> *</span></td>
                            <td width="44%"><form id="archRef">
                                    <div class="row fileupload-buttonbar">
                                        <div class="span7">
                                            <!-- The fileinput-button span is used to style the file input field as button -->
                                            <span class="btn btn-success fileinput-button">
                                                <i class="icon-plus icon-white"></i>
                                                <span>Añadir Referencia</span>
                                                <input type="file" name="referencia" id="referencia" title="Requiere referencia firmada" />
                                            </span>
                                            <button type="submit" class="btn btn-primary start">
                                                <i class="icon-upload icon-white"></i>
                                                <span>Iniciar Carga</span>
                                            </button>
                                            <button type="reset" class="btn btn-warning cancel">
                                                <i class="icon-ban-circle icon-white"></i>
                                                <span>Cancelar Carga</span>
                                            </button>
                                            <button type="button" class="btn btn-danger delete">
                                                <i class="icon-trash icon-white"></i>
                                                <span>Eliminar Archivo</span>
                                            </button>
                                            <input type="checkbox" class="toggle">
                                        </div><div class="span5 fileupload-progress fade">
                                            <!-- The global progress bar -->
                                            <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                <div class="bar" style="width:0%;"></div>
                                            </div>
                                            <!-- The extended global progress information -->
                                            <div class="progress-extended">&nbsp;</div>
                                        </div></div>
                                <div class="fileupload-loading"></div>
                                </form></td>
                        </tr>
                        <tr>
                            <td>Estudios de Laboratorio(Cardiologia, si lo requiere)</td>
                            <td><form id="archLab"><label for="laboratorio"></label>
                                    <input type="file" name="laboratorio" id="laboratorio" data-url="<?php echo file_url('documentos_cita'); ?>" /><br />
                                    <div id="lab_progress" class="bar"></div></form></td>
                        </tr>
                        <tr>
                            <td>Placas de Rayos X(Torax-Cardiologia, si lo requiere)</td>
                            <td><form id="archRx"><label for="Placas"></label>
                                    <input type="file" name="Placas[]" id="Placas" data-url="<?php echo file_url('documentos_cita'); ?>" multiple/><br />
                                    <div id="placas_progress" class="bar"></div></form></td>
                        </tr>
                        <tr>
                            <td>Estudios de Eco(Cardiología,si lo requiere)</td>
                            <td><form id="archEcos"><label for="Ecos"></label>
                                    <input type="file" name="Ecos[]" id="Ecos" data-url="<?php echo file_url('documentos_cita'); ?>" multiple /><br />
                                    <div id="eco_progress" class="bar"></div>
                                    <div class="row fileupload-buttonbar">
                                        <span class="btn btn-success fileinput-button">
                                            <i class="icon-plus icon-white"></i>
                                            <span>Añadir Estudio</span>
                                    </div></form></td>
                        </tr>
                        <tr><td colspan="2" align="center"><p>
                                    <input type="button" name="regresar" id="regresar" value="regresar" onClick="cancelarCita();" />  
                                    &nbsp;      
                                    <input name="apartarCita" type="button" value="Apartar Cita" onClick="apartarCitaConfirmar()" />

                                </p>
                                <p class="obligatorios">* campo obligatorio</p></td></tr>
                    </table></form></td>
            </tr>
        </table>
    </body>
    <script src="<?php echo site_url('lib/jQuery-file-Upload/js/jquery.fileupload-ui.js'); ?>"></script>
    <script src="<?php echo site_url('lib/jQuery-file-Upload/js/jquery.fileupload.js'); ?>"></script>
    <script src="<?php echo site_url('lib/jQuery-file-Upload/js/jquery.iframe-transport.js'); ?>"></script>
    <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#referencia").fileupload({
                                            dataType: "json",
                                            done: function(e, data)
                                            {
                                                $.each(data.result.files, function(index, file) {
                                                    $('<p/>').text(file.name).appendTo(document.body);
                                                });
                                            },
                                            progressall: function(e, data) {
                                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                                $('#ref_progress .bar').css(
                                                        'width',
                                                        progress + '%'
                                                        );
                                            }
                                        });
                                        $("#laboratorio").fileupload({
                                            dataType: "json",
                                            done: function(e, data)
                                            {
                                                $.each(data.result.files, function(index, file) {
                                                    $('<p/>').text(file.name).appendTo(document.body);
                                                });
                                            },
                                            progressall: function(e, data) {
                                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                                $('#lab_progress .bar').css(
                                                        'width',
                                                        progress + '%'
                                                        );
                                            }
                                        });
                                        $("#Placas").fileupload({
                                            dataType: "json",
                                            done: function(e, data)
                                            {
                                                $.each(data.result.files, function(index, file) {
                                                    $('<p/>').text(file.name).appendTo(document.body);
                                                });
                                            },
                                            progressall: function(e, data) {
                                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                                $('#placas_progress .bar').css(
                                                        'width',
                                                        progress + '%'
                                                        );
                                            }
                                        });

                                        $("#Ecos").fileupload({
                                            dataType: "json",
                                            done: function(e, data)
                                            {
                                                $.each(data.result.files, function(index, file) {
                                                    $('<p/>').text(file.name).appendTo(document.body);
                                                });
                                            },
                                            progressall: function(e, data) {
                                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                                $('#eco_progress .bar').css(
                                                        'width',
                                                        progress + '%'
                                                        );
                                            }
                                        });
                                    });
    </script>

</html>
