<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Documento sin título</title>
    </head>

    <body>
        <?php
        error_reporting(E_ALL ^ E_NOTICE);
        session_start();
        include('lib/funciones.php');
        $datosUsuario = obtenerDatosUsuario($_SESSION['idUsuario']);
        $datosClinica = obtenerDatosClinica($datosUsuario['id_clinica']);
        ?>
        <table width="1024" border="1">
            <tr>
                <td class="tituloVentana">Contrarreferencias Recibidas </td>
            </tr>
            <tr>
                <td>Buscar Referencias por continuación de tercer nivel&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <select name="seleccion" id="seleccion" onchange="javascript:filtroBusqueda()">
                        <option value="0">todas</option>
                        <option value="1">si</option>
                        <option value="-1">no</option>
                    </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" name="actualizar" id="actualizar" value="Actualizar" onclick="filtroBusqueda()"/></td>
            </tr>
            <tr>
                <td><div id="Referencias">
                        <table id="contrarreferencias">
                            <thead>
                                <tr>
                                    <td scope="col">Folio</td>
                                    <td scope="col">C&eacute;dula</td>
                                    <td scope="col">Nombre de Derechohabiente</td>
                                    <td scope="col">Servicio</td>
                                    <td scope="col">Fecha de Autorizaci&oacute;n por el depto. de Contrarreferencia</td>
                                    <td scope="col">Continua tratamiento en tercer Nivel</td>
                                    <td scope="col">imprimir</td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div></td>
            </tr>
        </table>
    </body>
    <script type="text/javascript">
        $(document).ready(function() {
            var uTable = $("#contrarreferencias").dataTable({
                'bProcessing': true,
                'bDestroy': true,
                'bRetrieve': true,
                'sScrollY': '400px',
                'sScrollX': '100%',
                'bJQueryUI': true,
                'bPaginate': false,
                'bAutoWidth': false,
                'bScrollCollapse': false,
                'ordercolumn': '',
                'aaSorting': [[0, 'desc']],
                'sAjaxSource': 'ajaxnullsource.txt',
                'aoColumns': [
                    {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
                    {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
                    {'bSearchable': true, 'bSortable': true},
                    {'bSearchable': true, 'bSortable': true},
                    {'bSearchable': true, 'bSortable': true},
                    {'sClass': 'dt_center','bSearchable': true, 'bSortable': true},
                    {'sClass': 'dt_center'}

                ],
                'oLanguage': {
                    'sProcessing': 'Procesando...',
                    'sLengthMenu': 'Mostrar _MENU_ registros',
                    'sZeroRecords': 'No se encontraron resultados',
                    'sInfo': 'Mostrando desde _START_ hasta _END_ de _TOTAL_ registros',
                    'sInfoEmpty': 'Mostrando desde 0 hasta 0 de 0 registros',
                    'sInfoFiltered': '(filtrado de _MAX_ registros en total)',
                    'sInfoPostFix': '',
                    'sSearch': 'Buscar:',
                    'sUrl': '',
                    'oPaginate': {
                        'sFirst': '&nbsp;Primero&nbsp;',
                        'sPrevious': '&nbsp;Anterior&nbsp;',
                        'sNext': '&nbsp;Siguiente&nbsp;',
                        'sLast': '&nbsp;&Uacute;ltimo&nbsp;'
                    }
                }

            });
            sUrl_source_ajax = '';
            uTable.fnReloadAjax('referenciasEnviadas.php');
            uTable.fnAdjustColumnSizing();
        });
    </script>
</html>
