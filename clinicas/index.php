<?php
session_start();
$_SESSION['idUsuario'] = "-1";
$_SESSION['tipoUsuario'] = "-1";
$_SESSION['idClinica'] = "-1";
?>

<!DOCTYPE html>
<html>
    <head>
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Citas y contrarrefefencias de Unidades Medicas al HRVGF</title>
       
        
        <script src="lib/jquery-ui-1.10.4.custom/js/jquery-1.10.2.js"></script>
        <script src="lib/cookies.js"></script>
        <script src="lib/funciones.js"></script>
        <script src="lib/datatables-1.9.4/media/js/jquery.dataTables.js"></script>
        <script src="lib/datatables-1.9.4/media/js/jquery.dataTables.ex.js"></script>
        <script src="lib/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.js"></script>
        <script src="lib/jquery-validate/dist/jquery.validate.js"></script>
        <script src="lib/jquery-validate/dist/additional-methods.js"></script>
        
        <script type="text/javascript">

            $(document).unload(function() {

                eliminarCitas();

            });
            
            $(document).ready(
                function(){
                    cargarLogin();
                });

        </script>
        
        <link rel="stylesheet" href="lib/jquery-ui-1.10.4.custom/css/issstePRI/jquery-ui-1.10.4.custom.css" type="text/css">';
        <link rel="stylesheet" href="lib/datatables-1.9.4/media/css/jquery.dataTables.css" type="text/css">
        <link rel="stylesheet" href="lib/datatables-1.9.4/media/css/jquery.dataTables_themeroller.css" type="text/css">
        <link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
        
        <style type="text/css">
            .tituloVentana{
                background-image: url(diseno/fondoEncabezado.jpg);
            }
            .tituloEncabezado {
                font-size: 36px;
                font-family:Arial, Helvetica, sans-serif;
                color: #808080;
            }
            .Ventana {
                background-color: #FFF;
            }
            .subtituloEncabezado {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 16px;
                color: #2A3F55;
            }
        </style>
    </head>
    <!---<body onload="cargarLogin();" onbeforeunload="eliminarCitas()"> --->
    <body>
                   
                    <div class="Ventana" width="100%" border="1">
                        <header class="tituloVentana" align="center"><table align="center">
                                    <tr>
                                        <td rowspan="2"><img src="diseno/LOGO-ISSSTE.jpg" alt="" /></td>
                                      <td class="tituloEncabezado">Sistema de Referencias de Clinicas</td>
                                        
                          </tr>
                                    <tr>
                                      <td class="subtituloEncabezado">INSTITUTO DE SEGURIDAD Y SERVICIOS SOCIALES DE LOS TRABAJADORES DEL ESTADO</td></tr>
                                    <tr><td colspan="3"><div id="clinica" class="subtituloEncabezado"></div></td></tr>
                                </table></header>
                                <div id="menus" style="width:auto;"><menu id="menuReferencia" style="display: none">
                                    <table width="100%">
                                        <tr align="center">
                                            <td></td>
                                            <td><a href="javascript:buscarCitas()" class="botones_menu"><img src="diseno/calendario.jpg" width="50" height="50" ><br>Consultar Estado de <br> citas apartadas</a></td>
                                            <td>&nbsp;</td>
                                            <td><a href="javascript:ConsultaReferencias()" class="botones_menu"><img src="diseno/buscarReferencias.png" width="40" height="40"><br>Contrarreferencia del<br> Hospital Regional</a></td>
                                            <td><a href="javascript:enviarReferencia()" class="botones_menu"><img src="diseno/enviarReferencia.png" width="40" height="40"><br>Enviar Referencia <br> a Hospital</a></td>
                                            <td><a href="javascript:ConsultaReferenciasDH()" class="botones_menu"><img src="diseno/buscar_expediente.png" width="40" height="40"><br>Consulta de Referencias Recibidas<br> por Derechohabiente</a></td>
                                            <td><a href="javascript:ListaMedicamentos()" class="botones_menu"><img src="diseno/_medAdmi.gif" width="40" height="40"><br>Lista de Medicamentos referidos <br>de Hospital</a></td>
                                            <td><a href="javascript:ReporteMedicamentos()" class="botones_menu"><img src="diseno/_medAdmi.gif" width="40" height="40"><br>Reporte Mensual de Medicamentos referidos <br>de Hospital</a></td>
                                            <td><a href="javascript:DesactivarTratamientos()" class="botones_menu"><img src="diseno/_medDel.gif" width="40" height="40"><br>Desactivar Tratamientos Cr&oacute;nicos</a></td>
                                            <td><a href="logout.php" class="botones_menu"><img src="diseno/logout.png"><br>Salir</a></td>
                                        </tr>
                                    </table>
                                </menu>
                                    <menu id="menuVisor" style="display: none">
                                    <table>
                                        <tr align="center">
                                            <td>&nbsp;</td>
                                            <td><a href="javascript:ConsultaReferencias()" class="botones_menu"><img src="diseno/buscarReferencias.png" width="40" height="40"><br>Contrarreferencia del<br> Hospital Regional</a></td>
                                            <td><a href="javascript:VerVisor()" class="botones_menu"><img src="diseno/buscar.png" width="40" height="40"><br>Ver Historial de <br> Citas en <br>Hospital Regional</a></td>
                                            <td><a href="javascript:buscarCitas()" class="botones_menu"><img src="diseno/calendario.jpg" width="50" height="50" ><br>Consultar Estado de <br> citas apartadas</a></td>
                                            <td><a href="javascript:ConsultaReferenciasDH()" class="botones_menu"><img src="diseno/buscar_expediente.png" width="40" height="40"><br>Consulta de Referencias Recibidas<br> por Derechohabiente</a></td>
                                            <td><a href="javascript:ListaMedicamentos()" class="botones_menu"><img src="diseno/_medAdmi.gif" width="40" height="40"><br>Lista de Medicamentos referidos <br>de Hospital</a></td>
                                            <td><a href="javascript:ReporteMedicamentos()" class="botones_menu"><img src="diseno/_medAdmi.gif" width="40" height="40"><br>Reporte Mensual de Medicamentos referidos <br>de Hospital</a></td>
                                            <td><a href="logout.php" class="botones_menu"><img src="diseno/logout.png"><br>Salir</a></td>
                                        </tr>
                                    </table>
                                </menu></div><div id="contenido" align="center"></div></td>
                    <span class="Ventana"></span></div>
    </body>
</html>
