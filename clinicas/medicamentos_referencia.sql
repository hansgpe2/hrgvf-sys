/*
SQLyog Ultimate v9.63 
MySQL - 5.6.12 : Database - sistema_agenducha
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistema_agenducha` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistema_agenducha`;

/*Table structure for table `medicamentos_contrarreferencias` */

DROP TABLE IF EXISTS `medicamentos_contrarreferencias`;

CREATE TABLE `medicamentos_contrarreferencias` (
  `id_medicamento` int(11) DEFAULT NULL,
  `id_contrarreferencia` int(11) DEFAULT NULL,
  `id_derecho` int(11) DEFAULT NULL,
  `dias` int(11) DEFAULT NULL,
  `cajas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
