<?php 
$mes=date("n");
$dia=date("d");
$meses=array("ene","feb","marzo","abril","mayo","junio","julio","ago","sept","oct","dic","");
session_start();
include('lib/funciones.php');
$clinica=obtenerDatosClinica($_SESSION['idClinica']);
$estado=$clinica['Estado'];
if(isset($_REQUEST['cedula']))
	$cedula=$_REQUEST['cedula'];
else
	$cedula="";
?>
<style type="text/css">
.etiquetas {
	font-size: xx-small;
	color: #F00;
}
</style>

<form name="agregarDH" id="agregarDH" submit="javascript:validarAgregarDH();">
    <table width="945" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="5" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td width="187">&nbsp;</td>
              <td width="177">&nbsp;</td>
            </tr>
            <tr>
                <td height="25" align="right"><label for="cedulaAgregar" class="error"><span class="textosParaInputs">CEDULA:</span></label> </td>
              <td align="left"><input name="cedulaAgregar" type="text" id="cedulaAgregar" tabindex="1" onfocus="if(this.value=='Cedula del DerechoHabiente'){this.value=''}" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo $cedula; ?>" maxlength="10"/>
                 
              </td>
            </tr>
            <tr><td class="textosParaInputs" align="right">Sexo</td><td><select name="sexo" id="sexo" tabindex="2" onchange="ObtenerTpoDerXSexo(this.id,'cedulaTipoAgregar');">
              <option value="-1" selected="selected"></option>
                        <option value="H">Hombre</option>
                        <option value="M">Mujer</option>
      </select></td><td width="236" class="textosParaInputs" align="right">Tipo de DerechoHabiente</td><td width="345"><select name="cedulaTipoAgregar" id="cedulaTipoAgregar" tabindex="3" onfocus="ObtenerTpoDerXSexo('sexo',this.id);"></select></td></tr>
  <tr>
    <td height="25" class="textosParaInputs" align="center" colspan="2">NOMBRE DEL DERECHOHABIENTE:</td></tr>
    <tr><td height="26" align="right" class="textosParaInputs">Apellido Paterno
      </td><td align="left" colspan="3"><input name="ap_pAgregar" type="text" id="ap_pAgregar" tabindex="4" onfocus="javascript:borrarReferencias(this);" onkeyup="this.value = this.value.toUpperCase();" size="20" maxlength="20" />
        <span class="textosParaInputs">Apellido Materno</span>
        <input name="ap_mAgregar" type="text" id="ap_mAgregar" tabindex="5" onkeyup="this.value = this.value.toUpperCase();" size="20" maxlength="20" /><label for="ap_mAgregar" class="error"></label></td></tr>
<tr><td class="textosParaInputs" align="right">Nombre(s)</td><td colspan="3" align="left">
        <input name="nombreAgregar" type="text" id="nombreAgregar" tabindex="6" onkeyup="this.value = this.value.toUpperCase();" size="50" maxlength="50" /><label for="nombreAgregar" class="error"></label></td>
  </tr>
  <tr>
      <td height="25" align="right"><label for="telefonoAgregar" class="error"><span class="textosParaInputs">TELEFONO: (01)</span></label></td>
    <td align="left" colspan="3"><input name="telefonoAgregar" type="text" id="telefonoAgregar" tabindex="6" size="11" maxlength="10" />
      <span class="etiquetas">
       formato (lada)numero ej 3312345678
      </span>
      <label for="fecha_nacAgregar" class="error"><span class="textosParaInputs"> FECHA NAC. </span></label>
      <span style="position:relative">
      <label for="mes1">mes</label>
      <select name="mes1" id="mes1" tabindex="7" onchange="calendarios('1');">
        <option value="-1"></option>
        <option value="01">ene</option>
        <option value="02">feb</option>
        <option value="03">mar</option>
        <option value="04">abr</option>
        <option value="05">may</option>
        <option value="06">jun</option>
        <option value="07">jul</option>
        <option value="08">ago</option>
        <option value="09">sept</option>
        <option value="10">oct</option>
        <option value="11">nov</option>
        <option value="12">dic</option>
      </select>
      <label for="dia1">dia</label>
      <select name="dia1" id="dia1" tabindex="8">
        <option value="-1"></option>
        <option value="01">1</option>
        <option value="02">2</option>
        <option value="03">3</option>
        <option value="04">4</option>
        <option value="05">5</option>
        <option value="06">6</option>
        <option value="07">7</option>
        <option value="08">8</option>
        <option value="09">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
      </select>
      <label for="anio1">año</label>
      <select name="anio1" id="anio1" tabindex="9">
        <?php
			$ini=date("Y");
			$fin=$ini-150;
			for($i=$ini;$i>$fin;$i--)
			{
				echo "<option value='$i'>$i</option>";
			}
		?>
      </select>
      </span></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td colspan="3" align="left"><select name="estadoAgregar" id="estadoAgregar" tabindex="10" onfocus="cargarEstados('estadoAgregar','<?php echo $estado; ?>')" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');">
    </select>
      <span class="textosParaInputs">
        <input name="estadoClinica" type="hidden" id="estadoClinica" value="<?php echo $clinica['Estado']; ?>" />
        MUNICIPIO: </span>
      <select name="municipioAgregar" id="municipioAgregar" tabindex="11" onchange="obtenerCodigoPostal('estadoAgregar',this.id,'colonia');cargarColonias('colonia','codigoPostal', 'municipioAgregar');">
      </select>
      <input name="municipioClinica" type="hidden" id="municipioClinica" value="<?php echo $clinica['Municipio']; ?>" /></td>
    </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right"><label for="direccionAgregar" class="error"><span class="textosParaInputs">DIRECCION:</span></label></td>
    <td colspan="3" align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" tabindex="12" onkeyup="this.value = this.value.toUpperCase();" size="64" maxlength="50" />
      <span class="etiquetas">No usar simbolos :#,*,$,',&lt;,&gt;,|, solo números y letras</span></td>
    </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right"><label for="colonia" class="textosParaInputs">COLONIA</label></td>
    <td colspan="3" align="left"><select name="colonia" id="colonia" tabindex="13" onchange="obtenerCodigoPostal('estadoAgregar','municipioAgregar',this.id,'codigoPostal');">
    </select>
  &nbsp;&nbsp;<span class="textosParaInputs">CODIGO POSTAL</span>
  <select name="codigoPostal" id="codigoPostal" tabindex="14" onchange="cargarColonias('colonia',this.id, 'municipioAgregar')"  value="0" >
</select></td>
    </tr>
  <tr><td class="textosParaInputs">Unidad Medica de Adscripcion</td><td colspan="3"><?php echo $clinica['nombre']; ?>
    <input name="Unidad" type="hidden" id="Unidad" value="<?php echo $clinica['id_unidad']; ?>" /></td></tr>
        <tr>
          <td align="right" class="textosParaInputs">CORREO ELECTR&Oacute;NICO</td>
          <td colspan="3"><input name="email" type="text" id="email" tabindex="15"  />
  &nbsp;<span class="textosParaInputs">CELULAR</span>&nbsp;
  <input name="cel" type="text" id="cel" tabindex="16" size="10" maxlength="10" /></td>
        </tr>
            <tr>
              <td colspan="4" align="center"><div id="divBotones_EstadoAgregarDH">
                      <button id="enviar" type="button" class="botones" onclick="regresar()">Regresar</button>&nbsp;&nbsp;<input name="" type="button" class="botones" id="guardar" tabindex="17" onclick="validarAgregarDH()" value="Agregar">
              </div>
                </td></tr>
                <tr><td colspan="3"><div id="citas"></div></td></tr>
                          </table></form>