<?php
session_start();
include_once 'lib/funciones.php';
$derechoHabiente=$_REQUEST['idDerecho'];
$cita=$_REQUEST['idCita'];
$fecha=$_REQUEST['fechaCita'];
$der=DatosDerechoHabiente($derechoHabiente);
$idhorario=$_REQUEST['idHorario'];
$datosCita=datosCita($cita);
$horario=datosHorarioXcita($idhorario);
$servicio=datosServicio($horario['id_servicio']);
$medico=  getMedicoXid($horario['id_medico']);
//$listDiagnosticos=  ListaDiags();
?>
<!Doctype html>
<html>
    <head>
        
     
<style type="text/css">
.obligatorios {
	color: #D40000;
}
</style>
    </head>
    <body>
<table width="800" class="Ventana">
  <tr>
    <td class="tituloVentana">Cita del derechoHabiente <?php echo ponerAcentos($der['ap_p']." ".$der['ap_m']." ".$der['nombres']); ?> en el servicio <?php echo $servicio['nombre']; ?></td>
  </tr>
  <tr>
    <td>Fecha de la Cita apartada <?php echo date("d/m/Y",strtotime($datosCita['fecha_cita'])); ?> hora de la cita <?php echo date("H:i",strtotime($horario['hora_inicio']))." a ".date("H:i",strtotime($horario['hora_fin'])); ?></td>
  </tr>
  <tr>
    <td><form action="apartarCita.php" method="post" enctype="multipart/form-data" id="FormCita" name="FormCita" target="_blank"><input type="hidden" id="dh"  name="dh" value="<?php echo $derechoHabiente; ?>"  /><input type="hidden" id="idCita" name="idCita" value="<?php echo $cita; ?>"  />
        <input name="clinica" type="hidden" id="clinica" value="<?php echo $_SESSION['idClinica']; ?>">
        <input type="hidden" name="fechaCita" id="fechaCita" value="<?php echo $fecha; ?>" >
        <input type="hidden" name="servicio" id="servicio" value="<?php echo $servicio['id_servicio']; ?>" >
        <input type="hidden" name="medico" id="medico" value="<?php echo $medico['id_medico'] ?>" >
        <input type="hidden" name="horaIni" id="horaIni" value="<?php echo $horario['hora_inicio'] ?>" >
        <input type="hidden" name="idDh" id="idDH" value="<?php echo $derechoHabiente; ?>" >
      <table width="100%" border="1">
          <tr>
        <td width="56%">Referencia Firmada<span class="obligatorios"> *</span></td>
        <td width="44%"><label for="referencia"></label>
          <input type="file" name="referencia" id="referencia" /></td>
      </tr>
      <tr>
        <td>Estudios de Laboratorio(Cardiologia, si lo requiere)</td>
        <td><label for="laboratorio"></label>
          <input type="file" name="laboratorio" id="laboratorio" /></td>
      </tr>
      <tr>
        <td>Placas de Rayos X(Torax-Cardiologia, si lo requiere)</td>
        <td><label for="Placas"></label>
          <input type="file" name="Placas" id="Placas" /></td>
      </tr>
      <tr>
        <td>Estudios de Eco(Cardiología,si lo requiere)</td>
        <td><label for="Ecos"></label>
          <input type="file" name="Ecos" id="Ecos" /></td>
      </tr>
      <tr>
          <td><label for="diagnostico">Diagnostico</label><span class="obligatorios"> *</span></td><td><div class="ui_widget" style="display:block"><input name="diagnostico" type="text" id="diagnostico" onKeyUp="listaDiagnosticos('<?php echo $der['sexo'];?>','<?php echo $der['fecha_nacimiento']; ?>','resultados',this.value);">
            <a href="docs/CIE10_SIMEF.pdf" target="_blank" class="botones">Catalogo CIE10</a><div id="resultados" style="display:compact"></div>
            <div id="oDiag" style="display:none">
              <textarea name="diagnostico1" id="diagnostico1" cols="45" rows="5"></textarea>
            </div>
          </div></td></tr>
          <tr>
            <td>Observaciones</td><td><label for="observaciones"></label>
              <textarea name="observaciones" id="observaciones" cols="45" rows="5"></textarea></td></tr>
      <tr><td colspan="2" align="center"><p>
        <input type="button" name="regresar" id="regresar" value="regresar" onClick="cancelarCita();" />  
        &nbsp;      
        <input name="apartarCita" type="button" value="Apartar Cita" onClick="apartarCitaConfirmar()" />
        <input name="citaRealizada" type="hidden" value="0" id="citaRealizada" />
      </p>
          <p class="obligatorios">* campo obligatorio</p></td></tr>
  </table></form></td>
  </tr>
</table>
    </body>
</html>
