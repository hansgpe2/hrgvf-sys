<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Documento sin título</title>
    </head>

    <body>
        <table width="100%" border="0">
            <tr>
                <td class="tituloVentana">ACEPTAR CITAS DE CLINICAS</td>
            </tr>
            <tr>
                <td><table width="100%" border="0">
                        <tr>
                            <td>Filtrar Busqueda
                                <select name="selectFiltro" id="selectFiltro" onChange="filtrarCitas()">
                                    <option value="-1" selected="selected">Seleccione Filtro</option>
                                    <option value="3">Todas</option>
                                    <option value="4">rechazadas</option>
                                    <option value="5">pendientes</option>
                                    <option value="2">aceptadas</option>
                                    <option value="1">fecha</option>
                                    <option value="6">reprogramadas</option>
                                    <option value="7">folio</option>
                                </select><br>
                                Si selecciona todas las citas puede tardar la busqueda
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>

                        </tr>
                        <tr>
                            <td>
                                <div id="busquedaFecha" style="display:none ">
                                    <form id="buscarCitasXFecha" action="javascript:mostrarCitas();">
                                    <table width="100%">
                                        <tr>
                                            <td>de
                                                <input name="altfecha1" type="text" id="altfecha1" value="<?php echo date("d-m-Y"); ?>" />
                                                <input name="fecha1" id="fecha1" type="hidden" value="<?php echo date("Ymd"); ?>">
                                                <label for="mes1"></label>
                                                <br />
                                                &nbsp;&nbsp;a
                                                <input name="altfecha2" type="text" id="altfecha2" value="<?php echo date("d-m-Y"); ?>">
                                                <input name="fecha2" id="fecha2" type="hidden" value="<?php echo date("Ymd"); ?>">
                                            </td>
                                        </tr>
                                        <tr><td align="center"><input type="submit" value="Ver Citas" class="botones"></td></tr>
                                    </table></form>
                                </div></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td>
                    <div id="folio" style="display:none "><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>Numero de Folio</td>
                                <td><input name="folio_field" type="text" id="folio_field"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"><input type="button" name="Submit" value="Ver Citas" class="botones" onclick="mostrarCitas();"></td>
                            </tr>
                        </table>
                    </div></td>
            </tr>
            <tr>
                <td><div id="citas">
                        <table id="citasApartadas" >
                            <thead>
                                <tr>
                                    <th scope="col">Solicitud</th>
                                    <th scope="col">Cedula</th>
                                    <th scope="col">Nombre del Derechohabiente</th>
                                    <th scope="col">Servicio</th>
                                    <th scope="col">Estado de la cita</th>
                                    <th scope="col">Motivo de rechazo</th>
                                    <th scope="col">Fecha de solicitud</th>
                                    <th scope="col">Fecha de revisi&oacute;n</th>
                                    <th scope="col">Fecha nueva de la Cita</th>
                                    <th scope="col">M&eacute;dico</th>
                                    <th scope="col">Usuario captura</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
         <script type="text/javascript">
                            $(document).ready(function() {
                                
                                $("#altfecha1").datepicker({
                                    changeMonth: true,
                                    changeYear: true,
                                    dateFormat: "dd-mm-yy",
                                    altField: "#fecha1",
                                    altFormat: "yymmdd"
                                });
                                $("#altfecha2").datepicker({
                                    changeMonth: true,
                                    changeYear: true,
                                    dateFormat: "dd-mm-yy",
                                    altField: "#fecha2",
                                    altFormat: "yymmdd"
                                });
                                var uTable = $("#citasApartadas").dataTable({
                                    'bProcessing': true,
                                    'bDestroy':true,
                                    'bRetrieve':true,
                                    'sScrollY': '400px',
                                    'sScrollX': '100%',
                                    'bJQueryUI': true,
                                    'bPaginate': false,
                                    'bAutoWidth': false,
                                    'bScrollCollapse': false,
                                    'ordercolumn': '',
                                    'aaSorting': [[0, 'desc']],
                                    
                                    'sAjaxSource': 'ajaxnullsource.txt',
                                    'aoColumns': [
                                        {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
                                        {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
                                        {'bSearchable': true, 'bSortable': true},
                                        {'bSearchable': true, 'bSortable': true},
                                        {'bSearchable': true, 'bSortable': true},
                                        {'sClass': 'dt_center'},
                                        {'sClass': 'dt_center'},
                                        {'sClass': 'dt_center'},
                                        {'sClass': 'dt_center'},
                                        {'sClass': 'dt_center'},
                                        {'sClass': 'dt_center'}

                                    ],
                                    'oLanguage': {
                                        'sProcessing': 'Procesando...',
                                        'sLengthMenu': 'Mostrar _MENU_ registros',
                                        'sZeroRecords': 'No se encontraron resultados',
                                        'sInfo': 'Mostrando desde _START_ hasta _END_ de _TOTAL_ registros',
                                        'sInfoEmpty': 'Mostrando desde 0 hasta 0 de 0 registros',
                                        'sInfoFiltered': '(filtrado de _MAX_ registros en total)',
                                        'sInfoPostFix': '',
                                        'sSearch': 'Buscar:',
                                        'sUrl': '',
                                        'oPaginate': {
                                            'sFirst': '&nbsp;Primero&nbsp;',
                                            'sPrevious': '&nbsp;Anterior&nbsp;',
                                            'sNext': '&nbsp;Siguiente&nbsp;',
                                            'sLast': '&nbsp;&Uacute;ltimo&nbsp;'
                                        }
                                    }
                                    
                                });
                                /*sUrl_source_ajax = 'MostrarCitasApartadas.php';
                                uTable.fnReloadAjax(sUrl_source_ajax);*/
                                uTable.fnAdjustColumnSizing();

                                
                            });
                        </script>
    </body>
</html>