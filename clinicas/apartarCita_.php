<!DOCTYPE html>
<html>
<head>
<title>Comprobante de Cita</title>
<link href="lib/impresion2.css">
<link href="lib/impresion2.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="window.print();">
<p>
  <?php
session_start();
include 'lib/funciones.php';
if(isset($_FILES))
{
    $referencia=$_FILES['referencia'];
    $tipoRef=$_FILES['referencia']['type'];
    $nomRef=$_FILES['referencia']['name'];
    $error_stringRef=$_FILES['referencia']['error'];
    $laboratorio=$_FILES['laboratorio'];
    $tipoLab=$_FILES['laboratorio']['type'];
    $nomLab=$_FILES['laboratorio']['name'];
    $error_stringLab=$_FILES['laboratorio']['error'];
    $placas=$_FILES['Placas'];
    $tipoPlacas=$_FILES['Placas']['type'];
    $nomPlacas=$_FILES['Placas']['name'];
    $error_stringPlacas=$_FILES['Placas']['error'];
    $ecos=$_FILES['Ecos'];
    $tipoEcos=$ecos['type'];
    $nombreEcos=$ecos['name'];
    $errorEcos=$ecos['error'];
    $cita=$_REQUEST['idCita'];
    $raiz="../documentos_cita/";
    if(!file_exists($raiz))
    {
        mkdir($raiz);
    }
    $carpeta=$raiz."/".$cita."/";    
    $idDerecho=$_REQUEST['dh'];
    $arch=0;
    
    mkdir($carpeta);
    if((strpos($tipoRef,"pdf") || (strpos($tipoRef,"jpeg") || strpos($tipoRef,"jpg"))) && empty($error_stringRef))
    {
        $tmp=$referencia['tmp_name'];
        $nomArch= explode(".", $nomRef);
        $ext=$nomArch[1];
        $nombreNuevo="Referencia_cita.".$ext;
        $ubicacion=$carpeta.$nombreNuevo;
        if(move_uploaded_file($tmp,$ubicacion))
        {
            $sql="insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
            $ej=  insertarSQL($sql);
            if($ej[0]==0)
            {
                $arch+=1;
            }
        }
    }
    if((strpos($tipoLab,"pdf") || (strpos($tipoLab,"jpeg") || strpos($tipoLab,"jpg"))) && empty($error_stringLab))
    {
        $tmp=$laboratorio['tmp_name'];
        $nomArch= explode(".", $nomLab);
        $ext=$nomArch[1];
        $nombreNuevo="estudios_laboratorio.".$ext;
        $ubicacion=$carpeta.$nombreNuevo;
        if(move_uploaded_file($tmp,$ubicacion))
        {
            $sql="insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
            $ej=  insertarSQL($sql);
            if($ej[0]==0)
            {
                $arch+=1;
            }
        }
    }
    
    if((strpos($tipoEcos,"pdf") || (strpos($tipoEcos,"jpeg") || strpos($tipoEcos,"jpg"))) && empty($errorEcos))
    {
        $tmp=$ecos['tmp_name'];
        $nomArch= explode(".", $nombreEcos);
        $ext=$nomArch[1];
        $nombreNuevo="Ecos".$ext;
        $ubicacion=$carpeta.$nombreNuevo;
        if(move_uploaded_file($tmp,$ubicacion))
        {
            $sql="insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
            $ej=  insertarSQL($sql);
            if($ej[0]==0)
            {
                $arch+=1;
            }
        }
    }
    
    if((strpos($tipoPlacas,"pdf") || (strpos($tipoPlacas,"jpeg") || strpos($tipoPlacas,"jpg"))) && empty($error_stringPlacas))
    {
        $tmp=$placas['tmp_name'];
        $nomArch= explode(".", $nomPlacas);
        $ext=$nomArch[1];
        $nombreNuevo="Placas_tx.".$ext;
        $ubicacion=$carpeta.$nombreNuevo;
        if(move_uploaded_file($tmp,$ubicacion))
        {
            $sql="insert into archivos_referencia values(NULL,$idDerecho,$cita,'$nombreNuevo','$ubicacion');";
            $ej=  insertarSQL($sql);
            if($ej[0]==0)
            {
                $arch+=1;
            }
        }
    }
    if($arch>0)
    {
        $idUnidad=$_SESSION['idClinica'];
        $datosCita=  datosCita($cita);
		$diagnostico=$_REQUEST['diagnostico'];
		if($diagnostico=="otro")
		$diagnostico=$_REQUEST['diagnostico1'];
		$usuario=$_SESSION['idUsuario'];
        $sql="insert into citas_apartadas values(NULL,$cita,$idUnidad,-1,'".$diagnostico."','".$_REQUEST['observaciones']."','',".date("Ymd").",NULL,".$datosCita['id_derecho'].",".$datosCita['id_horario'].",$usuario);";
        $ej=  insertarSQL($sql);
		$datosDerecho=DatosDerechoHabiente($datosCita['id_derecho']);
		$horario=datosHorarioXcita($datosCita['id_horario']);
		$servicio=DatosServicio($horario['id_servicio']);
		$medico=getMedicoXid($horario['id_medico']);
        if($ej[0]==0){
			?>
</p>
<table width="57%" height="71" border="0" class="tablaDiag">
  <tr>
  <td><img src="diseno/logoEncabezado.jpg" width="112" height="49"></td>
    <td class="encabezado" align="center">Cita Apartada</td>
  </tr>
  <tr>
    <td width="39%" class="encabezado">Cedula</td>
    <td width="61%" class="datos"><?php echo $datosDerecho['cedula']."/".$datosDerecho['cedula_tipo']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">Nombre</td>
    <td><?php echo ponerAcentos($datosDerecho['ap_p']." ".$datosDerecho['ap_m']." ".$datosDerecho['nombres']); ?></td>
  </tr><tr>
  <td class="encabezado">Telefono</td>
    <td><?php echo $datosDerecho['telefono']; ?></td>
    </tr>
    <td class="encabezado">Correo Electronico</td>
    <td><?php echo $datosDerecho['email']; ?></td>
    </tr>
    <tr><td class="encabezado">Celular</td>
    <td><?php echo $datosDerecho['celular']; ?></td>
    </tr>
  <tr>
    <td class="encabezado" colspan="2" align="center">Datos de la Cita</td>
    
  </tr>
  <tr>
    <td class="encabezado">Fecha</td>
    <td><?php echo date("d/m/Y",strtotime($datosCita['fecha_cita']))." ".date("H:i",strtotime($horario['hora_inicio']))."-".date("H:i",strtotime($horario['hora_fin'])); ?></td>
  </tr>
  <tr>
    <td class="encabezado">Servicio</td>
    <td><?php echo $servicio['nombre']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">M&eacute;dico</td>
    <td><?php echo $medico['titulo']." ".$medico['ap_p']." ".$medico['ap_m']." ".$medico['nombres']; ?></td>
  </tr>
  <tr>
    <td class="nota" colspan="2">Cualquier cambio en su cita se le avisara en los medios de contactos proporcionados</td>
    
  </tr>
</table>
<p>&nbsp;</p>
<p>
  <?php
		}
    }
    else
    {
        header("location:cancelarCita.php?idCita=".$cita);
        
    } }?>
  <br />
</p>
</body>
</html>