-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-04-2013 a las 15:34:00
-- Versión del servidor: 5.0.67-community-nt
-- Versión de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_agenducha`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos_referencia`
--

CREATE TABLE IF NOT EXISTS `archivos_referencia` (
  `id_archivo` int(11) NOT NULL auto_increment,
  `id_derecho` int(11) default NULL,
  `id_cita` int(11) default NULL,
  `archivo` varchar(30) default NULL,
  `ubicacion` varchar(100) default NULL,
  PRIMARY KEY  (`id_archivo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas_apartadas`
--

CREATE TABLE IF NOT EXISTS `citas_apartadas` (
  `id_cita_ap` int(11) NOT NULL auto_increment,
  `id_cita` int(11) NOT NULL,
  `id_unidad` int(11) NOT NULL,
  `aceptada` tinyint(1) default NULL,
  `diagnostico` varchar(400) default NULL,
  `observaciones` varchar(400) default NULL,
  `motivo_rechazo` varchar(300) default NULL,
  `fecha_apartada` date default NULL,
  `fecha_revisada` date default NULL,
  `id_derecho` int(11) default NULL,
  `id_horario` int(11) default NULL,
  PRIMARY KEY  (`id_cita_ap`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
