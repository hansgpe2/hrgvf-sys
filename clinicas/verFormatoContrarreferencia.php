<?php

error_reporting(E_ERROR);
session_start();
include './Connections/bdissste.php';
include './lib/funciones.php';
include './lib/consultaSQL.php';
require_once './lib/mpdf/mpdf.php';

function ObtenerMedicamentosXReferenciaInt($idRef) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $link = new mysqli();
    $link->connect($hostname_bdissste, $username_bdissste, $password_bdissste, $database_bdisssteR);
    $sql = "Select * from medicamentos_contrarreferencias where id_contrarreferencia=$idRef";
    $query = $link->query($sql);
    $ret = array();
    while ($row = $query->fetch_assoc()) {
        $ret[] = $row;
    }
    $link->close();
    return $ret;
}

$idContra = $_REQUEST['idContra'];
$csql = new consultaSQL();
$link = new mysqli();
$link->connect($hostname_bdissste, $username_bdissste, $password_bdissste, $database_bdisssteR);
$sql = $csql->select_where('contrarreferencias', array('id_contra' => $idContra));
$qContrarref = $link->query($sql);
if ($link->errno > 0)
    print_r($link->error_list . " " . $sql);
$dContrarreferencia = $qContrarref->fetch_assoc();
$dDerecho = DatosDerechoHabiente($dContrarreferencia['id_derecho']);
$sql = $csql->select_where('vgf', array('id_unidad' => $dContrarreferencia['id_unidad']));
$dUnidad = $link->query($sql)->fetch_assoc();
$sql = $csql->select_where('medicos', array('id_medico' => $dContrarreferencia['id_medico']));
$query = $link->query($sql);
$dMedico = $query->fetch_assoc();
$sql = $csql->select_campos_where('servicios', array('nombre'), array('id_servicio' => $dContrarreferencia['id_servicio']));
$query = $link->query($sql);
$dServicio = $query->fetch_assoc();
$medicinas = ObtenerMedicamentosXReferenciaInt($idContra);
$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>

        <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    </head>
    <body>
        <table border="0" cellpadding="5" cellspacing="5" width="100%">

            <tbody>
                <tr>
                    <td align="center" width="270"><img src="diseno/issste2013.jpg" alt="" height="80" width="270"></td>
                    <td class="encabezado" align="center" width="566">
                        <p>HOSPITAL REGIONAL </p>
                        <p>"DR. VALENT&Iacute;N G&Oacute;MEZ FAR&Iacute;AS" <br>
                            FORMATO DE CONTRARREFERENCIA</p>
                    </td>
                    <td width="221">
                        <p><img src="diseno/LogoHRVGF.jpg" alt="" height="104" width="141"></p>
                    </td>
                </tr>
            </tbody>
        </table>

        <p><span class="titulos">CLINICA DE ADSCRIPCI&Oacute;N</span><span class="datos">:' . ponerAcentos($dUnidad['nombre']) . '</span>
            <br>
            <span class="titulos">LOCALIDAD:' . ponerAcentos($dDerecho['municipio'] . ',' . $dDerecho['estado']) . '</span><span class="datos"></span>
            &nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">&nbsp;&nbsp;&nbsp;&nbsp;
                FECHA</span><span class="datos">' . date('d-m-Y', strtotime($dContrarreferencia['fecha_contrarreferencia'])) . '
                &nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">C&Eacute;DULA</span><span class="datos">:' . $dDerecho['cedula'] . '/' . $dDerecho['cedula_tipo'] . '</span></span><br>
            <span class="datos"><span class="titulos">NOMBRE</span>:&nbsp;' . ponerAcentos($dDerecho['ap_p'] . " " . $dDerecho['ap_m'] . " " . $dDerecho['nombres']) . '</span><br>
            <span class="datos"><span class="datos"></span> <span class="titulos">SERVICIO QUE ENVIA:</span>' . $dServicio['nombre'] . '</span><br>

            <span class="datos">
                <p>';
if ($dContrarreferencia['status_cita'] == 0)
    $html.="El paciente  continuar&aacute; su tratamiento con su m&eacute;dico familiar";
else
    $html.="El paciente  continuar&aacute; tratamiento con el m&eacute;dico especialista";
$html.='</p></span>
        <table style="width: 718px; height: 506px;" border="0" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td>
                        <p class="encabezado" align="center">INFORME DEL MEDICO CONSULTADO</p>
                    </td>
                </tr>
                <tr>
                    <td class="titulos">RESUMEN DE DATOS CLINICOS: </td>
                </tr>
                <tr>
                    <td class="datos">' . ponerAcentos($dContrarreferencia['resumen']) . '
                    <br>
                    </td>
                </tr>
                <tr>
                    <td class="titulos">DIAGNOSTICOS DE:</td>
                </tr>
                <tr>
                    <td>
                        <table class="tablaDiag" width="100%">
                            <tbody>
                                <tr>
                                    <td class="titulos">Referencia</td>
                                    <td class="titulos">Contrarreferencia</td>
                                </tr>
                                <tr>
                                    <td class="datos">' . ponerAcentos($dContrarreferencia['diagnostico_ref']) . '<br>
                                    </td>
                                    <td class="datos">' . ponerAcentos($dContrarreferencia['diagnostico']) . '<br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="titulos">SINTESIS DE LA EVOLUCION:</td>
                </tr>
                <tr>
                    <td class="datos">' . ponerAcentos($dContrarreferencia['evolucion']) . '</td>
                </tr>
                <tr>
                    <td class="titulos">TRATAMIENTO INSTITUIDO:</td>
                </tr>
                <tr>
                    <td class="datos">' . ponerAcentos($dContrarreferencia['tratamiento']) . '<br>
                    </td>
                </tr>
                <tr>
                    <td>';
if (count($medicinas)) {
    $html.='<table width="100%" border="1">
        <tr>
            <td class="encabezado">Clave</td>
          <td class="encabezado">Medicamento</td>
          <td class="encabezado">Tipo Tratamiento</td>
          <td class="encabezado">Tratamiento en Dias</td>
          <td class="encabezado">Cantidad de cajas</td>
          <td class="encabezado">Indicaciones</td>
        </tr>';

    foreach ($medicinas as $key => $medi)    
    {
        $medicina = getDatosMedicamentos($medi['id_medicamento']);

        $html.='<tr>
            <td>' . $medicina['id_medicamento'] . '</td>
            <td>';
        $desc = strtok($medicina['descripcion'], "/");
        $html.=$desc . ' </td>
          <td>';
        if ($medi['cronico'] == 1)
            $html.= "Critico";
        else
            $html.= "Temporal";
        $html.='</td>
          <td>' . $medi['dias'] . '&nbsp;</td>
          <td>' . $medi['cajas'] . '&nbsp;</td>
          <td>' . nl2br($medi['indicaciones']) . '</td>
        </tr>';
    }

    $html.='</table>';
}
$html.='</td>
                </tr>
                <tr>
                    <td class="titulos">RECOMENDACIONES:</td>
                </tr>
                <tr>
                    <td class="datos">' . ponerAcentos($dContrarreferencia['recomendaciones']) . '<br>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" height="30">_______________________________________________________</td>
                </tr>
                <tr>
                    <td align="center"><strong>' . $dMedico['titulo'] . ' ' . $dMedico['ap_p'] . ' ' . $dMedico['ap_m'] . ' ' . $dMedico['nombres'] . '<br>
                            DGP: ' . $dMedico['ced_pro'] . '<br>' . $dServicio['nombre'] . '<br>
                        </strong></td>
                </tr></tbody></table>
    <div class="nota">            
                    <p align="justify">Para Revisar las
                        recetas proporcionadas al paciente<br>
                        Dirijase a la siguiente Direccion http://192.165.95.30/farmaciag/,
                        ingrese con el usuario visorvisor, contraseña 123456<br>
       <p><strong>INDICACIONES:</strong><br>
<ol>
  <li>ESTE FORMATO DEBERA IMPRIMIRSE UNICAMENTE EN <strong>UN TANTO </strong>PARA SU RESGUARDO EN EL EXPEDIENTE <strong>CON SELLO Y FIRMA DEL MEDICO TRATANTE </strong> ,LAS UNIDADES MEDICAS PODRAN CONSULTAR Y/O IMPRIMIR ESTE DOCUMENTO DESDE EL SISTEMA DE REFERENCIA ELECTRONICA <strong>http://192.165.95.30/clinicas </strong> PARA REALIZAR EL CONTROL Y SEGUIMIENTO DE LOS PACIENTES DESPUÉS DE    48 HRS DE EMITIDO </li>
</body>
</html>';
$html = iconv("ISO-8859-1", "UTF-8", $html);
$decod = utf8_decode(htmlentities($html));

if (!$decod) {
    echo $html;
} else {
    $mpdf = new mpdf();
    $stylesheet = file_get_contents('lib/impresion2.css');
    $mpdf->WriteHTML($stylesheet, 1);
    $mpdf->WriteHTML($html, 2);
    $mpdf->debug = true;
    $mpdf->allow_output_buffering = true;
    $mpdf->Output();
}
?>