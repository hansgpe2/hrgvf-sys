<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="sp"><head>
        <title>Modificar DerechoHabiente</title>


        <?php
        error_reporting(E_ALL ^ E_NOTICE);

        include('lib/funciones.php');

        $tpoCedHom = array("10", "40", "41", "50", "51", "70", "71", "90");

        $tpoDerHom = array("Trabajador", "Esposo", "Concubino", "Padre", "Abuelo", "Hijo", "Hijo de conyuge", "Pensionado");

        $tpoCedMuj = array("20", "30", "31", "32", "60", "61", "80", "81", "91");

        $tpoDerMuj = array("Trabajadora", "Esposa", "Concubina", "Mujer", "Madre", "Abuela", "Hija", "Hija de conyuge", "Pensionada");

        $tpoCedGen = Array("92", "99");

        $tpoDerGen = Array("Familiar de pensionado", "No derechohabiente");

        $tpoCed = array_merge($tpoCedHom, $tpoCedMuj, $tpoCedGen);

        $tpoDer = array_merge($tpoDerHom, $tpoDerMuj, $tpoDerGen);

        $idderecho = $_REQUEST['idDerecho'];

        $derecho = DatosDerechoHabiente($idderecho);

        $direccion = explode(",", $derecho['direccion']);

        $cp = $derecho['Codigo_Postal'];
        ?>

        <style type="text/css">
            .etiquetas {
                font-size: xx-small;
                color: #F00;
            }
        </style>
    </head><body>
        <form name="agregarDH" id="agregarDH" submit="ModificarDH.php">
            <table class="ventana" border="0" cellpadding="0" cellspacing="0" width="945">
                <tbody>
                    <tr>
                        <td colspan="5" class="tituloVentana">MODIFICAR DERECHOHABIENTE</td>
                    </tr>
                    <tr>
                        <td width="187">&nbsp;</td>
                        <td width="177">&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="25" align="right"><label for="cedulaAgregar" class="error"><span class="textosParaInputs">CEDULA:</span></label> </td>
                        <td align="left"><input name="cedulaAgregar" type="text" id="cedulaAgregar" tabindex="1" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo $derecho['cedula']; ?>" maxlength="10"/>

                        </td>
                    </tr>
                    <tr><td class="textosParaInputs" align="right">Sexo</td><td><select name="sexo" id="sexo" tabindex="2" onchange="ObtenerTpoDerXSexo(this.id, 'cedulaTipoAgregar');">
                                <option value="-1"<?php if ($derecho['sexo'] == -1) echo " selected"; ?>></option>
                                <option value="M"<?php if ($derecho['sexo'] == 0) echo " selected"; ?>>Mujer</option>
                                <option value="H"<?php if ($derecho['sexo'] == 1) echo " selected"; ?>>Hombre</option>
                            </select></td><td width="236" class="textosParaInputs" align="right">Tipo de DerechoHabiente</td>
                        <td width="345"><select name="cedulaTipoAgregar" id="cedulaTipoAgregar" tabindex="3" onfocus="ObtenerTpoDerXSexo('sexo', this.id);">
                                <?php
                                if ($derecho['sexo'] == 0) {

                                    $tpoCed = $tpoCedMuj;

                                    $tpoDer = $tpoDerMuj;
                                } else {

                                    $tpoCed = $tpoCedHom;

                                    $tpoDer = $tpoDerHom;
                                }

                                echo '<option value="-1"></option>';

                                for ($i = 0; $i < count($tpoCed); $i++) {

                                    if ($derecho['cedula_tipo'] == $tpoCed[$i]) {

                                        echo '<option value="' . $tpoCed[$i] . '" selected>' . $tpoCed[$i] . " " . $tpoDer[$i] . "</option>";

                                        $band = 1;
                                    }
                                    else
                                        echo '<option value="' . $tpoCed[$i] . '">' . $tpoCed[$i] . " " . $tpoDer[$i] . "</option>";
                                }

                                for ($i = 0; $i < count($tpoCedGen); $i++) {

                                    if (($derecho['cedula_tipo'] == $tpoCedGen[$i]) && $band == 0)
                                        echo '<option value="' . $tpoCedGen[$i] . '" selected>' . $tpoCedGen[$i] . " " . $tpoDerGen[$i] . "</option>";
                                    else
                                        echo '<option value="' . $tpoCedGen[$i] . '">' . $tpoCedGen[$i] . " " . $tpoDerGen[$i] . "</option>";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td height="25" class="textosParaInputs" align="center" colspan="2">NOMBRE DEL DERECHOHABIENTE:</td></tr>
                    <tr><td height="26" align="right" class="textosParaInputs">Apellido Paterno
                        </td><td align="left" colspan="3"><input name="ap_pAgregar" type="text" id="ap_pAgregar" tabindex="4" onblur="this.value = this.value.toUpperCase();" size="20" maxlength="20" value="<?php echo ponerAcentos($derecho['ap_p']); ?>" />
                            <span class="textosParaInputs">Apellido Materno</span>
                            <input name="ap_mAgregar" type="text" id="ap_mAgregar" tabindex="5" onblur="this.value = this.value.toUpperCase();" size="20" maxlength="20" value="<?php echo ponerAcentos($derecho['ap_m']); ?>"/><label for="ap_mAgregar" class="error" /></label></td></tr>
                    <tr><td class="textosParaInputs" align="right">Nombre(s)</td><td colspan="3" align="left">
                            <input name="nombreAgregar" type="text" id="nombreAgregar" tabindex="6" onblur="this.value = this.value.toUpperCase();" size="50" maxlength="50" value="<?php echo ponerAcentos($derecho['nombres']); ?>" /><label for="nombreAgregar" class="error" value="<?php echo $derecho['nombres']; ?>" /></label></td>
                    </tr>
                    <tr>
                        <td height="25" align="right"><label for="telefonoAgregar" class="error"><span class="textosParaInputs">TELEFONO: (01)</span></label></td>
                        <td align="left" colspan="3"><input name="telefonoAgregar" type="text" id="telefonoAgregar" tabindex="6" size="11" maxlength="10" value="<?php echo $derecho['telefono']; ?>" />
                            <span class="etiquetas">
                                formato (lada)numero ej 3312345678
                            </span>
                            <label for="fecha_nacAgregar" class="error"><span class="textosParaInputs"> FECHA NAC. </span></label>
                            <input type="text" id="fecha_nacAgregar" name="fecha_nacAgregar" value="<?php echo date('d/m/Y', strtotime($derecho['fecha_nacimiento'])); ?>" ><input type="hidden" name="fechaNacimiento" id="fechaNacimiento" value="<?php echo $derecho['fecha_nacimiento']; ?>"</td>
                    </tr>
                    <tr>
                        <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
                        <td colspan="3" align="left"><select name="estadoAgregar" id="estadoAgregar" tabindex="10" ></select>     

                            <span class="textosParaInputs">
                                <input name="estadoClinica" type="hidden" id="estadoClinica" value="<?php echo $derecho['estado']; ?>" />
                                MUNICIPIO: </span>
                            <select name="municipioAgregar" id="municipioAgregar" tabindex="11" >      </select>
                            <input name="municipioClinica" type="hidden" id="municipioClinica" value="<?php echo $derecho['municipio']; ?>" /></td>
                    </tr>
                    <tr>
                        <td height="25" class="textosParaInputs" align="right"><label for="direccionAgregar" class="error"><span class="textosParaInputs">DIRECCION:</span></label></td>
                        <td colspan="3" align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" tabindex="12" onkeyup="this.value = this.value.toUpperCase();" size="64" maxlength="50" value="<?php echo $direccion[0]; ?>" />
                            <span class="etiquetas">No usar simbolos :#,*,$,',&lt;,&gt;,|, solo números y letras</span></td>
                    </tr>
                    <tr>
                        <td height="25" class="textosParaInputs" align="right"><label for="colonia" class="textosParaInputs">COLONIA</label></td>
                        <td colspan="3" align="left"><select name="colonia" id="colonia" tabindex="13" >
                            </select>
                            &nbsp;&nbsp;<span class="textosParaInputs">CODIGO POSTAL</span>
                            <select name="codigoPostal" id="codigoPostal" tabindex="14" >
                            </select></td>
                    </tr>
                    <tr><td class="textosParaInputs">Unidad Medica de Adscripcion</td>

                        <td colspan="3">

                            <select  name="Unidad" id="Unidad">
                                <?php echo UnidadMedicaProcedencia($derecho['estado'], $derecho['unidad_medica']) ?>
                            </select>

                        </td></tr>
                    <tr>
                        <td align="right" class="textosParaInputs">CORREO ELECTR&Oacute;NICO</td>
                        <td colspan="3"><input name="email" type="text" id="email" tabindex="15" value="<?php echo $derecho['email']; ?>" />
                            &nbsp;<span class="textosParaInputs">CELULAR</span>&nbsp;
                            <input name="cel" type="text" id="cel" tabindex="16" size="10" maxlength="10" value="<?php echo $derecho['celular']; ?>" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <div id="divBotones_EstadoAgregarDH"> <button id="enviar" type="button" class="botones">Ocultar Datos</button>&nbsp;&nbsp;<div id="cambiar"><input value="Modificar" class="botones" type="button"></div><div id="guardar" style="display: none;"><button type="button" class="button" id="btnGuardar" disabled>Guardar</button></div></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"> <br>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input type="hidden" name="idDerecho" id="idDerecho" value="<?php echo $derecho['id_derecho']; ?>" />
        </form>

        <?php
        $fechaMin = strtotime("now");

        $yfm = date("Y", $fechaMin) - 50;

        $mfm = date("m", $fechaMin) - 1;

        $dfm = date("d", $fechaMin);

        $dMax = date("d");

        $mMax = date("m") - 1;

        $yMax = date("Y");
        ?>
        <script type="text/javascript">
                            $(document).ready(function() {
                                DesHabilitarModDH();

                                $("#cambiar").click(function(event) {

                                    HabilitarModDH();
                                    mostrarDiv('guardar');
                                    ocultarDiv('cambiar');
                                    $('#btnGuardar').removeAttr('disabled');
                                });
                                $("#enviar").click(function() {
                                    DesHabilitarModDH();
                                    ocultarDiv('DatosDH');
                                });

                                $("#estadoAgregar").ready(function() {
                                    $.ajax({
                                        url: "cargarEstados.php",
                                        data:
                                                {
                                                    estado: '<?php echo $derecho['estado']; ?>'
                                                },
                                        success: function(data)
                                        {
                                            $("#estadoAgregar").append(data);
                                        }
                                    });
                                });
                                $("#estadoAgregar").change(function() {
                                    vaciarSelect('municipioAgregar');
                                    $.ajax({
                                        url: "cargarMunicipios.php",
                                        data: {
                                            estado: $("#estadoAgregar option:selected").val(),
                                            municipio: ""
                                        },
                                        success: function(data) {
                                            $("#municipioAgregar").append(data);
                                        }
                                    });
                                    $.ajax({
                                        url: "ObtenerUMFConsulta.php",
                                        data:
                                                {
                                                    estado: $("#estadoAgregar option:selected").val(),
                                                    unidad: "-1"
                                                },
                                        success: function(data) {
                                            vaciarSelect('Unidad');
                                            $("#Unidad").append(data);
                                        }
                                    })
                                });
                                $("#municipioAgregar").ready(function() {
                                    vaciarSelect('municipioAgregar');
                                    $.ajax({
                                        url: "cargarMunicipios.php",
                                        data: {
                                            estado: '<?php echo $derecho['estado']; ?>',
                                            municipio: "<?php echo $derecho['municipio']; ?>"
                                        },
                                        success: function(data) {
                                            $("#municipioAgregar").append(data);
                                        }
                                    });
                                    vaciarSelect('colonia');
                                    vaciarSelect('codigoPostal');
                                    $.ajax({
                                        url: "mostrarColonias.php",
                                        data: {
                                            estado: '<?php echo $derecho['estado']; ?>',
                                            municipio: '<?php echo $derecho['municipio']; ?>',
                                            colonia: '<?php echo $direccion[1]; ?>'
                                        },
                                        success: function(data) {
                                            $("#colonia").append(data);
                                        }
                                    });
                                    $.ajax({
                                        url: "codigos_postales.php",
                                        data: {
                                            estado: '<?php echo $derecho['estado']; ?>',
                                            municipio: '<?php echo $derecho['municipio']; ?>',
                                            cp: '<?php echo $derecho['CodigoPostal']; ?>',
                                            col: '<?php echo $direccion[1]; ?>'
                                        },
                                        success: function(data) {
                                            $("#codigoPostal").append(data);
                                        }
                                    });
                                });
                                $("#municipioAgregar").change(function() {
                                    vaciarSelect('colonia');
                                    vaciarSelect('codigoPostal');
                                    $.ajax({
                                        url: "mostrarColonias.php",
                                        data: {
                                            estado: $("#estadoAgregar option:selected").val(),
                                            municipio: $("#municipioAgregar option:selected").val()
                                        },
                                        success: function(data) {
                                            $("#colonia").append(data);
                                        }
                                    });
                                    $.ajax({
                                        url: "codigos_postales.php",
                                        data: {
                                            estado: $("#estadoAgregar").val(),
                                            municipio: $("#municipioAgregar option:selected").val(),
                                            cp: ""
                                        },
                                        success: function(data) {
                                            $("#codigoPostal").append(data);
                                        }
                                    });
                                });
                                $("#codigoPostal").change(function()
                                {
                                    vaciarSelect('colonia');
                                    $.ajax({
                                        url: "mostrarColonias.php",
                                        data: {
                                            cp: $("#codigoPostal").val(),
                                            municipio: $("#municipioAgregar").val()
                                        },
                                        success: function(data) {
                                            $("#colonia").append(data);
                                        }
                                    });
                                });
                                $("#colonia").change(function() {
                                    vaciarSelect('codigoPostal');
                                    $.ajax({
                                        url: "codigos_postales.php",
                                        data: {
                                            estado: $("#estadoAgregar option:selected").val(),
                                            municipio: $("#municipioAgregar option:selected").val(),
                                            col: $("#colonia option:selected").val()
                                        },
                                        success: function(data) {
                                            $("#codigoPostal").append(data);
                                        }

                                    });
                                });
                                $("#fecha_nacAgregar").datepicker({
                                    changeMonth: true,
                                    changeYear: true,
                                    minDate: new Date(<?php echo $yfm; ?>,<?php echo $mfm; ?>,<?php echo $dfm; ?>),
                                    maxDate: new Date(<?php echo $yMax; ?>,<?php echo $mMax; ?>,<?php echo $dMax; ?>),
                                    dateFormat: "dd/mm/yy",
                                    altFormat: "yymmdd",
                                    altField: "#fechaNacimiento",
                                    yearRange: "<?php echo $yfm; ?>:<?php echo $yMax; ?>"
                                });
//                                $("#btnGuardar").click(function(evt) {
////                                $("#agregarDH").submit(function(event) {
////                                    
////                                    var validacion = validarAgregarDH();
//                                      evt.preventDefault();
//                                      validarModificarDH();
//                                      $("#btnGuardar").attr('disabled','true');
//                                    
//                                });
//                                
//                                $("#agregarDH").submit(function(event) {
//                                    event.preventDefault();
//                                    console.log('Eviando Formulario');
//                                    $("#btnGuardar").val("Guardando...");
//                                    $("#btnGuardar").attr('disabled',true);
//                                    return true;
//                                });
                                $('#btnGuardar').one('click', function() {
                                    validarModificarDH();
                                });
                            });
        </script>
    </body></html>