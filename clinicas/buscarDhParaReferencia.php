<?php
session_start(); 
include('lib/funciones.php');
$mes=date("n");
$dia=date("d");
$meses=array("ene","feb","marzo","abril","mayo","junio","julio","ago","sept","oct","nov","dic");
$clinica=obtenerDatosClinica($_SESSION['idClinica']);
if(isset($_REQUEST['idDerecho']))
{
$derecho=DatosDerechoHabiente($_REQUEST['idDerecho']);
$cedula=$derecho['cedula'];	
}
else
$cedula="";
?>
<script src="lib/js/jquery.ui.core.js"></script>
        <script src="lib/js/jquery.ui.datepicker.js"></script>
        <link href="lib/css/jquery.ui.datepicker.css">
		<script src="lib/funciones.js"></script>
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><div id="buscar" style="height:150px; margin-top:10px;">
      <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
        <tr>
          <td colspan="2" class="tituloVentana">BUSCAR DERECHOHABIENTE PARA APARTAR CITA</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
          <td align="left"><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();ocultarDiv('incapacidad');">
            <option value="cedula" selected="selected">C&eacute;dula</option>
            <option value="nombre">Nombre</option>
          </select></td>
        </tr>
        <tr>
          <td colspan="2"><div id="buscarPorCedula">
            <form id="selDH" method="post" action="javascript: buscarDHbusqueda(document.getElementById('cedulaBuscar').value);">
              <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                  <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();" value="<?php echo $cedula; ?>"/>
                    <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." />
                    <input name="button" type="button" id="button" onclick="mostrarDiv('DatosDH');" value="Mostrar Datos" />
                    <div class="botones" id="agreDH">
                      &nbsp;
                      <input type="button" name="Agregar DerechoHabiente" id="Agregar DerechoHabiente" class="botones" value="Agregar DerechoHabiente" onclick="agregarDH(document.getElementById('cedulaBuscar').value)"/>
                  </div></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div></td>
                </tr>
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
            </form>
          </div>
            <div id="buscarPorNombre" style="display:none;">
              <form id="selDHN" method="post" action="javascript: buscarDHNbusqueda(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                    <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                    <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                    <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                  </tr>
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                    <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                      <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                  </tr>
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                    <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div></td>
                  </tr>
                  <tr>
                    <td colspan="4" align="center"></td>
                  </tr>
                </table>
              </form>
            </div></td>
        </tr>
        <tr><td colspan="2">
                <div id="DatosDH"></div>
            </td></tr>
            <tr><td class="tituloVentana" align="center" colspan="2">DATOS DE LA CITA</td></tr>
        <tr><td colspan="2"><div id="incapacidad" style="display:none">
                <span class="textosParaInputs">CUENTA CON INCAPACIDAD</span>&nbsp;&nbsp;<select id="inca" onchange="discapacidad()">
                <option value="0">no</option>
                <option value="1">si</option>
                </select>&nbsp;&nbsp;<div id="fechaInc" style="display:none;"><span class="textosParaInputs">FECHA DE TERMINO</span>&nbsp;&nbsp;<span style="position:relative">
                <label for="mes1">mes</label>
                <select name="mes1" id="mes1" onchange="calendarios('1');">
                  <?php
                    for($i=1;$i<=12;$i++)
                    {
                        if($i==$mes)
                        {
                            if($i<10)
                                echo "<option value='0".$i."' selected>".$meses[$i-1]."</option>";
                            else
                                echo "<option value='".$i."' selected>".$meses[$i-1]."</option>";
                        }
                        else {
                            if($i<10)
                                echo "<option value=0".$i.">".$meses[$i-1]."</option>";
                            else
                                echo "<option value=".$i.">".$meses[$i-1]."</option>";
                        }
                    }
                  ?>
                </select>
                <label for="dia1">dia</label>
                <select name="dia1" id="dia1" onfocus="calenDia('1','<?php echo $dia; ?>');">
                  <option value="01" <?php if (!(strcmp(01, $dia))) {echo "selected=\"selected\"";} ?>>1</option>
                  <option value="02" <?php if (!(strcmp(02, $dia))) {echo "selected=\"selected\"";} ?>>2</option>
                  <option value="03" <?php if (!(strcmp(03, $dia))) {echo "selected=\"selected\"";} ?>>3</option>
                  <option value="04" <?php if (!(strcmp(04, $dia))) {echo "selected=\"selected\"";} ?>>4</option>
                  <option value="05" <?php if (!(strcmp(05, $dia))) {echo "selected=\"selected\"";} ?>>5</option>
                  <option value="06" <?php if (!(strcmp(06, $dia))) {echo "selected=\"selected\"";} ?>>6</option>
                  <option value="07" <?php if (!(strcmp(07, $dia))) {echo "selected=\"selected\"";} ?>>7</option>
                  <option value="08" <?php if (!(strcmp(08, $dia))) {echo "selected=\"selected\"";} ?>>8</option>
                  <option value="09" <?php if (!(strcmp(09, $dia))) {echo "selected=\"selected\"";} ?>>9</option>
                  <option value="10" <?php if (!(strcmp(10, $dia))) {echo "selected=\"selected\"";} ?>>10</option>
                  <option value="11" <?php if (!(strcmp(11, $dia))) {echo "selected=\"selected\"";} ?>>11</option>
                  <option value="12" <?php if (!(strcmp(12, $dia))) {echo "selected=\"selected\"";} ?>>12</option>
                  <option value="13" <?php if (!(strcmp(13, $dia))) {echo "selected=\"selected\"";} ?>>13</option>
                  <option value="14" <?php if (!(strcmp(14, $dia))) {echo "selected=\"selected\"";} ?>>14</option>
                  <option value="15" <?php if (!(strcmp(15, $dia))) {echo "selected=\"selected\"";} ?>>15</option>
                  <option value="16" <?php if (!(strcmp(16, $dia))) {echo "selected=\"selected\"";} ?>>16</option>
                  <option value="17" <?php if (!(strcmp(17, $dia))) {echo "selected=\"selected\"";} ?>>17</option>
                  <option value="18" <?php if (!(strcmp(18, $dia))) {echo "selected=\"selected\"";} ?>>18</option>
                  <option value="19" <?php if (!(strcmp(19, $dia))) {echo "selected=\"selected\"";} ?>>19</option>
                  <option value="20" <?php if (!(strcmp(20, $dia))) {echo "selected=\"selected\"";} ?>>20</option>
                  <option value="21" <?php if (!(strcmp(21, $dia))) {echo "selected=\"selected\"";} ?>>21</option>
                  <option value="22" <?php if (!(strcmp(22, $dia))) {echo "selected=\"selected\"";} ?>>22</option>
                  <option value="23" <?php if (!(strcmp(23, $dia))) {echo "selected=\"selected\"";} ?>>23</option>
                  <option value="24" <?php if (!(strcmp(24, $dia))) {echo "selected=\"selected\"";} ?>>24</option>
                  <option value="25" <?php if (!(strcmp(25, $dia))) {echo "selected=\"selected\"";} ?>>25</option>
                  <option value="26" <?php if (!(strcmp(26, $dia))) {echo "selected=\"selected\"";} ?>>26</option>
                  <option value="27" <?php if (!(strcmp(27, $dia))) {echo "selected=\"selected\"";} ?>>27</option>
                  <option value="28" <?php if (!(strcmp(28, $dia))) {echo "selected=\"selected\"";} ?>>28</option>
                  <option value="29" <?php if (!(strcmp(29, $dia))) {echo "selected=\"selected\"";} ?>>29</option>
                  <option value="30" <?php if (!(strcmp(30, $dia))) {echo "selected=\"selected\"";} ?>>30</option>
                  <option value="31" <?php if (!(strcmp(31, $dia))) {echo "selected=\"selected\"";} ?>>31</option>
                </select>
                <label for="anio1">año</label>
                <input name="anio1" type="text" id="anio1" value="<?php echo date("Y"); ?>" size="4" maxlength="4" />
                </span></div></div><tr><td class="textosParaInputs" align="right">SERVICIO</td><td>
                    <?php
                    include_once 'lib/funciones.php';
$sql = "select * from servicios_x_clinica as sc join servicios as s on(s.id_servicio=sc.id_servicio) where sc.id_clinica=".$_SESSION['idClinica']." order by s.nombre ASC";
$servicios = ObtenerDHS($sql);
$select = "<select id='servicio'><option value='-1'>&nbsp;</option>";
foreach ($servicios as $key => $value) {
    $select.="<option value='" . $value['id_servicio'] . "'>" . $value['nombre'] . "</option>";
}
$select.="</select>";
echo $select;
?></td></tr>
                <tr>
                  <td colspan="2" align="center"><input name="seleccionarDH2" type="button" class="botones" id="seleccionarDH2" onclick="verCitasDH();ocultarDiv('DatosDH');" value="Ver Historial de Citas en HRVGF" />
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="checarClinicaDH();ocultarDiv('DatosDH');" value="Ver Citas Disponibles" />
                    <br />
                    <br /></td>
                </tr>
      </table>
    </div></td>
  </tr>
</table>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
<div id="citas" style="display:none;">
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td></td>
    </tr>
  </table>
</div>