<?php

error_reporting(E_ERROR);
session_start();
include_once 'lib/funciones.php';
$clinica = $_SESSION['idClinica'];
$referencias = obtenerReferenciasCont($clinica);
$output['aaData'] = array();
while ($i < count($referencias)) {
$row = array();
if ($referencias[$i]['status_citas'] == 1)
$cont = "si";
else
$cont = "no";
$row[] = $referencias[$i]['id_contra'];
$row[] = $referencias[$i]['cedula'] . "/" . $referencias[$i]['cedula_tipo'];
$row[] = ponerAcentosDT(htmlentities($referencias[$i]['ap_p'] . " " . $referencias[$i]['ap_m'] . " " . $referencias[$i]['nombres']));
$row[] = $referencias[$i]['servicio'];
$row[] = date("d/m/Y", strtotime($referencias[$i]['fecha_envio_clinica']));
$row[] = $cont;
$row[] = "<a href='verFormatoContrarreferencia.php?idContra=" . $referencias[$i]['id_contra'] . "' target='_blank' class='button'>imprimir</a>";
$ren = $row;
//if (!json_encode($ren, JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_QUOT)) {
//        $row = array();
//        $row[] = $referencias[$i]['id_contra'];
//        $row[] = $referencias[$i]['cedula'] . "/" . $referencias[$i]['cedula_tipo'];
//        $row[] = ponerAcentosDT(quitarAcentos($referencias[$i]['ap_p'] . " " . $referencias[$i]['ap_m'] . " " . $referencias[$i]['nombres']));
//        $row[] = $referencias[$i]['servicio'];
//        $row[] = date("d/m/Y", strtotime($referencias[$i]['fecha_envio_clinica']));
//        $row[] = $cont;
//        $row[] = "<button onclick='VerReferencia(" . $referencias[$i]['id_contra'] . ")'>imprimir</button>";
//        $ren = $row;
//        if (json_encode($ren, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_QUOT))
//            $band = 1;
//        else {
//            $band = 0;
//            $eput['aaData'][] = json_last_error_msg() . $i;
//        }
//    } else
//        $band = 1;
//    if ($band == 1)
        $output['aaData'][] = $row;

    $i++;
}
$jout = json_encode($output,  JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_QUOT);
if (!$jout)
    echo json_encode($eput);
else
    echo $jout;
?>
