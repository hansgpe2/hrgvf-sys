<?php
error_reporting(E_ERROR);
session_start();
include './lib/funciones.php';
$fechaIni = date("Ymd", strtotime($_REQUEST['fecha1']));
$fechaFin = date("Ymd", strtotime($_REQUEST['fecha2']));
$medCrit=  ObtenerTratamientosCriticos($_SESSION['idClinica'],$fechaIni,$fechaFin);
echo "<table class='Ventana'><tr class='tituloVentana'>
    <th>Clave</th>
    <th>Medicamento</th>
    <th>Total</th>
    <th>Desglose por DerechoHabiente</th></tr>";
$cont=  count($medCrit);
for($i=0;$i<$cont;$i++) {
    $medicina=  getMedicamentoXId($medCrit[$i]);
    $pacientes=  ObtenerPacientesXMed($medCrit[$i],$_SESSION['idClinica'],$fechaIni,$fechaFin);
    $totalMed=0;
    $printPacientes="<table class='Ventana'><tr>
        <th>Cedula</th><th>Nombre</th><th>Edad</th><th>Sexo</th><th>Unidad Medica</th><th>Cantidad <br>(Cajas/Unidades)</th>";
    foreach ($pacientes as $key1=>$paciente)
    {
        $totalMed+=$paciente['cajas'];
        $umf=  obtenerDatosClinica($paciente['unidad']);
        $fechaNacimiento=  strtotime($paciente['fecha_N']);
        $edad=  intval((time()-$fechaNacimiento)/(365*24*60*60));
        $printPacientes.="<tr>
            <td>".$paciente['cedula']."</td>
                <td>".ponerAcentos($paciente['nombreDH'])."</td>
                    <td>".$edad."</td><td>";
        if($paciente['sexo']==1)
            $printPacientes.="Masculino";
        else
            $printPacientes.="Femenino";
                        
           $printPacientes.="</td><td>".$umf['nombre']."</td>
                <td>".$paciente['cajas']."</td></tr>";
    }
    $printPacientes.="</table>";
    echo "<tr><td>".$medicina['id_medicamento']."</td>
        <td>".$medicina['descripcion']."</td>
            <td>".$totalMed."</td>
            <td>".$printPacientes."</td>
                </tr>";
           
}
echo "</table>";
?>
