/*Funciones administrativas de clinica
*Version 0.1
*ISSSTE HRVGF
*Derechos Reservados (R)*/

function NuevoServicio()
{
    $("#contenido").load("NuevoServicio.php");
}

function AgregarServicio()
{
$.ajax({
    url:"AgregarServicio.php",
    data:"idServicio="+$("#Servicio").val(),
    type:"GET"
}).done(function (Respuesta)
{
    if(Respuesta=="ok")
        alert("Servicio agregado a lista exitosamente");
    else
        alert(Respuesta);
});
}

function EliminarServicios()
{
     $("#contenido").load("eliminarServicios.php");
}

function eliminarServicios()
{
    var opciones=[]
        $("input[name='servicios']:checked").each(function(){
        opciones.push($(this).val());
    });
    $.ajax({
        url:"eliminarServiciosConfirmar.php",
        data:"servicios="+opciones,
        dataType:"json",
        type:"get",
        success:function(data)
        {
            if(data=="ok")
                {
                    alert("Servicios Eliminados");
                }
        }
    });
}

function AgregarUsuario()
{
    var usuario=$("#usuario").val();
    var pass=$("#password").val();
    var nombre=$("#nombre").val();
    var tipoUsuario=$("#tipoUsuario").val();
    if(usuario==""|| pass=="" || nombre=="" || tipoUsuario==-1)
        {
            alert("Favor de Llenar los Campos faltantes");
        }
        else
            {
                patron=/^[x]+$/;
                patronContra=/^[a-z\d]{6,15}$/i
                if(usuario.match(patron)||pass.match(patron)||nombre.match(patron))
                    {
                        alert("Datos ingresados Invalidos");
                    }
                   else
                       {
                           patronLogin=/^[a-z\d_]{6,15}$/i
                           if(!usuario.match(patronLogin))
                               alert("Nombre de Usuario invalido \n puede llevar letras y numeros \n minimo 6 caracteres");
                           else
                               if(!pass.match(patronContra))
                                   alert("Contrase\u00f1a invalida \n minimo 6 caracteres numeros y letras");
                           else
                               {
                                   $.ajax({
                                       url:"nuevoUsuarioConfirmar.php",
                                       data:"login="+usuario+"&password="+pass+"&nombre="+nombre+"&tipo="+tipoUsuario,
                                       type:"get",
                                       success:function(resp){
                                           alert(resp);
                                       }
                                   });
                               }
                       }
            }
}


function NuevoUsuario()
{
    $("#contenido").load("NuevoUsuario.php");
}

function BuscarUsuariosXTipo()
{
	var tipo=$("#servicio").val();
	$("#serviciosAsignados").load("ConsultaUsuarios.php?Servicio="+tipo);
}

function BuscarUsuarioXUnidad()
{
	var unidad=$("#Unidad").val();
	$("#serviciosAsignados").load("ConsultaUsuarios.php?unidad="+unidad);
}

function VerTodosUsuarios()
{
	$("#serviciosAsignados").load("ConsultaUsuarios.php");
}

function ConsultaUsuarios()
{
	$("#contenido").load("VerUsuarios.php");
}

function mostrarDiv(div)
{
    var div=document.getElementById(div);
    div.style.display="block";
}

function ocultarDiv(div)
{
    var div=document.getElementById(div).style;
    div.display="none";
}

function ModificarUsuario(num,clinica,tipoUsuario)
{
	mostrarDiv("usuario"+num);
	$("#tipoUsuario"+num+"option[value="+tipoUsuario+"]").attr("selected",true);	
}

function ModificarUsuarioConfirmar(ind,clin)
{
    var login=$("#login"+ind).val();
    var pas=$("#password"+ind).val();
    var usuario=$("#nombre"+ind).val();
	var idUser=$("#idUsuario"+ind).val();
    var clinica=clin;
    patronLogin=/^[A-Za-z][A-Za-z0-9_]{4,10}[A-Za-z0-9]$/i;
    patronContra=/^[A-Z0-9_]{6,12}/i;
    patronInvalido=/^[xX]+/;
    if(login=="" || pas=="" || usuario=="" || clinica=="-1")
        alert("Datos Faltantes");
    else
    {
        if(login.match(patronInvalido) || !login.match(patronLogin)){
            alert("Nombre de Usuario Invalido,\n debe ser de 6 a 12 caracteres a-z 0-9 _ \n iniciando con letra");
            return ;
        }
        if(pas.match(patronInvalido)|| !pas.match(patronContra))
        {
            alert("Contrase\u00f1a invalida \n debe ser de 6 a 12 caracteres a-z 0-9 _");
            return;
        }
        if(usuario.match(patronInvalido))
        {
            alert("Nombre del Usuario Invalido");
            return;
        }
        var tipoUsuario=$("#tipoUsuario"+ind).val();
        $.ajax({
            url:"ModificarUsuario.php",
            type:"GET",
            data:"login="+login+"&password="+pas+"&nombre="+usuario+"&tipo="+tipoUsuario+"&clinica="+clinica+"&idUser="+idUser
        }).done(function (resp){
            if(resp=="ok")
            {
                alert("Usuario Modificado");
                ocultarDiv("usuario"+ind);
            }
            else
            {
                alert(resp);
            }
        });					
    }
}

function eliminarUsuario(ind)
{
    $.ajax({
        url:"eliminarUsuario.php",
        type:"GET",
        data:"idUsuario="+$("#idUsuario"+ind).val()
    }).done(function (resp){
        if(resp=="ok")
        {
            alert("Usuario Eliminado");
			$("#contenido").load("VerUsuarios.php");
        }
        else
            alert(resp);
    });
}