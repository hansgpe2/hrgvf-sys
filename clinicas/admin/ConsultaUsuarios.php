<?php
session_start();
include_once 'lib/funcionesAdmin.php';
if (isset($_REQUEST['Servicio']))
    $sql = "select * from usuarios_contrarreferencias where tipo_usuario=" . $_REQUEST['Servicio'] . " and id_clinica=".$_SESSION['idClinica'];
else
    $sql = "select * from usuarios_contrarreferencias where id_clinica=".$_SESSION['idClinica'];
$usuarios = ConsultaMultiple($sql);
?>
<table class="Ventana" border="0">
    <tr>
        <th class="tituloVentana">Usuario</th>
        <th class="tituloVentana">Nombre</th>
        <th class="tituloVentana">Modificar</th>
    </tr>
    <?php
    $i = 0;
    foreach ($usuarios as $key => $usuario) {
        
        ?>
        <tr>
            <td><?php echo $usuario['login']; ?></td>
            <td><?php echo $usuario['nombre']; ?></td>
            <td><input type="button" value="Modificar/Eliminar Usuario" onclick="ModificarUsuario(<?php echo $i ?>,<?php echo $usuario['id_clinica']; ?>,<?php echo $usuario['tipo_usuario'] ?>);"></td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="usuario<?php echo $i; ?>" style="display:none">
                    <form id="datosUsuario<?php echo $i;?>" name="Usuario<?php echo $i;?>" action="javascript:ModificarUsuarioConfirmar(<?php echo $i; ?>,<?php echo $_SESSION['idClinica']; ?>);">
                    <table width="100%" border="0">
                        <tr>
                            <td class="textosParaInputs" align="right">Usuario</td>
                            <td><label for="usuario<?php echo $i; ?>">
                                    <input name="login<?php echo $i; ?>" type="text" id="login<?php echo $i; ?>" onblur="this.value=this.value.toUpperCase();" maxlength="12" value="<?php echo $usuario['login']; ?>" />
                          </label></td>
                        </tr>
                        <tr>
                            <td class="textosParaInputs" align="right">Contrase&ntilde;a</td>
                            <td><input type="password" name="password<?php echo $i; ?>" id="password<?php echo $i; ?>" value="<?php echo $usuario['pass']; ?>" /></td>
                        </tr>
                        <tr>
                            <td class="textosParaInputs" align="right">Nombre del Usuario</td>
                            <td><input type="hidden" value="<?php echo $usuario['id_usuario'];?>" name="idUsuario<?php echo $i; ?>" id="idUsuario<?php echo $i; ?>">
                                <label for="nombre<?php echo $i; ?>"></label>
                          <input type="text" name="nombre<?php echo $i; ?>" id="nombre<?php echo $i; ?>" value="<?php echo $usuario['nombre'] ?>" /></td>
                        </tr>
                        <tr>
                            <td class="textosParaInputs" align="right">Tipo de Usuario</td>
                            <td><label for="tipoUsuario<?php echo $i; ?>"></label>
                                <select name="tipoUsuario<?php echo $i; ?>" id="tipoUsuario<?php echo $i; ?>">
                                  <option value="-1"></option>
                                    <option value="6" <?php if($usuario['tipo_usuario']=="6") echo "selected='selected'";?>>Administrador Clinica</option>
                                    <option value="2" <?php if($usuario['tipo_usuario']=="2") echo "selected='selected'";?>>Usuario Clinica</option>
                                    <option value="4" <?php if($usuario['tipo_usuario']=="4") echo "selected='selected'";?>>Visor de clinica</option>
                          </select></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right" class="textosParaInputs">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" class="botones"><input type="submit" name="boton" id="boton" value="Modificar" />
                                &nbsp;
                                <input type="button" name="boton2" id="boton2" value="Regresar" onclick="ocultarDiv('usuario<?php echo $i ?>');" />&nbsp;
                                <input name="" type="button" onclick="eliminarUsuario(<?php echo $i; ?>);" value="Eliminar Usuario">
                            </td>
                        </tr>
                    </table></form>
                </div>
            </td>
        </tr>
        <?php
        $i++;
    }
    ?>
</table>
