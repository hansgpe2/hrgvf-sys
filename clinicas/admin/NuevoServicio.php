<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Documento sin título</title>
        <link href="../lib/misEstilos.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <header class="tituloVentana">Servicio Nuevo de tercer nivel requerido</header>
        <article><form action="" method="get" name="ServicioNuevo">
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="46%" align="right" class="textosParaInputs">Servicio</td>
                        <td width="54%"><label for="Servicio"></label>
                            <select name="Servicio" id="Servicio">
                                <?php
                                session_start();
                                include('lib/funcionesAdmin.php');
                                $idClinica = $_SESSION['idClinica'];
                                $sql = "select * from servicios where id_grupo_servicio=1";
                                $servicios = ConsultaMultiple($sql);
                                foreach ($servicios as $key => $servicio) {
                                    $band=  ServicioXClinica($servicio['id_servicio'], $idClinica);
                                    if(!$band)
                                        echo "<option value='".$servicio['id_servicio']."'>".$servicio['nombre']."</option>";
                                }
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="button" name="button" id="button" value="Agregar" class="botones" onclick="AgregarServicio()"></td>

                    </tr>
                </table>

            </form></article>
    </body>
</html>