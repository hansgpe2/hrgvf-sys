<?php
error_reporting(E_ERROR);
include('../lib/funciones.php');
session_start();
$_SESSION['idUsuario']=$_REQUEST['idUsuario'];
$datosUsuario=obtenerDatosUsuario($_SESSION['idUsuario']);
$_SESSION['idClinica']=$datosUsuario['id_clinica'];
$clinica=obtenerDatosClinica($_SESSION['idClinica']);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrador de Clinica</title>
<?php if(!strstr($_SERVER['HTTP_USER_AGENT'],"MSIE")) {?>
<link rel="stylesheet" href="lib/estilosAdmin.css">
<?php } 
else {?>
<link rel="stylesheet" href="lib/estilosAdmin_ie.css">
<?php }?>
<link rel="stylesheet" href="../lib/misEstilos.css">
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
<script src="lib/jquery-1.8.3.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script src="lib/funcionesAdmin.js"></script>
</head>

<body>
<header>
  <div class="tituloVentana" align="center"><table align="center">
                                    <tr>
                                         <td rowspan="2"><img src="../diseno/LOGO-ISSSTE.jpg" width="104" height="108"></td>
                                        <td class="tituloEncabezado">Sistema de Administraci&oacute;n de Clinica</td>
                                    </tr>
                                    <tr><td class="subtituloEncabezado">INSTITUTO DE SEGURIDAD SOCIAL Y SERVICIOS PARA LOS TRABAJADORES DEL ESTADO</td></tr>
                                </table></div>
</header>
<?php if(isset($_REQUEST['idUsuario'])) { ?>
<nav align="center"><!---
<ul class="menu">
<li class="parent"><a href="">Servicios</a>
<ul>
<li><a href="">Alta de Servicios</a></li>
<li><a href="">Baja de Servicios</a></li>
</ul>
</li>
<li class="parent"><a href="">Unidades M&eacute;dicas</a>
<ul>
<li><a href="">Alta</a></li>
<li><a href="">Baja</a></li>
<li><a href="">Consulta y modificaci&oacute;n</a></li>
</ul></li>
<li><a href="">Salir</a></li>
</ul>--->
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a  class="MenuBarItemSubmenu" href="#">Usuarios</a>
      <ul>
        <li><a href="javascript:NuevoUsuario();">Alta</a></li>
        <li><a href="javascript:ConsultaUsuarios()">Consulta y modificaci&oacute;n</a></li>
      </ul>
    </li>
    <li><a href="Salir.php">Salir</a>
  </ul>
</nav>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
</script>
<br>
<br>
<br>
<article id="contenido"></article>
<?php }

else
{
	echo "Sessi&oacute;n caducada entre de nuevo";
}?>
</body>
</html>