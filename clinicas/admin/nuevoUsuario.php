<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="../lib/misEstilos.css" rel="stylesheet" type="text/css">
</head>

<body>
<header class="tituloVentana">
Nuevo Usuario
</header>
<form id="nuevoUsuario" name="nuevoUsuario" method="get" action="javascript:AgregarUsuario()">
  <table width="100%" border="0">
    <tr>
      <td class="textosParaInputs" align="right">Usuario</td>
      <td><label for="usuario">
      <input type="text" name="usuario" id="usuario" onBlur="this.value=this.value.toUpperCase();"></label></td>
    </tr>
    <tr>
      <td class="textosParaInputs" align="right">Contrase&ntilde;a</td>
      <td><input type="password" name="password" id="password"></td>
    </tr>
    <tr>
      <td class="textosParaInputs" align="right">Nombre del Usuario</td>
      <td><label for="nombre"></label>
      <input type="text" name="nombre" id="nombre"></td>
    </tr>
    <tr>
      <td class="textosParaInputs" align="right">Tipo de Usuario</td>
      <td><label for="tipoUsuario"></label>
        <select name="tipoUsuario" id="tipoUsuario">
          <option value="-1" selected="selected"></option>
          <option value="2">Usuario de Clinica</option>
          <option value="4">Visor Clinicas</option>
          <option value="6">Administrador de Clinicas</option>
        </select></td>
    </tr>
    <tr>
      <td colspan="2" align="right" class="textosParaInputs">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" align="center" class="botones"><input type="submit" name="boton" id="boton" value="Agregar">&nbsp;
      <input type="reset" name="boton2" id="boton2" value="Regresar"></td>
    </tr>
  </table>
</form>
</body>
</html>