<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>

<body>
<header class="tituloVentana" align="center">Nuevo Usuario</header>
<form id="form1" name="form1" method="post" action="javascript:AgregarUsuario()">
<table width="100%" border="0">
  <tr>
    <td class="textosParaInputs" align="right">Usuario</td>
    <td><label for="usuario">
      <input name="usuario" type="text" id="usuario" onblur="this.value=this.value.toUpperCase();" maxlength="12" />
    </label></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">Contrase&ntilde;a</td>
    <td><input type="password" name="password" id="password" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">Nombre del Usuario</td>
    <td><label for="nombre"></label>
      <input type="text" name="nombre" id="nombre" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">Tipo de Usuario</td>
    <td><label for="tipoUsuario"></label>
      <select name="tipoUsuario" id="tipoUsuario" onchange="mostrarSelectClinica()">
        <option value="-1" selected="selected"></option>
        <option value="6">Administrador Clinica</option>
        <option value="2">Usuario Clinica</option>
        <option value="4">Visor de clinica</option>
        <option value="1">Usuario Contrarreferencia</option>
        <option value="3">Visor Contrarreferencias</option>
        <option value="5">Administrador Contrarreferencia</option>
      </select></td>
  </tr><tr><td colspan="2"><div id="clinica" style="display:none">
  <table width="100%">
  <tr>
    <td width="40%" align="right" class="textosParaInputs">Estado</td>
    <td width="60%"><label for="Unidad"></label>
      <select name="estado" id="estado" onfocus="cargarEstados(this.id);" onchange="cargarMunicipios(this.value,'municipio');ObtenerUnidadesMedicas(this.value);">
      </select>&nbsp;Municipio</span>
      <select name="municipio" id="municipio" onchange="ObtenerUnidadesMedicasMunicipio('estado',this.value);">
      </select></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">Unidad Médica de Adscripción</td>
    <td><label for="Unidad"></label>
      <select name="Unidad" id="Unidad" >
        <option value="-1"></option>
        <option value="0" selected="selected">Hospital Regional</option>
      </select></td>
  </tr></table></div></td></tr>
  <tr>
    <td colspan="2" align="right" class="textosParaInputs">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center" class="botones"><input type="submit" name="boton" id="boton" value="Agregar" />
      &nbsp;
      <input type="reset" name="boton2" id="boton2" value="Regresar" /></td>
  </tr>
</table>
</form>
</body>
</html>