<?php
session_start();
include 'lib/funciones.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>

<body>
<p class="tituloVentana">BUSCAR DERECHOHABIENTE PARA DESACTIVAR TRATAMIENTO</p>
<div class="ventana">
  <tr>
    <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
    <td align="left"><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();">
      <option value="todos" selected="selected">Todos</option>
      <option value="cedula">Cédula DerechoHabiente</option>
      <option value="nombre">Nombre DerechoHabiente</option>
      <option value="medicamento">Medicamento</option>
    </select></td>
  </tr>
</table>
<p>&nbsp;</p>
<div id="buscarPorCedula" style="display:none">
<table width="700" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><form id="selDH" method="post" action="javascript: buscarDHbusqueda(document.getElementById('cedulaBuscar').value);">
    <input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onblur="this.value = this.value.toUpperCase();"/>
      <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." /></form>
      </td>
      <td colspan="2"></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
    <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div></td>
  </tr>
  <tr>
    <td colspan="2" align="center"></td>
  </tr>
</table>
<p>&nbsp;</p>
</div>
<div id="buscarPorNombre" style="display:none;">
              <form id="selDHN" method="post" action="javascript: buscarDHNbusqueda(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                    <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                    <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                    <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                  </tr>
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                    <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                      <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                  </tr>
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                    <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div></td>
                  </tr>
                  <tr>
                    <td colspan="4" align="center"></td>
                  </tr>
                </table>
              </form></div>
    <p align="center"><input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: BuscarTratamientos();" value="Ver Tratamientos Activos" style="display:none" /></p>
  </div>
  <div id="buscarPorMed" style="display:none">
    <p>Buscar Por 
      <select name="buscarMedPor" id="buscarMedPor" onchange="BuscarMediPor()">
        <option value="clave" selected="selected">Clave</option>
        <option value="desc">Nombre de Medicamento</option>
      </select>
    </p>
    <div id="busClave">
      <form id="form1" name="form1" method="get" action="javascript:BuscarMedXClave()">
        <label>Clave de Medicamento
          <input type="text" name="claveMed" id="claveMed" />
        </label>
        <input type="submit" name="buscarMediB" id="buscarMediB" value="Buscar" />
      </form>
    </div>
    <div id="buscarDesc" style="display:none">
      <div class="ui_widget">
        Nombre del Medicamento
        <input name="medicamento1" type="text" id="medicamento1" tabindex="7" onkeyup="SeleccionarMedicina(this.id,1)" />
        <div id="resultados_1" class="miselect2" style="display:compact"></div>
      </div>
    </div>
      <form id="form2" name="form2" action="javascript:BuscarMedicamentosDesact()">
    <p><table width="100%" border="0">
  <tr>
    <td>Medicamento</td>
    <td><div id="medicamento">Ingrese el medicamento a buscar (clave o descripción) y presione buscar...</div></td>
  </tr>
</table>
</p>
<p align="center">
  <input type="submit" name="button2" id="button2" value="Ver Tratamientos Cronicos" />
</p></form>
  </div>
<div align="center">
  
</div>
</div>
            <p>&nbsp;</p>
            <br>
<div id="citas">
<?php
$sql = "Select * from medicamentos_contrarreferencias where id_clinica=" . $_SESSION['idClinica'] . " and activo=1 and cronico=1 group by id_medicamento";
$derechoCrit = ObtenerDHS($sql);
$cont=count($derechoCrit);
if ($cont > 0) {
    echo "<table border='1'><tr class='tituloVentana'><th>Clave</th>
        <th>Medicamento</th>
        <th>Servicio</th>
        <tr>";
		$i=0;
    foreach ($derechoCrit as $key => $Med) {
        $medicamento=  getMedicamentoXId($Med['id_medicamento']);
        $servicio=  DatosServicio($Med['id_servicio']);        
        echo "<tr><td>".$medicamento['id_medicamento']."</td>
            <td>".$medicamento['descripcion']."</td>
            <td>".$servicio['nombre']."</td>
            </tr>";
        echo "<tr><td colspan='3'><table><tr><tr><td colspan='3' class='tituloVentana'>Lista de Pacientes</th></tr><tr><th>Cedula</th><th>Nombre</th><th>&nbsp;</td></tr>";
        $sqlPac="select * from medicamentos_contrarreferencias where id_clinica=" . $_SESSION['idClinica'] . " and activo=1 and cronico=1 and id_medicamento=".$Med['id_medicamento']." and id_servicio=".$Med['id_servicio']." group by id_derecho";
        $listDH= ObtenerDHS($sqlPac);
        foreach ($listDH as $key => $dh) {
            $datosDh=  DatosDerechoHabiente($dh['id_derecho']);
            echo "<tr><td>".$datosDh['cedula']."/".$datosDh['cedula_tipo']."</td>
                <td>".ponerAcentos($datosDh['ap_p']." ".$datosDh['ap_m']." ".$datosDh['nombres'])."</td>
                    <td><input type='button' onclick='DesactivarTratamiento(".$dh['id_Tratamiento'].")' value='Desactivar Tratamiento'></tr>";
        }
        echo "</table></td></tr>";
				$i++;
    }
    echo "</table>";
}
else
    echo "<p class='tituloVentana' align='center'>No existen Tratamientos Cr&iacute;ticos</p>";
?>
</div>
            
</body>
</html>