<?php
session_start();
include './lib/funciones.php';
$tipoDes = $_REQUEST['tipoDes'];
$clin = $_SESSION['idClinica'];
$motivo = $_REQUEST['motivo'];
if ($tipoDes == 1) {
    $idDer = $_REQUEST['idDer'];
    $sql = "update medicamentos_contrarreferencias set activo=0 where id_derecho=$idDer and id_clinica=$clin";
    $desact = insertarSQL($sql);
    if ($desact[0] == 0) {
        $sql = "select * from medicamentos_contrarreferencias where id_derecho=$idDer and id_clinica=$clin";
        $trats = ObtenerDHS($sql);
        foreach ($trats as $key => $trat) {
            $sql = "insert into tratamientosCriticosCancelados Values(Null,".$trat['id_Tratamiento'].",". date("Ymd") . ",'" . $motivo . "'," . $_SESSION['idUsuario'] . "," . $clin . ")";
            insertarSQL($sql);
            if($motivo=="Por Fallecimiento" || $motivo=="Sin Vigencia")
            {
                $sqlDh="update derechohabientes set st=0,extra1='$motivo',status=0 where id_derecho=$idDer";
                insertarSQL($sqlDh);
            }
        }
        echo "1";
    }
    else
        echo "0";
}
else {
    $tratamientos = explode(",", $_REQUEST['tratamientos']);
    $contEr=0;
    $totalTrat=  count($tratamientos);
    foreach ($tratamientos as $key => $trat) {
        $datosTrat =  ObtenerTratamientoXid($trat);
        $sql = "update medicamentos_contrarreferencias set activo=0 where id_Tratamiento=$trat";
        $bandDesact = insertarSQL($sql);
        if ($bandDesact[0] == 0) {
            $sql = "insert into tratamientosCriticosCancelados Values(Null," .$trat. "," . date("Ymd") . ",'" . $motivo . "'," . $_SESSION['idUsuario'] . "," . $clin .")";
            insertarSQL($sql);
        } else {
            $contEr++;
        }
    }
    if($contEr==$totalTrat)
        echo 0;
    else {
        if($contEr==0)
            echo 1;
        else
        echo -1;
    }
}
?>
