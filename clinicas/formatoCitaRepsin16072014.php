<!Doctype html>
<html lang="sp">
<?php
session_start();
include './lib/funciones.php';
$cita=$_REQUEST['idCita'];
$datosCita=  datosCita($cita);
$datosDerecho=  DatosDerechoHabiente($datosCita['id_derecho']);
$horario=  datosHorarioXcita($datosCita['id_horario']);
$servicio=  nombreServicio($horario['id_servicio']);
$medico=  getMedicoXid($horario['id_medico']);
?>
</p>
<head>
<link rel="stylesheet" href="lib/impresion2.css" />
<title>Cita reprogramada <?php echo $cita; ?> </title>
</head>
<body>
<table width="92%" height="71" border="0" class="tablaDiag">
  <tr>
    <td><img src="diseno/issste2013.png" alt="" width="157" height="46"></td>
    <td class="encabezado" align="center"><p>Comprobante de Cita al Hospital Regional Valent&iacute;n Gomez Farias</p>
      <p>Folio <?php echo $_REQUEST['idCitaApartada']; ?></p></td>
  </tr>
  <tr>
    <td width="39%" class="encabezado">Cedula</td>
    <td width="61%"><?php echo $datosDerecho['cedula']."/".$datosDerecho['cedula_tipo']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">Nombre</td>
    <td class="datos"><?php echo ponerAcentos($datosDerecho['ap_p']." ".$datosDerecho['ap_m']." ".$datosDerecho['nombres']); ?></td>
  </tr>
  <tr>
    <td class="encabezado">Telefono</td>
    <td class="datos"><?php echo $datosDerecho['telefono']; ?></td>
  </tr>
  <td class="encabezado">Correo Electronico</td>
    <td class="datos"><?php echo $datosDerecho['email']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">Celular</td>
    <td class="datos"><?php echo $datosDerecho['celular']; ?></td>
  </tr>
  <tr>
    <td class="encabezado" colspan="2" align="center">Datos de la Cita</td>
  </tr>
  <tr>
    <td class="encabezado">Fecha</td>
    <td class="datos"><?php echo date("d/m/Y",strtotime($datosCita['fecha_cita']))." ".date("H:i",strtotime($horario['hora_inicio']))."-".date("H:i",strtotime($horario['hora_fin'])); ?></td>
  </tr>
  <tr>
    <td class="encabezado">Servicio</td>
    <td class="datos"><?php echo $servicio; ?></td>
  </tr>
  <tr>
    <td class="encabezado">M&eacute;dico</td>
    <td class="datos"><?php echo $medico['titulo']." ".$medico['ap_p']." ".$medico['ap_m']." ".$medico['nombres']; ?></td>
  </tr>
  <tr>
    <td class="nota" colspan="2"><p>Cualquier cambio en su cita se le avisara en los medios de contactos proporcionados</p>
      <p>Hospital Regional Valent&iacute;n G&oacute;mez Farias <br />
        Av. Soledad Orozco 203, colonia El Capullo, Zapopan, Jalisco <br />
        Telefono 38360650</p></td>
  </tr>
</table>
<p class="nota">&nbsp;</p>
<p>&nbsp;</p>
<p>
</p></body>
</html>