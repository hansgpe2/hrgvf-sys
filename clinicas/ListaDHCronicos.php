<?php 
session_start();
include './lib/funciones.php';
$idDer=$_REQUEST['idDh'];
$derecho=  DatosDerechoHabiente($idDer);
?>
<!Doctype html>
<html>
<body>

<p class="tituloVentana" align="center">Tratamiento del Paciente <?php echo ponerAcentos($derecho['ap_p']." ".$derecho['ap_m']." ".$derecho['nombres']); ?></p>
<p class="textosParaInputs">Desactivar Por&nbsp;&nbsp;
    <select name="TipoDesc" id="TipoDesc" onchange="ActivarTipoDesc()">
        <option value="1">C&eacute;dula</option>
        <td><option value="2">Tratamientos</option>
    </select></p>
<p>
<form name="DesTrat" id="DesTrat" action="javascript:DesactivarTratamientosConfirmar()">
    <div id="Tratamientos" style="display: none">
  <?php
$sql = "Select * from medicamentos_contrarreferencias where id_clinica=" . $_SESSION['idClinica'] . " and activo=1 and cronico=1 and id_derecho=$idDer";
$derechoCrit = ObtenerDHS($sql);
$cont=count($derechoCrit);
if ($cont > 0) {
    echo "<table class='Ventana'><tr class='tituloVentana'><th>Clave</th>
        <th>Medicamento</th>
        <th>Servicio</th>
        <th>Desactivar</th><tr>";
		$i=0;
    foreach ($derechoCrit as $key => $Med) {
        $medicamento=  getMedicamentoXId($Med['id_medicamento']);
        $servicio=  DatosServicio($Med['id_servicio']);        
        echo "<tr><td>".$medicamento['id_medicamento']."</td>
            <td>".$medicamento['descripcion']."</td>
            <td>".$servicio['nombre']."</td>
                <td><input type='checkbox' id='trat[]' name='trat[]' value='".$Med['id_Tratamiento']."'></td></tr>";
				$i++;
    }
    echo "</table>";
}
else
    echo "<p class='tituloVentana' align='center'>No existen Tratamientos Cr&iacute;ticos</p>";
?>
    </div>
<?php if($cont>0){ ?>
  <div id="trat">
    <p><span class="textosParaInputs">Motivo de Cancelación </span>
      <label for="MotCan"></label>
      <select name="MotCan" id="MotCan" onChange="ActivarOtroMotivo()">
        <option value="-1" selected="selected"></option>
        <option value="Sin Vigencia">Sin Vigencia</option>
        <option value="Por Fallecimiento">Por Fallecimiento</option>
        <option value="otro">otro</option>
        </select>
      <span class="textosParaInputs">
      <input name="idDer" type="hidden" id="idDer" value="<?php echo $idDer; ?>">
    </span></p>
  </div>
  <p>
  <div id="om" style="display:none">
    <label for="otroMot" class="textosParaInputs">Especifique Motivo</label>
    <input type="text" name="otroMot" id="otroMot">
</div>
<?php 
}
?>
<p align="center">
  <input type="button" name="button2" id="button2" value="Cancelar" class="botones">&nbsp;
  <input type="submit" name="button" id="button" value="Desactivar Tratamiento" class="botones" <?php if ($cont == 0) echo "disabled='disabled'"; ?>>
</p>
</form>
</body>
</html>
