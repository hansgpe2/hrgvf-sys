<!Doctype html>
<html lang="sp">
<?php
session_start();
include './lib/funciones.php';
$cita=$_REQUEST['idCita'];
$datosCita=  datosCita($cita);
$datosDerecho=  DatosDerechoHabiente($datosCita['id_derecho']);
$horario=  datosHorarioXcita($datosCita['id_horario']);
$servicio=  nombreServicio($horario['id_servicio']);
$medico=  getMedicoXid($horario['id_medico']);
?>
</p>
<head>
<link rel="stylesheet" href="lib/impresion2.css" />
<title>Cita apartada <?php echo $cita; ?> </title>
<style type="text/css">
<!--
.Estilo1 {font-size: 12px}
-->
</style>
</head>
<body>
<table width="92%" height="71" border="0" class="tablaDiag">
  <tr>
  <td><img src="diseno/issste2013.png" width="157" height="46"></td>
    <td align="center" class="encabezado"><p>Comprobante de Cita "APARTADA" al </p>
      <p>Hospital Regional &quot;Dr. Valent&iacute;n G&oacute;mez Far&iacute;as&quot;</p>
    <p>Folio <?php echo $_REQUEST['idCitaApartada']; ?></p></td>
  </tr>
  <tr>
    <td width="39%" class="encabezado">Cedula</td>
    <td width="61%"><?php echo $datosDerecho['cedula']."/".$datosDerecho['cedula_tipo']; ?></td>
  </tr>
  <tr>
    <td class="encabezado">Nombre</td>
    <td class="datos"><?php echo ponerAcentos($datosDerecho['ap_p']." ".$datosDerecho['ap_m']." ".$datosDerecho['nombres']); ?></td>
  </tr><tr>
  <td class="encabezado">Tel&eacute;fono</td>
    <td class="datos"><?php echo $datosDerecho['telefono']; ?></td>
    </tr>
      <td class="encabezado">Correo Electr&oacute;nico</td>
        <td class="datos"><?php echo $datosDerecho['email']; ?></td>
    </tr>
    <tr><td class="encabezado">Celular</td>
    <td class="datos"><?php echo $datosDerecho['celular']; ?></td>
    </tr>
  <tr>
    <td class="encabezado" colspan="2" align="center">Datos de la Cita</td>
    
  </tr>
  <tr>
    <td class="encabezado">Fecha</td>
    <td class="datos"><?php echo date("d/m/Y",strtotime($datosCita['fecha_cita']))." ".date("H:i",strtotime($horario['hora_inicio']))."-".date("H:i",strtotime($horario['hora_fin'])); ?></td>
  </tr>
  <tr>
    <td class="encabezado">Servicio</td>
    <td class="datos"><?php echo $servicio; ?></td>
  </tr>
  <tr>
    <td class="encabezado">M&eacute;dico</td>
    <td class="datos"><?php echo $medico['titulo']." ".$medico['ap_p']." ".$medico['ap_m']." ".$medico['nombres']; ?></td>
  </tr>
  <tr>
    <td class="nota" colspan="2"><p class="datos Estilo1">La Confirmacion de su cita es Directamente en su Cliniaca y cualquier cambio se le avisara en los medios de contactos proporcionados</p>
      <p class="datos Estilo1"><strong>&quot;Ay&uacute;danos a brindar un mejor servicio. Si por alg&uacute;n motivo no  puedes acudir a tu cita h&aacute;znoslo saber con anticipaci&oacute;n al  Tel&eacute;fono (33)3836-0650 ext 110, Juntos ayudaremos a otro paciente a recibir una atenci&oacute;n m&aacute;s  pronta. Gracias.&quot;</strong></p>
      <p class="datos Estilo1"><strong>Hospital Regional &quot;Dr. Valent&iacute;n G&oacute;mez Farias&quot;<br />
      Av. Soledad Orozco 203, colonia El Capullo, Zapopan, Jalisco <br />
    </strong></p></td>
    
  </tr>
</table>
<p class="nota">&nbsp;</p>
<p>&nbsp;</p>
<p>
</p></body>
</html>