<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Documento sin título</title>
    </head>

    <body>
        <center>
            <form id="form1" name="form1" method="post" action="reporteMensual.php" target="_blank">
                <table width="100%" border="0">
                    <tr>
                        <td colspan="2">Reporte de Medicamentos</td>
                    </tr>
                    <tr>
                        <td width="39%">Mes</td>
                        <td width="61%"><label for="meses"></label>
                            <select name="meses" id="meses">
                                <?php
                                $anio = date("Y");
                                $mes = date("n");
                                $anioF = $anio - 1;
                                while ($anio >= $anioF) {
                                    for ($i = (int) $mes; $i >= 1; $i--) {
                                        switch ($i) {
                                            case 1:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-01-01|$anio-01-" . date("d") . "'>Enero $anio</option>";
                                                else
                                                    echo "<option value='$anio-01-01|$anio-01-31'>Enero $anio</option>";
                                                break;
                                            case 2:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-02-01|$anio-02-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-02-01|$anio-02-28'>Febrero $anio</option>";
                                                break;
                                            case 3:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-03-01|$anio-03-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-03-01|$anio-03-31'>Marzo $anio</option>";
                                                break;
                                            case 4:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-04-01|$anio-04-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-04-01|$anio-04-30'>Abril $anio</option>";
                                                break;
                                            case 5:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-05-01|$anio-05-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-05-01|$anio-05-31'>Mayo $anio</option>";
                                                break;
                                            case 6:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-06-01|$anio-06-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-06-01|$anio-06-30'>Junio $anio</option>";
                                                break;
                                            case 7:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-07-01|$anio-07-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-07-01|$anio-07-31'>Julio $anio</option>";
                                                break;
                                            case 8:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-08-01|$anio-08-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-08-01|$anio-08-30'>Agosto $anio</option>";
                                                break;
                                            case 9:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-09-01|$anio-09-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-09-01|$anio-09-30'>Septiembre $anio</option>";
                                                break;
                                            case 10:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-10-01|$anio-10-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-10-01|$anio-10-31'>Octubre $anio</option>";
                                                break;
                                            case 11:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-11-01|$anio-11-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-11-01|$anio-11-30'>Noviembre $anio</option>";
                                                break;
                                            case 12:
                                                if ($mes == date("n"))
                                                    echo "<option value='$anio-12-01|$anio-12-" . date("d") . "'>Febrero $anio</option>";
                                                else
                                                    echo "<option value='$anio-12-01|$anio-12-31'>Diciembre $anio</option>";
                                                break;
                                        }
                                    }
                                    $anio = $anio - 1;
                                    $mes = 12;
                                }
                                ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="submit" value="Generar Reporte" /></td>
                    </tr>
                </table>
            </form>
        </center>
    </body>
</html>