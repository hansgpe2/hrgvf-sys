<?php

include 'lib/funciones.php';
$estado = $_REQUEST['estado'];
$municipio = quitarAcentos($_REQUEST['municipio']);
$cp = $_REQUEST['cp'];
if (isset($_REQUEST['colonia']))
    $colonia = $_REQUEST['colonia'];
else
    $colonia = "";
if (isset($estado))
    $sql = "select colonia from codigo_postales where estado='$estado' and municipio like '%" . $municipio . "%' order by colonia ASC";
elseif (isset($cp)) {
    $sql = "select colonia from codigo_postales where cp='$cp' and municipio like '%" . $municipio . "%' order by colonia ASC";
}
$query = ObtenerDHS($sql);
$ret = "<option value='-1'> </option>";
if (count($query) > 0)
    foreach ($query as $key => $row) {
        if ($colonia == $row['colonia'])
            $ret.="<option value='" . htmlentities($row['colonia']) . "' selected='selected'>" . ponerAcentos($row['colonia']) . "</option>";
        else
            $ret.="<option value='" . htmlentities($row['colonia']) . "'>" . ponerAcentos($row['colonia']) . "</option>";
    }
print $ret;
?>
