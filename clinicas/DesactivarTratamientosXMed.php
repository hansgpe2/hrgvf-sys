<?php

session_start();
include './lib/funciones.php';
$idMed = $_REQUEST['idMed'];
$medCrit = ObtenerPacientesXMed($idMed, $_SESSION['idClinica'], date("Ymd"), date("Ymd"));
$datosMed = getMedicamentoXId($idMed);
$cont=  count($medCrit);
?>
<!Doctype html>
<html>
<body>

<p class="tituloVentana" align="center">Tratamiento del Medicamento <?php echo $datosMed['id_medicamento']." ".substr($datosMed['descripcion'], 0,60); ?>...</p>
<p class="textosParaInputs">Desactivar Por&nbsp;&nbsp;
    <select name="TipoDesc" id="TipoDesc" onchange="ActivarTipoDescMed()">
        <option value="1">Medicamentos</option>
        <td><option value="2">Pacientes</option>
    </select></p>
<p>
<form name="DesTrat" id="DesTrat" action="javascript:DesactivarTratamientosXMedConfirmar()">
    <div id="Tratamientos" style="display: none">
<?php
if (count($medCrit) > 0) {
    echo "<table class='Ventana'><tr class='tituloVentana'>
    <th>Cedula</th>
    <th>Nombre</th>
    <th>Vigente desde</th>
    <th>Desactivar</th>";
    foreach ($medCrit as $key => $Med) {
        //$datosDH = DatosDerechoHabiente($Med['id_derecho']);
        echo "<tr><td>" . $Med['cedula']. "</td>
        <td>" . $Med['nombreDH'] . "</td>
            <td>" . $Med['vigente'] . "</td>
                <td><input type='checkbox' id='trat[]' name='trat[]' value='" . $Med['id_Tratamiento'] . "'></td></tr>";
    }
    echo "</table>";
}
else
    echo "<p class='tituloVentana' align='center'>No existen Tratamientos Cr&iacute;ticos</p>";
?>
    </div>
    <?php if($cont>0){ ?>
  <div id="trat">
    <p><span class="textosParaInputs">Motivo de Cancelación </span>
      <label for="MotCan"></label>
      <select name="MotCan" id="MotCan" onChange="ActivarOtroMotivo()">
        <option value="-1" selected="selected"></option>
        <option value="Sin Tratamiento">Sin Tratamiento</option>
        <option value="otro">otro</option>
        </select>
      <span class="textosParaInputs">
      <input name="idMed" type="hidden" id="idMed" value="<?php echo $idMed; ?>">
    </span></p>
  </div>
  <p>
  <div id="om" style="display:none">
    <label for="otroMot" class="textosParaInputs">Especifique Motivo</label>
    <input type="text" name="otroMot" id="otroMot">
</div>
<?php 
}
?>
<p align="center">
  <input type="button" name="button2" id="button2" value="Cancelar" class="botones">&nbsp;
  <input type="submit" name="button" id="button" value="Desactivar Tratamiento" class="botones" <?php if ($cont == 0) echo "disabled='disabled'"; ?>>
</p>
</form>
</body>
</html>
