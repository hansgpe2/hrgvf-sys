<?php
include 'lib/funciones.php';
crearVistacitas();
$citas = citasXderechohabiente($_GET['id_derecho']);
if (count($citas) > 0) {
    ?>
    <br>
    <table width="600" border="1" cellspacing="0" cellpadding="0">
        <tbody>
            <?php
            foreach ($citas as $key => $cita) {
                $nombreMedico = $cita['titulo'] . " " . $cita['appm'] . " " . $cita['apmm'] . " " . $cita['nomm'];
                $nombreDH = $cita['appdh'] . " " . $cita['apmdh'] . " " . $cita['nomdh'];
                switch ($cita['tipo_usuario']) {
                    case 2:
                        $datosCitaMedico = citaMedico($cita['id_cita']);
                        $datosUsuario = getMedicoXid($datosCitaMedico['id_medico']);
                        break;
                    case 3:
                        $datosUsuario = obtenerDatosUsuario($datosCita['id_usuario']);
                        break;
                    default:
                        $datosUsuario = getUsuarioXidAgenda($datosCita['id_usuario']);
                }
                $datos = $cita['cedula'] . "/" . $cita['cedula_tipo'] . " - " . $cita['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $cita['extra1'] . " - " . $cita['observaciones'];
                $datosCita = $cita['servicio'] . " - " . $nombreMedico;
                if ($cita['tipo_cita'] == 0)
                    $claseParaDia = "citaXdiaPRV";
                if ($cita['tipo_cita'] == 1)
                    $claseParaDia = "citaXdiaSUB";
                if ($cita['tipo_cita'] == 2)
                    $claseParaDia = "citaXdiaPRO";
                $fecha = formatoDia($cita['fecha_cita'], "imprimirCita");
                ?>
                <tr>
                    <td class="<?php echo $claseParaDia; ?> ">
                        <table width="700" height="40" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="180" class="citaXdiaHora"><?php echo $fecha . " - " . formatoHora($cita['hora_inicio']) . " - " . formatoHora($cita['hora_fin']) ?></td>
                                <td align="left" width="520" class="citaXdiaNombre"><?php echo ponerAcentos($nombreDH); ?></td>
                            </tr>
                            <tr>
                                <td align="left" class="citaXdiaInfo" colspan="2"><?php echo ponerAcentos($datos); ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align=\"left\" class=\"citaXdiaInfo\"><?php echo ponerAcentos($datosCita); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
}
else {
    ?>

    <br>
    <table width="600" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center">
                NO EXISTEN CITAS DEL DERECHOHABIENTE
            </td>
        </tr>
    </table>
    <?php
}
?>
<p align="center"><table width="500" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">
    <tr>
        <td width="35" height="25" class="citaXdiaPRV"></td><td width="129">Citas Primera Vez</td>
        <td width="35" class="citaXdiaSUB"></td><td width="138">Citas Subsecuentes</td>
        <td width="35" class="citaXdiaPRO"></td><td width="138">Citas Procedimientos</td>
    </tr>
</table>
</p>
<?php
crearVistacitasExt();
$citas=citasExtXderechohabiente($_GET['id_derecho']);
if (count($citas) > 0) {
    ?>
    <br>
    <table width="600" border="1" cellspacing="0" cellpadding="0">
        <tbody>
            <?php
            foreach ($citas as $key => $cita) {
                $nombreMedico = $cita['titulo'] . " " . $cita['appm'] . " " . $cita['apmm'] . " " . $cita['nomm'];
                $nombreDH = $cita['appdh'] . " " . $cita['apmdh'] . " " . $cita['nomdh'];
                switch ($cita['tipo_usuario']) {
                    case 2:
                        $datosCitaMedico = citaMedico($cita['id_cita']);
                        $datosUsuario = getMedicoXid($datosCitaMedico['id_medico']);
                        break;
                    case 3:
                        $datosUsuario = obtenerDatosUsuario($datosCita['id_usuario']);
                        break;
                    default:
                        $datosUsuario = getUsuarioXidAgenda($datosCita['id_usuario']);
                }
                $datos = $cita['cedula'] . "/" . $cita['cedula_tipo'] . " - " . $cita['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $cita['extra1'] . " - " . $cita['observaciones'];
                $datosCita = $cita['servicio'] . " - " . $nombreMedico;
                if ($cita['tipo_cita'] == 0)
                    $claseParaDia = "citaXdiaPRV";
                if ($cita['tipo_cita'] == 1)
                    $claseParaDia = "citaXdiaSUB";
                if ($cita['tipo_cita'] == 2)
                    $claseParaDia = "citaXdiaPRO";
                $fecha = formatoDia($cita['fecha_cita'], "imprimirCita");
                ?>
                <tr>
                    <td class="<?php echo $claseParaDia; ?> ">
                        <table width="700" height="40" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="180" class="citaXdiaHora"><?php echo $fecha . " - " . formatoHora($cita['hora_inicio']) . " - " . formatoHora($cita['hora_fin']) ?></td>
                                <td align="left" width="520" class="citaXdiaNombre"><?php echo ponerAcentos($nombreDH); ?></td>
                            </tr>
                            <tr>
                                <td align="left" class="citaXdiaInfo" colspan="2"><?php echo ponerAcentos($datos); ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align=\"left\" class=\"citaXdiaInfo\"><?php echo ponerAcentos($datosCita); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <?php
}
?>
