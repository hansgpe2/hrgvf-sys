<?php
$mes = date("n");
$dia = date("d");
$meses = array("ene", "feb", "marzo", "abril", "mayo", "junio", "julio", "ago", "sept", "oct", "nov", "dic");
session_start();
include('lib/funciones.php');
$clinica = obtenerDatosClinica($_SESSION['idClinica']);
$estado = $clinica['Estado'];
if (isset($_REQUEST['cedula']))
    $cedula = $_REQUEST['cedula'];
else
    $cedula = "";
?>
<style type="text/css">
    .etiquetas {
        font-size: xx-small;
        color: #F00;
    }
</style>

<form name="agregarDH" id="agregarDH" submit="javascript:validarAgregarDH();">
    <table width="945" border="0" cellspacing="0" cellpadding="0" class="ventana">
        <tr>
            <td colspan="5" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
        </tr>
        <tr>
            <td width="187">&nbsp;</td>
            <td width="177">&nbsp;</td>
        </tr>
        <tr>
            <td height="25" align="right"><label for="cedulaAgregar" class="error"><span class="textosParaInputs">CEDULA:</span></label> </td>
            <td align="left"><input name="cedulaAgregar" type="text" id="cedulaAgregar" tabindex="1" onfocus="if (this.value == 'Cedula del DerechoHabiente') {
                        this.value = ''
                    }" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo $cedula; ?>" maxlength="10"/>

            </td>
        </tr>
        <tr><td class="textosParaInputs" align="right">Sexo</td><td><select name="sexo" id="sexo" tabindex="2" onchange="ObtenerTpoDerXSexo(this.id, 'cedulaTipoAgregar');">
                    <option value="-1" selected="selected"></option>
                    <option value="H">Hombre</option>
                    <option value="M">Mujer</option>
                </select></td><td width="236" class="textosParaInputs" align="right">Tipo de DerechoHabiente</td><td width="345"><select name="cedulaTipoAgregar" id="cedulaTipoAgregar" tabindex="3" onfocus="ObtenerTpoDerXSexo('sexo', this.id);"></select></td></tr>
        <tr>
            <td height="25" class="textosParaInputs" align="center" colspan="2">NOMBRE DEL DERECHOHABIENTE:</td></tr>
        <tr><td height="26" align="right" class="textosParaInputs">Apellido Paterno
            </td><td align="left" colspan="3"><input name="ap_pAgregar" type="text" id="ap_pAgregar" tabindex="4" onfocus="javascript:borrarReferencias(this);" onkeyup="this.value = this.value.toUpperCase();" size="20" maxlength="20" />
                <span class="textosParaInputs">Apellido Materno</span>
                <input name="ap_mAgregar" type="text" id="ap_mAgregar" tabindex="5" onkeyup="this.value = this.value.toUpperCase();" size="20" maxlength="20" /><label for="ap_mAgregar" class="error"></label></td></tr>
        <tr><td class="textosParaInputs" align="right">Nombre(s)</td><td colspan="3" align="left">
                <input name="nombreAgregar" type="text" id="nombreAgregar" tabindex="6" onkeyup="this.value = this.value.toUpperCase();" size="50" maxlength="50" /><label for="nombreAgregar" class="error"></label></td>
        </tr>
        <tr>
            <td height="25" align="right"><label for="telefonoAgregar" class="error"><span class="textosParaInputs">TELEFONO: (01)</span></label></td>
            <td align="left" colspan="3"><input name="telefonoAgregar" type="text" id="telefonoAgregar" tabindex="6" size="11" maxlength="10" />
                <span class="etiquetas">
                    formato (lada)numero ej 3312345678
                </span>
                <label for="fecha_nacAgregar" class="error"><span class="textosParaInputs"> FECHA NAC. </span></label>
                <input type="text" id="fecha_nacAgregar" name="fecha_nacAgregar" value="<?php echo date('d/m/Y'); ?>" ></td>
        </tr>
        <tr>
            <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
            <td colspan="3" align="left"><select name="estadoAgregar" id="estadoAgregar" tabindex="10" ></select>     

                <span class="textosParaInputs">
                    <input name="estadoClinica" type="hidden" id="estadoClinica" value="<?php echo $clinica['Estado']; ?>" />
                    MUNICIPIO: </span>
                <select name="municipioAgregar" id="municipioAgregar" tabindex="11" >      </select>
                <input name="municipioClinica" type="hidden" id="municipioClinica" value="<?php echo $clinica['Municipio']; ?>" /></td>
        </tr>
        <tr>
            <td height="25" class="textosParaInputs" align="right"><label for="direccionAgregar" class="error"><span class="textosParaInputs">DIRECCION:</span></label></td>
            <td colspan="3" align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" tabindex="12" onkeyup="this.value = this.value.toUpperCase();" size="64" maxlength="50" />
                <span class="etiquetas">No usar simbolos :#,*,$,',&lt;,&gt;,|, solo números y letras</span></td>
        </tr>
        <tr>
            <td height="25" class="textosParaInputs" align="right"><label for="colonia" class="textosParaInputs">COLONIA</label></td>
            <td colspan="3" align="left"><select name="colonia" id="colonia" tabindex="13" >
                </select>
                &nbsp;&nbsp;<span class="textosParaInputs">CODIGO POSTAL</span>
                <select name="codigoPostal" id="codigoPostal" tabindex="14" >
                </select></td>
        </tr>
        <tr><td class="textosParaInputs">Unidad Medica de Adscripcion</td>

            <td colspan="3"><?php
                if ($clinica['tiene_poblacion_adscrita'] == 1) {
                    echo $clinica['nombre'];
                    ?>
                    <input name="Unidad" type="hidden" id="Unidad" value="<?php echo $clinica['id_unidad']; ?>" />
                    <?php
                } else {
                    ?>
                    <select  name="Unidad" id="Unidad">
                        <?php echo UnidadMedicaProcedencia($_SESSION['estadoClinica'], $_SESSION['idClinica']) ?>
                    </select>
                    <?php
                }
                ?>
            </td></tr>
        <tr>
            <td align="right" class="textosParaInputs">CORREO ELECTR&Oacute;NICO</td>
            <td colspan="3"><input name="email" type="text" id="email" tabindex="15"  />
                &nbsp;<span class="textosParaInputs">CELULAR</span>&nbsp;
                <input name="cel" type="text" id="cel" tabindex="16" size="10" maxlength="10" /></td>
        </tr>
        <tr>
            <td colspan="4" align="center"><div id="divBotones_EstadoAgregarDH">
                    <button id="enviar" type="button" class="botones" onclick="regresar()">Regresar</button>&nbsp;&nbsp;<input name="" type="button" class="botones" id="guardar" tabindex="17" onclick="validarAgregarDH()" value="Agregar">
                </div>
            </td></tr>
        <tr><td colspan="3"><div id="citas"></div></td></tr>
    </table>
    <input type="hidden" name="fechaNacimiento" id="fechaNacimiento" value="<?php echo date('Ymd') ?>" >
</form>
<?php
$fechaMin = strtotime("");
$yfm = date("Y", $fechaMin) - 50;
$mfm = date("m", $fechaMin) - 1;
$dfm = date("d", $fechaMin);
$dMax = date("d");
$mMax = date("m") - 1;
$yMax = date("Y");
?>
<script type="text/javascript">
                $(document).ready(function() {
                    $("#estadoAgregar").ready(function() {
                        $.ajax({
                            url: "cargarEstados.php",
                            data:
                                    {
                                        estado: '<?php echo $clinica['Estado']; ?>'
                                    },
                            success: function(data)
                            {
                                $("#estadoAgregar").append(data);
                            }
                        });
                    });
                    $("#estadoAgregar").change(function() {
                        $.ajax({
                            url: "cargarMunicipios.php",
                            data: {
                                estado: $("#estadoAgregar option:selected").val(),
                                municipio: undefined
                            },
                            success: function(data) {
                                $("#municipioAgregar").append(data);
                            }
                        });
                    });
                    $("#municipioAgregar").change(function() {
                        vaciarSelect('colonia');
                        vaciarSelect('codigoPostal');
                        $.ajax({
                            url: "mostrarColonias.php",
                            data: {
                                estado: $("#estadoAgregar option:selected").val(),
                                municipio: $("#municipioAgregar option:selected").val()
                            },
                            success: function(data) {
                                $("#colonia").append(data);
                            }
                        });
                        $.ajax({
                            url: "codigos_postales.php",
                            data: {
                                estado: $("#estadoAgregar").val(),
                                municipio: $("#municipioAgregar option:selected").val(),
                                cp: ""
                            },
                            success: function(data) {
                                $("#codigoPostal").append(data);
                            }
                        });
                    });
                    $("#codigoPostal").change(function()
                    {
                        vaciarSelect('colonia');
                        $.ajax({
                            url: "mostrarColonias.php",
                            data: {
                                cp: $("#codigoPostal").val(),
                                municipio: $("#municipioAgregar").val()
                            },
                            success: function(data) {
                                $("#colonia").append(data);
                            }
                        });
                    });
                    $("#colonia").change(function() {
                        vaciarSelect('codigoPostal');
                        $.ajax({
                            url: "codigos_postales.php",
                            data: {
                                estado: $("#estadoAgregar option:selected").val(),
                                municipio: $("#municipioAgregar option:selected").val(),
                                col: $("#colonia option:selected").val()
                            },
                            success: function(data) {
                                $("#codigoPostal").append(data);
                            }

                        });
                    });
                    $("#fecha_nacAgregar").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        minDate: new Date(<?php echo $yfm; ?>,<?php echo $mfm; ?>,<?php echo $dfm; ?>),
                        maxDate: new Date(<?php echo $yMax; ?>,<?php echo $mMax; ?>,<?php echo $dMax; ?>),
                        dateFormat: "dd-mm-yy",
                        altFormat: "yymmdd",
                        altField: "#fechaNacimiento",
                        yearRange:"<?php echo $yfm; ?>:<?php echo $yMax; ?>"
                    });
                });
</script>