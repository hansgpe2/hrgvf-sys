<?php require_once('Connections/bdissste.php'); ?>
<?php
@session_start();
date_default_timezone_set('America/Mexico_City');
$hoyC = date('Ymd');
$sesion_galleta = '';
if ((isset($_SESSION['idUsuario'])) && ($_SESSION['idUsuario'] > 0)) $sesion_galleta = $_SESSION['idUsuario'];

$galleta_citas = 'citas'.$hoyC.$sesion_galleta;
$galleta_citasE = 'citasE'.$hoyC.$sesion_galleta;
$galleta_enca = 'enca'.$hoyC.$sesion_galleta;

$statusRecetas = array("CANCELADA", "SIN SURTIR", "SURTIDA");
$statusMedicam = array("APLICADO", "SIN APLICAR", "CANCELADO", "SIN SURTIR", "SIN CADUCIDAD", "REMANENTE SIN APLICAR");
$tipoLogs = array("ingreso al sistema", "agregar derechohabiente", "agregar receta", "aplicar dosis", "baja medicamento", "baja medicamento caduco", "reutilizar medicamento", "impresion de reporte", "salir del sistema");
$statusCitas = array("DISPONIBLE", "CITA PENDIENTE", "CITA CUBIERTA", "DE VACACIONES", "EN CONGRESO", "DIA INHABIL", "LICENCIA MEDICA", "BAJA TEMPORAL", "OTRO", "ELIMINADA");
define("ENTIDAD_FEDERATIVA", "JALISCO");
define("CLAVE_UNIDAD_MEDICA", "03420300");
// constantes para la importacion de existencias de siam xls
define("SIAM_NOMBRE_TABLA", "SIAM2-ExistenciasMasApartados");
define("SIAM_FILA_INICIAL", 5);
define("SIAM_COL_ID_MEDICAMENTO", 0);
define("SIAM_COL_PARTIDA", 1);
define("SIAM_COL_DESCRIPCION", 2);
define("SIAM_COL_EXISTENCIA", 3);
define("SIAM_COL_PRECIO", 7);

define("SERIE", "034RM");
define("FOLIO_INICIAL", "1");
define("FOLIO_FINAL", "9999999");
define("FOLIO_ACTUAL", "1");

function tituloDia($dia_num) {
    switch ($dia_num) {
        case 0: return "<abbr title=\"Domingo\">Domingo</abbr>";
            break;
        case 1: return "<abbr title=\"Lunes\">Lunes</abbr>";
            break;
        case 2: return "<abbr title=\"Martes\">Martes</abbr>";
            break;
        case 3: return "<abbr title=\"Miercoles\">Mi&eacute;rcoles</abbr>";
            break;
        case 4: return "<abbr title=\"Jueves\">Jueves</abbr>";
            break;
        case 5: return "<abbr title=\"Viernes\">Viernes</abbr>";
            break;
        case 6: return "<abbr title=\"Sabado\">S&aacute;bado</abbr>";
            break;
    }
}

function tituloMes($mes) {
    $meses = array(12);
    if ($mes[0] == "0")
        $mes = $mes[1];
    $meses[1] = "Enero";
    $meses[2] = "Febrero";
    $meses[3] = "Marzo";
    $meses[4] = "Abril";
    $meses[5] = "Mayo";
    $meses[6] = "Junio";
    $meses[7] = "Julio";
    $meses[8] = "Agosto";
    $meses[9] = "Septiembre";
    $meses[10] = "Octubre";
    $meses[11] = "Noviembre";
    $meses[12] = "Diciembre";
    return $meses[$mes];
}

function diaSemana($dia_num) {
    switch ($dia_num) {
        case 0: return "DOMINGO";
            break;
        case 7: return "DOMINGO";
            break;
        case 1: return "LUNES";
            break;
        case 2: return "MARTES";
            break;
        case 3: return "MIERCOLES";
            break;
        case 4: return "JUEVES";
            break;
        case 5: return "VIERNES";
            break;
        case 6: return "SABADO";
            break;
    }
}

function regresarIdMedico($idConsultorio, $idServicio) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT id_medico FROM servicios_x_consultorio WHERE id_consultorio='" . $idConsultorio . "' and id_servicio='" . $idServicio . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $idMedico = $row_query['id_medico'];
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $idMedico;
}

function formatoHora($hora) {
    return substr($hora, 0, 2) . ":" . substr($hora, 2, 2);
}

function quitarPuntosHora($hora) {
    return substr($hora, 0, 2) . substr($hora, 3, 2);
}

function getDH($where, $order) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM derechohabientes " . $where . " " . $order;
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_derecho' => $row_query['id_derecho'],
                'cedula' => $row_query['cedula'],
                'cedula_tipo' => $row_query['cedula_tipo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres'],
                'fecha_nacimiento' => $row_query['fecha_nacimiento'],
                'telefono' => $row_query['telefono'],
                'direccion' => $row_query['direccion'],
                'estado' => $row_query['estado'],
                'municipio' => $row_query['municipio'],
                'status' => $row_query['status']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function buscarDHxCedulaParaCita($cedula) {
    $dhs = getDH("WHERE cedula like '%" . $cedula . "%'", "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE CEDULA QUE CONTENGA " . strtoupper($cedula) . "</option>";
        $out2 = " <input name=\"agregarDH\" type=\"button\" value=\"Agregar Derechohabiente\" class=\"botones\" id=\"agregarDH\" onClick=\"javascript: agregarDHenCita();\" />";
    }
    $out .= "</select>";
    return $out . $out2;
}

function buscarDHxNombreParaCita($ap_p, $ap_m, $nombre) {
    $temp = "";
    if (strlen($ap_p) > 0) {
        $temp .= "ap_p like '%" . $ap_p . "%'";
    }
    if (strlen($ap_m) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR ap_m like '%" . $ap_m . "%'";
        } else {
            $temp .= "ap_m like '%" . $ap_m . "%'";
        }
    }
    if (strlen($nombre) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR nombres like '%" . $nombre . "%'";
        } else {
            $temp .= "nombres like '%" . $nombre . "%'";
        }
    }
    $dhs = getDH("WHERE " . $temp, "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE PACIENTE CON DATOS PROPORCIONADOS</option>";
        $out2 = " <input name=\"agregarDH\" type=\"button\" value=\"Agregar Derechohabiente\" class=\"botones\" id=\"agregarDH\" onClick=\"javascript: agregarDHenCita();\" />";
    }
//	$out.= "<option value=\"fas\">" . $totalMedicos . "</option>";
    $out .= "</select>";
    return $out . $out2;
}

function buscarDHxCedulaParaBusqueda($cedula) {
    $dhs = getDH("WHERE cedula like '%" . $cedula . "%'", "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE CEDULA QUE CONTENGA " . strtoupper($cedula) . "</option>";
        $out2 = "";
    }
    $out .= "</select>";
    return $out . $out2;
}

function buscarDHxNombreParaBusqueda($ap_p, $ap_m, $nombre) {
    $temp = "";
    if (strlen($ap_p) > 0) {
        $temp .= "ap_p like '%" . $ap_p . "%'";
    }
    if (strlen($ap_m) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR ap_m like '%" . $ap_m . "%'";
        } else {
            $temp .= "ap_m like '%" . $ap_m . "%'";
        }
    }
    if (strlen($nombre) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR nombres like '%" . $nombre . "%'";
        } else {
            $temp .= "nombres like '%" . $nombre . "%'";
        }
    }
    $dhs = getDH("WHERE " . $temp, "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE PACIENTE CON DATOS PROPORCIONADOS</option>";
        $out2 = "";
    }
    $out .= "</select>";
    return $out . $out2;
}

function ejecutarSQL($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query = mysql_query($query_query, $bdissste); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $error;
}

function ejecutarSQLR($query_query) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query = mysql_query($query_query, $bdissste); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $error;
}

function existeDuplica($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query = mysql_query($query_query, $bdissste) ;//or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUsuarioXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM usuarios WHERE id_usuario='" . $id . "' AND st='1' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
	$ret=array();
    if ($totalRows_query > 0) {
        $ret = /*array(
            'id_usuario' => $row_query['id_usuario'],
            'login' => $row_query['login'],
            'pass' => $row_query['pass'],
            'nombre' => $row_query['nombre'],
            'tipo_usuario' => $row_query['tipo_usuario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'status' => $row_query['status']
        )*/$row_query;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServicioXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM servicios WHERE id_servicio='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
	$ret=array();
    if ($totalRows_query > 0) {
        $ret = $row_query['nombre'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function compararFechas($hoy, $otra) { // formato yyyymmdd
    $hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
    $otraMK = mktime(0, 0, 0, substr($otra, 4, 2), substr($otra, 6, 2), substr($otra, 0, 4));
    if ($otraMK >= $hoyMK)
        return true; else
        return false;
}

function quitarAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "&") {
            $temp = substr($Text, $j, 8);
            switch ($temp) {
                case "&aacute;": $cadena .= "(/a)";
                    $j = $j + 7;
                    break;
                case "&Aacute;": $cadena .= "(/A)";
                    $j = $j + 7;
                    break;
                case "&eacute;": $cadena .= "(/e)";
                    $j = $j + 7;
                    break;
                case "&Eacute;": $cadena .= "(/E)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/i)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/I)";
                    $j = $j + 7;
                    break;
                case "&oacute;": $cadena .= "(/o)";
                    $j = $j + 7;
                    break;
                case "&Oacute;": $cadena .= "(/O)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/u)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/U)";
                    $j = $j + 7;
                    break;
                case "&ntilde;": $cadena .= "(/n)";
                    $j = $j + 7;
                    break;
                case "&Ntilde;": $cadena .= "(/N)";
                    $j = $j + 7;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            switch ($cara) {
                case "�": $cadena.="(/a)";
                    break;
                case "�": $cadena.="(/e)";
                    break;
                case "�": $cadena.="(/i)";
                    break;
                case "�": $cadena.="(/o)";
                    break;
                case "�": $cadena.="(/u)";
                    break;
                case "�": $cadena.="(/A)";
                    break;
                case "�": $cadena.="(/E)";
                    break;
                case "�": $cadena.="(/I)";
                    break;
                case "�": $cadena.="(/O)";
                    break;
                case "�": $cadena.="(/U)";
                    break;
                case "�": $cadena.="(/n)";
                    break;
                case "�": $cadena.="(/N)";
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        }
    }
    return $cadena;
}

function ponerAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "(") {
            $temp = substr($Text, $j, 4);
            switch ($temp) {
                case "(/a)": $cadena .= "&aacute;";
                    $j = $j + 3;
                    break;
                case "(/A)": $cadena .= "&Aacute;";
                    $j = $j + 3;
                    break;
                case "(/e)": $cadena .= "&eacute;";
                    $j = $j + 3;
                    break;
                case "(/E)": $cadena .= "&Eacute;";
                    $j = $j + 3;
                    break;
                case "(/i)": $cadena .= "&iacute;";
                    $j = $j + 3;
                    break;
                case "(/I)": $cadena .= "&Iacute;";
                    $j = $j + 3;
                    break;
                case "(/o)": $cadena .= "&oacute;";
                    $j = $j + 3;
                    break;
                case "(/O)": $cadena .= "&Oacute;";
                    $j = $j + 3;
                    break;
                case "(/u)": $cadena .= "&uacute;";
                    $j = $j + 3;
                    break;
                case "(/U)": $cadena .= "&Uacute;";
                    $j = $j + 3;
                    break;
                case "(/n)": $cadena .= "&ntilde;";
                    $j = $j + 3;
                    break;
                case "(/N)": $cadena .= "&Ntilde;";
                    $j = $j + 3;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            $cadena.=$Text[$j];
        }
    }
    return $cadena;
}

function tipoDH($municipio) {
    $ret = "X";
    if (($municipio == "Guadalajara") || ($municipio == "Tlajomulco de Z(/u)(/n)iga") || ($municipio == "Tonal(/a)") || ($municipio == "Zapopan") || ($municipio == "Tlaquepaque"))
        $ret = "&nbsp;";
    return $ret;
}

function queSexoTipoCedula($tipoCedula) {
    $ret = "";
    if (($tipoCedula == "10") || ($tipoCedula == "40") || ($tipoCedula == "41") || ($tipoCedula == "50") || ($tipoCedula == "51") || ($tipoCedula == "70") || ($tipoCedula == "71") || ($tipoCedula == "90") || ($tipoCedula == "92"))
        $ret = "M"; else
        $ret = "F";
    return $ret;
}

// ---------------------------------------------------------------------------------------------  N U E V A S    F U N C I O N E S  -------------
function getDatosDerecho($id_derecho) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM derechohabientes WHERE id_derecho='" . $id_derecho . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'cedula' => $row_query['cedula'],
            'cedula_tipo' => $row_query['cedula_tipo'],
            'ap_p' => $row_query['ap_p'],
            'ap_m' => $row_query['ap_m'],
            'nombres' => $row_query['nombres'],
            'fecha_nacimiento' => $row_query['fecha_nacimiento'],
            'telefono' => $row_query['telefono'],
            'direccion' => $row_query['direccion'],
            'estado' => $row_query['estado'],
            'municipio' => $row_query['municipio'],
            'status' => $row_query['status'],
            'unidad_medica' => $row_query['unidad_medica']
        );
    } else {
        $ret = array(
            'cedula' => "-1",
            'cedula_tipo' => "-1",
            'ap_p' => "-1",
            'ap_m' => "-1",
            'nombres' => "-1",
            'telefono' => "-1",
            'fecha_nacimiento' => "-1",
            'direccion' => "-1",
            'estado' => "-1",
            'municipio' => "-1",
            'status' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicoXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM medicos WHERE id_medico='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
	$ret=array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_medico' => $row_query['id_medico'],
            'cedula' => $row_query['cedula'],
            'cedula_tipo' => $row_query['cedula_tipo'],
            'n_empleado' => $row_query['n_empleado'],
            'ced_pro' => $row_query['cedula_profesional'],
            'titulo' => $row_query['titulo'],
            'ap_p' => $row_query['ap_p'],
            'ap_m' => $row_query['ap_m'],
            'nombres' => $row_query['nombres'],
            'turno' => $row_query['turno'],
            'telefono' => $row_query['telefono'],
            'direccion' => $row_query['direccion'],
            'tipo_medico' => $row_query['tipo_medico'],
            'id_servicio1' => $row_query['id_servicio1'],
            'id_servicio2' => $row_query['id_servicio2'],
            'hora_entrada' => $row_query['hora_entrada'],
            'hora_salida' => $row_query['hora_salida'],
            'intervalo_citas0' => $row_query['intervalo_citas0'],
            'intervalo_citas1' => $row_query['intervalo_citas1'],
            'intervalo_citas2' => $row_query['intervalo_citas2'],
            'observaciones' => $row_query['observaciones'],
            'st' => $row_query['st']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDatosReceta($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_receta='" . $id_receta . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'n_serie' => $row_query['n_serie'],
            'codigo_barras' => $row_query['codigo_barras'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_captura' => $row_query['fecha_captura'],
            'fecha_expedicion' => $row_query['fecha_expedicion'],
            'entidad' => $row_query['entidad'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDatosRecetaXserie($n_serie) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE n_serie='" . $n_serie . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'n_serie' => $row_query['n_serie'],
            'codigo_barras' => $row_query['codigo_barras'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_captura' => $row_query['fecha_captura'],
            'fecha_expedicion' => $row_query['fecha_expedicion'],
            'entidad' => $row_query['entidad'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function formatoDia($fecha, $paraDonde) {
    $dia = substr($fecha, 6, 2);
    $mes = substr($fecha, 4, 2);
    $ano = substr($fecha, 0, 4);
    $diaSem = date("N", mktime(0, 0, 0, $mes, $dia, $ano));
    if ($paraDonde == 'tituloCitasXdia') {
        $fechaO = diaSemana($diaSem) . " " . $dia . " DE " . strtoupper(tituloMes($mes)) . " DE " . $ano;
    }
    if ($paraDonde == 'imprimirCita') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    if ($paraDonde == 'fecha') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    return $fechaO;
}

function getRecetaRecienAgregada($n_serie) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE n_serie='" . $n_serie . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = $row_query['id_receta'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosStock() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='1' ORDER BY id_receta ASC, fecha_caducidad DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'dosis' => $row_query['dosis'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosXingresar() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='4' ORDER BY id_receta ASC, fecha_caducidad DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'dosis' => $row_query['dosis'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosStockFechas($fechaI, $fechaF) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE fecha_ingreso>='" . $fechaI . "' AND fecha_ingreso<='" . $fechaF . "' ORDER BY fecha_ingreso ASC, id_receta ASC, fecha_caducidad DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'dosis' => $row_query['dosis'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function datosMedicamentoI($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='0' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'codigo_barras' => $row_query['codigo_barras'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function datosMedicamentoRUI($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='5' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'codigo_barras' => $row_query['codigo_barras'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getContenido($idMed) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $idMed . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = $row_query['contenido'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosRemanentes() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT DISTINCT id_ingreso FROM medicamentos_remanentes WHERE status = '1' ORDER BY fecha_aplicacion ASC, hora_aplicacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query2 = "SELECT * FROM medicamentos_remanentes WHERE id_ingreso = '" . $row_query['id_ingreso'] . "' ORDER BY id_movimiento DESC";
            $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_movimiento' => $row_query2['id_movimiento'],
                'id_ingreso' => $row_query2['id_ingreso'],
                'cantidad_remanente' => $row_query2['cantidad_remanente'],
                'fecha_aplicacion' => $row_query2['fecha_aplicacion'],
                'hora_aplicacion' => $row_query2['hora_aplicacion'],
                'status' => $row_query2['status'],
                'extra1' => $row_query2['extra1']
            );
            @mysql_free_result($query2);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosRemanentesXaplicar() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT DISTINCT id_ingreso FROM medicamentos_remanentes WHERE status = '5' ORDER BY fecha_aplicacion ASC, hora_aplicacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query2 = "SELECT * FROM medicamentos_remanentes WHERE id_ingreso = '" . $row_query['id_ingreso'] . "' ORDER BY id_movimiento DESC";
            $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_movimiento' => $row_query2['id_movimiento'],
                'id_ingreso' => $row_query2['id_ingreso'],
                'cantidad_remanente' => $row_query2['cantidad_remanente'],
                'fecha_aplicacion' => $row_query2['fecha_aplicacion'],
                'hora_aplicacion' => $row_query2['hora_aplicacion'],
                'status' => $row_query2['status'],
                'extra1' => $row_query2['extra1']
            );
            @mysql_free_result($query2);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosCaducos() {
// CALCULO PARA SABER CUALES CADUCARON
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_bajas";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'id_receta' => $row_query['id_receta'],
                'codigo_barras' => $row_query['codigo_barras'],
                'contenido' => $row_query['contenido'],
                'fecha_caducidad' => $row_query['fecha_caducidad'],
                'fecha_ingreso' => $row_query['fecha_ingreso'],
                'hora_ingreso' => $row_query['hora_ingreso'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosEnReceta($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query = "SELECT * FROM medicamentos_inventario WHERE id_receta='" . $id_receta . "' AND id_medicamento='" . $row_query['id_medicamento'] . "'";
            $query1 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query1 = mysql_fetch_assoc($query1);
            $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
            $query2 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'dosis' => $row_query['dosis'],
                'diagnostico' => $row_query['diagnostico'],
                'etapa_clinica' => $row_query['etapa_clinica'],
                'id_ingreso' => $row_query1['id_ingreso'],
                'codigo_barras' => $row_query1['codigo_barras'],
                'contenido' => $row_query1['contenido'],
                'fecha_caducidad' => $row_query1['fecha_caducidad'],
                'fecha_ingreso' => $row_query1['fecha_ingreso'],
                'hora_ingreso' => $row_query1['hora_ingreso'],
                'clave_farmacia' => $row_query2['clave_farmacia'],
                'descripcion' => $row_query2['descripcion'],
                'presentacion' => $row_query2['presentacion'],
                'unidad' => $row_query2['unidad'],
                'tipo' => $row_query2['tipo'],
                'tiempo_cad_remanente' => $row_query2['tiempo_cad_remanente']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosEnRecetaSinSurtir($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE id_receta='" . $id_receta . "' AND status='3'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query = "SELECT * FROM medicamentos_inventario WHERE id_receta='" . $id_receta . "' AND id_medicamento='" . $row_query['id_medicamento'] . "' AND status='3'";
            $query1 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query1 = mysql_fetch_assoc($query1);
            $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
            $query2 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'dosis' => $row_query['dosis'],
                'diagnostico' => $row_query['diagnostico'],
                'etapa_clinica' => $row_query['etapa_clinica'],
                'id_ingreso' => $row_query1['id_ingreso'],
                'codigo_barras' => $row_query1['codigo_barras'],
                'contenido' => $row_query1['contenido'],
                'fecha_caducidad' => $row_query1['fecha_caducidad'],
                'fecha_ingreso' => $row_query1['fecha_ingreso'],
                'hora_ingreso' => $row_query1['hora_ingreso'],
                'clave_farmacia' => $row_query2['clave_farmacia'],
                'descripcion' => $row_query2['descripcion'],
                'presentacion' => $row_query2['presentacion'],
                'unidad' => $row_query2['unidad'],
                'tipo' => $row_query2['tipo'],
                'tiempo_cad_remanente' => $row_query2['tiempo_cad_remanente']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentoXingresar($id_ingreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE id_ingreso='" . $id_ingreso . "' AND status='4' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $row_query['id_receta'] . "' AND id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
        $query1 = mysql_query($query_query, $bdissste) or die(mysql_error());
        $row_query1 = mysql_fetch_assoc($query1);
        $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
        $query2 = mysql_query($query_query, $bdissste) or die(mysql_error());
        $row_query2 = mysql_fetch_assoc($query2);
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'id_medicamento' => $row_query['id_medicamento'],
            'dosis' => $row_query['dosis'],
            'id_ingreso' => $row_query['id_ingreso'],
            'contenido' => $row_query['contenido'],
            'fecha_caducidad' => $row_query['fecha_caducidad'],
            'fecha_ingreso' => $row_query['fecha_ingreso'],
            'hora_ingreso' => $row_query['hora_ingreso'],
            'diagnostico' => $row_query1['diagnostico'],
            'etapa_clinica' => $row_query1['etapa_clinica'],
            'clave_farmacia' => $row_query2['clave_farmacia'],
            'descripcion' => $row_query2['descripcion'],
            'presentacion' => $row_query2['presentacion'],
            'unidad' => $row_query2['unidad'],
            'tipo' => $row_query2['tipo'],
            'tiempo_cad_remanente' => $row_query2['tiempo_cad_remanente']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function listaRecetasXaplicar() {
    $recetas = getRecetas(1); // 1 es sin aplicar
    $totalRecetas = count($recetas);
    $out = "";
    for ($i = 0; $i < $totalRecetas; $i++) {
        $paciente = getDatosDerecho($recetas[$i]['id_derecho']);
        $medico = getMedicoXid($recetas[$i]['id_medico']);
        $medicamentos = getMedicamentosEnReceta($recetas[$i]['id_receta']);
        $totalMedicamentos = count($medicamentos);
        $out .= "<div><a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('R" . $recetas[$i]['id_receta'] . "');\" onmouseout=\"javascript: ocultarDiv('R" . $recetas[$i]['id_receta'] . "');\">&nbsp; - RECETA " . $recetas[$i]['n_serie'] . " - " . formatoDia($recetas[$i]['fecha_captura'], "fecha") . "</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript: aplicarDosis('" . $recetas[$i]['n_serie'] . "');\" class=\"botones_menu\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Aplicar Dosis</a></div>
				 <div id=\"R" . $recetas[$i]['id_receta'] . "\" style=\"position:absolute; margin-left:300px; margin-top:-13px; display:none;\">
				 	<table width=\"400\" class=\"ventanaConFondo\">
						<tr><td class=\"textoAzulN\" align=\"left\">" . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
						<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($recetas[$i]['id_servicio']) . " - 
						" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
        for ($j = 0; $j < $totalMedicamentos; $j++) {
            if ($medicamentos[$j]['descripcion'] != '')
                $out .= "
						<tr><td class=\"textoGrisN\" align=\"left\">" . ponerAcentos($medicamentos[$j]['descripcion'] . " - " . $medicamentos[$j]['indicaciones']) . " <br>&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"textoGris\">FECHA DE CADUCIDAD: " . formatoDia($medicamentos[$j]['fecha_caducidad'], "fecha") . "</span></td></tr>
					";
        }
        $out .= "	</table>
				</div>";
    }
    return $out;
}

function listaMedicamentosXingresar() {
    $medicamentos = getMedicamentosXingresar();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $datosMedicamento = getDatosMedicamentos($medicamentos[$i]['id_medicamento']);
        $fecha_cad = $medicamentos[$i]['fecha_caducidad'];
        $hora_ingreso = $medicamentos[$i]['hora_ingreso'];
        $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
        $fecha_caducidad = formatoHora($hora_ingreso) . " " . formatoDia($fecha_cad, "fecha") . " - " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
        $receta = getDatosReceta($medicamentos[$i]['id_receta']);
        $paciente = getDatosDerecho($receta['id_derecho']);
        $medico = getMedicoXid($receta['id_medico']);
        $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: registrarMedicamento('" . $receta['n_serie'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\"  onmouseover=\"javascript: mostrarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Registrar Medicamento</a>
						</td></tr></table></div>
					 <div id=\"M" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
						<table width=\"400\" class=\"ventanaConFondo\">
							<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . "</td></tr>
							<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
							<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
							<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
							" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
        $out .= "	</table>
					</div>";
    }
    return $out;
}

function listaRecetasXsurtir() {
    $recetas = getRecetas(3); // 3 es sin surtir
    $totalRecetas = count($recetas);
    $out = "";
    for ($i = 0; $i < $totalRecetas; $i++) {
        $paciente = getDatosDerecho($recetas[$i]['id_derecho']);
        $medico = getMedicoXid($recetas[$i]['id_medico']);
        $medicamentos = getMedicamentosEnReceta($recetas[$i]['id_receta']);
        $totalMedicamentos = count($medicamentos);
        $out .= "<div><a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('R" . $recetas[$i]['id_receta'] . "');\" onmouseout=\"javascript: ocultarDiv('R" . $recetas[$i]['id_receta'] . "');\">&nbsp; - RECETA " . $recetas[$i]['n_serie'] . " - " . formatoDia($recetas[$i]['fecha_captura'], "fecha") . "</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"javascript: surtirReceta('" . $recetas[$i]['n_serie'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('R" . $recetas[$i]['id_receta'] . "');\" onmouseout=\"javascript: ocultarDiv('R" . $recetas[$i]['id_receta'] . "');\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Surtir Receta</a></div>
				 <div id=\"R" . $recetas[$i]['id_receta'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
				 	<table width=\"400\" class=\"ventanaConFondo\">
						<tr><td class=\"textoAzulN\" align=\"left\">" . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
						<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($recetas[$i]['id_servicio']) . " - 
						" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
        for ($j = 0; $j < $totalMedicamentos; $j++) {
            if ($medicamentos[$j]['descripcion'] != '')
                $out .= "
						<tr><td class=\"textoGrisN\" align=\"left\">" . ponerAcentos($medicamentos[$j]['descripcion']) . "</td></tr>
					";
        }
        $out .= "	</table>
				</div>";
    }
    return $out;
}

function listaMedicamentosStock() {
    $medicamentos = getMedicamentosStock();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $datosMedicamento = getDatosMedicamentos($medicamentos[$i]['id_medicamento']);
        $fecha_cad = $medicamentos[$i]['fecha_caducidad'];
        $hora_ingreso = $medicamentos[$i]['hora_ingreso'];
        $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
        $fecha_caducidad = formatoHora($hora_ingreso) . " " . formatoDia($fecha_cad, "fecha") . " - " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
        if (strtotime("now") <= strtotime($fecha_caducidad)) {
            $receta = getDatosReceta($medicamentos[$i]['id_receta']);
            $paciente = getDatosDerecho($receta['id_derecho']);
            $medico = getMedicoXid($receta['id_medico']);
            $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: aplicarDosis('" . $receta['n_serie'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\"  onmouseover=\"javascript: mostrarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Aplicar Dosis</a>
						</td></tr></table></div>
					 <div id=\"M" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
						<table width=\"400\" class=\"ventanaConFondo\">
							<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA DE CADUCIDAD: " . formatoDia($medicamentos[$i]['fecha_caducidad'], "fecha") . "</td></tr>
							<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
							<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
							<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
							" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
            $out .= "	</table>
					</div>";
        }
    }
    return $out;
}

function listaMedicamentosRemanentes() {
    $medicamentos = getMedicamentosRemanentes();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $medicamentosI = datosMedicamentoI($medicamentos[$i]['id_ingreso']);
        $datosMedicamento = getDatosMedicamentos($medicamentosI['id_medicamento']);
        if ($datosMedicamento['tipo'] == '0') { // 0 es un medicamento reutilizable ;)
            $fecha_ingreso = $medicamentosI['fecha_ingreso'];
            $hora_ingreso = $medicamentosI['hora_ingreso'];
            $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
            $tiempoIngreso = formatoHora($hora_ingreso) . " " . formatoDia($fecha_ingreso, "fecha") . " + " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
            if (strtotime("now") < strtotime($tiempoIngreso)) {
                //			echo date('H:i d/m/Y',strtotime($tiempoIngreso)) . "<br>";
                $receta = getDatosReceta($medicamentosI['id_receta']);
                $paciente = getDatosDerecho($receta['id_derecho']);
                $medico = getMedicoXid($receta['id_medico']);
                $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: reutilizarMedicamento('" . $medicamentosI['id_medicamento'] . "','" . $receta['id_receta'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Reutilizar</a>
						</td></tr></table></div>
						 <div id=\"M1" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
							<table width=\"400\" class=\"ventanaConFondo\">
								<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">CANTIDAD REMANENTE: " . $medicamentos[$i]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE APLICACION: " . formatoDia($medicamentos[$i]['fecha_aplicacion'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">HORA DE APLICACION: " . formatoHora($medicamentos[$i]['hora_aplicacion']) . "</td></tr>
								<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
								<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
								<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
								" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
                $out .= "	</table>
						</div>";
            }
        }
    }
    return $out;
}

function listaMedicamentosRemanentesXaplicar() {
    $medicamentos = getMedicamentosRemanentesXaplicar();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {

        $medicamentosI = datosMedicamentoRUI($medicamentos[$i]['id_ingreso']);
        $datosMedicamento = getDatosMedicamentos($medicamentosI['id_medicamento']);
        if ($datosMedicamento['tipo'] == '0') { // 0 es un medicamento reutilizable ;)
            $fecha_ingreso = $medicamentosI['fecha_ingreso'];
            $hora_ingreso = $medicamentosI['hora_ingreso'];
            $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
            $tiempoIngreso = formatoHora($hora_ingreso) . " " . formatoDia($fecha_ingreso, "fecha") . " + " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
            if (strtotime("now") < strtotime($tiempoIngreso)) {
                //			echo date('H:i d/m/Y',strtotime($tiempoIngreso)) . "<br>";
                $receta = getDatosReceta($medicamentosI['id_receta']);
                $paciente = getDatosDerecho($receta['id_derecho']);
                $medico = getMedicoXid($receta['id_medico']);
                $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: aplicarDosisRU('" . $medicamentosI['id_medicamento'] . "','" . $receta['n_serie'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M1" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medAdmi.gif\" border=\"0\" width=\"30\">Aplicar Dosis</a>
						</td></tr></table></div>
						 <div id=\"M1" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
							<table width=\"400\" class=\"ventanaConFondo\">
								<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">CANTIDAD REMANENTE: " . $medicamentos[$i]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE APLICACION: " . formatoDia($medicamentos[$i]['fecha_aplicacion'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">HORA DE APLICACION: " . formatoHora($medicamentos[$i]['hora_aplicacion']) . "</td></tr>
								<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
								<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
								<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
								" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
                $out .= "	</table>
						</div>";
            }
        }
    }
    return $out;
}

function listaMedicamentosRemanentesCaducos() {
    $medicamentos = getMedicamentosRemanentes();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $medicamentosI = datosMedicamentoI($medicamentos[$i]['id_ingreso']);
        $datosMedicamento = getDatosMedicamentos($medicamentosI['id_medicamento']);
        if ($datosMedicamento['tipo'] == '1') { // 1 es un medicamento NO reutilizable ;)
            $receta = getDatosReceta($medicamentosI['id_receta']);
            $paciente = getDatosDerecho($receta['id_derecho']);
            $medico = getMedicoXid($receta['id_medico']);
            $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M2" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M2" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: bajaMedicamento('" . $medicamentosI['id_medicamento'] . "','" . $receta['id_receta'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M2" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M2" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medDel.gif\" border=\"0\" width=\"30\">Dar de Baja</a>
						</td></tr></table></div>
					 <div id=\"M2" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
						<table width=\"400\" class=\"ventanaConFondo\">
							<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">CANTIDAD REMANENTE: " . $medicamentos[$i]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE APLICACION: " . formatoDia($medicamentos[$i]['fecha_aplicacion'], "fecha") . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">HORA DE APLICACION: " . formatoHora($medicamentos[$i]['hora_aplicacion']) . "</td></tr>
							<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
							<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
							<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
							" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
            $out .= "	</table>
					</div>";
        }
    }
    return $out;
}

function listaMedicamentosReutilizablesCaducos() {
    $medicamentos = getMedicamentosRemanentes();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $medicamentosI = datosMedicamentoI($medicamentos[$i]['id_ingreso']);
        $datosMedicamento = getDatosMedicamentos($medicamentosI['id_medicamento']);
        if ($datosMedicamento['tipo'] == '0') { // 0 es un medicamento reutilizable ;)
            $fecha_ingreso = $medicamentosI['fecha_ingreso'];
            $hora_ingreso = $medicamentosI['hora_ingreso'];
            $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
            $tiempoIngreso = formatoHora($hora_ingreso) . " " . formatoDia($fecha_ingreso, "fecha") . " + " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
            if (strtotime("now") >= strtotime($tiempoIngreso)) {
                //			echo date('H:i d/m/Y',strtotime($tiempoIngreso)) . "<br>";
                $receta = getDatosReceta($medicamentosI['id_receta']);
                $paciente = getDatosDerecho($receta['id_derecho']);
                $medico = getMedicoXid($receta['id_medico']);
                $out .= "<div>
							<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
							<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M3" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M3" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
							</td><td width=\"110\"><a href=\"javascript: bajaMedicamento('" . $medicamentosI['id_medicamento'] . "','" . $receta['id_receta'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M3" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M3" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medDel.gif\" border=\"0\" width=\"30\">Dar de Baja</a>
							</td></tr></table></div>
						 <div id=\"M3" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-13px; display:none;\">
							<table width=\"400\" class=\"ventanaConFondo\">
								<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">CANTIDAD REMANENTE: " . $medicamentos[$i]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentosI['fecha_ingreso'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">FECHA DE APLICACION: " . formatoDia($medicamentos[$i]['fecha_aplicacion'], "fecha") . "</td></tr>
								<tr><td class=\"textoGris\" align=\"left\">HORA DE APLICACION: " . formatoHora($medicamentos[$i]['hora_aplicacion']) . "</td></tr>
								<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
								<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
								<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
								" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
                $out .= "	</table>
						</div>";
            }
        }
    }
    return $out;
}

function listaMedicamentosCaducos() {
    $medicamentos = getMedicamentosStock();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $datosMedicamento = getDatosMedicamentos($medicamentos[$i]['id_medicamento']);
        $fecha_cad = $medicamentos[$i]['fecha_caducidad'];
        $hora_ingreso = $medicamentos[$i]['hora_ingreso'];
        $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
        $fecha_caducidad = formatoHora($hora_ingreso) . " " . formatoDia($fecha_cad, "fecha") . " - " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
        if (strtotime("now") > strtotime($fecha_caducidad)) {
            $receta = getDatosReceta($medicamentos[$i]['id_receta']);
            $paciente = getDatosDerecho($receta['id_derecho']);
            $medico = getMedicoXid($receta['id_medico']);
            $out .= "<div>
						<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>
						<a href=\"#\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M4" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M4" . $medicamentos[$i]['id_ingreso'] . "');\">&nbsp; - INGRESO " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " - " . $datosMedicamento['descripcion'] . "</a>
						</td><td width=\"110\"><a href=\"javascript: bajaMedicamentoCaduco('" . $medicamentos[$i]['id_medicamento'] . "','" . $receta['id_receta'] . "','" . $medicamentos[$i]['id_ingreso'] . "');\" class=\"botones_menu\" onmouseover=\"javascript: mostrarDiv('M4" . $medicamentos[$i]['id_ingreso'] . "');\" onmouseout=\"javascript: ocultarDiv('M4" . $medicamentos[$i]['id_ingreso'] . "');\"><img src=\"diseno/_medDel.gif\" border=\"0\" width=\"30\">Dar de Baja</a>
						</td></tr></table></div>
					 <div id=\"M4" . $medicamentos[$i]['id_ingreso'] . "\" style=\"position:absolute; margin-left:435px; margin-top:-93px; display:none;\">
						<table width=\"400\" class=\"ventanaConFondo\">
							<tr><td class=\"textoGris\" align=\"left\">PRESENTACION: " . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>
							<tr><td class=\"textoGris\" align=\"left\">FECHA DE INGRESO: " . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA DE CADUCIDAD: " . formatoDia($medicamentos[$i]['fecha_caducidad'], "fecha") . "</td></tr>
							<tr><td class=\"textoGrisN\" align=\"center\">INFORMACION DE LA RECETA</td></tr>
							<tr><td class=\"textoAzulN\" align=\"left\">RECETA " . $receta['n_serie'] . " - " . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</td></tr>
							<tr><td class=\"textoAzul\" align=\"left\">" . getServicioXid($receta['id_servicio']) . " - 
							" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
            $out .= "	</table>
					</div>";
        }
    }
    return $out;
}

function listaMedicamentosConMovimientosImp($fechaI, $fechaF) {
    $medicamentos = getMedicamentosStockFechas($fechaI, $fechaF);
    $totalMedicamentos = count($medicamentos);
    $out = "";
    for ($i = 0; $i < $totalMedicamentos; $i++) {
        $datosMedicamento = getDatosMedicamentos($medicamentos[$i]['id_medicamento']);
        $fecha_cad = $medicamentos[$i]['fecha_caducidad'];
        $hora_ingreso = $medicamentos[$i]['hora_ingreso'];
        $tiempo_cad = $datosMedicamento['tiempo_cad_remanente'];
        $fecha_caducidad = formatoHora($hora_ingreso) . " " . formatoDia($fecha_cad, "fecha") . " - " . substr($tiempo_cad, 0, 2) . " hours " . substr($tiempo_cad, 2, 2) . " minutes";
        $receta = getDatosReceta($medicamentos[$i]['id_receta']);
        $paciente = getDatosDerecho($receta['id_derecho']);
        $medico = getMedicoXid($receta['id_medico']);
        $out .= "<table border=\"2\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"3\">
					<tr><td class=\"contenido8\" colspan=\"4\">- FECHA DE INGRESO DEL MEDICAMENTO: <span class=\"contenido8bold\">" . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . "</span></td></tr>
					<tr><td class=\"contenido8\" colspan=\"3\">MEDICAMENTO: <span class=\"contenido8bold\">" . $datosMedicamento['descripcion'] . "</span></td>
						<td class=\"contenido8\" align=\"left\">PRESENTACION: <span class=\"contenido8bold\">" . $datosMedicamento['presentacion'] . " " . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</span></td></tr>
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">FECHA DE CADUCIDAD: <span class=\"contenido8bold\">" . formatoDia($medicamentos[$i]['fecha_caducidad'], "fecha") . "<span</td></tr>
					<tr><td class=\"contenido8bold\" align=\"center\" colspan=\"4\">INFORMACION DE LA RECETA</td></tr>
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">RECETA No: <span class=\"contenido8bold\">" . $receta['n_serie'] . "</td></tr>
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">PACIENTE: <span class=\"contenido8bold\">" . ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']) . "</span></td></tr>
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">SERVICIO: <span class=\"contenido8bold\">" . getServicioXid($receta['id_servicio']) . "</span></td></tr> 
					<tr><td class=\"contenido8\" align=\"left\" colspan=\"4\">MEDICO: <span class=\"contenido8bold\">" . ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</span></td></tr>
					<tr><td class=\"contenido8bold\" align=\"center\" colspan=\"4\">MOVIMIENTOS</td></tr>
					<tr><td class=\"contenido8\" align=\"right\" width=\"20%\">INGRESO: </td><td class=\"contenido8bold\" align=\"left\">" . formatoDia($medicamentos[$i]['fecha_ingreso'], "fecha") . " A LAS " . formatoHora($medicamentos[$i]['hora_ingreso']) . " HRS</td><td class=\"contenido8\" align=\"right\">CONTENIDO: </td><td align=\"left\" class=\"contenido8bold\">" . $datosMedicamento['contenido'] . " " . $datosMedicamento['unidad'] . "</td></tr>";
        $medicamentoAplicado = getMedicamentoRemanente($medicamentos[$i]['id_ingreso']);
        $cAplicado = count($medicamentoAplicado);
        if ($cAplicado > 0) {
            for ($j = 0; $j < $cAplicado; $j++)
                $out .= "	<tr><td class=\"contenido8\" align=\"right\">APLICACION: </td><td class=\"contenido8bold\" align=\"left\">" . formatoDia($medicamentoAplicado[$j]['fecha_aplicacion'], "fecha") . " A LAS " . formatoHora($medicamentoAplicado[$j]['hora_aplicacion']) . " HRS</td><td class=\"contenido8\" align=\"right\">CANTIDAD REMANTE: </td><td align=\"left\" class=\"contenido8bold\">" . $medicamentoAplicado[$j]['cantidad_remanente'] . " " . $datosMedicamento['unidad'] . "</td></tr>";
        }
        $medicamentoBaja = getMedicamentoBaja($medicamentos[$i]['id_ingreso']);
        $cBaja = count($medicamentoBaja);
        if ($cBaja > 0) {
            for ($j = 0; $j < $cBaja; $j++)
                $out .= "	<tr><td class=\"contenido8\" align=\"right\">BAJA DEL MEDICAMENTO: </td><td class=\"contenido8bold\" align=\"left\">" . formatoDia($medicamentoBaja[$j]['fecha_baja'], "fecha") . " A LAS " . formatoHora($medicamentoBaja[$j]['hora_baja']) . " HRS</td><td class=\"contenido8\" align=\"right\">CANTIDAD BAJA: </td><td align=\"left\" class=\"contenido8bold\">" . $medicamentoBaja[$j]['cantidad_baja'] . " " . $datosMedicamento['unidad'] . "</td></tr>";
        }
        $out .= "</table></td></tr></table>";
    }
    return $out;
}

function getServicios() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT* FROM servicios ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_servicio' => $row_query['id_servicio'],
                'clave' => $row_query['clave'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicos() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT* FROM medicos ORDER BY ap_p ASC, ap_m ASC, nombres ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medico' => $row_query['id_medico'],
                'titulo' => $row_query['titulo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDiagnosticos() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM diagnosticos ORDER BY descripcion";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_diagnostico' => $row_query['id_diagnostico'],
                'descripcion' => $row_query['descripcion']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function listaServicios() {
    $servicios = getServicios();
    $totalServicios = count($servicios);
    $out = "";
    $out.= "<option selected value=\"0\"> </option>";
    if ($totalServicios > 0) {
        for ($i = 0; $i < $totalServicios; $i++) {
            $out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
        }
    }
    return $out;
}

function listaMedicos() {
    $medicos = getMedicos();
    $totalMedicos = count($medicos);
    $out = "";
    $out.= "<option selected value=\"0\"> </option>";
    if ($totalMedicos > 0) {
        for ($i = 0; $i < $totalMedicos; $i++) {
            $out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
        }
    }
    return $out;
}

function listaMedicamentos() {
    $medicamentos = getMedicamentos();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    $out.= "<option selected value=\"0\"> </option>";
    if ($totalMedicamentos > 0) {
        for ($i = 0; $i < $totalMedicamentos; $i++) {
            $out.= "<option value=\"" . $medicamentos[$i]['clave_farmacia'] . "\">" . $medicamentos[$i]['clave_farmacia'] . " - " . ponerAcentos($medicamentos[$i]['descripcion']) . "</option>";
        }
    }
    return $out;
}

function listaDiagnosticos() {
    $medicamentos = getDiagnosticos();
    $totalMedicamentos = count($medicamentos);
    $out = "";
    $out.= "<option selected value=\"0\"> </option>";
    if ($totalMedicamentos > 0) {
        for ($i = 0; $i < $totalMedicamentos; $i++) {
            $out.= "<option value=\"" . $medicamentos[$i]['id_diagnostico'] . "\">" . ponerAcentos($medicamentos[$i]['descripcion']) . "</option>";
        }
    }
    return $out;
}

function formatoFechaBD($fecha) {
    $dia = substr($fecha, 0, 2);
    $mes = substr($fecha, 3, 2);
    $ano = substr($fecha, 6, 4);
    $fechaO = $ano . $mes . $dia;
    return $fechaO;
}

function datosMedicamentoAaplicar($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='1' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $datosMed = getDatosMedicamentos($row_query['id_medicamento']);
        $datosMedR = getDatosMedicamentoEnReceta($row_query['id_receta'], $row_query['id_medicamento']);
        $ret = array(
            'id_ingreso' => $row_query['id_ingreso'],
            'id_medicamento' => $row_query['id_medicamento'],
            'id_receta' => $row_query['id_receta'],
            'dosis' => $row_query['dosis'],
            'contenido' => $row_query['contenido'],
            'fecha_caducidad' => $row_query['fecha_caducidad'],
            'fecha_ingreso' => $row_query['fecha_ingreso'],
            'hora_ingreso' => $row_query['hora_ingreso'],
            'extra1' => $row_query['extra1'],
            'clave_farmacia' => $datosMed['clave_farmacia'],
            'descripcion' => $datosMed['descripcion'],
            'unidad' => $datosMed['unidad'],
            'diagnostico' => $datosMedR['diagnostico'],
            'etapa_clinica' => $datosMedR['etapa_clinica']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function datosMedicamentoRUaAplicar($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='5' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $datosMed = getDatosMedicamentos($row_query['id_medicamento']);
        $datosMedR = getDatosMedicamentoEnReceta($row_query['id_receta'], $row_query['id_medicamento']);
        $ret = array(
            'id_ingreso' => $row_query['id_ingreso'],
            'id_medicamento' => $row_query['id_medicamento'],
            'id_receta' => $row_query['id_receta'],
            'dosis' => $row_query['dosis'],
            'contenido' => $row_query['contenido'],
            'fecha_caducidad' => $row_query['fecha_caducidad'],
            'fecha_ingreso' => $row_query['fecha_ingreso'],
            'hora_ingreso' => $row_query['hora_ingreso'],
            'extra1' => $row_query['extra1'],
            'clave_farmacia' => $datosMed['clave_farmacia'],
            'descripcion' => $datosMed['descripcion'],
            'unidad' => $datosMed['unidad'],
            'diagnostico' => $datosMedR['diagnostico'],
            'etapa_clinica' => $datosMedR['etapa_clinica']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function datosMedicamentoRemanente($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE status='0' AND id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $datosMed = getDatosMedicamentos($row_query['id_medicamento']);
        $datosMedR = getDatosMedicamentoEnReceta($row_query['id_receta'], $row_query['id_medicamento']);
        $datosMedRem =
                $ret = array(
            'id_ingreso' => $row_query['id_ingreso'],
            'id_medicamento' => $row_query['id_medicamento'],
            'id_receta' => $row_query['id_receta'],
            'dosis' => $row_query['dosis'],
            'contenido' => $row_query['contenido'],
            'fecha_caducidad' => $row_query['fecha_caducidad'],
            'fecha_ingreso' => $row_query['fecha_ingreso'],
            'hora_ingreso' => $row_query['hora_ingreso'],
            'extra1' => $row_query['extra1'],
            'clave_farmacia' => $datosMed['clave_farmacia'],
            'descripcion' => $datosMed['descripcion'],
            'unidad' => $datosMed['unidad'],
            'diagnostico' => $datosMedR['diagnostico'],
            'cantidad_remanente' => getContenidoRemanente($idIngreso),
            'etapa_clinica' => $datosMedR['etapa_clinica']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getContenidoRemanente($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_remanentes WHERE status='1' AND id_ingreso='" . $idIngreso . "' ORDER BY id_movimiento DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $datosMed = getDatosMedicamentos($row_query['id_medicamento']);
        $datosMedR = getDatosMedicamentoEnReceta($row_query['id_receta'], $row_query['id_medicamento']);
        $ret = $row_query['cantidad_remanente'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentoRemanente($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_remanentes WHERE id_ingreso = '" . $idIngreso . "' ORDER BY id_movimiento ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_movimiento' => $row_query['id_movimiento'],
                'id_ingreso' => $row_query['id_ingreso'],
                'cantidad_remanente' => $row_query['cantidad_remanente'],
                'fecha_aplicacion' => $row_query['fecha_aplicacion'],
                'hora_aplicacion' => $row_query['hora_aplicacion'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentoBaja($idIngreso) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_bajas WHERE id_ingreso='" . $idIngreso . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_ingreso' => $row_query['id_ingreso'],
                'id_medicamento' => $row_query['id_medicamento'],
                'cantidad_baja' => $row_query['cantidad_baja'],
                'fecha_baja' => $row_query['fecha_baja'],
                'hora_baja' => $row_query['hora_baja'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicosXServicio($idSer) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
//	$idMedico =  regresarIdMedico($idConsultorio,$idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT x.id_consultorio, x.id_servicio, x.id_medico, m.id_medico, m.titulo, m.nombres, m.ap_p, m.ap_m FROM servicios_x_consultorio x, medicos m WHERE x.id_servicio='" . $idSer . "' AND x.id_medico=m.id_medico AND m.st='1' ORDER BY m.ap_p ASC, m.ap_m ASC, m.nombres ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medico' => $row_query['id_medico'],
                'titulo' => $row_query['titulo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentoXclave($clave) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos WHERE clave_farmacia='" . $clave . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_medicamento' => $row_query['id_medicamento'],
            'clave_farmacia' => $row_query['clave_farmacia'],
            'codigo_barras' => $row_query['codigo_barras'],
            'descripcion' => $row_query['descripcion'],
            'presentacion' => $row_query['presentacion'],
            'contenido' => $row_query['contenido'],
            'unidad' => $row_query['unidad'],
            'tipo' => $row_query['tipo'],
            'tiempo_cad_remanente' => $row_query['tiempo_cad_remanente'],
            'status' => $row_query['status']
        );
    } else {
        $ret = array(
            'id_medicamento' => "",
            'clave_farmacia' => "",
            'codigo_barras' => "",
            'descripcion' => "",
            'presentacion' => "",
            'contenido' => "",
            'unidad' => "",
            'tipo' => "",
            'tiempo_cad_remanente' => "",
            'status' => ""
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getNMedicamentosEnReceta($n_serie) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE n_serie='" . $n_serie . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $id_receta = $row_query['id_receta'];
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    return $totalRows_query;
}

function medicamentoRecienInventariado($id_medicamento, $id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_inventario WHERE id_medicamento='" . $id_medicamento . "' AND id_receta='" . $id_receta . "';";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $id_ingreso = $row_query['id_ingreso'];
    return $id_ingreso;
}

function getMedicamentosXdiagnostico($id_diagnostico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentosXdiagnostico WHERE id_diagnostico='" . $id_diagnostico . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
            $query2 = mysql_query($query_query, $bdissste) or die(mysql_error());
            $row_query2 = mysql_fetch_assoc($query2);
            $ret[] = array(
                'id_medicamento' => $row_query['id_medicamento'],
                'clave_farmacia' => $row_query2['clave_farmacia'],
                'descripcion' => $row_query2['descripcion'],
                'presentacion' => $row_query2['presentacion'],
                'unidad' => $row_query2['unidad'],
                'tipo' => $row_query2['tipo'],
                'tiempo_cad_remanente' => $row_query2['tiempo_cad_remanente']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDiagnosticoXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM diagnosticos WHERE id_diagnostico='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = ponerAcentos($row_query['descripcion']);
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasXdia($fechaCitas, $idMedico) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $queDia = date("N", mktime(0, 0, 0, substr($fechaCitas, 4, 2), substr($fechaCitas, 6, 2), substr($fechaCitas, 0, 4)));
    $query_query = "SELECT * FROM horarios WHERE id_medico='" . $idMedico . "' and dia='" . diaSemana($queDia) . "' ORDER BY hora_inicio";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_horario' => $row_query['id_horario'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'id_servicio' => $row_query['id_servicio']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasOcupadasXdia($idHorario, $fechaCita) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $idHorario . "' and fecha_cita='" . $fechaCita . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_derecho' => $row_query['id_derecho'],
            'id_cita' => $row_query['id_cita'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1'],
            'id_usuario' => $row_query['id_usuario'],
            'observaciones' => $row_query['observaciones'],
			'tipo_usuario'=>$row_query['tipo_usuario']
        );
    } else {
        $ret = array(
            'id_derecho' => "-1",
            'id_cita' => "-1",
            'status' => "-1",
            'extra1' => "-1",
            'id_usuario' => "-1",
            'observaciones' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitasExtemporaneas($fechaCitas, $idMedico) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_medico='" . $idMedico . "' AND fecha_cita='" . $fechaCitas . "' ORDER BY hora_inicio ASC, hora_fin ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_cita'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'status' => $row_query['status'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUsuarioCitaXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM usuarios WHERE id_usuario='" . $id . "' AND st='1' ORDER BY nombre LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret=array();
    if ($totalRows_query > 0) {
        $ret = /*array(
            'id_usuario' => $row_query['id_usuario'],
            'login' => $row_query['login'],
            'pass' => $row_query['pass'],
            'nombre' => $row_query['nombre'],
            'tipo_usuario' => $row_query['tipo_usuario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'status' => $row_query['status']
        );*/$row_query;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getConsultorios() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM consultorios WHERE st='1' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_consultorio' => $row_query['id_consultorio'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServiciosXConsultorio($idCon) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $idMedico = regresarIdMedico($idConsultorio, $idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT DISTINCT x.id_servicio, s.id_servicio, s.nombre, s.clave FROM servicios_x_consultorio x, servicios s WHERE x.id_consultorio='" . $idCon . "' AND x.id_servicio=s.id_servicio ORDER BY s.nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_servicio' => $row_query['id_servicio'],
                'clave' => $row_query['clave'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicosXServicioXConsultorio($idCon, $idSer) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $idMedico = regresarIdMedico($idConsultorio, $idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT x.id_consultorio, x.id_servicio, x.id_medico, m.id_medico, m.titulo, m.nombres, m.ap_p, m.ap_m FROM servicios_x_consultorio x, medicos m WHERE x.id_consultorio='" . $idCon . "' AND x.id_servicio='" . $idSer . "' AND x.id_medico=m.id_medico AND m.st='1' ORDER BY m.ap_p ASC, m.ap_m ASC, m.nombres ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medico' => $row_query['id_medico'],
                'titulo' => $row_query['titulo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function opcionesCon($idCon) {
    $consultorios = getConsultorios();
    $totalConsultorios = count($consultorios);
    $out = "";
    if ($totalConsultorios > 0) {
        if ($idCon == "-1") { // que no se ha seleccionado ningun consultorio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalConsultorios; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['IdCon'] = $consultorios[$i]['id_consultorio'];
                } else {
                    $out.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalConsultorios; $i++) {
                if ($idCon == $consultorios[$i]['id_consultorio']) {
                    $out.= "<option selected value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                    $_SESSION['IdCon'] = $consultorios[$i]['id_consultorio'];
                } else {
                    $out.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN CONSULTORIOS</option>";
    }
    return $out;
}

function opcionesSer($idCon, $idSer) {
    $servicios = getServiciosXConsultorio($idCon);
    $totalServicios = count($servicios);
    $out = "";
    if ($totalServicios > 0) {
        if ($idSer == "-1") { // que no se ha seleccionado ningun servicio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalServicios; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['idServ'] = $servicios[$i]['id_servicio'];
                } else {
                    $out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalServicios; $i++) {
                if ($idSer == $servicios[$i]['id_servicio']) {
                    $out.= "<option selected value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                    $_SESSION['idServ'] = $servicios[$i]['id_servicio'];
                } else {
                    $out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN SERVICIOS</option>";
    }
    return $out;
}

function opcionesMed($idCon, $idSer, $idMed) {
    $medicos = getMedicosXServicioXConsultorio($idCon, $idSer);
    $totalMedicos = count($medicos);
    $out = "";
    if ($totalMedicos > 0) {
        if ($idMed == "-1") { // que no se ha seleccionado ningun servicio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalMedicos; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['idDr'] = $medicos[$i]['id_medico'];
                } else {
                    $out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalMedicos; $i++) {
                if ($idMed == $medicos[$i]['id_medico']) {
                    $out.= "<option selected value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                    $_SESSION['idDr'] = $medicos[$i]['id_medico'];
                } else {
                    $out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN MEDICOS</option>";
    }
    return $out;
}

function getFolios($id_medico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM folios WHERE id_medico='" . $id_medico . "' ORDER BY fecha_asignacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_folio' => $row_query['id_folio'],
                'id_medico' => $row_query['id_medico'],
                'serie' => $row_query['serie'],
                'folio_inicial' => $row_query['folio_inicial'],
                'folio_final' => $row_query['folio_final'],
                'folio_actual' => $row_query['folio_actual'],
                'fecha_asignacion' => $row_query['fecha_asignacion']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getSeries() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM series ORDER BY serie ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_serie' => $row_query['id_serie'],
                'serie' => $row_query['serie']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getFoliosXserie($serie) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM folios WHERE serie='" . $serie . "' ORDER BY fecha_asignacion ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_folio' => $row_query['id_folio'],
                'id_medico' => $row_query['id_medico'],
                'serie' => $row_query['serie'],
                'folio_inicial' => $row_query['folio_inicial'],
                'folio_final' => $row_query['folio_final'],
                'folio_actual' => $row_query['folio_actual'],
                'fecha_asignacion' => $row_query['fecha_asignacion']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getFolioActual($id_medico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM folios WHERE id_medico='" . $id_medico . "' ORDER BY fecha_asignacion DESC LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) { // si ya estan dados de alta sus folios
        $ret["id_folio"] = $row_query['id_folio'];
        $ret["serie"] = $row_query['serie'];
        $ret["folio_inicial"] = intval($row_query['folio_inicial']);
        $ret["folio_final"] = intval($row_query['folio_final']);
        $ret["folio_actual"] = intval($row_query['folio_actual']);
    } else { // si no estan dados de alta sus  folios (medico nuevo)
        $query_query = "INSERT INTO folios VALUES(NULL,'" . $id_medico . "','" . SERIE . "','" . FOLIO_INICIAL . "','" . FOLIO_FINAL . "','" . FOLIO_ACTUAL . "','" . date('Ymd') . "');";
        $query = mysql_query($query_query, $bdissste) or die(mysql_error());
        $query_query = "SELECT * FROM folios WHERE id_medico='" . $id_medico . "' ORDER BY fecha_asignacion DESC LIMIT 1";
        $query = mysql_query($query_query, $bdissste) or die(mysql_error());
        $row_query = mysql_fetch_assoc($query);
        $ret["id_folio"] = $row_query['id_folio'];
        $ret["serie"] = $row_query['serie'];
        $ret["folio_inicial"] = intval($row_query['folio_inicial']);
        $ret["folio_final"] = intval($row_query['folio_final']);
        $ret["folio_actual"] = intval($row_query['folio_actual']);
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function ponerCeros($cant, $cuantos) {
    $aCeros = array("", "0", "00", "000", "0000", "00000", "000000", "0000000", "00000000", "000000000", "0000000000", "00000000000", "000000000000", "0000000000000", "00000000000000", "000000000000000");
    $tCant = strlen($cant);
    $cuantos = $cuantos - $tCant;
    return $aCeros[$cuantos] . $cant;
}

function getRecetaXfolio($folio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE folio='" . $folio . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'serie' => $row_query['serie'],
            'folio' => $row_query['folio'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha' => $row_query['fecha'],
            'hora' => $row_query['hora'],
            'codigo_barras' => $row_query['codigo_barras'],
            'entidad_federativa' => $row_query['entidad_federativa'],
            'clave_unidad_medica' => $row_query['clave_unidad_medica'],
            'numero_vale' => $row_query['numero_vale'],
            'dgp' => $row_query['dgp'],
            'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
            'id_usuario_surtio' => $row_query['id_usuario_surtio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetaXCodigoBarrasActivas($cod_bar) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE codigo_barras='" . $cod_bar . "' AND status!='0' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query2 > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'serie' => $row_query['serie'],
            'folio' => $row_query['folio'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha' => $row_query['fecha'],
            'hora' => $row_query['hora'],
            'codigo_barras' => $row_query['codigo_barras'],
            'entidad_federativa' => $row_query['entidad_federativa'],
            'clave_unidad_medica' => $row_query['clave_unidad_medica'],
            'numero_vale' => $row_query['numero_vale'],
            'dgp' => $row_query['dgp'],
            'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
            'id_usuario_surtio' => $row_query['id_usuario_surtio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetaXCodigoBarras($cod_bar) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE codigo_barras='" . $cod_bar . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query2 > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'serie' => $row_query['serie'],
            'folio' => $row_query['folio'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha' => $row_query['fecha'],
            'hora' => $row_query['hora'],
            'codigo_barras' => $row_query['codigo_barras'],
            'entidad_federativa' => $row_query['entidad_federativa'],
            'clave_unidad_medica' => $row_query['clave_unidad_medica'],
            'numero_vale' => $row_query['numero_vale'],
            'dgp' => $row_query['dgp'],
            'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
            'id_usuario_surtio' => $row_query['id_usuario_surtio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetaXid($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_receta='" . $id_receta . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query2 > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'serie' => $row_query['serie'],
            'folio' => $row_query['folio'],
            'id_medico' => $row_query['id_medico'],
            'id_servicio' => $row_query['id_servicio'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha' => $row_query['fecha'],
            'hora' => $row_query['hora'],
            'codigo_barras' => $row_query['codigo_barras'],
            'entidad_federativa' => $row_query['entidad_federativa'],
            'clave_unidad_medica' => $row_query['clave_unidad_medica'],
            'numero_vale' => $row_query['numero_vale'],
            'dgp' => $row_query['dgp'],
            'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
            'id_usuario_surtio' => $row_query['id_usuario_surtio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function regresarIdConsultorio($idDr) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT id_consultorio FROM servicios_x_consultorio WHERE id_medico='" . $idDr . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $id_servicio = $row_query['id_consultorio'];
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $id_servicio;
}

function regresarIdServicio($idDr) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT id_servicio FROM servicios_x_consultorio WHERE id_medico='" . $idDr . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $id_servicio = $row_query['id_servicio'];
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $id_servicio;
}

function getExistenciasMedicamentos($id_medicamento) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_existencias WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_medicamento' => $row_query['id_medicamento'],
            'existencia_anterior' => $row_query['existencia_anterior'],
            'existencia_actual' => $row_query['existencia_actual']
        );
    } else
        $ret["id_medicamento"] = 0;
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function restaExistenciasMedicamentos($id_medicamento, $cantidad) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_existencias WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $existencia_actual = (int) $row_query["existencia_actual"];
    $resta = $existencia_actual - (int) $cantidad;
    $query_query = "UPDATE medicamentos_existencias SET existencia_actual='" . $resta . "' WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    @mysql_free_result($query);
    @mysql_close($dbissste);
}

function getDatosMedicamentos($id_medicamento) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_medicamento' => $row_query['id_medicamento'],
            'partida' => $row_query['partida'],
            'codigo_barras' => $row_query['codigo_barras'],
            'descripcion' => $row_query['descripcion'],
            'presentacion' => $row_query['presentacion'],
            'unidad' => $row_query['unidad'],
            'precio' => $row_query['precio'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1']
        );
    } else
        $ret["id_medicamento"] = 0;
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentos() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos ORDER BY descripcion";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medicamento' => $row_query['id_medicamento'],
                'partida' => $row_query['partida'],
                'codigo_barras' => $row_query['codigo_barras'],
                'descripcion' => $row_query['descripcion'],
                'presentacion' => $row_query['presentacion'],
                'unidad' => $row_query['unidad'],
                'precio' => $row_query['precio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCodigoBarras() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM codigo_barras LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "000000000001";
    if ($totalRows_query > 0) {
        $ret = $row_query['codigo'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getIdRecetaAgregada($serie, $folioSiguiente, $id_medico, $id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT id_receta FROM recetas WHERE serie='" . $serie . "' AND folio='" . $folioSiguiente . "' AND id_medico='" . $id_medico . "' AND id_derecho='" . $id_derecho . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "0";
    if ($totalRows_query > 0) {
        $ret = $row_query['id_receta'];
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getConsultorioXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM consultorios WHERE id_consultorio='" . $id . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_consultorio' => $row_query['id_consultorio'],
            'nombre' => $row_query['nombre']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCita($idHorario, $fechaCita) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $idHorario . "' and fecha_cita='" . $fechaCita . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_derecho' => $row_query['id_derecho'],
            'id_cita' => $row_query['id_cita'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1'],
            'id_usuario' => $row_query['id_usuario'],
            'observaciones' => $row_query['observaciones']
        );
    } else {
        $ret = array(
            'id_derecho' => "-1",
            'id_cita' => "-1",
            'status' => "-1",
            'extra1' => "-1",
            'id_usuario' => "-1",
            'observaciones' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitaExt($id_cita) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_cita='" . $id_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'id_derecho' => $row_query['id_derecho'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita'],
            'fecha_cita' => $row_query['fecha_cita'],
            'id_usuario' => $row_query['id_usuario'],
            'extra1' => $row_query['extra1'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetas($status) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE status='" . $status . "' ORDER BY fecha,hora DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            /*$ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );*/
			$ret[]=$row_query;
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetaCancelada($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_canceladas WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_receta' => $row_query['id_receta'],
            'id_usuario' => $row_query['id_usuario'],
            'motivo' => $row_query['motivo'],
            'fecha' => $row_query['fecha'],
            'hora' => $row_query['hora'],
            'extra' => $row_query['extra']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDatosMedicamentoEnReceta($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_concepto' => $row_query['id_concepto'],
                'id_receta' => $row_query['id_receta'],
                'id_medicamento' => $row_query['id_medicamento'],
                'cantidad' => $row_query['cantidad'],
                'unidad' => $row_query['unidad'],
                'dias' => $row_query['dias'],
                'indicaciones' => $row_query['indicaciones'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function cambiarStatusMed($id_receta, $id_med, $status) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "UPDATE recetas_conceptos SET extra1='" . $status . "', fecha='" . date('Ymd') . "', hora='" . date('H:i:s') . "', id_usuario_surtio='" . $_SESSION["id_usuario"] . "' WHERE id_receta='" . $id_receta . "' AND id_medicamento='" . $id_med . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "' AND id_medicamento='" . $id_med . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $row_query["cantidad"];
}

function todosMedicamentosSurtidos($id_receta) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = "1";
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $id_receta . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            if ($row_query['extra1'] == "")
                $ret = "0";
        }while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function cambiarStatusReceta($id_receta, $status) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "UPDATE recetas SET status='" . $status . "' WHERE id_receta='" . $id_receta . "'  LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    @mysql_free_result($query);
    @mysql_close($dbissste);
}

function hayMedicamento($id_med, $cantidad) {
    $existencias = getExistenciasMedicamentos($id_med);
    $exist_actual = (int) $existencias["existencia_actual"];
    $cantidad = (int) $cantidad;
    $ret = false;
    if ($exist_actual < $cantidad)
        $ret = false; else
        $ret = true;
    return $ret;
}

function tieneTratamiento($id_med, $id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = false;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos_esp WHERE id_medicamento='" . $id_med . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query == 0) { // SI EL MEDICAMENTO NO ESTA EN LA TABLA DE MEDICAMENTOS SIN RESTRICCIONES, SE VERIFICA SI TIENE O NO TRATAMIENTO
        $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "' AND status!='0'";
        $query = mysql_query($query_query, $bdissste) or die(mysql_error());
        $row_query = mysql_fetch_assoc($query);
        $totalRows_query = mysql_num_rows($query);
        if ($totalRows_query > 0) {
            do {
                $query_query2 = "SELECT * FROM recetas_conceptos WHERE id_receta='" . $row_query['id_receta'] . "' AND id_medicamento='" . $id_med . "'";
                $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
                $row_query2 = mysql_fetch_assoc($query2);
                $totalRows_query2 = mysql_num_rows($query2);
                if ($totalRows_query2 > 0) {
                    $fecha_factura = substr($row_query["fecha"], 6, 2) . "-" . substr($row_query["fecha"], 4, 2) . "-" . substr($row_query["fecha"], 0, 4);
                    $hoy = date("d-m-Y");
                    $fecha_termino = strtotime($fecha_factura . " +" . $row_query2["dias"] . " day");
                    $fecha_termino = date("Ymd", $fecha_termino);
                    $hoy = date("Ymd");
                    $hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
                    $terminoMK = mktime(0, 0, 0, substr($fecha_termino, 4, 2), substr($fecha_termino, 6, 2), substr($fecha_termino, 0, 4));
                    if ($terminoMK > $hoyMK)
                        $ret = true;
                }
            }while ($row_query = mysql_fetch_assoc($query));
        }
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServiciosColectivos() {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM servicios WHERE id_grupo_servicio='2' AND st='1' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_servicio' => $row_query['id_servicio'],
                'clave' => $row_query['clave'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasXderechohabiente($id_derecho) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $hoy = date('Ymd');
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_derecho='" . $id_derecho . "' ORDER BY fecha_cita ASC, id_horario ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_horario' => $row_query['id_horario'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasExtXderechohabiente($id_derecho) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $hoy = date('Ymd');
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_derecho='" . $id_derecho . "' AND fecha_cita<='" . $hoy . "' ORDER BY fecha_cita ASC, hora_inicio ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_horario'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getHorarioXid($id) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM horarios WHERE id_horario='" . $id . "' ORDER BY id_horario";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
	$ret=array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_horario' => $row_query['id_horario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'dia' => $row_query['dia'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXderecho($id_derecho, $id_medico, $id_servicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = array();
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "' AND id_medico='" . $id_medico . "' AND id_servicio='" . $id_servicio . "' ORDER BY fecha DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXderechoXcita($id_derecho, $id_medico, $id_servicio, $fecha) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = array();
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "' AND id_medico='" . $id_medico . "' AND id_servicio='" . $id_servicio . "' AND fecha='" . $fecha . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getRecetasXMedico($idDr, $fechaInicio, $fechaTermino) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;

    $fechaInicio = formatoFechaBD($fechaInicio);
    $fechaTermino = formatoFechaBD($fechaTermino);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_medico='" . $idDr . "' AND fecha<='" . $fechaTermino . "' AND fecha>='" . $fechaInicio . "' ORDER BY fecha DESC, hora DESC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function buscarDHxCedulaParaCenso($cedula) {
    mysql_connect("localhost", "root", "1234567890");
    mysql_select_db("sistema_agenducha");
    $sql = "SELECT * FROM derechohabientes WHERE cedula like '%" . $cedula . "%' ORDER BY cedula_tipo";
    $dhs = mysql_query($sql);
    $out = "<select name=\"dh\" id=\"dh\">";
    $cont = 0;
    if (mysql_num_rows($dhs) > 0) {
        mysql_select_db("sistema_censo") or die(mysql_error());
        while ($row = mysql_fetch_array($dhs)) {
            $idDerecho = $row['id_derecho'];
            $sql1 = "select * from pacientes_en_piso where id_derecho=$idDerecho";
            $query = mysql_query($sql1) or die(mysql_error());
            if (mysql_num_rows($query) > 0) {
                $out.= "<option value=\"" . $row['id_derecho'] . "|" . $row['cedula'] . "|" . $row['cedula_tipo'] . "|" . ponerAcentos($row['ap_p']) . "|" . ponerAcentos($row['ap_m']) . "|" . ponerAcentos($row['nombres']) . "|" . ponerAcentos($row['telefono']) . "|" . ponerAcentos($row['direccion']) . "|" . $row['estado'] . "|" . $row['municipio'] . "|" . $row['fecha_nacimiento'] . "\">" . $row['cedula'] . "/" . $row['cedula_tipo'] . " " . ponerAcentos($row['ap_p']) . " " . ponerAcentos($row['ap_m']) . " " . ponerAcentos($row['nombres']) . "</option>";
                $cont++;
            }
        }
    }
    if ($cont == 0)
        $out .= "<option value=\"-1\"><span class='noExiste'>NO HAY PACIENTE HOSPITALIZADO</span> " . strtoupper($cedula) . "</option>";
    $out .= "</select>";
    return $out;
}

function mostrarMes() {
    if (!isset($_GET['getdate'])) {
        $unix_time = strtotime(date('Ymd'));
    } else {
        $unix_time = strtotime($_GET['getdate']);
    }

    $display_date = tituloMes(date('n', $unix_time)) . " " . date('Y', $unix_time);
    $meses = "<select id=\"meses\" name=\"meses\" onchange=\"javascript: cambiarMes(this.value);\">";
    for ($i = 6; $i > 0; $i--) {
        $fechaCompleta = date('Ymd', strtotime(date("Y/m/d", $unix_time) . " - " . $i . " month"));
        $mes = date('n', strtotime(date("Y/m/d", $unix_time) . " - " . $i . " month"));
        $anio = date('Y', strtotime(date("Y/m/d", $unix_time) . " - " . $i . " month"));
        $meses .= "<option value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
    }
    for ($i = 0; $i < 7; $i++) {
        $fechaCompleta = date('Ymd', strtotime(date("Y/m/d", $unix_time) . " + " . $i . " month"));
        $mes = date('n', strtotime(date("Y/m/d", $unix_time) . " + " . $i . " month"));
        $anio = date('Y', strtotime(date("Y/m/d", $unix_time) . " + " . $i . " month"));
        if ($i == 0) {
            $meses .= "<option selected value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
        } else {
            $meses .= "<option value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
        }
    }
    $meses .= "</select>";
    $display_date = $meses;

    $MC = array();
    $MC['title'] = $display_date;
    $MC['webcals'] = array();
    return $MC;
}

function idTipoCita($tipoCita) {
    if ($tipoCita == "PRV")
        return 0;
    if ($tipoCita == "SUB")
        return 1;
    if ($tipoCita == "PRO")
        return 2;
}

function recuperarCantidadCitasEXT($fechaCitas, $idUsuario, $idConsultorio, $idServicio, $idMedico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
//	$idMedico =  regresarIdMedico($idConsultorio,$idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query2 = "SELECT * FROM citas_extemporaneas WHERE id_consultorio='" . $idConsultorio . "' AND id_servicio='" . $idServicio . "' AND id_medico='" . $idMedico . "' AND fecha_cita='" . $fechaCitas . "'";
    $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
    $cuantas = mysql_num_rows($query2);
    return '<span class="conteoCitasAmbar">' . $cuantas . ' EXT</span><br>';
}

function recuperarCantidadCitas($tipoCitas, $fechaCitas, $idUsuario, $idConsultorio, $idServicio, $idMedico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
//	$idMedico =  regresarIdMedico($idConsultorio,$idServicio);
    $idCita = idTipoCita($tipoCitas);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $temp = mktime(0, 0, 0, substr($fechaCitas, 4, 2), substr($fechaCitas, 6, 2), substr($fechaCitas, 0, 4));
    $queDia = date("N", $temp);
    $query_query = "SELECT * FROM horarios WHERE id_medico='" . $idMedico . "' and id_servicio='" . $idServicio . "'and id_consultorio='" . $idConsultorio . "' and tipo_cita='" . $idCita . "' and dia='" . diaSemana($queDia) . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);

    $cuantas = 0;
    if ($totalRows_query2 > 0) {
        do {
            $query_query2 = "SELECT * FROM citas WHERE id_horario='" . $row_query['id_horario'] . "' and fecha_cita='" . $fechaCitas . "'";
            $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
            $cuantas+= mysql_num_rows($query2);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_free_result($query2);
    @mysql_close($dbissste);
    if ($totalRows_query2 == 0) {
        $clase = "conteoCitasGris";
    } else {
        $porcentaje = round(($cuantas / $totalRows_query2) * 100);
        if (($porcentaje >= 0) && ($porcentaje <= 50))
            $clase = "conteoCitasVerde";
        if (($porcentaje >= 51) && ($porcentaje <= 98))
            $clase = "conteoCitasAmbar";
        if (($porcentaje >= 99) && ($porcentaje <= 100))
            $clase = "conteoCitasRojo";
    }
    return '<span class="' . $clase . '">' . $cuantas . '/' . $totalRows_query2 . ' ' . $tipoCitas . '</span><br>';
}

function diferenciaDeDiasEntreFechas($ano1, $mes1, $dia1, $ano2, $mes2, $dia2) {
    //defino fecha 1
    /* 	$ano1 = 2006;
      $mes1 = 10;
      $dia1 = 2;

      //defino fecha 2
      $ano2 = 2006;
      $mes2 = 10;
      $dia2 = 27;
     */
    //calculo timestam de las dos fechas
    $timestamp1 = mktime(0, 0, 0, $mes1, $dia1, $ano1);
    $timestamp2 = mktime(4, 12, 0, $mes2, $dia2, $ano2);

    //resto a una fecha la otra
    $segundos_diferencia = $timestamp1 - $timestamp2;
    //echo $segundos_diferencia;
    //convierto segundos en d�as
    $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

    //obtengo el valor absoulto de los d�as (quito el posible signo negativo)
    $dias_diferencia = abs($dias_diferencia);

    //quito los decimales a los d�as de diferencia
    $dias_diferencia = floor($dias_diferencia);

    return $dias_diferencia;
}

function VerCitasXMedico($fecha, $medico, $servicio) {
    global $username_bdissste;
    global $hostname_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
    $sql = "SELECT id_derecho,hora_inicio,hora_fin,id_servicio FROM citas AS c JOIN horarios AS h ON(c.`id_horario`=h.`id_horario`) WHERE h.`id_medico`=$medico AND c.`fecha_cita`=$fecha AND id_servicio=$servicio
UNION
SELECT id_derecho,hora_inicio,hora_fin,id_servicio FROM citas_extemporaneas WHERE id_medico=$medico AND fecha_cita=$fecha AND id_servicio=$servicio";
}

function existeDuplicaCita($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function insertarAgenda($sql) {
    global $hostname_bdisssteR;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    $bdi = mysql_connect($hostname_bdisssteR, $username_bdisssteR, $password_bdisssteR) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdi);
    $query = mysql_query($sql, $bdi);
    return mysql_affected_rows($bdi);
}

function ejecutarSQLAgenda($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());

    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    else
        $ret = array();
    @mysql_free_result($query);
    @mysql_close($bdissste);
    return $ret;
}

function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function ejecutarLOG($log) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "INSERT INTO bitacora VALUES('" . date('Ymd') . "','" . date('Hi') . "','" . getRealIpAddr() . "','" . $log . "','" . $_SESSION['idUsuario'] . "')";
    $query = mysql_query($query_query, $bdissste); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $error;
}

function getCitaRecienAgregada($id_horario, $id_derecho, $fecha_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $id_horario . "' AND id_derecho='" . $id_derecho . "' AND fecha_cita='" . $fecha_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_horario' => $row_query['id_horario'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitaERecienAgregada($id_consultorio, $id_servicio, $id_medico, $id_derecho, $fecha_cita, $hora_inicio, $hora_fin) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_consultorio='" . $id_consultorio . "' AND id_servicio='" . $id_servicio . "' AND id_medico='" . $id_medico . "' AND id_derecho='" . $id_derecho . "' AND hora_inicio='" . $hora_inicio . "' AND hora_fin='" . $hora_fin . "' AND fecha_cita='" . $fecha_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'id_derecho' => $row_query['id_derecho'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function UltimaCita($id_Derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $sql = "select * from citas where id_derecho=$id_Derecho order by fecha_cita DESC LIMIT 1";
    $query = mysql_query($sql);
    $datosCita = array();
    if (mysql_num_rows($query) > 0)
        $datosCita = mysql_fetch_array($query);
    return $datosCita;
}

function UltimaCitaExt($id_Derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $sql = "select * from citas_extemporaneas where id_derecho=$id_Derecho order by fecha_cita DESC LIMIT 1";
    $query = mysql_query($sql);
    $datosCita = array();
    if (mysql_num_rows($query) > 0)
        $datosCita = mysql_fetch_array($query);
    return $datosCita;
}

function getCitaDatos($id_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_cita='" . $id_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_horario' => $row_query['id_horario'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitaExtemporaneaDatos($id_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_cita='" . $id_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'id_derecho' => $row_query['id_derecho'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicamentosXservicio($id_servicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentos ORDER BY descripcion";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $query_query2 = "SELECT * FROM medicamentosxservicio WHERE id_servicio='" . $id_servicio . "' AND id_medicamento='" . $row_query['id_medicamento'] . "' LIMIT 1";
            $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
            $totalRows_query2 = mysql_num_rows($query2);
            if ($totalRows_query2 > 0) {
                $ret[] = array(
                    'id_medicamento' => $row_query['id_medicamento'],
                    'partida' => $row_query['partida'],
                    'codigo_barras' => $row_query['codigo_barras'],
                    'descripcion' => $row_query['descripcion'],
                    'presentacion' => $row_query['presentacion'],
                    'unidad' => $row_query['unidad'],
                    'precio' => $row_query['precio'],
                    'status' => $row_query['status'],
                    'extra1' => $row_query['extra1']
                );
            }
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function tieneMedicamentoAsignado($id_derecho, $id_medicamento) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicamentosxderecho WHERE id_derecho='" . $id_derecho . "' AND id_medicamento='" . $id_medicamento . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    $ret = false;
    if ($totalRows_query > 0) {
        $ret = true;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

//Funciones para Contrarreferencias en misFunciones.php
function getContraReferencia($id_servicio, $id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    $ret = false;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdisssteR, $bdissste);
    $contraRef = getContraRef($id_derecho, $id_servicio);
    if (count($contraRef) > 0) {
        $ncitas = $contraRef['citas_subsecuentes'];
        $sql="select c.fecha_cita from citas as c join horarios as h on(c.id_horario=h.id_horario) where c.id_derecho=$id_derecho and h.id_servicio=$id_servicio
        union
        select fecha_cita from citas_extemporaneas where id_derecho=$id_derecho and id_servicio=$id_servicio order by fecha_cita DESC limit 1";
        $fechaCita=  mysql_fetch_array(mysql_query($sql));
        $fechaAct=date("Ymd");
        if($fechaCita['fecha_cita']>$fechaAct)
            $ncitas--;
        $referencia = getContraReferencia1($contraRef['id_referencia']);
    }
    else
        $ncitas = 0;

    @mysql_free_result($query);
    @mysql_close($dbissste);
    if ($ncitas > 2 && count($referencia)==0)
        $ret = true;
    else
    if($ncitas>2 && $referencia['fecha_contrarreferencia']!=date("Ymd"))
        $ret=true;
    return $ret;
}

function LlenarContraReferencias($idDerecho, $idServicio, $fecha, $resumen, $diagnosticoRef, $diagnostico, $evolucion, $tratamiento, $recomendaciones, $idUnidad, $idMedico, $cont) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
    $fechaPrv = ObtenerPrimeraCita($idDerecho, $idServicio);
    $referencia = getContraRefGen($idDerecho, $idServicio);
    $contra = ObtenerUltimaContra($idDerecho,$idServicio);
    if(count($contra)==0 || $contra['fecha_contrarreferencia']!=date("Ymd")){
        $sql = "insert into contrarreferencias values(NULL," . $referencia['id_referencia'] . ",$idDerecho,$idServicio,'" . $fecha . "','" . $fechaPrv['fecha_cita'] . "',1,'".quitarAcentos($resumen)."','".quitarAcentos($diagnosticoRef)."','".quitarAcentos($diagnostico)."','".quitarAcentos($evolucion)."','".quitarAcentos($tratamiento)."','".quitarAcentos($recomendaciones)."','$idUnidad','$idMedico','$cont',0,'');";
        $query = mysql_query($sql);
        $error = mysql_errno();
        if ($cont == 0) {
            $sql = "update referencia_contrarreferencia set cita_primera=0,citas_subsecuentes=0 where id_derecho=" .$idDerecho . " AND id_servicio=" . $idServicio;
        }
        else
            $sql = "update referencia_contrarreferencia set citas_subsecuentes=0 where id_derecho=" . $idDerecho . " AND id_servicio=" . $idServicio;
        $query = mysql_query($sql);
        $error = mysql_errno();
        return $error;
    }
    else
        return 1;
}

function getContraRefGen($idDerecho, $servicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
 
    $sql = "SELECT * FROM `referencia_contrarreferencia` WHERE `id_derecho`='$idDerecho' and `id_servicio`='$servicio' order by id_referencia DESC";
	$ret=array();
    $query = mysql_query($sql);
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    else
        {
			$sql="select c.id_cita from citas as c join horario as h on(c.id_horario=h.id_horario) where c.id_derecho=".$idDerecho." and h.id_servicio=".$servicio." and h.tipo_cita=1
			UNION
			select id_cita from citas_extemporaneas where id_derecho=".$idDerecho." and id_servicio=".$servicio." and tipo_cita=1";
			$citasSub=ejecutarSQLAgenda($sql);
			if(count($citasSub)>0){
			$sql="insert into referencias_contrarreferencias values(NULL,$id_derecho,$servicio,1,$citasSub,1);";
			$query=ejecutarSQLR($sql);
			if($query[0]==0)
			{
			getContraRef($idDerecho, $servicio);
			}
		}
		}
    return $ret;
}

function getContraRef($idDerecho, $servicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
 
    $sql = "SELECT * FROM `referencia_contrarreferencia` WHERE `id_derecho`='$idDerecho' and `id_servicio`='$servicio' order by id_referencia DESC";
    $query = mysql_query($sql);
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    else
        {
			$ret=array();		
		}
    return $ret;
}

function getContraReferencia1($idRef) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
    $sql = "select * from contrarreferencias where folio_contrarreferencia=" . $idRef;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_errno() == 0)
        if (mysql_num_rows($query)>0)
            $ret = mysql_fetch_array($query);

    return $ret;
}

function obtenerUnidadMedica($idUnidadMedica) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
	mysql_set_charset('utf8');
    $sql = "select * from vgf where id_unidad='$idUnidadMedica'";
    $query = mysql_query($sql);
    if (mysql_errno() == 0) {
        if (mysql_num_rows($query) > 0) {
            $ret = mysql_fetch_array($query);
        }
        else
            $ret = array();
    }
    return $ret;
}

function ObtenerPrimeraCita($idDerecho, $idServ) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
    $sql = "SELECT c.`fecha_cita` FROM citas AS c JOIN horarios AS h ON(c.`id_horario`=h.`id_horario`) WHERE c.`id_derecho`=$idDerecho AND h.`id_servicio`=$idServ AND h.`tipo_cita`=0
UNION
SELECT fecha_cita FROM citas_extemporaneas WHERE id_derecho=$idDerecho AND id_servicio=$idServ AND tipo_cita=0 order by fecha_cita DESC LIMIT 1";
    $consulta = mysql_query($sql);
    if (mysql_num_rows($consulta) > 0) {
        $ret = mysql_fetch_assoc($consulta);
    }
    else
        $ret = array();
    return $ret;
}

function ObtenerUltimaContra($idDer,$idServ)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
    $sql = "select * from contrarreferencias where id_derecho=" . $idDer." AND id_servicio=$idServ Order by fecha_contrarreferencia DESC Limit 1";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_errno() == 0)
        if (mysql_num_rows($query)>0)
            $ret = mysql_fetch_array($query);

    return $ret;
}

function CargarUnidadesMedicas($estado)
{
	 global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdisssteR);
	$estado=cambiarAcentosUMF($estado);
	$sql="select id_unidad,nombre from vgf where Estado='".$estado."'";
$query=  mysql_query($sql) or die(mysql_error());
$umf="<option value='-1'</option>";
if(mysql_num_rows($query)>0){
    while ($row = mysql_fetch_array($query)) {
        $umf.="<option value='".$row['id_unidad']."'>".htmlentities($row['nombre'])."</option>";
    }
}
 else {
     $umf='<option value="0">No hay Unidades Medicas encontradas</option>';
 }
return $umf;
}

function cambiarAcentosUMF($Text)
{
	 $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "(") {
            $temp = substr($Text, $j, 4);
            switch ($temp) {
                case "(/a)": $cadena .= "a";
                    $j = $j + 3;
                    break;
                case "(/A)": $cadena .= "A";
                    $j = $j + 3;
                    break;
                case "(/e)": $cadena .= "e";
                    $j = $j + 3;
                    break;
                case "(/E)": $cadena .= "E";
                    $j = $j + 3;
                    break;
                case "(/i)": $cadena .= "i";
                    $j = $j + 3;
                    break;
                case "(/I)": $cadena .= "I";
                    $j = $j + 3;
                    break;
                case "(/o)": $cadena .= "o";
                    $j = $j + 3;
                    break;
                case "(/O)": $cadena .= "O";
                    $j = $j + 3;
                    break;
                case "(/u)": $cadena .= "u";
                    $j = $j + 3;
                    break;
                case "(/U)": $cadena .= "U";
                    $j = $j + 3;
                    break;
                case "(/n)": $cadena .= "ñ";
                    $j = $j + 3;
                    break;
                case "(/N)": $cadena .= "Ñ";
                    $j = $j + 3;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            $cadena.=$Text[$j];
        }
    }
    return $cadena;
}

function ListaDiags($sexo, $edad,$q,$elemento) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    $cie_bd = "sistema_cie10";
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($cie_bd);
    $sql = "select * from cie_10_simef where (Sexo='$sexo' or sexo='Ambos') and (Decripcion like '%$q%' or Clave Like '$q%') ";
    $query = mysql_query($sql);
    $ret = "";
    if (mysql_num_rows($query) > 0) {
        while ($diag = mysql_fetch_array($query)) {
            if ($diag['LIM_INF'] != "" || $diag['LIM_SUP'] != "") {
                $edadDias = getDiasXfechaNac($edad);
                $edadAnios = getEdadXfechaNac($edad);
                if (substr($diag['LIM_INF'], strlen($diag['LIM_INF']) - 1) == "A") {
                    $edadMinDiag = substr($diag['LIM_INF'], 0, strlen($diag['LIM_INF']) - 1);
                    $bandEdadMin = $edadAnios >= $edadMinDiag;
                } else {
                    $edadMinDiag = substr($diag['LIM_INF'], 0, strlen($diag['LIM_INF']) - 1);
                    $bandEdadMin = $edadAnios >= $edadMinDiag;
                }
                if (substr($diag['LIM_SUP'], strlen($diag['LIM_SUP']) - 1) == "A") {
                    $edadMaxDiag = substr($diag['LIM_SUP'], 0, strlen($diag['LIM_SUP']) - 1);
                    $bandEdadMax = $edadAnios <= $edadMaxDiag;
                } else {
                    $edadMaxDiag = substr($diag['LIM_SUP'], 0, strlen($diag['LIM_SUP']) - 1);
                    $bandEdadMax = $edadAnios <= $edadMaxDiag;
                }
                if ($bandEdadMax && $bandEdadMin)
				if($elemento=="resultados1")
				$ret.="<li><a href='javascript:regresarDiagnostico1(\"" . $diag['Clave'] . "-" . $diag['Decripcion'] . "\");'>" . $diag['Decripcion'] . "</a></li>";
				else
                    $ret.="<li><a href='javascript:regresarDiagnostico(\"" . $diag['Clave'] . "-" . $diag['Decripcion'] . "\");'>" . $diag['Decripcion'] . "</a></li>";
            }
            else {
				if($elemento=="resultados1")
				$ret.="<li><a href='javascript:regresarDiagnostico1(\"" . $diag['Clave'] . "-" . $diag['Decripcion'] . "\");'>" . $diag['Decripcion'] . "</a></li>";
				else
                $ret.="<li><a href='javascript:regresarDiagnostico(\"" . $diag['Clave'] . "-" . $diag['Decripcion'] . "\");'>" . $diag['Decripcion'] . "</a></li>";
            }
        }
    }
    return $ret;
}

function getDiasXfechaNac($fecha) {
    if (strlen($fecha) == 10) {
        $diaHoy = date('j');
        $mesHoy = date('n');
        $anoHoy = date('Y');

        $ano = substr($fecha, 6, 4);
        $mes = substr($fecha, 3, 2);
        $dia = substr($fecha, 0, 2);
        if (($mes == $mesHoy) && ($dia > $diaHoy)) {
            $anoHoy = ($anoHoiy - 1);
        }
        //si el mes es superior al actual tampoco habr� cumplido a�os, por eso le quitamos un a�o al actual
        if ($mes > $mesHoy) {
            $anoHoy = ($anoHoy - 1);
        }
        //ya no habria mas condiciones, ahora simplemente restamos los a�os y mostramos el resultado como su edad
        $edad = ($anoHoy - $ano);
        $edad = $edad * 365;
    } else {
        $edad = "ND";
    }
    return $edad;
}

/*function getEdadXfechaNac($fecha) {
    if (strlen($fecha) == 10) {
        $diaHoy = date('j');
        $mesHoy = date('n');
        $anoHoy = date('Y');

        $ano = substr($fecha, 6, 4);
        $mes = substr($fecha, 3, 2);
        $dia = substr($fecha, 0, 2);
        if (($mes == $mesHoy) && ($dia > $diaHoy)) {
            $anoHoy = ($anoHoiy - 1);
        }
        //si el mes es superior al actual tampoco habr� cumplido a�os, por eso le quitamos un a�o al actual
        if ($mes > $mesHoy) {
            $anoHoy = ($anoHoy - 1);
        }
        //ya no habria mas condiciones, ahora simplemente restamos los a�os y mostramos el resultado como su edad
        $edad = ($anoHoy - $ano);
    } else {
        $edad = "ND";
    }
    return $edad;
}*/

function AgregarMedicinas($idMed,$idContra,$idDh,$cant,$trat,$fecha,$cronico,$id_servicio,$id_clin)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or die(mysql_error());
    mysql_select_db($database_bdisssteR)or die(mysql_error());
    $fechaFin=date("Ymd",  strtotime("+".$trat."days",  strtotime($fecha)));
    $sql="insert into medicamentos_contrarreferencias values('$idMed',$idContra,$idDh,$trat,$cant,'$fecha','$fechaFin',$cronico,true,$id_servicio,$id_clin)";
    $query=  mysql_query($sql);
    $error=  mysql_errno();
    return $error==0;
}

function ReferenciaXid($idRef)
{
     global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or die(mysql_error());
    mysql_select_db($database_bdisssteR)or die(mysql_error());
    $sql="select * from contrarreferencias where id_contra=$idRef";
    $result=  mysql_query($sql);
    $ret=array();
    if(mysql_num_rows($result)>0)
    {
        $ret=  mysql_fetch_assoc($result);
    }
    return $ret;
}

function ObtenerMedicinasReferencia($idRef)
{
     global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or die(mysql_error());
    mysql_select_db($database_bdisssteR)or die(mysql_error());
    $sql="select * from medicamentos_contrarreferencias where id_contrarreferencia=$idRef";
    $query=mysql_query($sql);
    $ret=array();
    if(mysql_num_rows($query)>0)
        while ($row = mysql_fetch_array($query)) {
            $med=getDatosMedicamentos($row['id_medicamento']);
            $ret[]=array(
                'medicina'=>  substr($med['descripcion'], 0,50),
                'dias'=>$row['dias'],
                'cajas'=>$row['cajas']
            );
        }
    return $ret;
}

function getUsuarioXidAgenda($id) {
     global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or die(mysql_error());
    mysql_select_db($database_bdisssteR)or die(mysql_error());
    $query_query = "SELECT * FROM usuarios WHERE id_usuario='" . $id . "' ORDER BY nombre";
    $query = mysql_query($query_query) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_usuario' => $row_query['id_usuario'],
            'login' => $row_query['login'],
            'pass' => $row_query['pass'],
            'nombre' => $row_query['nombre'],
            'tipo_usuario' => $row_query['tipo_usuario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'status' => $row_query['status']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citaMedico($id_cita) {
         global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or die(mysql_error());
    mysql_select_db($database_bdisssteR)or die(mysql_error());
    $sqlD = "select * from citas_medico where id_cita=$id_cita";
    $query = mysql_query($sqlD);
    $res = array();
    if (mysql_num_rows($query) > 0)
        $res = mysql_fetch_assoc($query);
    return $res;
}
?>
