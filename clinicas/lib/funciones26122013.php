<?php

$baseDatos_issste = 'sistema_agenducha';
$host_issste = 'localhost';
$usuario_issste = 'root';
$pass_issste = '1234567890';

function obtenerDatosUsuario($idUsuario) {
    global $baseDatos_issste;
    global $host_issste;
    global $pass_issste;
    global $usuario_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from usuarios_contrarreferencias where id_usuario=" . $idUsuario;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function obtenerDatosClinica($idClinica) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from vgf where id_unidad=" . $idClinica;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function obtenerReferenciasEnviadas($idClin) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from contrarreferencias where id_unidad=" . $idClin . " And envio_clinica=1 Order by fecha_envio_clinica DESC LIMIT 50";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query))
        while ($row = mysql_fetch_array($query)) {
            $ret[] = $row;
        }
    return $ret;
}

function obtenerReferenciasCont($idClin) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from contrarreferencias where id_unidad=" . $idClin . " And envio_clinica=1 AND status_citas=1 Order by fecha_envio_clinica DESC LIMIT 50";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query))
        while ($row = mysql_fetch_array($query)) {
            $ret[] = $row;
        }
    return $ret;
}

function obtenerReferenciaCitaSinCont($idClin) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from contrarreferencias where id_unidad=" . $idClin . " And envio_clinica=1 AND status_citas=0 Order by fecha_envio_clinica DESC LIMIT 50";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query))
        while ($row = mysql_fetch_array($query)) {
            $ret[] = $row;
        }
    return $ret;
}

function insertarSQL($sql) {
    global $host_issste;
    global $baseDatos_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    mysql_query($sql) or die(mysql_error() . "<br>" . $sql);
    $ret[0] = mysql_errno();
    $ret[1] = mysql_error();
    return $ret;
}

function consultaSQL($sql) {
    global $host_issste;
    global $baseDatos_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or die(mysql_error());
    mysql_select_db($baseDatos_issste) or die(mysql_error());
    $query = mysql_query($sql) or die(mysql_error());
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function DatosDerechoHabiente($idDer) {
    global $host_issste;
    global $baseDatos_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or die(mysql_error());
    mysql_select_db($baseDatos_issste);
    $sql = "select * from derechohabientes where id_derecho=" . $idDer;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function DatosServicio($idServ) {
    global $host_issste;
    global $baseDatos_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or die(mysql_error());
    mysql_select_db($baseDatos_issste);
    $sql = "select * from servicios where id_servicio=" . $idServ;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);
    return $ret;
}

function ponerAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "(") {
            $temp = substr($Text, $j, 4);
            switch ($temp) {
                case "(/a)": $cadena .= "&aacute;";
                    $j = $j + 3;
                    break;
                case "(/A)": $cadena .= "&Aacute;";
                    $j = $j + 3;
                    break;
                case "(/e)": $cadena .= "&eacute;";
                    $j = $j + 3;
                    break;
                case "(/E)": $cadena .= "&Eacute;";
                    $j = $j + 3;
                    break;
                case "(/i)": $cadena .= "&iacute;";
                    $j = $j + 3;
                    break;
                case "(/I)": $cadena .= "&Iacute;";
                    $j = $j + 3;
                    break;
                case "(/o)": $cadena .= "&oacute;";
                    $j = $j + 3;
                    break;
                case "(/O)": $cadena .= "&Oacute;";
                    $j = $j + 3;
                    break;
                case "(/u)": $cadena .= "&uacute;";
                    $j = $j + 3;
                    break;
                case "(/U)": $cadena .= "&Uacute;";
                    $j = $j + 3;
                    break;
                case "(/n)": $cadena .= "&ntilde;";
                    $j = $j + 3;
                    break;
                case "(/N)": $cadena .= "&Ntilde;";
                    $j = $j + 3;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            $cadena.=$Text[$j];
        }
    }
    return $cadena;
}

function ObtenerDHS($sql) {
    global $baseDatos_issste;
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0) {
        while ($row = mysql_fetch_assoc($query)) {
            $ret[] = $row;
        }
    }
    return $ret;
}

function buscarDHxCedulaParaBusqueda($ced) {

    $sql = "select * from derechohabientes where cedula like '" . $ced . "%' order by cedula_tipo ASC";
    $dhs = ObtenerDHS($sql);
    $tdh = count($dhs);
    $ret = "<select id='dh' name='dh' onchange='activarInc();MostrarDatosDH();'>";
    if ($tdh > 0)
        foreach ($dhs as $key => $value) {
            $ret.="<option value='" . $value['id_derecho'] . "|" . $value['cedula_tipo'] . "'>" . $value['cedula'] . "/" . $value['cedula_tipo'] . " " . ponerAcentos($value['ap_p']) . " " . ponerAcentos($value['ap_m']) . " " . ponerAcentos($value['nombres']) . "</option>";
        } else
        $ret.="<option value='-1'>No existe derechohabiente con la Cedula " . strtoupper($ced) . "</option>";
    $ret.="</select>";
    return $ret;
}

function buscarDHxNombreParaBusqueda($app, $apm, $nombres) {
    $sql = "select * from derechohabientes where ap_p like '%" . $app . "%' and ap_m like '%$apm%' and nombres like '%$nombres%' order by cedula_tipo ASC";
    $dhs = ObtenerDHS($sql);
    $tdh = count($dhs);
    $ret = "<select id='dh' name='dh' onchange='activarInc();MostrarDatosDH();'>";
    if ($tdh > 0)
        foreach ($dhs as $key => $value) {
            $ret.="<option value='" . $value['id_derecho'] . "|" . $value['cedula_tipo'] . "'>" . $value['cedula'] . "/" . $value['cedula_tipo'] . " " . ponerAcentos($value['ap_p']) . " " . ponerAcentos($value['ap_m']) . " " . ponerAcentos($value['nombres']) . "</option>";
        } else
        $ret.="<option value='-1'>No existe derechohabiente con la Cedula " . strtoupper($ced) . "</option>";
    $ret.="</select>";
    return $ret;
}

function getMedicoXid($idMed) {
    global $baseDatos_issste;
    global $host_issste;
    global $pass_issste;
    global $usuario_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from medicos where id_medico=$idMed";
    $result = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($result) > 0)
        $ret = mysql_fetch_assoc($result);
    return $ret;
}

function getContraReferencia($idRef) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from contrarreferencias where id_contra=$idRef";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_array($query);

    return $ret;
}

function nombreServicio($idServ) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from servicios where id_servicio=$idServ";
    $query = mysql_query($sql);
    $ret = "";
    if (mysql_num_rows($query) > 0) {
        $rows = mysql_fetch_array($query);
        $ret = $rows['nombre'];
    }
    return $ret;
}

function datosServicioXCita($idCita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from citas as c join horarios as h on(c.id_horario=h.id_horario) where c.id_cita=" . $idCita;
    $result = mysql_query($sql);
    $servicio = array();
    if (mysql_num_rows($result) > 0) {
        $horarioCita = mysql_fetch_array($result);
        $servicio = DatosServicio($horarioCita['id_servicio']);
    }
    return $servicio;
}

function datosCita($idCita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from citas where id_cita=" . $idCita;
    $result = mysql_query($sql);
    $res = array();
    if (mysql_num_rows($result) > 0)
        $res = mysql_fetch_array($result);
    return $res;
}

function datosHorarioXcita($idHorario) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from horarios where id_horario=" . $idHorario;
    $result = mysql_query($sql);
    $res = array();
    if (mysql_num_rows($result) > 0)
        $res = mysql_fetch_array($result);
    return $res;
}

function citasXHorario($idHorario, $fecha) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from citas where id_horario=" . $idHorario . " and fecha_cita=" . $fecha;
    $result = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($result) > 0) {
        $ret = mysql_fetch_array($result);
    }
    return $ret;
}

function diaSemana($dia_num) {
    switch ($dia_num) {
        case 0: return "DOMINGO";
            break;
        case 7: return "DOMINGO";
            break;
        case 1: return "LUNES";
            break;
        case 2: return "MARTES";
            break;
        case 3: return "MIERCOLES";
            break;
        case 4: return "JUEVES";
            break;
        case 5: return "VIERNES";
            break;
        case 6: return "SABADO";
            break;
    }
}

function obtenerHorarioPrv($serv, $fecha/* ,$doctor */) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $dia = diaSemana(date("N", strtotime($fecha)));
    $sql = "select * from horarios where tipo_cita=0 and id_servicio=" . $serv . " and dia='$dia'  "/* and id_medico=$doctor */ . " order by hora_inicio ASC";
    $result = mysql_query($sql);
    $res = array();
    if (mysql_num_rows($result) > 0)
        while ($row = mysql_fetch_array($result)) {
            $horarioElim = obtenerHrEliminado($row['id_consultorio'], $row['id_servicio'], $row['id_medico']);
            if ($horarioElim)
                $res[] = $row;
        }
    return $res;
}

function ponerCeros($cant, $cuantos) {
    $aCeros = array("", "0", "00", "000", "0000", "00000", "000000", "0000000", "00000000", "000000000", "0000000000", "00000000000", "000000000000", "0000000000000", "00000000000000", "000000000000000");
    $tCant = strlen($cant);
    $cuantos = $cuantos - $tCant;
    return $aCeros[$cuantos] . $cant;
}

function existeDuplicaCita($query_query) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query = mysql_query($query_query) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    return $ret;
}

function existeDuplica($query_query) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query = mysql_query($query_query) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    return $ret;
}

function getCitaRecienAgregada($id_horario, $id_derecho, $fecha_cita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $id_horario . "' AND id_derecho='" . $id_derecho . "' AND fecha_cita='" . $fecha_cita . "' LIMIT 1";
    $query = mysql_query($query_query) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_horario' => $row_query['id_horario'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    return $ret;
}

function ejecutarLOG($log) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $query_query = "INSERT INTO bitacora VALUES('" . date('Ymd') . "','" . date('Hi') . "','" . getRealIpAddr() . "','" . $log . "','" . $_SESSION['idUsuario'] . "')";
    $query = mysql_query($query_query); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    return $error;
}

function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function obtenerHrEliminado($idconsultorio, $idServicio, $idMedico) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from servicios_x_consultorio where id_consultorio=$idconsultorio and id_servicio=$idServicio and id_medico=$idMedico";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0)
        $ret = true;
    else {
        $ret = false;
    }
    return $ret;
}

function quitarAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "&") {
            $temp = substr($Text, $j, 8);
            switch ($temp) {
                case "&aacute;": $cadena .= "(/a)";
                    $j = $j + 7;
                    break;
                case "&Aacute;": $cadena .= "(/A)";
                    $j = $j + 7;
                    break;
                case "&eacute;": $cadena .= "(/e)";
                    $j = $j + 7;
                    break;
                case "&Eacute;": $cadena .= "(/E)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/i)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/I)";
                    $j = $j + 7;
                    break;
                case "&oacute;": $cadena .= "(/o)";
                    $j = $j + 7;
                    break;
                case "&Oacute;": $cadena .= "(/O)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/u)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/U)";
                    $j = $j + 7;
                    break;
                case "&ntilde;": $cadena .= "(/n)";
                    $j = $j + 7;
                    break;
                case "&Ntilde;": $cadena .= "(/N)";
                    $j = $j + 7;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            switch ($cara) {
                case "á": $cadena.="(/a)";
                    break;
                case "é": $cadena.="(/e)";
                    break;
                case "í": $cadena.="(/i)";
                    break;
                case "ó": $cadena.="(/o)";
                    break;
                case "ú": $cadena.="(/u)";
                    break;
                case "Á": $cadena.="(/A)";
                    break;
                case "É": $cadena.="(/E)";
                    break;
                case "Í": $cadena.="(/I)";
                    break;
                case "Ó": $cadena.="(/O)";
                    break;
                case "Ú": $cadena.="(/U)";
                    break;
                case "ñ": $cadena.="(/n)";
                    break;
                case "Ñ": $cadena.="(/N)";
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        }
    }
    return $cadena;
}

function HayMasMedicosXServicio($idServ) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select* from servicios_x_consultorio where id_servicio=$idServ group by id_medico";
    $query = mysql_query($sql);
    if (mysql_num_rows($query) > 1)
        $ret = true;
    else
        $ret = false;
    return $ret;
}

/* function getMedicoXid($id) {

  global $host_issste;
  global $usuario_issste;
  global $pass_issste;
  global $baseDatos_issste;
  mysql_connect($host_issste, $usuario_issste, $pass_issste);
  mysql_select_db($baseDatos_issste);
  $query_query = "SELECT * FROM medicos WHERE id_medico='" . $id . "'";
  $query = mysql_query($query_query) or die(mysql_error());
  $row_query = mysql_fetch_assoc($query);
  $totalRows_query = mysql_num_rows($query);
  if ($totalRows_query > 0) {
  $ret = array(
  'id_medico' => $row_query['id_medico'],
  'cedula' => $row_query['cedula'],
  'cedula_tipo' => $row_query['cedula_tipo'],
  'n_empleado' => $row_query['n_empleado'],
  'ced_pro' => $row_query['cedula_profesional'],
  'titulo' => $row_query['titulo'],
  'ap_p' => $row_query['ap_p'],
  'ap_m' => $row_query['ap_m'],
  'nombres' => $row_query['nombres'],
  'turno' => $row_query['turno'],
  'telefono' => $row_query['telefono'],
  'direccion' => $row_query['direccion'],
  'tipo_medico' => $row_query['tipo_medico'],
  'id_servicio1' => $row_query['id_servicio1'],
  'id_servicio2' => $row_query['id_servicio2'],
  'hora_entrada' => $row_query['hora_entrada'],
  'hora_salida' => $row_query['hora_salida'],
  'intervalo_citas0' => $row_query['intervalo_citas0'],
  'intervalo_citas1' => $row_query['intervalo_citas1'],
  'intervalo_citas2' => $row_query['intervalo_citas2'],
  'observaciones' => $row_query['observaciones'],
  'st' => $row_query['st']
  );
  }
  return $ret;
  } */

function DatosUsuario($idUsuario) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from usuarios_contrarreferencias where id_usuario=$idUsuario";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0)
        $ret = mysql_fetch_assoc($query);
    return $ret;
}

function getEdadXfechaNac($fecha) {
    if (strlen($fecha) == 10) {
        $diaHoy = date('j');
        $mesHoy = date('n');
        $anoHoy = date('Y');

        $ano = substr($fecha, 6, 4);
        $mes = substr($fecha, 3, 2);
        $dia = substr($fecha, 0, 2);
        if (($mes == $mesHoy) && ($dia > $diaHoy)) {
            $anoHoy = ($anoHoy - 1);
        }
        //si el mes es superior al actual tampoco habr� cumplido a�os, por eso le quitamos un a�o al actual
        if ($mes > $mesHoy) {
            $anoHoy = ($anoHoy - 1);
        }
        //ya no habria mas condiciones, ahora simplemente restamos los a�os y mostramos el resultado como su edad
        $edad = ($anoHoy - $ano);
    } else {
        $edad = "ND";
    }
    return $edad;
}

function getDiasXfechaNac($fecha) {
    if (strlen($fecha) == 10) {
        $diaHoy = date('j');
        $mesHoy = date('n');
        $anoHoy = date('Y');

        $ano = substr($fecha, 6, 4);
        $mes = substr($fecha, 3, 2);
        $dia = substr($fecha, 0, 2);
        if (($mes == $mesHoy) && ($dia > $diaHoy)) {
            $anoHoy = ($anoHoy - 1);
        }
        //si el mes es superior al actual tampoco habr� cumplido a�os, por eso le quitamos un a�o al actual
        if ($mes > $mesHoy) {
            $anoHoy = ($anoHoy - 1);
        }
        //ya no habria mas condiciones, ahora simplemente restamos los a�os y mostramos el resultado como su edad
        $edad = ($anoHoy - $ano);
        $edad = $edad * 365;
    } else {
        $edad = "ND";
    }
    return $edad;
}

function ListaDiags($sexo, $edad, $q) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    $cie_bd = "sistema_cie10";
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($cie_bd);
    $sql = "select * from cie_10_simef where (Sexo='$sexo' or sexo='Ambos') and (Decripcion like '%$q%' or Clave Like '$q%') ";
    $query = mysql_query($sql);
    $ret = "";
    if (mysql_num_rows($query) > 0) {
        while ($diag = mysql_fetch_array($query)) {
            if ($diag['LIM_INF'] != "" || $diag['LIM_SUP'] != "") {
                $edadDias = getDiasXfechaNac($edad);
                $edadAnios = getEdadXfechaNac($edad);
                if (substr($diag['LIM_INF'], strlen($diag['LIM_INF']) - 1) == "A") {
                    $edadMinDiag = substr($diag['LIM_INF'], 0, strlen($diag['LIM_INF']) - 1);
                    $bandEdadMin = $edadAnios >= $edadMinDiag;
                } else {
                    $edadMinDiag = substr($diag['LIM_INF'], 0, strlen($diag['LIM_INF']) - 1);
                    $bandEdadMin = $edadAnios >= $edadMinDiag;
                }
                if (substr($diag['LIM_SUP'], strlen($diag['LIM_SUP']) - 1) == "A") {
                    $edadMaxDiag = substr($diag['LIM_SUP'], 0, strlen($diag['LIM_SUP']) - 1);
                    $bandEdadMax = $edadAnios <= $edadMaxDiag;
                } else {
                    $edadMaxDiag = substr($diag['LIM_SUP'], 0, strlen($diag['LIM_SUP']) - 1);
                    $bandEdadMax = $edadAnios <= $edadMaxDiag;
                }
                if ($bandEdadMax && $bandEdadMin)
                    $ret.="<li><a href='javascript:regresarDiagnostico(\"" . $diag['Clave'] . "-" . $diag['Decripcion'] . "\");'>" . $diag['Decripcion'] . "</a></li>";
            }
            else {
                $ret.="<li><a href='javascript:regresarDiagnostico(\"" . $diag['Clave'] . "-" . $diag['Decripcion'] . "\");'>" . $diag['Decripcion'] . "</a></li>";
            }
        }
    }
    return $ret;
}

function ExisteMedico($idMedico) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from medicos where id_medico=" . $idMedico;
    $query = mysql_query($sql);
    if (mysql_num_fields($query) > 0)
        $ret = true;
    else
        $ret = false;
    return $ret;
}

function ObtenerMedicamentosReferidos($clinica, $fechaIni, $fechaFin) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "SELECT * FROM `medicamentos_contrarreferencias` AS m JOIN `contrarreferencias` AS c ON(m.`id_contrarreferencia`=c.`id_contra`) WHERE c.`id_unidad`=$clinica and c.fecha_contrarreferencia<=$fechaFin and c.fecha_contrarreferencia>=$fechaIni;";
    $query = mysql_query($sql);
    $ret = array();
    $i = 0;
    while ($row = mysql_fetch_assoc($query)) {
        $ret[$i]['id_medicamento'] = $row['id_medicamento'];
        $ret[$i]['id_contrarreferencia'] = $row['id_contrarreferencia'];
        $ret[$i]['id_derecho'] = $row['id_derecho'];
        $ret[$i]['dias'] = $row['dias'];
        $ret[$i]['cajas'] = $row['cajas'];
        $i++;
    }
    return $ret;
}

function ListaMedReferidos($clinica, $fechaIni, $fechaFin) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "SELECT id_medicamento FROM `medicamentos_contrarreferencias` AS m JOIN `contrarreferencias` AS c ON(m.`id_contrarreferencia`=c.`id_contra`) WHERE c.`id_unidad`=$clinica and c.fecha_contrarreferencia<=$fechaFin and c.fecha_contrarreferencia>=$fechaIni GROUP BY m.`id_medicamento`;";
    $query = mysql_query($sql);
    $ret = array();
    while ($row = mysql_fetch_assoc($query)) {
        $ret[] = $row;
    }
    return $ret;
}

function getMedicamentoXId($idMedicamento) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    $baseDatos_issste = "sistema_farmacia";
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from medicamentos where id_medicamento=$idMedicamento";
    $query = mysql_query($sql);
    $ret = array();
    while ($row = mysql_fetch_assoc($query)) {
        $ret = $row;
    }
    return $ret;
}

function ObtenerTratamientosCriticos($idClin, $fecha1, $fecha2) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "SELECT id_medicamento FROM (SELECT m.`id_medicamento` FROM medicamentos_contrarreferencias AS m WHERE activo=1 AND m.cronico=1 AND m.id_clinica=$idClin
UNION
SELECT m.`id_medicamento` FROM medicamentos_contrarreferencias AS m JOIN contrarreferencias AS c ON(m.`id_contrarreferencia`=c.`id_contra`) WHERE m.activo=1 AND m.cronico=0 AND m.fecha_inicio>='$fecha1' AND m.fecha_fin<='$fecha2' AND c.`status_citas`=0 AND c.`envio_clinica`=1 AND m.id_clinica=$idClin) AS medicamentos GROUP BY id_medicamento";
    $query = mysql_query($sql) or die(mysql_error());
    $ret = array();
    while ($row = mysql_fetch_assoc($query)) {
        $ret[] = $row['id_medicamento'];
    }
    return $ret;
}

function ObtenerPacientesXMed($idMed, $idClin, $fecha1, $fecha2) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from medicamentos_contrarreferencias where activo=1 and fecha_inicio>=$fecha1 and fecha_fin<=$fecha2 and id_medicamento=$idMed and cronico=0 and id_clinica=$idClin
            Union Select * from medicamentos_contrarreferencias where activo=1 and id_medicamento=$idMed and cronico=1 and id_clinica=$idClin";
    $query = mysql_query($sql);
    $ret = array();
    while ($row = mysql_fetch_assoc($query)) {
        $derecho = DatosDerechoHabiente($row['id_derecho']);
        $contra = getContraReferencia($row['id_contrarreferencia']);
        if ($row['cronico'] == 1)
            $cronico = "si";
        else
            $cronico = "no";
        //if(($contra['status_citas']==0 && $contra['envio_clinica']==1)&& $row['cronico']==0)
        $ret[] = array(
            'id_Tratamiento' => $row['id_Tratamiento'],
            'cedula' => $derecho['cedula'] . "/" . $derecho['cedula_tipo'],
            'nombreDH' => $derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres'],
            'unidad'=>$derecho['unidad_medica'],
            'fecha_N'=>$derecho['fecha_nacimiento'],
            'vigente' => date("d/m/Y", strtotime($row['fecha_inicio'])),
            'sexo'=>$derecho['sexo'],
            'cajas' => $row['cajas'],
            'cronico' => $cronico
        );
    }
    return $ret;
}

function ConteoMedicamentoXTiempo($clinica, $med, $fecha1, $fecha2) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from medicamentos_contrarreferencias where activo=1 and fecha_inicio>=$fecha1 and fecha_fin<=$fecha2 and id_medicamento=$med and cronico=0 and id_clinica=$clinica
            Union Select * from medicamentos_contrarreferencias where activo=1 and id_medicamento=$med and cronico=1 and id_clinica=$clinica";
    $query = mysql_query($sql);
    $cajas = 0;
    while ($medi = mysql_fetch_assoc($query)) {
        $cajas = $cajas + $medi['cajas'];
    }
    $ret = array(
        'id_med' => $med,
        'cajas' => $cajas
    );
    return $ret;
}

function ObtenerMedicamentosXReferencia($idRef) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "Select * from medicamentos_contrarreferencias where id_contrarreferencia=$idRef";
    $query = mysql_query($sql);
    $ret = array();
    while ($row = mysql_fetch_assoc($query)) {
        $ret[] = $row;
    }
    return $ret;
}

function ObtenerTratamientoXid($idTrat) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from medicamentos_contrarreferencias where id_Tratamiento=$idTrat";
    $query = mysql_query($sql);
    $ret = array();
    while ($row = mysql_fetch_assoc($query)) {
        $ret[] = $row;
    }
    return $ret;
}

function citasXderechohabiente($id_derecho) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    $hoy = date('Ymd');
    $bdissste = mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste);
    $query_query = "SELECT * FROM citas WHERE id_derecho='" . $id_derecho . "' ORDER BY fecha_cita ASC, id_horario ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_horario' => $row_query['id_horario'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getHorarioXid($id) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    //$hoy = date('Ymd');
    $bdissste = mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste);
    $query_query = "SELECT * FROM horarios WHERE id_horario='" . $id . "' ORDER BY id_horario";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_horario' => $row_query['id_horario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'dia' => $row_query['dia'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasOcupadasXdia($idHorario, $fechaCita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    //$hoy = date('Ymd');
    $bdissste = mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $idHorario . "' and fecha_cita='" . $fechaCita . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_derecho' => $row_query['id_derecho'],
            'id_cita' => $row_query['id_cita'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1'],
            'id_usuario' => $row_query['id_usuario'],
            'observaciones' => $row_query['observaciones'],
            'tipo_usuario' => $row_query['tipo_usuario']
        );
    } else {
        $ret = array(
            'id_derecho' => "-1",
            'id_cita' => "-1",
            'status' => "-1",
            'extra1' => "-1",
            'id_usuario' => "-1",
            'observaciones' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citaMedico($id_cita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste);
    $sqlD = "select * from citas_medico where id_cita=$id_cita";
    $query = mysql_query($sqlD);
    $res = array();
    if (mysql_num_rows($query) > 0)
        $res = mysql_fetch_assoc($query);
    return $res;
}

function getUsuarioXidAgenda($id) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    //$hoy = date('Ymd');
    $bdissste = mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste, $bdissste);
    $query_query = "SELECT * FROM usuarios WHERE id_usuario='" . $id . "' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error($bdissste));
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_usuario' => $row_query['id_usuario'],
            'login' => $row_query['login'],
            'pass' => $row_query['pass'],
            'nombre' => $row_query['nombre'],
            'tipo_usuario' => $row_query['tipo_usuario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'status' => $row_query['status']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function formatoDia($fecha, $paraDonde) {
    $dia = substr($fecha, 6, 2);
    $mes = substr($fecha, 4, 2);
    $ano = substr($fecha, 0, 4);
    $diaSem = date("N", mktime(0, 0, 0, $mes, $dia, $ano));
    if ($paraDonde == 'tituloCitasXdia') {
        $fechaO = diaSemana($diaSem) . " " . $dia . " DE " . strtoupper(tituloMes($mes)) . " DE " . $ano;
    }
    if ($paraDonde == 'imprimirCita') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    if ($paraDonde == 'citasAreprogramar') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    return $fechaO;
}

function getRecetasXderechoXcita($id_derecho, $id_medico, $id_servicio, $fecha) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    $ret = array();
    $bdissste = mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db("sistema_farmacia", $bdissste);
    $query_query = "SELECT * FROM recetas WHERE id_derecho='" . $id_derecho . "' AND id_medico='" . $id_medico . "' AND id_servicio='" . $id_servicio . "' AND fecha='" . $fecha . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_receta' => $row_query['id_receta'],
                'serie' => $row_query['serie'],
                'folio' => $row_query['folio'],
                'id_medico' => $row_query['id_medico'],
                'id_servicio' => $row_query['id_servicio'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha' => $row_query['fecha'],
                'hora' => $row_query['hora'],
                'codigo_barras' => $row_query['codigo_barras'],
                'entidad_federativa' => $row_query['entidad_federativa'],
                'clave_unidad_medica' => $row_query['clave_unidad_medica'],
                'numero_vale' => $row_query['numero_vale'],
                'dgp' => $row_query['dgp'],
                'id_usuario_elaboro' => $row_query['id_usuario_elaboro'],
                'id_usuario_surtio' => $row_query['id_usuario_surtio'],
                'status' => $row_query['status'],
                'extra1' => $row_query['extra1']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function formatoHora($hora) {
    return substr($hora, 0, 2) . ":" . substr($hora, 2, 2);
}

function citasExtXderechohabiente($id_derecho) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    $hoy = date('Ymd');
    $bdissste = mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_derecho='" . $id_derecho . "' AND fecha_cita<='" . $hoy . "' ORDER BY fecha_cita ASC, hora_inicio ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_horario'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function UnidadMedicaProcedencia($estado, $clinica) {
   global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from vgf where Estado='" . $estado . "'";
    $query = mysql_query($sql);
    $umf = "<option value='-1'>&nbsp;</option>";
    if (mysql_num_rows($query) > 0)
        while ($row = mysql_fetch_assoc($query)) {
            if ($row['unidad'] == 0)
                $clase = "clinica";
            else
                $clase = "umf";
            if ($row['id_unidad'] == $clinica)
                $umf.="<option value='" . $row['id_unidad'] . "' selected='selected' class='$clase'>" . htmlentities($row['nombre']) . "</option>";
            else
                $umf.="<option value='" . $row['id_unidad'] . "' class='$clase'>" . htmlentities($row['nombre']) . "</option>";
        }
    return $umf;
}

function CodigoPostalProc($estado, $municipio, $codigoPostal) {
   global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from codigo_postales where estado like '%" . $estado . "%' and municipio like '%" . $municipio . "%'";
    $query = mysql_query($sql);
    $ret = "<option value='-1'></option>";
    if (mysql_num_rows($query) > 0)
        while ($row = mysql_fetch_assoc($query)) {
            if ($codigoPostal == $row['cp'])
                $ret.="<option value='" . $row['cp'] . "' selected>" . $row['cp'] . "</option>";
            else
                $ret.="<option value='" . $row['cp'] . "'>" . $row['cp'] . "</option>";
        }
    return $ret;
}

function ColoniaProcedencia($estado, $municipio, $colonia) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from codigo_postales where estado like '%" . $estado . "%' and municipio like '%" . $municipio . "%' order by colonia ASC";
    $query = mysql_query($sql);
    $ret = "<option value='-1'></option>";
    if (mysql_num_rows($query) > 0)
        while ($row = mysql_fetch_assoc($query)) {
            if ($colonia == $row['colonia'])
                $ret.="<option value='" . $row['colonia'] . "' selected>" . ponerAcentos($row['colonia']) . "</option>";
            else
                $ret.="<option value='" . $row['colonia'] . "'>" . ponerAcentos($row['colonia']) . "</option>";
        }
    return $ret;
}

function CedulaDerechoHabiente($sexo, $tipoCedula) {
    $tpoCedHom = array("10", "40", "41", "50", "51", "70", "71", "90");
    $tpoDerHom = array("Trabajador", "Esposo", "Concubino", "Padre", "Abuelo", "Hijo", "Hijo de conyuge", "Pensionado");
    $tpoCedMuj = array("20", "30", "31", "32", "60", "61", "80", "81", "91");
    $tpoDerMuj = array("Trabajadora", "Esposa", "Concubina", "Mujer", "Madre", "Abuela", "Hija", "Hija de conyuge", "Pensionada");
    $tpoCedGen = Array("92", "99");
    $tpoDerGen = Array("Familiar de pensionado", "No derechohabiente");
    if ($sexo == 1) {
        $tpoCed = array_merge($tpoCedHom, $tpoCedGen);
        $tpoDer = array_merge($tpoDerHom, $tpoDerGen);
    } else {
        $tpoCed = array_merge($tpoCedMuj, $tpoCedGen);
        $tpoDer = array_merge($tpoDerMuj, $tpoDerGen);
    }
    $ret = "<option value='-1'></option>";
    for ($i = 0; $i < count($tpoCed); $i++) {
        if ($tpoCed[$i] == $tipoCedula)
            $ret.="<option value='" . $tpoCed[$i] . " selected>" . $tpoDer[$i] . "</option>";
        else {
            $ret.="<option value='" . $tpoCed[$i] . ">" . $tpoDer[$i] . "</option>";
        }
    }
    return $ret;
}

function QuitarEspaciosExtras($txt) {
    $txt = trim($txt);
    $ret = str_replace("  ", " ", $txt);
    if ($txt != $ret)
        $ret = QuitarEspaciosExtras($ret);
    return $ret;
}

function DatosCitaApartada($idCita) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from citas_apartadas where id_cita_ap=" . $idCita;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0) {
        $ret = mysql_fetch_assoc($query);
    }

    return $ret;
}

function ObtenerRefContra($idServ, $idDer) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $sql = "select * from referencia_contrarreferencia where id_servicio=$idServ and id_derecho=$idDer order by id_referencia DESC LIMIT 1";
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_num_rows($query) > 0) {
        $ret = mysql_fetch_assoc($query);
    }

    return $ret;
}

function CitasXMedicoDia($med, $fecha) {
    global $host_issste;
    global $usuario_issste;
    global $pass_issste;
    global $baseDatos_issste;
    mysql_connect($host_issste, $usuario_issste, $pass_issste);
    mysql_select_db($baseDatos_issste);
    $dia = diaSemana(date("w", strtotime($fecha)));
    $sql = "SELECT * FROM horarios AS h 
JOIN medicos AS m 
WHERE h.id_medico=$med AND h.dia='$dia' AND h.tipo_cita=0 AND m.`id_medico`=h.`id_medico`";
    $result = mysql_query($sql);
    $ret = array();
    while ($row = mysql_fetch_assoc($result)) {
        $medico = getMedicoXid($row['id_medico']);
        $sql = "select id_cita from citas where id_horario=" . $row['id_horario'] . " and fecha_cita='$fecha'";
        $query = mysql_query($sql);
        if (mysql_num_rows($query) == 0)
            $ret[] = array(
                'id_horario' => $row['id_horario'],
                'servicio' => $row['id_servicio'],
                'medico' => $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'],
                'id_medico' => $medico['id_medico'],
                'fecha' => $fecha,
                'dia' => $dia,
                'horaI' => $row['hora_inicio'],
                'horaF' => $row['hora_fin']
            );
    }
    return $ret;
}

?>
