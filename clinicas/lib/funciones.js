/*Funciones de clinicas
 * Version 1.5
 * ISSSTE Derechos Reservados
 */
//lista de palabras para verificar Cedula
var palInc = ["BUEI", "CACA", "CAGA", "CAKA", "COGE", "COJE", "COJO", "FETO", "JOTO", "BUEY", "CACO", "CAGO", "CAKO", "COJA", "COJI", "CULO", "GUEY", "KACA", "KACO", "KAGA", "KAGO", "KOGE", "KOJO", "KAKA", "KULO", "MAME", "MAMO", "MEAR", "MEAS", "MEON", "MION", "MOCO", "MULA", "PEDA", "PEDO", "PENE", "PUTA", "PUTO", "QULO", "RATA", "RUIN"];
var sustPalInc = ["BUEX", "CACX", "CAGX", "CAKX", "COGX", "COJX", "COJX", "FETX", "JOTX", "BUEX", "CACX", "CAGX", "CAKX", "COJX", "COJX", "CULX", "GUEX", "KACX", "KACX", "KAGX", "KAGX", "KOGX", "KOJX", "KAKX", "KULX", "MAMX", "MAMX", "MEAX", "MEAX", "MEOX", "MIOX", "MOCX", "MULX", "PEDX", "PEDX", "PENX", "PUTX", "PUTX", "QULX", "RATX", "RUIX"];

//Funciones del sistema
function mostrarDiv(div)
{
    capa = document.getElementById(div).style;
    capa.display = '';
}

function ocultarDiv(div)
{
    capa = document.getElementById(div).style;
    capa.display = 'none';
}

function enlaceJava()
{
    var xmlhttp = false;
    xmlhttp = new XMLHttpRequest();
    return xmlhttp;
}

function cargarLogin()
{
    $('#contenido').load('login.php');

}

function entrarSistema()
{
    var user = document.getElementById('usuario').value;
    var pass = document.getElementById('pass').value;
    var contenedor = document.getElementById('contenido');
    var objeto = enlaceJava();
    objeto.onreadystatechange = function() {
        if (objeto.readyState == 4)
        {
            var resp = objeto.responseText.split("|");
            if (resp[0] == "1")
            {
                var ventana = enlaceJava();
                ventana.onreadystatechange = function() {
                    if (ventana.readyState == 4)
                        contenedor.innerHTML = ventana.responseText;

                }
                ventana.open("GET", resp[2], true);
                ventana.send(null);
                switch (resp[2]) {
                    case "inicioConClin.php":
                        mostrarDiv('menuReferencia');
                        break;
                    case "inicioConsultClin.php":
                        mostrarDiv('menuVisor');
                        break;
                    case "admin/index.php":
                        alert("Va a ser redirigido al modulo administrativo de la clinica");
                        $(location).attr("href", resp[2] + "?idUsuario=" + resp[1]);
                        $.post(resp[2], "idUsuario=" + resp[1]);
                        break;
                    default:
                        alert("Va a ser redirigido al modulo de Contrarreferencias");
                        location.href = resp[2];
                }
                $("#clinica").load("nombreClinica.php");
            }
            else
            {
                alert("Nombre de Usuario o Contrase\u00f1a incorrectos");
                cargarLogin();
            }
        }
        if (objeto.readyState == 2)
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    objeto.open("GET", "accesoSist.php?login=" + user + "&pass=" + pass, true);
    objeto.send(null);
}

function verContrarreferencias()
{
    var contenedor = document.getElementById('Referencias');
    var objeto = enlaceJava();
    objeto.open("GET", "referenciasEnviadas.php", true);
    objeto.onreadystatechange = function() {
        if (objeto.readyState == 4)
        {
            contenedor.innerHTML = objeto.responseText;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    objeto.send(null);

}

function ConsultaReferencias()
{
    DesbloquearCita();
    var contenedor = $('#contenido');
    var objeto = enlaceJava();
    objeto.open("GET", "VerContrarreferencias.php", true);
    objeto.onreadystatechange = function() {
        if (objeto.readyState == 4)
        {
            $('#contenido').html(objeto.responseText);
            script = document.getElementsByTagName('script');
            eval(script);
        }
        else
            $('#contenido').html("<img src=\"diseno/loading.gif\">");
    }
    objeto.send(null);
}

function inicioClin()
{
    var contenedor = document.getElementById('contenido');
    var objeto = enlaceJava();
    objeto.open("GET", "inicioConClin.php", true);
    objeto.onreadystatechange = function() {
        if (objeto.readyState == 4)
        {
            contenedor.innerHTML = objeto.responseText;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    objeto.send(null);
}

function filtroBusqueda()
{
    var sel = $("#seleccion option:selected").val();
    var uTable = $("#contrarreferencias").dataTable({
        'bProcessing': true,
        'bDestroy': true,
        'bRetrieve': true,
        'sScrollY': '400px',
        'sScrollX': '100%',
        'bJQueryUI': true,
        'bPaginate': false,
        'bAutoWidth': false,
        'bScrollCollapse': false,
        'ordercolumn': '',
        'aaSorting': [[0, 'desc']],
        'sAjaxSource': 'ajaxnullsource.txt',
        'aoColumns': [
            {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
            {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
            {'bSearchable': true, 'bSortable': true},
            {'bSearchable': true, 'bSortable': true},
            {'bSearchable': true, 'bSortable': true},
            {'sClass': 'dt_center'},
            {'sClass': 'dt_center'}

        ],
        'oLanguage': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sInfo': 'Mostrando desde _START_ hasta _END_ de _TOTAL_ registros',
            'sInfoEmpty': 'Mostrando desde 0 hasta 0 de 0 registros',
            'sInfoFiltered': '(filtrado de _MAX_ registros en total)',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'oPaginate': {
                'sFirst': '&nbsp;Primero&nbsp;',
                'sPrevious': '&nbsp;Anterior&nbsp;',
                'sNext': '&nbsp;Siguiente&nbsp;',
                'sLast': '&nbsp;&Uacute;ltimo&nbsp;'
            }
        }
    });
    switch (sel)
    {

        case "0":
            uTable.fnReloadAjax('referenciasEnviadas.php');
            uTable.fnAdjustColumnSizing();
            break;
        case "1":
            uTable.fnReloadAjax("referenciasEnviadasCont.php");
            uTable.fnAdjustColumnSizing();
            break;
        case "-1":
            uTable.fnReloadAjax("referenciasEnviadasSinCont.php");
            uTable.fnAdjustColumnSizing();
            break;
    }

}

function buscarPor() {
    var sel = document.getElementById('tipo_busqueda').value;
    switch (sel)
    {
        case 'nombre':
            ocultarDiv('buscarPorCedula');
            //ocultarDiv('buscarPorMed');
            mostrarDiv('buscarPorNombre');
            document.getElementById('derechohabientes').innerHTML = "Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...";
            mostrarDiv('seleccionarDH');
            break;
        case 'cedula':
            ocultarDiv('buscarPorNombre');
            //ocultarDiv('buscarPorMed');
            mostrarDiv('buscarPorCedula');
            document.getElementById('derechohabientes2').innerHTML = "Ingrese los datos del derechohabiente y haga click en Buscar...";
            mostrarDiv('seleccionarDH');
            break;
        case 'medicamento':
            ocultarDiv('buscarPorNombre');
            ocultarDiv('buscarPorCedula');
            mostrarDiv('buscarPorMed');
            document.getElementById('medicamento').innerHTML = ">Ingrese el medicamento a buscar (clave o descripción) y presione buscar...";
            ocultarDiv('seleccionarDH');
            break;
        case 'todos':
            ocultarDiv('buscarPorNombre');
            ocultarDiv('buscarPorCedula');
            ocultarDiv('buscarPorMed');
            ocultarDiv('seleccionarDH');
            break;
    }
}

function buscarDHbusqueda(cedula) {
    if (cedula.length > 3) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes');
        var objeto = enlaceJava();
        objeto.open("GET", "buscarDHparaBusqueda.php?cedula=" + cedula, true);
        objeto.onreadystatechange = function()
        {
            if (objeto.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {

                contenedor.innerHTML = objeto.responseText;
                ind = document.getElementById('dh').selectedIndex;
                resp = document.getElementById('dh').options[ind].value;
                if (resp == "-1")
                    mostrarDiv('agreDH');
                MostrarDatosDH();
                activarInc();

            }
            if ((objeto.readyState == 1) || (objeto.readyState == 2) || (objeto.readyState == 3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe teclear al menos 4 caracteres');
    }
}

function buscarDHNbusqueda(ap_p, ap_m, nombre) {
    if ((ap_p.length > 0) || (ap_m.length > 0) || (nombre.length > 0)) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes2');
        var objeto = enlaceJava();
        objeto.open("GET", "buscarDHNparaBusqueda.php?ap_p=" + quitarAcentos(ap_p) + "&ap_m=" + quitarAcentos(ap_m) + "&nombre=" + quitarAcentos(nombre), true);
        objeto.onreadystatechange = function()
        {
            if (objeto.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
                MostrarDatosDH();
                activarInc();
            }
            if ((objeto.readyState == 1) || (objeto.readyState == 2) || (objeto.readyState == 3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe ingresar al menos un campo de busqueda (apellido paterno, apellido materno o nombre)');
    }
}

function quitarAcentos(Text)
{
    var cadena = "";
    var codigo = "";
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)
    {
        var Char = Text.charCodeAt(j);
        var cara = Text.charAt(j);
        if (cara == "&") {
            temp = Text.substring(j, j + 8);
            switch (temp) {
                case "&aacute;":
                    cadena += "(/a)";
                    j = j + 7;
                    break;
                case "&Aacute;":
                    cadena += "(/A)";
                    j = j + 7;
                    break;
                case "&eacute;":
                    cadena += "(/e)";
                    j = j + 7;
                    break;
                case "&Eacute;":
                    cadena += "(/E)";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "(/i)";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "(/I)";
                    j = j + 7;
                    break;
                case "&oacute;":
                    cadena += "(/o)";
                    j = j + 7;
                    break;
                case "&Oacute;":
                    cadena += "(/O)";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "(/u)";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "(/U)";
                    j = j + 7;
                    break;
                case "&ntilde;":
                    cadena += "(/n)";
                    j = j + 7;
                    break;
                case "&Ntilde;":
                    cadena += "(/N)";
                    j = j + 7;
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        }
        else {
            switch (Char)
            {
                case 225:
                    cadena += "(/a)";
                    break;
                case 233:
                    cadena += "(/e)";
                    break;
                case 237:
                    cadena += "(/i)";
                    break;
                case 243:
                    cadena += "(/o)";
                    break;
                case 250:
                    cadena += "(/u)";
                    break;
                case 193:
                    cadena += "(/A)";
                    break;
                case 201:
                    cadena += "(/E)";
                    break;
                case 205:
                    cadena += "(/I)";
                    break;
                case 211:
                    cadena += "(/O)";
                    break;
                case 218:
                    cadena += "(/U)";
                    break;
                case 241:
                    cadena += "(/n)";
                    break;
                case 209:
                    cadena += "(/N)";
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        }
        codigo += "_" + Text.charCodeAt(j);
    }
    return cadena;
}

function ponerAcentos(Text) {
    var cadena = "";
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)
    {
        var cara = Text.charAt(j);
        if (cara == "(") {
            temp = Text.substring(j, j + 4);
            switch (temp) {
                case "(/a)":
                    cadena += "&aacute;";
                    j = j + 3;
                    break;
                case "(/A)":
                    cadena += "&Aacute;";
                    j = j + 3;
                    break;
                case "(/e)":
                    cadena += "&eacute;";
                    j = j + 3;
                    break;
                case "(/E)":
                    cadena += "&Eacute;";
                    j = j + 3;
                    break;
                case "(/i)":
                    cadena += "&iacute;";
                    j = j + 3;
                    break;
                case "(/I)":
                    cadena += "&Iacute;";
                    j = j + 3;
                    break;
                case "(/o)":
                    cadena += "&oacute;";
                    j = j + 3;
                    break;
                case "(/O)":
                    cadena += "&Oacute;";
                    j = j + 3;
                    break;
                case "(/u)":
                    cadena += "&uacute;";
                    j = j + 3;
                    break;
                case "(/U)":
                    cadena += "&Uacute;";
                    j = j + 3;
                    break;
                case "(/n)":
                    cadena += "&ntilde;";
                    j = j + 3;
                    break;
                case "(/N)":
                    cadena += "&Ntilde;";
                    j = j + 3;
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        } else {
            cadena += Text.charAt(j);
        }
    }
    return cadena;
}

function ObtenerContraRef()
{
    if (!document.getElementById('dh')) {
        alert("Ingrese la cedula del derechohabiente y haga click en Buscar...");
    } else {
        valor = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
        valor = ponerAcentos(valor);
        if (valor == -1) {
            alert('No existe derechohabiente con la cedula introducida');
        } else {
            aValor = valor;
            mostrarDiv("citas");
            var idDer = aValor;
            var contenedor = document.getElementById('citas');
            var ref = enlaceJava();
            ref.open("GET", "mostrarReferencias.php?id_derecho=" + idDer, true);
            ref.onreadystatechange = function() {
                if (ref.readyState == 4)
                {
                    contenedor.innerHTML = ref.responseText;
                }
                if (ref.readyState == 2)
                {
                    contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            };
            ref.send(null);
        }
    }
}

function ConsultaReferenciasDH()
{
    DesbloquearCita();
    var contenedor = document.getElementById('contenido');
    var objeto = enlaceJava();
    objeto.open("GET", "buscarReferencias.php", true);
    objeto.onreadystatechange = function() {
        if (objeto.readyState == 4)
        {
            contenedor.innerHTML = objeto.responseText;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    objeto.send(null);
}

function VerReferencia(idRef)
{
    window.open("verFormatoContrarreferencia.php?folioContrarrefencia=" + idRef);
}

function agregarDH(cedula)
{
    var contenedor = document.getElementById('contenido');
    //var ced = document.getElementById('cedulaBuscar').value;
    var objeto = enlaceJava();
    objeto.open("GET", "AgregarDH.php?cedula=" + cedula, true);
    objeto.onreadystatechange = function() {
        if (objeto.readyState == 4)
        {
            alert("Antes de Agendar la cita al DerechoHabiente \n Favor de cotejar los datos con el SIPE");
            contenedor.innerHTML = objeto.responseText;
            $("#contenido").find("script").each(function(i) {
                eval($(this).text());
            });
            /*var estadoClinica = document.getElementById('estadoClinica').value;
             for (i = 0; i < listaEstados.length; i++)
             {
             if (listaEstados[i].toLowerCase() == estadoClinica)
             {
             estadoClinica = listaEstados[i];
             break;
             }
             }
             cargarEstados('estadoAgregar', estadoClinica);
             //document.getElementById('estadoAgregar').disabled = "disabled";
             cargarMunicipios(estadoClinica, 'municipioAgregar');
             //document.getElementById('estadoAgregar').value = estadoClinica;
             var municipio = $("#municipioClinica").val().toLowerCase();
             /*for (i = 0; i < estados[estadoClinica].length; i++)
             {
             if (estados[estadoClinica][i].toLowerCase() == municipio)
             {
             municipio = estados[estadoClinica][i];
             break;
             }
             }
             document.getElementById('municipioAgregar').value = municipio/
             obtenerCodigoPostal(estadoClinica, municipio, 'CodigoPostal');
             cargarColonias('colonia', 'estadoAgregar', municipioClinica);*/
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    objeto.send(null);
}

function enviarReferencia()
{
    eliminarCitas();
    var contenedor = document.getElementById('contenido');
//    $.ajax({});
    var objeto = enlaceJava();
    objeto.open("GET", "buscarDhParaReferencia.php", true);
    objeto.onreadystatechange = function() {
        if (objeto.readyState == 4)
        {
            contenedor.innerHTML = objeto.responseText;
            $("#contenido").find("script").each(function(i) {
                eval($(this).text());
            });
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    objeto.send(null);
}

function cargarEstados(elemento, estado) {
    var obj = $("#" + elemento);
    $.ajax({
        url: "cargarEstados.php",
        data: {
            estado: estado
        },
        success: function(data, textStatus, jqXHR) {
            obj.append(data);
        }
    });
}

function cargarMunicipios(estado, elemento) {
    vaciarSelect(elemento);
    var obj = $("#" + elemento);
    vaciarSelect(elemento);
    $.ajax({
        url: "cargarMunicipios.php",
        data: {
            estado: estado,
            municipio: undefined
        },
        success: function(data, textStatus, jqXHR) {
            obj.append(data);
        }
    });
    //ObtenerUnidadesMedicas(estado);
}

function cargarMunicipios1(estado, elemento, municipio) {
    vaciarSelect(elemento);
    var obj = $("#" + elemento);
    $.ajax({
        url: "cargarMunicipios.php",
        data: {
            estado: estado,
            municipio: municipio
        },
        success: function(data, textStatus, jqXHR) {
            obj.append(data);
        }
    });
    //ObtenerUnidadesMedicas(estado);
}

function vaciarSelect(elemeto)
{
    opciones = document.getElementById(elemeto).length;
    lista = document.getElementById(elemeto);
    while (opciones > 0) {
        lista.options[opciones - 1] = null;
        opciones = document.getElementById(elemeto).length;
    }
}

function ObtenerTpoDerXSexo(elemento, elemeto)
{
    ind = document.getElementById(elemento).selectedIndex;
    var sex = document.getElementById(elemento).options[ind].value;
    var tpoCedHom = new Array("10", "40", "41", "50", "51", "70", "71", "90");
    var tpoDerHom = new Array("Trabajador", "Esposo", "Concubino", "Padre", "Abuelo", "Hijo", "Hijo de conyuge", "Pensionado");
    var tpoCedMuj = new Array("20", "30", "31", "32", "60", "61", "80", "81", "91");
    var tpoDerMuj = new Array("Trabajadora", "Esposa", "Concubina", "Mujer", "Madre", "Abuela", "Hija", "Hija de conyuge", "Pensionada");
    var tpoCedGen = new Array("92", "99");
    var tpoDerGen = new Array("Familiar de pensionado", "No derechohabiente");
    var lista = document.getElementById(elemeto);
    opciones = document.getElementById(elemeto).length;
    vaciarSelect(elemeto);
    opcion = new Option("", "-1");
    lista.appendChild(opcion);
    if (sex == "H") {
        for (i = 0; i < tpoCedHom.length; i++)
        {
            opcion = new Option(tpoCedHom[i] + " " + tpoDerHom[i], tpoCedHom[i], "", "");
            lista.appendChild(opcion);
        }
    }
    else
    {
        for (i = 0; i < tpoCedMuj.length; i++)
        {
            opcion = new Option(tpoCedMuj[i] + " " + tpoDerMuj[i], tpoCedMuj[i], "", "");
            lista.appendChild(opcion);
        }
    }
    for (x = 0; x < tpoDerGen.length; x++)
    {
        opcion = new Option(tpoCedGen[x] + " " + tpoDerGen[x], tpoCedGen[x], "", "");
        lista.appendChild(opcion);
    }
}

function verCitasDisp()
{
    var contenedor = document.getElementById('citasDisponibles');
    ind = document.getElementById('servicio').selectedIndex
    var idServ = document.getElementById('servicio').options[ind].value;
    var ajax = enlaceJava();
    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            window.setInterval(verCitasDisp, 60);
            contenedor.innerHTML = ajax.responseText;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", idServ, true);
    ajax.send(null);
}

function discapacidad()
{
    incapacidad = document.getElementById('inca').options[document.getElementById('inca').selectedIndex].value;
    if (incapacidad == 1)
    {
        mostrarDiv('fechaInc');
    }
    else
        ocultarDiv('fechaInc');
}

function fecha()
{
    $('#fechaFinInc').Datepicker();
}

function calendarios(elemento)
{
    var mes = $("#mes" + elemento + " option:selected").val();
    vaciarSelect("dia" + elemento);
    switch (mes)
    {
        case '01':
            dias = 31;
            break;
        case '02':
            anio = document.getElementById('anio' + elemento).value;
            bisiesto = anio % 4;
            if (bisiesto == 0)
                dias = 29;
            else
                dias = 28;
            break;
        case '03':
            dias = 31;
            break;
        case '04':
            dias = 30;
            break;
        case '05':
            dias = 31;
            break;
        case '06':
            dias = 30;
            break;
        case '07':
            dias = 31;
            break;
        case '08':
            dias = 31;
            break;
        case '09':
            dias = 30;
            break;
        case '10':
            dias = 31;
            break;
        case '11':
            dias = 30;
            break;
        case '12':
            dias = 31;
            break;
    }
    for (i = 1; i <= dias; i++)
    {
        if (i < 10)
            opcion = new Option(i, "0" + i);
        else
            opcion = new Option(i, i);
        document.getElementById("dia" + elemento).options[i - 1] = opcion;
    }

}

function calenDia(elemento, diaAc)
{
    calendarios(elemento);
    dia = document.getElementById("dia" + elemento);
    for (i = 0; i < dia.length; i++)
        if (dia.options[i].value == diaAc)
        {
            dia.selectedIndex = i;
            break;
        }
}

function apartarCita()
{
    eliminarCitas();
    var derecho = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
    var aDer = derecho.split("|");
    var inca = document.getElementById('inca').value;
    var fecha = "";
    if (inca == 1) {
        fechaRev = new Date(document.getElementById('anio1').value, document.getElementById('mes1').options[document.getElementById('mes1').selectedIndex].value, document.getElementById('dia1').options[document.getElementById('dia1').selectedIndex].value);
        fecha = document.getElementById('anio1').value +
                document.getElementById('mes1').options[document.getElementById('mes1').selectedIndex].value +
                document.getElementById('dia1').options[document.getElementById('dia1').selectedIndex].value;
        hoy = new Date();
        if (hoy > fechaRev)
        {
            alert("La fecha de incapacidad es menor o ya termino su incapacidad");
            exit();
        }
    }
    var servicio = document.getElementById('servicio').options[document.getElementById('servicio').selectedIndex].value;
    $.ajax({
        url: "checarTratamiento.php",
        data: {
            servicio: servicio,
            id_derecho: aDer[0]
        },
        success:
                function(data) {
                    aData = data.split("|");
                    if (aData[0] == 1) {
                        mostrarDiv('citas');
                        var object = enlaceJava();
                        object.open("GET", "buscarCitas.php?id_derecho=" + aDer[0] + "&incapacidad=" + inca + "&fecha_inca=" + fecha + "&servicio=" + servicio, true);
                        object.onreadystatechange = function()
                        {
                            if (object.readyState == 4)
                            {
                                document.getElementById('citas').innerHTML = object.responseText;
                            }
                            else
                                document.getElementById('citas').innerHTML = "<img src=\"diseno/loading.gif\">";
                        }
                        object.send(null);
                    }
                    else
                        eval(aData[1]);
                }
    });
}

function elegirCita(idDerecho, idhorario, fecha, horario)
{
    var contenedor = document.getElementById('citas');
    var conf = confirm("Desea elegir la cita el dia de " + fecha + " con horario de " + horario);
    if (conf) {
        var objeto = enlaceJava();
        objeto.open("GET", "agregarCitaConfirmar.php?idDerecho=" + idDerecho + "&fecha=" + fecha + "&horario=" + idhorario, true);
        objeto.onreadystatechange = function() {
            if (objeto.readyState == 4)
            {

                var aResp = objeto.responseText.split("|");
                if (aResp.length > 2) {
                    var docu = enlaceJava();
                    docu.open("GET", "DocumentosReferencia.php?idDerecho=" + aResp[2] + "&idCita=" + aResp[0] + "&idHorario=" + aResp[1] + "&fechaCita=" + aResp[3], true);
                    docu.onreadystatechange = function() {
                        if (docu.readyState == 4) {
                            //setCookie("citas", aResp, 0.0104, "/", 0);
                            contenedor.innerHTML = docu.responseText;
                            $("#contenido").find("script").each(function(i) {
                                eval($(this).text());
                            });
                            contenedor.onHTMLUpdate = function() {
                                eliminarCitas();
                            };
                        }
                    }
                    docu.send(null);
                }
                else
                {
                    alert(aResp[1]);
                    apartarCita();
                }
            }
            else
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
        objeto.send(null);
    }
    else
        apartarCita();
}

function cancelarCita()
{
    var cita = document.getElementById('idCita').value;
    var contenedor = document.getElementById('citas');
    var objeto = enlaceJava();
    objeto.onreadystatechange = function() {
        if (objeto.readyState == 4)
        {
            contenedor.innerHTML = objeto.responseText;
            borrarcookie("cita");
            apartarCita();
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    objeto.open("GET", "cancelarCita.php?idCita=" + cita, true);
    objeto.send(null);
}

function apartarCitaConfirmar()
{
    var archivoRef = document.getElementById('referencia').value;
    var archivoLab = document.getElementById('laboratorio').value;
    var archivoRx = document.getElementById('Placas').value;
    var archivoEco = document.getElementById('Ecos').value;
    var diag = document.getElementById('diagnostico').value;
    if (diag.match(/^[a-zA-Z][0-9]+[.]*[0-9]*$/) || diag.length < 4)
    {
        alert("diagnostico invalido");
        return;
    }
    if (diag == "otro")
        diag = document.getElementById('diagnostico1').value;
    if (archivoRef == "")
        alert("Seleccione el archivo de referencia");
    else
    if (diag == "")
        alert("ingrese el diagnostico del paciente");
    else
    {
        $("#citaRealizada").val("1");
        document.forms['FormCita'].submit();
        /*var cita = getCookie("cita");
         if (cita != "undefined")
         del_cookie("cita");*/
        document.getElementById('citas').innerHTML = "";
        document.getElementById('cedulaBuscar').value = "";
        document.getElementById('derechohabientes').innerHTML = "Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...";
        document.getElementById('ap_pB').value = "";
        document.getElementById('ap_mB').value = "";
        document.getElementById('nombreB').value = "";
        document.getElementById('derechohabientes2').innerHTML = "Ingrese los datos del derechohabiente y haga click en Buscar...";
    }
}

function validarCedula(val) {
    out = "";
    if (val.length != 10)
        out = 'La cedula debe ser de 10 caracteres';
    else {
        l1 = val.charAt(0);
        l2 = val.charAt(1);
        l3 = val.charAt(2);
        l4 = val.charAt(3);
        ano = val.charAt(4) + val.charAt(5);
        mes = val.charAt(6) + val.charAt(7);
        dia = val.charAt(8) + val.charAt(9);
        if (!esConsonante(l1))
            out = 'El primer caracter de la cedula debe ser una consonante.\n';
        else if (!esVocal(l2))
            out = 'El segundo caracter de la cedula debe ser una vocal.\n';
        else if (!esConsonante(l3))
            out = 'El tercer caracter de la cedula debe ser una consonante.\n';
        else if (!esConsonante(l4))
            out = 'El cuarto caracter de la cedula debe ser una consonante.\n';
        else if (!esFechaValida(dia + "/" + mes + "/19" + ano))
            out = 'La fecha de la cedula es invalida.\n';
    }
    return out;
}

function esFechaValida(fecha) {
    if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha)) {
        return false;
    }
    var dia = parseInt(fecha.substring(0, 2), 10);
    var mes = parseInt(fecha.substring(3, 5), 10);
    var anio = parseInt(fecha.substring(6), 10);

    switch (mes) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            numDias = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            numDias = 30;
            break;
        case 2:
            numDias = 29;
            break;
        default:
            return false;
    }

    if (dia > numDias || dia == 0) {
        return false;
    }
    return true;
}

function esConsonante(valor) {
    var conso = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', '&ntilde;', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    tConso = conso.length;
    seEncontro = false;
    for (i = 0; i < tConso; i++)
        if (conso[i] == valor.toUpperCase())
            seEncontro = true;
    return seEncontro;
}

function esVocal(valor) {
    var vocal = new Array('A', 'E', 'I', 'O', 'U');
    tVocal = vocal.length;
    seEncontro = false;
    for (i = 0; i < tVocal; i++)
        if (vocal[i] == valor.toUpperCase())
            seEncontro = true;
    return seEncontro;
}

function validarAgregarDH()
{
    var cedula = $("#cedulaAgregar").val();
    var sexo = $("#sexo").val();
    var tpder = $("#cedulaTipoAgregar").val();
    tpoder = parseInt(tpder);
    var app = quitarAcentos($("#ap_pAgregar").val());
    var apm = quitarAcentos($("#ap_mAgregar").val());
    var nombre = quitarAcentos($("#nombreAgregar").val());
    var letras = /^[a-zA-Z(/)]+$/;
    var alfanumerico = /^[a-zA-Z0-9(/)]+$/;
    var direccion = quitarAcentos($("#direccionAgregar").val());
    var fecha = $("#fecha_nacAgregar").val();
    var registro = 1;
    var mensajes = [];
    if (sexo != "-1") {
        switch (tpoder)
        {
            case 10:
            case 20:
            case 90:
            case 91:
                cedulaTrab = validarCedulaTrabajador(cedula, nombre, app, apm, fecha);
                if (!cedulaTrab)
                {
                    registro = 0;
                    mensajes.push('Verifique la fecha de Nacimiento o los datos del Derechohabiente');
                }
                break;
            case -1:
                registro = 0;
                mensajes.push("Tipo de Cedula");
                break;
            default:
                ced = validarCedula(cedula)
                if (ced != "")
                {
                    registro = 0;
                    mensajes.push(ced);
                }

        }
    }
    else
    {
        registro = 0;
        mensajes.push("sexo del DerechoHabiente");
    }
    if (!letras.test(app))
    {
        registro = 0;
        mensajes.push("Apellido paterno");
    }
    if (!letras.test(apm))
    {
        registro = 0;
        mensajes.push("Apellido materno");
    }
    if (nombre.length < 2)
    {
        registro = 0;
//        alert(nombre);
        mensajes.push("Nombre del DerechoHabiente");
    }
    if (direccion.length < 4)
    {
//        alert()
        registro = 0;
        mensajes.push("Direccion");
    }
    var estado = $("#estadoAgregar option:selected").val();
    var municipio = $("#municipioAgregar option:selected").val();
    var cp = $("#codigoPostal option:selected").val();
    var colonia = $("#colonia option:selected").val();
    if (estado == "-1")
    {
        registro = 0;
        mensajes.push("Estado");
    }
    if (municipio == "-1")
    {
        registro = 0;
        mensajes.push("Municipio");
    }
    if (colonia == "-1")
    {
        registro = 0;
        mensajes.push("Colonia");
    }
    if (cp == "-1")
    {
        registro = 0;
        mensajes.push("Codigo Postal");
    }
    var telefono = /[0-9]{10}/;
    if (!telefono.test($("#telefonoAgregar").val()))
    {
        registro = 0;
        mensajes.push("Telefono");
    }
    if ($("#cel").val() != "")
    {
        if (!telefono.test($("#cel").val()))
        {
            registro = 0;
            mensajes.push("Telefono");
        }
    }
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if ($("#email").val() != "")
        if (!re.test($("#email").val()))
        {
            registro = 0;
            mensajes.push("Correo Electrónico valido");
        }
    return [registro, mensajes];
}

function fechas(elEvento, obj) {
    var evento = elEvento || window.event;
    if (evento.keyCode == 8) {
    }
    else {
        var fecha = obj;
        if (fecha.value.length == 2 || fecha.value.length == 5) {
            fecha.value += "/";
        }
    }
}

function regresar()
{
    var contenedor = document.getElementById('contenido');
    var citas = enlaceJava();
    citas.open("GET", "inicioConClin.php", true);
    citas.onreadystatechange = function() {
        if (citas.readyState == 4)
        {
            contenedor.innerHTML = citas.responseText;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    citas.send(null);
}

function buscarCitas()
{
    $.ajax({
        url: "VerCitas.php",
        beforeSend: function(xhr) {
            $("#contenido").html("<img src=\"diseno/loading.gif\">");
        },
        success: function(data) {
            $("#contenido").html(data);
            $("#contenido").find("script").each(function(i) {
                eval($(this).text());
            });
        }
    });
}

function filtrarCitas()
{
    ind = document.getElementById('selectFiltro').selectedIndex;
    tipo = parseInt(document.getElementById('selectFiltro').options[ind].value);
    switch (tipo) {
        case 1:
            ocultarDiv('folio');
            mostrarDiv('busquedaFecha');
            break;
        case 7:
            mostrarDiv('folio');
            ocultarDiv('busquedaFecha');
            break;
        default:
            ocultarDiv('busquedaFecha');
            ocultarDiv('folio');
            mostrarCitas();
    }
}

function mostrarCitas()
{

    var tipo = parseInt($('#selectFiltro option:selected').val());
    var fecha1 = "", fecha2 = "", umf = "", folio = "";
    switch (tipo)
    {

        case 1:
            fecha1 = $("#fecha1").val();
            fecha2 = $("#fecha2").val();
            break;
        case 7:
            folio = $("#folio_field").val();
            break;
        case -1:
            alert("seleccione Filtro")
            return;
            break;
        case 3:
            var uTable = $("#citasApartadas").dataTable({
                'bDestroy': true,
                'bRetrieve': false,
                'bProcessing': true,
                'sScrollY': '400px',
                'sScrollX': '100%',
                'bJQueryUI': true,
                'bPaginate': false,
                'bAutoWidth': false,
                'bScrollCollapse': false,
                'ordercolumn': '',
                'aaSorting': [[0, 'desc']],
                'aoColumns': [
                    {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
                    {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
                    {'bSearchable': true, 'bSortable': true},
                    {'bSearchable': true, 'bSortable': true},
                    {'bSearchable': true, 'bSortable': true},
                    {'sClass': 'dt_center'},
                    {'sClass': 'dt_center'},
                    {'sClass': 'dt_center'},
                    {'sClass': 'dt_center'},
                    {'sClass': 'dt_center'},
                    {'sClass': 'dt_center'}

                ],
                'oLanguage': {
                    'sProcessing': 'Procesando...',
                    'sLengthMenu': 'Mostrar _MENU_ registros',
                    'sZeroRecords': 'No se encontraron resultados',
                    'sInfo': 'Mostrando desde _START_ hasta _END_ de _TOTAL_ registros',
                    'sInfoEmpty': 'Mostrando desde 0 hasta 0 de 0 registros',
                    'sInfoFiltered': '(filtrado de _MAX_ registros en total)',
                    'sInfoPostFix': '',
                    'sSearch': 'Buscar en resultado:',
                    'sUrl': '',
                    'oPaginate': {
                        'sFirst': '&nbsp;Primero&nbsp;',
                        'sPrevious': '&nbsp;Anterior&nbsp;',
                        'sNext': '&nbsp;Siguiente&nbsp;',
                        'sLast': '&nbsp;&Uacute;ltimo&nbsp;'
                    }
                },
                'sAjaxSource': 'MostrarCitasApartadas.php?filtro=3'
            });
            sUrl_source_ajax = '';
            //uTable.fnReloadAjax("");
            break;

    }
    if (tipo != 3)
        var uTable = $("#citasApartadas").dataTable({
            'bDestroy': true,
            'bRetrieve': false,
            'bProcessing': true,
            'sScrollY': '400px',
            'sScrollX': '100%',
            'bJQueryUI': true,
            'bPaginate': false,
            'bAutoWidth': false,
            'bScrollCollapse': false,
            'ordercolumn': '',
            'aaSorting': [[0, 'desc']],
            'aoColumns': [
                {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
                {'sClass': 'dt_right', 'bSearchable': true, 'bSortable': true},
                {'bSearchable': true, 'bSortable': true},
                {'bSearchable': true, 'bSortable': true},
                {'bSearchable': true, 'bSortable': true},
                {'sClass': 'dt_center'},
                {'sClass': 'dt_center'},
                {'sClass': 'dt_center'},
                {'sClass': 'dt_center'},
                {'sClass': 'dt_center'},
                {'sClass': 'dt_center'}

            ],
            'oLanguage': {
                'sProcessing': 'Procesando...',
                'sLengthMenu': 'Mostrar _MENU_ registros',
                'sZeroRecords': 'No se encontraron resultados',
                'sInfo': 'Mostrando desde _START_ hasta _END_ de _TOTAL_ registros',
                'sInfoEmpty': 'Mostrando desde 0 hasta 0 de 0 registros',
                'sInfoFiltered': '(filtrado de _MAX_ registros en total)',
                'sInfoPostFix': '',
                'sSearch': 'Buscar en resultado:',
                'sUrl': '',
                'oPaginate': {
                    'sFirst': '&nbsp;Primero&nbsp;',
                    'sPrevious': '&nbsp;Anterior&nbsp;',
                    'sNext': '&nbsp;Siguiente&nbsp;',
                    'sLast': '&nbsp;&Uacute;ltimo&nbsp;'
                }
            },
            'sAjaxSource': "MostrarCitasApartadas.php?filtro=" + tipo + "&clinica=" + umf + "&fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&folio=" + folio
        });
    sUrl_source_ajax = '';
    //uTable.fnReloadAjax("MostrarCitasApartadas.php?filtro=" + tipo + "&clinica=" + umf + "&fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&folio=" + folio);
}


function regresarDiagnostico(diag)
{
    $("#diagnostico").val(diag);
    if (diag == "otro")
    {
        ocultarDiv('resultDiag');
        mostrarDiv("oDiag");
    }
    document.getElementById('resultados').innerHTML = "";
}

function activarInc()
{
    var derecho = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
    var aDer = derecho.split("|");
    switch (aDer[1])
    {
        case "10":
        case "20":
            mostrarDiv('incapacidad');
            break;
        default:
            ocultarDiv('incapacidad');
    }
}

function validarCedulaTrabajador(cedula, nombre, app, apm, fecha)
{
    var valCedula = cedula;
    var aNombres = nombre.split(" ");
    var nombres, cedulaGen, ap_p, ap_m;
    /*if(aNombres.length>=2){
     switch(aNombres[0])
     {
     case "MA":case "MARIA":case "MA.":case "JOSE":case "DA":case "DAS":case "DE":case "DEL":case "DER":case "DI":case "DIE":case "DD":case "EL":case "LA":case "LOS":case "LAS":case "MAC":case "MC":case "VAN":case "VON":case "Y":
     try{
     nombres=aNombres[1];
     }
     catch(ex){
     nombres="X";
     }
     break;
     default:
     nombres=aNombres[0];
     }
     if(aNombres.length>2)
     for(i=1;i<aNombres.length;i++)
     switch (aNombres[i])
     {
     case "LOS":case "LAS":case "DE":
     nombre=aNombres[i+1];
     break;
     default:
     nombre=aNombres[i];
     break;	
     }
     }*/
    if (app != undefined) {
        var aAp = app.split(" ");
        i = 0;
        band = 0;
        while (i < aAp.length && band == 0)
        {
            switch (aAp[i])
            {
                case "DA":
                case "DAS":
                case "DE":
                case "DEL":
                case "DER":
                case "DI":
                case "DIE":
                case "DD":
                case "EL":
                case "LA":
                case "LOS":
                case "LAS":
                case "MAC":
                case "MC":
                case "VAN":
                case "VON":
                case "Y":
                    /* try{
                     ap_p=aAp[i+1];
                     }
                     catch(ex){
                     ap_p="X";
                     }*/
                    break;
                default:
                    ap_p = aAp[i];
                    band = 1;
            }
            i++;
        }
    }
    else
        ap_p = apm;
    if (apm != "" && apm != ".") {
        var aAm = apm.split(" ");
        i = 0;
        band = 0;
        while (i < aAm.length && band == 0)
        {
            switch (aAm[i])
            {
                case "DA":
                case "DAS":
                case "DE":
                case "DEL":
                case "DER":
                case "DI":
                case "DIE":
                case "DD":
                case "EL":
                case "LA":
                case "LOS":
                case "LAS":
                case "MAC":
                case "MC":
                case "VAN":
                case "VON":
                case "Y":
                    /* try{
                     ap_m=aAm[1];
                     }
                     catch(ex){
                     ap_m="X";
                     }*/
                    break;
                default:
                    ap_m = aAm[i];
                    band = 1;
            }
            i++;
        }
    }
    else
        ap_m = "X";
    var cedulaaP = "";

    if (ap_p[0] == "Ñ")
        cedulaaP = "X";
    else
        cedulaaP = ap_p[0];
    for (var i = 1; i < ap_p.length; i++)
    {
        if (esVocal(ap_p[i]))
        {
            cedulaGen = cedulaaP + ap_p[i];
            break;
        }
    }

    var iniCed = "";
    if (ap_m != "X") {
        cedulaGen = cedulaGen + ap_m[0];
        iniCed = valCedula.toString().substring(0, 3);
    }
    else
    {
        iniCed = valCedula.toString().substring(0, 2);
    }
    /*   for(i=0;i<palInc.length;i++)
     {
     if(palInc[i]==cedulaGen)
     {
     cedulaGen=sustPalInc[i];
     break;
     }
     }*/

    var cedulaAP = cedulaGen;
    var aFecha = fecha.split("/");
    var fechaCedula = valCedula.toString().substring(4);
    var fechaGen = aFecha[2].substr(2, 2) + aFecha[1] + aFecha[0];
    //cedulaGen+=fechaGen;
    if (iniCed == cedulaGen)
    {
        if (fechaCedula == fechaGen)
            return true;
        else
        {
            alert("Fecha Incorrecta de Nacimiento \u00f3 c\u00e9dula incorrecta \n Favor de Verificar");
            return false;
        }
    }
    else
    {
        if (cedulaAP != iniCed && fechaCedula != fechaGen)
            alert("Cedula incorrecta \n Favor de Verificar");
        else
        if (cedulaAP != iniCed)
            alert("Nombre o Apellidos incorrectos, \n Favor de verificar");
        else
        if (fechaCedula != fechaGen)
            alert("Fecha Incorrecta de Nacimiento \u00f3 c\u00e9dula incorrecta \n Favor de Verificar");
        return false;
    }
}

function validarFechaCedula(cedula, fechaNac, tipoCed)
{

    var ret = false;
    var fechaCed = cedula.toString().substring(4);
    var aFecha = fechaNac.split("/");
    fechaNac = aFecha[2].toString().substring(1) + aFecha[1] + aFecha[0] + "";
    if ((tipoCed == 60 || tipoCed == 61) || (tipoCed == 50 || tipoCed == 51)) {
        if (fechaNac < fechaCed)
            alert("La fecha de Nacimiento debe ser mayor a la del trabajador");
        else
            ret = true;
    }
    else {
        if (fechaNac > fechaCed)
            alert("La fecha de Nacimiento debe ser menor a la del trabajador");
        else
            ret = true;
    }

    return ret;
}

function obtenerCodigoPostal(edo, mun, col)
{
    vaciarSelect('codigoPostal');
    var contenedor = document.getElementById('codigoPostal');
    var municipio = document.getElementById(mun).value;
    var estado = document.getElementById(edo).value;
    var colonia = document.getElementById(col).value;
    var seleccionado = contenedor.value;
    var ajax = enlaceJava();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
            contenedor.value = seleccionado;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    if (isset(municipio))
        ajax.open("GET", "codigos_postales.php?estado=" + estado + "&municipio=" + municipio + "&cp=" + colonia, true);
    else
        ajax.open("GET", "codigos_postales.php?estado=" + estado + "&municipio=''&cp=" + colonia, true);
    ajax.send(null);
}


function obtenerCodigoPostal1(edo, mun, col)
{
    vaciarSelect('codigoPostal');
    var contenedor = document.getElementById('codigoPostal');
    var municipio = document.getElementById(mun).value;
    var estado = document.getElementById(edo).value;
    var colonia = document.getElementById(col).value;
    var seleccionado = contenedor.value;
    var ajax = enlaceJava();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
            contenedor.value = seleccionado;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", "codigos_postales.php?estado=" + estado + "&municipio=" + municipio + "&col=" + colonia, true);
    ajax.send(null);
}

function cargarColonias(colonia, estado, mun)
{
    vaciarSelect(colonia);
    var codigo = document.getElementById(estado).value;
    var municipio = document.getElementById(mun).value;
    var contenedor = document.getElementById(colonia);
    var seleccionado = document.getElementById(colonia).value;
    vaciarSelect(colonia);
    var ajax = enlaceJava();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
            contenedor.value = seleccionado;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", "mostrarColonias.php?estado=" + codigo + "&municipio=" + municipio);
    ajax.send(null);
}

function checarClinicaDH()
{
    var dh = $("#dh option:selected").val();
    var aDh = dh.split("|");
    $.ajax({
        url: "checarClinica.php",
        data: "idDh=" + aDh[0],
        type: "get",
        success: function(resp)
        {
            if (resp == "change")
            {
                var cambio = confirm("Desea cambiar al Derechohabiente a esta Cllinica");
                if (cambio)
                {
                    window.open("cambiarClinica.php?idDer=" + aDh[0], "CambioClinica", "height=400px,width=300px,resizable=0");
                }
            }
            apartarCita();
        }
    });

}

function checarTextoDocs(input, label)
{
    var texto = document.getElementById(input).value;
    patronInc = /^[x]+/;
    if (texto.match(patronInc))
    {
        document.getElementById(label).value = "Datos invalidos";
        document.getElementById(apartarCita).disabled = "disabled";
    }
    else
    {
        document.getElementById(label).value = "";
        document.getElementById('apartarCita').disabled = "";
    }
}

function listaDiagnosticos(sexo, edad, elemento, q)
{

    if (q.length > 3)
        $.ajax({
            url: "diagnostico.php",
            data: "sexo=" + sexo + "&edad=" + edad + "&query=" + q,
            type: "GET"
        }).done(function(resp) {
            $("#" + elemento).html(resp);
        });
}

function ListaMedicamentos()
{
    $("#contenido").load("VerMedicinas.php");
    DesbloquearCita();
}

function FiltrarTiempoMed()
{
    var fechaIni = $("#fechaInicio").val();
    var fechaFin = $("#fechaFin").val();
    if (fechaIni <= fechaFin)
        $.ajax({
            url: "ConsultaMedicamentosReferidos.php",
            data: {
                fecha1: fechaIni,
                fecha2: fechaFin
            },
            type: "GET",
            success: function(resp)
            {
                $("#resultado").html(resp);
            },
            beforeSend: function()
            {
                $("#resultado").html("<img src='diseno/loading.gif'>");
            }

        });
    else
        alert("Ingrese una fecha de Inicio Menor a la fecha Final");
}

function ChecarCierre()
{
    var bandCita = $("#citaRealizada").val();
    var cierre = confirm("Desea cancelar el apartado de la Cita");
    if (bandCita == 0 && cierre)
        cancelarCita();
    else
        return;
}

function eliminarCitas()
{
    /*var cita = $.cookie("cita");
     if(cita==undefined)*/
    var cita = $("#idCita").val();
    if (cita != undefined) {
        conf = confirm("Desea Salir,\n Si procede perdera el espacio apartado");
        if (conf) {
            $.ajax({
                url: "eliminarBloqueoCita.php",
                success: function(data)
                {
                    if (data == 1) {
                        alert("La cita apartada ha sido eliminada");
                        $.cookie("citas", undefined, {expires: -1});
                    }
                    else
                        return;

                }
            });
        }
        else
            return;
    }
}

function verCitasDH() {
    if (!document.getElementById('dh')) {
        alert("Ingrese la cedula del derechohabiente y haga click en Buscar...");
    } else {
        valor = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
        valor = ponerAcentos(valor);
        if (valor == -1) {
            alert('No existe derechohabiente con la cedula introducida');
        } else {
            aValor = valor.split('|');
            mostrarDiv("citas");
            var contenedor2 = document.getElementById('citas');
            var agenda = enlaceJava();
            agenda.open("GET", "citasXbusqueda.php?id_derecho=" + aValor[0], true);
            agenda.onreadystatechange = function()
            {
                if (agenda.readyState == 4) // Readystate 4 significa que ya acab&oacute; de cargarlo
                {
                    contenedor2.innerHTML = agenda.responseText;
                }
                if ((agenda.readyState == 1) || (agenda.readyState == 2) || (agenda.readyState == 3))
                {
                    contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            }
            agenda.send(null);
        }
    }
}

function VerVisor()
{
    $("#contenido").load("inicioVisor.php");
}

function ReporteMedicamentos()
{
    $("#contenido").load("VerReporteMensual.php");
    DesbloquearCita();
}

function DesactivarTratamientos()
{

    $("#contenido").load("BuscarDHTrat.php");
    DesbloquearCita();
}

function BuscarTratamientos()
{
    var dh = $("#dh option:selected").val();
    var aDh = dh.split("|");
    $("#citas").load("ListaDHCronicos.php?idDh=" + aDh[0]);
    mostrarDiv('citas');
}

function ActivarTipoDesc()
{
    var sel = $("#TipoDesc option:selected").val();
    if (sel == 1)
    {
        ocultarDiv('Tratamientos');
    }
    else
        mostrarDiv('Tratamientos');
}

function DesactivarTratamientosConfirmar()
{
    var sele = $("#TipoDesc option:selected").val();
    var motivo = $("#MotCan option:selected").val();
    if (sele == 1)
    {
        var idDer = $("#idDer").val();
        if (motivo == -1)
        {
            alert("Seleccione un motivo");
            return;
        }
        else
        {
            if (motivo == "otro")
            {
                motivo = $("otroMot").val();
                if (motivo == "")
                {
                    alert("escriba el motivo");
                    return;
                }
            }
            $.ajax({
                url: "DesactivarTratamientos.php",
                data: {
                    tipoDes: sele,
                    idDer: idDer,
                    motivo: motivo
                },
                success: function(data)
                {
                    if (data == 1) {
                        alert("Tratamientos del Paciente fueron eliminados");
                        $("#citas").html("");
                    }
                    else
                        alert("Error, Intente Nuevamente");
                }
            });
        }
    }
    else
    {
        var Tratamientos = new Array();
        $("input[name='trat[]']:checked").each(function() {
            Tratamientos.push($(this).val());
        });
        Tratamientos = Tratamientos.toString();
        if (motivo == -1)
        {
            alert("Seleccione un motivo");
            return;
        }
        else
        {
            if (motivo == "otro")
            {
                motivo += "|" + $("otroMot").val();
                if (motivo == "")
                {
                    alert("escriba el motivo");
                    return;
                }
            }
            $.ajax({
                url: "DesactivarTratamientos.php",
                data: {
                    motivo: motivo,
                    tipoDes: sele,
                    tratamientos: Tratamientos,
                },
                success: function(data)
                {
                    if (data == 1) {
                        alert("Tratamientos del Paciente fueron eliminados");
                        $("#citas").html("");
                    }
                    else {
                        if (data == 0)
                            alert("Error, Intente Nuevamente");
                        if (data == -1)
                            alert("Algunos tratamientos no pudieron eliminarse, intente nuevamente");
                        BuscarTratamientos();
                    }
                }
            });
        }

    }
}

function ActivarOtroMotivo()
{
    var sel = $("#MotCan option:selected").val();
    if (sel == "otro")
        mostrarDiv('om');
    else
        ocultarDiv('om');
}

function SeleccionarMedicina(idTexto, campo)
{
    var texto = $("#" + idTexto).val()
    if (texto.length > 3)
    {
        var ajax = enlaceJava();
        ajax.onreadystatechange = function()
        {
            if (ajax.readyState == 4)
            {
                document.getElementById('resultados_' + campo).innerHTML = ajax.responseText;
            }
            else
                document.getElementById('resultados_' + campo).innerHTML = "<img src=\"diseno/loading.gif\">";
        }
        ajax.open("GET", "medicinaContrarreferencia.php?q=" + texto + "&campo=" + campo, true);
        ajax.send();
    }
}

function regresarMedicamento(idMed, desc, campo, presentacion, unidad, grupo)
{
    var textMed = document.createElement("input");
    textMed.type = "text";
    textMed.name = "textMed";
    textMed.id = "textMed";
    textMed.readOnly = true;
    textMed.value = desc;
    document.getElementById('medicamento').innerHTML = "";
    document.getElementById('medicamento').appendChild(textMed);
    var idMedField = document.createElement("input");
    idMedField.type = "hidden";
    idMedField.name = "idMedBusq";
    idMedField.id = "idMedBusq";
    idMedField.value = idMed;
    document.getElementById('medicamento').appendChild(idMedField);
    $("#resultados_" + campo).html("");
}

function BuscarMediPor()
{
    var sel = $("#buscarMedPor option:selected").val();
    if (sel == "clave")
    {
        ocultarDiv('buscarDesc');
        mostrarDiv('busClave');
    }
    else
    {
        ocultarDiv('busClave');
        mostrarDiv('buscarDesc');
    }
}

function BuscarMedXClave()
{
    var idMed = $("#claveMed").val();
    $.ajax({
        url: "BuscarMedXClave.php",
        data:
                {
                    idMed: idMed
                },
        success: function(data)
        {
            var aData = data.split("|");
            if (aData[0] == 1) {
                var textMed = document.createElement("input");
                textMed.type = "text";
                textMed.name = "textMed";
                textMed.id = "textMed";
                textMed.readOnly = true;
                textMed.value = aData[2];
                textMed.size = 50;
                document.getElementById('medicamento').innerHTML = "";
                document.getElementById('medicamento').appendChild(textMed);
                var idMedField = document.createElement("input");
                idMedField.type = "hidden";
                idMedField.name = "idMedBusq";
                idMedField.id = "idMedBusq";
                idMedField.value = aData[1];
                document.getElementById('medicamento').appendChild(idMedField);
            }
            else
                document.getElementById('medicamento').innerHTML = aData[1];
        }
    });

}

function BuscarMedicamentosDesact()
{
    var med = document.getElementById('textMed').value;
    var idMed = document.getElementById('idMedBusq').value
    if (med == null || idMed == null)
    {
        alert("Seleccione el medicamento");
    }
    else
    {
        $.ajax({
            url: "DesactivarTratamientosXMed.php",
            data: {
                idMed: idMed
            },
            success: function(data) {
                $("#citas").html(data);
            }

        });
    }
}

function ActivarTipoDescMed()
{
    var sel = $("#TipoDesc option:selected").val();
    if (sel == 1)
    {
        ocultarDiv('Tratamientos');
        document.getElementById('MotCan').options.length = 0;
        document.getElementById('MotCan').options[0] = new Option("", "-1");
        document.getElementById('MotCan').options[1] = new Option("Sin Tratamientos", "Sin Tratamientos");
        document.getElementById('MotCan').options[2] = new Option("Otro", "Otro");

    }
    else {
        mostrarDiv('Tratamientos');
        document.getElementById('MotCan').options.length = 0;
        document.getElementById('MotCan').options[0] = new Option("", "-1");
        document.getElementById('MotCan').options[1] = new Option("Sin Vigencia", "Sin Vigencia");
        document.getElementById('MotCan').options[2] = new Option("Por Fallecimiento", "Por Fallecimiento");
        document.getElementById('MotCan').options[3] = new Option("Otro", "Otro");
    }

}

function DesactivarTratamientosXMedConfirmar()
{
    var sele = $("#TipoDesc option:selected").val();
    var motivo = $("#MotCan option:selected").val();
    var idDer = $("#idMed").val();
    if (sele == 1)
    {

        if (motivo == -1)
        {
            alert("Seleccione un motivo");
            return;
        }
        else
        {
            if (motivo == "otro")
            {
                motivo = $("otroMot").val();
                if (motivo == "")
                {
                    alert("escriba el motivo");
                    return;
                }
            }
            $.ajax({
                url: "DesactivarMedicamentos.php",
                data: {
                    tipoDes: sele,
                    idMed: idDer,
                    motivo: motivo
                },
                success: function(data)
                {
                    if (data == 1) {
                        alert("Tratamientos con el Medicamento fueron eliminados");
                        $("#citas").html("");
                    }
                    else
                        alert("Error, Intente Nuevamente");
                }
            });
        }
    }
    else
    {
        var Tratamientos = new Array();
        $("input[name='trat[]']:checked").each(function() {
            Tratamientos.push($(this).val());
        });
        Tratamientos = Tratamientos.toString();
        if (motivo == -1)
        {
            alert("Seleccione un motivo");
            return;
        }
        else
        {
            if (motivo == "otro")
            {
                motivo += "|" + document.getElementById("otroMot").value;
                if (motivo == "")
                {
                    alert("escriba el motivo");
                    return;
                }
            }
            $.ajax({
                url: "DesactivarMedicamentos.php",
                data: {
                    idMed: idDer,
                    motivo: motivo,
                    tipoDes: sele,
                    tratamientos: Tratamientos,
                },
                success: function(data)
                {
                    if (data == 1) {
                        alert("Tratamientos con Medicamento fueron eliminados");
                        $("#citas").html("");
                    }
                    else {
                        if (data == 0)
                            alert("Error, Intente Nuevamente");
                        if (data == -1)
                            alert("Algunos tratamientos no pudieron eliminarse, intente nuevamente");
                        BuscarTratamientos();
                    }
                }
            });
        }

    }
}

function DesactivarTratamiento(idTrat)
{
    var conf = confirm("Desea Desactivar el tratamiento seleccionado");
    if (conf) {
        var motivo = prompt("Escriba el motivo de desactivacion", motivo);
        $.ajax({
            url: "DesactivarTratamiento.php",
            data: {
                idTratamiento: idTrat,
                motivo: motivo
            },
            success: function(data)
            {
                if (data == 1)
                {
                    alert("Tratamiento Desactivado");
                }
                else
                    alert("Error, Intente de nueva Cuenta");
            }
        });
    }
}

function DesbloquearCita()
{
    if (document.getElementById('idCita') != undefined) {
        var cita = document.getElementById('idCita').value;
        var contenedor = document.getElementById('citas');
        var hecha = document.getElementById('citaRealizada');
        if (hecha == 0) {
            var objeto = enlaceJava();
            objeto.onreadystatechange = function() {
                if (objeto.readyState == 4)
                {
                    contenedor.innerHTML = objeto.responseText;
                    borrarcookie("cita");
                    apartarCita();
                }
                else
                    contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
            objeto.open("GET", "cancelarCita.php?idCita=" + cita, true);
            objeto.send(null);
        }
    }
}

/*function cargarEstados(elemento) {
 var obj = $("#"+elemento);
 $.ajax({
 url:"cargarEstados.php",
 data:{
 estado:undefined
 },
 success: function(data, textStatus, jqXHR) {
 obj.append(data);
 }
 });
 }*/

function ReprogramarAceptada(idCitaAp, estado)
{
    $.ajax({
        url: "buscarCitasRep.php",
        data: {
            idCitaAp: idCitaAp,
            estado: estado
        },
        type: 'GET',
        success: function(data, textStatus, jqXHR) {
            $("#citas").html(data);
        }
    });
}

function elegirCitaRep(idDerecho, idhorario, fecha, horario, idSolicitud, estado)
{
    var contenedor = document.getElementById('contenido');
    var conf = confirm("Desea elegir la cita el dia de " + fecha + " con horario de " + horario);
    if (conf) {
        $.ajax({
            url: "buscarCitasPerdidas.php",
            data: {
                fecha: fecha,
                horario: idhorario,
                idSol: idSolicitud,
                id_derecho: idDerecho,
                estado: estado
            },
            type: 'GET',
            beforeSend: function(xhr) {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            },
            success: function(data, textStatus, jqXHR) {
                var aResp = data.split("|");
                if (aResp[0] == 1) {

                    window.open("formatoCitaRep.php?idCita=" + aResp[2] + "&idCitaApartada=" + aResp[3]);
                    alert(aResp[1]);
                    $("#contenido").load("VerCitas.php");
                    //mostrarCitas();
                }
                else {
                    if (aResp[1] == 2)
                    {
                        alert(aResp[1]);
                        ReprogramarAceptada(idSolicitud, estado);
                    }
                    if (aResp[0] = -1)
                    {
                        alert(aResp[1]);
                        ReprogramarAceptada(idSolicitud, estado);
                    }
                    else
                    {
                        $("#citas").html(aResp[2]);
                    }
                }
            }
        });
    }
    else
        ReprogramarAceptada(idSolicitud);
}

function ReprogramarCitaRechazada(idCitaAp)
{
    var misArch, misServ;
    misServ = confirm(/* Confirmar(*/"Desea Agendar al mismo servicio \n Aceptar \"Si\" Cancelar \"No\"");
    /*function ansFunction(resp){
     return resp;
     },"CitasCanceladas");*/
    misArch = confirm(/*Confirmar(*/"Desea usar los mismos Archivos \n Aceptar \"Si\" Cancelar \"No\"");/*,"Si","No",
     function ansFunction(resp){
     return resp;
     },"CitasCanceladas");*/
    $.ajax({
        url: "ReprogramarCita.php",
        data: {
            idCita: idCitaAp,
            MSer: misServ,
            MArch: misArch
        },
        type: "GET",
        success: function(data, textStatus, jqXHR) {
            var aResp = data.split("|");
            if (aResp[1] == 1)
            {
                alert(aResp[2]);
            }
            else
            {
                $("#citas").load(aResp[2]);
            }
        }

    });

}

function Confirmar(text, bot1, bot2, resp, caja)
{
    var box = document.getElementById(caja);
    box.getElementsByTagName("p")[0].firstChild.nodeValue = text;
    var button = box.getElementsByTagName("input");
    button[0].value = bot1;
    button[1].value = bot2;
    respFunc = resp;
    box.style.visibility = "visible";
}

function Respuesta(response, caja)
{
    document.getElementById(caja).style.visibility = "hidden";
    ansFunction(response);
}

function ReprogramarCitaDifServ(idDerecho, idCitaAp)
{
    var idServicio = $("#servicio").val();
    var MArch = $("#misArch option:selected").val();
    if (MArch == "S") {
        $.ajax({
            url: "buscarCitasRep.php",
            data: {
                idCitaAp: idCitaAp,
                estado: 0,
                idServ: idServicio
            },
            type: 'GET',
            success: function(data, textStatus, jqXHR) {
                $("#citas").html(data);
            }
        });
    }
    else
    {
        dat = new Date();

        $.ajax({
            url: "buscarCitas.php",
            data: {
                servicio: idServicio,
                id_derecho: idDerecho,
                fecha_inca: dat.toString("yyyymmdd"),
                incapacidad: 0

            },
            beforeSend: function(xhr) {
                document.getElementById('citas').innerHTML = "<img src=\"diseno/loading.gif\">";
            },
            success: function(data, textStatus, jqXHR) {
                $("#citas").html(data);
            }
        });
    }
}

function ObtenerUnidadesMedicasConsulta(estado, umf)
{
    estado = quitarAcentos(estado);
    $.ajax({
        url: "ObtenerUMFConsulta.php",
        data: {
            estado: estado,
            unidad: umf
        },
        success: function(data, textStatus, jqXHR) {
            vaciarSelect('Unidad');
            $("#Unidad").append(data);
        }
    });
    /*var xml=new XMLHttpRequest();
     xml.onreadystatechange=function(){
     if(xml.readyState==4)
     {
     document.getElementById('Unidad').innerHTML=ponerAcentos(xml.responseText);
     }
     if(xml.readyState==2)
     {
     document.getElementById('Unidad').innerHTML="<img src=\"diseno/loading.gif\">";
     }
     };
     xml.open("GET", "ObtenerUMFConsulta.php?estado="+estado+"&unidad="+umf, true);
     xml.send();*/

}

function quitarAcentosUmf(Text)
{
    var cadena = "";
    var codigo = "";
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)
    {
        var Char = Text.charCodeAt(j);
        var cara = Text.charAt(j);
        if (cara == "&") {
            temp = Text.substring(j, j + 8);
            switch (temp) {
                case "&aacute;":
                    cadena += "a";
                    j = j + 7;
                    break;
                case "&Aacute;":
                    cadena += "A";
                    j = j + 7;
                    break;
                case "&eacute;":
                    cadena += "e";
                    j = j + 7;
                    break;
                case "&Eacute;":
                    cadena += "E";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "i";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "I";
                    j = j + 7;
                    break;
                case "&oacute;":
                    cadena += "o";
                    j = j + 7;
                    break;
                case "&Oacute;":
                    cadena += "O";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "u";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "U";
                    j = j + 7;
                    break;
                case "&ntilde;":
                    cadena += "n";
                    j = j + 7;
                    break;
                case "&Ntilde;":
                    cadena += "N";
                    j = j + 7;
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        } else {
            switch (Char)
            {
                case 225:
                    cadena += "a";
                    break;
                case 233:
                    cadena += "e";
                    break;
                case 237:
                    cadena += "i";
                    break;
                case 243:
                    cadena += "o";
                    break;
                case 250:
                    cadena += "u";
                    break;
                case 193:
                    cadena += "A";
                    break;
                case 201:
                    cadena += "E";
                    break;
                case 205:
                    cadena += "I";
                    break;
                case 211:
                    cadena += "O";
                    break;
                case 218:
                    cadena += "U";
                    break;
                case 241:
                    cadena += "n";
                    break;
                case 209:
                    cadena += "N";
                    break;
                default:
                    cadena += Text.charAt(j);
                    break;
            }
        }
        codigo += "_" + Text.charCodeAt(j);
    }
    return cadena;
}

function MostrarDatosDH()
{
    $("#citas").html("");
    mostrarDiv('DatosDH');
    var aDh = $("#dh option:selected").val().split("|");
    if (aDh[0] > 0)
        $.ajax({
            url: "ModificarDerechoHabiente.php",
            data: {
                idDerecho: aDh[0]
            },
            success: function(data, textStatus, jqXHR) {
                $("#DatosDH").html(data);
//                estado = $("#valorEstado").val();
//                cargarEstados('estadoAgregar', estado);
//                $("#estadoAgregar option[value=" + estado + "]").attr("selected", "selected");
//                var umf = $("#valorUmf").val();
//                /*tam=menuEstados.length;
//                 for(ind=0;ind<tam;ind++)
//                 {
//                 if(ponerAcentos(quitarAcentos(menuEstados.options[ind].value))==estado)
//                 {
//                 menuEstados.selectedIndex=ind;
//                 break;
//                 }
//                 }*/
//
//                var municipio = document.getElementById('valorMunicipio').value;
//                //var menuMunicipios=document.getElementById('municipioAgregar');
//                //tam=menuMunicipios.length;
//
//                cargarMunicipios1(estado, 'municipioAgregar', municipio);
//                var coloniaSel = $("#valColonia").val();
//                var cp = $("#valorCodigo").val();
//                cargarColoniasMod('col', 'valorEstado', 'valorMunicipio', coloniaSel);
//                obtenerCodigoPostalMod(estado, municipio, cp);
//               
//                ObtenerUnidadesMedicasConsulta(estado, umf);
                $("#DatosDH").find("script").each(function(i) {
                    eval($(this).text());
                });

            }
        });
}

function validarModificarDH()
{
    var cedula = $("#cedulaAgregar").val();
    var sexo = $("#sexo").val();
    var tpder = $("#cedulaTipoAgregar").val();
    tpoder = parseInt(tpder);
    var app = quitarAcentos($("#ap_pAgregar").val());
    var apm = quitarAcentos($("#ap_mAgregar").val());
    var nombre = quitarAcentos($("#nombreAgregar").val());
    var letras = /^[a-zA-Z(/)]+$/;
    var alfanumerico = /^[a-zA-Z0-9(/)]+$/;
    var direccion = quitarAcentos($("#direccionAgregar").val());
    var fecha = $("#fecha_nacAgregar").val();
    var registro = 1;
    var mensajes = [];
    if (sexo != "-1") {
        switch (tpoder)
        {
            case 10:
            case 20:
            case 90:
            case 91:
                cedulaTrab = validarCedulaTrabajador(cedula, nombre, app, apm, fecha);
                if (!cedulaTrab)
                {
                    registro = 0;
                    mensajes.push('Verifique la fecha de Nacimiento o los datos del Derechohabiente');
                }
                break;
            case -1:
                registro = 0;
                mensajes.push("Tipo de Cedula");
                break;
            default:
                ced = validarCedula(cedula)
                if (ced != "")
                {
                    registro = 0;
                    mensajes.push(ced);
                }

        }
    }
    else
    {
        registro = 0;
        mensajes.push("sexo del DerechoHabiente");
    }
    if (!letras.test(app))
    {
        registro = 0;
        mensajes.push("Apellido paterno");
    }
    if (!letras.test(apm))
    {
        registro = 0;
        mensajes.push("Apellido materno");
    }
    if (nombre.length < 2)
    {
        registro = 0;
//        alert(nombre);
        mensajes.push("Nombre del DerechoHabiente");
    }
    if (direccion.length < 4)
    {
//        alert()
        registro = 0;
        mensajes.push("Direccion");
    }
    var estado = $("#estadoAgregar option:selected").val();
    var municipio = $("#municipioAgregar option:selected").val();
    var cp = $("#codigoPostal option:selected").val();
    var colonia = $("#colonia option:selected").val();
    if (estado == "-1")
    {
        registro = 0;
        mensajes.push("Estado");
    }
    if (municipio == "-1")
    {
        registro = 0;
        mensajes.push("Municipio");
    }
    if (colonia == "-1")
    {
        registro = 0;
        mensajes.push("Colonia");
    }
    if (cp == "-1")
    {
        registro = 0;
        mensajes.push("Codigo Postal");
    }
    var telefono = /[0-9]{10}/;
    if (!telefono.test($("#telefonoAgregar").val()))
    {
        registro = 0;
        mensajes.push("Telefono");
    }
    if ($("#cel").val() != "")
    {
        if (!telefono.test($("#cel").val()))
        {
            registro = 0;
            mensajes.push("Telefono");
        }
    }
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if ($("#email").val() != "")
        if (!re.test($("#email").val()))
        {
            registro = 0;
            mensajes.push("Correo Electrónico valido");
        }
//            return [registro,mensajes];
    if (registro == 0)
    {
        var mensajes1 = "Verifique lo siguiente";
        $.each(mensajes, function(index, value) {
            mensajes1 += "\n" + value;
        });
        alert(mensajes1);
    }
    else
    {
        $.ajax({
            url: "ModificarDH.php",
            data: $("#agregarDH").serialize(),
            dataType: "json",
            success: function(data)
            {
                var aResp = data;
                if (aResp[0] == 1) {
                    mostrarDiv('cambiar');
                    $('#btnGuardar').attr('disabled', 'disabled');
                    ocultarDiv('guardar');

                    DesHabilitarModDH();
                }
                alert(aResp[1]);
                console.log('Formulario enviado');
                return;
                //        ocultarDiv('DatosDH');
            }

        });
    }
}

function HabilitarModDH()
{
    $("#cedulaAgregar").removeAttr("readonly");
    $("#sexo").removeAttr("disabled");
    $("#cedulaTipoAgregar").removeAttr("disabled");
    $("#ap_pAgregar").removeAttr("readonly");
    $("#ap_mAgregar").removeAttr("readonly");
    $("#nombreAgregar").removeAttr("readonly");
    $("#telefonoAgregar").removeAttr("readonly");
    $("#mes1").removeAttr("disabled");
    $("#dia1").removeAttr("disabled");
    $("#anio1").removeAttr("disabled");
    $("#direccionAgregar").removeAttr("readonly");
    $("#estadoAgregar").removeAttr("disabled");
    $("#municipioAgregar").removeAttr("disabled");
    $("#codigoPostal").removeAttr("disabled");
    $("#col").removeAttr("disabled");
    $("#Unidad").removeAttr("disabled");
    $("#emailAgregar").removeAttr("readonly");
    $("#cel").removeAttr("readonly");
}

function obtenerCodigoPostalMun(cp, edo, mun)
{
    vaciarSelect(cp);
    var contenedor = document.getElementById(cp);
    var municipio = document.getElementById(mun).value;
    var estado = document.getElementById(edo).value;
    var seleccionado = contenedor.value;
    var ajax = enlaceJava();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
            contenedor.value = seleccionado;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", "codigos_postales.php?estado=" + quitarAcentos(estado + "&municipio=" + municipio + "&cp="), true);
    ajax.send(null);
    //document.getElementById('cp').disabled = false;
}

function obtenerCodigoPostalMod(edo, mun, col)
{

    var contenedor = document.getElementById('codigoPostal');
    var municipio = mun;
    var estado = edo;
    var colonia = col;
    var seleccionado = contenedor.value;
    vaciarSelect('codigoPostal');
    var ajax = enlaceJava();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
            contenedor.value = seleccionado;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", "codigos_postales.php?estado=" + quitarAcentos(estado + "&municipio=" + municipio + "&cp=" + colonia), true);
    ajax.send(null);
    //document.getElementById('codigoPostal').disabled = false;
}

function cargarColonias1(colonia, cp, mun)
{
    vaciarSelect(colonia);
    var codigo = document.getElementById(cp).value;
    var municipio = document.getElementById(mun).value;
    var contenedor = document.getElementById(colonia);
    var seleccionado = document.getElementById(colonia).value;
    vaciarSelect(colonia);
    var ajax = enlaceJava();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
            contenedor.value = seleccionado;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", "mostrarColonias.php?cp=" + codigo + "&municipio=" + municipio);
    ajax.send(null);
}

function cargarColoniasMod(colonia, cp, mun, sel)
{
    vaciarSelect(colonia);
    var codigo = document.getElementById(cp).value;
    var municipio = document.getElementById(mun).value;
    var contenedor = document.getElementById(colonia);
    var seleccionado = sel;
    vaciarSelect(colonia);
    var ajax = enlaceJava();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
            contenedor.value = seleccionado;
        }
        else
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET", "mostrarColonias.php?estado=" + codigo + "&municipio=" + municipio + "&colonia=" + sel);
    ajax.send(null);
}

function ObtenerUnidadesMedicas(estado)
{
    estado = quitarAcentos(estado);
    var xml = enlaceJava()
    xml.onreadystatechange = function() {
        if (xml.readyState == 4)
        {
            document.getElementById('Unidad').innerHTML = ponerAcentos(xml.responseText);
        }
        if (xml.readyState == 2)
        {
            document.getElementById('Unidad').innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    };
    xml.open("GET", "ObtenerUMFConsulta.php?estado=" + estado + "&unidad=-1", true);
    xml.send();

}

function CambiarBoton()
{
    var boton = $("#guardar").val();
    if (boton == "Modificar") {
        HabilitarModDH();
        $("#guardar").val("Guardar");
        $("#guardar").removeAttr("type");
        $("#guardar").attr("type", "submit");
        $("#guardar").unbind("click");
    }
//    else {
//        $("#guardar").removeAttr("type");
//        $("#guardar").attr("type","button");
//        DesHabilitarModDH();
//        ocultarDiv('DatosDH');
//        //MostrarDatosDH();
//    }
}

function DesHabilitarModDH()
{
    $("#cedulaAgregar").attr("readonly", true);
    $("#sexo").attr("disabled", true);
    $("#cedulaTipoAgregar").attr("disabled", true);
    $("#ap_pAgregar").attr("readonly", true);
    $("#ap_mAgregar").attr("readonly", true);
    $("#nombreAgregar").attr("readonly", true);
    $("#telefonoAgregar").attr("readonly", true);
    $("#fecha_nacAgregar").attr("readonly", true);
    $("#direccionAgregar").attr("readonly", true);
    $("#estadoAgregar").attr("disabled", true);
    $("#municipioAgregar").attr("disabled", true);
    $("#codigoPostal").attr("disabled", true);
    $("#col").attr("disabled", true);
    $("#Unidad").attr("disabled", true);
    $("#emailAgregar").attr("readonly", true);
    $("#cel").attr("readonly", true);
    $("#guardar").val("Modificar");
}