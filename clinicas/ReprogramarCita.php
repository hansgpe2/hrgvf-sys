<?php

session_start();
include './lib/funciones.php';
$idCitaAp = $_REQUEST['idCita'];
$datosCita = DatosCitaApartada($idCitaAp);
$misServ = $_REQUEST['MSer'];
$misArch = $_REQUEST['MArch'];
$fecha = $datosCita['fecha_cita'];
if ($misServ == "true") {
    $queDia = diaSemana(date("N", strtotime($fecha)));
    $sql = "select * from horarios where id_servicio=" . $datosCita['id_servicio'] . " and id_medico=" . $datosCita['id_medico'] . " and hora_inicio='" . $datosCita['hora_cita'] . "' and dia='" . $queDia . "'";
    $horario = consultaSQL($sql);
    $sql = "select * from citas where fecha_cita='" . date("Ymd", strtotime($datosCita['fecha_cita'])) . "' and id_horario=" . $horario['id_horario'];
    $duplica = existeDuplica($sql);
    if (!$duplica && $fecha>date("Ymd")) {
        $sql = "insert into citas values(NULL," . $horario['id_horario'] . "," . $datosCita['id_derecho'] . ",'" . date("Ymd", strtotime($datosCita['fecha_cita'])) . "',1,''," . $_SESSION['idUsuario'] . ",'" . date("d/m/Y -H:i") . "',3,'".$datosCita['diagnostico']."');";
        $citaInsert = insertarSQL($sql);
        if ($citaInsert[0] == 0) {
            $citaR = getCitaRecienAgregada($horario['id_horario'], $datosCita['id_derecho'], $datosCita['fecha_cita']);
            if ($misArch) {

                $sql = "insert into citas_apartadas values(NULL," . $citaR['id_cita'] . "," . $_SESSION['idClinica'] . ",-1,'" . $datosCita['diagnostico'] . "','Reprogramacion X Rechazo','','" . date("Ymd H:i") . "',''," . $datosCita['id_derecho'] . "," . $_SESSION['idUsuario'] . "," . $datosCita['carpeta'] . "," . $datosCita['id_servicio'] . "," . $datosCita['id_medico'] . ",'" . $citaR['fecha_cita'] . "','" . $datosCita['hora_cita'] . ");";
                $citaAIns = insertarSQL($sql);
                if ($citaAIns[0] == 0) {
                    echo "1|1|Cita agregada";
                } else
                    echo "0|1|" . $citaAIns[1];
            }
            else {
                echo "1|2|DocumentosReferencia.php?idDerecho=" . $datosCita['id_derecho'] . "&idCita=" . $citaR['id_cita'] . "&fechaCita=" . $citaR['fecha_cita'] . "&idHorario=" . $citaR['id_horario'];
            }
        } else
            echo "0|1|" . $citaInsert[1];
    }
    else {
        if ($misArch=="true") {
            echo "1|2|buscarCitasRep.php?idCitaAp=" . $datosCita['id_cita_ap'] . "&estado=0";
        } else {
            echo "1|2|buscarCitas.php?servicio=" . $datosCita['id_servicio'] . "&id_derecho=" . $datosCita['id_derecho'] . "&fecha_inca=" . date("Ymd") . "&incapacidad=0";
        }
    }
} else {
    echo '1|2|ReprogramarDifServ.php?id_derecho=' . $datosCita['id_derecho'] . '&id_citaA=' . $datosCita['id_cita_ap'] . "&MisArch=" . $MArch;
}
?>
