ALTER TABLE `sistema_agenducha`.`citas_apartadas` ADD COLUMN `id_servicio` INT NULL AFTER `carpeta`, ADD COLUMN `id_medico` INT NULL AFTER `id_servicio`, ADD COLUMN `hora_cita` VARCHAR(10) NULL AFTER `id_medico`; 
UPDATE citas_apartadas AS ca SET id_servicio=(SELECT h.id_servicio FROM horarios AS h WHERE h.id_horario=ca.`id_horario`);
UPDATE citas_apartadas AS ca SET id_medico=(SELECT h.id_medico FROM horarios AS h WHERE h.id_horario=ca.`id_horario`);
UPDATE citas_apartadas AS ca SET hora_cita=(SELECT h.hora_inicio FROM horarios AS h WHERE h.id_horario=ca.`id_horario`);
ALTER TABLE `sistema_agenducha`.`citas_apartadas` ADD COLUMN `fecha_cita` DATE NULL AFTER `hora_cita`; 
UPDATE citas_apartadas AS ca SET fecha_cita=(SELECT c.fecha_cita FROM citas AS c WHERE c.`id_cita`=ca.id_cita) ; 
UPDATE citas_apartadas SET carpeta=id_cita;
ALTER TABLE citas_apartadas DROP COLUMN id_horario;