function validarAgregarCita2014() {
    var id_derecho = document.getElementById('id_derecho').value;
    var id_cita = document.getElementById('id_cita').value;
    var tipo_reprogramacion = document.getElementById('tipo_reprogramacion').value;
    var cedula = document.getElementById('cedula').value;
    var ap_p = document.getElementById('ap_p').value;
    var ap_m = document.getElementById('ap_m').value;
    var nombre = document.getElementById('nombre').value;
    var telefono = document.getElementById('telefono').value;
    var direccion =	document.getElementById('direccion').value;
    var estado = document.getElementById('estado').value;
    var municipio =	document.getElementById('municipio').value;
	var estudi_1 = '';
	var estudi_2 = '';
    var observaciones =	document.getElementById('observaciones').value;
    if (document.getElementById('diagnostico') != undefined) {
        var diagnostico = document.getElementById('diagnostico').value;
    } else {
        var diagnostico = "";
    }
    if (document.getElementById('concertada') != undefined) {
        if (document.getElementById('concertada').checked == true)
            var concertada = "si";
        else 
            var concertada = "no";
    } else {
        var concertada = "";
    }
    if ((id_derecho.length > 0) && (cedula.length > 0) && (ap_p.length > 0) && (ap_m.length > 0) && (nombre.length > 0)) {
		var todoBien = false;
		if(document.getElementById('combobox') == undefined) {
			todoBien = true;
		} else {
			estudi_1 = document.getElementById('combobox').value;
			if (estudi_1 == '') {
				alert('Debes seleccionar el estudio a realizar');
			} else {
				todoBien = true;
			}
		}
		todoBien = false;
		if(document.getElementById('combobox2') == undefined) {
			todoBien = true;
		} else {
			estudi_2 = document.getElementById('combobox2').value;
			if (estudi_2 == '') {
				alert('Debes seleccionar el servicio que solicita el estudio');
			} else {
				todoBien = true;
			}
		}
		if (todoBien == true) {
			var contenedor;
			contenedor = document.getElementById('contenido');
			var contenedor2;
			contenedor2 = document.getElementById('enviando');
			var objeto= new AjaxGET();
			objeto.open("GET", "agregarCitaConfirmar.php?id_derecho="+id_derecho+"&idHorario="+document.getElementById('id_horario').value+"&fechaCita="+document.getElementById('fechaCita').value+"&obs="+observaciones+"&id_cita="+id_cita+"&tipo_reprogramacion="+tipo_reprogramacion+"&diagnostico="+diagnostico+"&concertada="+concertada+"&estudio="+estudi_1+"&servEstudio="+estudi_2,true);
			objeto.onreadystatechange=function()
			{
				if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
				{
					contenedor2.innerHTML = "&nbsp;";
					hayCitasArep();
					listaCitasArep();
					hayCitasRezagadas();
					listaCitasRezagadas();
					document.getElementById('agregar').disabled = '';
					document.getElementById('regresar').disabled = '';
					document.getElementById('modificar').disabled = '';
					document.getElementById('seleccionar').disabled = '';
					document.getElementById('botonReprogramar').disabled = '';
					citaAgregada(objeto.responseText);
				}
				if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
				{
					document.getElementById('agregar').disabled = 'disabled';
					document.getElementById('regresar').disabled = 'disabled';
					document.getElementById('modificar').disabled = 'disabled';
					document.getElementById('seleccionar').disabled = 'disabled';
					document.getElementById('botonReprogramar').disabled = 'disabled';
					contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
				}
			}
			objeto.send(null);
		}
    } else {
        alert('Cedula y nombre completo es requerido');
    }
}


function validarAgregarCitaExtemporanea2014() {
    var id_derecho = document.getElementById('id_derecho').value;
    var id_cita = document.getElementById('id_cita').value;
    var hora_inicio = document.getElementById('hora_inicio').value;
    var hora_fin = document.getElementById('hora_fin').value;
    var cedula = document.getElementById('cedula').value;
    var ap_p = document.getElementById('ap_p').value;
    var ap_m = document.getElementById('ap_m').value;
    var nombre = document.getElementById('nombre').value;
    var telefono = document.getElementById('telefono').value;
    var direccion =	document.getElementById('direccion').value;
    var estado = document.getElementById('estado').value;
    var municipio =	document.getElementById('municipio').value;
    var tipo_cita =	document.getElementById('tipo_cita').value;
    var diagnostico =	document.getElementById('diagnostico').value;
    var observaciones =	document.getElementById('observaciones').value;
    var validarHoraInicio = esHoraValida(hora_inicio,'Hora Inicio');
	var estudi_1 = '';
	var estudi_2 = '';
    var concertada = "";
    if (tipo_cita == "0") {
        if (document.getElementById('concertada').checked == true)
            var concertada = "si";
        else 
            var concertada = "no";
    }
    if (tipo_cita == "-1") {
        alert('Tipo de Cita es requerido');
    } else {
        if(validarHoraInicio == "") {
            var validarHoraFin = esHoraValida(hora_fin,'Hora Fin');
            if(validarHoraFin == "") {
                if ((id_derecho.length > 0) && (cedula.length > 0) && (ap_p.length > 0) && (ap_m.length > 0) && (nombre.length > 0)) {
						var todoBien = false;
						if(document.getElementById('combobox') == undefined) {
							todoBien = true;
						} else {
							estudi_1 = document.getElementById('combobox').value;
							if (estudi_1 == '') {
								alert('Debes seleccionar el estudio a realizar');
							} else {
								todoBien = true;
							}
						}
						todoBien = false;
						if(document.getElementById('combobox2') == undefined) {
							todoBien = true;
						} else {
							estudi_2 = document.getElementById('combobox2').value;
							if (estudi_2 == '') {
								alert('Debes seleccionar el servicio que solicita el estudio');
							} else {
								todoBien = true;
							}
						}
						if (todoBien == true) {
							var contenedor;
							contenedor = document.getElementById('contenido');
							var objeto= new AjaxGET();
							var fechaCita=document.getElementById('fechaCita').value;
							objeto.open("GET", "NuevacitaExtemporaneaConfirmar.php?id_derecho="+id_derecho+"&hora_inicio="+hora_inicio+"&hora_fin="+hora_fin+"&fechaCita="+fechaCita+"&obs="+observaciones+"&id_cita="+id_cita+"&tipo_cita="+tipo_cita+"&diagnostico="+diagnostico+"&concertada="+concertada+"&estudio="+estudi_1+"&servEstudio="+estudi_2,true);
							objeto.onreadystatechange=function()
							{
								if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
								{
									
									citaExtemporaneaAgregada(objeto.responseText,fechaCita);
								}
								if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
								{
									contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
								}
							}
							objeto.send(null)
						}
                } else {
                    alert('Cedula y nombre completo es requerido');
                }
            } else {
                alert(validarHoraFin);
            }
        } else {
            alert(validarHoraInicio);
        }
    }
}

function reporteLaboratoriosDo(idUsuario, idConsultorio, idServicio, idMedico){
    var fechaI = document.getElementById('date1');
    var fechaF = document.getElementById('date2');
    f1=fechaI.value.length;
    f2=fechaF.value.length;
    if (f1==0 || f2==0){
        alert("Ambas fechas deben estar establecidas!");
        return false;
    }
	
    var fechaIn = fechaI.value;
    var fechaFi = fechaF.value;
    var diaIn = fechaIn.substring(0,2); 
    var mesIn = (fechaIn.substring(3,5))*1 - 1;
    var anoIn = fechaIn.substring(6,10); 
    var fechaInU = new Date(anoIn,mesIn,diaIn);
    var diaFi = fechaFi.substring(0,2); 
    var mesFi = (fechaFi.substring(3,5))*1 - 1;
    var anoFi = fechaFi.substring(6,10); 
    var fechaFiU = new Date(anoFi,mesFi,diaFi);
    if(fechaInU > fechaFiU) {
        alert("La fecha final no puede ser menor a la fecha inicial");
    } else {
		var tipoReporte = '';
        var rg1 = document.getElementById('RadioGroup1_0');
/*
        var rg2 = document.getElementById('RadioGroup1_1');
        var rg3 = document.getElementById('RadioGroup1_2');
        var rg4 = document.getElementById('RadioGroup1_3');
*/ 
        if (rg1.checked) tipoReporte = "unidad";
 /*
        if (rg2.checked) tipoReporte = "consultorio";
        if (rg3.checked) tipoReporte = "servicio";
        if (rg4.checked) tipoReporte = "medico";
*/
        window.open("REPORTE_LABORATORIO.php?idConsultorio="+idConsultorio+"&idServicio="+idServicio+"&idMedico="+idMedico+"&idUsuario="+idUsuario+"&tipoReporte="+tipoReporte+"&fechaI="+fechaIn+"&fechaF="+fechaFi,'_blank');

    /*
		var contenedor2 = document.getElementById('contenido');
		var agenda= new AjaxGET();
		agenda.open("GET", "SOLICITUD_EXPEDIENTES.php?idConsultorio="+idConsultorio+"&idServicio="+idServicio+"&idMedico="+idMedico+"&idUsuario="+idUsuario+"&tipoReporte="+tipoReporte+"&fechaI="+fechaIn+"&fechaF="+fechaFi,true);
		agenda.onreadystatechange=function()
		{
			if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor2.innerHTML = agenda.responseText;
			}
			if (agenda.readyState==2)
			{
				contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
			}
		}
		agenda.send(null)
*/	}
    return false;
}


