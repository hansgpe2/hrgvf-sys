<?php require_once('Connections/archjaja.php'); ?>
<?php

//@session_start();

$statusCitas = array("DISPONIBLE", "CITA PENDIENTE", "CITA CUBIERTA", "DE VACACIONES", "EN CONGRESO", "DIA INHABIL", "LICENCIA MEDICA", "BAJA TEMPORAL", "OTRO", "ELIMINADA");
$tipoCita = array("PRIMERA VEZ", "SUBSECUENTE", "PROCEDIMIENTO", "ADMINISTRATIVO");

function diferenciaDeDiasEntreFechas($ano1, $mes1, $dia1, $ano2, $mes2, $dia2) {
    //defino fecha 1
    /* 	$ano1 = 2006;
      $mes1 = 10;
      $dia1 = 2;

      //defino fecha 2
      $ano2 = 2006;
      $mes2 = 10;
      $dia2 = 27;
     */
    //calculo timestam de las dos fechas
    $timestamp1 = mktime(0, 0, 0, $mes1, $dia1, $ano1);
    $timestamp2 = mktime(4, 12, 0, $mes2, $dia2, $ano2);

    //resto a una fecha la otra
    $segundos_diferencia = $timestamp1 - $timestamp2;
    //echo $segundos_diferencia;
    //convierto segundos en días
    $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

    //obtengo el valor absoulto de los días (quito el posible signo negativo)
    $dias_diferencia = abs($dias_diferencia);

    //quito los decimales a los días de diferencia
    $dias_diferencia = floor($dias_diferencia);

    return $dias_diferencia;
}

function tituloDia($dia_num) {
    switch ($dia_num) {
        case 0: return "<abbr title=\"Domingo\">Domingo</abbr>";
            break;
        case 1: return "<abbr title=\"Lunes\">Lunes</abbr>";
            break;
        case 2: return "<abbr title=\"Martes\">Martes</abbr>";
            break;
        case 3: return "<abbr title=\"Miercoles\">Mi&eacute;rcoles</abbr>";
            break;
        case 4: return "<abbr title=\"Jueves\">Jueves</abbr>";
            break;
        case 5: return "<abbr title=\"Viernes\">Viernes</abbr>";
            break;
        case 6: return "<abbr title=\"Sabado\">S&aacute;bado</abbr>";
            break;
    }
}

function tituloMes($mes) {
    $meses = array(12);
    if ($mes[0] == "0")
        $mes = $mes[1];
    $meses[1] = "Enero";
    $meses[2] = "Febrero";
    $meses[3] = "Marzo";
    $meses[4] = "Abril";
    $meses[5] = "Mayo";
    $meses[6] = "Junio";
    $meses[7] = "Julio";
    $meses[8] = "Agosto";
    $meses[9] = "Septiembre";
    $meses[10] = "Octubre";
    $meses[11] = "Noviembre";
    $meses[12] = "Diciembre";
    return $meses[$mes];
}

function diaSemana($dia_num) {
    switch ($dia_num) {
        case 0: return "DOMINGO";
            break;
        case 1: return "LUNES";
            break;
        case 2: return "MARTES";
            break;
        case 3: return "MIERCOLES";
            break;
        case 4: return "JUEVES";
            break;
        case 5: return "VIERNES";
            break;
        case 6: return "SABADO";
            break;
        case 7: return "DOMINGO";
            break;
    }
}

function idTipoCita($tipoCita) {
    if ($tipoCita == "PRV")
        return 0;
    if ($tipoCita == "SUB")
        return 1;
    if ($tipoCita == "PRO")
        return 2;
}

function regresarIdMedico($idConsultorio, $idServicio) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT id_medico FROM servicios_x_consultorio WHERE id_consultorio='" . $idConsultorio . "' and id_servicio='" . $idServicio . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $idMedico = $row_query['id_medico'];
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $idMedico;
}

function recuperarCantidadCitas($tipoCitas, $fechaCitas, $idUsuario, $idConsultorio, $idServicio, $idMedico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
//	$idMedico =  regresarIdMedico($idConsultorio,$idServicio);
    $idCita = idTipoCita($tipoCitas);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $temp = mktime(0, 0, 0, substr($fechaCitas, 4, 2), substr($fechaCitas, 6, 2), substr($fechaCitas, 0, 4));
    $queDia = date("N", $temp);
    $query_query = "SELECT * FROM horarios WHERE id_medico='" . $idMedico . "' and id_servicio='" . $idServicio . "'and id_consultorio='" . $idConsultorio . "' and tipo_cita='" . $idCita . "' and dia='" . diaSemana($queDia) . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);

    $cuantas = 0;
    if ($totalRows_query2 > 0) {
        do {
            $query_query2 = "SELECT * FROM citas WHERE id_horario='" . $row_query['id_horario'] . "' and fecha_cita='" . $fechaCitas . "'";
            $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
            $cuantas+= mysql_num_rows($query2);
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_free_result($query2);
    @mysql_close($dbissste);
    if ($totalRows_query2 == 0) {
        $clase = "conteoCitasGris";
    } else {
        $porcentaje = round(($cuantas / $totalRows_query2) * 100);
        if (($porcentaje >= 0) && ($porcentaje <= 50))
            $clase = "conteoCitasVerde";
        if (($porcentaje >= 51) && ($porcentaje <= 98))
            $clase = "conteoCitasAmbar";
        if (($porcentaje >= 99) && ($porcentaje <= 100))
            $clase = "conteoCitasRojo";
    }
    return '<span class="' . $clase . '">' . $cuantas . '/' . $totalRows_query2 . ' ' . $tipoCitas . '</span><br>';
}

function recuperarCantidadCitasEXT($fechaCitas, $idUsuario, $idConsultorio, $idServicio, $idMedico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
//	$idMedico =  regresarIdMedico($idConsultorio,$idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query2 = "SELECT * FROM citas_extemporaneas WHERE id_consultorio='" . $idConsultorio . "' AND id_servicio='" . $idServicio . "' AND id_medico='" . $idMedico . "' AND fecha_cita='" . $fechaCitas . "'";
    $query2 = mysql_query($query_query2, $bdissste) or die(mysql_error());
    $cuantas = mysql_num_rows($query2);
    return '<span class="conteoCitasAmbar">' . $cuantas . ' EXT</span><br>';
}

function getNcitasRezagadas() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_por_reprogramar_rezago WHERE status='0' ORDER BY id_cita";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $cuantas = 0;
    $hoy = date('Ymd', strtotime("now"));
    if ($totalRows_query > 0) {
        do {
            $esFutura = compararFechas($hoy, $row_query['fecha_cita']);
            if ($esFutura)
                $cuantas++;
        }while ($row_query = mysql_fetch_assoc($query));
    }

    return $cuantas;
}

function getcitasRezagadas() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_por_reprogramar_rezago WHERE status='0' OR status='9' ORDER BY id_cita";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
//	$cuantas = 0;
    $hoy = date('Ymd', strtotime("now"));
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $esFutura = compararFechas($hoy, $row_query['fecha_cita']);
            if ($esFutura) {
//				$cuantas++;
                $ret[] = array(
                    'id_cita' => $row_query['id_cita'],
                    'id_horario' => $row_query['id_horario'],
                    'id_derecho' => $row_query['id_derecho'],
                    'fecha_cita' => $row_query['fecha_cita'],
                    'status' => $row_query['status'],
                    'observaciones' => $row_query['observaciones'],
                    'id_usuario' => $row_query['id_usuario'],
                    'extra1' => $row_query['extra1']
                );
            }
        } while ($row_query = mysql_fetch_assoc($query));
    }

    return $ret;
}

function getcitasReprogramadasRezago() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_reprogramadas_rezago ORDER BY id_cita";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $hoy = date('Ymd', strtotime("now"));
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $esFutura = compararFechas($hoy, $row_query['fecha_cita']);
            if ($esFutura) {
                $ret[] = array(
                    'id_cita' => $row_query['id_cita'],
                    'id_horario' => $row_query['id_horario'],
                    'id_derecho' => $row_query['id_derecho'],
                    'fecha_cita' => $row_query['fecha_cita'],
                    'id_cita_anterior' => $row_query['id_cita_anterior'],
                    'id_horario_anterior' => $row_query['id_horario_anterior'],
                    'fecha_cita_anterior' => $row_query['fecha_cita_anterior'],
                    'status_anterior' => $row_query['status_anterior'],
                    'status' => $row_query['status'],
                    'observaciones' => $row_query['observaciones'],
                    'id_usuario' => $row_query['id_usuario'],
                    'extra1' => $row_query['extra1']
                );
            }
        } while ($row_query = mysql_fetch_assoc($query));
    }

    return $ret;
}

function getcitasRezagadasSin9() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_por_reprogramar_rezago WHERE status ='0' ORDER BY id_cita";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $hoy = date('Ymd', strtotime("now"));
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $esFutura = compararFechas($hoy, $row_query['fecha_cita']);
            if ($esFutura) {
                $ret[] = array(
                    'id_cita' => $row_query['id_cita'],
                    'id_horario' => $row_query['id_horario'],
                    'id_derecho' => $row_query['id_derecho'],
                    'fecha_cita' => $row_query['fecha_cita'],
                    'status' => $row_query['status'],
                    'observaciones' => $row_query['observaciones'],
                    'id_usuario' => $row_query['id_usuario'],
                    'extra1' => $row_query['extra1']
                );
            }
        } while ($row_query = mysql_fetch_assoc($query));
    }

    return $ret;
}

function getNcitasAreprogramar() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_por_reprogramar WHERE status='0' ORDER BY id_cita";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $cuantas = 0;
    $hoy = date('Ymd', strtotime("now"));
    if ($totalRows_query > 0) {
        do {
            $esFutura = compararFechas($hoy, $row_query['fecha_cita']);
            if ($esFutura)
                $cuantas++;
        }while ($row_query = mysql_fetch_assoc($query));
    }

    return $cuantas;
}

function getcitasAreprogramar() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_por_reprogramar WHERE status='0' OR status='9' ORDER BY id_horario ASC, id_cita ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
//	$cuantas = 0;
    $hoy = date('Ymd', strtotime("now"));
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $esFutura = compararFechas($hoy, $row_query['fecha_cita']);
            if ($esFutura) {
//				$cuantas++;
                $ret[] = array(
                    'id_cita' => $row_query['id_cita'],
                    'id_horario' => $row_query['id_horario'],
                    'id_derecho' => $row_query['id_derecho'],
                    'fecha_cita' => $row_query['fecha_cita'],
                    'status' => $row_query['status'],
                    'observaciones' => $row_query['observaciones'],
                    'id_usuario' => $row_query['id_usuario'],
                    'extra1' => $row_query['extra1']
                );
            }
        } while ($row_query = mysql_fetch_assoc($query));
    }

    return $ret;
}

function getcitasAreprogramarXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_por_reprogramar WHERE id_cita='" . $id . "' ORDER BY id_cita";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_horario' => $row_query['id_horario'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario'],
            'extra1' => $row_query['extra1']
        );
    }

    return $ret;
}

function getcitasAreprogramarSin9() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT c.id_cita, c.id_horario, c.id_derecho, c.fecha_cita, c.status, c.observaciones, c.id_usuario, c.extra1, d.id_derecho, d.ap_p, d.ap_m, d.nombres FROM citas_por_reprogramar c, derechohabientes d WHERE c.status ='0' AND d.id_derecho=c.id_derecho ORDER BY d.ap_p ASC, d.ap_m ASC, d.nombres, c.id_horario";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $hoy = date('Ymd', strtotime("now"));
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $esFutura = compararFechas($hoy, $row_query['fecha_cita']);
            if ($esFutura) {
                $ret[] = array(
                    'id_cita' => $row_query['id_cita'],
                    'id_horario' => $row_query['id_horario'],
                    'id_derecho' => $row_query['id_derecho'],
                    'fecha_cita' => $row_query['fecha_cita'],
                    'status' => $row_query['status'],
                    'observaciones' => $row_query['observaciones'],
                    'id_usuario' => $row_query['id_usuario'],
                    'extra1' => $row_query['extra1']
                );
            }
        } while ($row_query = mysql_fetch_assoc($query));
    }

    return $ret;
}

function getcitasReprogramadas() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_reprogramadas ORDER BY id_cita";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $hoy = date('Ymd', strtotime("now"));
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $esFutura = compararFechas($hoy, $row_query['fecha_cita']);
            if ($esFutura) {
                $ret[] = array(
                    'id_cita' => $row_query['id_cita'],
                    'id_horario' => $row_query['id_horario'],
                    'id_derecho' => $row_query['id_derecho'],
                    'fecha_cita' => $row_query['fecha_cita'],
                    'id_cita_anterior' => $row_query['id_cita_anterior'],
                    'id_horario_anterior' => $row_query['id_horario_anterior'],
                    'fecha_cita_anterior' => $row_query['fecha_cita_anterior'],
                    'status_anterior' => $row_query['status_anterior'],
                    'status' => $row_query['status'],
                    'observaciones' => $row_query['observaciones'],
                    'id_usuario' => $row_query['id_usuario'],
                    'extra1' => $row_query['extra1']
                );
            }
        } while ($row_query = mysql_fetch_assoc($query));
    }

    return $ret;
}

function citasXdia($fechaCitas, $idUsuario, $idConsultorio, $idServicio, $idMedico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
//	$idMedico =  regresarIdMedico($idConsultorio,$idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $queDia = date("N", mktime(0, 0, 0, substr($fechaCitas, 4, 2), substr($fechaCitas, 6, 2), substr($fechaCitas, 0, 4)));
    $query_query = "SELECT * FROM horarios WHERE id_medico='" . $idMedico . "' and id_servicio='" . $idServicio . "'and id_consultorio='" . $idConsultorio . "' and dia='" . diaSemana($queDia) . "' ORDER BY hora_inicio";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = /*array(
                'id_horario' => $row_query['id_horario'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
				'tipo_usuario'=>$row_query['tipo_usuario']
            )*/$row_query;
        } while ($row_query = mysql_fetch_array($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasOcupadasXdia($idHorario, $fechaCita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $idHorario . "' and fecha_cita='" . $fechaCita . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_derecho' => $row_query['id_derecho'],
            'id_cita' => $row_query['id_cita'],
            'status' => $row_query['status'],
            'extra1' => $row_query['extra1'],
            'id_usuario' => $row_query['id_usuario'],
            'observaciones' => $row_query['observaciones'],
			'tipo_usuario'=>$row_query['tipo_usuario']
        );
    } else {
        $ret = array(
            'id_derecho' => "-1",
            'id_cita' => "-1",
            'status' => "-1",
            'extra1' => "-1",
            'id_usuario' => "-1",
            'observaciones' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function formatoHora($hora) {
    return substr($hora, 0, 2) . ":" . substr($hora, 2, 2);
}

function quitarPuntosHora($hora) {
    return substr($hora, 0, 2) . substr($hora, 3, 2);
}

function getDatosDerecho($id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM derechohabientes WHERE id_derecho='" . $id_derecho . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'cedula' => $row_query['cedula'],
            'cedula_tipo' => $row_query['cedula_tipo'],
            'ap_p' => $row_query['ap_p'],
            'ap_m' => $row_query['ap_m'],
            'nombres' => $row_query['nombres'],
            'fecha_nacimiento' => $row_query['fecha_nacimiento'],
            'telefono' => $row_query['telefono'],
            'direccion' => $row_query['direccion'],
            'estado' => $row_query['estado'],
            'municipio' => $row_query['municipio'],
            'status' => $row_query['status']
        );
    } else {
        $ret = array(
            'cedula' => "-1",
            'cedula_tipo' => "-1",
            'ap_p' => "-1",
            'ap_m' => "-1",
            'nombres' => "-1",
            'telefono' => "-1",
            'fecha_nacimiento' => "-1",
            'direccion' => "-1",
            'estado' => "-1",
            'municipio' => "-1",
            'status' => "-1"
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function formatoDia($fecha, $paraDonde) {
    $dia = substr($fecha, 6, 2);
    $mes = substr($fecha, 4, 2);
    $ano = substr($fecha, 0, 4);
    $diaSem = date("N", mktime(0, 0, 0, $mes, $dia, $ano));
    if ($paraDonde == 'tituloCitasXdia') {
        $fechaO = diaSemana($diaSem) . " " . $dia . " DE " . strtoupper(tituloMes($mes)) . " DE " . $ano;
    }
    if ($paraDonde == 'imprimirCita') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    if ($paraDonde == 'citasAreprogramar') {
        $fechaO = $dia . "-" . $mes . "-" . $ano;
    }
    return $fechaO;
}

function getConsultorios() {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
//	$idMedico =  regresarIdMedico($idConsultorio,$idServicio);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM consultorios WHERE st='1' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_consultorio' => $row_query['id_consultorio'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServiciosXConsultorio($idCon) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query = "select id_servicio from servicios_x_consultorio where id_consultorio=$idCon";
    $serv = mysql_query($query);
    $idServicio = mysql_fetch_assoc($serv);
    $idMedico = regresarIdMedico($idCon, $idServicio['id_servicio']);
    $query_query = "SELECT DISTINCT x.id_servicio, s.id_servicio, s.nombre, s.clave FROM servicios_x_consultorio x, servicios s WHERE x.id_consultorio='" . $idCon . "' AND x.id_servicio=s.id_servicio ORDER BY s.nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_servicio' => $row_query['id_servicio'],
                'clave' => $row_query['clave'],
                'nombre' => $row_query['nombre']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicosXServicioXConsultorio($idCon, $idSer) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $idMedico = regresarIdMedico($idCon, $idSer);
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT x.id_consultorio, x.id_servicio, x.id_medico, m.id_medico, m.titulo, m.nombres, m.ap_p, m.ap_m FROM servicios_x_consultorio x, medicos m WHERE x.id_consultorio='" . $idCon . "' AND x.id_servicio='" . $idSer . "' AND x.id_medico=m.id_medico AND m.st='1' ORDER BY m.ap_p ASC, m.ap_m ASC, m.nombres ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_medico' => $row_query['id_medico'],
                'titulo' => $row_query['titulo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function opcionesCon6($idCon, $limites) {
    $consultorios = getConsultorios();
    $totalConsultorios = count($consultorios);
    $out = "";
	$tLimites = count($limites);
    if ($totalConsultorios > 0) {
        if ($idCon == "-1") { // que no se ha seleccionado ningun consultorio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalConsultorios; $i++) {
				for ($z=0; $z<$tLimites; $z++) {
					if ($limites[$z]["id_consultorio"] == $consultorios[$i]['id_consultorio']) {
						if ($seleccionado == false) {
							$out.= "<option selected value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
							$seleccionado = true;
							$_SESSION['IdCon'] = $consultorios[$i]['id_consultorio'];
						} else {
							$out.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
						}
					}
				}			
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalConsultorios; $i++) {
				for ($z=0; $z<$tLimites; $z++) {
					if ($limites[$z]["id_consultorio"] == $consultorios[$i]['id_consultorio']) {
						if ($idCon == $consultorios[$i]['id_consultorio']) {
							$out.= "<option selected value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
							$_SESSION['IdCon'] = $consultorios[$i]['id_consultorio'];
						} else {
							$out.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
						}
					}
				}
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN CONSULTORIOS</option>";
    }
    return $out;
}

function opcionesSer6($idCon, $idSer, $limites) {
    $servicios = getServiciosXConsultorio($idCon);
    $totalServicios = count($servicios);
    $out = "";
	$tLimites = count($limites);
    if ($totalServicios > 0) {
        if ($idSer == "-1") { // que no se ha seleccionado ningun servicio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalServicios; $i++) {
				for ($z=0; $z<$tLimites; $z++) {
					if ($limites[$z]["id_servicio"] == $servicios[$i]['id_servicio']) {
						if ($seleccionado == false) {
							$out.= "<option selected value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
							$seleccionado = true;
							$_SESSION['idServ'] = $servicios[$i]['id_servicio'];
						} else {
							$out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
						}
					}
				}
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalServicios; $i++) {
				for ($z=0; $z<$tLimites; $z++) {
					if ($limites[$z]["id_servicio"] == $servicios[$i]['id_servicio']) {
						if ($idSer == $servicios[$i]['id_servicio']) {
							$out.= "<option selected value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
							$_SESSION['idServ'] = $servicios[$i]['id_servicio'];
						} else {
							$out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
						}
					}
				}
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN SERVICIOS</option>";
    }
    return $out;
}

function opcionesMed6($idCon, $idSer, $idMed, $limites) {
    $medicos = getMedicosXServicioXConsultorio($idCon, $idSer);
    $totalMedicos = count($medicos);
    $out = "";
	$tLimites = count($limites);
    if ($totalMedicos > 0) {
        if ($idMed == "-1") { // que no se ha seleccionado ningun servicio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalMedicos; $i++) {
				for ($z=0; $z<$tLimites; $z++) {
					if ($limites[$z]["id_medico"] == $medicos[$i]['id_medico']) {
						if ($seleccionado == false) {
							$out.= "<option selected value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
							$seleccionado = true;
							$_SESSION['idDr'] = $medicos[$i]['id_medico'];
						} else {
							$out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
						}
					}
				}
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalMedicos; $i++) {
				for ($z=0; $z<$tLimites; $z++) {
					if ($limites[$z]["id_medico"] == $medicos[$i]['id_medico']) {
						if ($idMed == $medicos[$i]['id_medico']) {
							$out.= "<option selected value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
							$_SESSION['idDr'] = $medicos[$i]['id_medico'];
						} else {
							$out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
						}
					}
				}
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN MEDICOS</option>";
    }
    return $out;
}

function getLimitadosXidDistintoCon($id_usuario) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "SELECT DISTINCT id_consultorio FROM usuarios_limitados WHERE id_usuario='" . $id_usuario . "'";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	$ret = array();
	if ($totalRows_query>0){
		do{
			$ret[]=array(
						'id_consultorio' => $row_query['id_consultorio']
					);
		}while($row_query = mysql_fetch_assoc($query));
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}

function getLimitadosXidDistintoSer($id_usuario) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "SELECT DISTINCT id_servicio FROM usuarios_limitados WHERE id_usuario='" . $id_usuario . "'";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	$ret = array();
	if ($totalRows_query>0){
		do{
			$ret[]=array(
						'id_servicio' => $row_query['id_servicio']
					);
		}while($row_query = mysql_fetch_assoc($query));
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}

function getLimitadosXidDistintoMed($id_usuario) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "SELECT DISTINCT id_medico FROM usuarios_limitados WHERE id_usuario='" . $id_usuario . "'";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	$ret = array();
	if ($totalRows_query>0){
		do{
			$ret[]=array(
						'id_medico' => $row_query['id_medico']
					);
		}while($row_query = mysql_fetch_assoc($query));
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}

function getLimitadosXid($id_usuario) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "SELECT * FROM usuarios_limitados WHERE id_usuario='" . $id_usuario . "'";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	$ret = array();
	if ($totalRows_query>0){
		do{
			$ret[]=array(
						'id_limite' => $row_query['id_limite'],
						'id_usuario' => $row_query['id_usuario'],
						'id_consultorio' => $row_query['id_consultorio'],
						'id_servicio' => $row_query['id_servicio'],
						'id_medico' => $row_query['id_medico'],
						'extra' => $row_query['extra']
					);
		}while($row_query = mysql_fetch_assoc($query));
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}

function opcionesCon($idCon) {
    $consultorios = getConsultorios();
    $totalConsultorios = count($consultorios);
    $out = "";
    if ($totalConsultorios > 0) {
        if ($idCon == "-1") { // que no se ha seleccionado ningun consultorio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalConsultorios; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['IdCon'] = $consultorios[$i]['id_consultorio'];
                } else {
                    $out.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalConsultorios; $i++) {
                if ($idCon == $consultorios[$i]['id_consultorio']) {
                    $out.= "<option selected value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                    $_SESSION['IdCon'] = $consultorios[$i]['id_consultorio'];
                } else {
                    $out.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . ponerAcentos($consultorios[$i]['nombre']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN CONSULTORIOS</option>";
    }
    return $out;
}

function opcionesSer($idCon, $idSer) {
    $servicios = getServiciosXConsultorio($idCon);
    $totalServicios = count($servicios);
    $out = "";
    if ($totalServicios > 0) {
        if ($idSer == "-1") { // que no se ha seleccionado ningun servicio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalServicios; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['idServ'] = $servicios[$i]['id_servicio'];
                } else {
                    $out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalServicios; $i++) {
                if ($idSer == $servicios[$i]['id_servicio']) {
                    $out.= "<option selected value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                    $_SESSION['idServ'] = $servicios[$i]['id_servicio'];
                } else {
                    $out.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . ponerAcentos($servicios[$i]['nombre']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN SERVICIOS</option>";
    }
    return $out;
}

function opcionesMed($idCon, $idSer, $idMed) {
    $medicos = getMedicosXServicioXConsultorio($idCon, $idSer);
    $totalMedicos = count($medicos);
    $out = "";
    if ($totalMedicos > 0) {
        if ($idMed == "-1") { // que no se ha seleccionado ningun servicio, seleccionamos el primero de la base de datos 
            $seleccionado = false;
            for ($i = 0; $i < $totalMedicos; $i++) {
                if ($seleccionado == false) {
                    $out.= "<option selected value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                    $seleccionado = true;
                    $_SESSION['idDr'] = $medicos[$i]['id_medico'];
                } else {
                    $out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                }
            }
        } else { // seleccionamos de la base de datos el consultorio con idCon;
            for ($i = 0; $i < $totalMedicos; $i++) {
                if ($idMed == $medicos[$i]['id_medico']) {
                    $out.= "<option selected value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                    $_SESSION['idDr'] = $medicos[$i]['id_medico'];
                } else {
                    $out.= "<option value=\"" . $medicos[$i]['id_medico'] . "\">" . ponerAcentos($medicos[$i]['ap_p'] . " " . $medicos[$i]['ap_m'] . " " . $medicos[$i]['nombres']) . "</option>";
                }
            }
        }
    } else {
        $out = "<option value=\"-1\">NO EXISTEN MEDICOS</option>";
    }
    return $out;
}

function getDH($where, $order) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
	mysql_set_charset('utf8');
    $query_query = "SELECT * FROM derechohabientes " . $where . " " . $order;
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = /*array(
                'id_derecho' => $row_query['id_derecho'],
                'cedula' => $row_query['cedula'],
                'cedula_tipo' => $row_query['cedula_tipo'],
                'ap_p' => $row_query['ap_p'],
                'ap_m' => $row_query['ap_m'],
                'nombres' => $row_query['nombres'],
                'fecha_nacimiento' => $row_query['fecha_nacimiento'],
                'telefono' => $row_query['telefono'],
                'direccion' => $row_query['direccion'],
                'estado' => $row_query['estado'],
                'municipio' => $row_query['municipio'],
                'status' => $row_query['status']
            )*/$row_query;
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function buscarDHxCedulaParaCita($cedula) {
    $dhs = getDH("WHERE cedula like '%" . $cedula . "%'", "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) ."|".$dhs[$i]['Codigo_Postal']."|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] ."|".$dhs[$i]['unidad_medica']."|".$dhs[$i]['email']."|".$dhs[$i]['celular']."|".$dhs[$i]['sexo']. "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE CEDULA QUE CONTENGA " . strtoupper($cedula) . "</option>";
		if (($_SESSION['tipoUsuario'] == '1') || ($_SESSION['tipoUsuario'] == '5'))
        	$out2 = " <input name=\"agregarDH\" type=\"button\" value=\"Agregar Derechohabiente\" class=\"botones\" id=\"agregarDH\" onClick=\"javascript: agregarDHenCita();\" />";
    }
//	$out.= "<option value=\"fas\">" . $totalMedicos . "</option>";
    $out .= "</select>";
    return $out . $out2;
}

function buscarDHxNombreParaCita($ap_p, $ap_m, $nombre) {
    $temp = "";
    if (strlen($ap_p) > 0) {
        $temp .= "ap_p like '%" . $ap_p . "%'";
    }
    if (strlen($ap_m) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " and ap_m like '%" . $ap_m . "%'";
        } else {
            $temp .= "ap_m like '%" . $ap_m . "%'";
        }
    }
    if (strlen($nombre) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " and nombres like '%" . $nombre . "%'";
        } else {
            $temp .= "nombres like '%" . $nombre . "%'";
        }
    }
    $dhs = getDH("WHERE " . quitarAcentos($temp), "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE PACIENTE CON DATOS PROPORCIONADOS</option>";
		if (($_SESSION['tipoUsuario'] == '1') || ($_SESSION['tipoUsuario'] == '5'))
	        $out2 = " <input name=\"agregarDH\" type=\"button\" value=\"Agregar Derechohabiente\" class=\"botones\" id=\"agregarDH\" onClick=\"javascript: agregarDHenCita();\" />";
    }
//	$out.= "<option value=\"fas\">" . $totalMedicos . "</option>";
    $out .= "</select>";
    return $out . $out2;
}

function buscarDHxCedulaParaBusqueda($cedula) {
    $dhs = getDH("WHERE cedula like '%" . $cedula . "%'", "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE CEDULA QUE CONTENGA " . strtoupper($cedula) . "</option>";
        $out2 = "";
    }
    $out .= "</select>";
    return $out . $out2;
}

function buscarDHxNombreParaBusqueda($ap_p, $ap_m, $nombre) {
    $temp = "";
    if (strlen($ap_p) > 0) {
        $temp .= "ap_p like '%" . $ap_p . "%'";
    }
    if (strlen($ap_m) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR ap_m like '%" . $ap_m . "%'";
        } else {
            $temp .= "ap_m like '%" . $ap_m . "%'";
        }
    }
    if (strlen($nombre) > 0) {
        if (strlen($temp) > 0) {
            $temp .= " OR nombres like '%" . $nombre . "%'";
        } else {
            $temp .= "nombres like '%" . $nombre . "%'";
        }
    }
    $dhs = getDH("WHERE " . $temp, "ORDER BY cedula_tipo");
    $totaldhs = count($dhs);
    $out = "<select name=\"dh\" id=\"dh\">";
    $out2 = "";
    if ($totaldhs > 0) {
        for ($i = 0; $i < $totaldhs; $i++) {
            $out.= "<option value=\"" . $dhs[$i]['id_derecho'] . "|" . $dhs[$i]['cedula'] . "|" . $dhs[$i]['cedula_tipo'] . "|" . ponerAcentos($dhs[$i]['ap_p']) . "|" . ponerAcentos($dhs[$i]['ap_m']) . "|" . ponerAcentos($dhs[$i]['nombres']) . "|" . ponerAcentos($dhs[$i]['telefono']) . "|" . ponerAcentos($dhs[$i]['direccion']) . "|" . $dhs[$i]['estado'] . "|" . $dhs[$i]['municipio'] . "|" . $dhs[$i]['fecha_nacimiento'] . "\">" . $dhs[$i]['cedula'] . "/" . $dhs[$i]['cedula_tipo'] . " " . ponerAcentos($dhs[$i]['ap_p']) . " " . ponerAcentos($dhs[$i]['ap_m']) . " " . ponerAcentos($dhs[$i]['nombres']) . "</option>";
        }
    } else {
        $out .= "<option value=\"-1\">NO EXISTE PACIENTE CON DATOS PROPORCIONADOS</option>";
        $out2 = "";
    }
    $out .= "</select>";
    return $out . $out2;
}

function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function ejecutarSQL($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query = mysql_query($query_query, $bdissste); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $error;
}

function ejecutarLOG($log) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "INSERT INTO bitacora VALUES('" . date('Ymd') . "','" . date('Hi') . "','" . getRealIpAddr() . "','" . $log . "','" . $_SESSION['idUsuario'] . "')";
    $query = mysql_query($query_query, $bdissste); //or die(mysql_error());
    $error[0] = mysql_errno();
    $error[1] = mysql_error();
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $error;
}

function getCitaDatos($id_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_cita='" . $id_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_horario' => $row_query['id_horario'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitaExtemporaneaDatos($id_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_cita='" . $id_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'id_derecho' => $row_query['id_derecho'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function existeDuplica($query_query) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = true;
    } else {
        $ret = false;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function existeCita($id_derecho,$id_servicio,$fecha_cita)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $sql="SELECT
    `citas`.`id_derecho`
FROM
    `sistema_agenducha`.`citas`
    INNER JOIN `sistema_agenducha`.`horarios` 
        ON (`citas`.`id_horario` = `horarios`.`id_horario`)
WHERE (`citas`.`id_derecho` =".$id_derecho."
    AND `citas`.`fecha_cita` =".$fecha_cita."
    AND `horarios`.`id_servicio` =".$id_servicio.");";
$query=mysql_query($sql);
$citas=mysql_num_rows($query);
//echo $sql;
//echo 'citas normales '.$citas.'<br>';
$sql2="SELECT
    `id_derecho`
    , `id_servicio`
    , `fecha_cita`
FROM
    `sistema_agenducha`.`citas_extemporaneas`
WHERE (`id_derecho` =".$id_derecho."
    AND `id_servicio` =".$id_servicio."
    AND `fecha_cita` =".$fecha_cita.");";
    $query=mysql_query($sql2);
    //echo $sql;
    $citas1=mysql_num_rows($query);
    //echo 'citas extemporaneas '.$citas1.'<br>';
    $citas+=$citas1;
    //echo 'citas totales '.$citas.'<br>';
    if($citas > 0)
    {
        $ret=true;
    }
    else
    {
        $ret=false;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function existeCitaPrevia($id_derecho,$id_servicio,$fecha_cita,$tipo=-1)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $sql="SELECT
    `citas`.`id_derecho`
FROM
    `sistema_agenducha`.`citas`
    INNER JOIN `sistema_agenducha`.`horarios` 
        ON (`citas`.`id_horario` = `horarios`.`id_horario`)
WHERE (`citas`.`id_derecho` =".$id_derecho."
    AND `citas`.`fecha_cita` <".$fecha_cita."
    AND `horarios`.`id_servicio` =".$id_servicio;
    if( $tipo <> -1) {
        $sql.=" AND `horarios`.`tipo_cita`=".$tipo.");";
    }
    else
    {
        $sql.=");";
    }
    // echo '<pre>'.$sql.'</pre>';
$query=mysql_query($sql);
$citas=mysql_num_rows($query);
//echo $sql;
//echo 'citas normales '.$citas.'<br>';
$sql2="SELECT
    `id_derecho`
    , `id_servicio`
    , `fecha_cita`
FROM
    `sistema_agenducha`.`citas_extemporaneas`
WHERE (`id_derecho` =".$id_derecho."
    AND `id_servicio` =".$id_servicio."
    AND `fecha_cita` <".$fecha_cita;
    if( $tipo <> -1) {
        $sql2.=" AND tipo_cita=".$tipo.");";
    }
    else
    {
        $sql2.=");";   
    }
    // echo '<pre>'.$sql2.'</pre>';
    $query=mysql_query($sql2);
    //echo $sql;
    $citas1=mysql_num_rows($query);
    //echo 'citas extemporaneas '.$citas1.'<br>';
    $citas+=$citas1;
    //echo 'citas totales '.$citas.'<br>';
    if($citas > 0)
    {
        $ret=true;
    }
    else
    {
        $ret=false;
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitaRecienAgregada($id_horario, $id_derecho, $fecha_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_horario='" . $id_horario . "' AND id_derecho='" . $id_derecho . "' AND fecha_cita='" . $fecha_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_horario' => $row_query['id_horario'],
            'id_derecho' => $row_query['id_derecho'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitaERecienAgregada($id_consultorio, $id_servicio, $id_medico, $id_derecho, $fecha_cita, $hora_inicio, $hora_fin) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_consultorio='" . $id_consultorio . "' AND id_servicio='" . $id_servicio . "' AND id_medico='" . $id_medico . "' AND id_derecho='" . $id_derecho . "' AND hora_inicio='" . $hora_inicio . "' AND hora_fin='" . $hora_fin . "' AND fecha_cita='" . $fecha_cita . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'id_derecho' => $row_query['id_derecho'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita'],
            'fecha_cita' => $row_query['fecha_cita'],
            'status' => $row_query['status'],
            'observaciones' => $row_query['observaciones'],
            'id_usuario' => $row_query['id_usuario']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getContraReferencia($id_servicio, $id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $ret = "no";
    $nCitas = 0;
    $bdissste = mysqli_connect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysqli_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM horarios WHERE id_servicio='" . $id_servicio . "'";
    $query = mysqli_query($query_query, $bdissste) or die(mysqli_error());
    $row_query = mysqli_fetch_assoc($query);
    $totalRows_query = mysqli_num_rows($query);
    if ($totalRows_query > 0) {
        do {
            $query_query2 = "SELECT * FROM citas WHERE id_horario='" . $row_query['id_horario'] . "' AND id_derecho='" . $id_derecho . "'";
            $query2 = mysqli_query($query_query2, $bdissste) or die(mysql_error());
            $totalRows_query = mysqli_num_rows($query2);
            $nCitas = $nCitas + $totalRows_query;
        } while ($row_query = mysqli_fetch_assoc($query));
    }
    $query_query2 = "SELECT * FROM citas_extemporaneas WHERE id_servicio='" . $id_servicio . "' AND id_derecho='" . $id_derecho . "' AND tipo_cita!='3'";
    $query2 = mysqli_query($query_query2, $bdissste) or die(mysqli_error());
    $totalRows_query = mysqli_num_rows($query2);
    $nCitas = $nCitas + $totalRows_query;

    @mysql_free_result($query);
    @mysql_close($dbissste);
    if ($nCitas > 3)
        $ret = "si";
    return $ret;
}

function registroCitaServicioDer($id_derecho,$id_servicio)
{
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste=new mysqli($hostname_bdissste,$username_bdissste,$password_bdissste,$database_bdissste);
    $sql="select * from referencia_contrarreferencia where id_derecho=".$id_derecho." AND id_servicio=".$id_servicio;
    $query=$bdissste->query($sql);
    return $query->fetch_assoc();
}

function getHorarioXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM horarios WHERE id_horario='" . $id . "' ORDER BY id_horario";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_horario' => $row_query['id_horario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'dia' => $row_query['dia'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getHorarioEliminadoXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM horarios_eliminados WHERE id_horario='" . $id . "' ORDER BY id_horario";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_horario' => $row_query['id_horario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'id_medico' => $row_query['id_medico'],
            'dia' => $row_query['dia'],
            'hora_inicio' => $row_query['hora_inicio'],
            'hora_fin' => $row_query['hora_fin'],
            'tipo_cita' => $row_query['tipo_cita']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getUsuarioXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM usuarios WHERE id_usuario='" . $id . "' ORDER BY nombre";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_usuario' => $row_query['id_usuario'],
            'login' => $row_query['login'],
            'pass' => $row_query['pass'],
            'nombre' => $row_query['nombre'],
            'tipo_usuario' => $row_query['tipo_usuario'],
            'id_consultorio' => $row_query['id_consultorio'],
            'id_servicio' => $row_query['id_servicio'],
            'status' => $row_query['status']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getServicioXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM servicios WHERE id_servicio='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    if ($totalRows_query > 0) {
        $ret = array(
            'id_servicio' => $row_query['id_servicio'],
            'clave' => $row_query['clave'],
            'nombre' => $row_query['nombre']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getMedicoXid($id) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM medicos WHERE id_medico='" . $id . "'";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        $ret = array(
            'id_medico' => $row_query['id_medico'],
            'cedula' => $row_query['cedula'],
            'cedula_tipo' => $row_query['cedula_tipo'],
            'n_empleado' => $row_query['n_empleado'],
            'ced_pro' => $row_query['cedula_profesional'],
            'titulo' => $row_query['titulo'],
            'ap_p' => $row_query['ap_p'],
            'ap_m' => $row_query['ap_m'],
            'nombres' => $row_query['nombres'],
            'turno' => $row_query['turno'],
            'telefono' => $row_query['telefono'],
            'direccion' => $row_query['direccion'],
            'tipo_medico' => $row_query['tipo_medico'],
            'id_servicio1' => $row_query['id_servicio1'],
            'id_servicio2' => $row_query['id_servicio2'],
            'hora_entrada' => $row_query['hora_entrada'],
            'hora_salida' => $row_query['hora_salida'],
            'intervalo_citas0' => $row_query['intervalo_citas0'],
            'intervalo_citas1' => $row_query['intervalo_citas1'],
            'intervalo_citas2' => $row_query['intervalo_citas2'],
            'observaciones' => $row_query['observaciones'],
            'st' => $row_query['st']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function compararFechas($hoy, $otra) {
    $hoyMK = mktime(0, 0, 0, substr($hoy, 4, 2), substr($hoy, 6, 2), substr($hoy, 0, 4));
    $otraMK = mktime(0, 0, 0, substr($otra, 4, 2), substr($otra, 6, 2), substr($otra, 0, 4));
    if ($otraMK >= $hoyMK)
        return true; else
        return false;
}

function quitarAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "&") {
            $temp = substr($Text, $j, 8);
            switch ($temp) {
                case "&aacute;": $cadena .= "(/a)";
                    $j = $j + 7;
                    break;
                case "&Aacute;": $cadena .= "(/A)";
                    $j = $j + 7;
                    break;
                case "&eacute;": $cadena .= "(/e)";
                    $j = $j + 7;
                    break;
                case "&Eacute;": $cadena .= "(/E)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/i)";
                    $j = $j + 7;
                    break;
                case "&iacute;": $cadena .= "(/I)";
                    $j = $j + 7;
                    break;
                case "&oacute;": $cadena .= "(/o)";
                    $j = $j + 7;
                    break;
                case "&Oacute;": $cadena .= "(/O)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/u)";
                    $j = $j + 7;
                    break;
                case "&uacute;": $cadena .= "(/U)";
                    $j = $j + 7;
                    break;
                case "&ntilde;": $cadena .= "(/n)";
                    $j = $j + 7;
                    break;
                case "&Ntilde;": $cadena .= "(/N)";
                    $j = $j + 7;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            switch ($cara) {
                case "á": $cadena.="(/a)";
                    break;
                case "é": $cadena.="(/e)";
                    break;
                case "í": $cadena.="(/i)";
                    break;
                case "ó": $cadena.="(/o)";
                    break;
                case "ú": $cadena.="(/u)";
                    break;
                case "Á": $cadena.="(/A)";
                    break;
                case "É": $cadena.="(/E)";
                    break;
                case "Í": $cadena.="(/I)";
                    break;
                case "Ó": $cadena.="(/O)";
                    break;
                case "Ú": $cadena.="(/U)";
                    break;
                case "ñ": $cadena.="(/n)";
                    break;
                case "Ñ": $cadena.="(/N)";
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        }
    }
    return $cadena;
}

function ponerAcentos($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "(") {
            $temp = substr($Text, $j, 4);
            switch ($temp) {
                case "(/a)": $cadena .= "&aacute;";
                    $j = $j + 3;
                    break;
                case "(/A)": $cadena .= "&Aacute;";
                    $j = $j + 3;
                    break;
                case "(/e)": $cadena .= "&eacute;";
                    $j = $j + 3;
                    break;
                case "(/E)": $cadena .= "&Eacute;";
                    $j = $j + 3;
                    break;
                case "(/i)": $cadena .= "&iacute;";
                    $j = $j + 3;
                    break;
                case "(/I)": $cadena .= "&Iacute;";
                    $j = $j + 3;
                    break;
                case "(/o)": $cadena .= "&oacute;";
                    $j = $j + 3;
                    break;
                case "(/O)": $cadena .= "&Oacute;";
                    $j = $j + 3;
                    break;
                case "(/u)": $cadena .= "&uacute;";
                    $j = $j + 3;
                    break;
                case "(/U)": $cadena .= "&Uacute;";
                    $j = $j + 3;
                    break;
                case "(/n)": $cadena .= "&ntilde;";
                    $j = $j + 3;
                    break;
                case "(/N)": $cadena .= "&Ntilde;";
                    $j = $j + 3;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            $cadena.=$Text[$j];
        }
    }
    return $cadena;
}

function getHorariosXmedicoXservicioXconsultorio($idConsultorio, $idServicio, $idMedico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM horarios WHERE id_medico='" . $idMedico . "' and id_servicio='" . $idServicio . "'and id_consultorio='" . $idConsultorio . "' ORDER BY hora_inicio";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query2 = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query2 > 0) {
        do {
            $ret[] = array(
                'id_horario' => $row_query['id_horario'],
                'dia' => $row_query['dia'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasXrangoFechas($fechaI, $fechaF) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas WHERE fecha_cita>='" . $fechaI . "' and fecha_cita<='" . $fechaF . "' ORDER BY fecha_cita ASC, id_horario ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_cita'],
                'id_horario' => $row_query['id_horario'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasExtemporaneasXrangoFechas($fechaI, $fechaF) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE fecha_cita>='" . $fechaI . "' and fecha_cita<='" . $fechaF . "' ORDER BY fecha_cita ASC, hora_inicio ASC, hora_fin ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_cita'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function tipoDH($municipio) {
    $ret = "X";
    if (($municipio == "Guadalajara") || ($municipio == "Tlajomulco de Z(/u)(/n)iga") || ($municipio == "Tonal(/a)") || ($municipio == "Zapopan") || ($municipio == "Tlaquepaque"))
        $ret = "&nbsp;";
    return $ret;
}

function queSexoTipoCedula($tipoCedula) {
    $ret = "";
    if (($tipoCedula == "10") || ($tipoCedula == "40") || ($tipoCedula == "41") || ($tipoCedula == "50") || ($tipoCedula == "51") || ($tipoCedula == "70") || ($tipoCedula == "71") || ($tipoCedula == "90") || ($tipoCedula == "92"))
        $ret = "M"; else
        $ret = "F";
    return $ret;
}

function getEdadXrfc($cedula) {
    $diaHoy = date(j);
    $mesHoy = date(n);
    $anoHoy = date(Y);

    $ano = substr($cedula, 4, 2);
    $mes = substr($cedula, 6, 2);
    $dia = substr($cedula, 8, 2);
    if ($ano < 20)
        $ano = "20" . $ano; else
        $ano = "19" . $ano;

    if (($mes == $mesHoy) && ($dia > $diaHoy)) {
        $anoHoy = ($anoHoiy - 1);
    }
    //si el mes es superior al actual tampoco habr· cumplido aÒos, por eso le quitamos un aÒo al actual
    if ($mes > $mesHoy) {
        $anoHoy = ($anoHoy - 1);
    }
    //ya no habrÌa mas condiciones, ahora simplemente restamos los aÒos y mostramos el resultado como su edad
    $edad = ($anoHoy - $ano);
    return $edad;
}

function getEdadXfechaNac($fecha) {
    if (strlen($fecha) == 10) {
        $diaHoy = date('j');
        $mesHoy = date('n');
        $anoHoy = date('Y');

        $ano = substr($fecha, 6, 4);
        $mes = substr($fecha, 3, 2);
        $dia = substr($fecha, 0, 2);
        if (($mes == $mesHoy) && ($dia > $diaHoy)) {
            $anoHoy = ($anoHoy - 1);
        }
        //si el mes es superior al actual tampoco habr· cumplido aÒos, por eso le quitamos un aÒo al actual
        if ($mes > $mesHoy) {
            $anoHoy = ($anoHoy - 1);
        }
        //ya no habrÌa mas condiciones, ahora simplemente restamos los aÒos y mostramos el resultado como su edad
        $edad = ($anoHoy - $ano);
    } else {
        $edad = "ND";
    }
    return $edad;
}

function citasXderechohabiente($id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $hoy = date('Ymd');
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas WHERE id_derecho='" . $id_derecho . "' ORDER BY fecha_cita ASC, id_horario ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_horario' => $row_query['id_horario'],
                'id_derecho' => $row_query['id_derecho'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function citasExtXderechohabiente($id_derecho) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $hoy = date('Ymd');
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_derecho='" . $id_derecho . "' AND fecha_cita<='" . $hoy . "' ORDER BY fecha_cita ASC, hora_inicio ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_horario'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'status' => $row_query['status'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getCitasExtemporaneas($fechaCitas, $idUsuario, $idConsultorio, $idServicio, $idMedico) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_extemporaneas WHERE id_consultorio='" . $idConsultorio . "' AND id_servicio='" . $idServicio . "' AND id_medico='" . $idMedico . "' AND fecha_cita='" . $fechaCitas . "' ORDER BY hora_inicio ASC, hora_fin ASC";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = array();
    if ($totalRows_query > 0) {
        do {
            $ret[] = array(
                'id_cita' => $row_query['id_cita'],
                'id_consultorio' => $row_query['id_consultorio'],
                'id_servicio' => $row_query['id_servicio'],
                'id_medico' => $row_query['id_medico'],
                'id_derecho' => $row_query['id_derecho'],
                'hora_inicio' => $row_query['hora_inicio'],
                'hora_fin' => $row_query['hora_fin'],
                'tipo_cita' => $row_query['tipo_cita'],
                'fecha_cita' => $row_query['fecha_cita'],
                'id_usuario' => $row_query['id_usuario'],
                'extra1' => $row_query['extra1'],
                'status' => $row_query['status'],
                'observaciones' => $row_query['observaciones']
            );
        } while ($row_query = mysql_fetch_assoc($query));
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function getDetallesCita($id_cita, $tipo) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    $bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($database_bdissste, $bdissste);
    $query_query = "SELECT * FROM citas_detalles WHERE id_cita='" . $id_cita . "' AND tipo='" . $tipo . "' LIMIT 1";
    $query = mysql_query($query_query, $bdissste) or die(mysql_error());
    $row_query = mysql_fetch_assoc($query);
    $totalRows_query = mysql_num_rows($query);
    $ret = "";
    if ($totalRows_query > 0) {
        $ret = array(
            'id_cita' => $row_query['id_cita'],
            'tipo' => $row_query['tipo'],
            'campo1' => $row_query['campo1'],
            'campo2' => $row_query['campo2'],
            'campo3' => $row_query['campo3']
        );
    }
    @mysql_free_result($query);
    @mysql_close($dbissste);
    return $ret;
}

function mostrarMes() {
    if (!isset($_GET['getdate'])) {
        $unix_time = strtotime(date('Ymd'));
    } else {
        $unix_time = strtotime($_GET['getdate']);
    }

    $display_date = tituloMes(date('n', $unix_time)) . " " . date('Y', $unix_time);
    $meses = "<select id=\"meses\" name=\"meses\" onchange=\"javascript: cambiarMes(this.value);\">";
    for ($i = 6; $i > 0; $i--) {
        $fechaCompleta = date('Ymd', strtotime(date("Y/m/d", $unix_time) . " - " . $i . " month"));
        $mes = date('n', strtotime(date("Y/m/d", $unix_time) . " - " . $i . " month"));
        $anio = date('Y', strtotime(date("Y/m/d", $unix_time) . " - " . $i . " month"));
        $meses .= "<option value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
    }
    for ($i = 0; $i < 7; $i++) {
        $fechaCompleta = date('Ymd', strtotime(date("Y/m/d", $unix_time) . " + " . $i . " month"));
        $mes = date('n', strtotime(date("Y/m/d", $unix_time) . " + " . $i . " month"));
        $anio = date('Y', strtotime(date("Y/m/d", $unix_time) . " + " . $i . " month"));
        if ($i == 0) {
            $meses .= "<option selected value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
        } else {
            $meses .= "<option value=\"" . $fechaCompleta . "\">" . tituloMes($mes) . " " . $anio . "</option>";
        }
    }
    $meses .= "</select>";
    $display_date = $meses;

    $MC = array();
    $MC['title'] = $display_date;
    $MC['webcals'] = array();
    return $MC;
}

function citaMedico($id_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdissste);
    $sqlD = "select * from citas_medico where id_cita=$id_cita";
    $query = mysql_query($sqlD);
    $res = array();
    if (mysql_num_rows($query) > 0)
        $res = mysql_fetch_assoc($query);
    return $res;
}

function ejecutarConsulta($sql) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdissste);
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_errno() == 0)
        if (mysql_num_rows($query) > 0)
            $ret = mysql_fetch_array($query);
    return $ret;
}

function contarCitas($sql) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdissste);
    $query = mysql_query($sql);
    $ret = 0;
    if (mysql_errno() == 0)
        if (mysql_num_rows($query) > 0)
            $ret = mysql_num_rows($query);
    return $ret;
}

function contarCitas_sub($idServicio,$idDerecho,$fecha_cita) {
    global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
    mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
    mysql_select_db($database_bdissste);
    $query = "select * from citas where id_derecho=".$idDerecho." and fecha_cita <'".$fecha_cita."' and id_horario in (select id_horario from horarios where id_servicio=".$idServicio." and tipo_cita != 0)";
    $query=mysql_query($query);
    $ret = 0;
    if (mysql_errno() == 0)
        {
            $ret = mysql_num_rows($query);
            $query="select * from citas_extemporaneas where id_derecho=".$idDerecho." id_servicio=".$idServicio." and tipo_cita != 0 and fecha_cita <'".$fecha_cita."'";
            $ret2=mysql_query($query);
            $ret+=mysql_num_rows($ret2);
        }
    return $ret;
}

function ponerAcentosCP($Text) {
    $cadena = "";
    $temp = "";
    $total = strlen($Text);
    for ($j = 0; $j < $total; $j++) {
        $cara = $Text[$j];
        if ($cara == "(") {
            $temp = substr($Text, $j, 4);
            switch ($temp) {
                case "(/a)": $cadena .= "á";
                    $j = $j + 3;
                    break;
                case "(/A)": $cadena .= "Á";
                    $j = $j + 3;
                    break;
                case "(/e)": $cadena .= "é";
                    $j = $j + 3;
                    break;
                case "(/E)": $cadena .= "É";
                    $j = $j + 3;
                    break;
                case "(/i)": $cadena .= "í";
                    $j = $j + 3;
                    break;
                case "(/I)": $cadena .= "Í";
                    $j = $j + 3;
                    break;
                case "(/o)": $cadena .= "ó";
                    $j = $j + 3;
                    break;
                case "(/O)": $cadena .= "Ó";
                    $j = $j + 3;
                    break;
                case "(/u)": $cadena .= "ú";
                    $j = $j + 3;
                    break;
                case "(/U)": $cadena .= "Ú";
                    $j = $j + 3;
                    break;
                case "(/n)": $cadena .= "ñ";
                    $j = $j + 3;
                    break;
                case "(/N)": $cadena .= "Ñ";
                    $j = $j + 3;
                    break;
                default:
                    $cadena.=$Text[$j];
                    break;
            }
        } else {
            $cadena.=$Text[$j];
        }
    }
    return $cadena;
}

function getUsuarioReferencia($id)
{
	global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
	mysql_connect($hostname_bdissste,$username_bdissste,$password_bdissste);
	mysql_select_db($database_bdissste);
	$sql="select * from usuarios_contrarreferencias where id_usuario=".$id;
	$query=mysql_query($sql);
	$ret=array();
	if(mysql_num_rows($query)>0)
		$ret=mysql_fetch_array($query);
	return $ret;
}

function getCitaClinica($idCita)
{
	global $hostname_bdissste;
    global $username_bdissste;
    global $password_bdissste;
    global $database_bdissste;
	mysql_connect($hostname_bdissste,$username_bdissste,$password_bdissste);
	mysql_select_db($database_bdissste);
	$sql="select * from citas_apartadas where id_cita=".$idCita;
	$query=mysql_query($sql);
	$ret=array();
	if(mysql_num_rows($query)>0)
		$ret=mysql_fetch_array($query);
	return $ret;
}
?>