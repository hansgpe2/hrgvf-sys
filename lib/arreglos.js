// JavaScript Document
var palInc=["BUEI","CACA","CAGA","CAKA","COGE","COJE","COJO","FETO","JOTO","BUEY","CACO","CAGO","CAKO","COJA","COJI","CULO","GUEY","KACA","KACO","KAGA","KAGO","KOGE","KOJO","KAKA","KULO","MAME","MAMO","MEAR","MEAS","MEON","MION","MOCO","MULA","PEDA","PEDO","PENE","PUTA","PUTO","QULO","RATA","RUIN"];
var sustPalInc=["BUEX","CACX","CAGX","CAKX","COGX","COJX","COJX","FETX","JOTX","BUEX","CACX","CAGX","CAKX","COJX","COJX","CULX","GUEX","KACX","KACX","KAGX","KAGX","KOGX","KOJX","KAKX","KULX","MAMX","MAMX","MEAX","MEAX","MEOX","MIOX","MOCX","MULX","PEDX","PEDX","PENX","PUTX","PUTX","QULX","RATX","RUIX"];
function ponerAcentos2(Text) {
    var cadena=""; 
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)  
    {  
        var cara=Text.charAt(j);
        if (cara == "(") {
            temp = Text.substring(j,j+4);
            switch (temp) {
                case "(/a)":
                    cadena += "u00e1";
                    j = j + 3;
                    break;
                case "(/A)":
                    cadena += "\u00c1";
                    j = j + 3;
                    break;
                case "(/e)":
                    cadena += "\u00e9";
                    j = j + 3;
                    break;
                case "(/E)":
                    cadena += "\u00c9";
                    j = j + 3;
                    break;
                case "(/i)":
                    cadena += "\u00ed";
                    j = j + 3;
                    break;
                case "(/I)":
                    cadena += "\u00cd";
                    j = j + 3;
                    break;
                case "(/o)":
                    cadena += "\u00f3";
                    j = j + 3;
                    break;
                case "(/O)":
                    cadena += "\u00d3";
                    j = j + 3;
                    break;
                case "(/u)":
                    cadena += "\u00fa";
                    j = j + 3;
                    break;
                case "(/U)":
                    cadena += "\u00da";
                    j = j + 3;
                    break;
                case "(/n)":
                    cadena += "\u00f1";
                    j = j + 3;
                    break;
                case "(/N)":
                    cadena += "\u00d1";
                    j = j + 3;
                    break;
                default:
                    cadena+=Text.charAt(j);  
                    break;  
            }
        } else {
            cadena+=Text.charAt(j);  
        }
    }  
    return cadena;
}


function imprimir_exp () {
    window.open('expedientesImprimir.php?id_expediente=' + document.getElementById('id_derecho').value, 'imp_exp');
}

function codigoBarras(e) {
    var keynum
    var keychar
    var numcheck
	
    if(window.event) // IE
    {
        keynum = e.keyCode
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
        keynum = e.which
    }
    keychar = String.fromCharCode(keynum)
    numcheck = /[\d\b]/
    return numcheck.test(keychar)
}

function buscarCodigoBarrasExp() {
    if (document.getElementById("cod_bar").value.length == 8) {
        var contenedor2;
        contenedor2 = document.getElementById('buscando');
        var agenda= new AjaxGET();
        agenda.open("POST", "buscarExpediente.php?cod_bar=" + document.getElementById("cod_bar").value,true);
        agenda.onreadystatechange=function()
        {
            if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                if (agenda.responseText.length == 1) {
                    alert('No se encontro datos de derechohabiente con el codigo de barras ingresado');
                } else {
                    valor = agenda.responseText;
                    valor = ponerAcentos2(valor);
                    if (valor == -1) {
                        contenedor2.innerHTML ='No existe dh con este codigo';
                        document.getElementById('id_derecho').value = "";
                        document.getElementById('cedula').value = "";
                        document.getElementById('ap_p').value = "";
                        document.getElementById('ap_m').value = "";
                        document.getElementById('nombre').value = "";
                        document.getElementById('cedulaBuscar').value = '';
                        //						document.getElementById('derechohabientes').innerHTML = '';
                        document.getElementById('agregar').disabled = 'disabled';			
                        document.getElementById('modificar').disabled = 'disabled';			
                    } else {
                        aValor = valor.split('|');
                        document.getElementById('id_derecho').value = aValor[0];
                        document.getElementById('cedula').value = aValor[1];
                        document.getElementById('sexo').value=aValor[16];
                        ObtenerTpoDerXSexo('sexo','cedulaTipoCita');
                        document.getElementById('cedulaTipoCita').value=aValor[2];
                        document.getElementById('ap_p').value = aValor[3];
                        document.getElementById('ap_m').value = aValor[4];
                        document.getElementById('nombre').value = aValor[5];
                        document.getElementById('direccion').value=aValor[7];
                        document.getElementById('col1').value=aValor[8];
                        document.getElementById('cp1').value=aValor[9];
                        document.getElementById('estado').value=aValor[10];
                        cargarMunicipiosConsulta(aValor[10],'municipio','UnidadCita',aValor[13]);
                        document.getElementById('municipio').value=aValor[11];
                        document.getElementById('emailCita').value=aValor[14];
                        document.getElementById('cel1').value=aValor[15];
                        document.getElementById('telefono').value=aValor[6];
                        document.getElementById('fecha_nac').value=aValor[12];
                        document.getElementById('cedulaBuscar').value = '';
                        //						document.getElementById('derechohabientes').innerHTML = 'Ingrese la cedula del derechohabiente y haga click en Buscar...';
                        document.getElementById('agregar').disabled = '';			
                        document.getElementById('modificar').disabled = '';			
                        contenedor2.innerHTML ='';
                    }
                }
            }
            if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
            {
                contenedor2.innerHTML = "cargando...";
            }
        }
        agenda.send(null)
    }
}

function solicitudExpedientes(idUsuario, idConsultorio, idServicio, idMedico){
    var fechaI = document.getElementById('date1');
    var fechaF = document.getElementById('date2');
    f1=fechaI.value.length;
    f2=fechaF.value.length;
    if (f1==0 || f2==0){
        alert("Ambas fechas deben estar establecidas!");
        return false;
    }
	
    var fechaIn = fechaI.value;
    var fechaFi = fechaF.value;
    var diaIn = fechaIn.substring(0,2); 
    var mesIn = (fechaIn.substring(3,5))*1 - 1;
    var anoIn = fechaIn.substring(6,10); 
    var fechaInU = new Date(anoIn,mesIn,diaIn);
    var diaFi = fechaFi.substring(0,2); 
    var mesFi = (fechaFi.substring(3,5))*1 - 1;
    var anoFi = fechaFi.substring(6,10); 
    var fechaFiU = new Date(anoFi,mesFi,diaFi);
    if(fechaInU > fechaFiU) {
        alert("La fecha final no puede ser menor a la fecha inicial");
    } else {
        var rg1 = document.getElementById('RadioGroup1_0');
        var rg2 = document.getElementById('RadioGroup1_1');
        var rg3 = document.getElementById('RadioGroup1_2');
        var rg4 = document.getElementById('RadioGroup1_3');
        if (rg1.checked) tipoReporte = "unidad";
        if (rg2.checked) tipoReporte = "consultorio";
        if (rg3.checked) tipoReporte = "servicio";
        if (rg4.checked) tipoReporte = "medico";
        window.open("SOLICITUD_EXPEDIENTES.php?idConsultorio="+idConsultorio+"&idServicio="+idServicio+"&idMedico="+idMedico+"&idUsuario="+idUsuario+"&tipoReporte="+tipoReporte+"&fechaI="+fechaIn+"&fechaF="+fechaFi,'_blank');

    /*
		var contenedor2 = document.getElementById('contenido');
		var agenda= new AjaxGET();
		agenda.open("GET", "SOLICITUD_EXPEDIENTES.php?idConsultorio="+idConsultorio+"&idServicio="+idServicio+"&idMedico="+idMedico+"&idUsuario="+idUsuario+"&tipoReporte="+tipoReporte+"&fechaI="+fechaIn+"&fechaF="+fechaFi,true);
		agenda.onreadystatechange=function()
		{
			if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor2.innerHTML = agenda.responseText;
			}
			if (agenda.readyState==2)
			{
				contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
			}
		}
		agenda.send(null)
*/	}
    return false;
}



var statusCitas = new Array("DISPONIBLE","CITA PENDIENTE","CITA CUBIERTA","DE VACACIONES","EN CONGRESO","DIA INHABIL","LICENCIA MEDICA","BAJA TEMPORAL","OTRO","ELIMINADA");
var statusDerecho = new Array("ACTIVO","INACTIVO","OTRO");
var statusUsuario = new Array("ACTIVO","INACTIVO");
var tipoCita = new Array("PRIMERA VEZ","SUBSECUENTE","PROCEDIMIENTO");
var titulos = new Array("DR.","DRA.");
var tipoUsuario = new Array("ADMINISTRADOR","CAPTURISTA","CAPTURISTA ESPECIAL","ARCHIVO","REPORTES");
paraRestaurarModificacionesDHenAgregarCitaAp_p = "";
paraRestaurarModificacionesDHenAgregarCitaAp_m = "";
paraRestaurarModificacionesDHenAgregarCitaNombre = "";
paraRestaurarModificacionesDHenAgregarCitaTel = "";
paraRestaurarModificacionesDHenAgregarCitaDir = "";
restaurarUmf="";
restaurarSexo="";
restaurarTpoDer="";
tipo_cedulas = new Array("10","20","30","31","32","40","41","50","51","60","61","70","71","80","81","90","91","92","99");

var estados = new Array();
estados['Zacatecas'] = new Array('Apozol', 'Apulco', 'Atolinga', 'Benito Juarez', 'Calera', 'Canitas de Felipe Pescador', 'Concepcion del Oro', 'Cuauhtemoc', 'Chalchihuites', 'Fresnillo', 'Trinidad Garcia de la Cadena', 'Genaro Codina', 'General Enrique Estrada', 'General Francisco R. Murguia', 'El Plateado de Joaquin Amaro', 'General Panfilo Natera', 'Guadalupe', 'Huanusco', 'Jalpa', 'Jerez', 'Jimenez del Teul', 'Juan Aldama', 'Juchipila', 'Loreto', 'Luis Moya', 'Mazapil', 'Melchor Ocampo', 'Mezquital del Oro', 'Miguel Auza', 'Momax', 'Monte Escobedo', 'Morelos', 'Moyahua de Estrada', 'Nochistlan de Mejia', 'Noria de angeles', 'Ojocaliente', 'Panuco', 'Pinos', 'Rio Grande', 'Sain Alto', 'El Salvador', 'Sombrerete', 'Susticacan', 'Tabasco', 'Tepechitlan', 'Tepetongo', 'Teul de Gonzalez Ortega', 'Tlaltenango de Sanchez Roman', 'Valparaiso', 'Vetagrande', 'Villa de Cos', 'Villa Garcia', 'Villa Gonzalez Ortega', 'Villa Hidalgo', 'Villanueva', 'Zacatecas', 'Trancoso', 'Santa Maria de la Paz', 'Otro');

estados['Yucatan'] = new Array('Abala', 'Acanceh', 'Akil', 'Baca', 'Bokoba', 'Buctzotz', 'Cacalchen', 'Calotmul', 'Cansahcab', 'Cantamayec', 'Celestun', 'Cenotillo', 'Conkal', 'Cuncunul', 'Cuzama', 'Chacsinkin', 'Chankom', 'Chapab', 'Chemax', 'Chicxulub Pueblo', 'Chichimila', 'Chikindzonot', 'Chochola', 'Chumayel', 'Dzan', 'Dzemul', 'Dzidzantun', 'Dzilam de Bravo', 'Dzilam Gonzalez', 'Dzitas', 'Dzoncauich', 'Espita', 'Halacho', 'Hocaba', 'Hoctun', 'Homun', 'Huhi', 'Hunucma', 'Ixil', 'Izamal', 'Kanasin', 'Kantunil', 'Kaua', 'Kinchil', 'Kopoma', 'Mama', 'Mani', 'Maxcanu', 'Mayapan', 'Merida', 'Mococha', 'Motul', 'Muna', 'Muxupip', 'Opichen', 'Oxkutzcab', 'Panaba', 'Peto', 'Progreso', 'Quintana Roo', 'Rio Lagartos', 'Sacalum', 'Samahil', 'Sanahcat', 'San Felipe', 'Santa Elena', 'Seye', 'Sinanche', 'Sotuta', 'Sucila', 'Sudzal', 'Suma', 'Tahdziu', 'Tahmek', 'Teabo', 'Tecoh', 'Tekal de Venegas', 'Tekanto', 'Tekax', 'Tekit', 'Tekom', 'Telchac Pueblo', 'Telchac Puerto', 'Temax', 'Temozon', 'Tepakan', 'Tetiz', 'Teya', 'Ticul', 'Timucuy', 'Tinum', 'Tixcacalcupul', 'Tixkokob', 'Tixmehuac', 'Tixpehual', 'Tizimin', 'Tunkas', 'Tzucacab', 'Uayma', 'Ucu', 'Uman', 'Valladolid', 'Xocchel', 'Yaxcaba', 'Yaxkukul', 'Yobain', 'Otro');

estados['Veracruz'] = new Array('Acajete', 'Acatlan', 'Acayucan', 'Actopan', 'Acula', 'Acultzingo', 'Agua Dulce', 'Alpatlahuac', 'Alto Lucero de Gutierrez Barrios', 'Altotonga', 'Alvarado', 'Amatitlan', 'Amatitlan de los Reyes', 'angel R. Cabada', 'Apazapan', 'Aquila', 'Astacinga', 'Atlahuilco', 'Atoyac', 'Atzacan', 'Atzalan', 'Ayahualulco', 'Banderilla', 'Benito Juarez', 'Boca del Rio', 'Calcahualco', 'Camaron de Tejada', 'Camerino Z. Mendoza', 'Carlos A. Carrillo', 'Carrillo Puerto', 'Castillo de Teayo', 'Catemaco', 'Cazones de Herrera', 'Cerro Azul', 'Chacaltianguis', 'Chalma', 'Chiconamel', 'Chiconquiaco', 'Chicontepec', 'Chinameca', 'Chinampa de Gorostiza', 'Chocoman', 'Chontla', 'Chumatlan', 'Citlaltepetl', 'Coacoatzintla', 'Coahuitlan (Progreso de Zaragoza)', 'Coatepec', 'Coatzacoalcos', 'Coatzintla', 'Coetzala', 'Colipa', 'Comapa', 'Cordoba', 'Cosamaloapan', 'Cosautlan de Carvajal', 'Coscomatepec', 'Cosoleacaque', 'Cotaxtla', 'Coxquihi', 'Coyutla', 'Cuichapa', 'Cuitlahuac', 'El Higo', 'Emiliano Zapata', 'Espinal', 'Filomeno Mata', 'Fortin', 'Gutierrez Zamora', 'Hidalgotitlan', 'Huatusco', 'Huayacocotla', 'Hueyapan de Ocampo', 'Huiloapan de Cuauhtemoc', 'Igancio de la Llave', 'Ilamatlan', 'Isla', 'Ixcatepec', 'Ixhuacan de los Reyes', 'Ixhuatlan de Madero', 'Ixhuatlan del Cafe', 'Ixhuatlan del Sureste', 'Ixhuatlancillo', 'Ixmatlahuacan', 'Ixtaczoquitlan', 'Jalancingo', 'Jalcomulco', 'Jaltipan', 'Jamapa', 'Jesus Carranza', 'Jilotepec', 'Jose Azueta', 'Juan Rodriguez Clara', 'Juchique de Ferrer', 'La Antigua', 'La Perla', 'Landero y Coss', 'Las Choapas', 'Las Minas', 'Las Vigas de Ramirez', 'Lerdo de Tejada', 'Los Reyes', 'Magdalena', 'Maltrata', 'Manlio Fabio Altamirano', 'Mariano Escobedo', 'Martinez de la Torre', 'Mecatlan', 'Mecayapan', 'Medellin', 'Mihuatlan', 'Minatitlan', 'Misantla', 'Mixtla de Altamirano', 'Moloacan', 'Nanchital de Lazaro Cardenas del Rio', 'Naolinco', 'Naranjal', 'Naranjos-Amatlan', 'Nautla', 'Nogales', 'Oluta', 'Omealca', 'Orizaba', 'Otatitlan', 'Oteapan', 'Ozuluama', 'Pajapan', 'Panuco', 'Papantla', 'Paso de Ovejas', 'Paso del Macho', 'Perote', 'Platon Sanchez', 'Playa Vicente', 'Poza Rica de Hidalgo', 'Pueblo Viejo', 'Puente Nacional', 'Rafael Delgado', 'Rafael Lucio', 'Rio Blanco', 'Saltabarranca', 'San Andres Tenejapan', 'San Andres Tuxtla', 'San Juan Evangelista', 'San Rafael', 'Santiago Sochiapan', 'Santiago Tuxtla', 'Sayula de Aleman', 'Sochiapa', 'Soconusco', 'Soledad Atzompa', 'Soledad de Doblado', 'Soteapan', 'Tamalin', 'Tamiahua', 'Tampico Alto', 'Tancoco', 'Tantima', 'Tantoyuca', 'Tatahuicapan de Juarez', 'Tatatila', 'Tecolutla', 'Tehuipango', 'Temapache', 'Tempoal', 'Tenampa', 'Tenochtitlan', 'Teocelo', 'Tepatlaxco', 'Tepetlan', 'Tepetzintla', 'Tequila', 'Texcatepec', 'Texhuacan', 'Texistepec', 'Tezonapa', 'Tierra Blanca', 'Tihuatlan', 'Tlachichilco', 'Tlacojalpan', 'Tlacolulan', 'Tlacotalpan', 'Tlacotepec de Mejia', 'Tlalixcoyan', 'Tlalnelhuayocan', 'Tlaltetela', 'Tlapacoyan', 'Tlaquilpan', 'Tlilapan', 'Tomatlan', 'Tonayan', 'Totutla', 'Tres Valles', 'Tuxpan', 'Tuxtilla', 'ursulo Galvan', 'Uxpanapa', 'Vega de Alatorre', 'Veracruz', 'Villa Aldama', 'Xalapa', 'Xico', 'Xoxocotla', 'Yanga', 'Yecuatla', 'Zacualpan', 'Zaragoza', 'Zentla', 'Zongolica', 'Zontecomatlan', 'Zozocolco', 'Otro');

estados['Tlaxcala'] = new Array('Amaxac de Guerrero', 'Apetatitlan de Antonio Carvajal', 'Atlangatepec', 'Altzayanca', 'Apizaco', 'Benito Juarez', 'Calpulalpan', 'El Carmen Tequexquitla', 'Cuapiaxtla', 'Cuaxomulco', 'Emiliano Zapata', 'Santa Ana Chiautempan', 'Munoz de Domingo Arenas', 'Espanita', 'Huamantla', 'Hueyotlipan', 'Ixtacuixtla de Mariano Matamoros', 'Ixtenco', 'Mazatecochco de Jose Maria Morelos', 'Contla de Juan Cuamatzi', 'Tepetitla de Lardizabal', 'Sanctorum de Lazaro Cardenas', 'Nanacamilpa de Mariano Arista', 'Acuamanala de Miguel Hidalgo', 'Nativitas', 'Panotla', 'Papalotla de Xicohtencatl', 'San Pablo del Monte', 'Santa Cruz Tlaxcala', 'Tenancingo', 'Teolocholco', 'Tepeyanco', 'Terrenate', 'Tetla de la Solidaridad', 'Tetlatlahuca', 'Tlaxcala de Xicohtencatl', 'Tlaxco', 'Tocatlan', 'Totolac', 'Tzompantepec', 'Xaloztoc', 'Xicohtzinco', 'Yauhquemecan', 'Zacatelco', 'Zitlaltepec de Trinidad Sanchez Santos', 'Lazaro Cardenas', 'La Magdalena Tlaltelulco', 'San Damian Texoloc', 'San Francisco Tetlanohcan', 'San Jeronimo Zacualpan', 'San Jose Teacalco', 'San Juan Huactzinco', 'San Lorenzo Axocomanitla', 'San Lucas Tecopilco', 'Santa Ana Nopalucan', 'Santa Apolonia Teacalco', 'Santa Catarina Ayometla', 'Santa Cruz Quilehtla', 'Santa Isabel Xiloxoxtla', 'Otro');

estados['Tamaulipas'] = new Array('Abasolo', 'Aldama', 'Altamira', 'Antiguo Morelos', 'Burgos', 'Bustamante', 'Camargo', 'Casas', 'Ciudad Madero', 'Cruillas', 'Gomez Farias', 'Gonzalez', 'Güemez', 'Guerrero', 'Gustavo Diaz Ordaz', 'Hidalgo', 'Jaumave', 'Jimenez', 'Llera', 'Mainero', 'El Mante', 'Matamoros', 'Mendez', 'Mier', 'Miguel Aleman', 'Miquihuana', 'Nuevo Laredo', 'Nuevo Morelos', 'Ocampo', 'Padilla', 'Palmillas', 'Reynosa', 'Rio Bravo', 'San Carlos', 'San Fernando', 'San Nicolas', 'Soto la Marina', 'Tampico', 'Tula', 'Valle Hermoso', 'Victoria', 'Villagran', 'Xicotencatl', 'Otro');

estados['Tabasco'] = new Array('Balancan', 'Cardenas', 'Centla', 'Centro', 'Comalcalco', 'Cunduacan', 'Emiliano Zapata', 'Huimanguillo', 'Jalapa', 'Jalpa de Mendez', 'Jonuta', 'Macuspana', 'Nacajuca', 'Paraiso', 'Tacotalpa', 'Teapa', 'Tenosique', 'Otro');

estados['Sonora'] = new Array('Aconchi', 'Agua Prieta', 'alamos', 'Altar', 'Arivechi', 'Arizpe', 'Atil', 'Bacadehuachi', 'Bacanora', 'Bacerac', 'Bacoachi', 'Bacum', 'Banamichi', 'Baviacora', 'Bavispe', 'Benito Juarez', 'Benjamin Hill', 'Caborca', 'Cajeme', 'Cananea', 'Carbo', 'Cucurpe', 'Cumpas', 'Divisaderos', 'Empalme', 'Etchojoa', 'Fronteras', 'General Plutarco Elias Calles', 'Granados', 'Guaymas', 'Hermosillo', 'Huachinera', 'Huasabas', 'Huatabampo', 'Huepac', 'imuris', 'La Colorada', 'Magdalena', 'Mazatan', 'Moctezuma', 'Naco', 'Nacori Chico', 'Nacozari', 'Navojoa', 'Nogales', 'Onavas', 'Opodepe', 'Oquitoa', 'Pitiquito', 'Puerto Penasco', 'Quiriego', 'Rayon', 'Rosario', 'Sahuaripa', 'San Felipe de Jesus', 'San Ignacio Rio Muerto', 'San Javier', 'San Luis Rio Colorado', 'San Miguel de Horcasitas', 'San Pedro de la Cueva', 'Santa Ana', 'Santa Cruz', 'Saric', 'Soyopa', 'Suaqui Grande', 'Tepache', 'Trincheras', 'Tubutama', 'Ures', 'Villa Hidalgo', 'Villa Pesqueira', 'Yecora', 'Otro');

estados['Sinaloa'] = new Array('Ahome', 'Angostura', 'Badiraguato', 'Choix', 'Concordia', 'Cosala', 'Culiacan', 'El Fuerte', 'Elota', 'Escuinapa', 'Guasave', 'Mazatlan', 'Mocorito', 'Navolato', 'Rosario', 'Salvador Alvarado', 'San Ignacio', 'Sinaloa', 'Otro');

estados['San Luis Potosi'] = new Array('Ahualulco', 'Alaquines', 'Aquismon', 'Armadillo de los Infante', 'Axtla de Terrazas', 'Cardenas', 'Catorce', 'Cedral', 'Cerritos', 'Cerro de San Pedro', 'Charcas', 'Ciudad del Maiz', 'Ciudad Fernandez', 'Ciudad Valles', 'Coxcatlan', 'ebano', 'El Naranjo', 'Guadalcazar', 'Huehuetlan', 'Lagunillas', 'Matehuala', 'Matlapa', 'Mexquitic de Carmona', 'Moctezuma', 'Rayon', 'Rioverde', 'Salinas', 'San Antonio', 'San Ciro de Acosta', 'San Luis Potosi', 'San Martin Chalchicuautla', 'San Nicolas de Tolentino', 'San Vicente Tancuayalab', 'Santa Catarina', 'Santa Maria del Rio', 'Santo Domingo', 'Soledad de Graciano Sanchez', 'Tamasopo', 'Tamazunchale', 'Tampacan', 'Tampamolon', 'Tamuin', 'Tancahuitz', 'Tanlajas', 'Tanquian de Escobedo', 'Tierra Nueva', 'Vanegas', 'Venado', 'Villa de Arista', 'Villa de Arriaga', 'Villa de Guadalupe', 'Villa de la Paz', 'Villa de Ramos', 'Villa de Reyes', 'Villa Hidalgo', 'Villa Juarez', 'Xilitla', 'Zaragoza', 'Otro');

estados['Quintana Roo'] = new Array('Benito Juarez', 'Cozumel', 'Felipe Carrillo Puerto', 'Isla Mujeres', 'Jose Maria Morelos', 'Lazaro Cardenas', 'Othon P. Blanco', 'Solidaridad', 'Tulum', 'Otro');

estados['Queretaro'] = new Array('Amealco de Bonfil', 'Arroyo Seco', 'Cadereyta de Montes', 'Colon', 'Corregidora', 'El Marques', 'Ezequiel Montes', 'Huimilpan', 'Jalpan de Serra', 'Landa de Matamoros', 'Pedro Escobedo', 'Penamiller', 'Pinal de Amoles', 'Queretaro', 'San Joaquin', 'San Juan del Rio', 'Tequisquiapan', 'Toliman', 'Otro');

estados['Puebla'] = new Array('Acajete', 'Acateno', 'Acatlan de Osorio', 'Acatzingo', 'Acteopan', 'Ahuacatlan', 'Ahuatlan', 'Ahuazotepec', 'Ahuehuetitla', 'Ajalpan', 'Albino Zertuche', 'Aljojuca', 'Altepexi', 'Amixtlan', 'Amozoc', 'Aquixtla', 'Atempan', 'Atexcal', 'Atlequizayan', 'Atlixco', 'Atoyatempan', 'Atzala', 'Atzitzihuacan', 'Atzitzintla', 'Axutla', 'Ayotoxco de Guerrero', 'Calpan', 'Caltepec', 'Camocuautla', 'Canada Morelos', 'Caxhuacan', 'Chalchicomula de Sesma', 'Chapulco', 'Chiautla', 'Chiautzingo', 'Chichiquila', 'Chiconcuautla', 'Chietla', 'Chigmecatitlan', 'Chignahuapan', 'Chignautla', 'Chila de la Sal', 'Chila de las Flores', 'Chilchotla', 'Chinantla', 'Coatepec', 'Coatzingo', 'Cohetzala', 'Cohuecan', 'Coronango', 'Coxcatlan', 'Coyomeapan', 'Coyotepec', 'Cuapiaxtla de Madero', 'Cuautempan', 'Cuautinchan', 'Cuautlancingo', 'Cuayuca de Andrade', 'Cuetzalan del Progreso', 'Cuyoaco', 'Domingo Arenas', 'Eloxochitlan', 'Epatlan', 'Esperanza', 'Francisco Z. Mena', 'General Felipe angeles', 'Guadalupe', 'Guadalupe Victoria', 'Hermenegildo Galeana', 'Honey', 'Huaquechula', 'Huatlatlauca', 'Huauchinango', 'Huehuetla', 'Huehuetlan el Chico', 'Huehuetlan el Grande', 'Huejotzingo', 'Hueyapan', 'Hueytamalco', 'Hueytlalpan', 'Huitzilan de Serdan', 'Huitziltepec', 'Ixcamilpa de Guerrero', 'Ixcaquixtla', 'Ixtacamaxtitlan', 'Ixtepec', 'Izucar de Matamoros', 'Jalpan', 'Jolalpan', 'Jonotla', 'Jopala', 'Juan C. Bonilla', 'Juan Galindo', 'Juan N. Mendez', 'La Magdalena Tlatlauquitepec', 'Lafragua', 'Libres', 'Los Reyes de Juarez', 'Mazapiltepec de Juarez', 'Mixtla', 'Molcaxac', 'Naupan', 'Nauzontla', 'Nealtican', 'Nicolas Bravo', 'Nopalucan', 'Ocotepec', 'Ocoyucan', 'Olintla', 'Oriental', 'Pahuatlan', 'Palmar de Bravo', 'Pantepec', 'Petlalcingo', 'Piaxtla', 'Puebla de Zaragoza', 'Quecholac', 'Quimixtlan', 'Rafael Lara Grajales', 'San Andres Cholula', 'San Antonio Canada', 'San Diego La Mesa Tochimiltzingo', 'San Felipe Teotlalcingo', 'San Felipe Tepatlan', 'San Gabriel Chilac', 'San Gregorio Atzompa', 'San Jeronimo Tecuanipan', 'San Jeronimo Xayacatlan', 'San Jose Chiapa', 'San Jose Miahuatlan', 'San Juan Atenco', 'San Juan Atzompa', 'San Martin Texmelucan', 'San Martin Totoltepec', 'San Matias Tlalancaleca', 'San Miguel Ixitlan', 'San Miguel Xoxtla', 'San Nicolas Buenos Aires', 'San Nicolas de los Ranchos', 'San Pablo Anicano', 'San Pedro Cholula', 'San Pedro Yeloixtlahuaca', 'San Salvador el Seco', 'San Salvador el Verde', 'San Salvador Huixcolotla', 'San Sebastian Tlacotepec', 'Santa Catarina Tlaltempan', 'Santa Ines Ahuatempan', 'Santa Isabel Cholula', 'Santiago Miahuatlan', 'Santo Tomas Hueyotlipan', 'Soltepec', 'Tecali de Herrera', 'Tecamachalco', 'Tecomatlan', 'Tehuacan', 'Tehuitzingo', 'Tenampulco', 'Teopantlan', 'Teotlalco', 'Tepanco de Lopez', 'Tepango de Rodriguez', 'Tepatlaxco de Hidalgo', 'Tepeaca', 'Tepemaxalco', 'Tepeojuma', 'Tepetzintla', 'Tepexco', 'Tepexi de Rodriguez', 'Tepeyahualco', 'Tepeyahualco de Cuauhtemoc', 'Tetela de Ocampo', 'Teteles de avila Castillo', 'Teziutlan', 'Tianguismanalco', 'Tilapa', 'Tlachichuca', 'Tlacotepec de Benito Juarez', 'Tlacuilotepec', 'Tlahuapan', 'Tlalnepantla', 'Tlaltenango', 'Tlaola', 'Tlapacoya', 'Tlapanala', 'Tlatlauquitepec', 'Tlaxco', 'Tochimilco', 'Tochtepec', 'Totoltepec de Guerrero', 'Tulcingo', 'Tuzamapan de Galeana', 'Tzicatlacoyan', 'Venustiano Carranza', 'Vicente Guerrero', 'Xayacatlan de Bravo', 'Xicotepec', 'Xicotlan', 'Xiutetelco', 'Xochiapulco', 'Xochiltepec', 'Xochitlan de Vicente Suarez', 'Xochitlan Todos Santos', 'Yaonahuac', 'Yehualtepec', 'Zacapala', 'Zacapoaxtla', 'Zacatlan', 'Zapotitlan', 'Zapotitlan de Mendez', 'Zaragoza', 'Zautla', 'Zihuateutla', 'Zinacatepec', 'Zongozotla', 'Zoquiapan', 'Zoquitlan', 'Otro');

estados['Oaxaca'] = new Array('Abejones', 'Acatlan de Perez Figueroa', 'animas Trujano', 'Asuncion Cacalotepec', 'Asuncion Cuyotepeji', 'Asuncion Ixtaltepec', 'Asuncion Nochixtlan', 'Asuncion Ocotlan', 'Asuncion Tlacolulita', 'Ayoquezco de Aldama', 'Ayotzintepec', 'Calihuala', 'Candelaria Loxicha', 'Capulalpam de Mendez', 'Chahuites', 'Chalcatongo de Hidalgo', 'Chiquihuitlan de Benito Juarez', 'Cienega de Zimatlan', 'Ciudad Ixtepec', 'Coatecas Altas', 'Coicoyan de las Flores', 'Concepcion Buenavista', 'Concepcion Papalo', 'Constancia del Rosario', 'Cosolapa', 'Cosoltepec', 'Cuilapam de Guerrero', 'Cuyamecalco Villa de Zaragoza', 'Ejutla de Crespo', 'El Barrio de la Soledad', 'El Espinal', 'Eloxochitlan de Flores Magon', 'Fresnillo de Trujano', 'Guadalupe de Ramirez', 'Guadalupe Etla', 'Guelatao de Juarez', 'Guevea de Humboldt', 'Heroica Ciudad de Tlaxiaco', 'Huajuapan de Leon', 'Huautepec', 'Huautla de Jimenez', 'Ixpantepec Nieves', 'Ixtlan de Juarez', 'Juchitan de Zaragoza', 'La Compania', 'La Pe', 'La Reforma', 'La Trinidad Vista Hermosa', 'Loma Bonita', 'Magdalena Apasco', 'Magdalena Jaltepec', 'Magdalena Mixtepec', 'Magdalena Ocotlan', 'Magdalena Penasco', 'Magdalena Teitipac', 'Magdalena Tequisistlan', 'Magdalena Tlacotepec', 'Magdalena Yodocono de Porfirio Diaz', 'Magdalena Zahuatlan', 'Mariscala de Juarez', 'Martires de Tacubaya', 'Matias Romero', 'Mazatlan Villa de Flores', 'Mesones Hidalgo', 'Miahuatlan de Porfirio Diaz', 'Mixistlan de la Reforma', 'Monjas', 'Natividad', 'Nazareno Etla', 'Nejapa de Madero', 'Nuevo Zoquiapam', 'Oaxaca de Juarez', 'Ocotlan de Morelos', 'Pinotepa de Don Luis', 'Pluma Hidalgo', 'Putla Villa de Guerrero', 'Reforma de Pineda', 'Reyes Etla', 'Rojas de Cuauhtemoc', 'Salina Cruz', 'San Agustin Amatengo', 'San Agustin Atenango', 'San Agustin Chayuco', 'San Agustin de las Juntas', 'San Agustin Etla', 'San Agustin Loxicha', 'San Agustin Tlacotepec', 'San Agustin Yatareni', 'San Andres Cabecera Nueva', 'San Andres Dinicuiti', 'San Andres Huaxpaltepec', 'San Andres Huayapam', 'San Andres Ixtlahuaca', 'San Andres Lagunas', 'San Andres Nuxino', 'San Andres Paxtlan', 'San Andres Sinaxtla', 'San Andres Solaga', 'San Andres Teotilalpam', 'San Andres Tepetlapa', 'San Andres Yaa', 'San Andres Zabache', 'San Andres Zautla', 'San Antonino Castillo Velasco', 'San Antonino el Alto', 'San Antonino Monte Verde', 'San Antonio Acutla', 'San Antonio de la Cal', 'San Antonio Huitepec', 'San Antonio Nanahuatipam', 'San Antonio Sinicahua', 'San Antonio Tepetlapa', 'San Baltazar Chichicapam', 'San Baltazar Loxicha', 'San Baltazar Yatzachi el Bajo', 'San Bartolo Coyotepec', 'San Bartolo Soyaltepec', 'San Bartolo Yautepec', 'San Bartolome Ayautla', 'San Bartolome Loxicha', 'San Bartolome Quialana', 'San Bartolome Yucuane', 'San Bartolome Zoogocho', 'San Bernardo Mixtepec', 'San Blas Atempa', 'San Carlos Yautepec', 'San Cristobal Amatlan', 'San Cristobal Amoltepec', 'San Cristobal Lachirioag', 'San Cristobal Suchixtlahuaca', 'San Dionisio del Mar', 'San Dionisio Ocotepec', 'San Dionisio Ocotlan', 'San Esteban Atatlahuca', 'San Felipe Jalapa de Diaz', 'San Felipe Tejalapam', 'San Felipe Usila', 'San Francisco Cahuacua', 'San Francisco Cajonos', 'San Francisco Chapulapa', 'San Francisco Chindua', 'San Francisco del Mar', 'San Francisco Huehuetlan', 'San Francisco Ixhuatan', 'San Francisco Jaltepetongo', 'San Francisco Lachigolo', 'San Francisco Logueche', 'San Francisco Nuxano', 'San Francisco Ozolotepec', 'San Francisco Sola', 'San Francisco Telixtlahuaca', 'San Francisco Teopan', 'San Francisco Tlapancingo', 'San Gabriel Mixtepec', 'San Ildefonso Amatlan', 'San Ildefonso Sola', 'San Ildefonso Villa Alta', 'San Jacinto Amilpas', 'San Jacinto Tlacotepec', 'San Jeronimo Coatlan', 'San Jeronimo Silacayoapilla', 'San Jeronimo Sosola', 'San Jeronimo Taviche', 'San Jeronimo Tecoatl', 'San Jeronimo Tlacochahuaya', 'San Jorge Nuchita', 'San Jose Ayuquila', 'San Jose Chiltepec', 'San Jose del Penasco', 'San Jose del Progreso', 'San Jose Estancia Grande', 'San Jose Independencia', 'San Jose Lachiguiri', 'San Jose Tenango', 'San Juan Achiutla', 'San Juan Atepec', 'San Juan Bautista Atatlahuca', 'San Juan Bautista Coixtlahuaca', 'San Juan Bautista Cuicatlan', 'San Juan Bautista Guelache', 'San Juan Bautista Jayacatlan', 'San Juan Bautista lo de Soto', 'San Juan Bautista Suchitepec', 'San Juan Bautista Tlachichilco', 'San Juan Bautista Tlacoatzintepec', 'San Juan Bautista Tuxtepec', 'San Juan Bautista Valle Nacional', 'San Juan Cacahuatepec', 'San Juan Chicomezuchil', 'San Juan Chilateca', 'San Juan Cieneguilla', 'San Juan Coatzospam', 'San Juan Colorado', 'San Juan Comaltepec', 'San Juan Cotzocon', 'San Juan de los Cues', 'San Juan del Estado', 'San Juan del Rio', 'San Juan Diuxi', 'San Juan Evangelista Analco', 'San Juan Guelavia', 'San Juan Guichicovi', 'San Juan Ihualtepec', 'San Juan Juquila Mixes', 'San Juan Juquila Vijanos', 'San Juan Lachao', 'San Juan Lachigalla', 'San Juan Lajarcia', 'San Juan Lalana', 'San Juan Mazatlan', 'San Juan Mixtepec Distrito 08', 'San Juan Mixtepec Distrito 26', 'San Juan Ozolotepec', 'San Juan Petlapa', 'San Juan Quiahije', 'San Juan Quiotepec', 'San Juan Sayultepec', 'San Juan Tabaa', 'San Juan Tamazola', 'San Juan Teita', 'San Juan Teitipac', 'San Juan Tepeuxila', 'San Juan Teposcolula', 'San Juan Yaee', 'San Juan Yatzona', 'San Juan Yucuita', 'San Juan Yumi', 'San Lorenzo', 'San Lorenzo Albarradas', 'San Lorenzo Cacaotepec', 'San Lorenzo Cuaunecuiltitla', 'San Lorenzo Texmelucan', 'San Lorenzo Victoria', 'San Lucas Camotlan', 'San Lucas Ojitlan', 'San Lucas Quiavini', 'San Lucas Zoquiapam', 'San Luis Amatlan', 'San Marcial Ozolotepec', 'San Marcos Arteaga', 'San Martin de los Cansecos', 'San Martin Huamelulpam', 'San Martin Itunyoso', 'San Martin Lachila', 'San Martin Peras', 'San Martin Tilcajete', 'San Martin Toxpalan', 'San Martin Zacatepec', 'San Mateo Cajonos', 'San Mateo del Mar', 'San Mateo Etlatongo', 'San Mateo Nejapam', 'San Mateo Penasco', 'San Mateo Pinas', 'San Mateo Rio Hondo', 'San Mateo Sindihui', 'San Mateo Tlapiltepec', 'San Mateo Yoloxochitlan', 'San Melchor Betaza', 'San Miguel Achiutla', 'San Miguel Ahuehuetitlan', 'San Miguel Aloapam', 'San Miguel Amatitlan', 'San Miguel Amatlan', 'San Miguel Chicahua', 'San Miguel Chimalapa', 'San Miguel Coatlan', 'San Miguel del Puerto', 'San Miguel del Rio', 'San Miguel Ejutla', 'San Miguel el Grande', 'San Miguel Huautla', 'San Miguel Mixtepec', 'San Miguel Panixtlahuaca', 'San Miguel Peras', 'San Miguel Piedras', 'San Miguel Quetzaltepec', 'San Miguel Santa Flor', 'San Miguel Soyaltepec', 'San Miguel Suchixtepec', 'San Miguel Tecomatlan', 'San Miguel Tenango', 'San Miguel Tequixtepec', 'San Miguel Tilquiapam', 'San Miguel Tlacamama', 'San Miguel Tlacotepec', 'San Miguel Tulancingo', 'San Miguel Yotao', 'San Nicolas', 'San Nicolas Hidalgo', 'San Pablo Coatlan', 'San Pablo Cuatro Venados', 'San Pablo Etla', 'San Pablo Huitzo', 'San Pablo Huixtepec', 'San Pablo Macuiltianguis', 'San Pablo Tijaltepec', 'San Pablo Villa de Mitla', 'San Pablo Yaganiza', 'San Pedro Amuzgos', 'San Pedro Amuzgos', 'San Pedro Atoyac', 'San Pedro Cajonos', 'San Pedro Comitancillo', 'San Pedro Coxcaltepec Cantaros', 'San Pedro el Alto', 'San Pedro Huamelula', 'San Pedro Huilotepec', 'San Pedro Ixcatlan', 'San Pedro Ixtlahuaca', 'San Pedro Jaltepetongo', 'San Pedro Jicayan', 'San Pedro Jocotipac', 'San Pedro Juchatengo', 'San Pedro Martir', 'San Pedro Martir Quiechapa', 'San Pedro Martir Yucuxaco', 'San Pedro Mixtepec - Distrito 22 -', 'San Pedro Mixtepec - Distrito 26 -', 'San Pedro Molinos', 'San Pedro Nopala', 'San Pedro Ocopetatillo', 'San Pedro Ocotepec', 'San Pedro Pochutla', 'San Pedro Quiatoni', 'San Pedro Sochiapam', 'San Pedro Tapanatepec', 'San Pedro Taviche', 'San Pedro Teozacoalco', 'San Pedro Teutila', 'San Pedro Tidaa', 'San Pedro Topiltepec', 'San Pedro Totolapa', 'San Pedro y San Pablo Ayutla', 'San Pedro y San Pablo Teposcolula', 'San Pedro y San Pablo Tequixtepec', 'San Pedro Yaneri', 'San Pedro Yolox', 'San Pedro Yucunama', 'San Raymundo Jalpan', 'San Sebastian Abasolo', 'San Sebastian Coatlan', 'San Sebastian Ixcapa', 'San Sebastian Nicananduta', 'San Sebastian Rio Hondo', 'San Sebastian Tecomaxtlahuaca', 'San Sebastian Teitipac', 'San Sebastian Tutla', 'San Simon Almolongas', 'San Simon Zahuatlan', 'San Vicente Coatlan', 'San Vicente Lachixio', 'San Vicente Nunu', 'Santa Ana', 'Santa Ana Ateixtlahuaca', 'Santa Ana Cuauhtemoc', 'Santa Ana del Valle', 'Santa Ana Tavela', 'Santa Ana Tlapacoyan', 'Santa Ana Yareni', 'Santa Ana Zegache', 'Santa Catalina Quieri', 'Santa Catarina Cuixtla', 'Santa Catarina Ixtepeji', 'Santa Catarina Juquila', 'Santa Catarina Lachatao', 'Santa Catarina Loxicha', 'Santa Catarina Mechoacan', 'Santa Catarina Minas', 'Santa Catarina Quiane', 'Santa Catarina Quioquitani', 'Santa Catarina Tayata', 'Santa Catarina Ticua', 'Santa Catarina Yosonotu', 'Santa Catarina Zapoquila', 'Santa Cruz Acatepec', 'Santa Cruz Amilpas', 'Santa Cruz de Bravo', 'Santa Cruz Itundujia', 'Santa Cruz Mixtepec', 'Santa Cruz Nundaco', 'Santa Cruz Papalutla', 'Santa Cruz Tacache de Mina', 'Santa Cruz Tacahua', 'Santa Cruz Tayata', 'Santa Cruz Xitla', 'Santa Cruz Xoxocotlan', 'Santa Cruz Zenzontepec', 'Santa Gertrudis', 'Santa Ines de Zaragoza', 'Santa Ines del Monte', 'Santa Ines Yatzeche', 'Santa Lucia del Camino', 'Santa Lucia Miahuatlan', 'Santa Lucia Monteverde', 'Santa Lucia Ocotlan', 'Santa Magdalena Jicotlan', 'Santa Maria Alotepec', 'Santa Maria Apazco', 'Santa Maria Atzompa', 'Santa Maria Camotlan', 'Santa Maria Chachoapam', 'Santa Maria Chilchotla', 'Santa Maria Chimalapa', 'Santa Maria Colotepec', 'Santa Maria Cortijo', 'Santa Maria Coyotepec', 'Santa Maria del Rosario', 'Santa Maria del Tule', 'Santa Maria Ecatepec', 'Santa Maria Guelace', 'Santa Maria Guienagati', 'Santa Maria Huatulco', 'Santa Maria Huazolotitlan', 'Santa Maria Ipalapa', 'Santa Maria Ixcatlan', 'Santa Maria Jacatepec', 'Santa Maria Jalapa del Marques', 'Santa Maria Jaltianguis', 'Santa Maria la Asuncion', 'Santa Maria Lachixio', 'Santa Maria Mixtequilla', 'Santa Maria Nativitas', 'Santa Maria Nduayaco', 'Santa Maria Ozolotepec', 'Santa Maria Papalo', 'Santa Maria Penoles', 'Santa Maria Petapa', 'Santa Maria Quiegolani', 'Santa Maria Sola', 'Santa Maria Tataltepec', 'Santa Maria Tecomavaca', 'Santa Maria Temaxcalapa', 'Santa Maria Temaxcaltepec', 'Santa Maria Teopoxco', 'Santa Maria Tepantlali', 'Santa Maria Texcatitlan', 'Santa Maria Tlahuitoltepec', 'Santa Maria Tlalixtac', 'Santa Maria Tonameca', 'Santa Maria Totolapilla', 'Santa Maria Xadani', 'Santa Maria Yalina', 'Santa Maria Yavesia', 'Santa Maria Yolotepec', 'Santa Maria Yosoyua', 'Santa Maria Yucuhiti', 'Santa Maria Zacatepec', 'Santa Maria Zaniza', 'Santa Maria Zoquitlan', 'Santiago Amoltepec', 'Santiago Apoala', 'Santiago Apostol', 'Santiago Astata', 'Santiago Atitlan', 'Santiago Ayuquililla', 'Santiago Cacaloxtepec', 'Santiago Camotlan', 'Santiago Chazumba', 'Santiago Choapam', 'Santiago Comaltepec', 'Santiago del Rio', 'Santiago Huajolotitlan', 'Santiago Huauclilla', 'Santiago Ihuitlan Plumas', 'Santiago Ixcuintepec', 'Santiago Ixtayutla', 'Santiago Jamiltepec', 'Santiago Jocotepec', 'Santiago Juxtlahuaca', 'Santiago Lachiguiri', 'Santiago Lalopa', 'Santiago Laollaga', 'Santiago Laxopa', 'Santiago Llano Grande', 'Santiago Matatlan', 'Santiago Miltepec', 'Santiago Minas', 'Santiago Nacaltepec', 'Santiago Nejapilla', 'Santiago Niltepec', 'Santiago Nundiche', 'Santiago Nuyoo', 'Santiago Pinotepa Nacional', 'Santiago Suchilquitongo', 'Santiago Tamazola', 'Santiago Tapextla', 'Santiago Tenango', 'Santiago Tepetlapa', 'Santiago Tetepec', 'Santiago Texcalcingo', 'Santiago Textitlan', 'Santiago Tilantongo', 'Santiago Tillo', 'Santiago Tlazoyaltepec', 'Santiago Xanica', 'Santiago Xiacui', 'Santiago Yaitepec', 'Santiago Yaveo', 'Santiago Yolomecatl', 'Santiago Yosondua', 'Santiago Yucuyachi', 'Santiago Zacatepec', 'Santiago Zoochila', 'Santo Domingo Albarradas', 'Santo Domingo Armenta', 'Santo Domingo Chihuitan', 'Santo Domingo de Morelos', 'Santo Domingo Ingenio', 'Santo Domingo Ixcatlan', 'Santo Domingo Nuxaa', 'Santo Domingo Ozolotepec', 'Santo Domingo Petapa', 'Santo Domingo Roayaga', 'Santo Domingo Tehuantepec', 'Santo Domingo Teojomulco', 'Santo Domingo Tepuxtepec', 'Santo Domingo Tlatayapam', 'Santo Domingo Tomaltepec', 'Santo Domingo Tonala', 'Santo Domingo Tonaltepec', 'Santo Domingo Xagacia', 'Santo Domingo Yanhuitlan', 'Santo Domingo Yodohino', 'Santo Domingo Zanatepec', 'Santo Tomas Jalieza', 'Santo Tomas Mazaltepec', 'Santo Tomas Ocotepec', 'Santo Tomas Tamazulapan', 'Santos Reyes Nopala', 'Santos Reyes Papalo', 'Santos Reyes Tepejillo', 'Santos Reyes Yucuna', 'Silacayoapam', 'Sitio de Xitlapehua', 'Soledad Etla', 'Tamazulapam del Espiritu Santo', 'Tanetze de Zaragoza', 'Taniche', 'Tataltepec de Valdes', 'Teococuilco de Marcos Perez', 'Teotitlan de Flores Magon', 'Teotitlan del Valle', 'Teotongo', 'Tepelmeme Villa de Morelos', 'Tezoatlan de Segura y Luna', 'Tlacolula de Matamoros', 'Tlacotepec Plumas', 'Tlalixtac de Cabrera', 'Totontepec Villa de Morelos', 'Trinidad Zaachila', 'Union Hidalgo', 'Valerio Trujano', 'Villa de Chilapa de Diaz', 'Villa de Etla', 'Villa de Tamazulapam del Progreso', 'Villa de Tututepec de Melchor Ocampo', 'Villa de Zaachila', 'Villa Diaz Ordaz', 'Villa Hidalgo', 'Villa Sola de Vega', 'Villa Talea de Castro', 'Villa Tejupam de la Union', 'Yaxe', 'Yogana', 'Yutanduchi de Guerrero', 'Zapotitlan del Rio', 'Zapotitlan Lagunas', 'Zapotitlan Palmas', 'Zimatlan de Alvarez', 'Otro');

estados['Nuevo Leon'] = new Array('Abasolo', 'Agualeguas', 'Allende', 'Anahuac', 'Apodaca', 'Aramberri', 'Bustamante', 'Cadereyta Jimenez', 'Carmen', 'Cerralvo', 'China', 'Cienega de Flores', 'Doctor Arroyo', 'Doctor Coss', 'Doctor Gonzalez', 'Galeana', 'Garcia', 'General Bravo', 'General Escobedo', 'General Teran', 'General Trevino', 'General Zaragoza', 'General Zuazua', 'Guadalupe', 'Hidalgo', 'Higueras', 'Hualahuises', 'Iturbide', 'Juarez', 'Lampazos de Naranjo', 'Linares', 'Los Aldamas', 'Los Herreras', 'Los Ramones', 'Marin', 'Melchor Ocampo', 'Mier y Noriega', 'Mina', 'Montemorelos', 'Monterrey', 'Paras', 'Pesqueria', 'Rayones', 'Sabinas Hidalgo', 'Salinas Victoria', 'San Nicolas de los Garza', 'San Pedro Garza Garcia', 'Santa Catarina', 'Santiago', 'Vallecillo', 'Villaldama', 'Otro');



estados['Nayarit'] = new Array('Acaponeta', 'Ahuacatlan', 'Amatlan de Canas', 'Bahia de Banderas', 'Compostela', 'Del Nayar', 'Huajicori', 'Ixtlan del Rio', 'Jala', 'La Yesca', 'Rosamorada', 'Ruiz', 'San Blas', 'San Pedro Lagunillas', 'Santa Maria del Oro', 'Santiago Ixcuintla', 'Tecuala', 'Tepic', 'Tuxpan', 'Xalisco', 'Otro');

estados['Morelos'] = new Array('Amacuzac', 'Atlatlahucan', 'Axoyiapan', 'Ayala', 'Coatlan del Rio', 'Cuatla', 'Cuernavaca', 'Emiliano Zapata', 'Huitzilac', 'Jantetelco', 'Jiutepec', 'Jojutla', 'Jonacatepec', 'Mazatepec', 'Miacatlan', 'Ocuituco', 'Puente de Ixtla', 'Temixco', 'Temoac', 'Tepalcingo', 'Tepoztlan', 'Tetecala', 'Tetela del Volcan', 'Tlalnepantla', 'Tlaltizapan', 'Tlaquiltenango', 'Tlayacapan', 'Totolapan', 'Xochitepec', 'Yautepec', 'Yecapixtla', 'Zacatepec de Hidalgo', 'Zacualpan de Amilpas', 'Otro');

estados['Michoacan'] = new Array('Acuitzio', 'Aguililla', 'alvaro Obregon', 'Angamacutiro', 'Angangueo', 'Apatzingan', 'Aporo', 'Aquila', 'Ario', 'Arteaga', 'Brisenas', 'Buenavista', 'Caracuaro', 'Chaparan', 'Charo', 'Chavinda', 'Cheran', 'Chilchota', 'Chinicuila', 'Chirintzio', 'Chucandiro', 'Churumuco', 'Coahuayana', 'Coalcoman de Vazquez Pallares', 'Coeneo', 'Cojumatlan de Regules', 'Contepec', 'Copandaro', 'Cotija', 'Cuitzeo', 'Ecuandureo', 'Epitacio Huerta', 'Erongaricuaro', 'Gabriel Zamora', 'Hidalgo', 'Huandacareo', 'Huaniqueo', 'Huetamo', 'Huiramba', 'Indaparapeo', 'Irimbo', 'Ixtlan', 'Jacona', 'Jimenez', 'Jiquilpan', 'Jose Sixto Verduzco', 'Juarez', 'Jungapeo', 'La Huacana', 'La Piedad', 'Lagunillas', 'Lazaro Cardenas', 'Los Reyes', 'Madero', 'Maravatio', 'Marcos Castellanos', 'Morelia', 'Morelos', 'Mugica', 'Nahuatzen', 'Nocupetaro', 'Nuevo Parangaricutiro', 'Nuevo Urecho', 'Numaran', 'Ocampo', 'Pajacuaran', 'Panindicuaro', 'Paracho', 'Paracuaro', 'Patzcuaro', 'Penjamillo', 'Periban', 'Purepero', 'Puruandiro', 'Querendaro', 'Quiroga', 'Salvador Escalante', 'San Lucas', 'Santa Ana Maya', 'Senguio', 'Suahuayo', 'Susupuato', 'Tacambaro', 'Tancitaro', 'Tangamandapio', 'Tangancicuaro', 'Tanhuato', 'Taretan', 'Tarimbaro', 'Tepalcatepec', 'Tingambato', 'Tingüindin', 'Tiquicheo de Nicolas Romero', 'Tlalpujahua', 'Tlazazalca', 'Tocumbo', 'Tumbiscatio', 'Turicato', 'Tuxpan', 'Tuzantla', 'Tzintzuntzan', 'Tzitzio', 'Uruapan', 'Venustiano Carranza', 'Villamar', 'Vista Hermosa', 'Yurecuaro', 'Zacapu', 'Zamora', 'Zinaparo', 'Zinapecuaro', 'Ziracuaretiro', 'Zitacuaro', 'Otro');

estados['Estado de Mexico'] = new Array('Acambay', 'Acolman', 'Aculco', 'Almoloya de Alquisiras', 'Almoloya de Juarez', 'Almoloya del Rio', 'Amanalco', 'Amatepec', 'Amemeca', 'Apaxco', 'Atenco', 'Atizapan', 'Atizapan de Zaragoza', 'Atlacomulco', 'Atlautla', 'Axapusco', 'Ayapango', 'Calimaya', 'Capulhuac', 'Chalco', 'Chapa de Mota', 'Chapultepec', 'Chiautla', 'Chicoloapan', 'Chiconcuac', 'Chimalhuacan', 'Coacalco de Berriozabal', 'Coatepec Harinas', 'Cocotitlan', 'Coyotepec', 'Cuautitlan', 'Cuautitlan Izcalli', 'Donato Guerra', 'Ecatepec de Morelos', 'Ecatzingo', 'El Oro', 'Huehuetoca', 'Hueypoxtla', 'Huixquilucan', 'Isidro Fabela', 'Ixtapaluca', 'Ixtapan de la Sal', 'Ixtapan del Oro', 'Ixtlahuaca', 'Jaltenco', 'Jilotepec', 'Jilotzingo', 'Jiquipilco', 'Jocotitlan', 'Joquicingo', 'Juchitepec', 'La Paz', 'Lerma', 'Luvianos', 'Malinalco', 'Melchor Ocampo', 'Metepec', 'Mexicaltzingo', 'Morelos', 'Naucalpan de Juarez', 'Nextlalpan', 'Nezahualcoyotl', 'Nicolas Romero', 'Nopaltepec', 'Ocoyoacac', 'Ocuilan', 'Otumba', 'Otzoloapan', 'Otzolotepec', 'Ozumba', 'Papalotla', 'Polotitlan', 'Rayon', 'San Antonio la Isla', 'San Felipe del Progreso', 'San Jose del Rincon', 'San Martin de las Piramides', 'San Mateo Atenco', 'San Simon de Guerrero', 'Santo Tomas', 'Soyaniquilpan de Juarez', 'Sultepec', 'Tecamac', 'Tejupilco', 'Temamatla', 'Temascalapa', 'Temascalcingo', 'Temascaltepec', 'Temoaya', 'Tenancingo', 'Tenango del Aire', 'Tenango del Valle', 'Teoloyucan', 'Teotihuacan', 'Tepetlaoxtoc', 'Tepetlixpa', 'Tepotzotlan', 'Tequixquiac', 'Texcaltitlan', 'Texcalyacac', 'Texcoco', 'Tezoyuca', 'Tianguistenco', 'Timilpan', 'Tlalmanalco', 'Tlalnepantla de Baz', 'Tlatlaya', 'Toluca', 'Tonanitla', 'Tonatico', 'Tultepec', 'Tultitlan', 'Valle de Bravo', 'Valle de Chalco Solidaridad', 'Villa de Allende', 'Villa del Carbon', 'Villa Guerrero', 'Villa Victoria', 'Xalatlaco', 'Xonacatlan', 'Zacazonapan', 'Zacualpan', 'Zinacantepec', 'Zumpahuacan', 'Zumpango', 'Otro');

estados['Jalisco'] = new Array('Guadalajara', 'Tlajomulco de Zuniga', 'Tlaquepaque', 'Tonala', 'Zapopan', 'Acatic', 'Acatlan de Juarez', 'Ahualulco de Mercado', 'Amacueca', 'Amatitan', 'Ameca', 'Arandas', 'Atemajac de Brizuela', 'Atengo', 'Atenguillo', 'Atotonilco el Alto', 'Atoyac', 'Autlan de Navarro', 'Ayotlan', 'Ayutla', 'Bolanos', 'Cabo Corrientes', 'Canadas de Obregon', 'Capilla de Guadalupe', 'Casimiro Castillo', 'Chapala', 'Chimaltitan', 'Chiquilistlan', 'Cihuatlan', 'Cocula', 'Colotlan', 'Concepcion de Buenos Aires', 'Cuautitlan de Garcia Barragan', 'Cuautla', 'Cuquio', 'Degollado', 'Ejutla', 'El Arenal', 'El Grullo', 'El Limon', 'El Salto', 'Encarnacion de Diaz', 'Etzatlan', 'Gomez Farias', 'Guachinango', 'Hostotipaquillo', 'Huejucar', 'Huejuquilla el Alto', 'Ixtlahuacan de los Membrillos', 'Ixtlahuacan del Rio', 'Jalostotitlan', 'Jamay', 'Jesus Maria', 'Jilotlan de los Dolores', 'Jocotepec', 'Juanacatlan', 'Juchitlan', 'La Barca', 'La Huerta', 'La Manzanilla de la Paz', 'Lagos de Moreno', 'Magdalena', 'Mascota', 'Mazamitla', 'Mexticacan', 'Mezquitic', 'Mixtlan', 'Ocotlan', 'Ojuelos de Jalisco', 'Pihuamo', 'Poncitlan', 'Puerto Vallarta', 'Quitupan', 'San Cristobal de la Barranca', 'San Diego de Alejandria', 'San Gabriel', 'San Ignacio Cerro Gordo', 'San Juan de los Lagos', 'San Juanito de Escobedo', 'San Julian', 'San Marcos', 'San Martin de Bolanos', 'San Martin de Hidalgo', 'San Miguel el Alto', 'San Sebastian del Oeste', 'Santa Maria de los angeles', 'Santa Maria del Oro', 'Sayula', 'Tala', 'Talpa de Allende', 'Tamazula de Gordiano', 'Tapalpa', 'Tecalitlan', 'Techaluta de Montenegro', 'Tecolotlan', 'Tenamaxtlan', 'Teocaltiche', 'Teocuitatlan de Corona', 'Tepatitlan de Morelos', 'Tequila', 'Teuchitlan', 'Tizapan el Alto', 'Toliman', 'Tomatlan', 'Tonaya', 'Tonila', 'Totatiche', 'Tototlan', 'Tuxcacuesco', 'Tuxcueca', 'Tuxpan', 'Union de San Antonio', 'Union de Tula', 'Valle de Guadalupe', 'Valle de Juarez', 'Villa Corona', 'Villa Guerrero', 'Villa Hidalgo', 'Villa Purificacion', 'Yahualica de Gonzalez Gallo', 'Zacoalco de Torres', 'Zapotiltic', 'Zapotitlan de Vadillo', 'Zapotlan del Rey', 'Zapotlan El Grande', 'Zapotlanejo', 'Otro');

estados['Hidalgo'] = new Array('Acatlan', 'Acaxochitlan', 'Actopan', 'Agua Blanca de Iturbide', 'Ajacuba', 'Alfajayucan', 'Almoloya', 'Apan', 'Atitalaquia', 'Atlapexco', 'Atotonilco de Tula', 'Atotonilco el Grande', 'Calnali', 'Cardonal', 'Chapantongo', 'Chapulhuacan', 'Chilcuautla', 'Cuautepec de Hinojosa', 'El Arenal', 'Eloxochitlan', 'Emiliano Zapata', 'Epazoyucan', 'Francisco I. Madero', 'Huasca de Ocampo', 'Huautla', 'Huazalingo', 'Huehuetla', 'Huejutla de Reyes', 'Huichapan', 'Ixmiquilpan', 'Jacala de Ledezma', 'Jaltocan', 'Juarez Hidalgo', 'La Mision', 'Lolotla', 'Metepec', 'Metztitlan', 'Mineral de la Reforma', 'Mineral del Chico', 'Mineral del Monte', 'Mixquiahuala de Juarez', 'Molango de Escamilla', 'Nicolas Flores', 'Nopala de Villagran', 'Omitlan de Juarez', 'Pachuca de Soto', 'Pacula', 'Pisaflores', 'Progreso de Obregon', 'San Agustin Metzquititlan', 'San Agustin Tlaxiaca', 'San Bartolo Tutotepec', 'San Felipe Orizatlan', 'San Salvador', 'Santiago de Anaya', 'Santiago Tulantepec de Lugo Guerrero', 'Singuilucan', 'Tasquillo', 'Tecozautla', 'Tenango de Doria', 'Tepeapulco', 'Tepehuacan de Guerrero', 'Tepeji del Rio de Ocampo', 'Tepetitlan', 'Tetepango', 'Tezontepec de Aldama', 'Tianguistengo', 'Tizayuca', 'Tlahuelilpan', 'Tlahuiltepa', 'Tlanalapa', 'Tlanchinol', 'Tlaxcoapan', 'Tolcayuca', 'Tula de Allende', 'Tulancingo de Bravo', 'Villa de Tezontepec', 'Xochiatipan', 'Xochicoatlan', 'Yahualica', 'Zacualtipan de Angeles', 'Zapotlan de Juarez', 'Zempoala', 'Zimapan', 'Otro');

estados['Guerrero'] = new Array('Acapulco de Juarez', 'Acatepec', 'Ahuacuotzingo', 'Ajuchitlan del Progreso', 'Alcozauca de Guerrero', 'Alpoyeca', 'Apaxtla', 'Arcelia', 'Atenango del Rio', 'Atlamajalcingo del Monte', 'Atlixtac', 'Atoyac de alvarez', 'Ayutla de los Libres', 'Azoyu', 'Benito Juarez', 'Buenavista de Cuellar', 'Chilapa de alvarez', 'Chilpancingo de los Bravo', 'Coahuayutla de Jose Maria Izazaga', 'Cochoapa el Grande', 'Cocula', 'Copala', 'Copalillo', 'Copanatoyac', 'Coyuca de Benitez', 'Coyuca de Catalan', 'Cuajinicuilapa', 'Cualac', 'Cuautepec', 'Cuetzala del Progreso', 'Cutzamala de Pinzon', 'Eduardo Neri', 'Florencio Villarreal', 'General Canuto A. Neri', 'General Heliodoro Castillo', 'Huamuxtitlan', 'Huitzuco de los Figueroa', 'Iguala de la Independencia', 'Igualapa', 'Iliatenco', 'Ixcateopan de Cuauhtemoc', 'Jose Azueta', 'Jose Joaquin Herrera', 'Juan R. Escudero', 'Juchitan', 'La Union de Isidoro Montes de Oca', 'Leonardo Bravo', 'Malinaltepec', 'Marquelia', 'Martir de Cuilapan', 'Metlatonoc', 'Mochitlan', 'Olinala', 'Ometepec', 'Pedro Ascencio Alquisiras', 'Petatlan', 'Pilcaya', 'Pungarabato', 'Quechultenango', 'San Luis Acatlan', 'San Marcos', 'San Miguel Totolapan', 'Taxco de Alarcon', 'Tecoanapa', 'Tecpan de Galeana', 'Teloloapan', 'Tepecoacuilco de Trujano', 'Tetipac', 'Tixtla de Guerrero', 'Tlacoachistlahuaca', 'Tlacoapa', 'Tlalchapa', 'Tlalixtaquilla de Maldonado', 'Tlapa de Comonfort', 'Tlapehuala', 'Xalpatlahuac', 'Xochihuehuetlan', 'Xochistlahuaca', 'Zapotitlan Tablas', 'Zirandaro', 'Zitlala', 'Otro');

estados['Guanajuato'] = new Array('Abasolo', 'Acambaro', 'Allende', 'Apaseo el Alto', 'Apaseo el Grande', 'Atarjea', 'Celaya', 'Comonfort', 'Coroneo', 'Cortazar', 'Cueramaro', 'Doctor Mora', 'Dolores Hidalgo', 'Guanajuato', 'Huanimaro', 'Irapuato', 'Jaral del Progreso', 'Jerecuaro', 'Leon', 'Manuel Doblado', 'Moroleon', 'Ocampo', 'Penjamo', 'Pueblo Nuevo', 'Purisima del Rincon', 'Romita', 'Salamanca', 'Salvatierra', 'San Diego de la Union', 'San Felipe', 'San Francisco del Rincon', 'San Jose Iturbide', 'San Luis de la Paz', 'Santa Catarina', 'Santa Cruz de Juventino Rosas', 'Santiago Maravatio', 'Silao', 'Tarandacuao', 'Tarimoro', 'Tierra Blanca', 'Uriangato', 'Valle de Santiago', 'Victoria', 'Villagran', 'Xichu', 'Yuriria', 'Otro');

estados['Durango'] = new Array('Canatlan', 'Canelas', 'Coneto de Comonfort', 'Cuencame', 'Durango', 'El Oro', 'General Simon Bolivar', 'Gomez Palacio', 'Guadalupe Victoria', 'Guanacevi', 'Hidalgo', 'Inde', 'Lerdo', 'Mapimi', 'Mezquital', 'Nazas', 'Nombre de Dios', 'Nuevo Ideal', 'Ocampo', 'Otaez', 'Panuco de Coronado', 'Penon Blanco', 'Poanas', 'Pueblo Nuevo', 'Rodeo', 'San Bernardo', 'San Dimas', 'San Juan de Guadalupe', 'San Juan del Rio', 'San Luis del Cordero', 'San Pedro del Gallo', 'Santa Clara', 'Santiago Papasquiaro', 'Suchil', 'Tamazula', 'Tepehuanes', 'Tlahualilo', 'Topia', 'Vicente Guerrero', 'Otro');

estados['Colima'] = new Array('Armeria', 'Colima', 'Comala', 'Coquimatlan', 'Cuauhtemoc', 'Ixtlahuacan', 'Manzanillo', 'Minatitlan', 'Tecoman', 'Villa de alvarez', 'Otro');

estados['Coahuila'] = new Array('Abasolo', 'Acuna', 'Allende', 'Arteaga', 'Candela', 'Castanos', 'Cuatrocienegas', 'Escobedo', 'Francisco I. Madero', 'Frontera', 'General Cepeda', 'Guerrero', 'Hidalgo', 'Jimenez', 'Juarez', 'Lamadrid', 'Matamoros', 'Monclova', 'Morelos', 'Muzquiz', 'Nadadores', 'Nava', 'Ocampo', 'Parras', 'Piedras Negras', 'Progreso', 'Ramos Arizpe', 'Sabinas', 'Sacramento', 'Saltillo', 'San Buenaventura', 'San Juan de Sabinas', 'San Pedro', 'Sierra Mojada', 'Torreon', 'Viesca', 'Villa Union', 'Zaragoza', 'Otro');

estados['Chihuahua'] = new Array('Ahumada', 'Aldama', 'Allende', 'Aquiles Serdan', 'Ascension', 'Bachiniva', 'Balleza', 'Batopilas', 'Bocoyna', 'Buenaventura', 'Camargo', 'Carichi', 'Casas Grandes', 'Chihuahua', 'Chinipas', 'Coronado', 'Coyame del Sotol', 'Cuauhtemoc', 'Cusihuiriachi', 'Delicias', 'Dr. Belisario Dominguez', 'El Tule', 'Galeana', 'Gomez Farias', 'Gran Morelos', 'Guachochi', 'Guadalupe', 'Guadalupe y Calvo', 'Guazapares', 'Guerrero', 'Hidalgo del Parral', 'Huejotitan', 'Ignacio Zaragoza', 'Janos', 'Jimenez', 'Juarez', 'Julimes', 'La Cruz', 'Lopez', 'Madera', 'Maguarichi', 'Manuel Benavides', 'Matachi', 'Matamoros', 'Meoqui', 'Morelos', 'Moris', 'Namiquipa', 'Nonoava', 'Nuevo Casas Grandes', 'Ocampo', 'Ojinaga', 'Praxedis G. Guerrero', 'Riva Palacio', 'Rosales', 'Rosario', 'San Francisco de Borja', 'San Francisco de Conchos', 'San Francisco del Oro', 'Santa Barbara', 'Santa Isabel', 'Satevo', 'Saucillo', 'Temosachi', 'Urique', 'Uruachi', 'Valle de Zaragoza', 'Otro');

estados['Chiapas'] = new Array('Acacoyagua', 'Acala', 'Acapetahua', 'Aldama', 'Altamirano', 'Amatan', 'Amatenango de la Frontera', 'Amatenango del Valle', 'angel Albino Corzo', 'Arriaga', 'Bejucal de Ocampo', 'Bella Vista', 'Benemerito de las Americas', 'Berriozabal', 'Bochil', 'Cacahoatan', 'Capainala', 'Catazaja', 'Chalchihuitan', 'Chamula', 'Chanal', 'Chapultenango', 'Chenalho', 'Chiapa de Corzo', 'Chiapilla', 'Chicoasen', 'Chicomuselo', 'Chilon', 'Cintalapa', 'Coapilla', 'Comitan de Dominguez', 'El Bosque', 'El Porvenir', 'Escuintla', 'Francisco Leon', 'Frontera Comalapa', 'Frontera Hidalgo', 'Huehuetan', 'Huitiupan', 'Huixtan', 'Huixtla', 'Ixhuatan', 'Ixtacomitan', 'Ixtapa', 'Ixtapangajoya', 'Jiquipilas', 'Jitotol', 'Juarez', 'La Concordia', 'La Grandeza', 'La Independencia', 'La Libertad', 'La Trinataria', 'Larrainzar', 'Las Margaritas', 'Las Rosas', 'Mapastepec', 'Maravilla Tenejapa', 'Marques de Comillas', 'Mazapa de Madero', 'Mazatan', 'Metapa', 'Mitontic', 'Montecristo de Guerrero', 'Motozintla', 'Nicolas Ruiz', 'Ocosingo', 'Ocotepec', 'Ocozocoautla de Espinosa', 'Ostuacan', 'Osumacinta', 'Oxchuc', 'Palenque', 'Pantelho', 'Pantepec', 'Pichucalco', 'Pijijiapan', 'Pueblo Nuevo Solistahuacan', 'Rayon', 'Reforma', 'Sabanilla', 'Salto de Agua', 'San Andres Duraznal', 'San Cristobal de las Casas', 'San Fernando', 'San Juan Cancuc', 'San Lucas', 'Santiago el Pinar', 'Siltepec', 'Simojovel', 'Sitala', 'Socoltenango', 'Solosuchiapa', 'Soyalo', 'Suanuapa', 'Suchiapa', 'Suchiate', 'Tapachula', 'Tapalapa', 'Tapilula', 'Tecpatan', 'Tenejapa', 'Teopisca', 'Tila', 'Tonala', 'Totolapa', 'Tumbala', 'Tuxtla Chico', 'Tuxtla Gutierrez', 'Tuzantan', 'Tzimol', 'Union Juarez', 'Venustiano Carranza', 'Villa Comaltitlan', 'Villa Corzo', 'Villaflores', 'Yajalon', 'Zinacantan', 'Otro');

estados['Campeche'] = new Array('Calakmul', 'Calkini', 'Campeche', 'Candelaria', 'Carmen', 'Champoton', 'Escarcega', 'Hecelchakan', 'Hopelchen', 'Palizada', 'Tenabo', 'Otro');

estados['Baja California Sur'] = new Array('Comondu', 'La Paz', 'Loreto', 'Los Cabos', 'Mulege', 'Otro');

estados['Baja California'] = new Array('Ensenada', 'Mexicali', 'Tecate', 'Tijuana', 'Playas de Rosarito', 'Otro');

estados['Aguascalientes'] = new Array('Aguascalientes', 'Asientos', 'Calvillo', 'Cosio', 'El Llano', 'Jesus Maria', 'Pabellon de Arteaga', 'Rincon de Romos', 'San Francisco de los Romo', 'San Jose de Gracia', 'Tepezala', 'Otro');

estados['Distrito Federal'] = new Array('Distrito Federal', 'Otro');

listaEstados = new Array('Aguascalientes', 'Baja California', 'Baja California Sur', 'Campeche', 'Chiapas', 'Chihuahua', 'Coahuila', 'Colima', 'Distrito Federal', 'Durango', 'Guanajuato', 'Guerrero', 'Hidalgo', 'Jalisco', 'Estado de Mexico', 'Michoacan', 'Morelos', 'Nayarit', 'Nuevo Leon', 'Oaxaca', 'Puebla', 'Queretaro', 'Quintana Roo', 'San Luis Potosi', 'Sinaloa', 'Sonora', 'Tabasco', 'Tamaulipas', 'Tlaxcala', 'Veracruz', 'Yucatan', 'Zacatecas', 'Otro');

function cargarEstados(elemento) {
    sel = document.getElementById(elemento);
    totalEstados = listaEstados.length;
    opcion = document.createElement("OPTION");
    opcion.innerHTML = '';
    opcion.value = ''; 	
    sel.appendChild(opcion);
    for(i=0;i<totalEstados;i++) {
        opcion = document.createElement("OPTION");
        opcion.innerHTML = listaEstados[i];
        opcion.value = listaEstados[i]; 	
        sel.appendChild(opcion);
    }
}

function cargarMunicipios(estado,elementoMun,elementoUmf) {
    sel = document.getElementById(elementoMun);
    total = sel.options.length - 1;
    for(j=total;j>=0;j--) {
        sel.options[j] = null;
    }
    
    totalMunicipios = estados[estado].length;
    opcion = document.createElement("OPTION");
    opcion.innerHTML = '';
    opcion.value = ''; 	
    sel.appendChild(opcion);
    for(i=0;i<totalMunicipios;i++) {
        opcion = document.createElement("OPTION");
        opcion.innerHTML = estados[estado][i];
        opcion.value = estados[estado][i]; 	
        sel.appendChild(opcion);
    }
    ObtenerUnidadesMedicas(estado,'Unidad');
}

function cargarMunicipiosConsulta(estado,elementoMun,elementoUmf,umf) {
    sel = document.getElementById(elementoMun);
    total = sel.options.length - 1;
    for(j=total;j>=0;j--) {
        sel.options[j] = null;
    }
    
    totalMunicipios = estados[estado].length;
    opcion = document.createElement("OPTION");
    opcion.innerHTML = '';
    opcion.value = ''; 	
    sel.appendChild(opcion);
    for(i=0;i<totalMunicipios;i++) {
        opcion = document.createElement("OPTION");
        opcion.innerHTML = estados[estado][i];
        opcion.value = estados[estado][i]; 	
        sel.appendChild(opcion);
    }
    ObtenerUnidadesMedicasConsulta(estado,elementoUmf,umf);
}

function quitarAcentos(Text)  
{  
    var cadena=""; 
    var codigo="";  
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)  
    {  
        var Char=Text.charCodeAt(j);
        var cara=Text.charAt(j);
        if (cara == "&") {
            temp = Text.substring(j,j+8);
            switch (temp) {
                case "&aacute;":
                    cadena += "(/a)";
                    j = j + 7;
                    break;
                case "&Aacute;":
                    cadena += "(/A)";
                    j = j + 7;
                    break;
                case "&eacute;":
                    cadena += "(/e)";
                    j = j + 7;
                    break;
                case "&Eacute;":
                    cadena += "(/E)";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "(/i)";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "(/I)";
                    j = j + 7;
                    break;
                case "&oacute;":
                    cadena += "(/o)";
                    j = j + 7;
                    break;
                case "&Oacute;":
                    cadena += "(/O)";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "(/u)";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "(/U)";
                    j = j + 7;
                    break;
                case "&ntilde;":
                    cadena += "(/n)";
                    j = j + 7;
                    break;
                case "&Ntilde;":
                    cadena += "(/N)";
                    j = j + 7;
                    break;
                default:
                    cadena+=Text.charAt(j);  
                    break;  
            }
        } else {
            switch(Char)  
            {  
                case 225:
                    cadena+="(/a)";  
                    break;  
                case 233:
                    cadena+="(/e)";  
                    break;  
                case 237:
                    cadena+="(/i)";  
                    break;  
                case 243:
                    cadena+="(/o)";  
                    break;  
                case 250:
                    cadena+="(/u)";  
                    break;  
                case 193:
                    cadena+="(/A)";  
                    break;  
                case 201:
                    cadena+="(/E)";  
                    break;  
                case 205:
                    cadena+="(/I)";  
                    break;  
                case 211:
                    cadena+="(/O)";  
                    break;  
                case 218:
                    cadena+="(/U)";  
                    break;  
                case 241:
                    cadena+="(/n)";  
                    break;  
                case 209:
                    cadena+="(/N)";  
                    break;  
                default:
                    cadena+=Text.charAt(j);  
                    break;  
            }  
        }
        codigo+="_"+Text.charCodeAt(j);  
    }  
    return cadena;  
}  

function ponerAcentos(Text) {
    var cadena=""; 
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)  
    {  
        var cara=Text.charAt(j);
        if (cara == "(") {
            temp = Text.substring(j,j+4);
            switch (temp) {
                case "(/a)":
                    cadena += "&aacute;";
                    j = j + 3;
                    break;
                case "(/A)":
                    cadena += "&Aacute;";
                    j = j + 3;
                    break;
                case "(/e)":
                    cadena += "&eacute;";
                    j = j + 3;
                    break;
                case "(/E)":
                    cadena += "&Eacute;";
                    j = j + 3;
                    break;
                case "(/i)":
                    cadena += "&iacute;";
                    j = j + 3;
                    break;
                case "(/I)":
                    cadena += "&Iacute;";
                    j = j + 3;
                    break;
                case "(/o)":
                    cadena += "&oacute;";
                    j = j + 3;
                    break;
                case "(/O)":
                    cadena += "&Oacute;";
                    j = j + 3;
                    break;
                case "(/u)":
                    cadena += "&uacute;";
                    j = j + 3;
                    break;
                case "(/U)":
                    cadena += "&Uacute;";
                    j = j + 3;
                    break;
                case "(/n)":
                    cadena += "&ntilde;";
                    j = j + 3;
                    break;
                case "(/N)":
                    cadena += "&Ntilde;";
                    j = j + 3;
                    break;
                default:
                    cadena+=Text.charAt(j);  
                    break;  
            }
        } else {
            cadena+=Text.charAt(j);  
        }
    }  
    return cadena;
}

function ConstructorXMLHttpRequest()  
{ 
    if(window.XMLHttpRequest) /*Vemos si el objeto window(la base de la ventana del navegador) posee el m&eacute;todo XMLHttpRequest(Navegadores como Mozilla y Safari). */  
    { 
        return new XMLHttpRequest(); //Si lo tiene, crearemos el objeto con este m&eacute;todo. 
    } 
    else if(window.ActiveXObject) /*Sino ten&iacute;a el m&eacute;todo anterior, deber&iacute;a ser el Internet Exp. un navegador que emplea objetos ActiveX, lo mismo, miramos si tiene el m&eacute;todo de creaci&oacute;n. */ 
    { 
	
        var request = false;
        try {
            request = new XMLHttpRequest();
        } catch (trymicrosoft) {
            try {
                request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (othermicrosoft) {
                try {
                    request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (failed) {
                    request = false;
                }
            }
        }
        return request;
    } 
    /* Si el navegador llego aqu&iacute; es porque no posee manera alguna de crear el objeto, emitimos un mensaje de error. */ 
    throw new Error("No se pudo crear el objeto XMLHttpRequest"); 
} 


/* El objetivo de este fichero es crear la clase objetoAjax (en Javascript a las �clases� se les llama �prototipos�) */ 
function objetoAjax( )  
{ 
    /*Primero necesitamos un objeto XMLHttpRequest que cogeremos del constructor para que sea compatible con la mayor&iacute;a de navegadores posible. */ 
    this.objetoRequest = new ConstructorXMLHttpRequest();  
} 
 
function peticionAsincrona(url,parametros,tipoPeticion) //Funci&oacute;n asignada al m&eacute;todo coger del objetoAjax. 
{ 
    /*Copiamos el objeto actual, si usamos this dentro de la funci&oacute;n que asignemos a  onreadystatechange, no funcionara.*/ 
    var objetoActual = this;  
    this.objetoRequest.open('POST', url, tipoPeticion); //Preparamos la conexi&oacute;n. 
    /*Aqu&iacute; no solo le asignamos el nombre de la funci&oacute;n, sino la funci&oacute;n completa, as&iacute; cada vez que se cree un nuevo objetoAjax se asignara una nueva funci&oacute;n. */ 
    this.objetoRequest.onreadystatechange =  
    function()  
    { 
        switch(objetoActual.objetoRequest.readyState)  
        { 
            case 1: //Funci&oacute;n que se llama cuando se est&aacute; cargando. 
                objetoActual.cargando(); 
                break; 
            case 2: //Funci&oacute;n que se llama cuando se a cargado. 
                objetoActual.cargado(); 
                break; 
            case 3: //Funci&oacute;n que se llama cuando se est&aacute; en interactivo. 
                objetoActual.interactivo(); 
                break; 
            case 4:
                /*Funci&oacute;n que se llama cuando se completo la transmisi&oacute;n, se le 
env&iacute;an 4  
                                                         par&aacute;metros.*/ 
                objetoActual.completado(objetoActual.objetoRequest.status,  
                    objetoActual.objetoRequest.statusText, 
                    objetoActual.objetoRequest.responseText,  
                    objetoActual.objetoRequest.responseXML); 
                break; 
        } 
    } 
    this.objetoRequest.setRequestHeader('Content-Type','application/x-www-form-urlencoded'); 
    this.objetoRequest.send(parametros); //Iniciamos la transmisi&oacute;n de datos. 
} 
/*Las siguientes funciones las dejo en blanco ya que las redefiniremos seg&uacute;n nuestra necesidad  
haci&eacute;ndolas muy sencillas o complejas dentro de la p&aacute;gina o omitiendolas sino son necesarias.*/ 
function objetoRequestCargando() {} 
function objetoRequestCargado() {} 
function objetoRequestInteractivo() {} 
function objetoRequestCompletado(estado, estadoTexto, respuestaTexto, respuestaXML) {} 
 
/* Por &uacute;ltimo diremos que las funciones que hemos creado, pertenecen al ObjetoAJAX,  con prototype,  
de esta manera todos los objetoAjax que se creen, lo har&aacute;n conteniendo estas funciones en ellos*/ 
 
//Definimos la funci&oacute;n de recoger informaci&oacute;n. 
objetoAjax.prototype.coger = peticionAsincrona ; 
//Definimos una serie de funciones que ser&iacute;a posible utilizar y las dejamos en blanco en esta clase. 
objetoAjax.prototype.cargando = objetoRequestCargando ; 
objetoAjax.prototype.cargado = objetoRequestCargado ; 
objetoAjax.prototype.interactivo = objetoRequestInteractivo ; 
objetoAjax.prototype.completado = objetoRequestCompletado ; 


function AjaxGET()
{
    if(window.XMLHttpRequest) /*Vemos si el objeto window(la base de la ventana del navegador) posee el m&eacute;todo XMLHttpRequest(Navegadores como Mozilla y Safari). */  
    { 
        return new XMLHttpRequest(); //Si lo tiene, crearemos el objeto con este m&eacute;todo. 
    } 
    else if(window.ActiveXObject) /*Sino ten&iacute;a el m&eacute;todo anterior, deber&iacute;a ser el Internet Exp. un navegador que emplea objetos ActiveX, lo mismo, miramos si tiene el m&eacute;todo de creaci&oacute;n. */ 
    { 
	
        var request = false;
        try {
            request = new XMLHttpRequest();
        } catch (trymicrosoft) {
            try {
                request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (othermicrosoft) {
                try {
                    request = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (failed) {
                    request = false;
                }
            }
        }
        return request;
    } 
    /* Si el navegador llego aqu&iacute; es porque no posee manera alguna de crear el objeto, emitimos un mensaje de error. */ 
    throw new Error("No se pudo crear el objeto XMLHttpRequest"); 
}

function cambiarMes(mes) {
    var contenedor;
    contenedor = document.getElementById('contenido');
    var agenda= new AjaxGET();
    var aleatorio = Math.random();
    agenda.open("GET", "agenda.php?getdate="+mes+"&ale="+aleatorio,true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = agenda.responseText
        }
        if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
	
}

function selDia(fechaC, fechaH) {
    var fechaCita = fechaC;
    var fechaHoy = fechaH
    var diaCita = fechaCita.substring(6,8); 
    var mesCita = (fechaCita.substring(4,6))*1 - 1;
    var anoCita = fechaCita.substring(0,4); 
    var fechaCitaU = new Date(anoCita,mesCita,diaCita)
    var diaHoy = fechaHoy.substring(6,8); 
    var mesHoy = (fechaHoy.substring(4,6))*1 - 1;
    var anoHoy = fechaHoy.substring(0,4); 
    var fechaCitaH = new Date(anoHoy,mesHoy,diaHoy)
    //	if(fechaCitaU < fechaCitaH)
    //   {
    //      alert("La fecha de la cita es anterior a la fecha de hoy")
    //    } else {
    var contenedor = document.getElementById('contenido');
    var diario= new AjaxGET();
    var aleatorio = Math.random();
    diario.open("GET", "citasXdia.php?getdate="+fechaC+"&ale="+aleatorio,true);
    diario.onreadystatechange=function()
    {
        if (diario.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = diario.responseText
        }
        if ((diario.readyState==1) ||(diario.readyState==2)||(diario.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    diario.send(null)		
//	}
	
}

var cursores=new Array("auto","hand","crosshair","default","pointer","move","e-resize","ne-resize","nw-resize","n-resize","se-resize","sw-resize","s-resize","w-resize","text","wait");
function setCursor() {
    document.body.style.cursor = 'hand'
}
function restoreCursor() {
    document.body.style.cursor = 'default'
}

function hayCitasArep() {
    var contenedor = document.getElementById('citasReprogramar');
    var objeto= new AjaxGET();
    objeto.open("GET", "nCitasAreprogramar.php",true);
    objeto.onreadystatechange=function()
    {
        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = objeto.responseText;
        }
        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
        {
            contenedor.innerHTML = "...";
        }
    }
    objeto.send(null)
}

function hayCitasRezagadas() {
    var contenedor = document.getElementById('citasRezago');
    var objeto= new AjaxGET();
    objeto.open("GET", "nCitasRezagadas.php",true);
    objeto.onreadystatechange=function()
    {
        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = objeto.responseText;
        }
        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
        {
            contenedor.innerHTML = "...";
        }
    }
    objeto.send(null)
}

function listaCitasArep() {
    var contenedor = document.getElementById('listaCitasReprogramar');
    var objeto= new AjaxGET();
    objeto.open("GET", "listaCitasAreprogramar.php",true);
    objeto.onreadystatechange=function()
    {
        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = objeto.responseText;
        }
        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
        {
            contenedor.innerHTML = "...";
        }
    }
    objeto.send(null)
}

function listaCitasRezagadas() {
    var contenedor = document.getElementById('listaCitasRezago');
    var objeto= new AjaxGET();
    objeto.open("GET", "listaCitasRezagadas.php",true);
    objeto.onreadystatechange=function()
    {
        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = objeto.responseText;
        }
        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
        {
            contenedor.innerHTML = "...";
        }
    }
    objeto.send(null)
}

function cargarCitasAReprogramar() {
    var contenedor = document.getElementById('listaCitasAreprogramar');
    var objeto= new AjaxGET();
    objeto.open("GET", "cargarCitasAReprogramar.php",true);
    objeto.onreadystatechange=function()
    {
        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = objeto.responseText;
        }
        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
        {
            contenedor.innerHTML = "...";
        }
    }
    objeto.send(null)
}

function cargarAgenda() {
    var contenedor2;
    contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    var aleatorio = Math.random();
    agenda.open("GET", "agenda.php?ale="+aleatorio,true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
            mostrarDiv('menu');
            hayCitasArep();
            listaCitasArep();
        //			hayCitasRezagadas();
        //			listaCitasRezagadas();
        }
        if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
	
}

function cargarAgenda2() {
    var contenedor2;
    contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    var aleatorio = Math.random();
    agenda.open("GET", "agenda.php?i="+aleatorio,true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
            mostrarDiv('menu');
        }
        if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
	
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
    var MCon, MConI, MSer, MSerI, MDr, MDrI;
    MCon = document.getElementById('MCon');
    MConI = MCon.selectedIndex;
    idCon = MCon.options[MConI].value;
    MSer = document.getElementById('MSer');
    MSerI = MSer.selectedIndex;
    idSer = MSer.options[MSerI].value;
    MDr = document.getElementById('MMed');
    MDrI = MDr.selectedIndex;
    idDr = MDr.options[MDrI].value;
    var contenedor;
    contenedor = document.getElementById('seleccion');
    var seleccion= new AjaxGET();
    seleccion.open("GET", "seleccionConSerDr.php?idConsultorio="+idCon+"&idServicio="+idSer+"&idMedico="+idDr,true);
    seleccion.onreadystatechange=function()
    {
        if (seleccion.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = seleccion.responseText;
            cargarAgenda2()
        }
        if ((seleccion.readyState==1) ||(seleccion.readyState==2)||(seleccion.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    seleccion.send(null)
	
//  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
//  if (restore) selObj.selectedIndex=0;
}

function agregarCita(idHorario,fecha,horaInicio,horaFin) {
    var contenedor2;
    contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "agregarCita.php?idHorario="+idHorario+"&getdate="+fecha+"&horaInicio="+horaInicio+"&horaFin="+horaFin,true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
            cargarEstados('estado');
            cargarEstados('estadoAgregar');
            document.getElementById("cod_bar").focus();
			  $(function() {
				$( "#combobox" ).combobox();
				$( "#combobox2" ).combobox();
				$( "#toggle" ).live("click", function() {
				  $( "#combobox" ).toggle();
				});
			  });
			
        }
        if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function mostrarDiv(nombre) {
    capa = eval('document.getElementById("' + nombre + '").style');
    //	div = document.getElementById(nombre);
    capa.display ='';
}

function ocultarDiv(nombre) {
    capa = eval('document.getElementById("' + nombre + '").style');
    //	div = document.getElementById(nombre);
    capa.display='none';
}

function validarCedula(val) {
    out = "";
    if (val.length != 10) out = 'La cedula debe ser de 10 caracteres';
    else {
        l1 = val.charAt(0);
        l2 = val.charAt(1);
        l3 = val.charAt(2);
        l4 = val.charAt(3);
        ano = val.charAt(4) + val.charAt(5);
        mes = val.charAt(6) + val.charAt(7);
        dia = val.charAt(8) + val.charAt(9);
        if(!esConsonante(l1)) out = 'El primer caracter de la cedula debe ser una consonante.\n';
        else if (!esVocal(l2))  out = 'El segundo caracter de la cedula debe ser una vocal.\n';
        else if(!esConsonante(l3)) out = 'El tercer caracter de la cedula debe ser una consonante.\n';
        else if(!esConsonante(l4)) out = 'El cuarto caracter de la cedula debe ser una consonante.\n';
        else if(!esFechaValida(dia+"/"+mes+"/19"+ano)) out = 'La fecha de la cedula es invalida.\n';
    }
    return out;
}

function buscarDH(cedula) {
    //	cedulaCorrecta = validarCedula(cedula);
    if (cedula.length > 3) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes');
        var objeto= new AjaxGET();
        objeto.open("GET", "buscarDHparaCita.php?cedula="+cedula,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe teclear al menos 4 caracteres');
    }
}

function buscarDHN(ap_p, ap_m, nombre) {
    if ((ap_p.length > 0) || (ap_m.length > 0) || (nombre.length > 0)) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes2');
        var objeto= new AjaxGET();
        objeto.open("GET", "buscarDHNparaCita.php?ap_p="+ap_p+"&ap_m="+ap_m+"&nombre="+nombre,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe ingresar al menos un campo de busqueda (apellido paterno, apellido materno o nombre)');
    }
}

function buscarDHbusqueda(cedula) {
    if (cedula.length > 3) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes');
        var objeto= new AjaxGET();
        objeto.open("GET", "buscarDHparaBusqueda.php?cedula="+cedula,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe teclear al menos 4 caracteres');
    }
}

function buscarDHNbusqueda(ap_p, ap_m, nombre) {
    if ((ap_p.length > 0) || (ap_m.length > 0) || (nombre.length > 0)) {
        var contenedor;
        contenedor = document.getElementById('derechohabientes2');
        var objeto= new AjaxGET();
        objeto.open("GET", "buscarDHNparaBusqueda.php?ap_p="+ap_p+"&ap_m="+ap_m+"&nombre="+nombre,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor.innerHTML = objeto.responseText;
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Debe ingresar al menos un campo de busqueda (apellido paterno, apellido materno o nombre)');
    }
}

function cargarDatosDH () {
    if (!document.getElementById('dh')) {
        alert( "Ingrese la cedula del derechohabiente y haga click en Buscar..." )	;
    } else {
        valor = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
        valor = ponerAcentos(valor);
        if (valor == -1) {
            alert('No existe derechohabiente con la cedula introducida');
        } else {
            aValor = valor.split('|');
            document.getElementById('id_derecho').value = aValor[0];
            document.getElementById('cedula').value = aValor[1];
            document.getElementById('ap_p').value = aValor[3];
            document.getElementById('ap_m').value = aValor[4];
            document.getElementById('nombre').value = aValor[5];
            document.getElementById('telefono').value = aValor[6];
            dir=aValor[7].split(",");
            document.getElementById('direccion').value = dir[0];
            document.getElementById('col1').value=dir[1];
            document.getElementById('cp1').value=aValor[8];
            document.getElementById('observaciones').disabled = '';
            document.getElementById('estado').value = aValor[9];
            cargarMunicipiosConsulta(aValor[9],'municipio','UnidadCita',aValor[12]);
            var sexo;
            if(aValor[15]=="0")
                sexo="M";
            else
                sexo="H";
            document.getElementById('sexo1').value=sexo;
            ObtenerTpoDerXSexo('sexo1','cedulaTipoCita');
            document.getElementById('cedulaTipoCita').value=aValor[2];
            
            document.getElementById('fecha_nac').value = aValor[11];
            document.getElementById('observaciones').focus();
            document.getElementById('cedulaBuscar').value = '';
            document.getElementById('derechohabientes').innerHTML = 'Ingrese la cedula del derechohabiente y haga click en Buscar...';
            ocultarDiv('buscar');
            document.getElementById('modificar').disabled = '';
            document.getElementById('agregar').disabled = '';
            document.getElementById('municipio').value = aValor[10];
            document.getElementById('emailCita').value=aValor[14];
            document.getElementById('cel1').value=aValor[13];
        }
    }
}

function cargarDatosCitaReprogramar() {
    if ((document.getElementById('selectCitaAreprogramar').value != "") && (document.getElementById('selectCitaRezagada').value != "")) {
        alert( "Seleccione solo una de las opciones");
    } else {
        if ((document.getElementById('selectCitaAreprogramar')) && (document.getElementById('selectCitaAreprogramar').value != "")) {
            valor = document.getElementById('selectCitaAreprogramar').options[document.getElementById('selectCitaAreprogramar').selectedIndex].value;
            valor = ponerAcentos(valor);
            aValor = valor.split('|');
            document.getElementById('id_cita').value = aValor[0];
            document.getElementById('cedula').value = aValor[1];
            document.getElementById('id_derecho').value = aValor[2];
            document.getElementById('ap_p').value = aValor[3];
            document.getElementById('ap_m').value = aValor[4];
            document.getElementById('nombre').value = aValor[5];
            document.getElementById('telefono').value = aValor[6];
            document.getElementById('direccion').value = aValor[7];
            document.getElementById('observaciones').disabled = '';
            document.getElementById('estado').value = aValor[8];
            cargarMunicipios(aValor[8],'municipio');
            document.getElementById('observaciones').focus();
            document.getElementById('cedulaBuscar').value = '';
            ocultarDiv('reprogramar');
            document.getElementById('modificar').disabled = '';
            document.getElementById('agregar').disabled = '';
            document.getElementById('municipio').value = aValor[9];
            document.getElementById('tipo_reprogramacion').value = aValor[10];
        } else {
            if ((document.getElementById('selectCitaRezagada')) && (document.getElementById('selectCitaRezagada').value != "")) {
                valor = document.getElementById('selectCitaRezagada').options[document.getElementById('selectCitaRezagada').selectedIndex].value;
                valor = ponerAcentos(valor);
                aValor = valor.split('|');
                document.getElementById('id_cita').value = aValor[0];
                document.getElementById('cedula').value = aValor[1];
                document.getElementById('id_derecho').value = aValor[2];
                document.getElementById('ap_p').value = aValor[3];
                document.getElementById('ap_m').value = aValor[4];
                document.getElementById('nombre').value = aValor[5];
                document.getElementById('telefono').value = aValor[6];
                document.getElementById('direccion').value = aValor[7];
                document.getElementById('observaciones').disabled = '';
                document.getElementById('estado').value = aValor[8];
                cargarMunicipios(aValor[8],'municipio');
                document.getElementById('observaciones').focus();
                document.getElementById('cedulaBuscar').value = '';
                ocultarDiv('reprogramar');
                document.getElementById('modificar').disabled = '';
                document.getElementById('agregar').disabled = '';
                document.getElementById('municipio').value = aValor[9];
                document.getElementById('tipo_reprogramacion').value = aValor[10];
            } else {
                alert( "Seleccione una cita a reprogramar");
            }
        }
    }
}

function validarAgregarCita() {
    var id_derecho = document.getElementById('id_derecho').value;
    var id_cita = document.getElementById('id_cita').value;
    var tipo_reprogramacion = document.getElementById('tipo_reprogramacion').value;
    var cedula = document.getElementById('cedula').value;
    var ap_p = document.getElementById('ap_p').value;
    var ap_m = document.getElementById('ap_m').value;
    var nombre = document.getElementById('nombre').value;
    var telefono = document.getElementById('telefono').value;
    var direccion =	document.getElementById('direccion').value;
    var estado = document.getElementById('estado').value;
    var municipio =	document.getElementById('municipio').value;
    var observaciones =	document.getElementById('observaciones').value;
    if (document.getElementById('diagnostico') != undefined) {
        var diagnostico = document.getElementById('diagnostico').value;
    } else {
        var diagnostico = "";
    }
    if (document.getElementById('concertada') != undefined) {
        if (document.getElementById('concertada').checked == true)
            var concertada = "si";
        else 
            var concertada = "no";
    } else {
        var concertada = "";
    }
    if ((id_derecho.length > 0) && (cedula.length > 0) && (ap_p.length > 0) && (ap_m.length > 0) && (nombre.length > 0)) {
        var contenedor;
        contenedor = document.getElementById('contenido');
        var contenedor2;
        contenedor2 = document.getElementById('enviando');
        var objeto= new AjaxGET();
        objeto.open("GET", "agregarCitaConfirmar.php?id_derecho="+id_derecho+"&idHorario="+document.getElementById('id_horario').value+"&fechaCita="+document.getElementById('fechaCita').value+"&obs="+observaciones+"&id_cita="+id_cita+"&tipo_reprogramacion="+tipo_reprogramacion+"&diagnostico="+diagnostico+"&concertada="+concertada,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                contenedor2.innerHTML = "&nbsp;";
                hayCitasArep();
                listaCitasArep();
                hayCitasRezagadas();
                listaCitasRezagadas();
                document.getElementById('agregar').disabled = '';
                document.getElementById('regresar').disabled = '';
                document.getElementById('modificar').disabled = '';
                document.getElementById('seleccionar').disabled = '';
                document.getElementById('botonReprogramar').disabled = '';
                citaAgregada(objeto.responseText);
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                document.getElementById('agregar').disabled = 'disabled';
                document.getElementById('regresar').disabled = 'disabled';
                document.getElementById('modificar').disabled = 'disabled';
                document.getElementById('seleccionar').disabled = 'disabled';
                document.getElementById('botonReprogramar').disabled = 'disabled';
                contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Cedula y nombre completo es requerido');
    }
}

function esHoraValida(val,nm) {
    errors = "";
    a = val.charAt(0);
    b = val.charAt(1);
    c = val.charAt(2);
    d = val.charAt(3);
    e = val.charAt(4);
    if (val.length != 5) errors += '- '+nm+' debe contener una hora valida. ej 08:30\n';
    else if (isNaN(a)) errors+='- '+nm+' debe contener una hora valida. ej 08:30\n';
    else if (isNaN(b)) errors+='- '+nm+' debe contener una hora valida. ej 08:30\n';
    else if (c != ':') errors+='- '+nm+' debe contener una hora valida. ej 08:30\n';
    else if (isNaN(d)) errors+='- '+nm+' debe contener una hora valida. ej 08:30\n';
    else if (isNaN(e)) errors+='- '+nm+' debe contener una hora valida. ej 08:30\n';
    else if ((a==2 && b>3) || (a>2)) errors+='- '+nm+' debe contener una hora valida. ej 08:30\n';
    else if (d>5) errors+='- '+nm+' debe contener una hora valida. ej 08:30\n';
    return errors;
}

function validarAgregarCitaExtemporanea() {
    var id_derecho = document.getElementById('id_derecho').value;
    var id_cita = document.getElementById('id_cita').value;
    var hora_inicio = document.getElementById('hora_inicio').value;
    var hora_fin = document.getElementById('hora_fin').value;
    var cedula = document.getElementById('cedula').value;
    var ap_p = document.getElementById('ap_p').value;
    var ap_m = document.getElementById('ap_m').value;
    var nombre = document.getElementById('nombre').value;
    var telefono = document.getElementById('telefono').value;
    var direccion =	document.getElementById('direccion').value;
    var estado = document.getElementById('estado').value;
    var municipio =	document.getElementById('municipio').value;
    var tipo_cita =	document.getElementById('tipo_cita').value;
    var diagnostico =	document.getElementById('diagnostico').value;
    var observaciones =	document.getElementById('observaciones').value;
    var validarHoraInicio = esHoraValida(hora_inicio,'Hora Inicio');
    var concertada = "";
    if (tipo_cita == "0") {
        if (document.getElementById('concertada').checked == true)
            var concertada = "si";
        else 
            var concertada = "no";
    }
    if (tipo_cita == "-1") {
        alert('Tipo de Cita es requerido');
    } else {
        if(validarHoraInicio == "") {
            var validarHoraFin = esHoraValida(hora_fin,'Hora Fin');
            if(validarHoraFin == "") {
                if ((id_derecho.length > 0) && (cedula.length > 0) && (ap_p.length > 0) && (ap_m.length > 0) && (nombre.length > 0)) {
                    var contenedor;
                    contenedor = document.getElementById('contenido');
                    var objeto= new AjaxGET();
                    var fechaCita=document.getElementById('fechaCita').value;
                    objeto.open("GET", "agregarCitaExtemporaneaConfirmar.php?id_derecho="+id_derecho+"&hora_inicio="+hora_inicio+"&hora_fin="+hora_fin+"&fechaCita="+fechaCita+"&obs="+observaciones+"&id_cita="+id_cita+"&tipo_cita="+tipo_cita+"&diagnostico="+diagnostico+"&concertada="+concertada,true);
                    objeto.onreadystatechange=function()
                    {
                        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
                        {
							
                            citaExtemporaneaAgregada(objeto.responseText,fechaCita);
                        }
                        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
                        {
                            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
                        }
                    }
                    objeto.send(null)
                } else {
                    alert('Cedula y nombre completo es requerido');
                }
            } else {
                alert(validarHoraFin);
            }
        } else {
            alert(validarHoraInicio);
        }
    }
}


function DetectaBloqueoPops()
{
    var popup
    try
    {
        //Se crea una nueva ventana para probar si esta o no activo
        // el bloqueador de ventanas emergentes.
        //Si esta activo, se lanza el error, de lo contrario s�lo se cierra la ventana creada
        if(!(popup = window.open('about:blank','_blank','width=1,height=1')))
            throw "ErrPop"
        msj = "La ventana se cre� con �xito"
        popup.close()
    }
    catch(err)
    {
        //Se captura el error, si fue por motivo de bloqueo, se muestra el mensaje de advertencia
        //Si no fue por bloque, entonces se muestra la descripci�n del error ocurrido.
        if(err=="ErrPop")
            msj = "tA T E N C I � Nnn�El bloqueo de popups esta activo!"
        else
        {
            msj="Hubo un erro en la p�gina.nn"
            msj+="Descripci�n del error: " + err.description + "nn"
        }
    }
    alert(msj)
 
}

function citaAgregada(datosCita) {
    aRespuesta = datosCita.split('|');
    tRespuesta = aRespuesta.length;
    if (tRespuesta<2) {
        eval(datosCita);
    } else {
        window.open('imprimirCita.php?id_cita='+aRespuesta[0]+'&id_horario='+aRespuesta[1]+'&id_derecho='+aRespuesta[2]+'&fecha_cita='+aRespuesta[3]+'&id_usuario='+aRespuesta[4]+'&tipo='+aRespuesta[5],'_blank');
        alert('Cita Agregada Correctamente\n\r');
        if (aRespuesta[6] == "si") {
            alert('contrareferencia');
        }
        selDia(aRespuesta[3], aRespuesta[3]);
    }
}

function citaExtemporaneaAgregada(datosCita,fecha) {
    aRespuesta = datosCita.split('|');
    tRespuesta = aRespuesta.length;
    contenedor = document.getElementById('contenido');
    if(tRespuesta<2){
        eval(datosCita);
        var objeto= new AjaxGET();
        objeto.open("GET", "agregarCitaExtemporanea.php?getdate="+fecha,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
							
                contenedor.innerHTML=objeto.responseText;
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    }
    else{
        window.open('imprimirCitaExtemporanea.php?id_consultorio='+aRespuesta[0]+'&id_servicio='+aRespuesta[1]+'&id_derecho='+aRespuesta[2]+'&fecha_cita='+aRespuesta[3]+'&id_usuario='+aRespuesta[4]+'&fecha_inicio='+aRespuesta[5]+'&fecha_fin='+aRespuesta[6],'_blank');
        alert('Cita Agregada Extemporanea Correctamente\n\r');
        if (aRespuesta[7] == "si") {
            alert('contrareferencia');
        }
        selDia(aRespuesta[3], aRespuesta[3]);
    }
}

function habilitarParaModificarDH() {
    paraRestaurarModificacionesDHenAgregarCitaAp_p = document.getElementById('ap_p').value;
    paraRestaurarModificacionesDHenAgregarCitaAp_m = document.getElementById('ap_m').value;
    paraRestaurarModificacionesDHenAgregarCitaNombre = document.getElementById('nombre').value;
    paraRestaurarModificacionesDHenAgregarCitaFec_N = document.getElementById('fecha_nac').value;
    paraRestaurarModificacionesDHenAgregarCitaTel = document.getElementById('telefono').value;
    paraRestaurarModificacionesDHenAgregarCitaDir = document.getElementById('direccion').value;
    ind=document.getElementById('estado').selectedIndex;
    paraRestaurarModificacionesDHenAgregarCitaEst = document.getElementById('estado').options[ind].value;
	ind=document.getElementById('municipio').selectedIndex;
	if(ind>0)
    paraRestaurarModificacionesDHenAgregarCitaMun = document.getElementById('municipio').options[ind].value;
    ind=document.getElementById('UnidadCita').selectedIndex;
	if(ind>0)
    restaurarUMF=document.getElementById('UnidadCita').options[ind].value;
    ind=document.getElementById('sexo').selectedIndex;
    restaurarSexo=document.getElementById('sexo').options[ind].value;
    restaurarTpoDer=document.getElementById('cedulaTipoCita').options[document.getElementById('cedulaTipoCita').selectedIndex].value;
    document.getElementById('ap_p').readOnly = '';
    document.getElementById('ap_m').readOnly = '';
    document.getElementById('nombre').readOnly = '';
    document.getElementById('telefono').readOnly = '';
    document.getElementById('fecha_nac').readOnly = '';
    document.getElementById('direccion').readOnly = '';
    document.getElementById('estado').disabled = '';
    document.getElementById('municipio').disabled = '';
    document.getElementById('UnidadCita').disabled='';
    document.getElementById('sexo1').disabled='';
    document.getElementById('cedulaTipoCita').disabled='';
    document.getElementById('col1').readOnly=false;
    document.getElementById('cp1').readOnly=false;
    document.getElementById('emailCita').readOnly=false;
    document.getElementById('cel1').readOnly=false;
    mostrarDiv('modificarDHGuardar');
    ocultarDiv('modificarDH');
}

function cancelarModificacionesDH() {
    document.getElementById('ap_p').value = paraRestaurarModificacionesDHenAgregarCitaAp_p;
    document.getElementById('ap_m').value = paraRestaurarModificacionesDHenAgregarCitaAp_m;
    document.getElementById('nombre').value = paraRestaurarModificacionesDHenAgregarCitaNombre;
    document.getElementById('fecha_nac').value = paraRestaurarModificacionesDHenAgregarCitaFec_N;
    document.getElementById('telefono').value = paraRestaurarModificacionesDHenAgregarCitaTel;
    document.getElementById('direccion').value = paraRestaurarModificacionesDHenAgregarCitaDir;
    document.getElementById('estado').value = paraRestaurarModificacionesDHenAgregarCitaEst;
    document.getElementById('municipio').value = paraRestaurarModificacionesDHenAgregarCitaMun;
    paraRestaurarModificacionesDHenAgregarCitaAp_p = "";
    paraRestaurarModificacionesDHenAgregarCitaAp_m = "";
    paraRestaurarModificacionesDHenAgregarCitaNombre = "";
    paraRestaurarModificacionesDHenAgregarCitaFec_N = "";
    paraRestaurarModificacionesDHenAgregarCitaTel = "";
    paraRestaurarModificacionesDHenAgregarCitaDir = "";
    paraRestaurarModificacionesDHenAgregarCitaEst = "";
    paraRestaurarModificacionesDHenAgregarCitaMun = "";
    document.getElementById('ap_p').readOnly = 'readOnly';
    document.getElementById('ap_m').readOnly = 'readOnly';
    document.getElementById('nombre').readOnly = 'readOnly';
    document.getElementById('fecha_nac').readOnly = 'readOnly';
    document.getElementById('telefono').readOnly = 'readOnly';
    document.getElementById('direccion').readOnly = 'readOnly';
    document.getElementById('estado').disabled = 'disabled';
    document.getElementById('municipio').disabled = 'disabled';
    document.getElementById('UnidadCita').disabled='disabled';
    document.getElementById('col1').readOnly=true;
    document.getElementById('cp1').readOnly=true;
    document.getElementById('sexo1').disabled=true;
    document.getElementById('cedulaTipoCita').disabled=true;
    document.getElementById('emailCita').readOnly=true;
    document.getElementById('cel1').readOnly=true;
    mostrarDiv('modificarDH');
    ocultarDiv('modificarDHGuardar');
}

function guardarModificacionesDH() {
    var id_derecho = document.getElementById('id_derecho').value;
    var cedula = document.getElementById('cedula').value;
    var ap_p = document.getElementById('ap_p').value;
    var ap_m = document.getElementById('ap_m').value;
    var nombre = document.getElementById('nombre').value;
    var fecha_nac = document.getElementById('fecha_nac').value;
    var telefono = document.getElementById('telefono').value;
    var direccion =	document.getElementById('direccion').value+","+document.getElementById('col1').value
	var cp=document.getElementById('cp1').value;
    var estado = document.getElementById('estado').value;
    var municipio =	document.getElementById('municipio').value;
    var umf=document.getElementById('UnidadCita').value;
    var sexo=document.getElementById('sexo1').value;
    var tpoDer=document.getElementById('cedulaTipoCita').value;
    var cel=document.getElementById('cel1').value;
    var email=document.getElementById('emailCita').value;
	if(tpoDer==60 || tpoDer==61 || tpoDer==50 || tpoDer==51 || tpoDer==80 || tpoDer==81 || tpoDer==70 || tpoDer==71)
    {
        fechaNac=validarFechaCedula(cedula, fecha_nac, tpoDer);
        if(!fechaNac)
            return;
    }
    if(tpoDer=="10"||tpoDer=="20"||tpoDer=="90")
    {
        cedulaTrab=validarCedulaTrabajador('cedulaAgregar',nom,ap_p,ap_m,fecha_nac);
        if(!cedulaTrab)
            return;
    }
    if ((id_derecho.length > 0) && (cedula.length > 0) && (ap_p.length > 0) && (ap_m.length > 0) && (nombre.length > 0)) {
        var contenedor;
        contenedor = document.getElementById('modificarDHGuardar');
        var objeto= new AjaxGET();
        cadena = quitarAcentos("modificarDHenCita.php?id_derecho="+id_derecho+"&ap_p="+ap_p+"&ap_m="+ap_m+"&nombre="+nombre+"&fecha_nac="+fecha_nac+"&telefono="+telefono+"&direccion="+direccion+"&cp="+cp+"&estado="+estado+"&municipio="+municipio+"&sexo="+sexo+"&umf="+umf+"&tpoCed="+tpoDer+"&email="+email+"&cel="+cel);
        objeto.open("GET", cadena,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                if (objeto.responseText == "ok") {
                    alert("Datos Modificados Correctamente");
                } else {
                    alert(objeto.responseText);
                //					alert("Error al modificar los datos, pongase en contacto con el administrador del sistema");
                }
                document.getElementById('ap_p').readOnly = 'readOnly';
                document.getElementById('ap_m').readOnly = 'readOnly';
                document.getElementById('nombre').readOnly = 'readOnly';
                document.getElementById('fecha_nac').readOnly = 'readOnly';
                document.getElementById('telefono').readOnly = 'readOnly';
                document.getElementById('direccion').readOnly = 'readOnly';
                document.getElementById('estado').disabled = 'disabled';
                document.getElementById('municipio').disabled = 'disabled';
                document.getElementById('UnidadCita').disabled=true;
                document.getElementById('cedulaTipoCita').disabled=true;
                document.getElementById('cel1').readOnly=true;
                document.getElementById('emailCita').readOnly=true;
                document.getElementById('col1').readOnly=true;
                document.getElementById('cp1').readOnly=true;
                mostrarDiv('modificarDH');
                ocultarDiv('modificarDHGuardar');
                contenedor.innerHTML = '<input name="guardarMod" type="button" value="Guardar Modificaciones" class="botones" onclick="javascript: guardarModificacionesDH();" /> <input name="cancelarMod" type="button" value="Cancelar" class="botones" onclick="javascript: cancelarModificacionesDH();" />';
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Nombre completo es requerido');
    }
}

function agregarDHenCita() {
    var cedula = document.getElementById('cedulaBuscar').value;
    //	document.getElementById('buscar').style.height = "0px;";
    ocultarDiv('buscar'); 
    mostrarDiv('divAgregarDH');
    document.getElementById('divBotones_EstadoAgregarDH').innerHTML = '<input name="botonCancelarAgregarDH" type="button" class="botones" id="botonCancelarAgregarDH" onclick="javascript: cancelarAgregarDHenCita();" value="Cancelar" />&nbsp;&nbsp;&nbsp;&nbsp;<input name="botonAgregarDH" type="submit" class="botones" id="botonAgregarDH" value="Agregar" />';
    document.getElementById('divAgregarDH').style.height = "180px";
    document.getElementById('cedulaAgregar').value = cedula;
    document.getElementById('cedulaTipoAgregar').focus();
}

function cancelarAgregarDHenCita() {
    document.getElementById('cedulaAgregar').value = "";	
    document.getElementById('cedulaTipoAgregar').value = "";	
    document.getElementById('ap_pAgregar').value = "";	
    document.getElementById('ap_mAgregar').value = "";	
    document.getElementById('nombreAgregar').value = "";	
    document.getElementById('telefonoAgregar').value = "";	
    document.getElementById('direccionAgregar').value = "";	
    document.getElementById('estadoAgregar').value = "";	
    document.getElementById('municipioAgregar').value = "";	
    ocultarDiv('divAgregarDH');
}

function agregarDHenCitaForma() {
    var patronTel=/^[0-9]{2,3}[0-9]{7,8}$/;
    var patronCed=/^[A-Z][AEIOU][A-Z]{2}[0-9]{2}[0][0-9]|[1][0-2][0-2][0-9]|[3][0-1]$/;
    var patronFecha=/^[0-2][0-9]|[3][0-1]\/[0][9]|[1][0-2]\/[0-9][0-9][0-9][0-9]$/;
    var patronCodigoPostal=/^[0-9]{5}$/;
    var patronNombre=/^\d+$/
    var cedula=patronCed.test($("#cedulaAgregar").val());
    var telefono=patronTel.test($("#telefonoAgregar").val());
    var fecha=patronFecha.test($("#fecha_nacAgregar").val());
    var cp=patronCodigoPostal.test($("#cp").val());
    var ced=$("#cedulaAgregar").val();
    ind=document.getElementById('sexo').selectedIndex;
    var sexo=document.getElementById('sexo').options[ind].value;
    ind=document.getElementById('cedulaTipoAgregar').selectedIndex;
    var tpoCed=document.getElementById('cedulaTipoAgregar').options[ind].value;
    var app=quitarAcentos($("#ap_pAgregar").val());
    var apm=quitarAcentos($("#ap_mAgregar").val());
    var nom=quitarAcentos($("#nombreAgregar").val());
    var tel=$("#telefonoAgregar").val();
    var fecNac=$("#dia1").val()+"/"+$("#mes1").val()+"/"+$("#anio1").val();
    var dir=$("#direccionAgregar").val();
    var col=$("#col").val();
    var cp1=$("#cp").val();
    var email=$("#emailAgregar").val();
    var cel=$("#cel").val();
    ind=document.getElementById('estadoAgregar').selectedIndex;
    var edo=document.getElementById('estadoAgregar').options[ind].value;
    ind=document.getElementById('municipioAgregar').selectedIndex;
    var mun=document.getElementById('municipioAgregar').options[ind].value;
    ind=document.getElementById('Unidad').selectedIndex;
    var um=document.getElementById('Unidad').options[ind].value;
    var cont=true;
    if(um=="-1")
    {
        alert("Clinica de procedencia del derechohabiente");
        cont=false;
    }
	if(tpoCed==60 || tpoCed==61 || tpoCed==50 || tpoCed==51 || tpoCed==80 || tpoCed==81 || tpoCed==70 || tpoCed==71)
    {
        fechaNac=validarFechaCedula(ced, fecNac, tpoCed);
        if(!fechaNac)
            return;
    }
    if(tpoCed=="10"||tpoCed=="20"||tpoCed=="90")
    {
        cedulaTrab=validarCedulaTrabajador('cedulaAgregar',nom,app,apm,fecNac);
        if(!cedulaTrab)
            return;
    }
    if(!cedula || $("#cedulaAgregar").val()=="")
    {
        if($("#cedulaAgregar").val()=="")
            alert("introduzca la cedula del derechoHabiente");
        else
            alert("Introduzca una cedula correcta");
        $("#cedulaAgregar").val("");
        cont=false;
    }
    else
    if(validarCedula(document.getElementById('cedulaAgregar').value)!=""){
        alert(validarCedula(document.getElementById('cedulaAgregar').value));
        $("#cedulaAgregar").val("");
        cont=false;
    }
    else
    if(($("#ap_pAgregar").val()=="" || patronNombre.test(app))|| $("#ap_pAgregar").val()=="Apellido Paterno") 
    {
        alert("introduzca el apellido Paterno");
        cont=false;
    }
    if(($("#ap_mAgregar").val()=="" || patronNombre.test(apm))|| $("#ap_pAgregar").val()=="Apellido Materno" ) 
    {
        alert("introduzca el apellido Materno");
        cont=false;
    }
    else
    if(($("#nombreAgregar").val()=="" || patronNombre.test(nom))|| $("#nombreAgregar").val()=="Apellido Materno" ) 
    {
        alert("introduzca el nombre del DerechoHabiente");
        cont=false;
    }
    else
    if($("#telefonoAgregar").val()==""|| !telefono)
    {
        if($("#telefonoAgregar").val()=="")
            alert("introduzca un numero de telefono");
        else
            alert("Introduzca el numero de telefono con formato 01(lada)numero \n o (lada)telefono \n de 8 a 10 digitos");
        cont=false;
    }
    else
    if($("#direccionAgregar").val()=="" || $("#col").val()==""||($("#cp").val()=="" || !cp)){
        alert("introduzca la direccion del DerechoHabiente");
        cont=false;
    }
    else
    if($("#estadoAgregar option:selected").val()=="")
    {
        alert("elija el estado de procedencia del DerechoHabiente");
        cont=false;
    }
    else
    if($("#municipioAgregar option:selected").val()=="")
    {
        alert("elija el municipio de procedencia del DerechoHabiente");
        cont=false;
    }
    else{
        if($("#emailAgregar").val()!="")
        {
            Pemail = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
            if(!Pemail.test($("#emailAgregar").val()))
            {
                alert("Introduzca el correo Electronico alguien@dominio.com");
                cont=false;
            }
                
        }
        if($("#cel").val()!="")
        {
            PCel=/[0-9]{10}/;
            if(!PCel.test($("#cel").val()))
            {
                alert("El Numero de celular tiene que ser numerico y de 10 digitos \n ejemplo 3310000100");
                cont=false;
            }
        }
        if(cont){
            var contenedor = document.getElementById('divBotones_EstadoAgregarDH');
            var objeto= new AjaxGET();
            cadena = quitarAcentos("agregarDHenCita.php?cedulaAgregar="+ced+"&cedulaTipoAgregar="+tpoCed+"&ap_pAgregar="+app+"&ap_mAgregar="+apm+"&nombreAgregar="+nom+"&fecha_nacAgregar="+fecNac+"&direccionAgregar="+dir+"&telefonoAgregar="+tel+"&estadoAgregar="+edo+"&municipioAgregar="+mun+"&cel="+cel+"&cp="+cp1+"&col="+col+"&Unidad="+um+"&emailAgregar="+email+"&sexo="+sexo);
            objeto.open("GET",cadena ,true);
            objeto.onreadystatechange=function()
            {
                if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
                {
                    if (objeto.responseText > 0) {
                        alert("Derechohabiente Agregado Correctamente");
                        document.getElementById('id_derecho').value = objeto.responseText;
                        document.getElementById('cedula').value = ced+'/'+tpoCed;
                        document.getElementById('ap_p').value = app;
                        document.getElementById('ap_m').value = apm;
                        document.getElementById('nombre').value = nom;
                        document.getElementById('fecha_nac').value = fecNac;
                        document.getElementById('telefono').value = tel;
                        document.getElementById('direccion').value = dir;
                        document.getElementById('estado').value = edo;
                        cargarMunicipios(edo,'municipio','UnidadCita');
                        document.getElementById('municipio').value = mun;
                        document.getElementById('UnidadCita').value=um;
                        document.getElementById('observaciones').disabled = '';
                        document.getElementById('observaciones').focus();
                        document.getElementById('modificar').disabled = '';
                        document.getElementById('agregar').disabled = '';
                    } else {
                        alert("Error al agregar derechohabiente, pongase en contacto con el administrador del sistema");
                    }
                    ocultarDiv('divAgregarDH');
                    contenedor.innerHTML = '<input name="botonCancelarAgregarDH" type="button" class="botones" id="botonCancelarAgregarDH" onclick="javascript: cancelarAgregarDHenCita();" value="Cancelar" />&nbsp;&nbsp;&nbsp;&nbsp;<input name="botonAgregarDH" type="submit" class="botones" id="botonAgregarDH" value="Agregar" />';
                }
                if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
                {
                    contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            }
            objeto.send(null)
        }
    }
}

function eliminarCitaXdia(id_cita,horaInicio,horaFin) {
    var contenedor;
    contenedor = document.getElementById('contenido');
    var objeto= new AjaxGET();
    objeto.open("GET", "eliminarCita.php?id_cita=" + id_cita+"&horaInicio="+horaInicio+"&horaFin="+horaFin,true);
    objeto.onreadystatechange=function()
    {
        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = objeto.responseText;
        }
        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    objeto.send(null)
}

function eliminarCitaXdiaConfirmar(id_cita, fecha, textoPregunta) {
    var motivo = document.getElementById('motivo').value;
    var desc = document.getElementById('desc').value;
    var nombreAutorizo = document.getElementById('nombreAutorizo').value;
    error = '';
    if ((motivo == "OTRO") && (desc.length < 5)) error = 'Debe introducir la descripcion de la cancelacion de la cita'; 
    if (nombreAutorizo.length < 5) error = 'Debe introducir el nombre de quien autorizo la cancelacion de la cita'; 
    if (error == '') {
        var respuesta = window.confirm(textoPregunta);
        if (respuesta == true) {
            var contenedor;
            contenedor = document.getElementById('estadoEliminando');
            var objeto= new AjaxGET();
            objeto.open("GET", "eliminarCitaConfirmar.php?id_cita=" + id_cita+'&motivo='+motivo+'&nombreAutorizo='+nombreAutorizo+'&desc='+desc,true);
            objeto.onreadystatechange=function()
            {
                if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
                {
                    if (objeto.responseText == "ok") {		
                        selDia(fecha, fecha);
                    } else {
                        alert("Error al eliminar cita, pongase en contacto con el administrador del sistema");
                    }
                    contenedor.innerHTML = '';
                }
                if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
                {
                    contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            }
            objeto.send(null)
        }
    } else {
        alert(error);
    }
}

function eliminarCitaEXdia(id_cita) {
    var contenedor;
    contenedor = document.getElementById('contenido');
    var objeto= new AjaxGET();
    objeto.open("GET", "eliminarCitaE.php?id_cita=" + id_cita,true);
    objeto.onreadystatechange=function()
    {
        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = objeto.responseText;
        }
        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    objeto.send(null)
}

function eliminarCitaEXdiaConfirmar(id_cita, fecha, textoPregunta) {
    var motivo = document.getElementById('motivo').value;
    var desc = document.getElementById('desc').value;
    var nombreAutorizo = document.getElementById('nombreAutorizo').value;
    error = '';
    if ((motivo == "OTRO") && (desc.length < 5)) error = 'Debe introducir la descripcion de la cancelacion de la cita'; 
    if (nombreAutorizo.length < 5) error = 'Debe introducir el nombre de quien autorizo la cancelacion de la cita'; 
    if (error == '') {
        var respuesta = window.confirm(textoPregunta);
        if (respuesta == true) {
            var contenedor;
            contenedor = document.getElementById('estadoEliminando');
            var objeto= new AjaxGET();
            objeto.open("GET", "eliminarCitaEConfirmar.php?id_cita=" + id_cita+'&motivo='+motivo+'&nombreAutorizo='+nombreAutorizo+'&desc='+desc,true);
            objeto.onreadystatechange=function()
            {
                if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
                {
                    if (objeto.responseText == "ok") {		
                        selDia(fecha, fecha);
                    } else {
                        alert("Error al eliminar cita, pongase en contacto con el administrador del sistema");
                    }
                    contenedor.innerHTML = '';
                }
                if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
                {
                    contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            }
            objeto.send(null)
        }
    } else {
        alert(error);
    }
}

function modificarCita(idCita,fecha,horaInicio,horaFin,id_derecho,cedula,cedula_tipo,telefono,direccion,ap_p,ap_m,nombre,observaciones,estado,municipio) {
    var contenedor;
    contenedor = document.getElementById('contenido');
    var objeto= new AjaxGET();
    objeto.open("GET", "modificarCita.php?idCita="+idCita+"&getdate="+fecha+"&horaInicio="+horaInicio+"&horaFin="+horaFin+"&id_derecho="+id_derecho+"&cedula="+cedula+"&cedula_tipo="+cedula_tipo+"&telefono="+telefono+"&direccion="+direccion+"&ap_p="+ap_p+"&ap_m="+ap_m+"&nombre="+nombre+"&observaciones="+observaciones+"&estado="+estado+"&municipio="+municipio,true);
    objeto.onreadystatechange=function()
    {
        if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor.innerHTML = objeto.responseText;
            cargarEstados('estado');
            document.getElementById('estado').value = ponerAcentos(estado); 		
            cargarMunicipios(ponerAcentos(estado),'municipio');
            document.getElementById('municipio').value = ponerAcentos(municipio); 
            cargarEstados('estadoAgregar');
        }
        if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
        {
            contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    objeto.send(null)
}

function validarModificarCita() {
    var id_derecho = document.getElementById('id_derecho').value;
    var cedula = document.getElementById('cedula').value;
    var ap_p = document.getElementById('ap_p').value;
    var ap_m = document.getElementById('ap_m').value;
    var nombre = document.getElementById('nombre').value;
    var telefono = document.getElementById('telefono').value;
    var direccion =	document.getElementById('direccion').value;
    var observaciones =	document.getElementById('observaciones').value;
    if ((id_derecho.length > 0) && (cedula.length > 0) && (ap_p.length > 0) && (ap_m.length > 0) && (nombre.length > 0)) {
        var contenedor;
        contenedor = document.getElementById('contenido');
        var objeto= new AjaxGET();
        objeto.open("GET", "modificarCitaConfirmar.php?id_derecho="+id_derecho+"&idCita="+document.getElementById('id_cita').value+"&fechaCita="+document.getElementById('fechaCita').value+"&obs="+observaciones,true);
        objeto.onreadystatechange=function()
        {
            if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
            {
                citaModificada(objeto.responseText);
            }
            if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
            {
                contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
            }
        }
        objeto.send(null)
    } else {
        alert('Cedula y nombre completo es requerido');
    }
}

function citaModificada(fechaCita) {
    alert('Cita Modificada Correctamente');
    selDia(fechaCita, fechaCita);
}

function esFechaValida(fecha){
    if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha)){
        return false;
    }
    var dia  =  parseInt(fecha.substring(0,2),10);
    var mes  =  parseInt(fecha.substring(3,5),10);
    var anio =  parseInt(fecha.substring(6),10);
 
    switch(mes){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            numDias=31;
            break;
        case 4: case 6: case 9: case 11:
            numDias=30;
            break;
        case 2:
            numDias=29;
            break;
        default:
            return false;
    }
 
    if (dia>numDias || dia==0){
        return false;
    }
    return true;
}

function esConsonante(valor) {
    var conso = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','&ntilde;','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    tConso = conso.length;
    seEncontro = false;
    for(i=0;i<tConso;i++)
        if (conso[i] == valor.toUpperCase()) seEncontro = true;
    return seEncontro;
}

function esVocal(valor) {
    var vocal = new Array('A','E','I','O','U');
    tVocal = vocal.length;
    seEncontro = false;
    for(i=0;i<tVocal;i++)
        if (vocal[i] == valor.toUpperCase()) seEncontro = true;
    return seEncontro;
}


// ------------- VARIABLES Y REDEFINICION DE FUNCIONES ---------------

var PeticionAjax01 = new objetoAjax(); //Definimos un nuevo objetoAjax. 
PeticionAjax01.cargando = requestCargando; //Funci&oacute;n completado del objetoAjax redefinida. 
PeticionAjax01.completado = objetoRequestCompletado01; //Funci&oacute;n completado del objetoAjax redefinida. 

var fresh = new objetoAjax(); //Definimos un nuevo objetoAjax. 
fresh.completado = freshCompletado; //Funci&oacute;n completado del objetoAjax redefinida. 
fresh.cargando = freshcargando; //Funci&oacute;n completado del objetoAjax redefinida. 


// ------------- FUNCIONES COMPLETADO  -------------------
function freshCompletado(estado, estadoTexto, respuestaTexto, respuestaXML) 
{
    document.getElementById('variables').innerHTML = respuestaTexto;
} 

// ------------ FUNCIONES CARGANDO  -----------------------
function requestCargando() {
    document.getElementById('estado').innerHTML = "<img src=\"diseno/loading.gif\">";
}
function freshcargando() {
    document.getElementById('variables').innerHTML = "cargando...";
}



function objetoRequestCompletado01(estado, estadoTexto, respuestaTexto, respuestaXML) 
{
    aRespuesta = respuestaTexto.split('|');
    tRespuesta = aRespuesta.length;
    if (aRespuesta[0] == 1) {  // cuando usuario es correcto
        switch (aRespuesta[2]) {
            case '0':
                alert(aRespuesta[3]);
                location.replace('admin/index.php');
                break;
            case '1':
            case '2':
            case '3':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                var contenedor = document.getElementById('seleccion');
                var seleccion= new AjaxGET();
                seleccion.open("GET", aRespuesta[3]+"?idConsultorio=-1&idServicio=-1&idMedico=-1",true);
                seleccion.onreadystatechange=function()
                {
                    if (seleccion.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
                    {
                        document.getElementById('estado').innerHTML = "";
                        document.getElementById('contenido').innerHTML = '';
                        contenedor.innerHTML = seleccion.responseText;
                        cargarAgenda();
                    }
                }
                seleccion.send(null)
                break;
            case '4':
                alert(aRespuesta[3]);
                location.replace('admin/index.php');
                break;
        }
		
    } else {
        document.getElementById('estado').innerHTML = aRespuesta[3] // cuando usuario no es correcto
    }

/*
	if (respuestaXML.getElementsByTagName("login")[0].getAttribute("estado") == 1) {  // cuando usuario es correcto
		switch (respuestaXML.getElementsByTagName("login")[0].getAttribute("tipoUsuario")) {
			case '0':
				alert(respuestaXML.getElementsByTagName("login")[0].firstChild.nodeValue);
				break;
			case '1':
				var contenedor;
				contenedor = document.getElementById('seleccion');
				seleccion=AjaxGET();
				seleccion.open("GET", respuestaXML.getElementsByTagName("login")[0].firstChild.nodeValue+"?idConsultorio=-1&idServicio=-1&idMedico=-1",true);
				seleccion.onreadystatechange=function()
				{
					if (seleccion.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
					{
						document.getElementById('contenido').innerHTML = '';
						contenedor.innerHTML = seleccion.responseText;
						cargarAgenda();
					}
				}
				seleccion.send(null)
				break;
			case '2':
				alert(respuestaXML.getElementsByTagName("login")[0].firstChild.nodeValue);
				break;
		}
		
//		alert(respuestaXML.getElementsByTagName("login")[0].firstChild.nodeValue);
	} else {
		document.getElementById('estado').innerHTML = respuestaXML.getElementsByTagName("login")[0].firstChild.nodeValue; // cuando usuario no es correcto
	}
	
*/	
} 




function hacerLogin() {
    //	document.getElementById('ingresar')focus();
    PeticionAjax01.coger('loginAcceder.php','usuario='+document.getElementById('usuario').value+'&pass='+document.getElementById('pass').value,true)
}


function refrescarVariables() {
    fresh.coger('variables.php','',true)
}  

function verificarLogin() {   
    var usuario = document.getElementById('usuario').value;
    var pass = document.getElementById('pass').value;
    var correcto = true;
    if (usuario.length < 6) {
        alert('Introduce un Nombre de Usuario Correcto');
        correcto = false;
    }
    if ((pass.length < 6) && (correcto == true)) {
        alert('Introduce una Contrase&ntilde;a Correcta');
        correcto = false;
    }
    if (correcto == true) hacerLogin();
}   
   
function obtenerLogin() {
    var contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "login.php",true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState==2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
	
}

function buscarPor() {
    if (document.getElementById('tipo_busqueda').value == 'nombre') {
        ocultarDiv('buscarPorCedula');
        mostrarDiv('buscarPorNombre');
    } else {
        ocultarDiv('buscarPorNombre');
        mostrarDiv('buscarPorCedula');
    }
}

function citaExtemporanea(fecha) {
    var contenedor2;
    contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "agregarCitaExtemporanea.php?getdate="+fecha,true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
            cargarEstados('estado');
            cargarEstados('estadoAgregar');
            document.getElementById("cod_bar").focus();
			  $(function() {
				$( "#combobox" ).combobox();
				$( "#combobox2" ).combobox();
				$( "#toggle" ).live("click", function() {
				  $( "#combobox" ).toggle();
				});
			  });
        }
        if ((agenda.readyState==1) ||(agenda.readyState==2)||(agenda.readyState==3))
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function reportes() {
    var contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "reportes.php",true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState==2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function reportesCargar(liga) {
    var contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", liga,true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState==2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function validarReporte(liga, idUsuario, idConsultorio, idServicio, idMedico){
    var fechaI = document.getElementById('date1');
    var fechaF = document.getElementById('date2');
    f1=fechaI.value.length;
    f2=fechaF.value.length;
    if (f1==0 || f2==0){
        alert("Ambas fechas deben estar establecidas!");
        return false;
    }
	
    var fechaIn = fechaI.value;
    var fechaFi = fechaF.value;
    var diaIn = fechaIn.substring(0,2); 
    var mesIn = (fechaIn.substring(3,5))*1 - 1;
    var anoIn = fechaIn.substring(6,10); 
    var fechaInU = new Date(anoIn,mesIn,diaIn);
    var diaFi = fechaFi.substring(0,2); 
    var mesFi = (fechaFi.substring(3,5))*1 - 1;
    var anoFi = fechaFi.substring(6,10); 
    var fechaFiU = new Date(anoFi,mesFi,diaFi);
    if(fechaInU > fechaFiU) {
        alert("La fecha final no puede ser menor a la fecha inicial");
    } else {
        var rg1 = document.getElementById('RadioGroup1_0');
        var rg2 = document.getElementById('RadioGroup1_1');
        var rg3 = document.getElementById('RadioGroup1_2');
        var rg4 = document.getElementById('RadioGroup1_3');
        if (rg1.checked) tipoReporte = "unidad";
        if (rg2.checked) tipoReporte = "consultorio";
        if (rg3.checked) tipoReporte = "servicio";
        if (rg4.checked) tipoReporte = "medico";
        window.open(liga+"?idConsultorio="+idConsultorio+"&idServicio="+idServicio+"&idMedico="+idMedico+"&idUsuario="+idUsuario+"&tipoReporte="+tipoReporte+"&fechaI="+fechaIn+"&fechaF="+fechaFi,'_blank');
    }
    return false;
}

function logout() {
    var contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "logout.php",true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            //			contenedor2.innerHTML = agenda.responseText;
            document.getElementById('menu').style.display = "none";
            document.getElementById('seleccion').innerHTML = "&nbsp;";
            obtenerLogin();
        }
        if (agenda.readyState==2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}
function ayuda() {
    var contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "ayuda.php",true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState==2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function buscar() {
    var contenedor2 = document.getElementById('contenido');
    var agenda= new AjaxGET();
    agenda.open("GET", "buscar.php",true);
    agenda.onreadystatechange=function()
    {
        if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
        {
            contenedor2.innerHTML = agenda.responseText;
        }
        if (agenda.readyState==2)
        {
            contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
        }
    }
    agenda.send(null)
}

function verCitasDH () {
    if (!document.getElementById('dh')) {
        alert( "Ingrese la cedula del derechohabiente y haga click en Buscar..." )	;
    } else {
        valor = document.getElementById('dh').options[document.getElementById('dh').selectedIndex].value;
        valor = ponerAcentos(valor);
        if (valor == -1) {
            alert('No existe derechohabiente con la cedula introducida');
        } else {
            aValor = valor.split('|');
            mostrarDiv("citas");
            var contenedor2 = document.getElementById('citas');
            var agenda= new AjaxGET();
            agenda.open("GET", "citasXbusqueda.php?id_derecho="+aValor[0],true);
            agenda.onreadystatechange=function()
            {
                if (agenda.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
                {
                    contenedor2.innerHTML = agenda.responseText;
                }
                if (agenda.readyState==2)
                {
                    contenedor2.innerHTML = "<img src=\"diseno/loading.gif\">";
                }
            }
            agenda.send(null)
        }
    }
}

function borrarReferencias(obj)
{
    if(obj.value=="Tipo de derechohabiente" || obj.value=="Apellido Paterno" || obj.value=="Apellido Materno" || obj.value=="Nombre(s)" || obj.value=="DD/MM/AAAA")
        obj.value="";
}

function ObtenerUnidadesMedicas(estado,elemento)
{
    estado=quitarAcentosUmf(estado);
    var xml=new XMLHttpRequest();
    xml.onreadystatechange=function(){
        if(xml.readyState==4)
        {
            document.getElementById(elemento).innerHTML=ponerAcentos(xml.responseText);
        }
        if(xml.readyState==2)
        {
            document.getElementById(elemento).innerHTML="<img src=\"diseno/loading.gif\">";
        }
    };
    xml.open("GET", "ObtenerUMF.php?estado="+estado, true);
    xml.send();
    
}

function ObtenerUnidadesMedicasConsulta(estado,elemento,umf)
{
    estado=quitarAcentosUmf(estado);
    var xml=new XMLHttpRequest();
    xml.onreadystatechange=function(){
        if(xml.readyState==4)
        {
            document.getElementById(elemento).innerHTML=ponerAcentos(xml.responseText);
        }
        if(xml.readyState==2)
        {
            document.getElementById(elemento).innerHTML="<img src=\"diseno/loading.gif\">";
        }
    };
    xml.open("GET", "ObtenerUMFConsulta.php?estado="+estado+"&unidad="+umf, true);
    xml.send();
    
}

function ObtenerTpoDerXSexo(elemento,elemeto)
{
    ind=document.getElementById(elemento).selectedIndex;
    var sex=document.getElementById(elemento).options[ind].value;
    var tpoCedHom=new Array("10","40","41","50","51","70","71","90");
    var tpoDerHom=new Array("Trabajador","Esposo","Concubino","Padre","Abuelo","Hijo","Hijo de conyuge","Pensionado");
    var tpoCedMuj=new Array("20","30","31","32","60","61","80","81","91");
    var tpoDerMuj=new Array("Trabajadora","Esposa","Concubina","Mujer","Madre","Abuela","Hija","Hija de conyuge","Pensionada");
    var tpoCedGen=new Array("92","99");
    var tpoDerGen=new Array("Familiar de pensionado","No derechohabiente");
    var lista=document.getElementById(elemeto);
    opciones=document.getElementById(elemeto).length;
    vaciarSelect(elemeto);
    opcion=new Option("","-1");
    lista.appendChild(opcion);
    if(sex=="H"){
        for(i=0;i<tpoCedHom.length;i++)
        {
            opcion=new Option(tpoDerHom[i],tpoCedHom[i],"","");
            lista.appendChild(opcion);
        }
    }
    else
    {
        for(i=0;i<tpoCedMuj.length;i++)
        {
            opcion=new Option(tpoDerMuj[i],tpoCedMuj[i],"","");
            lista.appendChild(opcion);
        }
    }
    for(x=0;x<tpoDerGen.length;x++)
    {
        opcion=new Option(tpoDerGen[x],tpoCedGen[x],"","");
        lista.appendChild(opcion);
    }
}

function vaciarSelect(elemeto)
{
    opciones=document.getElementById(elemeto).length;
    lista=document.getElementById(elemeto);
    while(opciones>0){
        lista.options[opciones-1]=null;
        opciones=document.getElementById(elemeto).length;
    }
}

function fechas(elEvento,obj){
    var evento = elEvento || window.event;
    if(evento.keyCode == 8){
    } else {
        var fecha = obj;
        if(fecha.value.length == 2 || fecha.value.length == 5){
            fecha.value += "/";
        }
    }
}

function quitarAcentosUmf(Text)  
{  
    var cadena=""; 
    var codigo="";  
    var temp = "";
    var total = Text.length;
    for (var j = 0; j < total; j++)  
    {  
        var Char=Text.charCodeAt(j);
        var cara=Text.charAt(j);
        if (cara == "&") {
            temp = Text.substring(j,j+8);
            switch (temp) {
                case "&aacute;":
                    cadena += "a";
                    j = j + 7;
                    break;
                case "&Aacute;":
                    cadena += "A";
                    j = j + 7;
                    break;
                case "&eacute;":
                    cadena += "e";
                    j = j + 7;
                    break;
                case "&Eacute;":
                    cadena += "E";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "i";
                    j = j + 7;
                    break;
                case "&iacute;":
                    cadena += "I";
                    j = j + 7;
                    break;
                case "&oacute;":
                    cadena += "o";
                    j = j + 7;
                    break;
                case "&Oacute;":
                    cadena += "O";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "u";
                    j = j + 7;
                    break;
                case "&uacute;":
                    cadena += "U";
                    j = j + 7;
                    break;
                case "&ntilde;":
                    cadena += "n";
                    j = j + 7;
                    break;
                case "&Ntilde;":
                    cadena += "N";
                    j = j + 7;
                    break;
                default:
                    cadena+=Text.charAt(j);  
                    break;  
            }
        } else {
            switch(Char)  
            {  
                case 225:
                    cadena+="a";  
                    break;  
                case 233:
                    cadena+="e";  
                    break;  
                case 237:
                    cadena+="i";  
                    break;  
                case 243:
                    cadena+="o";  
                    break;  
                case 250:
                    cadena+="u";  
                    break;  
                case 193:
                    cadena+="A";  
                    break;  
                case 201:
                    cadena+="E";  
                    break;  
                case 205:
                    cadena+="I";  
                    break;  
                case 211:
                    cadena+="O";  
                    break;  
                case 218:
                    cadena+="U";  
                    break;  
                case 241:
                    cadena+="n";  
                    break;  
                case 209:
                    cadena+="N";  
                    break;  
                default:
                    cadena+=Text.charAt(j);  
                    break;  
            }  
        }
        codigo+="_"+Text.charCodeAt(j);  
    }  
    return cadena;  
}  

function validarFechaCedula(cedula,fechaNac,tipoCed)
{
    
    var ret=false;
    var fechaCed=cedula.toString().substring(4);
    var aFecha=fechaNac.split("/");
    fechaNac=aFecha[2].toString().substring(1)+aFecha[1]+aFecha[0]+"";
    if((tipoCed==60 || tipoCed==61) || (tipoCed==50 || tipoCed==51)){
        if(fechaNac<fechaCed)
            alert("La fecha de Nacimiento debe ser mayor a la del trabajador");
        else
            ret=true;
    }
    else{
        if(fechaNac>fechaCed)
            alert("La fecha de Nacimiento debe ser menor a la del trabajador");
        else
            ret=true;
    }
    
    return ret;
}

function validarCedulaTrabajador(cedula,nombre,app,apm,fecha)
{
    cedula=document.getElementById(cedula);
    var valCedula=cedula.value;  
    var aNombres=nombre.split(" ");
    var nombres,cedulaGen,ap_p,ap_m;
    switch(aNombres[0])
    {
        case "MA":case "MARIA":case "MA.":case "J":case "J.":case "JOSE":case "DA":case "DAS":case "DE":case "DEL":case "DER":case "DI":case "DIE":case "DD":case "EL":case "LA":case "LOS":case "LAS":case "MAC":case "MC":case "VAN":case "VON":case "Y":
            try{
                nombres=aNombres[1];
            }
            catch(ex){
                nombres="X";
            }
            break;
        default:
            nombres=aNombres[0];
    }
    if(app!=undefined){
        var aAp=app.split(" ");
        switch(aAp[0])
        {
            case "DA":case "DAS":case "DE":case "DEL":case "DER":case "DI":case "DIE":case "DD":case "EL":case "LA":case "LOS":case "LAS":case "MAC":case "MC":case "VAN":case "VON":case "Y":
                try{
                    ap_p=aAp[1];
                }
                catch(ex){
                    ap_p="X";
                }
                break;
            default:
                ap_p=aAp[0];
        }
    }
    else
        ap_p=apm;
    if(apm!=undefined){
        var aAm=apm.split(" ");
        switch(aAm[0])
        {
            case "DA":case "DAS":case "DE":case "DEL":case "DER":case "DI":case "DIE":case "DD":case "EL":case "LA":case "LOS":case "LAS":case "MAC":case "MC":case "VAN":case "VON":case "Y":
                try{
                    ap_m=aAm[1];
                }
                catch(ex){
                    ap_m="X";
                }
                break;
            default:
                ap_m=aAm[0];
        }
    }
    else
        ap_m="X";
    var cedulaaP="";
    
    if(ap_p[0]=="Ñ")
        cedulaaP="X";
    else
        cedulaaP=ap_p[0];
    for(var i=1;i<ap_p.length;i++)
    {
        if(esVocal(ap_p[i]))
        {
            cedulaGen=cedulaaP+ap_p[i];
            break;
        }
    }
    
    
    cedulaGen=cedulaGen+ap_m[0]+nombres[0];
    
    for(i=0;i<palInc.length;i++)
    {
        if(palInc[i]==cedulaGen)
        {
            cedulaGen=sustPalInc[i];
            break;
        }
    }
    var iniCed=valCedula.toString().substring(0,4);
    var cedulaAP=cedulaGen;
    var aFecha=fecha.split("/");
    var fechaCedula=valCedula.toString().substring(4);
    var fechaGen=aFecha[2].substr(2,2)+aFecha[1]+aFecha[0];
    cedulaGen+=fechaGen;
    if(valCedula==cedulaGen)
    {
        return true;
    }
    else
    {
        if(cedulaAP!=iniCed && fechaCedula!=fechaGen)
            alert("Cedula incorrecta \n Favor de Verificar");
        else
        if(cedulaAP!=iniCed)
            alert("Nombre o Apellidos incorrectos, \n Favor de verificar");
        else
        if(fechaCedula!=fechaGen)
            alert("Fecha de Nacimiento \u00f3 C\u00e3dula Incorrecta \n Favor de Verificar");        
        return false;
    }
}

function obtenerCodigoPostal(edo,mun,col)
{
    vaciarSelect('cp');
    var contenedor=document.getElementById('cp');
    var municipio=document.getElementById(mun).value;
    var estado=document.getElementById(edo).value;
    var colonia=document.getElementById(col).value;
    var seleccionado=contenedor.value;
    var ajax=new AjaxGET();
    ajax.onreadystatechange=function(){
        if(ajax.readyState==4)
        {
            contenedor.innerHTML=ajax.responseText;
            contenedor.value=seleccionado;
        }
        else
            contenedor.innerHTML="<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET","codigos_postales.php?estado="+quitarAcentos(estado)+"&municipio="+quitarAcentos(municipio)+"&col="+ponerAcentos(colonia),true);
    ajax.send(null);
    document.getElementById('cp').disabled=false;
}

function cargarColonias(colonia,cp,mun,edo)
{
    var codigo=document.getElementById(cp).value;
    var municipio=document.getElementById(mun).value;
    var contenedor=document.getElementById(colonia);
    var estado=document.getElementById(edo).value;
    var seleccionado=document.getElementById(colonia).value;
    vaciarSelect(colonia);
    var ajax=new AjaxGET();
    ajax.onreadystatechange=function() {
        if(ajax.readyState==4)
        {
            contenedor.innerHTML=ajax.responseText;
            contenedor.value=seleccionado;
        }
        else
            contenedor.innerHTML="<img src=\"diseno/loading.gif\">"; 
    }
    ajax.open("GET","mostrarColonias.php?cp="+codigo+"&municipio="+quitarAcentos(municipio)+"&estado="+quitarAcentos(estado));
    ajax.send(null);
}

function cargarColoniasMod(colonia,cp,mun,sel)
{
    var codigo=document.getElementById(cp).value;
    var municipio=document.getElementById(mun).value;
    var contenedor=document.getElementById(colonia);
    var seleccionado=sel;
    vaciarSelect(colonia);
    var ajax=new AjaxGET();
    ajax.onreadystatechange=function() {
        if(ajax.readyState==4)
        {
            contenedor.innerHTML=ajax.responseText;
            contenedor.value=seleccionado;
        }
        else
            contenedor.innerHTML="<img src=\"diseno/loading.gif\">"; 
    }
    ajax.open("GET","mostrarColonias.php?cp="+codigo+"&municipio="+municipio);
    ajax.send(null);
}

function obtenerCodigoPostalMod(edo,mun,col)
{
    vaciarSelect('cp');
    var contenedor=document.getElementById('cp');
    var municipio=mun;
    var estado=edo;
    var colonia=col;
    var seleccionado=contenedor.value;
    var ajax=new AjaxGET();
    ajax.onreadystatechange=function(){
        if(ajax.readyState==4)
        {
            contenedor.innerHTML=ajax.responseText;
            contenedor.value=seleccionado;
        }
        else
            contenedor.innerHTML="<img src=\"diseno/loading.gif\">";
    }
    ajax.open("GET","codigos_postales.php?estado="+estado+"&municipio="+municipio+"&col="+colonia,true);
    ajax.send(null);
    document.getElementById('cp').disabled=false;
}

function calendarios(elemento)
{
    var mes=$("#mes"+elemento+" option:selected").val();
    vaciarSelect("dia"+elemento);
    switch(mes)
    {
        case '01':
            dias=31;
            break;
        case '02':
            anio=document.getElementById('anio'+elemento).value;
            if((anio%4)==0)
                dias=29;
            else
                dias=28;
            break;
        case '03':
            dias=30;
            break;
        case '04':
            dias=30;
            break;
        case '05':
            dias=31;
            break;
        case '06':
            dias=30;
            break;
        case '07':
            dias=31;
            break;
        case '08':
            dias=31;
            break;
        case '09':
            dias=30;
            break;
        case '10':
            dias=31;
            break;
        case '11':
            dias=30;
            break;
        case '12':
            dias=31;
            break;			
    }
    for(i=1;i<=dias;i++)
    {
        if(i<10)
            opcion=new Option(i,"0"+i);
        else
            opcion=new Option(i,i);
        document.getElementById("dia"+elemento).options[i-1]=opcion;
    }
		
}