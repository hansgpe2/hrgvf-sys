<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
  <?php
    include_once('lib/misFunciones.php');
	session_start ();
	$out = "<tr><td align=\"center\"><span class=\"textosParaInputs\">CITAS A REPROGRAMAR:</span><br>";
	$out .= "<select name=\"selectCitaAreprogramar\" id=\"selectCitaAreprogramar\"><option value=\"\" selected></option>";
	$citas = getcitasAreprogramarSin9();
	$totalCitas = count($citas);
	if ($totalCitas > 0) {
		for($i=0;$i<$totalCitas;$i++) {
			$horario = getHorarioEliminadoXid($citas[$i]['id_horario']);
			if ($horario == "") $horario = getHorarioXid($citas[$i]['id_horario']); // buscar horario sin eliminar
			$datosDerecho = getDatosDerecho($citas[$i]['id_derecho']);
			$servicio = getServicioXid($horario['id_servicio']);
			$medico = getMedicoXid($horario['id_medico']);
			$out .= "<option value=\"" . $citas[$i]['id_cita'] . "|" . $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . "|" . $citas[$i]['id_derecho'] . "|" . $datosDerecho['ap_p'] . "|" . $datosDerecho['ap_m'] . "|" . $datosDerecho['nombres'] . "|" . $datosDerecho['telefono'] . "|" . $datosDerecho['direccion'] . "|" . $datosDerecho['estado'] . "|" . $datosDerecho['municipio'] . "|RP\">" . ponerAcentos(strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres'])) . " - " . ponerAcentos($servicio['nombre']) . " - " . formatoDia($citas[$i]['fecha_cita'], 'citasAreprogramar') . " de " . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</option>";
		}
	}
	$out .=	"</select><br><br><td></tr>";


	$out .= "<tr><td align=\"center\"><span class=\"textosParaInputs\">CITAS CON REZAGO A REPROGRAMAR:</span><br>";
	$out .= "<select name=\"selectCitaRezagada\" id=\"selectCitaRezagada\"><option value=\"\" selected></option>";
	$citas = getcitasRezagadasSin9();
	$totalCitas = count($citas);
	if ($totalCitas > 0) {
		for($i=0;$i<$totalCitas;$i++) {
			$horario = getHorarioEliminadoXid($citas[$i]['id_horario']);
			if ($horario == "") $horario = getHorarioXid($citas[$i]['id_horario']); // buscar horario sin eliminar
			$datosDerecho = getDatosDerecho($citas[$i]['id_derecho']);
			$servicio = getServicioXid($horario['id_servicio']);
			$medico = getMedicoXid($horario['id_medico']);
			$out .= "<option value=\"" . $citas[$i]['id_cita'] . "|" . $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . "|" . $citas[$i]['id_derecho'] . "|" . $datosDerecho['ap_p'] . "|" . $datosDerecho['ap_m'] . "|" . $datosDerecho['nombres'] . "|" . $datosDerecho['telefono'] . "|" . $datosDerecho['direccion'] . "|" . $datosDerecho['estado'] . "|" . $datosDerecho['municipio'] . "|RZ\">" . formatoDia($citas[$i]['fecha_cita'], 'citasAreprogramar') . " de " . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) ." - " . ponerAcentos(strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres'])) . " - " . ponerAcentos($servicio['nombre']) . "</option>";
		}
	}
	$out .=	"</select><br><br><td></tr>";
	echo $out;
  ?>
</body>
</html>
