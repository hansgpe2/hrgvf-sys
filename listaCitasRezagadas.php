<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<?php 
	$citas = getcitasRezagadas();
	$totalCitas = count($citas);
	$citasReprogramadas = getcitasReprogramadasRezago();
	$totalCitasReprogramadas = count($citasReprogramadas);
	$out = "";
	$out.= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"470\"><tr><td align=\"right\"><input name=\"cerrarRep\" type=\"button\" value=\"X\" class=\"botones\" onclick=\"javascript: ocultarDiv('listaCitasRezago');\" /></td></tr></table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"470\" class=\"ventana\"><tr><td class=\"tituloVentana\" valign=\"top\">CITAS CON REZAGO</td></tr>";
	if ($totalCitas > 0) {
		for($i=0;$i<$totalCitas;$i++) {
			$textoReprogramada1 = "";
			$textoReprogramada2 = "";
			$horario = getHorarioEliminadoXid($citas[$i]['id_horario']); // buscar horario en los eliminados
			if ($horario == "") $horario = getHorarioXid($citas[$i]['id_horario']); // buscar horario sin eliminar
			$datosDerecho = getDatosDerecho($citas[$i]['id_derecho']);
			$servicio = getServicioXid($horario['id_servicio']);
			$medico = getMedicoXid($horario['id_medico']);
			if($horario['tipo_cita'] == 0) $claseParaDia = "citaXdiaPRV"; 
			if($horario['tipo_cita'] == 1) $claseParaDia = "citaXdiaSUB"; 
			if($horario['tipo_cita'] == 2) $claseParaDia = "citaXdiaPRO";
// checar si ya fue reprogramada			
			if ($totalCitasReprogramadas > 0) {
				for($j=0;$j<$totalCitasReprogramadas;$j++) {
					if ($citas[$i]['id_cita'] == $citasReprogramadas[$j]['id_cita_anterior']) {
						$horario_actual = getHorarioXid($citasReprogramadas[$j]['id_horario']);
						$servicio = getServicioXid($horario_actual['id_servicio']);
						$medico = getMedicoXid($horario_actual['id_medico']);
						$textoReprogramada1 .= "<tr><td colspan=\"2\" align=\"center\" class=\"tituloReprogramada\">------- reprogramada para: -------</td></tr><tr><td colspan=\"2\" valign=\"top\" class=\"tituloReprogramada\">" . formatoDia($citasReprogramadas[$j]['fecha_cita'], 'citasAreprogramar') . " de " . formatoHora($horario_actual['hora_inicio']) . " a " . formatoHora($horario_actual['hora_fin']) . "<br>" . ponerAcentos($servicio['nombre']) . " - " . ponerAcentos(strtoupper($medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>";
					}
				}
			}
// fin checar si ya fue reprogramada			
			
			
			
			$out .= "<tr><td class=\"" . $claseParaDia . "\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tr><td width=\"90\" valign=\"top\" class=\"citaXdiaHora\">" . formatoDia($citas[$i]['fecha_cita'], 'citasAreprogramar') . "<br>" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td><td class=\"citaXdiaHora\">" . ponerAcentos(strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres'])) . " &nbsp;&nbsp;tel: " . $datosDerecho['telefono'] . "<br>" . ponerAcentos($servicio['nombre']) . " - " . ponerAcentos(strtoupper($medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])) . "</td></tr>" . $textoReprogramada1 . "</table></td></tr>";
		}
	}
	$out .= "</table>";
	print($out);
?>
</body>
</html>
