<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include_once('lib/misFunciones.php');
session_start ();

$pisos = getPisos();
$Npisos = count($pisos);
$temp = "<table width=\"100%\" cellspaccing=\"0\" cellpadding=\"0\">";

for($i=0; $i<$Npisos; $i++) {
	$totalCamas = getNcamasXpiso($pisos[$i]['id_piso']);
	$totalCamillas = getNcamillasXpiso($pisos[$i]['id_piso']);
	$camillas = "";
	if($totalCamillas > 0) $camillas = $totalCamillas . " C y/o S";
	$totalCamasOcupadas = getNcamasOcupadasXpiso($pisos[$i]['id_piso']);
	$porcentaje = ($totalCamasOcupadas*100)/$totalCamas;
	$temp .= "<tr><td onmouseover=\"this.className='colorDiv1'\" onmouseout=\"this.className='colorDiv2'\" onclick=\"javascript: infoPiso('" . $pisos[$i]['id_piso'] . "');\">
	<dl>
    <dt>" . $pisos[$i]['nombre'] . "</dt>
    <dd>
        <div style=\"width:" . round($porcentaje) . "%;\"><table width=\"100%\" height=\"100%\"><tr><td><b>" . round($porcentaje) . "%</b></td></tr></table><strong>&nbsp;</strong></div>
    </dd>
	</dl><div class=\"estadisticas\" style=\"position: relative; top:10px;\">&nbsp;&nbsp;&nbsp;" . $totalCamasOcupadas . " de " . $totalCamas . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $camillas . "</div>
	</td>
	</tr>";
}
$temp .= "</table>";

$out = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"center\">" ;
$out.= "<table width=\"550\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">OCUPACION HOSPITALARIA</td>
        </tr>
        <tr>
          <td align=\"left\">" . $temp . "
		  </td>
        </tr>
      </table>
	  </td></tr></table>";
print($out);
?>