<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include_once('lib/misFunciones.php');
session_start ();
$tipoUsuario = $_SESSION['tipoUsuario'];


$piso = getPiso($_GET['id_piso']);
$camas = getCamasXpiso($_GET['id_piso']);

$NCamas = count($camas);
$temp = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";


for($i=0; $i<$NCamas; $i++) {
	$infoPaciente = "";
	$boton = "";
	if ($camas[$i]['status'] == "1") {
		if(($tipoUsuario == "2") && ($_GET['id_piso'] == "1")) { // urgencias
			$boton .= "onclick=\"javascript: bajaPaciente('" . $camas[$i]['id_cama'] . "');\"";
		}
		if(($tipoUsuario == "1") && ($_GET['id_piso'] != "1")) { // pisos
			$boton .= "onclick=\"javascript: bajaPaciente('" . $camas[$i]['id_cama'] . "');\"";
		}		
		if(($tipoUsuario == "9")) { // pisos y urgencias
			$boton .= "onclick=\"javascript: bajaPaciente('" . $camas[$i]['id_cama'] . "');\"";
		}		
		$datos = getDatosPacienteEnCama($camas[$i]['id_cama']);
		$infoPaciente = "<td valign=\"middle\" class=\"nombrePaciente\" width=\"450\">" . ponerAcentos($datos['ap_p'] . " " . $datos['ap_m'] . " " . $datos['nombres']) . "<br>" . ponerAcentos($datos['servicio_nombre'] . " - " . $datos['medico_titulo'] . " " . $datos['medico_ap_p'] . " " . $datos['medico_ap_m'] . " " . $datos['medico_nombres']) . "</td><td valign=\"middle\" class=\"nombrePaciente\" width=\"90\"><b>INGRESO</b><br>" . formatoDia($datos['fecha_ingreso'],"fecha") . "<br>" . formatoHora($datos['hora_ingreso']) . " HRS.</td><td valign=\"middle\" class=\"nombrePaciente\" width=\"50\" align=\"center\">" . getDiasEstancia($datos['fecha_ingreso'], $datos['hora_ingreso'], $_GET['id_piso']) . "</td><td align=\"center\"><img src=\"diseno/__camaBaja.png\"width=\"42\"></td>";
		
		$temp .= "<tr onmouseover=\"this.className='colorDiv1'; mostrarDiv('obs" . $i . "');\" onmouseout=\"this.className='colorDiv2'; ocultarDiv('obs" . $i . "');\" " . $boton . "><td class=\"cama\" width=\"80\">" . $camas[$i]['descripcion'] . "<div id=\"obs" . $i . "\" style=\"position:absolute; margin-left:300px; margin-top:-40px; display:none;\">
		<table width=\"400\" class=\"ventanaConFondo\">
			<tr>
			  <td class=\"tituloVentana\" height=\"23\">OBSERVACIONES</td>
			</tr>
			<tr>
				<td class=\"nombrePaciente\">" . ponerAcentos($datos['observaciones']) . "
				</td>
			</tr>
		</table>
		</div></td>" . $infoPaciente . "
		</td>
		</tr>";
	} else if ($camas[$i]['status'] == "0") {
		if(($tipoUsuario == "2") && ($_GET['id_piso'] == "1")) {
			$boton .= "onclick=\"javascript: altaPaciente('" . $camas[$i]['id_cama'] . "');\"";
		}
		if(($tipoUsuario == "1") && ($_GET['id_piso'] != "1")) {
			$boton .= "onclick=\"javascript: altaPaciente('" . $camas[$i]['id_cama'] . "');\"";
		}
		if(($tipoUsuario == "9")) {
			$boton .= "onclick=\"javascript: altaPaciente('" . $camas[$i]['id_cama'] . "');\"";
		}
		$infoPaciente = "<td valign=\"middle\" class=\"camaDisponible\" colspan=\"3\" width=\"590\">DISPONIBLE</td><td align=\"center\"><img src=\"diseno/__camaAlta.png\"width=\"42\"></td>";
		$temp .= "<tr onmouseover=\"this.className='colorDiv1'\" onmouseout=\"this.className='colorDiv2'\" " . $boton . "><td class=\"cama\" width=\"80\">" . $camas[$i]['descripcion'] . "</td>" . $infoPaciente . "
		</td>
		</tr>";
	}
}
$temp .= "</table>";
$out = "";

$listaCamillas = "<option value=\"0\"> </option>";
$camillasVacias = getCamillasVacias();
$tCamillasVacias = count($camillasVacias);
for ($i=0; $i<$tCamillasVacias; $i++) {
	$listaCamillas .= "<option value=\"" . $camillasVacias[$i]['id_cama'] . "\">" . $camillasVacias[$i]['descripcion'] . "</option>";
}

if((($tipoUsuario == "2") || ($tipoUsuario == "9")) && ($_GET['id_piso'] == "1")) {

	$out.= "<input name=\"agregar\" id=\"agregar\" value=\"Administrar Camillas\" type=\"button\" class=\"botones\" onclick=\"javascript: mostrarDiv('pedirPass'); document.getElementById('pass').value = '';\" /><br><br>
			<div id=\"pedirPass\" style=\"display:none;\">
				<form id=\"formaCita2\" method=\"POST\" action=\"javascript: validarPassUrgencias();\">
				
				<table width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
				  <tr>
					<td colspan=\"2\" class=\"tituloVentana\">ADMINISTRAR CAMILLAS</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td height=\"25\" class=\"textosParaInputs\" align=\"right\" width=\"250\">INTRODUCE TU CONTRASE&Ntilde;A: </td>
					<td align=\"left\" width=\"50\"><input name=\"pass\" id=\"pass\" type=\"password\" />
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan=\"2\" align=\"center\">
				  <input type=\"button\" name=\"regresar\" id=\"regresar\" value=\"Cerrar\" class=\"botones\"  onclick=\"javascript: ocultarDiv('pedirPass');\" />&nbsp;&nbsp;&nbsp;&nbsp;
				  <input type=\"submit\" name=\"agregar\" id=\"agregar\" value=\"Continuar\" class=\"botones\" />
				  </tr>
				</table>
				</form>
			</div>
			<div id=\"menuUrgencias\" style=\"display:none;\">
				<form id=\"formaCita3\" method=\"POST\" action=\"javascript: validarPassUrgencias();\">
				
				<table width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
				  <tr>
					<td colspan=\"2\" class=\"tituloVentana\">ADMINISTRAR CAMILLAS</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan=\"2\" align=\"center\">
				  <input type=\"button\" name=\"regresar2\" id=\"regresar2\" value=\"Cerrar\" class=\"botones\"  onclick=\"javascript: ocultarDiv('menuUrgencias');\" />&nbsp;&nbsp;&nbsp;&nbsp;
				  <input type=\"button\" name=\"agregar2\" id=\"agregar2\" value=\"Agregar Camilla\" class=\"botones\" onclick=\"javascript: ocultarDiv('eliminarCama'); mostrarDiv('agregarCama');\" />&nbsp;&nbsp;&nbsp;&nbsp;
				  <input type=\"button\" name=\"eliminar2\" id=\"eliminar2\" value=\"Eliminar Camilla\" class=\"botones\" onclick=\"javascript: ocultarDiv('agregarCama'); mostrarDiv('eliminarCama');\" />
				  </tr>
				</table>
				</form>
			</div>
			<div id=\"agregarCama\" style=\"display:none;\">
				<form id=\"formaCita4\" method=\"POST\" action=\"javascript: validarAgregarCamilla();\">
				
				<table width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
				  <tr>
					<td colspan=\"2\" class=\"tituloVentana\">AGREGAR CAMILLA</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td height=\"25\" class=\"textosParaInputs\" align=\"right\" width=\"250\">INTRODUCE EL N. DE CAMILLA (EJ. 5C): </td>
					<td align=\"left\" width=\"50\"><input name=\"cami\" id=\"cami\" type=\"text\" />
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan=\"2\" align=\"center\">
				  <input type=\"button\" name=\"regresar4\" id=\"regresar4\" value=\"Cerrar\" class=\"botones\"  onclick=\"javascript: ocultarDiv('agregarCama');\" />&nbsp;&nbsp;&nbsp;&nbsp;
				  <input type=\"submit\" name=\"agregar4\" id=\"agregar4\" value=\"Agregar Camilla\" class=\"botones\" />
				  </tr>
				</table>
				</form>
			</div>
			<div id=\"eliminarCama\" style=\"display:none;\">
				<form id=\"formaCita5\" method=\"POST\" action=\"javascript: validarEliminarCamilla();\">
				
				<table width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
				  <tr>
					<td colspan=\"2\" class=\"tituloVentana\">ELIMINAR CAMILLA</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td height=\"25\" class=\"textosParaInputs\" align=\"right\" width=\"250\">SELECCIONA EL N. DE CAMILLA: </td>
					<td align=\"left\" width=\"50\"><select name=\"camiEliminar\" id=\"camiEliminar\">" . $listaCamillas . "</select>
					</td>
				  </tr>
				  <tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td colspan=\"2\" align=\"center\">
				  <input type=\"button\" name=\"regresar5\" id=\"regresar5\" value=\"Cerrar\" class=\"botones\"  onclick=\"javascript: ocultarDiv('eliminarCama');\" />&nbsp;&nbsp;&nbsp;&nbsp;
				  <input type=\"submit\" name=\"agregar5\" id=\"agregar5\" value=\"Eliminar Camilla\" class=\"botones\" />
				  </tr>
				</table>
				</form>
			</div>
			<div id=\"cargando\"></div>";
}
$out.= "<br><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"center\">" ;
$out.= "<table width=\"750\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">" . $piso['nombre'] . " - OCUPACION</td>
        </tr>
        <tr>
          <td align=\"left\">" . $temp . "
		  </td>
        </tr>
      </table><br><input type=\"button\" name=\"regresar\" id=\"regresar\" value=\"Regresar\" class=\"botones\"  onclick=\"javascript: inicio('inicio.php');\" /><br><br>
	  </td></tr></table>";
print($out);
?>