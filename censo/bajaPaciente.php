<?php
session_start ();
include_once('lib/misFunciones.php');
$datos = getDatosPacienteEnCama($_GET['id_cama']);
$datos_cama = getDatosCama($_GET['id_cama']);
$datos_piso = getPiso($datos_cama['id_piso']);
$servicios = listaServicios();

$pisos = getPisos();
$tPisos = count($pisos);
$listaPisos = "<option value= \"0\"> </option>";
for ($i=0; $i<$tPisos; $i++) {
	$listaPisos .= "<option value= \"" . $pisos[$i]['id_piso'] . "\">" . $pisos[$i]['nombre'] . "</option>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarBajaPaciente('<?php echo $datos_cama['id_piso'] ?>','<?php echo $_GET['id_cama'] ?>');">

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">SALIDA DE PACIENTE EN <?php echo $datos_piso['nombre'] ?> - <?php echo $datos_cama['descripcion'] ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left" colspan="3"><?php echo $datos['cedula'] . "/" . $datos['cedula_tipo'] ?>
    </td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><?php echo ponerAcentos($datos['ap_p'] . " " . $datos['ap_m'] . " " . $datos['nombres']) ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right" valign="top">SERVICIO:</td>
    <td><?php echo ponerAcentos($datos['servicio_nombre']) ?><br /><input type="button" name="modifica" id="modifica" value="Modificar Servicio" class="botones"  onclick="javascript: mostrarDiv('modificarServicio');" />
    <br />
    <div id="modificarServicio" style="display:none;">
    	<br />
		<table width="300" border="0" cellspacing="0" cellpadding="0" class="ventana">
          <tr>
            <td colspan="2" class="tituloVentana">MODIFICAR SERVICIO</td>
          </tr>
          <tr>
            <td class="textosParaInputs" align="right">SERVICIO:</td>
            <td><select name="servicio" id="servicio" onchange="javascript:opcionesMedicosReceta(this);"><?php echo $servicios; ?>
            </select></td>
          </tr>
          <tr>
            <td class="textosParaInputs" align="right">MEDICO:</td>
            <td><div id="medicos"><select name="medico" id="medico"><option value="0"> </option></select></div></td>
          </tr>
          <tr>
            <td colspan="2" align="center">
          <input type="button" name="cerrar" id="cerrar" value="Cancelar" class="botones" onclick="javascript: ocultarDiv('modificarServicio');" />&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="button" name="modificar" id="modificar" value="Modificar" class="botones" onclick="javascript: modificarServicio('<?php echo $datos_cama['id_piso'] ?>','<?php echo $_GET['id_cama'] ?>');" />
          <br /><div id="enviandoModificacion">&nbsp;</div></td>
          </tr>
        </table>
        <br />
    </div>
    </td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MEDICO:</td>
    <td><?php echo ponerAcentos($datos['medico_titulo'] . " " . $datos['medico_ap_p'] . " " . $datos['medico_ap_m'] . " " . $datos['medico_nombres']) ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO ORIGEN:</td>
    <td><?php echo ponerAcentos($datos['procedencia']) ?>
  </td>
  </tr>
  <tr>
  	<td class="textosParaInputs" align="right">FECHA DE INGRESO: </td>
    <td><?php echo formatoDia($datos['fecha_ingreso'],"fechaI") ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">FECHA DE SALIDA:</td>
    <td><input name="fecha" type="text" id="fecha" maxlength="10" value="<?php echo date('d/m/Y') ?>" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">TIPO:</td>
    <td colspan="3"><select name="tipo" id="tipo">
      <option value="0"> </option>
      <option value="CURACION">CURACION</option>
      <option value="MEJORIA">MEJORIA</option>
      <option value="SIN MEJORIA">SIN MEJORIA</option>
      <option value="DEFUNCION">DEFUNCION</option>
      <option value="PASE">PASE A OTRA UNIDAD</option>
      <option value="ALTA">ALTA VOLUNTARIA</option>
      <option value="CANCELACION">CANCELACION DE CIRUGIA</option>
      <option value="OTRO">OTRO MOTIVO</option>
    </select></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">OBSERVACIONES:</td>
    <td colspan="3"><textarea name="observaciones" cols="40" rows="4" id="observaciones"></textarea></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: infoPiso('<?php echo $datos_cama['id_piso'] ?>');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Continuar" class="botones" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>
<br /><br />
<form id="formaCita2" method="POST" action="javascript: validarTransferencia('<?php echo $datos_cama['id_piso'] ?>','<?php echo $_GET['id_cama'] ?>','<?php echo $datos['id_derecho'] ?>');">

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="4" class="tituloVentana">TRANSFERIR PACIENTE A:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right" width="50">PISO: </td>
    <td align="left" width="50"><select id="piso" name="piso" onchange="javascript:listaCamasVacias(this);"><?php echo $listaPisos ?></select>
    </td>
    <td class="textosParaInputs" align="right" width="50">CAMA: </td>
    <td align="left"><div id="camas"><select name="cama" id="cama"><option value="0"> </option></select></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: infoPiso('<?php echo $datos_cama['id_piso'] ?>');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Transferir" class="botones" />
  <div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>

<br /><br />
<form id="modificar_obs" method="POST" action="javascript: validarModificarObs('<?php echo $_GET['id_cama'] ?>', '<?php echo $datos_cama['id_piso'] ?>');">

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="4" class="tituloVentana">MODIFICAR OBSERVACIONES A PACIENTE:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right" width="50">OBSERVACIONES: </td>
    <td align="left" width="50">
    		<textarea id="observaciones_mod" name="observaciones_mod" cols="40" rows="4"><?php echo $datos["observaciones"]; ?></textarea>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: infoPiso('<?php echo $datos_cama['id_piso'] ?>');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Modificar" class="botones" />
  <div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>

</body>
</html>
