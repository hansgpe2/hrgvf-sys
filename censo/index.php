<?php
session_start ();
$_SESSION['idUsuario'] = "-1";
$_SESSION['tipoUsuario'] = "-1";
$_SESSION['IdCon'] = "-1";
$_SESSION['idServ'] = "-1";
$_SESSION['idDr'] = "-1";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Censo Hospitalario</title>

<link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
<link href="lib/login.css" rel="stylesheet" type="text/css" />
<link href="lib/impresion.css" media="print" rel="stylesheet" type="text/css" />
<script src="lib/jquery-1.7.1.js" type="text/javascript"></script>
</head>

<body bgcolor="#EEEEEE" onload="javascript: obtenerLogin();">
<center>
<script language="javascript" type="text/javascript">
	document.write("<script type='text/javascript' src='lib/arreglos.js'></script"+">");
</script>
  <table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="diseno/logoEncabezado.jpg" width="104" height="108" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Censo Hospitalario&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    
    <table id="centro" class="centro" width="800" height="499">
    <tr><td align="center" valign="top">
      <table width="800">
      	<tr><td><div id="menu" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicio.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="250"><a href="javascript: buscar();" title="Buscar Paciente" class="botones_menu"><img src="diseno/buscar.png" width="40" height="40" border="0" /><br>Buscar Paciente</a></td>
                    	<td width="100" align="center"><a href="javascript: reportes();" title="Reportes" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reportes</a></td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
        </td></tr>
        <tr><td align="center"><br />
		      	<div id="contenido">
      			</div>
      	</td></tr>
      </table>
    </td></tr></table>
</td></tr></table>
</center>
</body>
</html>
