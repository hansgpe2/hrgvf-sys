<?php session_start ();
require_once("../lib/funcionesAdmin.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>PROMEDIO DE CITAS OTORGADAS</title>
<style type="text/css">
	@import url("../lib/impresion.css") print;
	@import url("../lib/reportes.css") screen;
</style>
</head>
</head>

<body>

<?php // reporte por medico

if ($_GET['tipoReporte'] == 'medico') {
	$unix_time_fechaI = strtotime($_GET['fechaI']);
	$unix_time_fechaF = strtotime($_GET['fechaF']);
	$tomorrows_date = date( "Ymd", strtotime("+1 day",  $unix_time_fechaI));
	$dias = (((($unix_time_fechaF - $unix_time_fechaI)/60)/60)/24)+1;
	$unix_time_temp = strtotime($_GET['fechaI']);
	$tomorrows_date = date( "Ymd", strtotime($_GET['fechaI']));
	$prv = 0;
	$sub = 0;
	$pro = 0;
	$tot = 0;
	$prvO = 0;
	$subO = 0;
	$proO = 0;
	$totO = 0;
	$citasCanceladas = 0;
	$diasLab = 0;
	$razonCancel = "";
	for($i=0;$i<$dias;$i++) {
		$datosMedico = getMedicoXid($_GET['idMedico']);
		$datosServicio = getServicioXid($_GET['idServicio']);
//		$datosConsultorio = getConsultorioXid($_GET['idConsultorio']);
		$fecha = $tomorrows_date;
		$claveDelMedico = $datosMedico['n_empleado'];
		$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
		$tipoMedico = $datosMedico['tipo_medico'];
		$especialidad = ponerAcentos($datosServicio['nombre']);
		$nombre = $nombreDelMedico . "<br><span class=\"contenido8bold\">" . $especialidad . "</span>";
		$horario = formatoHora($datosMedico['hora_entrada']) . "a" . formatoHora($datosMedico['hora_salida']);
		$n_horas = diferencia_horas(formatoHora($datosMedico['hora_salida']),formatoHora($datosMedico['hora_entrada']));
		$citasXdia = citasXdia($fecha,$_SESSION['id_usuario'],$_GET['idConsultorio'],$_GET['idServicio'],$_GET['idMedico']);
		$nCitas = count($citasXdia);
		if ($nCitas > 0) $diasLab++;
		$tot += $nCitas;
		for($n=0;$n<$nCitas;$n++) {
			if ($citasXdia[$n]['tipo_cita'] == 0) $prv++;
			else if ($citasXdia[$n]['tipo_cita'] == 1) $sub++;
			else if ($citasXdia[$n]['tipo_cita'] == 2) $pro++;
			$existeCita = existeCita($fecha,$citasXdia[$n]['id_horario']);
			if($existeCita) {
				if ($citasXdia[$n]['tipo_cita'] == 0) $prvO++;
				else if ($citasXdia[$n]['tipo_cita'] == 1) $subO++;
				else if ($citasXdia[$n]['tipo_cita'] == 2) $proO++;
				$totO++;
			}
			$canceladas = citasCanceladas($fecha,$citasXdia[$n]['id_horario']);
			$citasCanceladas += $canceladas[0];
			$razonCancel .= $canceladas[1];
		}	
		// siguiente dia
		$tomorrows_date = date( "Ymd", strtotime("+1 day",  $unix_time_temp));
		$unix_time_temp = strtotime($tomorrows_date);
	}
	$promedio = number_format($totO / $tot,2);
	$out = "<tr>
				<td align=\"left\" class=\"contenido8\">" . $nombre . "</td>
				<td align=\"left\" class=\"contenido8\">" . $claveDelMedico . "</td>
				<td align=\"left\" class=\"contenido8\">" . $tipoMedico . "</td>
				<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
				<td align=\"center\" class=\"contenido8\">" . $n_horas  . "</td>
				<td align=\"center\" class=\"contenido8\">" . $diasLab  . "</td>
				<td align=\"center\" class=\"contenido8\">" . $prvO . "/". $prv  . "</td>
				<td align=\"center\" class=\"contenido8\">" . $subO . "/" . $sub  . "</td>
				<td align=\"center\" class=\"contenido8\">" . $proO . "/" . $pro  . "</td>
				<td align=\"center\" class=\"contenido8\">" . $totO . "/" . $tot  . "</td>
				<td align=\"center\" class=\"contenido8bold\">" . $promedio  . "</td>
				<td align=\"center\" class=\"contenido8\">" . $citasCanceladas  . "</td>
				<td align=\"left\" class=\"contenido8\">" . $razonCancel  . "</td>
			</tr>";
	$fechaInicio = $_GET['fechaI'];
	$fechaFin = $_GET['fechaF'];
	$claveUnidadMedica = "&nbsp;";
	$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
	$delegacion = "DELEGACION ESTATAL EN JALISCO";
	$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
        <td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
          Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
        <td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />PROMEDIO DE CITAS OTORGADAS</td>
        <td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
            <td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
          </tr>
          <tr>
            <td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
            <td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
    <td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
    <td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
    <td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
					    <td colspan=\"2\" align=\"center\" class=\"contenido8bold\">M&eacute;dico que Otorga</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horario</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horas de Cons.</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">D&iacute;as Lab.</td>
						<td colspan=\"6\" align=\"center\" class=\"contenido8bold\">Consultas Otorgadas/Asignadas</td>
						<td rowspan=\"2\" align=\"center\" class=\"contenido8bold\">Motivo de Ausencia</td>
					  </tr>
					  <tr>
						<td width=\"350\" align=\"center\" class=\"contenido8bold\">Nombre</td>
						<td width=\"70\" align=\"center\" class=\"contenido8bold\">N. Empleado</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">PRV</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">SUB</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">PRO</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Total</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Prom.</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Cancel.</td>
					  </tr>" . $out . "
					</table></td>
				  </tr>
				</table>";
	echo $reporte;
}
// reporte por servicio

if ($_GET['tipoReporte'] == 'servicio') {
	$out = "";
	$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$_GET['idServicio']);
	$totalMedicos = count($medicos);
	for ($x=0; $x<$totalMedicos; $x++) {
		$unix_time_fechaI = strtotime($_GET['fechaI']);
		$unix_time_fechaF = strtotime($_GET['fechaF']);
		$tomorrows_date = date( "Ymd", strtotime("+1 day",  $unix_time_fechaI));
		$dias = (((($unix_time_fechaF - $unix_time_fechaI)/60)/60)/24)+1;
		$unix_time_temp = strtotime($_GET['fechaI']);
		$tomorrows_date = date( "Ymd", strtotime($_GET['fechaI']));
		$prv = 0;
		$sub = 0;
		$pro = 0;
		$tot = 0;
		$prvO = 0;
		$subO = 0;
		$proO = 0;
		$totO = 0;
		$citasCanceladas = 0;
		$diasLab = 0;
		$razonCancel = "";
		for($i=0;$i<$dias;$i++) {
			$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
			$datosServicio = getServicioXid($_GET['idServicio']);
	//		$datosConsultorio = getConsultorioXid($_GET['idConsultorio']);
			$fecha = $tomorrows_date;
			$claveDelMedico = $datosMedico['n_empleado'];
			$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
			$tipoMedico = $datosMedico['tipo_medico'];
			$especialidad = ponerAcentos($datosServicio['nombre']);
			$nombre = $nombreDelMedico . "<br><span class=\"contenido8bold\">" . $especialidad . "</span>";
			$horario = formatoHora($datosMedico['hora_entrada']) . "a" . formatoHora($datosMedico['hora_salida']);
			$n_horas = diferencia_horas(formatoHora($datosMedico['hora_salida']),formatoHora($datosMedico['hora_entrada']));
			$citasXdia = citasXdia($fecha,$_SESSION['id_usuario'],$_GET['idConsultorio'],$_GET['idServicio'],$medicos[$x]['id_medico']);
			$nCitas = count($citasXdia);
			if ($nCitas > 0) $diasLab++;
			$tot += $nCitas;
			for($n=0;$n<$nCitas;$n++) {
				if ($citasXdia[$n]['tipo_cita'] == 0) $prv++;
				else if ($citasXdia[$n]['tipo_cita'] == 1) $sub++;
				else if ($citasXdia[$n]['tipo_cita'] == 2) $pro++;
				$existeCita = existeCita($fecha,$citasXdia[$n]['id_horario']);
				if($existeCita) {
					if ($citasXdia[$n]['tipo_cita'] == 0) $prvO++;
					else if ($citasXdia[$n]['tipo_cita'] == 1) $subO++;
					else if ($citasXdia[$n]['tipo_cita'] == 2) $proO++;
					$totO++;
				}
				$canceladas = citasCanceladas($fecha,$citasXdia[$n]['id_horario']);
				$citasCanceladas += $canceladas[0];
				$razonCancel .= $canceladas[1];
			}	
			// siguiente dia
			$tomorrows_date = date( "Ymd", strtotime("+1 day",  $unix_time_temp));
			$unix_time_temp = strtotime($tomorrows_date);
		}
		$promedio = number_format($totO / $tot,2);
		$out .= "<tr>
					<td align=\"left\" class=\"contenido8\">" . $nombre . "</td>
					<td align=\"left\" class=\"contenido8\">" . $claveDelMedico . "</td>
					<td align=\"left\" class=\"contenido8\">" . $tipoMedico . "</td>
					<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
					<td align=\"center\" class=\"contenido8\">" . $n_horas  . "</td>
					<td align=\"center\" class=\"contenido8\">" . $diasLab  . "</td>
					<td align=\"center\" class=\"contenido8\">" . $prvO . "/". $prv  . "</td>
					<td align=\"center\" class=\"contenido8\">" . $subO . "/" . $sub  . "</td>
					<td align=\"center\" class=\"contenido8\">" . $proO . "/" . $pro  . "</td>
					<td align=\"center\" class=\"contenido8\">" . $totO . "/" . $tot  . "</td>
					<td align=\"center\" class=\"contenido8bold\">" . $promedio  . "</td>
					<td align=\"center\" class=\"contenido8\">" . $citasCanceladas  . "</td>
					<td align=\"left\" class=\"contenido8\">" . $razonCancel  . "</td>
				</tr>";
		$fechaInicio = $_GET['fechaI'];
		$fechaFin = $_GET['fechaF'];
		$claveUnidadMedica = "&nbsp;";
		$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
		$delegacion = "DELEGACION ESTATAL EN JALISCO";
		$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
	}
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
	<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	  <tr>
		<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
		<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
		  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
		<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />PROMEDIO DE CITAS OTORGADAS</td>
		<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
			<td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
		  </tr>
		  <tr>
			<td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
			<td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
		  </tr>
		</table></td>
	  </tr>
	</table></td>
  </tr>
  <tr>
	<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
	<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
	<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
	<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">M&eacute;dico que Otorga</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horario</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horas de Cons.</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">D&iacute;as Lab.</td>
						<td colspan=\"6\" align=\"center\" class=\"contenido8bold\">Consultas Otorgadas/Asignadas</td>
						<td rowspan=\"2\" align=\"center\" class=\"contenido8bold\">Motivo de Ausencia</td>
					  </tr>
					  <tr>
						<td width=\"350\" align=\"center\" class=\"contenido8bold\">Nombre</td>
						<td width=\"70\" align=\"center\" class=\"contenido8bold\">N. Empleado</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">PRV</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">SUB</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">PRO</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Total</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Prom.</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Cancel.</td>
					  </tr>" . $out . "
					</table></td>
				  </tr>
				</table>";
	echo $reporte;
}

// reporte por consultorio

if ($_GET['tipoReporte'] == 'consultorio') {
	$out = "";
	$servicios = getServiciosXConsultorio($_GET['idConsultorio']);
	$totalServicios = count($servicios);
	for ($y=0; $y<$totalServicios; $y++) {
		$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$servicios[$y]['id_servicio']);
		$totalMedicos = count($medicos);
		for ($x=0; $x<$totalMedicos; $x++) {
			$unix_time_fechaI = strtotime($_GET['fechaI']);
			$unix_time_fechaF = strtotime($_GET['fechaF']);
			$tomorrows_date = date( "Ymd", strtotime("+1 day",  $unix_time_fechaI));
			$dias = (((($unix_time_fechaF - $unix_time_fechaI)/60)/60)/24)+1;
			$unix_time_temp = strtotime($_GET['fechaI']);
			$tomorrows_date = date( "Ymd", strtotime($_GET['fechaI']));
			$prv = 0;
			$sub = 0;
			$pro = 0;
			$tot = 0;
			$prvO = 0;
			$subO = 0;
			$proO = 0;
			$totO = 0;
			$citasCanceladas = 0;
			$diasLab = 0;
			$razonCancel = "";
			for($i=0;$i<$dias;$i++) {
				$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
				$datosServicio = getServicioXid($servicios[$y]['id_servicio']);
		//		$datosConsultorio = getConsultorioXid($_GET['idConsultorio']);
				$fecha = $tomorrows_date;
				$claveDelMedico = $datosMedico['n_empleado'];
				$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
				$tipoMedico = $datosMedico['tipo_medico'];
				$especialidad = ponerAcentos($datosServicio['nombre']);
				$nombre = $nombreDelMedico . "<br><span class=\"contenido8bold\">" . $especialidad . "</span>";
				$horario = formatoHora($datosMedico['hora_entrada']) . "a" . formatoHora($datosMedico['hora_salida']);
				$n_horas = diferencia_horas(formatoHora($datosMedico['hora_salida']),formatoHora($datosMedico['hora_entrada']));
				$citasXdia = citasXdia($fecha,$_SESSION['id_usuario'],$_GET['idConsultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
				$nCitas = count($citasXdia);
				if ($nCitas > 0) $diasLab++;
				$tot += $nCitas;
				for($n=0;$n<$nCitas;$n++) {
					if ($citasXdia[$n]['tipo_cita'] == 0) $prv++;
					else if ($citasXdia[$n]['tipo_cita'] == 1) $sub++;
					else if ($citasXdia[$n]['tipo_cita'] == 2) $pro++;
					$existeCita = existeCita($fecha,$citasXdia[$n]['id_horario']);
					if($existeCita) {
						if ($citasXdia[$n]['tipo_cita'] == 0) $prvO++;
						else if ($citasXdia[$n]['tipo_cita'] == 1) $subO++;
						else if ($citasXdia[$n]['tipo_cita'] == 2) $proO++;
						$totO++;
					}
					$canceladas = citasCanceladas($fecha,$citasXdia[$n]['id_horario']);
					$citasCanceladas += $canceladas[0];
					$razonCancel .= $canceladas[1];
				}	
				// siguiente dia
				$tomorrows_date = date( "Ymd", strtotime("+1 day",  $unix_time_temp));
				$unix_time_temp = strtotime($tomorrows_date);
			}
			$promedio = number_format($totO / $tot,2);
			$out .= "<tr>
						<td align=\"left\" class=\"contenido8\">" . $nombre . "</td>
						<td align=\"left\" class=\"contenido8\">" . $claveDelMedico . "</td>
						<td align=\"left\" class=\"contenido8\">" . $tipoMedico . "</td>
						<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
						<td align=\"center\" class=\"contenido8\">" . $n_horas  . "</td>
						<td align=\"center\" class=\"contenido8\">" . $diasLab  . "</td>
						<td align=\"center\" class=\"contenido8\">" . $prvO . "/". $prv  . "</td>
						<td align=\"center\" class=\"contenido8\">" . $subO . "/" . $sub  . "</td>
						<td align=\"center\" class=\"contenido8\">" . $proO . "/" . $pro  . "</td>
						<td align=\"center\" class=\"contenido8\">" . $totO . "/" . $tot  . "</td>
						<td align=\"center\" class=\"contenido8bold\">" . $promedio  . "</td>
						<td align=\"center\" class=\"contenido8\">" . $citasCanceladas  . "</td>
						<td align=\"left\" class=\"contenido8\">" . $razonCancel  . "</td>
					</tr>";
			$fechaInicio = $_GET['fechaI'];
			$fechaFin = $_GET['fechaF'];
			$claveUnidadMedica = "&nbsp;";
			$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
			$delegacion = "DELEGACION ESTATAL EN JALISCO";
			$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
		}
	}
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
	<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	  <tr>
		<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
		<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
		  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
		<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />PROMEDIO DE CITAS OTORGADAS</td>
		<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
			<td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
		  </tr>
		  <tr>
			<td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
			<td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
		  </tr>
		</table></td>
	  </tr>
	</table></td>
  </tr>
  <tr>
	<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
	<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
	<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
	<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">M&eacute;dico que Otorga</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horario</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horas de Cons.</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">D&iacute;as Lab.</td>
						<td colspan=\"6\" align=\"center\" class=\"contenido8bold\">Consultas Otorgadas/Asignadas</td>
						<td rowspan=\"2\" align=\"center\" class=\"contenido8bold\">Motivo de Ausencia</td>
					  </tr>
					  <tr>
						<td width=\"350\" align=\"center\" class=\"contenido8bold\">Nombre</td>
						<td width=\"70\" align=\"center\" class=\"contenido8bold\">N. Empleado</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">PRV</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">SUB</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">PRO</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Total</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Prom.</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Cancel.</td>
					  </tr>" . $out . "
					</table></td>
				  </tr>
				</table>";
	echo $reporte;
}

if ($_GET['tipoReporte'] == 'unidad') {
	$consultorios = getConsultorios();
	$totalConsultorios = count($consultorios);
	for($z=0;$z<$totalConsultorios; $z++) {
		$servicios = getServiciosXConsultorio($consultorios[$z]['id_consultorio']);
		$totalServicios = count($servicios);
		for ($y=0; $y<$totalServicios; $y++) {
			$medicos = getMedicosXServicioXConsultorio($consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio']);
			$totalMedicos = count($medicos);
			for ($x=0; $x<$totalMedicos; $x++) {
				$unix_time_fechaI = strtotime($_GET['fechaI']);
				$unix_time_fechaF = strtotime($_GET['fechaF']);
				$tomorrows_date = date( "Ymd", strtotime("+1 day",  $unix_time_fechaI));
				$dias = (((($unix_time_fechaF - $unix_time_fechaI)/60)/60)/24)+1;
				$unix_time_temp = strtotime($_GET['fechaI']);
				$tomorrows_date = date( "Ymd", strtotime($_GET['fechaI']));
				$prv = 0;
				$sub = 0;
				$pro = 0;
				$tot = 0;
				$prvO = 0;
				$subO = 0;
				$proO = 0;
				$totO = 0;
				$citasCanceladas = 0;
				$diasLab = 0;
				$razonCancel = "";
				for($i=0;$i<$dias;$i++) {
					$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
					$datosServicio = getServicioXid($servicios[$y]['id_servicio']);
			//		$datosConsultorio = getConsultorioXid($_GET['idConsultorio']);
					$fecha = $tomorrows_date;
					$claveDelMedico = $datosMedico['n_empleado'];
					$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
					$tipoMedico = $datosMedico['tipo_medico'];
					$especialidad = ponerAcentos($datosServicio['nombre']);
					$nombre = $nombreDelMedico . "<br><span class=\"contenido8bold\">" . $especialidad . "</span>";
					$horario = formatoHora($datosMedico['hora_entrada']) . "a" . formatoHora($datosMedico['hora_salida']);
					$n_horas = diferencia_horas(formatoHora($datosMedico['hora_salida']),formatoHora($datosMedico['hora_entrada']));
					$citasXdia = citasXdia($fecha,$_SESSION['id_usuario'],$consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
					$nCitas = count($citasXdia);
					if ($nCitas > 0) $diasLab++;
					$tot += $nCitas;
					for($n=0;$n<$nCitas;$n++) {
						if ($citasXdia[$n]['tipo_cita'] == 0) $prv++;
						else if ($citasXdia[$n]['tipo_cita'] == 1) $sub++;
						else if ($citasXdia[$n]['tipo_cita'] == 2) $pro++;
						$existeCita = existeCita($fecha,$citasXdia[$n]['id_horario']);
						if($existeCita) {
							if ($citasXdia[$n]['tipo_cita'] == 0) $prvO++;
							else if ($citasXdia[$n]['tipo_cita'] == 1) $subO++;
							else if ($citasXdia[$n]['tipo_cita'] == 2) $proO++;
							$totO++;
						}
						$canceladas = citasCanceladas($fecha,$citasXdia[$n]['id_horario']);
						$citasCanceladas += $canceladas[0];
						$razonCancel .= $canceladas[1];
					}	
					// siguiente dia
					$tomorrows_date = date( "Ymd", strtotime("+1 day",  $unix_time_temp));
					$unix_time_temp = strtotime($tomorrows_date);
				}
				$promedio = number_format($totO / $tot,2);
				$out .= "<tr>
							<td align=\"left\" class=\"contenido8\">" . $nombre . "</td>
							<td align=\"left\" class=\"contenido8\">" . $claveDelMedico . "</td>
							<td align=\"left\" class=\"contenido8\">" . $tipoMedico . "</td>
							<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
							<td align=\"center\" class=\"contenido8\">" . $n_horas  . "</td>
							<td align=\"center\" class=\"contenido8\">" . $diasLab  . "</td>
							<td align=\"center\" class=\"contenido8\">" . $prvO . "/". $prv  . "</td>
							<td align=\"center\" class=\"contenido8\">" . $subO . "/" . $sub  . "</td>
							<td align=\"center\" class=\"contenido8\">" . $proO . "/" . $pro  . "</td>
							<td align=\"center\" class=\"contenido8\">" . $totO . "/" . $tot  . "</td>
							<td align=\"center\" class=\"contenido8bold\">" . $promedio  . "</td>
							<td align=\"center\" class=\"contenido8\">" . $citasCanceladas  . "</td>
							<td align=\"left\" class=\"contenido8\">" . $razonCancel  . "</td>
						</tr>";
				$fechaInicio = $_GET['fechaI'];
				$fechaFin = $_GET['fechaF'];
				$claveUnidadMedica = "&nbsp;";
				$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
				$delegacion = "DELEGACION ESTATAL EN JALISCO";
				$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
			}
		}
	}
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
	<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	  <tr>
		<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
		<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
		  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
		<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />PROMEDIO DE CITAS OTORGADAS</td>
		<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
			<td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
		  </tr>
		  <tr>
			<td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
			<td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
		  </tr>
		</table></td>
	  </tr>
	</table></td>
  </tr>
  <tr>
	<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
	<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
	<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
	<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">M&eacute;dico que Otorga</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horario</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horas de Cons.</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">D&iacute;as Lab.</td>
						<td colspan=\"6\" align=\"center\" class=\"contenido8bold\">Consultas Otorgadas/Asignadas</td>
						<td rowspan=\"2\" align=\"center\" class=\"contenido8bold\">Motivo de Ausencia</td>
					  </tr>
					  <tr>
						<td width=\"350\" align=\"center\" class=\"contenido8bold\">Nombre</td>
						<td width=\"70\" align=\"center\" class=\"contenido8bold\">N. Empleado</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">PRV</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">SUB</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">PRO</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Total</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Prom.</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Cancel.</td>
					  </tr>" . $out . "
					</table></td>
				  </tr>
				</table>";
	echo $reporte;
}
?>
</body>
</html>
