<?php session_start ();
require_once("../lib/funcionesAdmin.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CITA MAS LEJANA</title>
<style type="text/css">
	@import url("../lib/impresion.css") print;
	@import url("../lib/reportes.css") screen;
</style>
</head>
</head>

<body>

<?php // reporte por medico

if ($_GET['tipoReporte'] == 'medico') {
	$datosMedico = getMedicoXid($_GET['idMedico']);
	$datosServicio = getServicioXid($_GET['idServicio']);
	$datosConsultorio = getConsultorioXid($_GET['idConsultorio']);
	$citasLejana = citasMasLejanas(); //$_GET['idConsultorio'],$_GET['idServicio'],$_GET['idMedico']
	$totalCitas = count($citasLejana);
	$fecha = "";
	$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
	$especialidad = ponerAcentos($datosServicio['nombre']);
	$out = "";
	for ($i=0; $i<$totalCitas; $i++) { // CITAS DE PRIMERA VEZ
		$horario = getHorarioXid($citasLejana[$i]['id_horario']);
		if (($horario['id_medico'] == $_GET['idMedico']) && ($horario['id_servicio'] == $_GET['idServicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 0)) {
			$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
			if ($datosDH['ap_p'] <> "-1") {
				$fecha = $citasLejana[$i]['fecha_cita'];
				$out .= "<tr>
							<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
							<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
							<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
							<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
							<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
						</tr>";
				break;
			}
		}
	}
	for ($i=0; $i<$totalCitas; $i++) { // CITAS SUBSECUENTES
		$horario = getHorarioXid($citasLejana[$i]['id_horario']);
		if (($horario['id_medico'] == $_GET['idMedico']) && ($horario['id_servicio'] == $_GET['idServicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 1)) {
			$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
			if ($datosDH['ap_p'] <> "-1") {
				$fecha = $citasLejana[$i]['fecha_cita'];
				$out .= "<tr>
							<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
							<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
							<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
							<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
							<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
						</tr>";
				break;
			}
		}
	}
	for ($i=0; $i<$totalCitas; $i++) { // CITAS DE PROCEDIMIENTOS
		$horario = getHorarioXid($citasLejana[$i]['id_horario']);
		if (($horario['id_medico'] == $_GET['idMedico']) && ($horario['id_servicio'] == $_GET['idServicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 2)) {
			$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
			if ($datosDH['ap_p'] <> "-1") {
				$fecha = $citasLejana[$i]['fecha_cita'];
				$out .= "<tr>
							<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
							<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
							<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
							<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
							<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
						</tr>";
				break;
			}
		}
	}
	if ($out == "")	$out = "<tr height=\"30\"><td colspan=\"5\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS EN " . $datosConsultorio['nombre'] . " - " . $especialidad . " - " . $nombreDelMedico . "</td></tr>";
	$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
						<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
						  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
						<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />REPORTE DE CITA MAS LEJANA<br>" . $datosConsultorio['nombre'] . " - " . $especialidad . " - " . $nombreDelMedico . "</td>
						<td width=\"150\" valign=\"bottom\" align=\"right\">&nbsp;</td>
					  </tr>
					</table></td>
				  </tr>
				  <tr>
					<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
				  </tr>
				  <tr>
					<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td width=\"35\" align=\"center\" class=\"contenido8bold\">Fecha Cita</td>
						<td width=\"35\" align=\"center\" class=\"contenido8bold\">Hora Cita</td>
						<td width=\"100\" align=\"center\" class=\"contenido8bold\">Nombre Paciente</td>
						<td width=\"30\" align=\"center\" class=\"contenido8bold\">No. de<br />Expediente</td>
						<td width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo de<br />Cita</td>
					  </tr>" . $out . "
					</table></td>
				  </tr>
				</table>";
	echo $reporte;
}
// reporte por servicio

if ($_GET['tipoReporte'] == 'servicio') {
	$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$_GET['idServicio']);
	$totalMedicos = count($medicos);
	for ($x=0; $x<$totalMedicos; $x++) {
		$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
		$datosServicio = getServicioXid($_GET['idServicio']);
		$datosConsultorio = getConsultorioXid($_GET['idConsultorio']);
		$citasLejana = citasMasLejanas(); //$_GET['idConsultorio'],$_GET['idServicio'],$_GET['idMedico']
		$totalCitas = count($citasLejana);
		$fecha = "";
		$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
		$especialidad = ponerAcentos($datosServicio['nombre']);
		$out = "";
		for ($i=0; $i<$totalCitas; $i++) { // CITAS DE PRIMERA VEZ
			$horario = getHorarioXid($citasLejana[$i]['id_horario']);
			if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $_GET['idServicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 0)) {
				$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
				if ($datosDH['ap_p'] <> "-1") {
					$fecha = $citasLejana[$i]['fecha_cita'];
					$out .= "<tr>
								<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
								<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
								<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
								<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
								<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
							</tr>";
					break;
				}
			}
		}
		for ($i=0; $i<$totalCitas; $i++) { // CITAS SUBSECUENTES
			$horario = getHorarioXid($citasLejana[$i]['id_horario']);
			if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $_GET['idServicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 1)) {
				$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
				if ($datosDH['ap_p'] <> "-1") {
					$fecha = $citasLejana[$i]['fecha_cita'];
					$out .= "<tr>
								<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
								<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
								<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
								<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
								<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
							</tr>";
					break;
				}
			}
		}
		for ($i=0; $i<$totalCitas; $i++) { // CITAS DE PROCEDIMIENTOS
			$horario = getHorarioXid($citasLejana[$i]['id_horario']);
			if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $_GET['idServicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 2)) {
				$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
				if ($datosDH['ap_p'] <> "-1") {
					$fecha = $citasLejana[$i]['fecha_cita'];
					$out .= "<tr>
								<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
								<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
								<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
								<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
								<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
							</tr>";
					break;
				}
			}
		}
		if ($out == "")	$out = "<tr height=\"30\"><td colspan=\"5\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS EN " . $datosConsultorio['nombre'] . " - " . $especialidad . " - " . $nombreDelMedico . "</td></tr>";
		$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
		$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
							<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
							  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
							<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />REPORTE DE CITA MAS LEJANA<br>" . $datosConsultorio['nombre'] . " - " . $especialidad . " - " . $nombreDelMedico . "</td>
							<td width=\"150\" valign=\"bottom\" align=\"right\">&nbsp;</td>
						  </tr>
						</table></td>
					  </tr>
					  <tr>
						<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
					  </tr>
					  <tr>
						<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td width=\"35\" align=\"center\" class=\"contenido8bold\">Fecha Cita</td>
							<td width=\"35\" align=\"center\" class=\"contenido8bold\">Hora Cita</td>
							<td width=\"100\" align=\"center\" class=\"contenido8bold\">Nombre Paciente</td>
							<td width=\"30\" align=\"center\" class=\"contenido8bold\">No. de<br />Expediente</td>
							<td width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo de<br />Cita</td>
						  </tr>" . $out . "
						</table></td>
					  </tr>
					</table><br><br>";
		echo $reporte;
	}
}

// reporte por consultorio

if ($_GET['tipoReporte'] == 'consultorio') {
	$servicios = getServiciosXConsultorio($_GET['idConsultorio']);
	$totalServicios = count($servicios);
	for ($y=0; $y<$totalServicios; $y++) {
		$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$servicios[$y]['id_servicio']);
		$totalMedicos = count($medicos);
		for ($x=0; $x<$totalMedicos; $x++) {
			$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
			$datosServicio = getServicioXid($servicios[$y]['id_servicio']);
			$datosConsultorio = getConsultorioXid($_GET['idConsultorio']);
			$citasLejana = citasMasLejanas(); //$_GET['idConsultorio'],$_GET['idServicio'],$_GET['idMedico']
			$totalCitas = count($citasLejana);
			$fecha = "";
			$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
			$especialidad = ponerAcentos($datosServicio['nombre']);
			$out = "";
			for ($i=0; $i<$totalCitas; $i++) { // CITAS DE PRIMERA VEZ
				$horario = getHorarioXid($citasLejana[$i]['id_horario']);
				if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $servicios[$y]['id_servicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 0)) {
					$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
					if ($datosDH['ap_p'] <> "-1") {
						$fecha = $citasLejana[$i]['fecha_cita'];
						$out .= "<tr>
									<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
									<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
									<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
									<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
									<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
								</tr>";
						break;
					}
				}
			}
			for ($i=0; $i<$totalCitas; $i++) { // CITAS SUBSECUENTES
				$horario = getHorarioXid($citasLejana[$i]['id_horario']);
				if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $servicios[$y]['id_servicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 1)) {
					$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
					if ($datosDH['ap_p'] <> "-1") {
						$fecha = $citasLejana[$i]['fecha_cita'];
						$out .= "<tr>
									<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
									<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
									<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
									<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
									<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
								</tr>";
						break;
					}
				}
			}
			for ($i=0; $i<$totalCitas; $i++) { // CITAS DE PROCEDIMIENTOS
				$horario = getHorarioXid($citasLejana[$i]['id_horario']);
				if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $servicios[$y]['id_servicio']) && ($horario['id_consultorio'] == $_GET['idConsultorio']) && ($horario['tipo_cita'] == 2)) {
					$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
					if ($datosDH['ap_p'] <> "-1") {
						$fecha = $citasLejana[$i]['fecha_cita'];
						$out .= "<tr>
									<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
									<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
									<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
									<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
									<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
								</tr>";
						break;
					}
				}
			}
			if ($out == "")	$out = "<tr height=\"30\"><td colspan=\"5\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS EN " . $datosConsultorio['nombre'] . " - " . $especialidad . " - " . $nombreDelMedico . "</td></tr>";
			$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
			$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
							  <tr>
								<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
								<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
								  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
								<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />REPORTE DE CITA MAS LEJANA<br>" . $datosConsultorio['nombre'] . " - " . $especialidad . " - " . $nombreDelMedico . "</td>
								<td width=\"150\" valign=\"bottom\" align=\"right\">&nbsp;</td>
							  </tr>
							</table></td>
						  </tr>
						  <tr>
							<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
						  </tr>
						  <tr>
							<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
							  <tr>
								<td width=\"35\" align=\"center\" class=\"contenido8bold\">Fecha Cita</td>
								<td width=\"35\" align=\"center\" class=\"contenido8bold\">Hora Cita</td>
								<td width=\"100\" align=\"center\" class=\"contenido8bold\">Nombre Paciente</td>
								<td width=\"30\" align=\"center\" class=\"contenido8bold\">No. de<br />Expediente</td>
								<td width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo de<br />Cita</td>
							  </tr>" . $out . "
							</table></td>
						  </tr>
						</table><br><br>";
			echo $reporte;
		}
	}
}

if ($_GET['tipoReporte'] == 'unidad') {
	$consultorios = getConsultorios();
	$totalConsultorios = count($consultorios);
	for($z=0;$z<$totalConsultorios; $z++) {
		$servicios = getServiciosXConsultorio($consultorios[$z]['id_consultorio']);
		$totalServicios = count($servicios);
		for ($y=0; $y<$totalServicios; $y++) {
			$medicos = getMedicosXServicioXConsultorio($consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio']);
			$totalMedicos = count($medicos);
			for ($x=0; $x<$totalMedicos; $x++) {
				$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
				$datosServicio = getServicioXid($servicios[$y]['id_servicio']);
				$datosConsultorio = getConsultorioXid($consultorios[$z]['id_consultorio']);
				$citasLejana = citasMasLejanas(); //$_GET['idConsultorio'],$_GET['idServicio'],$_GET['idMedico']
				$totalCitas = count($citasLejana);
				$fecha = "";
				$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
				$especialidad = ponerAcentos($datosServicio['nombre']);
				$out = "";
				for ($i=0; $i<$totalCitas; $i++) { // CITAS DE PRIMERA VEZ
					$horario = getHorarioXid($citasLejana[$i]['id_horario']);
					if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $servicios[$y]['id_servicio']) && ($horario['id_consultorio'] == $consultorios[$z]['id_consultorio']) && ($horario['tipo_cita'] == 0)) {
						$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
						if ($datosDH['ap_p'] <> "-1") {
							$fecha = $citasLejana[$i]['fecha_cita'];
							$out .= "<tr>
										<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
										<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
										<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
										<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
										<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
									</tr>";
							break;
						}
					}
				}
				for ($i=0; $i<$totalCitas; $i++) { // CITAS SUBSECUENTES
					$horario = getHorarioXid($citasLejana[$i]['id_horario']);
					if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $servicios[$y]['id_servicio']) && ($horario['id_consultorio'] == $consultorios[$z]['id_consultorio']) && ($horario['tipo_cita'] == 1)) {
						$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
						if ($datosDH['ap_p'] <> "-1") {
							$fecha = $citasLejana[$i]['fecha_cita'];
							$out .= "<tr>
										<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
										<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
										<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
										<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
										<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
									</tr>";
							break;
						}
					}
				}
				for ($i=0; $i<$totalCitas; $i++) { // CITAS DE PROCEDIMIENTOS
					$horario = getHorarioXid($citasLejana[$i]['id_horario']);
					if (($horario['id_medico'] == $medicos[$x]['id_medico']) && ($horario['id_servicio'] == $servicios[$y]['id_servicio']) && ($horario['id_consultorio'] == $consultorios[$z]['id_consultorio']) && ($horario['tipo_cita'] == 2)) {
						$datosDH = getDatosDerecho($citasLejana[$i]['id_derecho']);
						if ($datosDH['ap_p'] <> "-1") {
							$fecha = $citasLejana[$i]['fecha_cita'];
							$out .= "<tr>
										<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoDia($fecha,"imprimirCita") . "</td>
										<td width=\"35\" align=\"center\" class=\"contenido8\">" . formatoHora($horario['hora_inicio']) . " a " . formatoHora($horario['hora_fin']) . "</td>
										<td width=\"100\" align=\"center\" class=\"contenido8\">" . $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']  . "</td>
										<td width=\"30\" align=\"center\" class=\"contenido8\">" . $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'] . "</td>
										<td width=\"30\" align=\"center\" class=\"contenido8\">" . $tipoCita[$horario['tipo_cita']] . "</td>
									</tr>";
							break;
						}
					}
				}
				if ($out == "")	$out = "<tr height=\"30\"><td colspan=\"5\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS EN " . $datosConsultorio['nombre'] . " - " . $especialidad . " - " . $nombreDelMedico . "</td></tr>";
				$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
				$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
							  <tr>
								<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
								  <tr>
									<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
									<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
									  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
									<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />REPORTE DE CITA MAS LEJANA<br>" . $datosConsultorio['nombre'] . " - " . $especialidad . " - " . $nombreDelMedico . "</td>
									<td width=\"150\" valign=\"bottom\" align=\"right\">&nbsp;</td>
								  </tr>
								</table></td>
							  </tr>
							  <tr>
								<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
							  </tr>
							  <tr>
								<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
								  <tr>
									<td width=\"35\" align=\"center\" class=\"contenido8bold\">Fecha Cita</td>
									<td width=\"35\" align=\"center\" class=\"contenido8bold\">Hora Cita</td>
									<td width=\"100\" align=\"center\" class=\"contenido8bold\">Nombre Paciente</td>
									<td width=\"30\" align=\"center\" class=\"contenido8bold\">No. de<br />Expediente</td>
									<td width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo de<br />Cita</td>
								  </tr>" . $out . "
								</table></td>
							  </tr>
							</table><br><br>";
				echo $reporte;
			}
		}
	}
}
?>
</body>
</html>
