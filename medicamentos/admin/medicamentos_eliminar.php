<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if (isset($_GET['id_medicamento'])) {
		$res = getDatosMedicamentos($_GET['id_medicamento']);
		$clave = $res['clave_farmacia'];
		$codigo = $res['codigo_barras'];
		$nombre = $res['descripcion'];
		$presentacion = $res['presentacion'];
		$contenido = $res['contenido'];
		$unidad = $res['unidad'];
		$caducidad = formatoHora($res['tiempo_cad_remanente']);
		$tipo = $res['tipo'];
		$total = count($tipoMedicamento);
		$opciones_tipo = "";
		for ($i=0; $i<$total; $i++) {
			$temp = "";
			if ($i == $tipo) $temp = " selected=\"selected\"";
			$opciones_tipo.= "<option value=\"" . $i . "\"" . $temp . ">" . $tipoMedicamento[$i] . "</option>";
		}
	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No existe la variable id del servicio'); history.back();</script>";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Medicamentos Oncológicos</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Medicamentos Oncológicos&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="medicamentos_eliminar_confirmar.php" method="post" name="forma">
        <input name="id_medicamento" type="hidden" id="id_medicamento" value="<?php echo $_GET['id_medicamento'] ?>" />
    	<table class="ventana">
          <tr class="tituloVentana">
            <td colspan="2">Eliminar Medicamento</td>
          </tr>
		  <tr>
		    <td>Clave de Farmacia:</td><td><input name="clave" type="text" size="18" maxlength="18" id="clave" value="<?php echo $clave ?>" readonly="readonly" /></td></tr>
		  <tr>
		    <td>Código de Barras:</td>
		    <td><input name="codigo" type="text" size="18" maxlength="18" id="codigo" value="<?php echo $codigo ?>" readonly="readonly" /></td></tr>
		  <tr><td>Nombre:</td>
		  <td><input name="nombre" type="text" size="40" maxlength="50" id="nombre" value="<?php echo $nombre ?>" readonly="readonly" /></td></tr>
		  <tr>
		    <td>Presentación:</td>
		    <td><input name="presentacion" type="text" size="15" maxlength="15" id="presentacion" value="<?php echo $presentacion ?>" readonly="readonly" /></td></tr>
		  <tr>
		    <td>Contenido:</td>
		    <td><input name="contenido" type="text" size="15" maxlength="10" id="contenido" value="<?php echo $contenido ?>" readonly="readonly" /></td></tr>
		  <tr>
		    <td>Unidad:</td>
		    <td><input name="unidad" type="text" size="15" maxlength="3" id="unidad" value="<?php echo $unidad ?>" readonly="readonly" /></td></tr>
		  <tr>
		    <td>Tiempo de Caducidad de Remanente:</td>
		    <td><input name="caducidad" type="text" size="15" maxlength="5" id="caducidad" value="<?php echo $caducidad ?>" readonly="readonly" />
		    hrs. (ej. 24:30)</td>
		  </tr>
		  <tr>
		    <td>Tipo de Medicamento:</td>
		    <td><select name="tipo" id="tipo" disabled="disabled"> <?php echo $opciones_tipo ?>
		      </select>
		    </td>
		  </tr>
		  <tr><td align="center" colspan="2"><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" onclick="" value="Eliminar Medicamento" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>