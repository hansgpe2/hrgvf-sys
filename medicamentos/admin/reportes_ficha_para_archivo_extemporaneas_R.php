<?php session_start();
require_once("../lib/funcionesAdmin.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ficha para Archivo (Citas Extempor&aacute;neas)</title>
<style type="text/css">
	@import url("../lib/impresion.css") print;
	@import url("../lib/reportes.css") screen;
</style>
</head>
</head>

<body>

<?php // reporte por medico

if ($_GET['tipoReporte'] == 'medico') {
	$datosMedico = getMedicoXid($_GET['idMedico']);
	$datosServicio = getServicioXid($_GET['idServicio']);
	$fechaI = substr($_GET['fechaI'],6,4) . substr($_GET['fechaI'],3,2) . substr($_GET['fechaI'],0,2);
	$fechaF = substr($_GET['fechaF'],6,4) . substr($_GET['fechaF'],3,2) . substr($_GET['fechaF'],0,2);
	$citas = citasExtemporaneasXrangoFechas($fechaI,$fechaF);
	$totalCitas = count($citas);
	$out = "<tr height=\"30\"><td colspan=\"2\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . $_GET['fechaI'] . " A " . $_GET['fechaF'] . "</td></tr>";
	//$j = 0;
	$bandera = false;
	for($i=0;$i<$totalCitas;$i++) {
		if($citas[$i]['id_derecho'] > 0) {;
			if ($citas[$i]['id_medico'] == $_GET['idMedico']) {
				if ($bandera == false) $out = "";
				$bandera = true;
				$datosDH = getDatosDerecho($citas[$i]['id_derecho']);
		//		$j++;
				$nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
				$expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
				$vigenciaSi = "X";
				$vigenciaNo = "&nbsp;";
				$sexoM = "&nbsp;";
				$sexoF = "&nbsp;";
				$edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
				$sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
				if ($sexo == "M") $sexoM = $edad;
				if ($sexo == "F") $sexoF = $edad;
				$foraneo = tipoDH($datosDH['municipio']);
				$prv = "&nbsp;";
				$sub = "&nbsp;";
				if ($citas[$i]['tipo_cita'] == 0) $prv= "X"; else $sub = "X";
				$out .= "<tr height=\"30\">
					<td align=\"left\" class=\"contenido8\">" . $expediente . "</td>
					<td align=\"left\" class=\"contenido8\">" . ponerAcentos($nombre) . "</td>
				  </tr>";
			}
		}
	}
	$fechaInicio = $_GET['fechaI'];
	$fechaFin = $_GET['fechaF'];
	$claveUnidadMedica = "&nbsp;";
	$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
	$delegacion = "DELEGACION ESTATAL EN JALISCO";
	$claveDelMedico = $datosMedico['n_empleado'];
	$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
	$servicio = "CONSULTA EXTERNA";
	$claveDeLaEspecialidad = $datosServicio['clave'];
	$especialidad = ponerAcentos($datosServicio['nombre']);
	$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
        <td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
          Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
        <td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />Ficha para Archivo (Citas Extempor&aacute;neas)</td>
        <td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
            <td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
          </tr>
          <tr>
            <td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
            <td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
    <td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del Médico</td>
  </tr>
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td align=\"left\" class=\"contenido8bold\" width=\"50\">Clave del Médico:</td>
        <td align=\"left\" class=\"contenido8\" width=\"100\">" . $claveDelMedico . "</td>
        <td align=\"left\" class=\"contenido8bold\" width=\"100\">Nombre del Médico:</td>
        <td align=\"left\" class=\"contenido8\" width=\"300\">" . $nombreDelMedico . "</td>
        <td align=\"left\" class=\"contenido8bold\" width=\"50\">Servicio:</td>
        <td align=\"left\" class=\"contenido8\" width=\"300\">" . $servicio . "</td>
      </tr>
      <tr>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Clave de la Especialidad:</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $claveDeLaEspecialidad . "</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Especialidad:</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $especialidad . "</td>
        <td align=\"left\" class=\"contenido8bold\">Horario:</td>
        <td align=\"left\" class=\"contenido8\">" . $horario . "</td>
      </tr>
      <tr>
        <td align=\"left\" class=\"contenido8bold\">Firma:</td>
        <td align=\"left\" class=\"contenido8\" height=\"40\">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
    <td><table width=\"50%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"60\" align=\"center\" class=\"contenido8bold\">C&eacute;dula</td>
        <td width=\"200\" align=\"center\" class=\"contenido8bold\">Nombre Paciente</td>
        </tr>" . $out . "
    </table></td>
  </tr>
</table>";
	echo $reporte;

}
// reporte por servicio

if ($_GET['tipoReporte'] == 'servicio') {
	$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$_GET['idServicio']);
	$totalMedicos = count($medicos);
	for ($x=0; $x<$totalMedicos; $x++) {

		$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
		$datosServicio = getServicioXid($_GET['idServicio']);
		$fechaI = substr($_GET['fechaI'],6,4) . substr($_GET['fechaI'],3,2) . substr($_GET['fechaI'],0,2);
		$fechaF = substr($_GET['fechaF'],6,4) . substr($_GET['fechaF'],3,2) . substr($_GET['fechaF'],0,2);
		$citas = citasExtemporaneasXrangoFechas($fechaI,$fechaF);
		$totalCitas = count($citas);
		$out = "<tr height=\"30\"><td colspan=\"2\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . $_GET['fechaI'] . " A " . $_GET['fechaF'] . "</td></tr>";
		$bandera = false;
		for($i=0;$i<$totalCitas;$i++) {
			if($citas[$i]['id_derecho'] > 0) {;
				if ($citas[$i]['id_medico'] == $medicos[$x]['id_medico']) {
					if ($bandera == false) $out = "";
					$bandera = true;
					$datosDH = getDatosDerecho($citas[$i]['id_derecho']);
					$nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
					$expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
					$vigenciaSi = "X";
					$vigenciaNo = "&nbsp;";
					$sexoM = "&nbsp;";
					$sexoF = "&nbsp;";
					$edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
					$sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
					if ($sexo == "M") $sexoM = $edad;
					if ($sexo == "F") $sexoF = $edad;
					$foraneo = tipoDH($datosDH['municipio']);
					$prv = "&nbsp;";
					$sub = "&nbsp;";
					if ($citas[$i]['tipo_cita'] == 0) $prv= "X"; else $sub = "X";
						$out .= "<tr height=\"30\">
							<td align=\"left\" class=\"contenido8\">" . $expediente . "</td>
							<td align=\"left\" class=\"contenido8\">" . ponerAcentos($nombre) . "</td>
						  </tr>";
				}
			}
		}
		$fechaInicio = $_GET['fechaI'];
		$fechaFin = $_GET['fechaF'];
		$claveUnidadMedica = "&nbsp;";
		$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
		$delegacion = "DELEGACION ESTATAL EN JALISCO";
		$claveDelMedico = $datosMedico['n_empleado'];
		$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
		$servicio = "CONSULTA EXTERNA";
		$claveDeLaEspecialidad = $datosServicio['clave'];
		$especialidad = ponerAcentos($datosServicio['nombre']);
		$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
        <td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
          Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
        <td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />Ficha para Archivo (Citas Extempor&aacute;neas)</td>
        <td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
            <td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
          </tr>
          <tr>
            <td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
            <td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
    <td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del Médico</td>
  </tr>
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td align=\"left\" class=\"contenido8bold\" width=\"50\">Clave del Médico:</td>
        <td align=\"left\" class=\"contenido8\" width=\"100\">" . $claveDelMedico . "</td>
        <td align=\"left\" class=\"contenido8bold\" width=\"100\">Nombre del Médico:</td>
        <td align=\"left\" class=\"contenido8\" width=\"300\">" . $nombreDelMedico . "</td>
        <td align=\"left\" class=\"contenido8bold\" width=\"50\">Servicio:</td>
        <td align=\"left\" class=\"contenido8\" width=\"300\">" . $servicio . "</td>
      </tr>
      <tr>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Clave de la Especialidad:</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $claveDeLaEspecialidad . "</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Especialidad:</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $especialidad . "</td>
        <td align=\"left\" class=\"contenido8bold\">Horario:</td>
        <td align=\"left\" class=\"contenido8\">" . $horario . "</td>
      </tr>
      <tr>
        <td align=\"left\" class=\"contenido8bold\">Firma:</td>
        <td align=\"left\" class=\"contenido8\" height=\"40\">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
    <td><table width=\"50%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"60\" align=\"center\" class=\"contenido8bold\">C&eacute;dula</td>
        <td width=\"200\" align=\"center\" class=\"contenido8bold\">Nombre Paciente</td>
        </tr>" . $out . "
    </table></td>
  </tr>
</table><H1 class=\"SaltoDePagina\"> </H1>";
		if ($out != "<tr height=\"30\"><td colspan=\"2\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . $_GET['fechaI'] . " A " . $_GET['fechaF'] . "</td></tr>") {
			echo $reporte;
		}
	}
}
// reporte por consultorio

if ($_GET['tipoReporte'] == 'consultorio') {
	$servicios = getServiciosXConsultorio($_GET['idConsultorio']);
	$totalServicios = count($servicios);
	for ($y=0; $y<$totalServicios; $y++) {
		$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$servicios[$y]['id_servicio']);
		$totalMedicos = count($medicos);
		for ($x=0; $x<$totalMedicos; $x++) {
			$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
			$datosServicio = getServicioXid($servicios[$y]['id_servicio']);
			$fechaI = substr($_GET['fechaI'],6,4) . substr($_GET['fechaI'],3,2) . substr($_GET['fechaI'],0,2);
			$fechaF = substr($_GET['fechaF'],6,4) . substr($_GET['fechaF'],3,2) . substr($_GET['fechaF'],0,2);
			$citas = citasExtemporaneasXrangoFechas($fechaI,$fechaF);
			$totalCitas = count($citas);
			$out = "<tr height=\"30\"><td colspan=\"2\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . $_GET['fechaI'] . " A " . $_GET['fechaF'] . "</td></tr>";
			$bandera = false;
			for($i=0;$i<$totalCitas;$i++) {
				if($citas[$i]['id_derecho'] > 0) {;
					if ($citas[$i]['id_medico'] == $medicos[$x]['id_medico']) {
						if ($bandera == false) $out = "";
						$bandera = true;
						$datosDH = getDatosDerecho($citas[$i]['id_derecho']);
						$nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
						$expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
						$vigenciaSi = "X";
						$vigenciaNo = "&nbsp;";
						$sexoM = "&nbsp;";
						$sexoF = "&nbsp;";
						$edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
						$sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
						if ($sexo == "M") $sexoM = $edad;
						if ($sexo == "F") $sexoF = $edad;
						$foraneo = tipoDH($datosDH['municipio']);
						$prv = "&nbsp;";
						$sub = "&nbsp;";
						if ($citas[$i]['tipo_cita'] == 0) $prv= "X"; else $sub = "X";
							$out .= "<tr height=\"30\">
								<td align=\"left\" class=\"contenido8\">" . $expediente . "</td>
								<td align=\"left\" class=\"contenido8\">" . ponerAcentos($nombre) . "</td>
							  </tr>";
					}
				}
			}
			$fechaInicio = $_GET['fechaI'];
			$fechaFin = $_GET['fechaF'];
			$claveUnidadMedica = "&nbsp;";
			$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
			$delegacion = "DELEGACION ESTATAL EN JALISCO";
			$claveDelMedico = $datosMedico['n_empleado'];
			$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
			$servicio = "CONSULTA EXTERNA";
			$claveDeLaEspecialidad = $datosServicio['clave'];
			$especialidad = ponerAcentos($datosServicio['nombre']);
			$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
		$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	  <tr>
		<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
			<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
			  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
			<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />Ficha para Archivo (Citas Extempor&aacute;neas)</td>
			<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
				<td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
			  </tr>
			  <tr>
				<td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
				<td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
			  </tr>
			</table></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
		<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
	  </tr>
	  <tr>
		<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
	  </tr>
	  <tr>
		<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del Médico</td>
	  </tr>
	  <tr>
		<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td align=\"left\" class=\"contenido8bold\" width=\"50\">Clave del Médico:</td>
			<td align=\"left\" class=\"contenido8\" width=\"100\">" . $claveDelMedico . "</td>
			<td align=\"left\" class=\"contenido8bold\" width=\"100\">Nombre del Médico:</td>
			<td align=\"left\" class=\"contenido8\" width=\"300\">" . $nombreDelMedico . "</td>
			<td align=\"left\" class=\"contenido8bold\" width=\"50\">Servicio:</td>
			<td align=\"left\" class=\"contenido8\" width=\"300\">" . $servicio . "</td>
		  </tr>
		  <tr>
			<td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Clave de la Especialidad:</td>
			<td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $claveDeLaEspecialidad . "</td>
			<td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Especialidad:</td>
			<td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $especialidad . "</td>
			<td align=\"left\" class=\"contenido8bold\">Horario:</td>
			<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"contenido8bold\">Firma:</td>
			<td align=\"left\" class=\"contenido8\" height=\"40\">&nbsp;</td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
		<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
	  </tr>
	  <tr>
		<td><table width=\"50%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td width=\"60\" align=\"center\" class=\"contenido8bold\">C&eacute;dula</td>
			<td width=\"200\" align=\"center\" class=\"contenido8bold\">Nombre Paciente</td>
			</tr>" . $out . "
		</table></td>
	  </tr>
	</table><H1 class=\"SaltoDePagina\"> </H1>";
			if ($out != "<tr height=\"30\"><td colspan=\"2\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . $_GET['fechaI'] . " A " . $_GET['fechaF'] . "</td></tr>") {
				echo $reporte;
			}
		}
	}
}

// reporte por unidad

if ($_GET['tipoReporte'] == 'unidad') {
	$consultorios = getConsultorios();
	$totalConsultorios = count($consultorios);
	for($z=0;$z<$totalConsultorios; $z++) {
		$servicios = getServiciosXConsultorio($consultorios[$z]['id_consultorio']);
		$totalServicios = count($servicios);
		for ($y=0; $y<$totalServicios; $y++) {
			$medicos = getMedicosXServicioXConsultorio($consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio']);
			$totalMedicos = count($medicos);
			for ($x=0; $x<$totalMedicos; $x++) {
				$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
				$datosServicio = getServicioXid($servicios[$y]['id_servicio']);
				$fechaI = substr($_GET['fechaI'],6,4) . substr($_GET['fechaI'],3,2) . substr($_GET['fechaI'],0,2);
				$fechaF = substr($_GET['fechaF'],6,4) . substr($_GET['fechaF'],3,2) . substr($_GET['fechaF'],0,2);
				$citas = citasExtemporaneasXrangoFechas($fechaI,$fechaF);
				$totalCitas = count($citas);
				$out = "<tr height=\"30\"><td colspan=\"2\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . $_GET['fechaI'] . " A " . $_GET['fechaF'] . "</td></tr>";
				$bandera = false;
				for($i=0;$i<$totalCitas;$i++) {
					if($citas[$i]['id_derecho'] > 0) {;
						if ($citas[$i]['id_medico'] == $medicos[$x]['id_medico']) {
							if ($bandera == false) $out = "";
							$bandera = true;
							$datosDH = getDatosDerecho($citas[$i]['id_derecho']);
							$nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
							$expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
							$vigenciaSi = "X";
							$vigenciaNo = "&nbsp;";
							$sexoM = "&nbsp;";
							$sexoF = "&nbsp;";
							$edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
							$sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
							if ($sexo == "M") $sexoM = $edad;
							if ($sexo == "F") $sexoF = $edad;
							$foraneo = tipoDH($datosDH['municipio']);
							$prv = "&nbsp;";
							$sub = "&nbsp;";
							if ($citas[$i]['tipo_cita'] == 0) $prv= "X"; else $sub = "X";
								$out .= "<tr height=\"30\">
									<td align=\"left\" class=\"contenido8\">" . $expediente . "</td>
									<td align=\"left\" class=\"contenido8\">" . ponerAcentos($nombre) . "</td>
								  </tr>";
						}
					}
				}
				$fechaInicio = $_GET['fechaI'];
				$fechaFin = $_GET['fechaF'];
				$claveUnidadMedica = "&nbsp;";
				$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
				$delegacion = "DELEGACION ESTATAL EN JALISCO";
				$claveDelMedico = $datosMedico['n_empleado'];
				$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
				$servicio = "CONSULTA EXTERNA";
				$claveDeLaEspecialidad = $datosServicio['clave'];
				$especialidad = ponerAcentos($datosServicio['nombre']);
				$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
			$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
				<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
				  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
				<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />Ficha para Archivo (Citas Extempor&aacute;neas)</td>
				<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
					<td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
				  </tr>
				  <tr>
					<td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
					<td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
				  </tr>
				</table></td>
			  </tr>
			</table></td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
		  </tr>
		  <tr>
			<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
		  </tr>
		  <tr>
			<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del Médico</td>
		  </tr>
		  <tr>
			<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td align=\"left\" class=\"contenido8bold\" width=\"50\">Clave del Médico:</td>
				<td align=\"left\" class=\"contenido8\" width=\"100\">" . $claveDelMedico . "</td>
				<td align=\"left\" class=\"contenido8bold\" width=\"100\">Nombre del Médico:</td>
				<td align=\"left\" class=\"contenido8\" width=\"300\">" . $nombreDelMedico . "</td>
				<td align=\"left\" class=\"contenido8bold\" width=\"50\">Servicio:</td>
				<td align=\"left\" class=\"contenido8\" width=\"300\">" . $servicio . "</td>
			  </tr>
			  <tr>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Clave de la Especialidad:</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $claveDeLaEspecialidad . "</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Especialidad:</td>
				<td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $especialidad . "</td>
				<td align=\"left\" class=\"contenido8bold\">Horario:</td>
				<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
			  </tr>
			  <tr>
				<td align=\"left\" class=\"contenido8bold\">Firma:</td>
				<td align=\"left\" class=\"contenido8\" height=\"40\">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		  <tr>
			<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
		  </tr>
		  <tr>
			<td><table width=\"50%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
			  <tr>
				<td width=\"60\" align=\"center\" class=\"contenido8bold\">C&eacute;dula</td>
				<td width=\"200\" align=\"center\" class=\"contenido8bold\">Nombre Paciente</td>
				</tr>" . $out . "
			</table></td>
		  </tr>
		</table><H1 class=\"SaltoDePagina\"> </H1>";
				if ($out != "<tr height=\"30\"><td colspan=\"2\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . $_GET['fechaI'] . " A " . $_GET['fechaF'] . "</td></tr>") {
					echo $reporte;
				}
			}
		}
	}
}
?>
</body>
</html>
