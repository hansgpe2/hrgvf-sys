<?php session_start ();
include_once('lib/misFunciones.php');
?>
<?php if ((isset($_GET["n_series"])) && (isset($_GET["estado"])) && (isset($_GET["fecha"])) && (isset($_GET["servicio"])) && (isset($_GET["medico"])) && (isset($_GET["id_derecho"]))) {
	$n_serie = $_GET["n_series"];
	$estado= $_GET["estado"];
	$fecha = $_GET["fecha"];
	$servicio = $_GET["servicio"];
	$medico = $_GET["medico"];
	$id_derecho = $_GET["id_derecho"];
	$remanente = $_GET["remanente"];

	$clave = $_GET["clave"];
	$dosis = $_GET["dosis"];
	$diag = $_GET["diag"];
	$etapa = $_GET["etapa"];

	$hayError = true;
	if ($remanente < $dosis) {
		echo "La dosis debe ser menor o igual a " . $remanente . " que es el remanente del medicamento";
	} else {
		$duplica = existeDuplica("SELECT * FROM recetas WHERE n_serie='" . $n_serie . "'");
		if (!$duplica) {
			$query_query = "INSERT INTO recetas VALUES (NULL,'" .  $n_serie . "','','" . $medico . "','" . $servicio . "','" . $id_derecho . "','" . date('Ymd') . "','" . formatoFechaBD($fecha) . "','" . $estado . "','4','');";
			$res = ejecutarSQL($query_query);
			if ($res[0] == 0) {// no hay error
				$id_receta = getRecetaRecienAgregada($n_serie);
				$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|receta agregada " . date("H:i d/m/Y") . "|" . $id_receta . "','2')";
				$res = ejecutarSQL($query_query);
				
				if ($clave != "0") {
					$medicamento = getMedicamentoXclave($clave);
					$query_query = "INSERT INTO recetas_conceptos VALUES (NULL,'" .  $id_receta . "','" . $medicamento['id_medicamento'] . "','" . $dosis . "','" . $diag . "','" . $etapa . "','');";
					$res = ejecutarSQL($query_query);
					if ($res[0] == 0) {// no hay error
/*
						$query_query = "UPDATE medicamentos_inventario set '" . $dosis . "','" . $remanente . "','" . date('Ymd') . "','" . date('Ymd') . "','" . date('Hi') . "','5','" . $_GET["id_ingreso"] . "');"; // en extra se pone el id ingreso anterior de este medicamento qeu se va a reutilizar
						$res = ejecutarSQL($query_query);
*/
						$query_query = "INSERT INTO medicamentos_inventario VALUES (NULL,'" .  $medicamento['id_medicamento'] . "','" . $id_receta . "','" . $dosis . "','" . $remanente . "','" . date('Ymd') . "','" . date('Ymd') . "','" . date('Hi') . "','5','" . $_GET["id_ingreso"] . "');"; // en extra se pone el id ingreso anterior de este medicamento qeu se va a reutilizar
						$res = ejecutarSQL($query_query);
						$id_ingreso = medicamentoRecienInventariado($medicamento['id_medicamento'], $id_receta);
						
						$contenidoActual = getContenidoRemanente($_GET["id_ingreso"]);
						$cantidad_remanente = $contenidoActual - $dosis;
						$query_query = "INSERT INTO medicamentos_remanentes VALUES (NULL,'" .  $id_ingreso . "','" . $cantidad_remanente . "','" .  date('Ymd') . "','" .  date('Hi') . "',5,'');";
						$res = ejecutarSQL($query_query);
						if ($res[0] == 0) { // no hay error
							$hayError = false;
							$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|reutilizo medicamento " . date("H:i d/m/Y") . "|" . $id_receta . "|" . $id_ingreso . "','6')";
							$res = ejecutarSQL($query_query);
						} else { // SI hay error al agregar medicamento al inventario
							echo "no se pudo aplicar la dosis remanente";
						}
					} else { // hay error al agregar concepto a receta
						echo "No se pudo agregar el concepto 1 a la receta";
					}
				} 
	
	
				if (!$hayError) {
					echo "Receta Agregada Correctamente";
				} else { // hay error en el alta de medicamentos o conceptos de receta
					echo "Hay errores en el alta de medicamentos al inventario local o de conceptos a la receta";
				}
			} else { // hay error mysql al agregar receta
				echo "La Receta no se pudo agregar";
			}
		} else { // else de receta duplicada en su n. de serie
			echo "El n. de serie ya existe en la base de datos";
		}
	}
} else {
	echo "No se pudo administrar la dosis, pongase en contacto con el administrador del sistema";
}
?>
