<?php session_start ();
include_once('lib/misFunciones.php');
?>
<?php if ((isset($_GET["id_receta"])) && (isset($_GET["id_ingreso"]))) {
	$id_receta = $_GET["id_receta"];
	$id_ingreso = $_GET["id_ingreso"];

	$hayError = true;
//	$medicamento = datosMedicamentoRemanente($id_ingreso);
	$contenidoActual = getContenidoRemanente($_GET["id_ingreso"]);

	$query_query = "UPDATE medicamentos_remanentes SET status='0' WHERE id_ingreso='" .  $id_ingreso . "'";
	$res = ejecutarSQL($query_query);
	if ($res[0] == 0) { // no hay error
		$query_query2 = "INSERT INTO medicamentos_bajas VALUES (NULL,'" .  $id_ingreso . "','" . $contenidoActual . "','" .  date('Ymd') . "','" .  date('Hi') . "',1,'');";
		$res2 = ejecutarSQL($query_query2);
		if ($res2[0] == 0) { // no hay error
			$hayError = false;
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|baja medicamento " . date("H:i d/m/Y") . "|" . $id_receta . "|" . $_GET["id_ingreso"] . "','4')";
			$res = ejecutarSQL($query_query);
		} else {
			echo "no se pudo agregar el registro a medicamentos bajas";
		}
	} else { // SI hay error al agregar medicamento al inventario
		echo "no se pudo cambiar el status a 0 de medicamentos remanentes";
	}

	if (!$hayError) {
		echo "Baja de Medicamento Realizada Correctamente";
//		$query_query = "UPDATE recetas SET status='0' WHERE id_receta='" . $id_receta . "' LIMIT 1";
//		$res = ejecutarSQL($query_query);
	}
} else {
	echo "No se pudo dar de baja el medicamento, pongase en contacto con el administrador del sistema";
}
?>
