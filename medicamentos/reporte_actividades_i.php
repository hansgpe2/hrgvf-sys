<?php session_start ();
include_once('lib/misFunciones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>REPORTE DE ACTIVIDADES</title>
<style type="text/css">
	@import url("lib/impresion.css") print;
	@import url("lib/reportes.css") screen;
</style>
</head>
</head>

<body>

<?php // reporte por medico
/*
	$datosMedico = getMedicoXid($_GET['idMedico']);
	$datosServicio = getServicioXid($_GET['idServicio']);
	$fechaI = substr($_GET['fechaI'],6,4) . substr($_GET['fechaI'],3,2) . substr($_GET['fechaI'],0,2);
	$fechaF = substr($_GET['fechaF'],6,4) . substr($_GET['fechaF'],3,2) . substr($_GET['fechaF'],0,2);
	$citas = citasXrangoFechas($fechaI,$fechaF);
	$totalCitas = count($citas);
	$out = "<tr height=\"30\"><td colspan=\"22\" align=\"center\" class=\"contenido8\">NO EXISTEN CITAS DE " . $_GET['fechaI'] . " A " . $_GET['fechaF'] . "</td></tr>";
	//$j = 0;
	$bandera = false;
	for($i=0;$i<$totalCitas;$i++) {
		if($citas[$i]['id_derecho'] > 0) {;
			$horario = getHorarioXid($citas[$i]['id_horario']);
			if ($horario['id_medico'] == $_GET['idMedico']) {
				if ($bandera == false) $out = "";
				$bandera = true;
				$datosDH = getDatosDerecho($citas[$i]['id_derecho']);
		//		$j++;
				$nombre = $datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres'];
				$expediente = $datosDH['cedula'] . "/" . $datosDH['cedula_tipo'];
				$vigenciaSi = "X";
				$vigenciaNo = "&nbsp;";
				$sexoM = "&nbsp;";
				$sexoF = "&nbsp;";
				$edad = getEdadXfechaNac($datosDH['fecha_nacimiento']);
				$sexo = queSexoTipoCedula($datosDH['cedula_tipo']);
				if ($sexo == "M") $sexoM = $edad;
				if ($sexo == "F") $sexoF = $edad;
				$foraneo = tipoDH($datosDH['municipio']);
				$prv = "&nbsp;";
				$sub = "&nbsp;";
				if ($horario['tipo_cita'] == 0) $prv= "X"; else $sub = "X";
				$out .= "<tr height=\"30\">
					<td align=\"center\" class=\"contenido8\">" . formatoDia($citas[$i]['fecha_cita'], "imprimirCita") . "<br>" . formatoHora($horario['hora_inicio']) . "a" . formatoHora($horario['hora_fin']) . "</td>
					<td align=\"left\" class=\"contenido8\">" . ponerAcentos($nombre) . "</td>
					<td align=\"left\" class=\"contenido8\">" . $expediente . "</td>
					<td align=\"center\" class=\"contenido8\">" . $vigenciaSi . "</td>
					<td align=\"center\" class=\"contenido8\">" . $vigenciaNo . "</td>
					<td align=\"center\" class=\"contenido8\">" . $sexoM . "</td>
					<td align=\"center\" class=\"contenido8\">" . $sexoF . "</td>
					<td align=\"center\" class=\"contenido8\">" . $foraneo . "</td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"left\" class=\"contenido8\"><?php echo $c2 ?></td>
					<td align=\"center\" class=\"contenido8\">" . $prv . "</td>
					<td align=\"center\" class=\"contenido8\">" . $sub . "</td>
				  </tr>";
			}
		}
	}
*/	$fechaInicio = $_GET['fechaI'];
	$fechaFin = $_GET['fechaF'];
	$claveUnidadMedica = "&nbsp;";
	$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
	$delegacion = "DELEGACION ESTATAL EN JALISCO";
	$claveDelMedico = $datosMedico['n_empleado'];
	$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
	$servicio = "CONSULTA EXTERNA";
	$claveDeLaEspecialidad = $datosServicio['clave'];
	$especialidad = ponerAcentos($datosServicio['nombre']);
	$horario = formatoHora($datosMedico['hora_entrada']) . " a " . formatoHora($datosMedico['hora_salida']) . " hrs.";
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"74\" align=\"left\"><img src=\"diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
        <td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
          Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
        <td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />Informe Diario de Labores del Médico<br />SM-1-10</td>
        <td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"2\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td class=\"tituloEncabezado\" align=\"center\">Fecha Inicio</td>
            <td class=\"tituloEncabezado\" align=\"center\">Fecha Fin</td>
          </tr>
          <tr>
            <td align=\"center\" class=\"contenido8bold\">" . $fechaInicio . "</td>
            <td align=\"center\" class=\"contenido8bold\">" . $fechaFin . "</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
    <td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
    <td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos del Médico</td>
  </tr>
  <tr>
    <td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td align=\"left\" class=\"contenido8bold\" width=\"50\">Clave del Médico:</td>
        <td align=\"left\" class=\"contenido8\" width=\"100\">" . $claveDelMedico . "</td>
        <td align=\"left\" class=\"contenido8bold\" width=\"100\">Nombre del Médico:</td>
        <td align=\"left\" class=\"contenido8\" width=\"300\">" . $nombreDelMedico . "</td>
        <td align=\"left\" class=\"contenido8bold\" width=\"50\">Servicio:</td>
        <td align=\"left\" class=\"contenido8\" width=\"300\">" . $servicio . "</td>
      </tr>
      <tr>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Clave de la Especialidad:</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $claveDeLaEspecialidad . "</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8bold\">Especialidad:</td>
        <td rowspan=\"2\" align=\"left\" class=\"contenido8\">" . $especialidad . "</td>
        <td align=\"left\" class=\"contenido8bold\">Horario:</td>
        <td align=\"left\" class=\"contenido8\">" . $horario . "</td>
      </tr>
      <tr>
        <td align=\"left\" class=\"contenido8bold\">Firma:</td>
        <td align=\"left\" class=\"contenido8\" height=\"40\">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
    <td><table width=\"100%\" border=\"2\" cellspacing=\"0\" cellpadding=\"0\">
      <tr>
        <td width=\"15\" rowspan=\"2\" align=\"center\" class=\"contenido8bold\">Cita</td>
        <td rowspan=\"2\" width=\"100\" align=\"center\" class=\"contenido8bold\">Nombre</td>
        <td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">No. de<br />Expediente</td>
        <td colspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Vig. de<br />Derechos</td>
        <td colspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Edad<br />y<br />Sexo</td>
        <td rowspan=\"2\" width=\"15\" align=\"center\" class=\"contenido8bold\">Fo-<br />rá-<br />neo</td>
        <td colspan=\"4\" width=\"60\" align=\"center\" class=\"contenido8bold\">Solicitud a:</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">Pase<br />a<br />Uni.</td>
        <td colspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Motivo<br />Licencia</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">No.<br />Serie<br />Lic.</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">Días<br />de<br />Lic.</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">No.<br />de<br />Medic.</td>
        <td colspan=\"2\" width=\"350\" align=\"center\" class=\"contenido8bold\">Diagnóstico</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">1a.<br />vez</td>
        <td rowspan=\"2\" width=\"20\" align=\"center\" class=\"contenido8bold\">Sub-<br />se-<br />cu-<br />ente</td>
      </tr>
      <tr>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">Si</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">No</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">M</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">F</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">LAB</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">RX.</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">Int.<br />Cons.</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">Otro</td>
        <td width=\"30\" align=\"center\" class=\"contenido8bold\">PRT</td>
        <td width=\"15\" align=\"center\" class=\"contenido8bold\">RT</td>
        <td width=\"30\" align=\"center\" class=\"contenido8bold\">Código</td>
        <td align=\"center\" class=\"contenido8bold\">Descripción</td>
        </tr>" . $out . "
    </table></td>
  </tr>
</table>";
//	echo $reporte;

$fechaI = substr($_GET['fechaI'],6,4) . substr($_GET['fechaI'],3,2) . substr($_GET['fechaI'],0,2);
$fechaF = substr($_GET['fechaF'],6,4) . substr($_GET['fechaF'],3,2) . substr($_GET['fechaF'],0,2);
$medicamentosStock = listaMedicamentosConMovimientosImp($fechaI, $fechaF);

$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|impresion reporte actividades " . date("H:i d/m/Y") . "','7')";
$res = ejecutarSQL($query_query);

//$medicamentosRemanentes = listaMedicamentosRemanentes();
//$medicamentosRemanentesCaducos = listaMedicamentosRemanentesCaducos();
//$medicamentosReutilizableCaducos = listaMedicamentosReutilizablesCaducos();
//$medicamentosCaducos = listaMedicamentosCaducos();

$out = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"left\">" ;
$out.= "<table width=\"100%\" border=\"2\" cellspacing=\"0\" cellpadding=\"0\">
        <tr>
          <td height=\"23\" class=\"contenido8bold\">HISTORIAL DE MEDICAMENTOS</td>
        </tr>
        <tr>
          <td align=\"left\">" . $medicamentosStock . "
		  </td>
        </tr>
      </table>
	  </td></tr></table>";
echo $out;

?>
</body>
</html>
