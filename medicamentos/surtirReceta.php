<?php /*
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\">DOSIS:</td>
							<td align=\"left\">" . $medicamentos[$j]['dosis'] . "</td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\">DIAGNOSTICO:</td>
							<td align=\"left\">" . $medicamentos[$j]['diagnostico'] . "</td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\">ETAPA CLINICA:</td>
							<td align=\"left\">" . $medicamentos[$j]['etapa_clinica'] . "</td>
						</tr>
*/
session_start ();
include_once('lib/misFunciones.php');
$receta = getDatosRecetaXserie($_GET['n_serie']);
$paciente = getDatosDerecho($receta['id_derecho']);
$medico = getMedicoXid($receta['id_medico']);
$medicamentos = getMedicamentosEnRecetaSinSurtir($receta['id_receta']);
$totalMedicamentos = count($medicamentos);
$out = "";
	for($j=0; $j<$totalMedicamentos; $j++) {
		if($medicamentos[$j]['descripcion'] != '') {
			$n = $j+1;
			$out.="<table width=\"97%\" border=\"0\" class=\"ventana\">
						<tr>
							<td class=\"tituloVentana\" colspan=\"2\">MEDICAMENTO " . $n . "</td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\">DATOS MEDICAMENTO:</td>
							<td align=\"left\">" . $medicamentos[$j]['clave_farmacia'] . " - " . $medicamentos[$j]['descripcion'] . "<input name=\"med_" . $n ."\"t id=\"med_" . $n ."\" type=\"hidden\" value=\"" . $medicamentos[$j]['id_ingreso'] . "\" /></td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\" valign=\"top\">MEDICAMENTO:</td>
							<td align=\"left\">
								<table width=\"100%\" border=\"0\">
								  <tr>
									<td><label>
									  <input type=\"radio\" name=\"accion_" . $n . "\" value=\"surtido\" id=\"accion_" . $n . "_0\" />
									  Surtido</label></td>
								  </tr>
								  <tr>
									<td><label>
									  <input type=\"radio\" name=\"accion_" . $n . "\" value=\"inexistencia\" id=\"accion_" . $n . "_1\" />
									  No surtido por inexistencia</label></td>
								  </tr>
								  <tr>
									<td><label>
									  <input type=\"radio\" name=\"accion_" . $n . "\" value=\"receta\" id=\"accion_" . $n . "_2\" />
									  No surtido por error en receta</label></td>
								  </tr>
								  <tr>
									<td><label>
									  <input type=\"radio\" name=\"accion_" . $n . "\" value=\"razon\" id=\"accion_" . $n . "_3\" />
									  No surtido por la siguiente razon:</label><input name=\"raz_" . $n . "\" id=\"raz_" . $n . "\" type=\"text\" /></td>
								  </tr>
								</table>
							</td>
						</tr>
				   </table>";
		}
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>
<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarSurtirReceta('<?php echo $_GET['n_serie'] ?>');">
<table width="730" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">SURTIR RECETA</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">N. DE SERIE:</td>
    <td align="left"><?php echo $receta['n_serie'] ?></td></tr>
  <tr>
    <td class="textosParaInputs" align="right">ENTIDAD FEDERATIVA:</td>
    <td align="left"><?php echo ponerAcentos($receta['entidad']) ?>
    &nbsp;&nbsp;&nbsp;<span class="textosParaInputs">FECHA DE EXPEDICION: </span>
    <?php echo formatoDia($receta['fecha_expedicion'],"fecha"); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO:</td>
    <td align="left"><?php echo getServicioXid($receta['id_servicio']); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MEDICO:</td>
    <td align="left"><?php echo ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])); ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><?php echo $paciente['cedula'] . "/" . $paciente['cedula_tipo'] ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><?php echo ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><?php echo $out ?>	
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicioFarmacia.php');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Surtir Receta" class="botones" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>
</body>
</html>
