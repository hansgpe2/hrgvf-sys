<?php
include_once('lib/misFunciones.php');
session_start ();

$medicamentosXingresar = listaMedicamentosXingresar();
$medicamentosStock = listaMedicamentosStock();
$medicamentosRemanentesXAplicar = listaMedicamentosRemanentesXaplicar();
$medicamentosRemanentesCaducos = listaMedicamentosRemanentesCaducos();
$medicamentosReutilizableCaducos = listaMedicamentosReutilizablesCaducos();
$medicamentosCaducos = listaMedicamentosCaducos();

$out = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"left\">
	  <table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">MEDICAMENTOS POR REGISTRAR</td>
        </tr>
        <tr>
          <td align=\"left\">" . $medicamentosXingresar . "
		  </td>
        </tr>
      </table>
	  <br>"; 
$out.= "<table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">MEDICAMENTO POR APLICAR</td>
        </tr>
        <tr>
          <td align=\"left\">" . $medicamentosStock . "
		  </td>
        </tr>
      </table>
	  <br>";
$out .= "<table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">MEDICAMENTO REMANENTE POR APLICAR</td>
        </tr>
        <tr>
          <td align=\"left\">" . $medicamentosRemanentesXAplicar . "
		  </td>
        </tr>
      </table>
	  <br>
	  <table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">MEDICAMENTO REMANENTE NO RU</td>
        </tr>
        <tr>
          <td align=\"left\">" . $medicamentosRemanentesCaducos . "
		  </td>
        </tr>
      </table>
	  <br>
	  <table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">MEDICAMENTO REMANENTE RU CADUCO</td>
        </tr>
        <tr>
          <td align=\"left\">" . $medicamentosReutilizableCaducos . "
		  </td>
        </tr>
      </table>
	  <br>
	  <table width=\"450\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"ventana\">
        <tr>
          <td class=\"tituloVentana\" height=\"23\">MEDICAMENTO CADUCO</td>
        </tr>
        <tr>
          <td align=\"left\">" . $medicamentosCaducos . "
		  </td>
        </tr>
      </table>
</td></tr></table>";
print($out);
?>