<?php session_start ();
include_once('lib/misFunciones.php');
?>
<?php if (isset($_GET["n_serie"])) {
	$n_serie = $_GET["n_serie"];

	$id_ingreso1 = $_GET["id_ingreso1"];
	$accion1 = $_GET["accion1"];
	$razon1 = $_GET["razon1"];
	$id_ingreso2 = $_GET["id_ingreso2"];
	$accion2 = $_GET["accion2"];
	$razon2 = $_GET["razon2"];
	$id_ingreso3 = $_GET["id_ingreso3"];
	$accion3 = $_GET["accion3"];
	$razon3 = $_GET["razon3"];
	$id_ingreso4 = $_GET["id_ingreso4"];
	$accion4 = $_GET["accion4"];
	$razon4 = $_GET["razon4"];

	$error = "";
	$med = 0;
	if ($id_ingreso1 > 0) {
		if ($accion1 == 1) {
			$query_query = "UPDATE medicamentos_inventario SET status='4' WHERE id_ingreso='" . $id_ingreso1 ."' LIMIT 1";
			$res = ejecutarSQL($query_query);
			$error = "ok";
			$med++;
		}
		if ($accion1 == 2) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por inexistencia " . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
		if ($accion1 == 3) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por error en receta " . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
		if ($accion1 == 4) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por: " . $razon1 . " "  . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
	}
	if ($id_ingreso2 > 0) {
		if ($accion2 == 1) {
			$query_query = "UPDATE medicamentos_inventario SET status='4' WHERE id_ingreso='" . $id_ingreso2 ."' LIMIT 1";
			$res = ejecutarSQL($query_query);
			$error = "ok";
			$med++;
		}
		if ($accion2 == 2) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por inexistencia " . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
		if ($accion2 == 3) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por error en receta " . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
		if ($accion2 == 4) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por: " . $razon2 . " "  . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
	}
	if ($id_ingreso3 > 0) {
		if ($accion3 == 1) {
			$query_query = "UPDATE medicamentos_inventario SET status='4' WHERE id_ingreso='" . $id_ingreso3 ."' LIMIT 1";
			$res = ejecutarSQL($query_query);
			$error = "ok";
			$med++;
		}
		if ($accion3 == 2) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por inexistencia " . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
		if ($accion3 == 3) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por error en receta " . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
		if ($accion3 == 4) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por: " . $razon3 . " "  . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
	}
	if ($id_ingreso4 > 0) {
		if ($accion4 == 1) {
			$query_query = "UPDATE medicamentos_inventario SET status='4' WHERE id_ingreso='" . $id_ingreso4 ."' LIMIT 1";
			$res = ejecutarSQL($query_query);
			$error = "ok";
			$med++;
		}
		if ($accion4 == 2) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por inexistencia " . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
		if ($accion4 == 3) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por error en receta " . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
		if ($accion4 == 4) {
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|No surtido por: " . $razon4 . " "  . date("H:i d/m/Y") . "|" . $n_serie . "','2')";
			$res = ejecutarSQL($query_query);
		}
	}

	$cuantosMedicamentos = getNMedicamentosEnReceta($n_serie);
	if ($med == $cuantosMedicamentos) {
		$query_query = "UPDATE recetas SET status='4' WHERE n_serie='" . $n_serie ."' LIMIT 1";
		$res = ejecutarSQL($query_query);
	}
	echo $error;
	
} else {
	echo "No se pudo agregar la Receta, pongase en contacto con el administrador del sistema";
}
?>
