<?php session_start ();
include_once('lib/misFunciones.php');
$receta = getDatosRecetaXserie($_GET['n_serie']);
$medico = getMedicoXid($receta['id_medico']);
$nombreMedico = $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'];
$derecho = getDatosDerecho($receta['id_derecho']);
$datosDerecho = $derecho['cedula'] . " - " . $derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho ['nombres'];
$out = "";
$medicamento = datosMedicamentoAaplicar($_GET['id_ingreso']);
	$out.= "
	  <tr>
		<td colspan=\"2\" align=\"center\">
			<table width=\"90%\" border=\"0\" class=\"ventana\">
				<tr><td class=\"tituloVentana\" colspan=\"2\">MEDICAMENTO</td></tr>
				<tr><td class=\"textosParaInputs\" align=\"right\">NOMBRE:</td><td class=\"textosParaInputsGris\" align=\"left\">" . $medicamento['clave_farmacia'] . " - " . ponerAcentos($medicamento['descripcion']) . "</td></tr>
				<tr><td class=\"textosParaInputs\" align=\"right\">DOSIS:</td><td class=\"textosParaInputsGris\" align=\"left\">" . ponerAcentos($medicamento['dosis']) . "</td></tr>
				<tr><td class=\"textosParaInputs\" align=\"right\">DIAGNOSTICO:</td><td class=\"textosParaInputsGris\" align=\"left\">" . getDiagnosticoXid($medicamento['diagnostico']) . "</td></tr>
				<tr><td class=\"textosParaInputs\" align=\"right\">ETAPA CLINICA:</td><td class=\"textosParaInputsGris\" align=\"left\">" . ponerAcentos($medicamento['etapa_clinica']) . "</td></tr>
				<tr><td class=\"textosParaInputs\" align=\"right\" width=\"200\">FECHA DE CADUCIDAD:</td><td class=\"textosParaInputsGris\" align=\"left\">" . formatoDia($medicamento['fecha_caducidad'],"fecha") . "</td></tr>
				<tr><td class=\"textosParaInputs\" align=\"right\" width=\"200\">DOSIS A APLICAR:</td><td class=\"textosParaInputsGris\" align=\"left\"><input name=\"dosis\" type=\"text\" id=\"dosis\" size=\"10\" maxlength=\"10\" /> " . $medicamento['unidad'] . "</td></tr>
		   </table>
		</td>
	  </tr>
	";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarAplicarDosis(<?php echo $receta['id_receta']; ?>,<?php echo $_GET['id_ingreso'] ?>);">
<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">APLICAR DOSIS</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">N. DE SERIE:&nbsp;</td>
    <td class="textosParaInputsGris" align="left"><?php echo $receta['n_serie']; ?></td></tr>
  <tr>
    <td class="textosParaInputs" align="right">ENTIDAD FEDERATIVA: </td>
    <td class="textosParaInputsGris" align="left"><?php echo ponerAcentos($receta['entidad']); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="textosParaInputs">FECHA DE EXPEDICION:</span> <?php echo formatoDia($receta['fecha_expedicion'],"fecha"); ?></td></tr>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO:&nbsp;</td>
    <td class="textosParaInputsGris" align="left"><?php echo ponerAcentos(getServicioXid($receta['id_servicio'])); ?></td></tr>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MEDICO:&nbsp;</td>
    <td class="textosParaInputsGris" align="left"><?php echo ponerAcentos($nombreMedico); ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">PACIENTE:&nbsp;</td>
    <td class="textosParaInputsGris" align="left"><?php echo $datosDerecho ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
  <?php echo $out ?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicio.php');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="aplicar" id="aplicar" value="Aplicar Dosis" class="botones" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>
</body>
</html>
