<?php session_start ();
include_once('lib/misFunciones.php');
?>
<?php if ((isset($_GET["n_series"])) && (isset($_GET["estado"])) && (isset($_GET["fecha"])) && (isset($_GET["servicio"])) && (isset($_GET["medico"])) && (isset($_GET["id_derecho"]))) {
	$n_serie = $_GET["n_series"];
	$estado= $_GET["estado"];
	$fecha = $_GET["fecha"];
	$servicio = $_GET["servicio"];
	$medico = $_GET["medico"];
	$entidad = $_GET["entidad"];
	$id_derecho = $_GET["id_derecho"];

	$clave1 = $_GET["clave1"];
	$dosis1 = $_GET["dosis1"];
	$diag1 = $_GET["diag1"];
	$etapa1 = $_GET["etapa1"];
	$clave2 = $_GET["clave2"];
	$dosis2 = $_GET["dosis2"];
	$diag2 = $_GET["diag2"];
	$etapa2 = $_GET["etapa2"];
	$clave3 = $_GET["clave3"];
	$dosis3 = $_GET["dosis3"];
	$diag3 = $_GET["diag3"];
	$etapa3 = $_GET["etapa3"];
	$clave4 = $_GET["clave4"];
	$dosis4 = $_GET["dosis4"];
	$diag4 = $_GET["diag4"];
	$etapa4 = $_GET["etapa4"];

	$hayError = true;
	$duplica = existeDuplica("SELECT * FROM recetas WHERE n_serie='" . $n_serie . "'");
	if (!$duplica) {
		$query_query = "INSERT INTO recetas VALUES (NULL,'" .  $n_serie . "','','" . $medico . "','" . $servicio . "','" . $id_derecho . "','" . date('Ymd') . "','" . formatoFechaBD($fecha) . "','" . $entidad . "','3','');";
		$res = ejecutarSQL($query_query);
		if ($res[0] == 0) {// no hay error
			$id_receta = getRecetaRecienAgregada($n_serie);
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|receta agregada " . date("H:i d/m/Y") . "|" . $id_receta . "','2')";
			$res = ejecutarSQL($query_query);
			
			if ($clave1 != "0") {
				$medicamento = getMedicamentoXclave($clave1);
				$query_query = "INSERT INTO recetas_conceptos VALUES (NULL,'" .  $id_receta . "','" . $medicamento['id_medicamento'] . "','" . $dosis1 . "','" . $diag1 . "','" . $etapa1 . "','');";
				$res = ejecutarSQL($query_query);
				if ($res[0] == 0) {// no hay error
					$query_query = "INSERT INTO medicamentos_inventario VALUES (NULL,'" .  $medicamento['id_medicamento'] . "','" . $id_receta . "','" . $dosis1 . "','" . $medicamento['contenido'] . "','','" . date('Ymd') . "','" . date('Hi') . "','3','');";
					$res = ejecutarSQL($query_query);
					if ($res[0] == 0) { // no hay error
						$hayError = false;
					} else { // SI hay error al agregar medicamento al inventario
						echo "No se pudo agregar el medicamento 1 al inventario local";
					}
				} else { // hay error al agregar concepto a receta
					echo "No se pudo agregar el concepto 1 a la receta";
				}
			} 

			if ($clave2 != "0") {
				$medicamento = getMedicamentoXclave($clave2);
				$query_query = "INSERT INTO recetas_conceptos VALUES (NULL,'" .  $id_receta . "','" . $medicamento['id_medicamento'] . "','" . $dosis2 . "','" . $diag2 . "','" . $etapa2 . "','');";
				$res = ejecutarSQL($query_query);
				if ($res[0] == 0) {// no hay error
					$query_query = "INSERT INTO medicamentos_inventario VALUES (NULL,'" .  $medicamento['id_medicamento'] . "','" . $id_receta . "','" . $dosis2 . "','" . $medicamento['contenido'] . "','','" . date('Ymd') . "','" . date('Hi') . "','3','');";
					$res = ejecutarSQL($query_query);
					if ($res[0] == 0) { // no hay error
						$hayError = false;
					} else { // SI hay error al agregar medicamento al inventario
						echo "No se pudo agregar el medicamento 2 al inventario local";
					}
				} else { // hay error al agregar concepto a receta
					echo "No se pudo agregar el concepto 2 a la receta";
				}
			} 

			if ($clave3 != "0") {
				$medicamento = getMedicamentoXclave($clave3);
				$query_query = "INSERT INTO recetas_conceptos VALUES (NULL,'" .  $id_receta . "','" . $medicamento['id_medicamento'] . "','" . $dosis3 . "','" . $diag3 . "','" . $etapa3 . "','');";
				$res = ejecutarSQL($query_query);
				if ($res[0] == 0) {// no hay error
					$query_query = "INSERT INTO medicamentos_inventario VALUES (NULL,'" .  $medicamento['id_medicamento'] . "','" . $id_receta . "','" . $dosis3 . "','" . $medicamento['contenido'] . "','','" . date('Ymd') . "','" . date('Hi') . "','3','');";
					$res = ejecutarSQL($query_query);
					if ($res[0] == 0) { // no hay error
						$hayError = false;
					} else { // SI hay error al agregar medicamento al inventario
						echo "No se pudo agregar el medicamento 3 al inventario local";
					}
				} else { // hay error al agregar concepto a receta
					echo "No se pudo agregar el concepto 3 a la receta";
				}
			} 

			if ($clave4 != "0") {
				$medicamento = getMedicamentoXclave($clave4);
				$query_query = "INSERT INTO recetas_conceptos VALUES (NULL,'" .  $id_receta . "','" . $medicamento['id_medicamento'] . "','" . $dosis4 . "','" . $diag4 . "','" . $etapa4 . "','');";
				$res = ejecutarSQL($query_query);
				if ($res[0] == 0) {// no hay error
					$query_query = "INSERT INTO medicamentos_inventario VALUES (NULL,'" .  $medicamento['id_medicamento'] . "','" . $id_receta . "','" . $dosis4 . "','" . $medicamento['contenido'] . "','','" . date('Ymd') . "','" . date('Hi') . "','3','');";
					$res = ejecutarSQL($query_query);
					if ($res[0] == 0) { // no hay error
						$hayError = false;
					} else { // SI hay error al agregar medicamento al inventario
						echo "No se pudo agregar el medicamento 4 al inventario local";
					}
				} else { // hay error al agregar concepto a receta
					echo "No se pudo agregar el concepto 4 a la receta";
				}
			} 

			if (!$hayError) {
				echo "Receta Agregada Correctamente";
			} else { // hay error en el alta de medicamentos o conceptos de receta
				echo "Hay errores en el alta de medicamentos al inventario local o de conceptos a la receta";
			}
		} else { // hay error mysql al agregar receta
			echo "La Receta no se pudo agregar";
		}
	} else { // else de receta duplicada en su n. de serie
		echo "El n. de serie ya existe en la base de datos";
	}
} else {
	echo "No se pudo agregar la Receta, pongase en contacto con el administrador del sistema";
}
?>
