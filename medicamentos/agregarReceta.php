<?php session_start ();
include_once('lib/misFunciones.php');
$servicios = listaServicios();
//$medicos = listaMedicos();
//$medicamentos = listaMedicamentos();
$diagnosticos = listaDiagnosticos();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarAgregarReceta();">
<input name="id_derecho" id="id_derecho" type="hidden" value="" />

<table width="730" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CAPTURAR RECETA</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">N. DE SERIE:</td>
    <td align="left"><input name="n_serie" type="text" id="n_serie" maxlength="16" tabindex="1" /></td></tr>
  <tr>
    <td class="textosParaInputs" align="right">ENTIDAD FEDERATIVA:</td>
    <td align="left"><select name="estado" id="estado" tabindex="2"></select>
    &nbsp;&nbsp;&nbsp;<span class="textosParaInputs">FECHA DE EXPEDICION: </span>
    <input name="fecha" type="text" id="fecha" maxlength="10" value="<?php echo date('d/m/Y') ?>" readonly="readonly" tabindex="3"/></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO:</td>
    <td align="left"><select name="servicio" id="servicio" onchange="javascript:opcionesMedicosReceta(this);" tabindex="4"><?php echo $servicios; ?>
    </select></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MEDICO:</td>
    <td align="left"><div id="medicos"><select name="medico" id="medico" tabindex="5"><option value="0"> </option></select></div></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><input name="cedula" type="text" id="cedula" maxlength="13" readonly="readonly" tabindex="6" />
     <input name="seleccionar" type="button" class="botones" id="seleccionar" onClick="javascript:  ocultarDiv('divAgregarDH'); mostrarDiv('buscar'); getElementById('cedulaBuscar').focus(); document.getElementById('buscar').style.height = '150px';
" value="Buscar Paciente" tabindex="7"></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_p" type="text" id="ap_p" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="ap_m" type="text" id="ap_m" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="nombre" type="text" id="nombre" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    	<table width="97%" border="0" class="ventana">
        	<tr>
            	<td class="tituloVentana" colspan="2">MEDICAMENTO 1</td>
            	<td class="tituloVentana" colspan="2">MEDICAMENTO 2</td>
            </tr>
    		<tr>
            	<td class="textosParaInputs" align="right">DIAGNOSTICO:</td>
                <td align="left"><select name="diag1" id="diag1" onchange="javascript:opcionesMedicamentosDiagnostico(this,'med1','clave1');" tabindex="8" /><?php echo $diagnosticos ?></select></td>
                <td class="textosParaInputs" align="right">DIAGNOSTICO:</td>
                <td align="left"><select name="diag2" id="diag2" onchange="javascript:opcionesMedicamentosDiagnostico(this,'med2','clave2');" tabindex="12" /><?php echo $diagnosticos ?></select></td>
            </tr>
            <tr>
            	<td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left"><div id="med1"><select name="clave1" id="clave1" tabindex="9"><option value="0"> </option></select></div></td>
                <td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left"><div id="med2"><select name="clave2" id="clave2" tabindex="13"><option value="0"> </option></select></div></td>
            </tr>
            <tr>
            	<td class="textosParaInputs" align="right">DOSIS:</td>
                <td align="left"><input name="dosis1" type="text" id="dosis1" size="20" maxlength="25" onkeyup="this.value=this.value.toUpperCase();" tabindex="10" /></td>
                <td class="textosParaInputs" align="right">DOSIS:</td>
                <td align="left"><input name="dosis2" type="text" id="dosis2" size="20" maxlength="25" onkeyup="this.value=this.value.toUpperCase();" tabindex="14" /></td>
          	</tr>
            <tr>
              <td class="textosParaInputs" align="right" width="200">ETAPA CLINICA:</td>
              <td align="left"><input name="etapa1" type="text" id="etapa1" size="20" maxlength="25" onkeyup="this.value=this.value.toUpperCase();" tabindex="11" /></td>
              <td class="textosParaInputs" align="right" width="200">ETAPA CLINICA:</td>
              <td align="left"><input name="etapa2" type="text" id="etapa2" size="20" maxlength="25" onkeyup="this.value=this.value.toUpperCase();" tabindex="15" /></td>
           	</tr>
       </table>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    	<table width="97%" border="0" class="ventana">
        	<tr>
            	<td class="tituloVentana" colspan="2">MEDICAMENTO 3</td>
            	<td class="tituloVentana" colspan="2">MEDICAMENTO 4</td>
            </tr>
     		<tr>
            	<td class="textosParaInputs" align="right">DIAGNOSTICO:</td>
                <td align="left"><select name="diag3" id="diag3" onchange="javascript:opcionesMedicamentosDiagnostico(this,'med3','clave3');" tabindex="16" /><?php echo $diagnosticos ?></select></td>
                <td class="textosParaInputs" align="right">DIAGNOSTICO:</td>
                <td align="left"><select name="diag4" id="diag4" onchange="javascript:opcionesMedicamentosDiagnostico(this,'med4','clave4');" tabindex="20" /><?php echo $diagnosticos ?></select></td>
            </tr>
           <tr>
            	<td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left"><div id="med3"><select name="clave3" id="clave3" tabindex="17"><option value="0"> </option></select></div></td>
                <td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left"><div id="med4"><select name="clave4" id="clave4" tabindex="21"><option value="0"> </option></select></div></td>
            </tr>
            <tr>
            	<td class="textosParaInputs" align="right">DOSIS:</td>
                <td align="left"><input name="dosis3" type="text" id="dosis3" size="20" maxlength="25" onkeyup="this.value=this.value.toUpperCase();" tabindex="18" /></td>
                <td class="textosParaInputs" align="right">DOSIS:</td>
                <td align="left"><input name="dosis4" type="text" id="dosis4" size="20" maxlength="25" onkeyup="this.value=this.value.toUpperCase();" tabindex="22" /></td>
          	</tr>
            <tr>
              <td class="textosParaInputs" align="right" width="200">ETAPA CLINICA:</td>
              <td align="left"><input name="etapa3" type="text" id="etapa3" size="20" maxlength="25" onkeyup="this.value=this.value.toUpperCase();" tabindex="19" /></td>
              <td class="textosParaInputs" align="right" width="200">ETAPA CLINICA:</td>
              <td align="left"><input name="etapa4" type="text" id="etapa4" size="20" maxlength="25" onkeyup="this.value=this.value.toUpperCase();" tabindex="23" /></td>
           	</tr>
       </table>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicioMedico.php');" tabindex="24" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Agregar Receta" class="botones" disabled="disabled" tabindex="25" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="buscar" style=" display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">BUSCAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
              <td align="left"><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();">
                <option value="cedula" selected="selected">C&eacute;dula</option>
                <option value="nombre">Nombre</option>
              </select>
              </td>
            </tr>
            <tr>
            <td colspan="2">
            	<div id="buscarPorCedula">
                <form id="selDH" method="POST" action="javascript: buscarDH(document.getElementById('cedulaBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                  <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                    <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="2" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
              </div>

                <div id="buscarPorNombre" style="display:none;">
                <form id="selDHN" method="POST" action="javascript: buscarDHN(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                  <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                  <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                  <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                    <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="4" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="4" align="center"></td>
                </tr>
              </table>
              </form>
              </div>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>

<form id="agregarDH" method="POST" action="javascript: agregarDHenCitaForma();">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="divAgregarDH" style=" display:none; height:0px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
              <td align="left"><input type="text" name="cedulaAgregar" id="cedulaAgregar" maxlength="10" /> / <input type="text" name="cedulaTipoAgregar" id="cedulaTipoAgregar" maxlength="2" size="5" /></td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_pAgregar" type="text" id="ap_pAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="ap_mAgregar" type="text" id="ap_mAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="nombreAgregar" type="text" id="nombreAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><input name="telefonoAgregar" type="text" id="telefonoAgregar" size="20" maxlength="10" /><span class="textosParaInputs"> EDAD: </span><input name="fecha_nacAgregar" type="text" id="fecha_nacAgregar" size="20" maxlength="3" />
    </td>
</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" size="64" maxlength="50" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estadoAgregar" id="estadoAgregar" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');"> 
    </select><span class="textosParaInputs">MUNICIPIO: </span><select name="municipioAgregar" id="municipioAgregar">
    </select>
    </td>
  </tr>
        
            <tr>
              <td colspan="2" align="center"><div id="divBotones_EstadoAgregarDH"></div>
                </td>
              </tr>
        
            <tr>
              <td colspan="2" align="center"></td>
            </tr>
          </table>
        </div>
	</td>
</tr>
</table>
</form>

</body>
</html>
