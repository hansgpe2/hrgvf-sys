<?php session_start ();
include_once('lib/misFunciones.php');
?>
<?php if ((isset($_GET["id_receta"])) && (isset($_GET["dosis"])) && (isset($_GET["id_ingreso"]))) {
	$id_receta = $_GET["id_receta"];
	$dosis = $_GET["dosis"];

	$hayError = true;
	$medicamento = datosMedicamentoRemanente($_GET["id_ingreso"]);
	$contenidoActual = getContenidoRemanente($_GET["id_ingreso"]);
	if ($contenidoActual < $dosis) {
		echo "La dosis debe ser menor o igual a " . $contenidoActual . " que es el contenido del medicamento";
	} else {
		$cantidad_remanente = $contenidoActual - $dosis;
		$query_query = "INSERT INTO medicamentos_remanentes VALUES (NULL,'" .  $_GET["id_ingreso"] . "','" . $cantidad_remanente . "','" .  date('Ymd') . "','" .  date('Hi') . "',1,'');";
		$res = ejecutarSQL($query_query);
		if ($res[0] == 0) { // no hay error
			$hayError = false;
			$query_query = "INSERT INTO logs values('','" . $_SESSION['idUsuario'] . "|reutilizo medicamento " . date("H:i d/m/Y") . "|" . $id_receta . "|" . $_GET["id_ingreso"] . "','6')";
			$res = ejecutarSQL($query_query);
		} else { // SI hay error al agregar medicamento al inventario
			echo "no se pudo aplicar la dosis remanente";
		}
	}
	if (!$hayError) {
		echo "Dosis Aplicada Correctamente";
//		$query_query = "UPDATE recetas SET status='0' WHERE id_receta='" . $id_receta . "' LIMIT 1";
//		$res = ejecutarSQL($query_query);
	}
} else {
	echo "No se pudo administrar la dosis, pongase en contacto con el administrador del sistema";
}
?>
