<?php session_start ();
include_once('lib/misFunciones.php');
$receta = getDatosRecetaXserie($_GET['n_serie']);
$paciente = getDatosDerecho($receta['id_derecho']);
$medico = getMedicoXid($receta['id_medico']);
$medicamento = getMedicamentoXingresar($_GET['id_ingreso']);
$out = "";
			$n = $j+1;
			$out.="<table width=\"97%\" border=\"0\" class=\"ventana\">
						<tr>
							<td class=\"tituloVentana\" colspan=\"2\">MEDICAMENTO 1</td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\">DATOS MEDICAMENTO:</td>
							<td align=\"left\">" . $medicamento['clave_farmacia'] . " - " . $medicamento['descripcion'] . "</td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\">DOSIS:</td>
							<td align=\"left\">" . $medicamento['dosis'] . "</td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\">DIAGNOSTICO:</td>
							<td align=\"left\">" . getDiagnosticoXid($medicamento['diagnostico']) . "</td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\">ETAPA CLINICA:</td>
							<td align=\"left\">" . $medicamento['etapa_clinica'] . "</td>
						</tr>
						<tr>
							<td class=\"textosParaInputs\" align=\"right\" width=\"200\" valign=\"top\">FECHA DE CADUCIDAD:</td>
							<td align=\"left\"><input name=\"fecha\" id=\"fecha\" type=\"text\" size=\"10\" maxlength=\"10\" /></td>
						</tr>
				   </table>";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>
<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarRegistrarMedicamento('<?php echo $_GET['n_serie'] ?>','<?php echo $_GET['id_ingreso'] ?>');">
<table width="730" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">SURTIR RECETA</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">N. DE SERIE:</td>
    <td align="left"><?php echo $receta['n_serie'] ?></td></tr>
  <tr>
    <td class="textosParaInputs" align="right">ENTIDAD FEDERATIVA:</td>
    <td align="left"><?php echo ponerAcentos($receta['entidad']) ?>
    &nbsp;&nbsp;&nbsp;<span class="textosParaInputs">FECHA DE EXPEDICION: </span>
    <?php echo formatoDia($receta['fecha_expedicion'],"fecha"); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO:</td>
    <td align="left"><?php echo getServicioXid($receta['id_servicio']); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MEDICO:</td>
    <td align="left"><?php echo ponerAcentos(strtoupper($medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'])); ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><?php echo $paciente['cedula'] . "/" . $paciente['cedula_tipo'] ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><?php echo ponerAcentos($paciente['ap_p'] . " " . $paciente['ap_m'] . " " . $paciente['nombres']); ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><?php echo $out ?>	
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicio.php');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Registrar Medicamento" class="botones" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>
</body>
</html>
