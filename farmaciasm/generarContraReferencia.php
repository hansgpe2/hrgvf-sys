<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="lib/misEstilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php
function statusParaReceta($id_medico, $id_derecho, $id_medicamento, $cantidad, $n_med) {
	$ret = array(
		'status' => '0', // 0=si se puede usar el medicamento para la receta, 1=error tiene tratamiento, 2=error no hay medicamento, 9=error no hay folios
		'txt' => '',
		'extra' => ''
	);
	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
	if ($folio["folio_actual"] < $folio["folio_final"]) { // si está el folio actual dentro de los folios disponibles entonces si podríamos agregar la receta
		if (tieneTratamiento($id_medicamento, $id_derecho)) {
			$ret['status'] = 1;
			$ret['txt'] = '<span style="color:#ff0000">- Tiene tratamiento vigente del medicamento ' . $n_med . '</span><br>';
		}
		if (!hayMedicamento($id_medicamento, $cantidad)) { 
			$ret['status'] = 2;
			$ret['txt'] = '<span style="color:#ff0000">- El medicamento ' . $n_med . ' no tiene existencias y se va a generar como vale</span><br>';
			$ret['extra'] = 'N';
		}
	} else { // mensaje de error por falta de folios
		$ret['status'] = 9;
		$ret['txt'] = '<span style="color:#ff0000">- No se generó Receta para Medicamento ' . $n_med . ' por falta de folios por parte del médico, pida se le asignen mas folios</span><br>';
	}
	return $ret;	
}
$nav=$_SERVER['HTTP_USER_AGENT'];
include "lib/misFunciones.php";
$opCantidades = '<option value="0" selected="selected"></option>';
	for ($i=1; $i<=60; $i++) {
		$opCantidades .= '<option value="' . $i . '">' . $i . '</option>';
	}
if(isset($_GET['id_horario'])){
$horario=$_GET['id_horario'];
$cita=getCita($horario,date("Ymd"));
}
else
{
	$cita=getCitaExt($_GET['idCita']);
}
$derecho=getDatosDerecho($cita['id_derecho']);
$idserv=$_SESSION['idServ'];
$serv=getServicioXid($idserv);
$medi=getMedicoXid($_SESSION['idDr']);
?>
<div align="center">
<form action="formatoContrarreferencia.php" name="contraReferencia" id="contraReferencia" method="post" target="_blank">
<input type="hidden" name="id_cita" id="id_cita" value="<?php echo $_GET['id_cita'] ?>" />
<input type="hidden" name="tipo_cita" id="tipo_cita" value="<?php echo $_GET['tipo_cita'] ?>" />
    <table width="100%" height="100%">
        <tr><td colspan="3" class="tituloVentana">FORMATO DE CONTRARREFERENCIA</td></tr>
        <tr><td width="16%" class="textosParaInputs">CEDULA</td><td colspan="2"><?php echo $derecho['cedula']."/".$derecho['cedula_tipo']; ?><input type="hidden" name="idDerecho" id="idDerecho" value="<?php echo $cita['id_derecho'] ?>" /></td></tr>
        <tr><td class="textosParaInputs">NOMBRE</td><td colspan="2"><?php echo ponerAcentos($derecho['ap_p']." ".$derecho['ap_m']." ".$derecho['nombres']); ?></td></tr>
        <tr><td class="textosParaInputs">SERVICIO</td><td colspan="2"><?php echo $serv ?><input type="hidden" name="servicio" id="servicio" value="<?php echo $idserv; ?>"  /><input type="hidden" name="fecha" id="fecha" value="<?php echo date("Ymd"); ?>"  /></td></tr>
        <tr><td class="textosParaInputs">RESUMEN DATOS CLINICOS</td><td colspan="2"><textarea name="datClin" cols="150" rows="10" id="datClin" tabindex="1" maxlength="1000"></textarea></td></tr>
        <tr><td class="textosParaInputs">DIAGNOSTICO REFERENCIA</td><td colspan="2"><textarea name="diagnosticoRef" cols="150" rows="10"  id="diagnosticoRef" tabindex="2" maxlength="1000"></textarea></td></tr>
        <tr><td class="textosParaInputs">DIAGNOSTICO CONTRARREFERENCIA</td><td colspan="2"><textarea name="diagnostico" cols="150" rows="10"  id="diagnostico" tabindex="3" maxlength="1000"></textarea></td></tr>
        <tr><td class="textosParaInputs">SINTESIS DE LA EVOLUCION</td><td colspan="2"><textarea name="evolucion" cols="150" rows="10" id="evolucion" tabindex="4" maxlength="1000"></textarea></td></tr>
        <tr><td class="textosParaInputs">TRATAMIENTO INSTITUIDO</td><td width="28%"><textarea name="tratamiento" cols="50" rows="30" id="tratamiento" tabindex="5" ></textarea></td>
  <td width="56%"><table width="100%" border="2" class="ventana">
    <tr>
      <td colspan="5" class="tablaFondoAzul">Medicamentos referidos para la clínica</td>
      </tr>
    <tr class="TD_REP_FN">
      <td width="6%"><p>
        <label for="todos"></label>
        </p></td>
      <td width="44%">Nombre</td>
      <td width="12%">Tipo de Tratamiento</td>
      <td width="13%">Tratamiento</td>
      <td width="25%">Cantidad de Cajas</td>
    </tr>
    <tr>
      <td height="45">1
        <input name="m1" type="checkbox" id="m1" tabindex="6" onClick="ActivarMedicamento('1');" value="on">
        <label for="m1"></label></td>
      <td height="45"><div class="ui_widget">
        <input name="medicamento1" type="text" id="medicamento1" tabindex="7" onKeyUp="SeleccionarMedicina(this.id,1)">
        <div id="resultados_1" class="miselect2" style="display:compact"></div>
        </div>
        <span class="mi_select">
          <input name="medi_1" id="medi_1" type="hidden" value="-1" />
          </span></td>
      <td><label for="tratamiento1"></label>
        <select name="tratamiento1" disabled="disabled" id="tratamiento1" tabindex="8" onChange="CambiarTratamiento('1');">
          <option value="-1"></option>
          <option value="1">crónico</option>
          <option value="0">temporal</option>
        </select></td>
      <td><table border="0" id="tratamiento1"><tr><td>Periodo 
        </td>
            <td><select name="frecuencia_1" disabled="disabled" id="frecuencia_1" tabindex="9" onChange="actualizarPeriodoContra('1');">
              <option value="DIAS" selected="selected">Diario</option>
              <option value="SEMANAS">Semanal</option>
              <option value="MESES">Mensual</option>
</select></td>
        </tr>
        <tr><td>
          Dosis
              </td>
          <td><input name="dosis_1" type="text" id="dosis_1" tabindex="10" onBlur="return compruebaNumeroContra(this,'1')" onKeyDown="return validarNumero(event, this.value);" size="5">
            &nbsp;&nbsp;
            <div style="display:inline" class="textosParaInputs" id="presentacion_1"></div></td>
        <tr>
          <td>Tratamiento</td>
          <td align="left" class="ventana"><div style="display:inline" id="divTrat_1">
            <select name="dias_1" disabled="disabled" id="dias_1" tabindex="11" onchange=" actualizarCantidadContra('1')" onBlur="actualizarCantidadContra('1')">
              <?php echo $opCantidades ?>
            </select>
          </div>
            <div id="periodo_1" style="display:inline" class="textosParaInputs">DIAS
            </div><input name="unidad_1" id="unidad_1" type="hidden" value="" />
              <input name="uni_1" id="uni_1" type="hidden" value="" />
              <input name="grupo_1" id="grupo_1" type="hidden" value="" />
              <input name="cantidad_1" id="cantidad_1" type="hidden" value="0" />
              <input name="diasTratamiento_1" id="diasTratamiento_1" type="hidden" value="1" /></td>
          </table></td>
      <td>
        <input name="divcantidad_1" type="text" id="divcantidad_1" readonly></td>
    </tr>
    <tr>
      <td height="45">2
        <input name="m2" type="checkbox" id="m2" tabindex="11" onClick="ActivarMedicamento('2');" value="on"></td>
      <td height="45"><div class="ui_widget">
        <label for="medicamento2"></label>
        <input name="medicamento2" type="text" id="medicamento2" tabindex="12" onKeyUp="SeleccionarMedicina(this.id,2)">
        <div id="resultados_2" class="miselect2" style="display:compact"></div>
        </div>
        
        <input name="medi_2" id="medi_2" type="hidden" value="-1" />
        </td>
      <td><select name="tratamiento2" disabled="disabled" id="tratamiento2" tabindex="13" onChange="CambiarTratamiento('2');">
        <option value="-1"></option>
        <option value="1">crónico</option>
        <option value="0">temporal</option>
      </select></td>
      <td><table border="0" id="tratamiento1"><tr><td>Periodo 
        </td>
            <td><select name="frecuencia_2" disabled="disabled" id="frecuencia_2" tabindex="13" onChange="javascript: actualizarPeriodoContra('2');">
              <option value="DIAS" selected="selected">Diario</option>
              <option value="SEMANAS">Semanal</option>
              <option value="MESES">Mensual</option>
</select></td>
        </tr>
        <tr><td>
          Dosis
              </td>
          <td><input name="dosis_2" type="text" id="dosis_2" tabindex="14" onBlur="return compruebaNumeroContra(this,'2')" onKeyDown="return validarNumero(event, this.value);" size="5">
            &nbsp;&nbsp;
            <div style="display:inline" class="textosParaInputs" id="presentacion_2"></div></td>
        <tr>
          <td>Tratamiento</td>
          <td align="left" class="ventana"><div style="display:inline" id="divTrat_2">
            <select name="dias_2" disabled="disabled" id="dias_2" tabindex="15" onchange="javascript: actualizarCantidadContra('2')" onBlur="javascript: actualizarCantidadContra('2')">
              <?php echo $opCantidades ?>
            </select>
          </div>
            <div id="periodo_2" style="display:inline" class="textosParaInputs">DIAS
            </div><input name="unidad_2" id="unidad_2" type="hidden" value="" />
              <input name="uni_2" id="uni_2" type="hidden" value="" />
              <input name="grupo_2" id="grupo_2" type="hidden" value="" />
              <input name="cantidad_2" id="cantidad_2" type="hidden" value="0" />
              <input name="diasTratamiento_2" id="diasTratamiento_2" type="hidden" value="1" /></td>
          </table></td>
      <td>
        <input name="divcantidad_2" type="text" id="divcantidad_2" readonly></td>
    </tr>
    <tr>
      <td height="45">3
        <input name="m3" type="checkbox" id="m3" tabindex="16" onClick="ActivarMedicamento('3');" value="on"></td>
      <td><div class="ui_widget">
        <label for="medicamento1"></label>
        <div class="ui_widget">
          <label for="medicamento7"></label>
          <input name="medicamento3" type="text" id="medicamento3" tabindex="17" onKeyUp="SeleccionarMedicina(this.id,3)">
          <div id="resultados_7" class="miselect2" style="display:compact"></div>
        </div>
        <div id="resultados_3" class="miselect2" style="display:compact"></div>
        <span class="mi_select4">
          <input name="medi_3" id="medi_3" type="hidden" value="-1" />
        </span></td>
      <td><select name="tratamiento3" disabled="disabled" id="tratamiento3" tabindex="18" onChange="CambiarTratamiento('3');">
        <option value="-1"></option>
        <option value="1">crónico</option>
        <option value="0">temporal</option>
      </select></td>
      <td><table border="0" id="tratamiento1"><tr><td>Periodo 
        </td>
            <td><select name="frecuencia_3" disabled="disabled" id="frecuencia_3" tabindex="19" onChange="javascript: actualizarPeriodoContra('3');">
              <option value="DIAS" selected="selected">Diario</option>
              <option value="SEMANAS">Semanal</option>
              <option value="MESES">Mensual</option>
</select></td>
        </tr>
        <tr><td>
          Dosis
              </td>
          <td><input name="dosis_3" type="text" id="dosis_3" tabindex="20" onBlur="return compruebaNumeroContra(this,'3')" onKeyDown="return validarNumero(event, this.value);" size="5">
            &nbsp;&nbsp;
            <div style="display:inline" class="textosParaInputs" id="presentacion_3"></div></td>
        <tr>
          <td>Tratamiento</td>
          <td align="left" class="ventana"><div style="display:inline" id="divTrat_3">
            <select name="dias_3" disabled="disabled" id="dias_3" tabindex="21" onchange="javascript: actualizarCantidadContra('3')">
              <?php echo $opCantidades ?>
            </select>
          </div>
            <div id="periodo_3" style="display:inline" class="textosParaInputs">DIAS
            </div><input name="unidad_3" id="unidad_3" type="hidden" value="" />
              <input name="uni_3" id="uni_3" type="hidden" value="" />
              <input name="grupo_3" id="grupo_3" type="hidden" value="" />
              <input name="cantidad_3" id="cantidad_3" type="hidden" value="0" />
              <input name="diasTratamiento_3" id="diasTratamiento_3" type="hidden" value="1" /></td>
          </table></td>
      <td>
        <input name="divcantidad_3" type="text" id="divcantidad_3" readonly></td>
    </tr>
    <tr>
      <td height="45">4
        <input name="m4" type="checkbox" id="m4" tabindex="24" onClick="ActivarMedicamento('4');" value="on"></td>
      <td><div class="ui_widget">
        <label for="medicamento4"></label>
        <input name="medicamento4" type="text" id="medicamento4" tabindex="25" onKeyUp="SeleccionarMedicina(this.id,4)">
        <div id="resultados_4" class="miselect2" style="display:compact"></div>
      </div>
        <span class="mi_select">
        <input name="medi_4" id="medi_4" type="hidden" value="-1" />
        </span></td>
      <td><select name="tratamiento4" disabled id="tratamiento4" tabindex="26" onChange="CambiarTratamiento('4');">
        <option value="-1"></option>
        <option value="1">crónico</option>
        <option value="0">temporal</option>
      </select></td>
      <td><table border="0" id="tratamiento9">
        <tr>
          <td>Periodo </td>
          <td><select name="frecuencia_4" disabled id="frecuencia_4" tabindex="27" onChange="javascript: actualizarPeriodoContra('4');">
            <option value="DIAS" selected="selected">Diario</option>
            <option value="SEMANAS">Semanal</option>
            <option value="MESES">Mensual</option>
          </select></td>
        </tr>
        <tr>
          <td> Dosis </td>
          <td><input name="dosis_4" type="text" id="dosis_4" tabindex="28" onBlur="return compruebaNumeroContra(this,'4')" onKeyDown="return validarNumero(event, this.value);" size="5">
            &nbsp;&nbsp;
            <div style="display:inline" class="textosParaInputs" id="presentacion_4"></div></td>
        <tr>
          <td>Tratamiento</td>
          <td align="left" class="ventana"><div style="display:inline" id="divTrat_4">
            <select name="dias_4" disabled id="dias_4" tabindex="29" onchange="javascript: actualizarCantidadContra('4')">
              <?php echo $opCantidades ?>
            </select>
          </div>
            <div id="periodo_4" style="display:inline" class="textosParaInputs">DIAS </div>
            <input name="unidad_4" id="unidad_4" type="hidden" value="" />
            <input name="uni_4" id="uni_4" type="hidden" value="" />
            <input name="grupo_4" id="grupo_4" type="hidden" value="" />
            <input name="cantidad_4" id="cantidad_4" type="hidden" value="0" />
            <input name="diasTratamiento_4" id="diasTratamiento_4" type="hidden" value="1" /></td>
        </table></td>
      <td>
        <input name="divcantidad_4" type="text" id="divcantidad_4" readonly></td>
    </tr>
    <tr>
      <td height="45">5
        <input name="m5" type="checkbox" id="m5" tabindex="30" onClick="ActivarMedicamento('5');" value="on"></td>
      <td><div class="ui_widget">
        <label for="medicamento5"></label>
        <input name="medicamento5" type="text" id="medicamento5" tabindex="31" onKeyUp="SeleccionarMedicina(this.id,5)">
        <div id="resultados_5" class="miselect2" style="display:compact"></div>
      </div>
        <span class="mi_select">
        <input name="medi_5" id="medi_5" type="hidden" value="-1" />
        </span></td>
      <td><select name="tratamiento5" disabled id="tratamiento5" tabindex="32" onChange="CambiarTratamiento('5');">
        <option value="-1"></option>
        <option value="1">crónico</option>
        <option value="0">temporal</option>
      </select></td>
      <td><table border="0" id="tratamiento7">
        <tr>
          <td>Periodo </td>
          <td><select name="frecuencia_5" disabled="disabled" id="frecuencia_5" tabindex="33" onChange="javascript: actualizarPeriodoContra('5');">
            <option value="DIAS" selected="selected">Diario</option>
            <option value="SEMANAS">Semanal</option>
            <option value="MESES">Mensual</option>
          </select></td>
        </tr>
        <tr>
          <td> Dosis </td>
          <td><input name="dosis_5" type="text" id="dosis_5" tabindex="34" onBlur="return compruebaNumeroContra(this,'5')" onKeyDown="return validarNumero(event, this.value);" size="5">
            &nbsp;&nbsp;
            <div style="display:inline" class="textosParaInputs" id="presentacion_5"></div></td>
        <tr>
          <td>Tratamiento</td>
          <td align="left" class="ventana"><div style="display:inline" id="divTrat_5">
            <select name="dias_5" disabled="disabled" id="dias_5" tabindex="35" onchange="javascript: actualizarCantidadContra('5')">
              <?php echo $opCantidades ?>
            </select>
          </div>
            <div id="periodo_5" style="display:inline" class="textosParaInputs">DIAS </div>
            <input name="unidad_5" id="unidad_5" type="hidden" value="" />
            <input name="uni_5" id="uni_5" type="hidden" value="" />
            <input name="grupo_5" id="grupo_5" type="hidden" value="" />
            <input name="cantidad_5" id="cantidad_5" type="hidden" value="0" />
            <input name="diasTratamiento_5" id="diasTratamiento_5" type="hidden" value="1" /></td>
        </table></td>
      <td>
        <input name="divcantidad_5" type="text" id="divcantidad_5" readonly></td>
    </tr>
    <tr>
      <td height="45">6
        <input name="m6" type="checkbox" id="m6" tabindex="36" onClick="ActivarMedicamento('6');" value="on"></td>
      <td><div class="ui_widget">
        <label for="medicamento6"></label>
        <input name="medicamento6" type="text" id="medicamento6" tabindex="37" onKeyUp="SeleccionarMedicina(this.id,6)">
        <div id="resultados_6" class="miselect2" style="display:compact"></div>
      </div>
        <span class="mi_select">
        <input name="medi_6" id="medi_6" type="hidden" value="-1" />
        </span></td>
      <td><select name="tratamiento6" disabled id="tratamiento6" tabindex="38" onChange="CambiarTratamiento('6');">
        <option value="-1"></option>
        <option value="1">crónico</option>
        <option value="0">temporal</option>
      </select></td>
      <td><table border="0" id="tratamiento8">
        <tr>
          <td>Periodo </td>
          <td><select name="frecuencia_6" disabled id="frecuencia_6" tabindex="39" onChange="javascript: actualizarPeriodoContra('6');">
            <option value="DIAS" selected="selected">Diario</option>
            <option value="SEMANAS">Semanal</option>
            <option value="MESES">Mensual</option>
          </select></td>
        </tr>
        <tr>
          <td> Dosis </td>
          <td><input name="dosis_6" type="text" id="dosis_6" tabindex="40" onBlur="return compruebaNumeroContra(this,'6')" onKeyDown="return validarNumero(event, this.value);" size="5">
            &nbsp;&nbsp;
            <div style="display:inline" class="textosParaInputs" id="presentacion_6"></div></td>
        <tr>
          <td>Tratamiento</td>
          <td align="left" class="ventana"><div style="display:inline" id="divTrat_6">
            <select name="dias_6" disabled id="dias_6" tabindex="41" onchange="javascript: actualizarCantidadContra('6')">
              <?php echo $opCantidades ?>
            </select>
          </div>
            <div id="periodo_6" style="display:inline" class="textosParaInputs">DIAS </div>
            <input name="unidad_6" id="unidad_6" type="hidden" value="" />
            <input name="uni_6" id="uni_6" type="hidden" value="" />
            <input name="grupo_6" id="grupo_6" type="hidden" value="" />
            <input name="cantidad_6" id="cantidad_6" type="hidden" value="0" />
            <input name="diasTratamiento_6" id="diasTratamiento_6" type="hidden" value="1" /></td>
        </table></td>
      <td>
        <input type="text" name="divcantidad_6" id="divcantidad_6"></td>
    </tr>
  </table></td>
</tr>
<tr><td class="textosParaInputs">RECOMENDACIONES</td><td colspan="2"><textarea name="recomendaciones" cols="150" rows="10" id="recomendaciones" tabindex="42" /></td></tr>
<?php 
	print '<tr><td class="textosParaInputs">Unidad Medica</td><td><select name="Unidad" id="Unidad" tabindex="43">'.
	CargarUnidadesMedicas($derecho['estado']).'</select></td></tr>';
?>
<tr>
  <td class="textosParaInputs" align="left">Requiere continuar tratamiento en 3° nivel</td>
  <td colspan="2"><select name="cont" id="cont" tabindex="43"><option value="0">no</option><option value="1">s&iacute;</option></select></td></tr>    
<tr><td colspan="3" align="center"><input type="button" name="regresar" id="regresar" value="regresar" onclick="regresarARecetas()" class="botones" />&nbsp;  <input name="" type="button" class="botones" tabindex="44" onclick="CrearReferenciaConfirmar()" value="Guardar Contrarreferencia" /></td></tr>
</table></form>
</body>
</html>