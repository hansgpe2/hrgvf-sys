<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<?php
if ((isset($_GET["cantidades"])) && (isset($_GET["unidades"])) && (isset($_GET["dias"])) && (isset($_GET["medicamentos"])) && (isset($_GET["indicaciones"])) && (isset($_GET["id_derecho"])) && (isset($_GET["serie"])) && (isset($_GET["folio"]))) {
	$cant = $_GET["cantidades"];
	$unid= $_GET["unidades"];
	$di = $_GET["dias"];
	$medi = $_GET["medicamentos"];
	$indi = $_GET["indicaciones"];
	$nuMed = $_GET["numerosMed"];
	$cantidades = explode(",",$cant);
	$unidades = explode(",",$unid);
	$dias = explode(",",$di);
	$medicamentos = explode(",",$medi);
	$indicaciones = explode(",",$indi);
	$numerosMed = explode(",",$nuMed);
	$id_derecho = $_GET["id_derecho"];
	$serie = $_GET["serie"];
//	$folio = $_GET["folio"];
	$id_medico = $_SESSION["idDr"];
	$id_servicio = regresarIdServicio($id_medico);
	$cuantasRecetas = count($cantidades) / 2;
	if (is_integer($cuantasRecetas)) $cuantasRecetas = $cuantasRecetas; else $cuantasRecetas = ceil($cuantasRecetas); // sube el valor de cuantas recetas cuando no es entero;
	$ret = '';
	$nMed = 0;
	$id_receta = '';
	for($i=0; $i<$cuantasRecetas; $i++) {
		$folio = getFolioActual($_SESSION["idDr"]); // traemos los valores del folio del dr
		if ($folio["folio_actual"] < $folio["folio_final"]) { // si está el folio actual dentro de los folios disponibles entonces si agregarmos la receta
			$hayExistencias = true;
			$enTratamiento = true;
			if (tieneTratamiento($medicamentos[$nMed], $id_derecho)) {
				$ret .= '<span style="color:#ff0000">- Tiene tratamiento vigente del medicamento 1</span><br>';
				$enTratamiento = false;
			}
			if (isset($cantidades[$nMed+1])) {
				if (tieneTratamiento($medicamentos[$nMed+1], $id_derecho)) {
					$ret .= '<span style="color:#ff0000">- Tiene tratamiento vigente del medicamento 2</span><br>';
					$enTratamiento = false;
				}
			}
// CODIGO PARA COMPROBAR EXISTENCIAS DE MEDICAMENTOS			
			if (!hayMedicamento($medicamentos[$nMed], $cantidades[$nMed])) { 
				$ret .= '<span style="color:#ff0000">- No hay existencias suficientes del medicamento 1</span><br>';
				$hayExistencias = false;
			}
			if (isset($cantidades[$nMed+1])) {
				if (!hayMedicamento($medicamentos[$nMed+1], $cantidades[$nMed+1])) {
					$ret .= '<span style="color:#ff0000">- No hay existencias suficientes del medicamento 2</span><br>';
					$hayExistencias = false;
				}
			}

// CODIGO PARA COMPROBAR EXISTENCIAS DE MEDICAMENTOS			
			if (($hayExistencias) && ($enTratamiento)) {
				$folioSiguiente = $folio["folio_actual"] + 1;
				$codigo_barras = getCodigoBarras();
				$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . date("Ymd") . "','" . date("H:i:s") . "','" . $codigo_barras . "','" . ENTIDAD_FEDERATIVA . "','" . CLAVE_UNIDAD_MEDICA . "','','','" . $_SESSION["idUsuario"] . "','0','1','" . $_GET["tipo_receta"] . "');";
				$res = ejecutarSQL($sql);
				$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
				$res = ejecutarSQL($sql);
				$sigCodigo = intval($codigo_barras)+1;
				$sql = "UPDATE codigo_barras SET codigo='" . ponerCeros($sigCodigo,12) . "' LIMIT 1";
				$res = ejecutarSQL($sql);
				$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
				$ret .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
				$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $medicamentos[$nMed] . "','" . $cantidades[$nMed] . "','" . $unidades[$nMed] . "','" . $dias[$nMed] . "','" . $indicaciones[$nMed] . "','','','','');";
				$res = ejecutarSQL($sql);
				restaExistenciasMedicamentos($medicamentos[$nMed], $cantidades[$nMed]);
				$ret .= '- Medicamento ' . $numerosMed[$nMed] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	//	si existe el segundo medicamento se agrega
				if (isset($cantidades[$nMed+1])) {
					$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $medicamentos[$nMed+1] . "','" . $cantidades[$nMed+1] . "','" . $unidades[$nMed+1] . "','" . $dias[$nMed+1] . "','" . $indicaciones[$nMed+1] . "','','','','');";
					$res = ejecutarSQL($sql);
					restaExistenciasMedicamentos($medicamentos[$nMed+1], $cantidades[$nMed+1]);
					$ret .= '- Medicamento ' . $numerosMed[$nMed+1] . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
				}
			} else { // se terminó el medicamento en lo que duraron haciendo la receta, se los marcó como existentes pero se terminó o no hay suficiente para surtir la receta
				if (!$hayExistencias) $ret .= '<span style="color:#ff0000">- No se generó la receta por falta de existencia de medicamentos</span>'; else $ret .= '<span style="color:#ff0000">- No se generó la receta porque el derechohabiente tiene tratamiento en curso</span>';
			}
		} else { // mensaje de error por falta de folios
			if (isset($numerosMed[$nMed])) $med1 = $numerosMed[$nMed];
			if (isset($numerosMed[$nMed+1])) $med2 = " y Medicamento " . $numerosMed[$nMed+1]; else $med2 = "";
			$ret .= '<span style="color:#ff0000">- No se generó Receta para Medicamento ' . $numerosMed[$nMed] . $med2 . " por falta de folios por parte del médico, pida se le asignen mas folios</span><br>";
		}
		$nMed = $nMed + 2;
	}
	
	print($ret . "|" . $id_receta);

} else {
	echo '<span style="color:#ff0000">No se pudo agregar la Receta, pongase en contacto con el administrador del sistema</span>';
}
?>
