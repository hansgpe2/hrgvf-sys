<?php
include_once('lib/misFunciones.php');

$datosUsuario = getUsuarioXid($_SESSION['idUsuario']);
setcookie("idMedico", $_SESSION['idUsuario']);
$datosMedico = getMedicoXid($datosUsuario["id_medico"]);
$id_servicio = regresarIdServicio($datosUsuario["id_medico"]);
$id_consultorio = regresarIdConsultorio($datosUsuario["id_medico"]);
$servicio = getServicioXid($id_servicio);
$consultorio = getConsultorioXid($id_consultorio);
$_SESSION['idDr'] = $datosUsuario["id_medico"];
$_SESSION['idServ'] = $id_servicio;
$_SESSION['IdCon'] = $id_consultorio;
$out = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"center\" class=\"tituloVentana\">";
$out.= "CITAS DE " . ponerAcentos(strtoupper($datosMedico['titulo'] . " " . $datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']) . "<br>");
$out.= "Consultorio: " . strtoupper($consultorio['nombre']) . " | Servicio: " . strtoupper($servicio);
$out .= "</td></tr></table><br>";

print($out);

$hoy = date('Ymd');
//$hoy = date('20121111');
?>



<center>
    <table width="800" border="0" height="600">
        <tr>
            <td width="687">
                <table width="630" border="0" cellspacing="0" cellpadding="0" class="ventana">
                    <tr>
                        <td class="tituloVentana" height="23"><?php echo formatoDia($hoy, 'tituloCitasXdia'); ?></td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <?php
							if($_SESSION['idServ']==9 || $_SESSION['idServ']==63 || $_SESSION['idServ']==18|| $_SESSION['idServ']==19 || $_SESSION['idServ'] == 65 || $_SESSION['idServ']==27 || $_SESSION['idServ']==45)
							$out="<script>mostrarDiv('CanCitas');</script>\n";
							else
                            $out = "";
                            $out.= "
<table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
                            $horario = citasXdia($hoy, $_SESSION['idDr']);
                            $citasDelDia = count($horario);
                            if ($citasDelDia > 0) {
                                $extempo = '';
                                /* 		$extempo = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <a href=\"javascript:agregarRecetaExtemporanea(" . $hoy . ");\" title=\"Capturar Receta Extempor&aacute;nea\" class=\"textoCitaExtemporanea\">Capturar Receta Extempor&aacute;nea <img src=\"diseno/Symbol-Add.png\" alt=\"Capturar Receta Extempor&aacute;nea\" border=\"0\" /></a>";
                                 */ for ($i = 0; $i < $citasDelDia; $i++) {
                                    $total = 0;
                                    $datosCita = citasOcupadasXdia($horario[$i]['id_horario'], $hoy);
                                    $datosDerecho = getDatosDerecho($datosCita['id_derecho']);
                                    $datosUsuario = getUsuarioCitaXid($datosCita['id_usuario']);
                                    if ($datosCita['id_derecho'] == "-1") { //no hay cita a x hora
                                        if ($horario[$i]['tipo_cita'] == "0")
                                            $claseParaDia = "citaXdiaLIBPRV"; //primera vez libre
                                        if ($horario[$i]['tipo_cita'] == "1")
                                            $claseParaDia = "citaXdiaLIBSUB"; //primera vez libre
                                        if ($horario[$i]['tipo_cita'] == "2")
                                            $claseParaDia = "citaXdiaLIBPRO"; //primera vez libre
                                        $nombre = "";
                                        $datos = "";
                                        $botones = "";
                                    } else { // hay cita
                                        $botones = "";
                                        $nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
                                        $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
                                        $botones = "<table><tr><td><a href='javascript:crearCalendario(\"" . $horario[$i]['id_horario'] . "\");' class='botones_menu' title='Agendar Cita'><img src='diseno/agendarCita.png'  width='30' height='30'/>Agendar Cita Subsecuente</a></td><td><a href=\"javascript: agregarReceta('" . $horario[$i]['id_horario'] . "');\" title=\"Capturar Receta\" class='botones_menu'><img src=\"diseno/nuevaReceta.png\" border=\"0\" width='30' height='30'><br>Agregar Receta</a></td><td><a href='javascript:agregarConstancia(" . $horario[$i]['id_horario'] . ");' class='botones_menu'><img src='diseno/generarConstancia.png' width='30' height='30' /><br>Agregar Constancia</a></td></tr></table>";
                                        $botonesSinCita = "<table border='1'><tr><td><a href=\"javascript: agregarReceta('" . $horario[$i]['id_horario'] . "');\" title=\"Capturar Receta\" class='botones_menu'><img src=\"diseno/nuevaReceta.png\" border=\"0\" width='30' height='30'><br>Agregar Receta</a></td><td><a href='javascript:agregarConstancia(" . $horario[$i]['id_horario'] . ");' class='botones_menu'><img src='diseno/generarConstancia.png' width='30' height='30' /><br>Agregar Constancia</a></td></tr></table>";
                                        if ($horario[$i]['tipo_cita'] == 0)
                                            $claseParaDia = "citaXdiaPRV";
                                        if ($horario[$i]['tipo_cita'] == 1)
                                            $claseParaDia = "citaXdiaSUB";
                                        if ($horario[$i]['tipo_cita'] == 2)
                                            $claseParaDia = "citaXdiaPRO";
                                        $sql = "SELECT * FROM citas_medico WHERE id_derecho=" . $datosCita['id_derecho'] . " AND fecha_cita=" . date("Ymd");
                                        $query = ejecutarSQLAgenda($sql);
                                        if ($query != 0)
                                            $total = count($query);
                                    }
                                    if (($datosCita['status'] > 2) && ($datosCita['status'] < 7)) { // esta de vacaciones, en congreso, etc.
                                        $nombre = $statusCitas[$datosCita['status']];
                                        $datos = $datosCita['observaciones'];
                                        $botones = "";
                                        $claseParaDia = "citaXdiaNOT";
                                    }

                                    if ($total == 0 && ($_SESSION['idServ']==9 || $_SESSION['idServ']==63 || $_SESSION['idServ']==18 || $_SESSION['idServ']==19 || $_SESSION['idServ'] == 65 || $_SESSION['idServ']==27 || $_SESSION['idServ']==45)) {
                                        $out.="
				  <tr>
					<td class=\"" . $claseParaDia . "\">
						<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td width=\"80\" class=\"citaXdiaHora\">" . formatoHora($horario[$i]['hora_inicio']) . " - " . formatoHora($horario[$i]['hora_fin']) . "</td>
							<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
							<td width=\"80\" rowspan=\"2\">" . $botones . "</td>
						  </tr>
						  <tr>
							<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\" >" . ponerAcentos($datos) . "</td>
						  </tr>
						</table>
					</td>
				  </tr>";
				  
                                    } else {
                                        $out.="
				  <tr>
					<td class=\"" . $claseParaDia . "\">
						<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td width=\"80\" class=\"citaXdiaHora\">" . formatoHora($horario[$i]['hora_inicio']) . " - " . formatoHora($horario[$i]['hora_fin']) . "</td>
							<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
							<td width=\"80\" rowspan=\"2\">" . $botonesSinCita . "</td>
						  </tr>
						  <tr>
							<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\" >" . ponerAcentos($datos) . "</td>
						  </tr>
						</table>
					</td>
				  </tr>";
                                    }
                                }
                            } else {
                                $extempo = '';
                                /* 		$extempo = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <a href=\"javascript:citaExtemporanea(" . $_GET['getdate'] . ");\" title=\"Capturar Cita Extempor&aacute;nea\" class=\"textoCitaExtemporanea\">Capturar Cita Extempor&aacute;nea <img src=\"diseno/Symbol-Add.png\" alt=\"Capturar Cita Extempor&aacute;nea\" border=\"0\" /></a>";
                                 */ echo '<span class="textosParaInputs">NO HAY CITAS DISPONIBLES PARA ESTE DIA<br><br></span>';
                            }
                            $out.= "
</table>";
                            echo $out;
                            ?>
                            <?php echo $extempo; ?> <br />
                            <div id="estadoEliminando"></div></td>
                    </tr>
                </table>
                <p align="center">&nbsp;</p>
                <table width="500" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">
                    <tr>
                        <td width="35" height="25" class="citaXdiaPRV"></td>
                        <td width="129">Citas Primera Vez</td>
                        <td width="35" class="citaXdiaSUB"></td>
                        <td width="138">Citas Subsecuentes</td>
                        <td width="35" class="citaXdiaPRO"></td>
                        <td width="138">Citas Procedimientos</td>
                        <?php if ($_SESSION['idServ'] == 9 || $_SESSION['idServ'] == 63 || $_SESSION['idServ'] == 18 || $_SESSION['idServ'] == 19 || $_SESSION['idServ'] == 65 || $_SESSION['idServ']==27 || $_SESSION['idServ']==45) {?>
        <td width="100"><div id="CanCitas">
          <a href="javascript:EliminarCitas();" class="botones_menu" title="Cancelar Cita"><img src="diseno/cancelarCitaMedico.png"  width="48" height="48"/>Cancelar Cita</a></div></td>
          <?php } ?>
                    </tr>
                </table>
                </p>
                <?php
                $citasExtemporaneas = getCitasExtemporaneas($hoy, $_SESSION['idDr']);
                $out = "<br>";
                $out.= "
<table width=\"630\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
<tr>
  <td class=\"tituloVentana\" height=\"23\">" . formatoDia($hoy, 'tituloCitasXdia') . " - EXTEMPORANEAS</td>
</tr>
";
                $totalCitas = count($citasExtemporaneas);
                for ($i = 0; $i < $totalCitas; $i++) {
                    if ($citasExtemporaneas[$i]['tipo_cita'] == "0")
                        $claseParaDia = "citaXdiaLIBPRV"; //primera vez libre
                    if ($citasExtemporaneas[$i]['tipo_cita'] == "1")
                        $claseParaDia = "citaXdiaLIBSUB"; //primera vez libre
                    if ($citasExtemporaneas[$i]['tipo_cita'] == "2")
                        $claseParaDia = "citaXdiaLIBPRO"; //primera vez libre
                    $datosDerecho = getDatosDerecho($citasExtemporaneas[$i]['id_derecho']);
                    $datosUsuario = getUsuarioCitaXid($citasExtemporaneas[$i]['id_usuario']);
                    $nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
                    $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $citasExtemporaneas[$i]['extra1'] . " - " . $citasExtemporaneas[$i]['observaciones'];
                    $botones = "<table border='0'><tr><td><a href='javascript:crearCalendarioExt(\"" . $citasExtemporaneas[$i]['id_cita'] . "\");' class='botones_menu' title='Agendar Cita'><img src='diseno/agendarCita.png'  width='30' height='30'/>Agendar Cita Subsecuente</a></td><td><a href='javascript: agregarRecetaCitaE(\"" . $citasExtemporaneas[$i]['id_cita'] . "\");' title='Capturar Receta' class='botones_menu'><img src='diseno/nuevaReceta.png' border='0' width='30' height='30'><br>Agregar Receta</a></td><td><a href='javascript: agregarConstanciaE(\"" . $citasExtemporaneas[$i]['id_cita'] . "\");' title='Generar Constancia' class='botones_menu'><img src='diseno/generarConstancia.png' border='0' width='30' height='30'><br>Agregar Constancia</a></td></tr></table>";
                    $botonesSinCita = "<table border='1'><tr><td><a href=\"javascript: agregarRecetaCitaE('" . $citasExtemporaneas[$i]['id_cita'] . "');\" title=\"Capturar Receta\" class='botones_menu'><img src=\"diseno/nuevaReceta.png\" border=\"0\" width='30' height='30'><br>Agregar Receta</a></td><td><a href='javascript:agregarConstanciaE(" . $citasExtemporaneas[$i]['id_cita'] . ");' class='botones_menu'><img src='diseno/generarConstancia.png' width='30' height='30' /><br>Agregar Constancia</a></td></tr></table>";
                    $sql = "SELECT * FROM citas_medico WHERE id_derecho=" . $citasExtemporaneas[$i]['id_derecho'] . " AND fecha_cita=" . date("Ymd");
                    $query = ejecutarSQLAgenda($sql);
                    if ($query != 0)
                        $total = count($query);
                    else {
                        $total = 0;
                    }
                    if ($total == 0 && ($_SESSION['idServ']==9 || $_SESSION['idServ']==63 || $_SESSION['idServ']==18 || $_SESSION['idServ']==19 || $_SESSION['idServ'] == 65 || $_SESSION['idServ']==27 || $_SESSION['idServ']==45)) {
                        $out.="
		  <tr>
			<td class=\"" . $claseParaDia . "\">
				<table width=\"630\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td width=\"80\" class=\"citaXdiaHora\">" . formatoHora($citasExtemporaneas[$i]['hora_inicio']) . " - " . formatoHora($citasExtemporaneas[$i]['hora_fin']) . "</td>
					<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
					<td width=\"80\" rowspan=\"2\">" . $botones . "</td>
				  </tr>
				  <tr>
					<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\" >" . ponerAcentos($datos) . "</td>
				  </tr>
				</table>
			</td>
		  </tr>";
                    } else {
                        $out.="
		  <tr>
			<td class=\"" . $claseParaDia . "\">
				<table width=\"630\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td width=\"80\" class=\"citaXdiaHora\">" . formatoHora($citasExtemporaneas[$i]['hora_inicio']) . " - " . formatoHora($citasExtemporaneas[$i]['hora_fin']) . "</td>
					<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
					<td width=\"80\" rowspan=\"2\">" . $botonesSinCita . "</td>
				  </tr>
				  <tr>
					<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\" >" . ponerAcentos($datos) . "</td>
				  </tr>
				</table>
			</td>
		  </tr>";
                    }
                }
                $out .= "</table>".$_SESSION['idServ'];
                if ($totalCitas > 0)
                    echo $out;
                ?>
            </td>
        </tr>
    </table>
</center>
