<?php
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');
date_default_timezone_set('America/Mexico_City');
$galle = $_SESSION[$galleta_citas];
$tCitas = count($galle);
$nombre = '';
$galle_actual = '';
foreach($galle as $indice => $arregloActual) {
	if ($arregloActual['id_cita'] == $_GET['id_cita']) {
		$galle_actual = $indice;
		break;
	}
}

$referencia = getContraRef2013($_GET['id_derecho'], $_GET['id_servicio']);
$detalles = getDetallesCita($_GET['id_cita'], 'N');

$hoyC = date('Ymd');

$_SESSION['id_cita'] = $_GET['id_cita'];
$_SESSION['tipo_cita'] = 'N';
$nombre = $_SESSION[$galleta_citas][$galle_actual]['nombre'];
if ($_SESSION[$galleta_citas][$galle_actual]['asistio'] != 'SI') {
	$_SESSION[$galleta_citas][$galle_actual]['asistio'] = 'SI';
	$n_subsec = 0;
	if (isset($referencia['citas_subsecuentes'])) $n_subsec = $referencia['citas_subsecuentes'];
	$res = ejecutarSQL("UPDATE citas_detalles SET asistio='SI', n_citas='" . $n_subsec . "' WHERE id_cita='" . $_GET['id_cita'] . "' AND tipo_cita='N' LIMIT 1");
	$res = ejecutarSQL("UPDATE citas_detalles2 SET hora_si_no='" . date('Y-m-d H:i:s') . "' WHERE id_cita='" . $_GET['id_cita'] . "' AND tipo_cita='N' LIMIT 1");
	$detalles = getDetallesCita($_GET['id_cita'], 'N');
}

$botonContra = '&nbsp;';
$botonCita = '&nbsp;';
$botonReceta = "&nbsp;";
$botonConstancia = "&nbsp;";


if ($detalles['n_citas'] >=3) { // CONTRAREFERENCIA AUTOMATICA
	if ($detalles['hizo_contra'] != 'SI') // SI NO HA HECHO SU CONTRAREFERENCIA AUTOMATICA LE APARECEMOS EL BOTON, UNA VEZ QUE HAGA LA CONTRAREFERENCIA YA NO APARECERÁ
		$botonContra = "<a href=\"javascript: crearReferencia2013('" . $_GET['id_horario'] . "','" . $_GET['id_cita'] . "','N');\" title=\"Contrarreferencia\" class='botones_menu'><img src=\"images/advertencia.png\" border=\"0\" width='30' height='30'><br>Hacer Contrarreferencia</a>";
} else { // SE PUEDEN HACER LAS DEMAS COSAS
	if ($detalles['hizo_contra'] != 'SI') { // SI NO HA HECHO SU CONTRAREFERENCIA PUEDE TENER TODOS LOS BOTONES
		if ($detalles['fecha_cita'] == '') { // NO SE HA HECHO CONSTANCIA NI CITA SUBSECUENTE
			$botonContra = "<a href=\"javascript: crearReferencia2013('" . $_GET['id_horario'] . "','" . $_GET['id_cita'] . "','N');\" title=\"Contrarreferencia\" class='botones_menu'><img src=\"images/Copy.png\" border=\"0\" width='30' height='30'><br>Hacer Contrarreferencia</a>";
			$botonCita = "<a href='javascript:crearCalendario2013(\"" . $_GET['id_horario'] . "\");' class='botones_menu' title='Agendar Cita'><img src='diseno/agendarCita.png'  width='30' height='30'/><br>Agendar Cita Subsecuente</a>";
		}
	}
	$botonReceta = "<a href=\"javascript: void(0);\" title=\"Capturar Receta\" class='botones_menu agregarReceta' id=\"ag_" . $_GET['id_horario'] . "\"><img src=\"diseno/nuevaReceta.png\" border=\"0\" width='30' height='30'><br>Agregar Receta</a>";
	$botonConstancia = "<a href='javascript:agregarConstancia2013(" . $_GET['id_horario'] . ");' class='botones_menu'><img src='diseno/generarConstancia.png' width='30' height='30' /><br>Agregar Constancia</a>";
}

$botonDiagnostico = botonDiagnostico($hoyC, $_SESSION['idUsuario'], $_SESSION['idDr'], $detalles['hizo_contra'], $detalles['fecha_cita'], $_GET['id_horario'], $_GET['id_cita'], 'N', $detalles['diagnostico']);

echo '<h2 style="text-align:center;">' . ponerAcentos($nombre) . '</h2><p align="center">' . $botonDiagnostico . '</p>';
?>
<p align="center">
<img src="lib/perfil.png" />
<?php
echo "
		<table border='0' cellpadding='2' width='100%'><tr><td align='center' width='25%'>" . $botonContra . "</td><td align='center' width='25%'>" . $botonCita . "</td><td align='center' width='25%'>" . $botonReceta . "</td><td align='center' width='25%'>" . $botonConstancia . "</td></tr></table>
	";

?>
</p>
<input type="image" src="lib/close.gif" class="jqmClose" />
