<?php
session_start ();
include_once('lib/misFunciones.php');
$id_servicio = regresarIdServicio($_SESSION["idDr"]);
$servicio = getServicioXid($id_servicio);
$medico = getMedicoXid($_SESSION["idDr"]);

$folio = getFolioActual($_SESSION["idDr"]);
$medicamentos = getMedicamentos();
$medicamentosT = "";
$tMed = count($medicamentos);
$opMedicamentos = '<option value="0" selected="selected"> </option>';
for ($i=0; $i<$tMed; $i++) {
	$opMedicamentos .= '<option value="' . $medicamentos[$i]["id_medicamento"] . '">' . $medicamentos[$i]["descripcion"] . '</option>';
	$medicamentosT .= $medicamentos[$i]["descripcion"] . "|";
}
$medicamentosT = substr($medicamentosT,0,strlen($medicamentosT)-1);

$cantidades = array("1","2","3","4","5","6","7","8","9","10");
$cantidadesT = implode("|",$cantidades);
$tCantidades = count($cantidades);

$opCantidades = '<option value="0" selected="selected"></option>';
for ($i=0; $i<$tCantidades; $i++) {
	$opCantidades .= '<option value="' . $cantidades[$i] . '">' . $cantidades[$i] . '</option>';
}
$unidades = array('caja' => 'caja', 'frasco' => 'frasco');
$unidadesT = implode("|",$unidades);
$tUnidades = count($unidades);

$opUnidades = '<option value="" selected="selected"></option>';
foreach ($unidades as $indice => $valor) {
	$opUnidades .= '<option value="' . $indice . '">' . $valor . '</option>';
}
                
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaReceta" method="POST" action="javascript: validarAgregarReceta();">
<input name="id_derecho" id="id_derecho" type="hidden" value="" />
<input name="serie" id="serie" type="hidden" value="<?php echo $folio["serie"]; ?>" />
<input name="folio" id="folio" type="hidden" value="<?php echo ponerCeros($folio["folio_actual"],7); ?>" />

<table width="730" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CAPTURAR RECETA</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">ENTIDAD FEDERATIVA:</td>
    <td align="left"><?php echo ENTIDAD_FEDERATIVA; ?>
    &nbsp;&nbsp;&nbsp;<span class="textosParaInputs">FECHA DE EXPEDICION: </span>
    <input name="fecha" type="text" id="fecha" maxlength="10" value="<?php echo date('d/m/Y') ?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO:</td>
    <td align="left"><?php echo $servicio; ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MEDICO:</td>
    <td align="left"><?php echo $medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]; ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><input name="cedula" type="text" id="cedula" maxlength="13" readonly="readonly" tabindex="6" />
     <input name="seleccionar" type="button" class="botones" id="seleccionar" onClick="javascript:  ocultarDiv('divAgregarDH'); mostrarDiv('buscar'); getElementById('cedulaBuscar').focus(); document.getElementById('buscar').style.height = '150px';
" value="Buscar Paciente" tabindex="7"></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_p" type="text" id="ap_p" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="ap_m" type="text" id="ap_m" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="nombre" type="text" id="nombre" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    	<table width="97%" border="0" class="ventana">
          <tr>
            <td class="textosParaInputs" align="right" colspan="4">SERIE: <span class="textoRojo"><?php echo $folio["serie"]; ?></span> &nbsp;&nbsp;&nbsp;&nbsp; <span class="textosParaInputs">FOLIO:</span> <span class="textoRojo"><?php echo ponerCeros($folio["folio_actual"],7); ?></span></td></tr>
        	<tr>
            	<td class="tituloVentana" colspan="2">MEDICAMENTO 1</td>
            	<td class="tituloVentana" colspan="2">MEDICAMENTO 2</td>
            </tr>
    		<tr>
            	<td class="textosParaInputs" align="right">CANTIDAD:</td>
                <td align="left"><select name="cantidad_1" id="cantidad_1" tabindex="9"><?php echo $opCantidades ?></select> <select name="unidad_1" id="unidad_1" tabindex="10"><?php echo $opUnidades ?></select></td>
                <td class="textosParaInputs" align="right">CANTIDAD:</td>
                <td align="left"><select name="cantidad_2" id="cantidad_2" tabindex="13"><?php echo $opCantidades ?></select> <select name="unidad_2" id="unidad_2" tabindex="14"><?php echo $opUnidades ?></select></td>
            </tr>
            <tr>
            	<td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left"><select style="width:210px;" name="medicamento_1" id="medicamento_1" tabindex="11"><?php echo $opMedicamentos ?></select></td>
                <td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left"><select style="width:210px;" name="medicamento_2" id="medicamento_2" tabindex="15"><?php echo $opMedicamentos ?></select></td>
            </tr>
            <tr>
            	<td class="textosParaInputs" align="right">INIDICACIONES:</td>
                <td align="left"><textarea name="indicaciones_1" id="indicaciones_1" cols="20" rows="5" tabindex="12"></textarea></td>
                <td class="textosParaInputs" align="right">INIDICACIONES:</td>
                <td align="left"><textarea name="indicaciones_2" id="indicaciones_2" cols="20" rows="5" tabindex="15"></textarea></td>
          	</tr>
       </table>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><div id="masRecetas" style="width:100%"></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"><div id="div_agregar"><a href="#" onclick="javascript:agregarOtraReceta(3, '<?php echo $folio["serie"]; ?>', <?php echo $folio["folio_actual"]; ?>);" class="linkIndex">Agregar Otra Receta</a></div></td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicioMedico.php');" tabindex="24" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Guardar Receta(s)" class="botones" disabled="disabled" tabindex="25" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="buscar" style=" display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">BUSCAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
              <td align="left"><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();">
                <option value="cedula" selected="selected">C&eacute;dula</option>
                <option value="nombre">Nombre</option>
              </select>
              </td>
            </tr>
            <tr>
            <td colspan="2">
            	<div id="buscarPorCedula">
                <form id="selDH" method="POST" action="javascript: buscarDH(document.getElementById('cedulaBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                  <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                    <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="2" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
              </div>

                <div id="buscarPorNombre" style="display:none;">
                <form id="selDHN" method="POST" action="javascript: buscarDHN(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                  <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                  <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                  <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                    <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="4" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="4" align="center"></td>
                </tr>
              </table>
              </form>
              </div>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>

<form id="agregarDH" method="POST" action="javascript: agregarDHenCitaForma();">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="divAgregarDH" style=" display:none; height:0px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
              <td align="left"><input type="text" name="cedulaAgregar" id="cedulaAgregar" maxlength="10" /> / <input type="text" name="cedulaTipoAgregar" id="cedulaTipoAgregar" maxlength="2" size="5" /></td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_pAgregar" type="text" id="ap_pAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="ap_mAgregar" type="text" id="ap_mAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="nombreAgregar" type="text" id="nombreAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><input name="telefonoAgregar" type="text" id="telefonoAgregar" size="20" maxlength="10" /><span class="textosParaInputs"> EDAD: </span><input name="fecha_nacAgregar" type="text" id="fecha_nacAgregar" size="20" maxlength="3" />
    </td>
</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" size="64" maxlength="50" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estadoAgregar" id="estadoAgregar" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');"> 
    </select><span class="textosParaInputs">MUNICIPIO: </span><select name="municipioAgregar" id="municipioAgregar">
    </select>
    </td>
  </tr>
        
            <tr>
              <td colspan="2" align="center"><div id="divBotones_EstadoAgregarDH"></div>
                </td>
              </tr>
        
            <tr>
              <td colspan="2" align="center"></td>
            </tr>
          </table>
        </div>
	</td>
</tr>
</table>
</form>

</body>
</html>
