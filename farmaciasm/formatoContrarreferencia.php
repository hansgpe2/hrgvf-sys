<?php
error_reporting(E_ALL);
session_start();
include_once('lib/misFunciones.php');
include 'Connections/bdissste.php';
include_once('lib/misFunciones2013.php');
$medico = getMedicoXid($_SESSION['idDr']);
$fecha = $_REQUEST['fecha'];
$derecho = getDatosDerecho($_REQUEST['idDerecho']);

    $idUni = $_REQUEST['Unidad'];
    mysql_connect($hostname_bdissste, $username_bdisssteR, $password_bdissste);
    mysql_select_db($database_bdisssteR);
	mysql_set_charset("utf8");
	if ($derecho['unidad_medica'] < 1){ 
    $sql = "update derechohabientes set unidad_medica=$idUni where id_derecho=" . $_REQUEST['idDerecho'];
    $query = mysql_query($sql);
	}
    $unidad = obtenerUnidadMedica($idUni);
$evo = nl2br($_REQUEST['evolucion']);
$datclin = nl2br($_REQUEST['datClin']);
$diagRef = nl2br($_REQUEST['diagnosticoRef']);
$diag = nl2br($_REQUEST['diagnostico']);
$tratamientos = nl2br($_REQUEST['tratamiento']);
$recom = nl2br($_REQUEST['recomendaciones']);
$cont = $_REQUEST['cont'];
$erref = LlenarContraReferencias2013($_REQUEST['idDerecho'], $_REQUEST['servicio'], $fecha, $datclin, $diagRef, $diag, $evo, $tratamientos, $recom, $idUni, $_SESSION['idDr'], $_REQUEST['cont']);
$res = ejecutarSQL("UPDATE citas_detalles SET hizo_contra='SI', n_citas='0' WHERE id_cita='" . $_REQUEST['id_cita'] . "' AND tipo_cita='" . $_REQUEST['tipo_cita'] . "' LIMIT 1");
$res = ejecutarSQL("UPDATE citas_detalles2 SET hora_contra='" . date('Y-m-d H:i:s') . "' WHERE id_cita='" . $_REQUEST['id_cita'] . "' AND tipo_cita='" . $_REQUEST['tipo_cita'] . "' LIMIT 1");

if ($erref == 0) {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>CONTRARREFERENCIA</title>
            <style type="text/css">
                @import url("lib/impresion2.css") print;
            </style>
            <link href="lib/impresion2.css" rel="stylesheet" type="text/css">
        </head>
    </head>

    <body>
        <table width="100%" border="0" cellspacing="5" cellpadding="5">
            <tr>
                <td width="165"><img src="diseno/logoEncabezado.jpg" width="115" height="78" /></td>
                <td width="271" align="center" class="encabezado">HOSPITAL REGIONAL "DR. VALENTIN GOMEZ FARIAS"
                    <br />FORMATO DE CONTRARREFERENCIA</td><td width="89"><img src="diseno/logo04.png" width="89" height="86" /></td>
            </tr>
        </table>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:solid; border-width:1px;">
            <tr>
                <td><p><span class="titulos">CLINICA DE ADSCRIPCION</span><span class="datos"> <?php echo ponerAcentos($unidad['nombre']); ?></span></td>
            </tr>
            <tr>
                <td><p><span class="titulos">LOCALIDAD</span><span class="datos"> <?php echo $derecho['municipio'] . "," . $derecho['estado'] ?></span> &nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">&nbsp;&nbsp;&nbsp;&nbsp; FECHA</span><span class="datos"> <?php echo date("Ymd", strtotime($fecha)); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">CEDULA</span><span class="datos">: <?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo'] ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="datos"><?php echo ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']); ?></span></p></td>
            <tr>
                <td><?php
    if ($cont == 0) {
        echo "El paciente  continuar&aacute; su tratamiento con su m&eacute;dico familiar";
		delContraRef2013($_REQUEST['idDerecho'], $_REQUEST['servicio']);
	}
    else
        echo "El paciente  continuar&aacute; tratamiento con el m&eacute;dico especialista";
    ?>
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:solid; border-width:1px;">
            <tr><td><p align="center" class="encabezado">INFORME DEL MEDICO CONSULTADO</p></td></tr>
            <tr><td class="titulos">RESUMEN DE DATOS CLINICOS: </strong></td></tr>
            <tr><td class="datos"><?php echo ponerAcentos($datclin); ?></td></tr>
            <tr><td class="titulos">DIAGNOSTICOS DE:</td></tr>
            <tr><td><table width="100%" class="tablaDiag">
                        <tr><td class="titulos">Referencia</td><td class="titulos">Contrarreferencia</td></tr><tr><td class="datos"><?php echo ponerAcentos($diagRef); ?></td><td class="datos">
                                <?php echo ponerAcentos($diag); ?></td></tr></table></td></tr>
            <tr><td class="titulos">SINTESIS DE LA EVOLUCION:</td></tr>
            <tr><td class="datos"><?php echo ponerAcentos($evo); ?></td></tr>

            <tr><td class="titulos">TRATAMIENTO INSTITUIDO:</td></tr>
            <tr><td class="datos"><?php echo ponerAcentos($tratamientos); ?></td></tr>
            <tr><td class="titulos">RECOMENDACIONES:</td></tr>
            <tr><td class="datos"><?php echo ponerAcentos($recom); ?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td height="30pt" align="center">_______________________________________________________</td></tr>
            <tr><td align="center"><strong><?php echo $medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'] . "<br /> DGP: " . $medico['ced_pro']; ?> </strong></td></tr>
            <tr><td colspan="4" align="center" class="nota">Para Revisar las recetas proporcionadas al paciente<br />Dirijase
                    a la siguiente Direccion http://192.165.95.30/farmaciag/, ingrese con el usuario visorvisor, contrase&ntilde;a 123456</td></tr>
            <tr><td></td></tr>
            <tr>
          <td height="168" colspan="4" align="center" class="nota"><p><strong>INDICACIONES:</strong><br>
            <strong>REGLAMENTO  DE SERVICIOS MÉDICOS DEL INSTITUTO DE SEGURIDAD Y SERVICIOS SOCIALES  DE LOS TRABAJADORES DEL ESTADO.</strong></p>
<p><strong>ARTICULO  111.- </strong>Corresponde a la  Unidad Médica receptora del paciente, proporcionar la Atención Médica que le  haya sido solicitada por la Unidad Médica emisora, evitando diferir la  atención, debiendo verificar la existencia del Expediente Clínico o en su caso  la apertura del mismo para <strong>atender al paciente hasta por cuatro consultas  subsecuentes por el mismo procedimiento y diagnóstico</strong>, excepto en los casos  que se justifique y <strong>sustente en el Expediente Clínico</strong>. Para la  Contrarreferencia del paciente, la Unidad Médica deberá establecer mecanismos  que permitan la supervisión, registro y seguimiento de los pacientes referidos y  contra referidos.            </p></td>
        </tr>
        <tr><td class="nota"><ol>
          <li>Al derechohabiente contra referido  se deberá entregar por <strong>duplicado </strong>este documento para sus trámites.</li>
          <li>Deberá presentar uno de los originales  en el <strong>Departamento de Referencia y  Contrarreferencia</strong> de esta unidad <strong>Hosp.  Regional Dr. Valentín Gómez Farías.</strong></li>
        </ol>
 El duplicado se deberá presentarse en <strong>su clínica de adscripción</strong> que lo refirió  a esta unidad en un periodo <strong>no mayor a 15 días hábiles</strong>,  y <strong>solicitar  una cita</strong>  en la cual su <strong>médico familia</strong>r revisará la  Contrarreferencia del médicos de especialidad   y  Solicitara los medicamentos  necesarios para realizar el control y seguimiento necesario en su clínica de  adscripción </td></tr>
        </table>
    </body>
    </html>
    <?php
}
else {
    ?>
    <script>alert("No se genero la referencia");
        window.close();
    </script>
    <?php }
?>