<?php
include_once('lib/misFunciones.php');
session_start ();

$fecha = date("d/m/Y");
$fechaInicio = $fecha;
$fechaTermino = $fecha;
if ($_GET["fecha_inicio"] != '') $fechaInicio = $_GET["fecha_inicio"];
if ($_GET["fecha_termino"] != '') $fechaTermino = $_GET["fecha_termino"];

$datosRecetas = getRecetasXMedico($_SESSION["idDr"], $fechaInicio, $fechaTermino);
$out = '<form name="forma"><input type="hidden" value="' . $fechaTermino . '" id="datechooser" name="datechooser" /><center><table border="0" cellpadding="0" cellspacing="0" width="780">';
$out .= '<tr><td colspan="6" class="tituloVentana">Recetas sin surtir</td></tr>';
$out .= '<tr><th>Fecha y Hora</th><th>Folio</th><th>Servicio</th><th>Derechohabiente</th><th>Status</th><th></th></tr>';
$tDatos = count($datosRecetas);

for ($i=0; $i<$tDatos; $i++) {
//	$datosMedico = getMedicoXid($datosRecetas[$i]['id_medico']);
	$datosServicio = getServicioXid($datosRecetas[$i]['id_servicio']);
	$datosDerecho = getDatosDerecho($datosRecetas[$i]['id_derecho']);
	$out .= '<tr class="botones_menu" height="25"><td>' . $datosRecetas[$i]['fecha'] . ' - ' . $datosRecetas[$i]['hora'] . '</td><td>' . $datosRecetas[$i]['serie'] . '' . $datosRecetas[$i]['folio'] . '</td><td>' . ponerAcentos($datosServicio) . '</td><td>' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td><td align="left">' . $statusRecetas[$datosRecetas[$i]["status"]] . '</td><td align="center"><input type="button" id="btn_' . $i . '" name="btn_' . $i . '" value="Reimprimir" onclick="javascript: reimprimirReceta(' . $datosRecetas[$i]["id_receta"] . ');"></td></tr>';
}
$out .= '<tr><td colspan="6" align="center">Del: <INPUT type="text" name="date1" id="date1" readonly="readonly" onfocus="doShow(\'datechooser1\',\'forma\',\'date1\')" value="' . $fechaInicio . '"><div enabled="false" id="datechooser1" style="display:inline;"></div>&nbsp;&nbsp;&nbsp;&nbsp;al: <INPUT type="text" name="date2" id="date2" readonly="readonly" onfocus="doShow(\'datechooser2\',\'forma\',\'date2\')" value="' . $fechaTermino . '"><div enabled="false" id="datechooser2" style="display:inline;"></div>&nbsp;&nbsp;&nbsp;<input type="button" id="btn_ac" name="btn_ac" value="Acturalizar Listado" onclick="javascript: reimprimirRec(document.forms[0].date1.value,document.forms[0].date2.value);"></td></tr>';
$out .= '</table></center></form>';

print($out);

?>

