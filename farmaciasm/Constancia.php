<?php
mb_http_input("utf-8");
mb_http_output("utf-8");
include('lib/misFunciones.php');
include('Connections/bdissste.php');

$res = ejecutarSQL("UPDATE citas_detalles SET hizo_constancia='SI' WHERE id_cita='" . $_SESSION['id_cita'] . "' AND tipo_cita='" . $_SESSION['tipo_cita'] . "' LIMIT 1");
$res = ejecutarSQL("UPDATE citas_detalles2 SET hora_constancia='" . date('Y-m-d H:i:s') . "' WHERE id_cita='" . $_SESSION['id_cita'] . "' AND tipo_cita='" . $_SESSION['tipo_cita'] . "' LIMIT 1");


mysql_connect($hostname_bdissste, $username_bdissste, $password_bdissste);
mysql_select_db($database_bdisssteR);
if (isset($_GET['idConstancia'])) {
    $idConstancia = $_GET['idConstancia'];
    $sql = "select * from constancia_cita where id_constancia=$idConstancia";
    $query = mysql_query($sql);
    $constancia = mysql_fetch_assoc($query);
    $servicio = getServicioXid($constancia['id_servicio']);
    $derecho = getDatosDerecho($constancia['id_derecho']);
    $medico = getMedicoXid($constancia['id_medico']);
    $nombre = ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']);
    $hEnt = $constancia['hora_entrada'];
    $hSal = $constancia['hora_salida'];
    $date=$constancia['fecha_cita'];
    $acom = ponerAcentos($constancia['acompanante']);
    $fecha = date("d/m/Y", strtotime($date));
    $fecha = explode("/", $fecha);
    switch ($fecha[1]) {
        case '01':$mes = "Enero";
            break;
        case '02': $mes = "Febrero";
            break;
        case '03': $mes = "Marzo";
            break;
        case '04': $mes = "Abril";
            break;
        case '05': $mes = "Mayo";
            break;
        case '06': $mes = "Junio";
            break;
        case '07': $mes = "Julio";
            break;
        case '08': $mes = "Agosto";
            break;
        case '09': $mes = "Septiembre";
            break;
        case '10': $mes = "Octubre";
            break;
        case '11': $mes = "Noviembre";
            break;
        case '12': $mes = "Diciembre";
            break;
    }
} else {
    $idDer = $_GET['idDerecho'];
    $idServ = $_GET['idServicio'];
    $servicio = getServicioXid($idServ);
    $idMedico = $_GET['medico'];
    $medico = getMedicoXid($idMedico);
    $hEnt = $_GET['hEnt'];
    $hSal = $_GET['hSalida'];
    $acom = ponerAcentos($_GET['Acomp']);
    $date = $_GET['fecha'];
    $derecho = getDatosDerecho($_GET['idDerecho']);
    $nombre = ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']);
    $fecha = date("d/m/Y", strtotime($date));
    $fecha = explode("/", $fecha);
    switch ($fecha[1]) {
        case '01':$mes = "Enero";
            break;
        case '02': $mes = "Febrero";
            break;
        case '03': $mes = "Marzo";
            break;
        case '04': $mes = "Abril";
            break;
        case '05': $mes = "Mayo";
            break;
        case '06': $mes = "Junio";
            break;
        case '07': $mes = "Julio";
            break;
        case '08': $mes = "Agosto";
            break;
        case '09': $mes = "Septiembre";
            break;
        case '10': $mes = "Octubre";
            break;
        case '11': $mes = "Noviembre";
            break;
        case '12': $mes = "Diciembre";
            break;
    }
    $sql = "select * from constancia_cita where id_derecho=$idDer and id_servicio=$idServ and fecha_cita=$date";
    $query = mysql_query($sql) or die(mysql_error());
    if (mysql_num_rows($query) < 1) {
        $sql = "insert into constancia_cita values(NULL,'$idServ','$idDer','$idMedico','$date','$hEnt','$hSal','$acom')";
        $query = mysql_query($sql) or die(mysql_error());
             }
    }
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Untitled Document</title>
                <link href="lib/impresion.css" rel="stylesheet" type="text/css">
                <link href="lib/reportes.css" rel="stylesheet" type="text/css">
            </head>

            <body>
                <table width="684" cellpadding="0" cellspacing="0" class="tituloEncabezadoConBorde" border="0">
                    <tr><td width="39%"></td></tr>
                    <tr>
                        <td class="tituloIssste" rowspan="3"><img src="diseno/logoEncabezado.jpg"></td>
                        <td width="61%" align="center" class="tituloEncabezado">HOSPITAL REGIONAL "DR. VALENT&Iacute;N G&Oacute;MEZ FAR&Iacute;AS"</td>
                    </tr></tr>
                <tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td><span class="tituloEncabezadoConBorde">CONSTANCIA</span></td><td class="tituloEncabezado" align="left">Consulta Externa</td></tr>
                <tr>
                    <td colspan="2" align="right" class="tituloEncabezado">Zapopan Jalisco a <?php echo $fecha[0]; ?> de <?php echo $mes; ?> de <?php echo $fecha[2]; ?>.</td>
                </tr>
                <td style="font-family:Arial, Helvetica, sans-serif;font-size:14px;" height="400" colspan="2"><p class="tituloEncabezado">A QUIEN CORRESPONDA:</p>
                    <p style="text-align:justify">Por medio de la presente se hace constar que <span class="tituloEncabezado"><?php echo $nombre; ?></span> Asisti&oacute; el d&iacute;a de hoy a la Consulta Externa de este centro Hospitalario al Servicio de <span class="tituloEncabezado"><?php echo $servicio; ?></span> entre las <span class="tituloEncabezado"><?php echo $hEnt; ?></span> y las <span class="tituloEncabezado"><?php echo $hSal; ?></span></p>
                    <p style="text-align:justify">A petici&oacute;n del interesado y para los fines que estime conveniente se expide la presente constancia que no surte efecto de licencia m&eacute;dica.</p>
                    <?php
                    if ($acom != "") {
                        print("<p>Acompa&ntilde;ante<span class='tituloEncabezado'> " . $acom . "</span></p></td></tr>");
                    }
                    ?>

                <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif;font-size:14px;">&nbsp;</td><td style="font-family:Arial, Helvetica, sans-serif;font-size:14px;" align="right" class="tituloEncabezado"><p><?php print($medico['titulo']." ".$medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']); ?></p>
                        <p>DGP:<?php echo $medico['ced_pro']; ?></p>
            </table>

</body>
</html>