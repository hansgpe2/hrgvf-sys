<?php

session_start();
include_once('lib/misFunciones.php');
if ($_SESSION['idUsuario'] > 0) {
?>
    <?php

//Obtenemos la fecha actual:
    $fecha = time();
//Queremos sumar 2 horas a la fecha actual:
    $horas = 0;
// Convertimos las horas a segundos y las sumamos:
    $fecha += ($horas * 60 * 60);
// Le damos al resultado el formato deseado:
    $fechaYhora = date('d/m/Y - H:i', $fecha);
	//Checa que no haya cita en el mismo servicio
    $citaMismoDia = existeDuplica("SELECT * FROM citas as c join horarios as h on(h.id_horario=c.id_horario) WHERE c.fecha_cita='" . $_GET['fechaCita'] . "' AND c.id_derecho='" . $_GET['id_derecho'] . "' AND h.id_servicio='".$_SESSION['idServ']."'");
    if (!$citaMismoDia) {
		//checa que no este ocupado el horario del serrvicio
        $duplica = existeDuplica("SELECT * FROM citas WHERE id_horario='" . $_GET['idHorario'] . "' AND fecha_cita='" . $_GET['fechaCita'] . "'");
        // agregar en citas
        if (!$duplica) {
//	$citaMismoDia = existeDuplica("SELECT * FROM citas WHERE fecha_cita='" . $_GET['fechaCita'] . "' AND id_derecho='" . $_GET['id_derecho'] . "'");
            $citaMismoDia = existeDuplica("SELECT cita.fecha_cita, cita.id_derecho, cita.id_horario, horario.id_horario, horario.id_consultorio, horario.id_servicio, horario.id_medico FROM citas cita, horarios horario WHERE cita.id_horario=horario.id_horario AND cita.fecha_cita='" . $_GET['fechaCita'] . "' AND cita.id_derecho='" . $_GET['id_derecho'] . "' AND horario.id_consultorio='" . $_SESSION['IdCon'] . "' AND horario.id_servicio='" . $_SESSION['idServ'] . "' AND horario.id_medico='" . $_SESSION['idDr'] . "'");
            if (!$citaMismoDia) {
                $duplica = existeDuplica("SELECT * FROM citas WHERE id_horario='" . $_GET['idHorario'] . "' AND fecha_cita='" . $_GET['fechaCita'] . "'");
                if (!$duplica) {
                    $cita=  strtotime($_GET['fechaCita']);
                    $dia=  date("j");
                    $mes= date("n");
                    $anio=  date("Y");
                    $fechaC= mktime(0,0,0,$mes,$dia,$anio)+6*30*24*60*60;
                    $fecha3semanas =  $fechaC;
                    $fecha6meses=date("d/m/Y",$fecha3semanas);
                    $horario = getHorarioXid($_GET['idHorario']);
                    $sql="Select  c.fecha_cita from citas as c inner join horarios as h on (c.id_horario=h.id_horario) where c.id_derecho='".$_REQUEST['id_derecho']."' and h.id_servicio='".$_SESSION['idServ']."'
					Union select fecha_cita from citas_extemporaneas where id_derecho='".$_REQUEST['id_derecho']."' and id_servicio='".$_SESSION['idServ']."' order by fecha_cita DESC LIMIT 1";
                    $res1= ejecutarSQLAgenda($sql);
                   
                        $ultimaCita=  strtotime ($res1['fecha_cita']);
                    $uCita=date("d/m/Y",$ultimaCita);
                    if ($cita > $fecha3semanas || $cita<$ultimaCita)
                        echo "alert('La cita no puede ser a mas tardar el " . $fecha6meses . " o antes del ".$uCita."');";
                    else {
                        $query_query = "INSERT INTO citas VALUES (NULL,'" . $_GET['idHorario'] . "','" . $_GET['id_derecho'] . "','" . $_GET['fechaCita'] . "','1','" . $_GET['diagnostico'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',2);";
                        $res = ejecutarSQLR($query_query);
                        /*$log =*/ ejecutarLOG("cita nueva | horario: " . $_GET['idHorario'] . " | dh: " . $_GET['id_derecho'] . " | fecha: " . $_GET['fechaCita'] . " | status: 1 | observaciones: " . $_GET['obs']);
                        if ($res[0] == 0) {// no hay error
                            $contraReferencia = "no";
                            $cita = getCitaRecienAgregada($_GET['idHorario'], $_GET['id_derecho'], $_GET['fechaCita']);
							$upd = ejecutarSQL("UPDATE citas_detalles SET fecha_cita='" . $_GET['fechaCita'] . "' WHERE id_cita='" . $_SESSION['id_cita'] . "' AND tipo_cita='" . $_SESSION['tipo_cita'] . "' LIMIT 1");
							$upd2 = ejecutarSQL("UPDATE citas_detalles2 SET hora_cita='" . date('Y-m-d H:i:s') . "' WHERE id_cita='" . $_SESSION['id_cita'] . "' AND tipo_cita='" . $_SESSION['tipo_cita'] . "' LIMIT 1");
                            $sql = "Insert into citas_medico values(NULL,'" . date("Ymd") . "'," . $cita['id_cita'] . "," . $cita['id_derecho'] . "," . $horario['id_medico'] . ",0	);";
                            $query = ejecutarSQLR($sql);
							if($horario['tipo_cita']==0)
					{
						$sql="SELECT * FROM `referencia_contrarreferencia` where id_derecho='".$_GET['id_derecho']."' AND id_servicio='".$horario['id_servicio']."'";
						$queryCon=ejecutarSQLR($sql);
						
						if(count($query)==0)
						{
							$sql="insert into referencia_contrarreferencia VALUES(NULL,'".$_GET['id_derecho']."','".$horario['id_servicio']."',1,0,1);";
							
						}
						else
						$sql="update referencia_contrarreferencia SET citas_subsecuentes=citas_subsecuentes+1 Where id_derecho=".$_GET['id_derecho']." AND id_servicio=".$horario['id_servicio'];
						$querypri=ejecutarSQLR($sql);
					}
					else
					if($horario['tipo_cita']==1)
					{
						$sql="update referencia_contrarreferencia SET citas_subsecuentes=citas_subsecuentes+1 Where id_derecho=".$_GET['id_derecho']." AND id_servicio=".$horario['id_servicio'];
						$query=ejecutarSQLR($sql);
					}
                            print($cita['id_cita'] . "|" . $_GET['idHorario'] . "|" . $_GET['id_derecho'] . "|" . $_GET['fechaCita'] . "|" . $_SESSION['idUsuario'] . "||" . $contraReferencia . "|");
                            //	print_r($res);
                        }
						else
							echo "alert('no se pudo agregar cita ".$res[1]." ".$sql.");";
                    }
                } else {
                    echo "alert('El horario ya ha sido llenado con otra cita');";
                }
            } else {
                echo "alert('Ya existe una cita este dia para el paciente seleccionado');";
            }
        }
    }
} else {
    echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Tu sesion ha caducado'); location.replace('index.php');</script>";
}
    ?>