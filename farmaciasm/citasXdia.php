<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<center>
<table width="630" border="0" cellspacing="0" cellpadding="0" class="ventana">
<tr>
  <td class="tituloVentana" height="23"><?php echo formatoDia($_GET['getdate'], 'tituloCitasXdia'); ?></td>
</tr>
<tr>
  <td align="center"><br />

<?php 
	$extempo = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href=\"javascript:citaExtemporanea(" . $_GET['getdate'] . ");\" title=\"Agregar Cita Extempor&aacute;nea\" class=\"textoCitaExtemporanea\">Agregar Cita Extempor&aacute;nea <img src=\"diseno/Symbol-Add.png\" alt=\"Agregar Cita Extempor&aacute;nea\" border=\"0\" /></a>";
	$out = "";
	$out.= "
<table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
	$horario = citasXdia($_GET['getdate'],$_SESSION['idDr']);
	$citasDelDia = count($horario);
	$citasMostradas=0;
	if ($citasDelDia > 0) {
		for($i=0;$i<$citasDelDia;$i++) {
			$datosCita = citasOcupadasXdia($horario[$i]['id_horario'],$_GET['getdate']);
			$datosDerecho = getDatosDerecho($datosCita['id_derecho']);
			$datosUsuario = getUsuarioXid($datosCita['id_usuario']);
			if ($datosCita['id_derecho'] == "-1") { //no hay cita a x hora
                            if($horario[$i]['tipo_cita']!="0")
                            $citasMostradas++;
				if ($horario[$i]['tipo_cita'] == "1") $claseParaDia = "citaXdiaLIBSUB"; //primera vez libre
				if ($horario[$i]['tipo_cita'] == "2") $claseParaDia = "citaXdiaLIBPRO"; //primera vez libre
				$nombre = "";
				$datos = "";
				if ($_SESSION['tipoUsuario'] == '3') {
					$botones = "";
				} else {
					$botones = "<a href=\"javascript: agregarCita('" . $horario[$i]['id_horario'] . "','" . $_GET['getdate'] . "','" . formatoHora($horario[$i]['hora_inicio']) . "','" . formatoHora($horario[$i]['hora_fin']) . "');\" title=\"Agregar Cita\"><img src=\"diseno/Symbol-Add.png\" border=\"0\"></a>";
				}
			}
				if($horario[$i]['tipo_cita'] == 1) $claseParaDia = "citaXdiaSUB"; 
				if($horario[$i]['tipo_cita'] == 2) $claseParaDia = "citaXdiaPRO"; 
			
			if (($datosCita['status'] > 2) && ($datosCita['status'] < 7)) { // esta de vacaciones, en congreso, etc.
				$nombre = $statusCitas[$datosCita['status']];
				$datos = $datosCita['observaciones'];
				$botones = "";
				$claseParaDia = "citaXdiaNOT"; 
			}
			$hoy = date('Ymd');
			$fecha = $_GET['getdate'];
			if ( $fecha < $hoy) { 
				$botones = ""; // si es una fecha anterior a hoy no pone botones
				
			}
                        if($horario[$i]['tipo_cita'] != 0 && $datosCita['id_derecho'] == "-1")
			$out.="
  <tr>
    <td class=\"" . $claseParaDia . "\">
        <table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td width=\"80\" class=\"citaXdiaHora\">" . formatoHora($horario[$i]['hora_inicio']) . " - " . formatoHora($horario[$i]['hora_fin']) . "</td>
            <td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
			<td width=\"80\" rowspan=\"2\">" . $botones . "</td>
          </tr>
          <tr>
            <td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\" >" . ponerAcentos($datos) . "</td>
          </tr>
        </table>
    </td>
  </tr>";
		}	
	} else {
		echo '<span class="textosParaInputs">NO HAY CITAS DISPONIBLES PARA ESTE DIA<br><br></span>'.$extempo;
	}
	$out.= "
</table>";
        if($citasMostradas==0)
            $out.=$extempo;
	echo $out;
?>
<a href="javascript:cambiarMes(<?php echo $_GET['getdate'] ?>);" title="Regresar a la agenda mensual"><img src="diseno/flechaIzq.png" alt="Regresar" border="0" /></a>
<br /><div id="estadoEliminando"></div></td>
</tr>
</table>
<p align="center"><table width="500" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">
<tr>
<td width="35" height="25" class="citaXdiaPRV"></td><td width="129">Citas Primera Vez</td>
<td width="35" class="citaXdiaSUB"></td><td width="138">Citas Subsecuentes</td>
<td width="35" class="citaXdiaPRO"></td><td width="138">Citas Procedimientos</td>
</tr>
</table></p>
</center>
</body>
</html>
