<?php
set_time_limit(600);
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if (!isset($_POST['mostrar'])) {
		echo "<script  language=\"javascript\" type=\"text/javascript\">alert('error en envio de variables'); history.back();</script>";
	} else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Recetas</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/datepicker.js" type="text/javascript"></script>
<script src="../lib/nuevasFunciones.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
	.ul_reporte {
		list-style:none;
		padding:0px;
		margin:0px;
	}
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
<script>
function buscarDHbusqueda(cedula) {
	if (cedula.length > 3) {
		var contenedor;
		contenedor = document.getElementById('derechohabientes');
		var objeto= new AjaxGET();
		objeto.open("GET", "../buscarDHparaBusqueda.php?cedula="+cedula,true);
		objeto.onreadystatechange=function()
		{
			if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor.innerHTML = objeto.responseText;
			}
			if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
			{
				contenedor.innerHTML = "<img src=\"../diseno/loading.gif\">";
			}
		}
		objeto.send(null)
	} else {
		alert('Debe teclear al menos 4 caracteres');
	}
}

function buscarDHNbusqueda(ap_p, ap_m, nombre) {
	if ((ap_p.length > 0) || (ap_m.length > 0) || (nombre.length > 0)) {
		var contenedor;
		contenedor = document.getElementById('derechohabientes2');
		var objeto= new AjaxGET();
		objeto.open("GET", "../buscarDHNparaBusqueda.php?ap_p="+ap_p+"&ap_m="+ap_m+"&nombre="+nombre,true);
		objeto.onreadystatechange=function()
		{
			if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor.innerHTML = objeto.responseText;
			}
			if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
			{
				contenedor.innerHTML = "<img src=\"../diseno/loading.gif\">";
			}
		}
		objeto.send(null)
	} else {
		alert('Debe ingresar al menos un campo de busqueda (apellido paterno, apellido materno o nombre)');
	}
}

function buscarDHCbusqueda(cod_bar) {
	if (cod_bar.length == 12) {
		var contenedor;
		contenedor = document.getElementById('derechohabientes3');
		var objeto= new AjaxGET();
		objeto.open("GET", "buscarDHCparaBusqueda.php?cod_bar="+parseInt(cod_bar),true);
		objeto.onreadystatechange=function()
		{
			if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor.innerHTML = objeto.responseText;
			}
			if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
			{
				contenedor.innerHTML = "<img src=\"../diseno/loading.gif\">";
			}
		}
		objeto.send(null)
	} else {
		alert('El Código de barras debe ser de 12 dígitos');
	}
}

function verTodasRecetas() {
	if ($('#dh').val() == undefined) {
		alert('Busca el derechohabiente');
	} else {
		var id_derecho = document.getElementById('dh').value;
		if (id_derecho == '-1') {
			alert('Debes seleccionar una opcion');
		} else {
			var datos = id_derecho.split('|');
			var id_d = ponerCeros2(datos[0], 12);
			$('#id').val(datos[0]);
			$('#formaEnviar').submit();
		}
	}
}

</script>
</head>

<body>
<center>
<form action="reporte_recetas3.php" name="formaEnviar" id="formaEnviar" method="post">
	<input type="hidden" name="mostrar" id="mostrar" value="<?php echo $_POST['mostrar']; ?>" />
	<input type="hidden" name="fecha_de" id="fecha_de" value="<?php echo $_POST['fecha_de']; ?>" />
	<input type="hidden" name="fecha_a" id="fecha_a" value="<?php echo $_POST['fecha_a']; ?>" />
	<input type="hidden" name="status" id="status" value="<?php echo $_POST['status']; ?>" />
	<input type="hidden" name="orden" id="orden" value="<?php echo $_POST['orden']; ?>" />
	<input type="hidden" name="id" id="id" value="" />
</form>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<table class="ventana" width="650">
          <tr class="tituloVentana">
            <td colspan="2">Configura el Reporte de Recetas</td>
          </tr>
          <tr>
            <td colspan="2" align="center">Seleccionaste Mostrar Recetas <b><?php echo $_POST['status'] ?></b> de <b><?php echo $_POST['mostrar'] ?></b> desde <b><?php echo $_POST['fecha_de'] ?></b> hasta <b><?php echo $_POST['fecha_a'] ?></b> y se ordenar&aacute; el reporte por <b><?php echo $_POST['orden'] ?></b></td>
          </tr>
		  <tr><td align="center" colspan="2"><br /><br />
<?php
	switch($_POST['mostrar']) {
		case 'paciente':
?>
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><div id="buscar" style="height:150px; margin-top:10px;">
      <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
        <tr>
          <td colspan="2" class="tituloVentana">Buscar Derechohabiente</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
          <td align="left"><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor2(this.value);">
            <option value="barras">C&oacute;digo de Barras</option>
            <option value="cedula" selected="selected">C&eacute;dula</option>
            <option value="nombre">Nombre</option>
          </select></td>
        </tr>
        <tr>
          <td colspan="2">
          	<div id="buscarPorBarras" style="display:none;">
                <form id="selDH" method="post" action="javascript: buscarDHCbusqueda(document.getElementById('codigoBuscar').value);">
                  <table width="700" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="25" class="textosParaInputs" align="right">CODIGO DE BARRAS: </td>
                      <td align="left"><input type="text" name="codigoBuscar" id="codigoBuscar" maxlength="12"/>
                        <input name="buscarBarras" type="submit" class="botones" id="buscarBarras" value="Buscar..." /></td>
                    </tr>
                    <tr>
                      <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                      <td align="left"><div id="derechohabientes3">Ingrese el c&oacute;digo de barras del derechohabiente y haga click en Buscar...</div></td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: verTodasRecetas();" value="Ver Recetas del Derechohabiente" />
                        <br />
                        <br /></td>
                    </tr>
                  </table>
                </form>
            </div>
          	<div id="buscarPorCedula">
                <form id="selDH" method="post" action="javascript: buscarDHbusqueda(document.getElementById('cedulaBuscar').value);">
                  <table width="700" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                      <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                        <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." /></td>
                    </tr>
                    <tr>
                      <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                      <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div></td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: verTodasRecetas();" value="Ver Recetas del Derechohabiente" />
                        <br />
                        <br /></td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"></td>
                    </tr>
                  </table>
                </form>
          	</div>
            <div id="buscarPorNombre" style="display:none;">
              <form id="selDHN" method="post" action="javascript: buscarDHNbusqueda(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                    <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                    <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                    <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                  </tr>
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                    <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                      <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                  </tr>
                  <tr>
                    <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                    <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div></td>
                  </tr>
                  <tr>
                      <td colspan="4" align="center"><input name="seleccionarDHn" type="button" class="botones" id="seleccionarDHn" onclick="javascript: verTodasRecetas();" value="Ver Recetas del Derechohabiente" />
                      <br />
                      <br /></td>
                  </tr>
                  <tr>
                    <td colspan="4" align="center"></td>
                  </tr>
                </table>
              </form>
            </div></td>
        </tr>
      </table>
    </div></td>
  </tr>
</table>
<div id="recetas" style="clear:both;">
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td></td>
    </tr>
  </table>
</div>
<?php
			break;
		case 'servicio':
			$servicios = getServicios();
			$tServicios = count($servicios);
			$sel = '';
			if ($tServicios>0) {
				$sel .= '<select name="dh" id="dh">';
				$sel .= '<option value="-1" selected="selected"></option>';
				for ($i=0; $i<$tServicios; $i++)
					$sel .= '<option value="' . $servicios[$i]['id_servicio'] . '|">' . ponerAcentos($servicios[$i]['nombre']) . '</option>';
				$sel .= '</select>';
			}
?>
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><div id="buscar" style="height:150px; margin-top:10px;">
      <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
        <tr>
          <td colspan="2" class="tituloVentana">Selecciona Servicio</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="textosParaInputs" align="right" width="191">SERVICIOS: </td>
          <td align="left"><?php echo $sel; ?></td>
        </tr>
        <tr>
          <td colspan="2" align="center"><br />
          <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: verTodasRecetas();" value="Ver Recetas del Servicio" />
          <br /><br />
          </td>
        </tr>
      </table>
    </div></td>
  </tr>
</table>
<div id="recetas" style="clear:both;">
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td></td>
    </tr>
  </table>
</div>

<?php		
			break;
		case 'medico':
			$medicos = getMedicos();
			$tMedicos = count($medicos);
			$sel = '';
			if ($tMedicos>0) {
				$sel .= '<select name="dh" id="dh">';
				$sel .= '<option value="-1" selected="selected"></option>';
				for ($i=0; $i<$tMedicos; $i++)
					$sel .= '<option value="' . $medicos[$i]['id_medico'] . '|">' . ponerAcentos($medicos[$i]['ap_p'] . ' ' . $medicos[$i]['ap_m'] . ' ' . $medicos[$i]['nombres']) . '</option>';
				$sel .= '</select>';
			}
?>
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><div id="buscar" style="height:150px; margin-top:10px;">
      <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
        <tr>
          <td colspan="2" class="tituloVentana">Selecciona M&eacute;dico</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="textosParaInputs" align="right" width="191">MEDICOS: </td>
          <td align="left"><?php echo $sel; ?></td>
        </tr>
        <tr>
          <td colspan="2" align="center"><br />
          <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: verTodasRecetas();" value="Ver Recetas del M&eacute;dico" />
          <br /><br />
          </td>
        </tr>
      </table>
    </div></td>
  </tr>
</table>
<div id="recetas" style="clear:both;">
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td></td>
    </tr>
  </table>
</div>

<?php		
			break;
		case 'medicamento':
			$medicos = getMedicamentosOrden('descripcion');
			$tMedicos = count($medicos);
			$sel = '';
			if ($tMedicos>0) {
				$sel .= '<select name="dh" id="dh" style="width:800px;">';
				$sel .= '<option value="-1" selected="selected"></option>';
				for ($i=0; $i<$tMedicos; $i++)
					$sel .= '<option value="' . $medicos[$i]['id_medicamento'] . '|">' . $medicos[$i]['descripcion'] . '</option>';
				$sel .= '</select>';
			}
?>
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><div id="buscar" style="height:150px; margin-top:10px;">
      <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
        <tr>
          <td colspan="2" class="tituloVentana">Selecciona Medicamento</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="left" colspan="2"><?php echo $sel; ?></td>
        </tr>
        <tr>
          <td colspan="2" align="center"><br />
          <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: verTodasRecetas();" value="Ver Recetas del M&eacute;dicamento" />
          <br /><br />
          </td>
        </tr>
      </table>
    </div></td>
  </tr>
</table>
<div id="recetas" style="clear:both;">
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td></td>
    </tr>
  </table>
</div>

<?php		
			break;
	}

?>

     </td></tr></table>
</td></tr></table>
          </td></tr>
        </table>


<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php 
	}
} ?>