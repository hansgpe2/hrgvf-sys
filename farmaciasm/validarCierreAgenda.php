<?php
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');

$ret = 'ok';
$hoyC = date('Ymd');
$citas = getDetallesCitaAgendaValidar2($hoyC, $_SESSION['idDr']);
$tCitas = count($citas);
if ($tCitas > 0) {
	for ($i=0; $i<$tCitas; $i++) {
		$citaActual = '';
		if (isset($_SESSION[$galleta_citasE]['E'.$citas[$i]['id_cita']])) $citaActual = $_SESSION[$galleta_citasE]['E'.$citas[$i]['id_cita']]; else $citaActual = $_SESSION[$galleta_citas]['N'.$citas[$i]['id_cita']];
		if ($citas[$i]['asistio'] == '') {
			if ($ret == 'ok') $ret = 'Debes corregir lo siguientes errores:-';
			$ret .= '- * Indica si asistió o no a cita ' . $citaActual['nombre'];
		}
		if (($citas[$i]['diagnostico'] == "") && ($citas[$i]['hizo_contra'] == "") && ($citas[$i]['fecha_cita'] == "") && ($citas[$i]['asistio'] == "SI")) {
			if ($ret == 'ok') $ret = 'Debes corregir lo siguientes errores:-';
			$ret .= '- * Hacer CITA SUBSECUENTE, CONTRARREFERENCIA o diagnostico a ' . $citaActual['nombre'];
		}
	}
}
$res = ejecutarSQL("UPDATE consulta_cierre SET extra2=extra2+1 WHERE fecha='" . $hoyC . "' AND id_usuario='" . $_SESSION['idUsuario'] . "' AND id_medico='" . $_SESSION['idDr'] . "' LIMIT 1;");
if ($ret == 'ok') {
	$consulta = getConsultaCierre($hoyC, $_SESSION['idUsuario'], $_SESSION['idDr']);
	if (count($consulta) > 0) { // ya existe el registro, ya se habia logeado este usuario
		$total_cierres = (int)$consulta['total_cierres']+1;
		$res = ejecutarSQL("UPDATE consulta_cierre SET total_cierres='" . $total_cierres . "', termino_consulta='" . date('Y-m-d H:i:s') . "' WHERE fecha='" . $hoyC . "' AND id_usuario='" . $_SESSION['idUsuario'] . "' AND id_medico='" . $_SESSION['idDr'] . "' LIMIT 1;");
	}
}
print($ret);
?>