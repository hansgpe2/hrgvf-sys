<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CONTRARREFERENCIA</title>
<style type="text/css">
	@import url("lib/impresion2.css") print;
	@import url("lib/reportes2.css") screen;
</style>
</head>
</head>

<body>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="115"><img src="images/logoIssste.png" width="107" height="123" /></td>
    <td><p><strong>INSTITUTO DE SEGURIDAD Y SERVICIOS SOCIALES DE LOS <br /> TRABAJADORES DEL ESTADO</strong></p><br />
    <p align="center"><strong>CONTRARREFERENCIA</strong></p></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:solid; border-width:1px;">
  <tr>
    <td><p>CLINICA DE ADSCRIPCION ________________________</p></td>
  </tr>
  <tr>
    <td><p>LOCALIDAD __________________&nbsp;&nbsp;&nbsp;&nbsp; FECHA ______________</p></td>
  </tr>
  <tr>
    <td><span style="text-decoration:underline"><strong><?php echo $_REQUEST['cedula'] ?></strong></span>&nbsp;&nbsp;&nbsp; <span style="text-decoration:underline"><strong><?php echo $_REQUEST['nombre'] ?></strong></span></td>
  </tr>
  <tr>
    <td><p>CEDULA&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;APELLIDO PATERNO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; APELLIDO MATERNO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NOMBRE</p></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:solid; border-width:1px;">
<tr><td><p align="center"><strong>INFORME DEL MEDICO CONSULTADO</strong></p></td></tr>
<tr><td height="20pt"><strong>RESUMEN DE DATOS CLINICOS: </strong></td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td>&nbsp;</td></tr>
<tr>
  <td><strong>DIAGNOSTICO</strong>:</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><strong>SINTESIS DE LA EVOLUCION: </strong></td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><strong>TRATAMIENTO INSTITUIDO:</strong></td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><strong>RECOMENDACIONES:</strong></td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td height="30pt">___________________________________________________________________________</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td height="30pt" align="center">_______________________________________________________</td></tr>
<tr><td align="center"><strong>NOMBRE, CLAVE Y FIRMA DEL MEDICO</strong></td></tr>
</table>
</body>
</html>
