<?php
include_once('lib/misFunciones.php');
session_start ();

function ponerCeros($cant,$cuantos) {
	$aCeros = array("","0","00","000","0000","00000","000000","0000000","00000000","000000000","0000000000","00000000000","000000000000","0000000000000","00000000000000","000000000000000");
	$tCant = strlen($cant);
	$cuantos = $cuantos - $tCant;
	return $aCeros[$cuantos] . $cant;
}

$id_expediente = "";
if (isset($_REQUEST["id_expediente"])) {
	if ($_REQUEST["id_expediente"] != "") {
		$exp = getDatosDerecho((int)$_REQUEST["id_expediente"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Imprimir Etiqueta</title>
<link href="lib/impresionExp.css" rel="stylesheet" type="text/css" media="print" />
</head>

<body onload="javascript: window.print();">
<?php	
	print("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"200\"><tr><td align=\"center\" class=\"datos_etiq\">" . $exp["cedula"] . "/" . $exp["cedula_tipo"] . "<br>" . ponerAcentos($exp["ap_p"]) . " " . ponerAcentos($exp["ap_m"]) . "<br>" . ponerAcentos($exp["nombres"]) . "<br><img src=\"barcode/barcode.php?code=" . ponerCeros($_REQUEST["id_expediente"],8) . "&tam=1\" alt=\"barcode\" /></td></tr></table>");
	} else print ("Seleccione un derechohabiente para imprimir su codigo de barras");

} else {
	print("error");
}

?>

</body>
</html>
