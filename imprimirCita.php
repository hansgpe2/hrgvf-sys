<?php
error_reporting(E_ALL^E_NOTICE);
session_start ();
include_once('lib/misFunciones.php');

//	$datosCita = getCitaDatos($_GET['id_cita']);
	$datosDH = getDatosDerecho($_GET['id_derecho']);
	$datosHorario = getHorarioXid($_GET['id_horario']);
	$datosServicio = getServicioXid($datosHorario['id_servicio']);
	$datosOperador = getUsuarioXid($_GET['id_usuario']);
	$datosDr = getMedicoXid($datosHorario["id_medico"]);
	$fecha = formatoDia($_GET['fecha_cita'],"imprimirCita");
	
	$fechaHora = $fecha . " " . formatoHora($datosHorario['hora_inicio']) . " hrs.";
	$servicioConsultorio = $datosServicio['nombre'];
	$nombre = strtoupper($datosDH['ap_p'] . " " . $datosDH['ap_m'] . " " . $datosDH['nombres']);
	$nombreDr = strtoupper($datosDr['ap_p'] . " " . $datosDr['ap_m'] . " " . $datosDr['nombres']);
	$datosAgendo = strtoupper($datosOperador['nombre']) . "-" . date('d/m/Y - H:i:s') . " " . $_GET['tipo'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Cita</title>
<link href="lib/impresionEtiquetas.css" media="print" rel="stylesheet" type="text/css" />
</head>

<body onload="javascript: window.print();" style="width:2in; font-family:Arial, Helvetica, sans-serif; font-size:9px">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" width="45"><img src="diseno/logoEncabezado.png" width="30" height="30" /></td>
    <td class="issste" align="center">HOSP. REGIONAL “DR. VALENT&Iacute;N G&Oacute;MEZ FAR&Iacute;AS”<br /><span>CONSULTA EXTERNA</span></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2" align="center"><b><?php echo  ponerAcentos($nombre) ?></b></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="50%" align="left"><b>D&Iacute;A: </b><?php echo $fecha ?></td><td align="left"><b>Hora:</b> <?php echo formatoHora($datosHorario['hora_inicio']) ?> Hrs.</td></tr></table></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2" align="left"><b>Serv.</b> <?php echo ponerAcentos($servicioConsultorio)  ?></td>
  </tr>
  <tr>
    <td class="diaFecha" colspan="2" align="left"><b>Dr.</b> <?php echo ponerAcentos($nombreDr)  ?></td>
  </tr>
  <tr>
    <td class="quienAgenda" colspan="2"><b>Capt. </b><?php echo ponerAcentos($datosAgendo) ?></td>
  </tr>
</table>
</body>
</html>
