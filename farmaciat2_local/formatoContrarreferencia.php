<?php
error_reporting(E_ALL ^ E_NOTICE);
set_time_limit(300);
session_start();
include_once('lib/misFunciones.php');
include 'Connections/bdissste.php';
include_once('lib/misFunciones2013.php');
$medico = getMedicoXid($_SESSION['idDr']);
$fecha = $_REQUEST['fecha'];
$derecho = getDatosDerecho($_REQUEST['idDerecho']);
$medicinas = array(
    array(
        'activo' => $_REQUEST['m1'],
        'medicina' => $_REQUEST['medi_1'],
        'tratamiento' => $_REQUEST['tratamiento1'],
        'dias' => $_REQUEST['diasTratamiento_1'],
        'frecuencia' => $_REQUEST['frecuencia_1'],
        'cantidad' => $_REQUEST['divcantidad_1'],
        'indicaciones'=>$_REQUEST['indicaciones_1']
    ),
    array(
        'activo' => $_REQUEST['m2'],
        'medicina' => $_REQUEST['medi_2'],
        'tratamiento' => $_REQUEST['tratamiento2'],
        'dias' => $_REQUEST['diasTratamiento_2'],
        'frecuencia' => $_REQUEST['frecuencia_2'],
        'cantidad' => $_REQUEST['divcantidad_2'],
        'indicaciones'=>$_REQUEST['indicaciones_2']
    ),
    array(
        'activo' => $_REQUEST['m3'],
        'medicina' => $_REQUEST['medi_3'],
        'tratamiento' => $_REQUEST['tratamiento3'],
        'dias' => $_REQUEST['diasTratamiento_3'],
        'frecuencia' => $_REQUEST['frecuencia_3'],
        'cantidad' => $_REQUEST['divcantidad_3'],
        'indicaciones'=>$_REQUEST['indicaciones_3']),
    array(
        'activo' => $_REQUEST['m4'],
        'medicina' => $_REQUEST['medi_4'],
        'tratamiento' => $_REQUEST['tratamiento4'],
        'dias' => $_REQUEST['diasTratamiento_4'],
        'frecuencia' => $_REQUEST['frecuencia_4'],
        'cantidad' => $_REQUEST['divcantidad_4'],
        'indicaciones'=>$_REQUEST['indicaciones_4']),
    array(
        'activo' => $_REQUEST['m5'],
        'medicina' => $_REQUEST['medi_5'],
        'tratamiento' => $_REQUEST['tratamiento5'],
        'dias' => $_REQUEST['diasTratamiento_5'],
        'frecuencia' => $_REQUEST['frecuencia_5'],
        'cantidad' => $_REQUEST['divcantidad_5'],
        'indicaciones'=>$_REQUEST['indicaciones_5']),
    array(
        'activo' => $_REQUEST['m6'],
        'medicina' => $_REQUEST['medi_6'],
        'tratamiento' => $_REQUEST['tratamiento6'],
        'dias' => $_REQUEST['diasTratamiento_6'],
        'frecuencia' => $_REQUEST['frecuencia_6'],
        'cantidad' => $_REQUEST['divcantidad_6'],
        'indicaciones'=>$_REQUEST['indicaciones_6'])
);

$idUni = $_REQUEST['Unidad'];
mysql_connect($hostname_bdissste, $username_bdisssteR, $password_bdissste);
mysql_select_db($database_bdisssteR);
mysql_set_charset("utf8");
if ($derecho['unidad_medica'] < 1) {
    $sql = "update derechohabientes set unidad_medica=$idUni where id_derecho=" . $_REQUEST['idDerecho'];
    $query = mysql_query($sql);
}
$unidad = obtenerUnidadMedica($idUni);
$evo = nl2br($_REQUEST['evolucion']);
$datclin = nl2br($_REQUEST['datClin']);
$diagRef = nl2br($_REQUEST['diagnosticoRef']);
$diagRef2 = nl2br($_REQUEST['diagnosticoRef2']);
if ($diagRef2 != '') $diagRef = $diagRef . ' || ' . $diagRef2;
$diag = nl2br($_REQUEST['diagnostico']);
$diag2 = nl2br($_REQUEST['diagnostico2']);
if ($diag2 != '') $diag = $diag . ' || ' . $diag2;
$tratamientos = nl2br($_REQUEST['tratamiento']);
$recom = nl2br($_REQUEST['recomendaciones']);
$cont = $_REQUEST['cont'];

$erref = LlenarContraReferencias2013($_REQUEST['idDerecho'], $_REQUEST['servicio'], $fecha, $datclin, $diagRef, $diag, $evo, $tratamientos, $recom, $idUni, $_SESSION['idDr'], $_REQUEST['cont']);
$res = ejecutarSQL("UPDATE citas_detalles SET hizo_contra='SI', n_citas='0' WHERE id_cita='" . $_REQUEST['id_cita'] . "' AND tipo_cita='" . $_REQUEST['tipo_cita'] . "' LIMIT 1");
$res = ejecutarSQL("UPDATE citas_detalles2 SET hora_contra='" . date('Y-m-d H:i:s') . "' WHERE id_cita='" . $_REQUEST['id_cita'] . "' AND tipo_cita='" . $_REQUEST['tipo_cita'] . "' LIMIT 1");
$servicio=getServicioXid($_SESSION['idServ']);
$ref = LlenarContraReferencias($_REQUEST['idDerecho'], $_REQUEST['servicio'], $fecha, $datclin, $diagRef, $diag, $evo, $tratamientos, $recom, $idUni, $_SESSION['idDr'], $_REQUEST['cont']);
if ($ref[0] == 0) {
    $ref = ObtenerUltimaContra($_REQUEST['idDerecho'], $_REQUEST['servicio']);
    $error = array();
    $i = 0;
    $count = count($medicinas);
    while ($i < $count) {
        if ($medicinas[$i]['activo'] == "on") {
            $periodo = $medicinas[$i]['frecuencia'];
            switch ($periodo) {
                case "DIAS":
                    $dias = $medicinas[$i]['dias'];
                    break;
                case "SEMANAS":
                    $dias = $medicinas[$i]['dias'] * 7;
                    break;
                case "MESES":
                    $dias = $medicinas[$i]['dias'] * 30;
            }
            $medicinas[$i]['dias'] = $dias;
            $error[] = AgregarMedicinas($medicinas[$i]['medicina'], $ref['id_contra'], $_REQUEST['idDerecho'], $medicinas[$i]['cantidad'], $dias, date("Ymd"), $medicinas[$i]['tratamiento'], $_REQUEST['servicio'], $idUni,$medicinas[$i]['indicaciones']);
            
        }
        $i++;
    }
    if (!array_search(FALSE, $error)) {
        ?>
        <!DOCTYPE html>
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title>CONTRARREFERENCIA</title>
                <style type="text/css">
                    @import url("lib/impresion2.css") print;
                .firmaMedico {
	font-size: 12px;
}
                </style>
                <link href="lib/impresion2.css" rel="stylesheet" type="text/css">
            </head>
        </head>

        <body>
        <table width="100%" border="0" cellspacing="5" cellpadding="5">
                <tr>
                    <td width="95" align="center"><img src="diseno/logoEncabezado.jpg" alt="" width="90" height="107" /></td>
                    <td width="317" align="center" class="encabezado"><p>HOSPITAL REGIONAL </p>
                      <p>"DR. VALENTÍN GÓMEZ FARÍAS"
                        <br />FORMATO DE CONTRARREFERENCIA</p></td><td width="152"><p><img src="diseno/LogoHRVGF.jpg" alt="" width="141" height="104" /></p></td>
                </tr>
            </table>
        <p><span class="titulos">Folio</span>&nbsp;&nbsp;<?php echo $ref['id_contra']; ?><br />
                    <span class="titulos">CLÍNICA DE ADSCRIPCIÓN</span><span class="datos"> <?php echo ponerAcentos($unidad['nombre']); ?></span>
    
                    <br><span class="titulos">LOCALIDAD</span><span class="datos">: <?php echo ponerAcentos($derecho['municipio'] . "," . $derecho['estado']); ?></span><span class="titulos">&nbsp; FECHA</span><span class="datos"> <?php echo date("d/m/Y", strtotime($fecha)); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">CÉDULA</span><span class="datos">:<?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo'] ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">NOMBRE:</span><span class="datos"><?php echo ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']); ?></span>&nbsp;&nbsp;&nbsp; <span class="titulos">SERVICIO QUE ENVIA:</span> <?php echo $servicio; ?><br>
                      <?php
                        if ($cont == 0)
                            echo "El paciente  continuar&aacute; su tratamiento con su m&eacute;dico familiar";
                        else
                            echo "El paciente  continuar&aacute; tratamiento con el m&eacute;dico especialista";
                        ?>
                    </p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr><td><p align="center" class="encabezado">INFORME DEL MÉDICO CONSULTADO</p></td></tr>
                <tr>
                <td class="titulos">RESUMEN DE DATOS CLÍNICOS: </strong></td></tr>
                <tr><td class="datos"><?php echo ponerAcentos($datclin); ?></td></tr>
                <tr>
                <td class="titulos">DIAGNÓSTICOS DE:</td></tr>
                <tr><td><table width="100%" class="tablaDiag">
                            <tr><td class="titulos">Referencia</td><td class="titulos">Contrarreferencia</td></tr><tr><td class="datos"><?php echo ponerAcentos($diagRef); ?></td><td class="datos">
                                    <?php echo ponerAcentos($diag); ?></td></tr></table></td></tr>
                <tr>
                <td class="titulos">SINTESIS DE LA EVOLUCIÓN:</td></tr>
                <tr><td class="datos"><?php echo ponerAcentos($evo); ?></td></tr>

                <tr><td class="titulos">TRATAMIENTO INSTITUIDO:</td></tr>
                <tr><td class="datos"><?php echo ponerAcentos($tratamientos); ?>
                        <table width="100%" border="1">
                            <tr>
                                <td class="encabezado">Clave</td>
                                <td class="encabezado">Medicamento</td>
                                <td class="encabezado">Tipo Tratamiento</td>
                                <td class="encabezado">Tratamiento en Dias</td>
                                <td class="encabezado">Cantidad de cajas</td>
                                <td class="encabezado">Indicaciones</td>
                            </tr>
                            <?php
                            $i = 0;
                            while ($i < $count) {
                                if ($medicinas[$i]['activo'] == "on") {
                                    $medicina = getDatosMedicamentos($medicinas[$i]['medicina']);
                                    ?>
                                    <tr>
                                        <td><?php echo $medicina['id_medicamento']; ?></td>
                                        <td><?php $desc=strtok($medicina['descripcion'],"/");
		  echo $desc; ?></td>
                                        <td><?php
                                            if ($medicinas[$i]['tratamiento'] == 1)
                                                echo "Critico";
                                            else
                                                echo "Temporal"
                                                ?></td>
                                        <td><?php echo $medicinas[$i]['dias']; ?>&nbsp;</td>
                                        <td><?php echo $medicinas[$i]['cantidad']; ?>&nbsp;</td>
                                        <td><?php echo nl2br($medicinas[$i]['indicaciones']); ?></td>
                                    </tr> <?php
                                    
                                }
                                $i++;
                            }
                            ?>
                        </table>
                        <p>&nbsp;</p></td></tr>
                <tr><td class="titulos">RECOMENDACIONES:</td></tr>
                <tr><td class="datos"><?php echo ponerAcentos($recom); ?></td></tr>
                <tr><td>&nbsp;</td></tr>
                <tr><td height="30pt" align="center">_______________________________________________________</td></tr>
                <tr><td align="center"><strong class="firmaMedico"><?php echo $medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'] . "<br /> DGP: " . $medico['ced_pro']."<br>".$servicio; ?> </strong></td></tr>
                <tr><td colspan="4" align="center" class="nota">Para Revisar las recetas proporcionadas al paciente<br />Dirijase
                        a la siguiente Direccion http://192.165.95.30/farmaciag/, ingrese con el usuario visorvisor, contrase&ntilde;a 123456</td></tr>
                <tr><td></td></tr>
            </table>
            <span class="nota">
<p><strong>INDICACIONES:</strong><br>
<ol>
  <li>Al derechohabiente contra referido  se deberá entregar por <strong>duplicado </strong>este documento para sus trámites.</li>
  <li>Deberá presentar uno de los originales  en el <strong>Departamento de Referencia y  Contrarreferencia</strong> de esta unidad <strong>Hosp.  Regional Dr. Valentín Gómez Farías.</strong></li>
</ol>
El duplicado se deberá presentarse en <strong>su clínica de adscripción</strong> que lo refirió  a esta unidad en un periodo <strong>no mayor a 15 días hábiles</strong>,  y <strong>solicitar  una cita</strong>  en la cual su <strong>médico familia</strong>r revisará la  Contrarreferencia del médicos de especialidad   y  Solicitara los medicamentos  necesarios para realizar el control y seguimiento necesario en su clínica de  adscripción
</p></span>
        </body>
        </html>
        <?php
    }
    else {
        ?>
        <script>alert("No se genero la referencia \n "<?php print_r($error) ?>);
                    window.close();
        </script>
        <?php
    }
} else {
    ?>
    <script>alert("No se genero la referencia \n"<?php print_r($ref); ?>);
                window.close();
    </script>
    <?php
}
?>