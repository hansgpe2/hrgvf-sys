<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="lib/misEstilos.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php

//session_start(); 
$nav=$_SERVER['HTTP_USER_AGENT'];
include "lib/misFunciones.php";
if(isset($_GET['id_horario'])){
$horario=$_GET['id_horario'];
$cita=getCita($horario,date("Ymd"));
}
else
{
	$cita=getCitaExt($_GET['idCita']);
}
$derecho=getDatosDerecho($cita['id_derecho']);
$idserv=$_SESSION['idServ'];
$serv=getServicioXid($idserv);
$medi=getMedicoXid($_SESSION['idDr']);
?>
<div align="center">
<form action="formatoContrarreferencia.php" name="contraReferencia" id="contraReferencia" method="post" target="_blank">
<input type="hidden" name="id_cita" id="id_cita" value="<?php echo $_GET['id_cita'] ?>" />
<input type="hidden" name="tipo_cita" id="tipo_cita" value="<?php echo $_GET['tipo_cita'] ?>" />
<table width="100%" height="100%">
<tr><td colspan="2" class="tituloVentana">FORMATO DE CONTRARREFERENCIA</td></tr>
<tr><td class="textosParaInputs">CEDULA</td><td><?php echo $derecho['cedula']."/".$derecho['cedula_tipo']; ?><input type="hidden" name="idDerecho" id="idDerecho" value="<?php echo $cita['id_derecho'] ?>" /></td></tr>
<tr><td class="textosParaInputs">NOMBRE</td><td><?php echo ponerAcentos($derecho['ap_p']." ".$derecho['ap_m']." ".$derecho['nombres']); ?></td></tr>
<tr><td class="textosParaInputs">SERVICIO</td><td><?php echo $serv ?><input type="hidden" name="servicio" id="servicio" value="<?php echo $idserv; ?>"  /><input type="hidden" name="fecha" id="fecha" value="<?php echo date("Ymd"); ?>"  /></td></tr>
<tr><td class="textosParaInputs">RESUMEN DATOS CLINICOS</td><td><textarea cols="150" rows="10" name="datClin" id="datClin" wrap="hard" maxlength="1000" /></td></tr>
<tr><td class="textosParaInputs">DIAGNOSTICO REFERENCIA</td><td><textarea  id="diagnosticoRef" cols="150" rows="10" name="diagnosticoRef" wrap="hard" maxlength="1000" /></td></tr>
<tr><td class="textosParaInputs">DIAGNOSTICO CONTRARREFERENCIA</td><td><textarea  id="diagnostico" cols="150" rows="10" name="diagnostico" wrap="hard" maxlength="1000" /></td></tr>
<tr><td class="textosParaInputs">SINTESIS DE LA EVOLUCION</td><td><textarea id="evolucion" cols="150" rows="10" name="evolucion" wrap="hard" maxlength="1000" /></td></tr>
<tr><td class="textosParaInputs">TRATAMIENTO INSTITUIDO</td><td><textarea id="tratamiento" cols="150" rows="10" name="tratamiento" wrap="hard" maxlength="1000" /></td></tr>
<tr><td class="textosParaInputs">RECOMENDACIONES</td><td><textarea cols="150" rows="10" name="recomendaciones" id="recomendaciones" wrap="hard" /></td></tr>
<?php 
	print '<tr><td class="textosParaInputs">Unidad Medica</td><td><select name="Unidad" id="Unidad">'.
	CargarUnidadesMedicas($derecho['estado']).'</select></td></tr>';
?>
<tr>
  <td class="textosParaInputs" align="left">Requiere continuar tratamiento en 3° nivel</td>
  <td><select name="cont" id="cont"><option value="0">no</option><option value="1">s&iacute;</option></select></td></tr>    
<tr><td colspan="2" align="center"><input type="button" name="regresar" id="regresar" value="regresar" onclick="regresarARecetas()" class="botones" />&nbsp;  <input type="button" value="Guardar Contrarreferencia" class="botones" onclick="CrearReferenciaConfirmar()" /></td></tr>
</table></form></div>
</body>
</html>