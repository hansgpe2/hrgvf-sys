<?php
date_default_timezone_set('America/Mexico_City');
$hoy = date('Ymd');
setcookie('enca'.$hoy, '.', time() -3600, '/'); 
setcookie('enca'.$hoy, '.', time() + 24 * 60 * 60, '/'); 
session_start ();
$_SESSION['idUsuario'] = "-1";
$_SESSION['tipoUsuario'] = "-1";
$_SESSION['IdCon'] = "-1";
$_SESSION['idServ'] = "-1";
$_SESSION['idDr'] = "-1";
$_SESSION['paciente']="-1";
if (!isset($_SESSION['citas'.$hoy])) {
	$_SESSION['citas'.$hoy] = array();
}
if (!isset($_SESSION['citasE'.$hoy])) {
	$_SESSION['citasE'.$hoy] = array();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>ISSSTE - Control de Recetas</title>

<link rel="stylesheet" href="lib/misEstilos.css" type="text/css">
<link rel="stylesheet" href="lib/misEstilos2013.css" type="text/css">
<link href="lib/impresion.css" media="print" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="lib/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="lib/arreglos.js"></script>
<script type="text/javascript" src="lib/js2013.js"></script>
<script type="text/javascript" src="lib/js2014.js"></script>
<script type="text/javascript" src="lib/jqModal.js"></script>
<link rel="stylesheet" href="lib/medicamentos.css" type="text/css" />
<link rel="stylesheet" href="lib/jqModal.css" type="text/css" />
    <script type="text/javascript" src="lib/jquery.autocomplete.js"></script>
    <style>
  
#diagnostico, #diagnostico2, #diagnosticoRef, #diagnosticoRef2 { width: 650px; }
.autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; width: 650px; font-size: 10px; font-family: arial; }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }

  </style>


<script>
	$(function() {
		$("#medicamento_1").live("focus", function() {
			if (($("#med1").css("display") == "none"))
				$("#med1").toggle(200);
			if (($("#med1").css("display") != "none") && ($("#medi_1").val() != "")) {
				$("#medicamento_1").attr("value", "");
				$("#medi_1").attr("value", "");
			}
		});
		$("#medicamento_1").live("keyup", function() {
			var valor = $(this).val();
			var long = valor.length;
			$("#med1 a").each(function(index, element) {
                if (valor.toUpperCase() == $(element).attr("title").substr(0,long).toUpperCase()) {
					$(this).focus();
					$("#medicamento_1").focus();
				}
            });
		});
		$("#med1 a").live("click", function() {
			$("#medicamento_1").val($(this).attr("title"));
			$("#medi_1").val($(this).attr("id"));
			$("#med1").toggle(200);
		});

		$("#medicamento_2").live("focus", function() {
			if (($("#med2").css("display") == "none"))
				$("#med2").toggle(200);
			if (($("#med2").css("display") != "none") && ($("#medi_2").val() != "")) {
				$("#medicamento_2").attr("value", "");
				$("#medi_2").attr("value", "");
			}
		});
		$("#medicamento_2").live("keyup", function() {
			var valor = $(this).val();
			var long = valor.length;
			$("#med2 a").each(function(index, element) {
                if (valor.toUpperCase() == $(element).attr("title").substr(0,long).toUpperCase()) {
					$(this).focus();
					$("#medicamento_2").focus();
				}
            });
		});
		$("#med2 a").live("click", function() {
			$("#medicamento_2").val($(this).attr("title"));
			$("#medi_2").val($(this).attr("id"));
			$("#med2").toggle(200);
		});
		
		obtenerLogin();
	});
	
	function liberarMedicamento() {
		var contenedor;
		contenedor = document.getElementById('contenido');
		var objeto= new AjaxGET();
		objeto.open("GET", "liberarMedicamento.php",true);
		objeto.onreadystatechange=function()
		{
			if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor.innerHTML = objeto.responseText;
			}
			if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
			{
				contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
			}
		}
		objeto.send(null)
	}
	
	function verRecetasXsurtir() {
		var contenedor;
		contenedor = document.getElementById('contenido');
		var objeto= new AjaxGET();
		objeto.open("GET", "recetasXsurtir.php",true);
		objeto.onreadystatechange=function()
		{
			if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
			{
				contenedor.innerHTML = objeto.responseText;
			}
			if ((objeto.readyState==1) ||(objeto.readyState==2)||(objeto.readyState==3))
			{
				contenedor.innerHTML = "<img src=\"diseno/loading.gif\">";
			}
		}
		objeto.send(null)
	}

function preparar_recetas() {
  var contenedor;
  contenedor = document.getElementById('contenido');
  var objeto= new AjaxGET();
  objeto.open("GET", "preparar_recetas.php",true);
  objeto.onreadystatechange=function()
  {
    if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
    {
      contenedor.innerHTML = objeto.responseText;
    }
    else
    contenedor.innerHTML="<img src='lib/loading.gif' />";
  }
  objeto.send(null)
}

function preparar_recetas_do(id_receta) {
  var contenedor;
  contenedor = document.getElementById('contenido');
  var objeto= new AjaxGET();
  objeto.open("GET", "preparar_recetas_do.php?id_receta="+id_receta,true);
  objeto.onreadystatechange=function()
  {
    if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
    {
      window.open("recetasPRINTetiqueta.php?id_receta="+id_receta);
      alert('Marcada como preparada');
      preparar_recetas();
    }
    else
    contenedor.innerHTML="<img src='lib/loading.gif' />";
  }
  objeto.send(null)
}

function recetas_preparadas() {
  var contenedor;
  contenedor = document.getElementById('contenido');
  var objeto= new AjaxGET();
  objeto.open("GET", "recetas_preparadas.php",true);
  objeto.onreadystatechange=function()
  {
    if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
    {
      contenedor.innerHTML = objeto.responseText;
    }
    else
    contenedor.innerHTML="<img src='lib/loading.gif' />";
  }
  objeto.send(null)
}

function recetas_preparadas_do(id_receta) {
  var contenedor;
  contenedor = document.getElementById('contenido');
  var objeto= new AjaxGET();
  objeto.open("GET", "recetas_preparadas_do.php?id_receta="+id_receta,true);
  objeto.onreadystatechange=function()
  {
    if (objeto.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
    {
      alert('Marcada como entregada');
      recetas_preparadas();
    }
    else
    contenedor.innerHTML="<img src='lib/loading.gif' />";
  }
  objeto.send(null)
}


</script>

</head>

<body bgcolor="#EEEEEE">
<div class="jqmWindow" id="ex2">
... <img src="lib/busy.gif" alt="loading" />
</div>

<center>
  <table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="diseno/logoEncabezado.jpg" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    
    <table id="centro" class="centro" width="800" height="499">
    <tr><td align="center" valign="top">
      <table width="800">
          <tr><td colspan="5"><div id="menu" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicio.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="250">&nbsp;</td>
                    	<td width="100" align="center"><a href="javascript: reportes();" title="Reportes" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reportes</a></td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuMedico" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicioMedico.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="100" align="center"><a href="javascript: reimprimirRec('','');" title="Reimprimir Receta" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reimprimir Receta</a></td>
                    	<td width="150" align="center"><a href="http://192.161.21.7/med-logon.asp" target="_blank" title="Resultados de Laboratorio" class="botones_menu"><img src="diseno/resultados_lab.jpg" width="40" height="40" border="0" /><br>Resultados Laboratorio<br />Cod.M&eacute;dico: aqc<br />Contrase&ntilde;a: 123</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuMedicoEsp" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicioMedicoEsp.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100" align="center"><a href="javascript: capturarRec2014();" title="Capturar Receta" class="botones_menu"><img src="diseno/_medRecAdd.gif" width="40" height="40" border="0" /><br>Capturar Receta de Piso</a></td>
                    	<td width="100" align="center"><a href="javascript: reimprimirRec('','');" title="Reimprimir Receta" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reimprimir Receta</a></td>
                    	<td width="150" align="center"><a href="http://192.161.21.7/med-logon.asp" target="_blank" title="Resultados de Laboratorio" class="botones_menu"><img src="diseno/resultados_lab.jpg" width="40" height="40" border="0" /><br>Resultados Laboratorio<br />Cod.M&eacute;dico: aqc<br />Contrase&ntilde;a: 123</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuExterna" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="100"><a href="javascript: inicio('inicioExterna.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100"><a href="javascript: recetasCancelar();" title="Cancelar Recetas" class="botones_menu"><img src="diseno/_medRecDel.gif" width="40" height="40" border="0" /><br>Cancelar Receta</a></td>
                    	<td width="150"><a href="javascript: buscarFolios();" title="Buscar Folios" class="botones_menu"><img src="diseno/buscar.png" width="40" height="40" border="0" /><br>Buscar Folios</a></td>
                        <td width="100"><a href="javascript:recetasCanceladas();" title="Recetas Canceladas" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Recetas Canceladas</a></td>
                    	<td width="100"><a href="javascript: liberarMedicamento();" title="Liberar Medicamento" class="botones_menu"><img src="diseno/_medRecDel.gif" width="40" height="40" border="0" /><br>Liberar Medicamento</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuFarmacia" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="70" align="center"><a href="javascript: inicio('inicioFarmacia.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                      <td width="20">&nbsp;</td>
                        <td width="100" align="center"><a href="javascript:preparar_recetas();" title="Preparar Recetas" class="botones_menu"><img src="diseno/preparar_recetas.png" width="40" height="40" border="0" /><br>Preparar Recetas</a></td>
                        <td width="100" align="center"><a href="javascript:recetas_preparadas();" title="Recetas Preparadas" class="botones_menu"><img src="diseno/preparar_recetas_ver.png" width="40" height="40" border="0" /><br>Recetas Preparadas</a></td>
                      <td width="50">&nbsp;</td>
                    	<td width="200" align="center"><a href="javascript: verTodasRecetas();" title="Ver todas las recetas" class="botones_menu"><img src="diseno/_medRecAviso.gif" width="40" height="40" border="0" /><br>Ver Todas</a></td>
                    	<td width="150" align="center"><a href="javascript: verRecetasXsurtir();" title="Ver todas las recetas" class="botones_menu"><img src="diseno/_medRecAviso.gif" width="40" height="40" border="0" /><br>Recetas X surtir</a></td>
                      <td width="100" align="center"><a href="javascript: cargarExistencias();" title="Cargar Existencias" class="botones_menu"><img src="diseno/_medAdd.gif" width="40" height="40" border="0" /><br>Cargar Existencias</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuColectivos" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicioColectivos.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="200">&nbsp;</td>
                    	<td width="200">&nbsp;</td>
                    	<td width="100" align="center">&nbsp;</td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
                <div id="menuVisor" style="display:none">
        			<table border="0" width="100%" class="tablaPrincipal"><tr>
                    	<td width="150"><a href="javascript: inicio('inicioVisor.php');" title="Inicio" class="botones_menu"><img src="diseno/_medHome.gif" width="40" height="40" border="0" /><br>Inicio</a></td>
                    	<td width="100">&nbsp;</td>
                    	<td width="200">&nbsp;</td>
                    	<td width="200">&nbsp;</td>
                    	<td width="100" align="center">&nbsp;</td>
                        <td width="100"><a href="javascript:ayuda();" title="Ayuda" class="botones_menu"><img src="diseno/ayuda.png" width="40" height="40" border="0" /><br>Ayuda</a></td>
                        <td width="100" align="right"><a href="javascript:logout();" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a></td>
                    </tr></table>
              	</div>
        </td></tr>
          <tr><td><div id="seleccion"></div></td></tr>
        <tr><td width="0" height="246" id="menuPaciente" valign="top"><div id="menuCitas" style="display:none"><table width="100%" height="100%" border="0">
        <tr>
          <td align="center"><a href="javascript:recuperarConstancias('contenido');" class="botones_menu"><img src="diseno/reimprimirConstancia.png" width="48" height="48" />Reimprimir <br />Constancia</a></td>
        </tr>
       <tr>
          <td><div id="CanCitas" style="display:none">
          <a href="javascript:EliminarCitas();" class="botones_menu" title="Cancelar Cita"><img src="diseno/cancelarCitaMedico.png"  width="48" height="48"/>Cancelar Cita</a></div></td>
        </tr>
        <tr>
          <td><a href="javascript:BuscarContraRef();" class="botones_menu" title="Cancelar Cita"><img src="images/copy.png"  width="48" height="48"/>Buscar Contrarreferencias</a></div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table></div></td>
        <td width="788" align="center" id="content" colspan="3"><br />
		      	<div id="contenido">
      			</div>
      	</td></tr>
      </table>
    </td></tr></table>
</td></tr></table>
</center>
</body>
</html>
