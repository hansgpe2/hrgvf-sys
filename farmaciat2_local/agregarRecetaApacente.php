<?php
error_reporting(0);
include_once('lib/misFunciones.php');
$id_servicio = regresarIdServicio($_SESSION["idDr"]);
$servicio = getServicioXid($id_servicio);
$medico = getMedicoXid($_SESSION["idDr"]);

	$folio = getFolioActual($_SESSION["idDr"]);
	if (count($folio) == 0) {
		$boton = '<span class="textoRojo">No tiene folios disponibles</span>';
	} else {
		$boton = '<input type="submit" name="agregar" id="agregar" value="Guardar Receta" class="botones" tabindex="10" />';
	}
	$medicamentos = getMedicamentosXservicio($_SESSION['idServ']);
//	$medicamentos = getMedicamentos();
	$tMed = count($medicamentos);
	$liMedicamentos = '<li><a href="#" id="" title=""><span>&nbsp;</span></a></li>';
	$liMedicamentos2 = '<li><a href="#" id="" title=""><span>&nbsp;</span></a></li>';

	for ($i=0; $i<$tMed; $i++) {
		$existencias = getExistenciasMedicamentos($medicamentos[$i]["id_medicamento"]);
		if ($existencias["existencia_actual"] == 0) $clase = 'sinExistencias'; else $clase = 'conExistencias';
			$liMedicamentos .= '<li><a onclick="javascript:actualizarMedicamento(\'' . $medicamentos[$i]["presentacion"] . '\',\'' . $medicamentos[$i]["unidad"] . '\',\'' . $medicamentos[$i]["extra1"] . '\',\'1\');" href="#" id="' . $medicamentos[$i]["id_medicamento"] . '" title="' . $medicamentos[$i]["descripcion"] . '"><span class="' . $clase . '">' . $medicamentos[$i]["descripcion"] . '</span></a></li>';
			$liMedicamentos2 .= '<li><a onclick="javascript:actualizarMedicamento(\'' . $medicamentos[$i]["presentacion"] . '\',\'' . $medicamentos[$i]["unidad"] . '\',\'' . $medicamentos[$i]["extra1"] . '\',\'2\');" href="#" id="' . $medicamentos[$i]["id_medicamento"] . '" title="' . $medicamentos[$i]["descripcion"] . '"><span class="' . $clase . '">' . $medicamentos[$i]["descripcion"] . '</span></a></li>';
	}

	$opCantidades = '<option value="0" selected="selected"></option>';
	for ($i=1; $i<=90; $i++) {
		$opCantidades .= '<option value="' . $i . '">' . $i . '</option>';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaReceta" method="POST" action="javascript: validarAgregarReceta();">
<input type="hidden" id="diagnostico_seleccionado" name="diagnostico_seleccionado" value="" />
<input name="id_derecho" id="id_derecho" type="hidden" value="" />
<input name="serie" id="serie" type="hidden" value="<?php echo $folio["serie"]; ?>" />
<input name="folio" id="folio" type="hidden" value="<?php echo ponerCeros($folio["folio_actual"],7); ?>" />
<input name="unidad_1" id="unidad_1" type="hidden" value="" />
<input name="unidad_2" id="unidad_2" type="hidden" value="" />
<input name="uni_1" id="uni_1" type="hidden" value="" />
<input name="uni_2" id="uni_2" type="hidden" value="" />
<input name="cantidad_1" id="cantidad_1" type="hidden" value="0" />
<input name="cantidad_2" id="cantidad_2" type="hidden" value="0" />
<input name="tipo_receta" id="tipo_receta" type="hidden" value="" />
<input name="medi_1" id="medi_1" type="hidden" value="" />
<input name="medi_2" id="medi_2" type="hidden" value="" />
<input name="grupo_1" id="grupo_1" type="hidden" value="" />
<input name="grupo_2" id="grupo_2" type="hidden" value="" />
<input name="diasTratamiento_1" id="diasTratamiento_1" type="hidden" value="1" />
<input name="diasTratamiento_2" id="diasTratamiento_2" type="hidden" value="1" />
<table width="730" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">CAPTURAR RECETA</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">ENTIDAD FEDERATIVA:</td>
    <td align="left"><?php echo ENTIDAD_FEDERATIVA; ?>
    &nbsp;&nbsp;&nbsp;<span class="textosParaInputs">FECHA DE EXPEDICION: </span>
    <input name="fecha" type="text" id="fecha" maxlength="10" value="<?php echo date('d/m/Y') ?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">SERVICIO:</td>
    <td align="left"><?php echo $servicio; ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">MEDICO:</td>
    <td align="left"><?php echo $medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]; ?></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
    <td align="left"><input name="cedula" type="text" id="cedula" maxlength="13" readonly="readonly" />
     <input name="seleccionar" type="button" class="botones" id="seleccionar" onClick="javascript:  ocultarDiv('divAgregarDH'); mostrarDiv('buscar'); getElementById('cedulaBuscar').focus(); document.getElementById('buscar').style.height = '150px';
" value="Buscar Paciente" tabindex="0"></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_p" type="text" id="ap_p" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="ap_m" type="text" id="ap_m" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="nombre" type="text" id="nombre" size="20" maxlength="20" readonly="readonly" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right" valign="top">DIAGNOSTICO:</td>
    <td align="left" valign="top"><input type="text" size="40" id="diagnostico" name="diagnostico" value="" tabindex="1" onkeydown="return sinTab(event, this);" ></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right" valign="top"><small><small><a href='#' onclick="javascript:$('#div_otro_diagnostico').css('display','block')">OTRO DIAGNOSTICO</a>:</small></small></td>
    <td align="left" valign="top"><div id="div_otro_diagnostico" style="display:none;"><input type="text" size="40" id="diagnostico2" name="diagnostico2" value="" onkeydown="return sinTab(event, this);" ></div></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">&nbsp;</td>
    <td align="left">
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center">
    	<table width="97%" border="0" class="ventana">
          <td colspan="2" align="center">
    	<table width="97%" border="0" class="ventana">
               	<td class="tituloVentana" colspan="2">MEDICAMENTO 1</td>
            	<td class="tituloVentana" colspan="2">MEDICAMENTO 2</td>
            <tr>
            	<td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left">
                    <div class="mi_select">
                            <input type="text" name="medicamento_1" id="medicamento_1" value="" tabindex="2">
                            <ul id="med1">
                                <?php echo $liMedicamentos ?>
                            </ul>
                    </div>
                </td>
                <td class="textosParaInputs" align="right" width="200">MEDICAMENTO:</td>
                <td align="left">
                    <div class="mi_select2">
                            <input type="text" name="medicamento_2" id="medicamento_2" value="" tabindex="7">
                            <ul id="med2">
                                <?php echo $liMedicamentos2 ?>
                            </ul>
                    </div>
                </td>
            </tr>
    		<tr>
            	<td class="textosParaInputs" align="right">PERIODO:</td>
                <td align="left"><select name="frecuencia_1" id="frecuencia_1" tabindex="3" onchange="javascript: actualizarPeriodo('1');"><option value="DIAS" selected="selected">Diario</option><option value="SEMANAS">Semanal</option><option value="MESES">Mensual</option><option value="TRIMESTRES">Trimestral</option></select></td>
                <td class="textosParaInputs" align="right">PERIODO:</td>
                <td align="left"><select name="frecuencia_2" id="frecuencia_2" tabindex="3" onchange="javascript: actualizarPeriodo('2');">
                  <option value="DIAS" selected="selected">Diario</option>
                  <option value="SEMANAS">Semanal</option>
                  <option value="MESES">Mensual</option>
                  <option value="TRIMESTRES">Trimestral</option>
                </select></td>
            </tr>
    		<tr>
            	<td class="textosParaInputs" align="right">DOSIS:</td>
                <td align="left"><input type="text" name="dosis_1" id="dosis_1" tabindex="4" value="" size="5" onkeydown="javascript: return validarNumero(event, this.value);" onblur="javascript: return compruebaNumero(this,'1')"> &nbsp;&nbsp;<div style="display:inline" class="textosParaInputs" id="presentacion_1">&nbsp;</div></td>
                <td class="textosParaInputs" align="right">DOSIS:</td>
                <td align="left"><input type="text" name="dosis_2" id="dosis_2" tabindex="9" value="" size="5" onkeydown="javascript: return validarNumero(event, this.value);" onblur="javascript: return compruebaNumero(this,'2')"> &nbsp;&nbsp;<div style="display:inline" class="textosParaInputs" id="presentacion_2">&nbsp;</div></td>
            </tr>
    		<tr>
            	<td class="textosParaInputs" align="right">TRATAMIENTO:</td>
                <td align="left"><div style="display:inline" id="divTrat_1"><select name="dias_1" id="dias_1" tabindex="5" onchange="javascript: actualizarCantidad('1')"><?php echo $opCantidades ?></select></div> <div id="periodo_1" style="display:inline" class="textosParaInputs">DIAS</div></td>
                <td class="textosParaInputs" align="right">TRATAMIENTO:</td>
                <td align="left"><div style="display:inline" id="divTrat_2"><select name="dias_2" id="dias_2" tabindex="10" onchange="javascript: actualizarCantidad('2')"><?php echo $opCantidades ?></select></div> <div id="periodo_2" style="display:inline" class="textosParaInputs">DIAS</div></td>
            </tr>
			<tr>
            	<td class="textosParaInputs" align="right">CANTIDAD:</td>
                <td align="left"><div id="divcantidad_1">&nbsp;</div></td>
                <td class="textosParaInputs" align="right">CANTIDAD:</td>
                <td align="left"><div id="divcantidad_2">&nbsp;</div></td>
            </tr>
			<tr>
            	<td class="textosParaInputs" align="right">INIDICACIONES:</td>
                <td align="left"><textarea name="indicaciones_1" id="indicaciones_1" cols="20" rows="5" tabindex="6"></textarea></td>
                <td class="textosParaInputs" align="right">INIDICACIONES:</td>
                <td align="left"><textarea name="indicaciones_2" id="indicaciones_2" cols="20" rows="5" tabindex="11"></textarea></td>
          	</tr>
       </table>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><div id="masRecetas" style="width:100%"></div></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><br /><span class="textoRojo">Los medicamentos en rojo tienen CERO existencias<br /><small><small>No se imprimen recetas con medicamentos de CERO existencias pero son tomadas en cuenta para concertación</small></small><br />Favor de buscar un alternativo antes de guardar la receta</span><br />
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: inicio('inicioMedicoEsp.php');" tabindex="11" />&nbsp;&nbsp;&nbsp;&nbsp;
  <?php echo $boton; ?>
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="buscar" style=" display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">BUSCAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="textosParaInputs" align="right" width="191">TIPO DE BUSQUEDA: </td>
              <td align="left"><select name="tipo_busqueda" id="tipo_busqueda" onchange="javascript: buscarPor();">
                <option value="cedula" selected="selected">C&eacute;dula</option>
                <option value="nombre">Nombre</option>
              </select>
              </td>
            </tr>
            <tr>
            <td colspan="2">
            	<div id="buscarPorCedula">
                <form id="selDH" method="POST" action="javascript: buscarDH(document.getElementById('cedulaBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                  <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                    <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="2" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
              </div>

                <div id="buscarPorNombre" style="display:none;">
                <form id="selDHN" method="POST" action="javascript: buscarDHN(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                  <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                  <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                  <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                  <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                    <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                  <td align="left" colspan="3"><div id="derechohabientes2">Ingrese los datos del derechohabiente y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="4" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosDH();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="4" align="center"></td>
                </tr>
              </table>
              </form>
              </div>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>

<form id="agregarDH" method="POST" action="javascript: agregarDHenCitaForma();">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="divAgregarDH" style=" display:none; height:0px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
              <td align="left"><input type="text" name="cedulaAgregar" id="cedulaAgregar" maxlength="10" /> / <input type="text" name="cedulaTipoAgregar" id="cedulaTipoAgregar" maxlength="2" size="5" /></td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_pAgregar" type="text" id="ap_pAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="ap_mAgregar" type="text" id="ap_mAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" />
      <input name="nombreAgregar" type="text" id="nombreAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO:</td>
    <td align="left"><input name="telefonoAgregar" type="text" id="telefonoAgregar" size="20" maxlength="10" /><span class="textosParaInputs"> EDAD: </span><input name="fecha_nacAgregar" type="text" id="fecha_nacAgregar" size="20" maxlength="3" />
    </td>
</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" size="64" maxlength="50" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estadoAgregar" id="estadoAgregar" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');"> 
    </select><span class="textosParaInputs">MUNICIPIO: </span><select name="municipioAgregar" id="municipioAgregar">
    </select>
    </td>
  </tr>
        
            <tr>
              <td colspan="2" align="center"><div id="divBotones_EstadoAgregarDH"></div>
                </td>
              </tr>
        
            <tr>
              <td colspan="2" align="center"></td>
            </tr>
          </table>
        </div>
	</td>
</tr>
</table>
</form>

</body>
</html>
