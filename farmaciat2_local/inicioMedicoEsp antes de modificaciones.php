<?php

set_time_limit(180);
date_default_timezone_set('America/Mexico_City');
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');
if (!isset($_SESSION[$galleta_citas])) {
	header('Location: index.php');
}
$out = '';
if (count($_SESSION[$galleta_citas]) == 0) {
	$datosUsuario = getUsuarioXid($_SESSION['idUsuario']);
	setcookie("idMedico", $_SESSION['idUsuario']);
	$datosMedico = getMedicoXid($datosUsuario["id_medico"]);
	$dts = regresarServicioConsultorio($datosUsuario["id_medico"]);
	$id_servicio = $dts["id_servicio"];
	$id_consultorio = $dts["id_consultorio"];
	$servicio = getServicioXid($id_servicio);
	$consultorio = getConsultorioXid($id_consultorio);
	$_SESSION['idDr'] = $datosUsuario["id_medico"];
	$_SESSION['idServ'] = $id_servicio;
	$_SESSION['IdCon'] = $id_consultorio;
	$out = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"780\"><tr><td align=\"center\" class=\"tituloVentana2013\">";
	$out.= "CITAS DE " . ponerAcentos(strtoupper($datosMedico['titulo'] . " " . $datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']) . "<br>");
	$out.= "Consultorio: " . strtoupper($consultorio['nombre']) . " | Servicio: " . strtoupper($servicio);
	$out .= "</td></tr></table><br>";
	setcookie($galleta_enca, $out, time() + 24 * 60 * 60, '/'); 
} else {
	$out = $_COOKIE[$galleta_enca];
}

$contra = true;
print($out);

$hoy = date('Ymd');
?>

<center>
    <table width="800" border="0" height="600">
        <tr>
            <td width="687">
                <table width="630" border="0" cellspacing="0" cellpadding="0" class="ventana">
                    <tr>
                        <td class="tituloVentana" height="23"><?php echo formatoDia($hoy, 'tituloCitasXdia'); ?></td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <?php
                            $out = "";
							$out.= "<table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
							$galle = array();
							$insert = "";
							$detCitas = getDetallesCitaAgendaInicio($hoy, $_SESSION['idDr'], 'N');
							if (count($_SESSION[$galleta_citas]) == 0) { // SI ES LA PRIMERA VEZ QUE ENTRA SE CONSULTA A LA BASE DE DATOS
								$horario = citasXdia($hoy, $_SESSION['idDr']);
								$citasDelDia = count($horario);
								if ($citasDelDia > 0) {
									 for ($i = 0; $i < $citasDelDia; $i++) {
										$total = 0;
										$datosCita = citasOcupadasXdia($horario[$i]['id_horario'], $hoy);
										$asistio = '';
										if ($datosCita['id_derecho'] == "-1") { //no hay cita a x hora
											$claseParaDia = claseParaDia($horario[$i]['tipo_cita']);
											$nombre = "";
											$datos = "";
											$botones = "";
										} else { // hay cita
											if ($insert == "") $insert .= "INSERT INTO citas_detalles (id_cita, id_derecho, id_servicio, n_citas, fecha_estudio, fecha_medicamento, fecha_cita, hizo_contra, hizo_constancia, asistio, fecha, id_medico, tipo_cita) VALUES";
											$claseParaDia = claseParaDia($horario[$i]['tipo_cita']);
											$datosDerecho = getDatosDerecho($datosCita['id_derecho']);
											$datosUsuario = getUsuarioCitaXid($datosCita['id_usuario']);
											$nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
											$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];

											$clasSI = '';
											$clasNO = '';
											$disableNO = '';
											foreach($detCitas as $indice => $valor) {
												if (in_array($datosCita['id_cita'], $valor)) {
													$asistio = $valor['asistio'];
												}
											}
											if ($asistio == 'SI') {
												$clasSI = ' r_on';
												$clasNO = 'dis';
												$disableNO = ' disabled="disabled" ';
											}
											if ($asistio == 'NO') {
												$clasNO = ' r_off';
											}
											$botones = 'ASISTIO: 
												<label class="label_radio' . $clasSI . '" for="asistio_SI_N_' . $datosCita['id_cita'] . '">
													<input name="sino_' . $datosCita['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $datosCita['id_derecho'] . '_' . $horario[$i]['id_horario'] . '" id="asistio_SI_N_' . $datosCita['id_cita'] . '" value="SI" type="radio" class="si_no" />
													SI
												</label>
												<label class="label_radio' . $clasNO . '" for="asistio_NO_N_' . $datosCita['id_cita'] . '">
													<input name="sino_' . $datosCita['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $datosCita['id_derecho'] . '_' . $horario[$i]['id_horario'] . '" id="asistio_NO_N_' . $datosCita['id_cita'] . '" value="NO" type="radio" class="si_no"' . $disableNO . ' />
												   NO
												</label>
											';

											$insert .= " ('" . $datosCita['id_cita'] . "','" . $datosCita['id_derecho'] . "','" . $_SESSION['idServ'] . "','0','','','','','','','" . $hoy . "','" . $_SESSION['idDr'] . "','N'),";
										}
										$galle[] = array(
											'id_cita' => $datosCita['id_cita'],
											'id_servicio' => $_SESSION['idServ'],
											'id_derecho' => $datosCita['id_derecho'],
											'id_horario' => $horario[$i]['id_horario'],
											'nombre' => $nombre,
											'datos' => $datos,
											'clase' => $claseParaDia,
											'hora_inicio' => $horario[$i]['hora_inicio'],
											'hora_fin' => $horario[$i]['hora_fin'],
											'asistio' => $asistio
										);
										$out.="
											  <tr>
												<td class=\"" . $claseParaDia . "\">
													<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
													  <tr>
														<td width=\"100\" class=\"citaXdiaHora\">" . formatoHora($horario[$i]['hora_inicio']) . " - " . formatoHora($horario[$i]['hora_fin']) . "</td>
														<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
														<td width=\"180\" rowspan=\"2\" class=\"citaXdiaHora\"><div id=\"div_btnN_" . $datosCita['id_cita'] . "\" style=\"position:relative; top:10px;\">" . $botones . "</div></td>
													  </tr>
													  <tr>
														<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($datos) . "</td>
													  </tr>
													</table>
												</td>
											  </tr>";
									 }
								} else {
									$out = '<span class="textosParaInputs">NO HAY CITAS DISPONIBLES PARA ESTE DIA<br><br></span>';
								}
								$insert = substr($insert,0,strlen($insert)-1);
								$res = ejecutarSQL($insert);
							} else { // SI YA HABÍA ENTRADO EN EL DIA EL DR SE RECUPERA LA INFORMACION DEL ARREGLO DE SESSION
								$galle = $_SESSION[$galleta_citas];
								$tCitas = count($galle);
								for ($i=0; $i<$tCitas; $i++) {
									$botones = '';
									if ($galle[$i]['id_derecho'] != '-1') {
										$clasSI = '';
										$clasNO = '';
										$disableNO = '';
										foreach($detCitas as $indice => $valor) {
											if (in_array($galle[$i]['id_cita'], $valor)) {
												$galle[$i]['asistio'] = $valor['asistio'];
											}
										}
										if ($galle[$i]['asistio'] == 'SI') {
											$clasSI = ' r_on';
											$clasNO = 'dis';
											$disableNO = ' disabled="disabled" ';
										}
										if ($galle[$i]['asistio'] == 'NO') {
											$clasNO = ' r_off';
										}
										$botones = 'ASISTIO: 
											<label class="label_radio' . $clasSI . '" for="asistio_SI_N_' . $galle[$i]['id_cita'] . '">
												<input name="sino_' . $galle[$i]['id_cita'] . '_' . $galle[$i]['id_servicio'] . '_' . $galle[$i]['id_derecho'] . '_' . $galle[$i]['id_horario'] . '" id="asistio_SI_N_' . $galle[$i]['id_cita'] . '" value="SI" type="radio" class="si_no" />
												SI
											</label>
											<label class="label_radio' . $clasNO . '" for="asistio_NO_N_' . $galle[$i]['id_cita'] . '">
												<input name="sino_' . $galle[$i]['id_cita'] . '_' . $galle[$i]['id_servicio'] . '_' . $galle[$i]['id_derecho'] . '_' . $galle[$i]['id_horario'] . '" id="asistio_NO_N_' . $galle[$i]['id_cita'] . '" value="NO" type="radio" class="si_no"' . $disableNO . ' />
											   NO
											</label>
										';
									}
									$out.="
										  <tr>
											<td class=\"" . $galle[$i]['clase'] . "\">
												<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
												  <tr>
													<td width=\"100\" class=\"citaXdiaHora\">" . formatoHora($galle[$i]['hora_inicio']) . " - " . formatoHora($galle[$i]['hora_fin']) . "</td>
													<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($galle[$i]['nombre']) . "</td>
													<td width=\"180\" rowspan=\"2\" class=\"citaXdiaHora\"><div id=\"div_btnN_" . $galle[$i]['id_cita'] . "\" style=\"position:relative; top:10px;\">" . $botones . "</div></td>
												  </tr>
												  <tr>
													<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($galle[$i]['datos']) . "</td>
												  </tr>
												</table>
											</td>
										  </tr>";
								}
							}
							$out.= "</table>";
                            echo $out;
							$_SESSION[$galleta_citas] = $galle;
							?>
                            <br />
                            <div id="estadoEliminando"></div></td>
                    </tr>
                </table>
                <p align="center"><input type="button" value="CERRAR CONSULTA E IMPRIMIR SM-1-10" class="boton g naranja" onclick="cerrarAgendaMedico('<?php echo $_SESSION['idUsuario'] ?>', '<?php echo $_SESSION['idServ'] ?>');" /></p>
                <table width="500" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">
                    <tr>
                        <td width="35" height="25" class="citaXdiaPRV"></td>
                        <td width="129">Citas Primera Vez</td>
                        <td width="35" class="citaXdiaSUB"></td>
                        <td width="138">Citas Subsecuentes</td>
                        <td width="35" class="citaXdiaPRO"></td>
                        <td width="138">Citas Procedimientos</td>
                        <td width="100"><div id="CanCitas">
                                <a href="javascript:EliminarCitas();" class="botones_menu" title="Cancelar Cita"><img src="diseno/cancelarCitaMedico.png"  width="48" height="48"/>Cancelar Cita</a></div></td>
                    </tr>
                </table>
                </p>
<?php

$totalCitas = 0;
$out = "<br>";
$out.= "
<table width=\"630\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
<tr>
  <td class=\"tituloVentana\" height=\"23\">" . formatoDia($hoy, 'tituloCitasXdia') . " - EXTEMPORANEAS</td>
</tr>
";
if (count($_SESSION[$galleta_citasE]) == 0) { // SI ES LA PRIMERA VEZ QUE ENTRA SE CONSULTA A LA BASE DE DATOS
	$citasExtemporaneas = getCitasExtemporaneas($hoy, $_SESSION['idDr']);
	$insert = "";
	$galleE = array();
	$totalCitas = count($citasExtemporaneas);
	$detCitas = getDetallesCitaAgendaInicio($hoy, $_SESSION['idDr'], 'E');
	for ($i = 0; $i < $totalCitas; $i++) {
		if ($insert == "") $insert .= "INSERT INTO citas_detalles (id_cita, id_derecho, id_servicio, n_citas, fecha_estudio, fecha_medicamento, fecha_cita, hizo_contra, hizo_constancia, asistio, fecha, id_medico, tipo_cita) VALUES";
		$claseParaDia = claseParaDia($citasExtemporaneas[$i]['tipo_cita']);
		$datosDerecho = getDatosDerecho($citasExtemporaneas[$i]['id_derecho']);
		$datosUsuario = getUsuarioCitaXid($citasExtemporaneas[$i]['id_usuario']);
		$nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
		$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $citasExtemporaneas[$i]['extra1'] . " - " . $citasExtemporaneas[$i]['observaciones'];

		$clasSI = '';
		$clasNO = '';
		$disableNO = '';
		$asistio = '';
		foreach($detCitas as $indice => $valor) {
			if (in_array($citasExtemporaneas[$i]['id_cita'], $valor)) {
				$asistio = $valor['asistio'];
			}
		}
		if ($asistio == 'SI') {
			$clasSI = ' r_on';
			$clasNO = 'dis';
			$disableNO = ' disabled="disabled" ';
		}
		if ($asistio == 'NO') {
			$clasNO = ' r_off';
		}
		$botones = 'ASISTIO: 
			<label class="label_radio' . $clasSI . '" for="asistio_SI_' . $i . '">
				<input name="sino_' . $citasExtemporaneas[$i]['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $citasExtemporaneas[$i]['id_derecho'] . '_' . $citasExtemporaneas[$i]['id_cita'] . '" id="asistio_SI_' . $i . '" value="SI" type="radio" class="si_noE" />
				SI
			</label>
			<label class="label_radio' . $clasNO . '" for="asistio_NO_' . $i . '">
				<input name="sino_' . $citasExtemporaneas[$i]['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $citasExtemporaneas[$i]['id_derecho'] . '_' . $citasExtemporaneas[$i]['id_cita'] . '" id="asistio_NO_' . $i . '" value="NO" type="radio" class="si_noE"' . $disableNO . ' />
			   NO
			</label>
		';





/*
		$botones = 'ASISTIO: 
			<label class="label_radio" for="asistio_SI_' . $i . '">
				<input name="sino_' . $citasExtemporaneas[$i]['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $citasExtemporaneas[$i]['id_derecho'] . '_' . $citasExtemporaneas[$i]['id_cita'] . '" id="asistio_SI_' . $i . '" value="SI" type="radio" class="si_noE" />
				SI
			</label>
			<label class="label_radio" for="asistio_NO_' . $i . '">
				<input name="sino_' . $citasExtemporaneas[$i]['id_cita'] . '_' . $_SESSION['idServ'] . '_' . $citasExtemporaneas[$i]['id_derecho'] . '_' . $citasExtemporaneas[$i]['id_cita'] . '" id="asistio_NO_' . $i . '" value="NO" type="radio" class="si_noE" />
			   NO
			</label>
		';
*/	
		$insert .= " ('" . $citasExtemporaneas[$i]['id_cita'] . "','" . $citasExtemporaneas[$i]['id_derecho'] . "','" . $_SESSION['idServ'] . "','0','','','','','','','" . $hoy . "','" . $_SESSION['idDr'] . "','E'),";
		$galleE[] = array(
			'id_cita' => $citasExtemporaneas[$i]['id_cita'],
			'id_servicio' => $_SESSION['idServ'],
			'id_derecho' => $citasExtemporaneas[$i]['id_derecho'],
			'id_horario' => $citasExtemporaneas[$i]['id_cita'],
			'nombre' => $nombre,
			'datos' => $datos,
			'clase' => $claseParaDia,
			'hora_inicio' => $citasExtemporaneas[$i]['hora_inicio'],
			'hora_fin' => $citasExtemporaneas[$i]['hora_fin'],
			'asistio' => $asistio
		);
		$out.="
			  <tr>
				<td class=\"" . $claseParaDia . "\">
					<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td width=\"100\" class=\"citaXdiaHora\">" . formatoHora($citasExtemporaneas[$i]['hora_inicio']) . " - " . formatoHora($citasExtemporaneas[$i]['hora_fin']) . "</td>
						<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
						<td width=\"180\" rowspan=\"2\" class=\"citaXdiaHora\"><div id=\"div_btnE_" . $citasExtemporaneas[$i]['id_cita'] . "\" style=\"position:relative; top:10px;\">" . $botones . "</div></td>
					  </tr>
					  <tr>
						<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\" >" . ponerAcentos($datos) . "</td>
					  </tr>
					</table>
				</td>
			  </tr>";
			}
	$insert = substr($insert,0,strlen($insert)-1);
	$res = ejecutarSQL($insert);
} else { // SI YA HABÍA ENTRADO EN EL DIA EL DR SE RECUPERA LA INFORMACION DEL ARREGLO DE SESSION
	$galleE = $_SESSION[$galleta_citasE];
	$totalCitas = count($galleE);
	$detCitas = getDetallesCitaAgendaInicio($hoy, $_SESSION['idDr'], 'E');
	for ($i=0; $i<$totalCitas; $i++) {
		$botones = '';
		if ($galleE[$i]['id_derecho'] != '-1') {
			$clasSI = '';
			$clasNO = '';
			$disableNO = '';


			foreach($detCitas as $indice => $valor) {
				if (in_array($galleE[$i]['id_cita'], $valor)) {
					$galleE[$i]['asistio'] = $valor['asistio'];
				}
			}
			if ($galleE[$i]['asistio'] == 'SI') {
				$clasSI = ' r_on';
				$clasNO = 'dis';
				$disableNO = ' disabled="disabled" ';
			}
			if ($galleE[$i]['asistio'] == 'NO') {
				$clasNO = ' r_off';
			}
			$botones = 'ASISTIO: 
				<label class="label_radio' . $clasSI . '" for="asistio_SI_' . $i . '">
					<input name="sino_' . $galleE[$i]['id_cita'] . '_' . $galleE[$i]['id_servicio'] . '_' . $galleE[$i]['id_derecho'] . '_' . $galleE[$i]['id_horario'] . '" id="asistio_SI_' . $i . '" value="SI" type="radio" class="si_noE" />
					SI
				</label>
				<label class="label_radio' . $clasNO . '" for="asistio_NO_' . $i . '">
					<input name="sino_' . $galleE[$i]['id_cita'] . '_' . $galleE[$i]['id_servicio'] . '_' . $galleE[$i]['id_derecho'] . '_' . $galleE[$i]['id_horario'] . '" id="asistio_NO_' . $i . '" value="NO" type="radio" class="si_noE"' . $disableNO . ' />
				   NO
				</label>
			';
		}
		$out.="
			  <tr>
				<td class=\"" . $galleE[$i]['clase'] . "\">
					<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td width=\"100\" class=\"citaXdiaHora\">" . formatoHora($galleE[$i]['hora_inicio']) . " - " . formatoHora($galleE[$i]['hora_fin']) . "</td>
						<td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($galleE[$i]['nombre']) . "</td>
						<td width=\"180\" rowspan=\"2\" class=\"citaXdiaHora\"><div id=\"div_btnE_" . $galleE[$i]['id_cita'] . "\" style=\"position:relative; top:10px;\">" . $botones . "</div></td>
					  </tr>
					  <tr>
						<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($galleE[$i]['datos']) . "</td>
					  </tr>
					</table>
				</td>
			  </tr>";
	}
}
$out .= "</table>";
$_SESSION[$galleta_citasE] = $galleE;
if ($totalCitas > 0)
    echo $out;
	
?>
            </td>
        </tr>
    </table>
</center>
