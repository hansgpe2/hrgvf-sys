<?php
error_reporting(E_ALL^E_NOTICE);
include_once('lib/misFunciones.php');
session_start ();

$datosRecetas = getRecetaXCodigoBarrasActivas($_REQUEST["cod_bar"]);
if ($datosRecetas == "") {
	$out = 'No existe receta activa con el c&oacute;digo de barras ingresado';
} else {
	$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="780">';
	$out .= '<tr><td colspan="5" class="tituloVentana">Receta ' . $_REQUEST["cod_bar"] . '</td></tr>';
	$out .= '<tr><th>Fecha y Hora</th><th>Folio</th><th>M&eacute;dico</th><th>Servicio</th><th>Derechohabiente</th></tr>';
	$datosMedico = getMedicoXid($datosRecetas['id_medico']);
	$datosServicio = getServicioXid($datosRecetas['id_servicio']);
	$datosDerecho = getDatosDerecho($datosRecetas['id_derecho']);
	$out .= '<tr class="botones_menu"><td>' . $datosRecetas['fecha'] . ' - ' . $datosRecetas['hora'] . '</td><td>' . $datosRecetas['serie'] . '' . $datosRecetas['folio'] . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td>' . ponerAcentos($datosServicio) . '</td><td>' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td></tr>';
	$out .= '<tr><td colspan="5">&nbsp;</td></tr>';
	$conceptos =  getDatosMedicamentoEnReceta($datosRecetas["id_receta"]);
	if ($datosRecetas["status"] == "2") $boton = ' disabled="disabled"'; else $boton = '';
	if ($conceptos != "") {
		$out .= '<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
		$tConceptos = count($conceptos);
		for ($i=0; $i < $tConceptos; $i++) {
//			$Extistencia_medicamento = getExistenciasMedicamentos($conceptos[$i]["id_medicamento"]);
			$medicamento = getDatosMedicamentos($conceptos[$i]["id_medicamento"]);
			if ($conceptos[$i]['extra1'] == 's') {
				$usuario = getUsuarioXid($conceptos[$i]["id_usuario_surtio"]);
				$readonly = ' disabled="disabled"';
				$datosExtra = '<p class="textoRojo">Medicamento surtido por: ' . $usuario["nombre"] . ' | ' . formatoDia($conceptos[$i]["fecha"],"fecha") . ' - ' . $conceptos[$i]["hora"] . '</p>';
			} else {
				$readonly = '';
				$datosExtra = '';
			}
			$out .= '<td width="50%" align="center">
						<table width="350" border="1" cellspacing="0" cellpadding="3">
							<tr><td align="left">Cantidad: </td><td>' . $conceptos[$i]["cantidad"] . '</td></tr>
							<tr><td align="left">Tratamiento: </td><td>' . $conceptos[$i]["dias"] . ' D&iacute;as</td></tr>
							<tr><td align="left">Medicamento: </td><td class="botones_menu">' . $medicamento["descripcion"] . '</td></tr>
							<tr><td align="left">Indicaciones: </td><td>' . $conceptos[$i]["indicaciones"] . '</td></tr>
							<tr><td align="center" colspan="2">Se surti&oacute; el medicamento: <input id="surtio_' . $i . '" name="surtio_' . $i . '" type="checkbox" value="' . $conceptos[$i]["id_medicamento"] . '" checked="checked"' . $readonly . ' />' . $datosExtra . '</td></tr>
						</table>
					</td>';
		}
		$out .= '</tr></table>';
		$out .= '<tr><td colspan="5"><br>Motivo de Cancelaci&oacute;n: <input type="text" name="motivo" id="motivo" value="" size="60"><br></td></tr>';
		$out .= '<tr><td colspan="5"><br><input name="cancelar" type="button" value="Cancelar Receta" onclick="javascript: validarCancelarReceta(' . $datosRecetas["id_receta"] . ')" /></td></tr>';
	}
	$out .= '</table><div id="enviando"></div></center>';
}
print($out);

?>

