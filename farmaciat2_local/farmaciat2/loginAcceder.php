<?php
include_once('lib/misFunciones.php');
include_once('lib/misFunciones2013.php');
set_time_limit(180);
date_default_timezone_set('America/Mexico_City');
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$usuario_hacerLogin = "-1";
if (isset($_POST['usuario'])) {
  $usuario_hacerLogin = $_POST['usuario'];
}
$password_hacerLogin = "-1";
if (isset($_POST['pass'])) {
  $password_hacerLogin = $_POST['pass'];
}
$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
mysql_select_db($database_bdissste, $bdissste);
$query_hacerLogin = sprintf("SELECT * FROM usuarios WHERE login = %s and pass = %s and status = '0' and st = '1'", GetSQLValueString($usuario_hacerLogin, "text"),GetSQLValueString($password_hacerLogin, "text"));
$hacerLogin = mysql_query($query_hacerLogin, $bdissste) or die(mysql_error());
$row_hacerLogin = mysql_fetch_assoc($hacerLogin);
$totalRows_hacerLogin = mysql_num_rows($hacerLogin);


mysql_free_result($hacerLogin);
//mysql_close($dbissste);
//header("Content-type:text/xml");
if ($totalRows_hacerLogin == 1) {
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "INSERT INTO logs values(NULL,'" . $row_hacerLogin['id_usuario'] . "|ingreso al sistema " . date("H:i d/m/Y") . "','0')";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$query_sesion = "UPDATE usuarios SET sesionId='" . session_id() . "' WHERE id_usuario='" . $row_hacerLogin['id_usuario'] . "' LIMIT 1";
	$sesion = mysql_query($query_sesion, $bdissste) or die(mysql_error());
	$_SESSION['idUsuario'] = $row_hacerLogin['id_usuario'];
	$_SESSION['tipoUsuario'] = $row_hacerLogin['tipo_usuario'];
//	$_SESSION['IdCon'] = $row_hacerLogin['id_consultorio'];  POR SI SE RESTRINGE EL ACCESO A USUARIOS X SERVICIO O CONSULTORIO
//	$_SESSION['idServ'] = $row_hacerLogin['id_servicio'];

	date_default_timezone_set('America/Mexico_City');
	$hoyC = date('Ymd');
	$galleta_citas = 'citas'.$hoyC.$_SESSION['idUsuario'];
	$galleta_citasE = 'citasE'.$hoyC.$_SESSION['idUsuario'];
	$galleta_enca = 'enca'.$hoyC.$_SESSION['idUsuario'];
	$_SESSION[$galleta_citas] = array();
	$_SESSION[$galleta_citasE] = array();
	$_SESSION[$galleta_enca] = array();

	$datosUsuario = getUsuarioXid($_SESSION['idUsuario']);
	$dts = regresarServicioConsultorio($datosUsuario["id_medico"]);
	$_SESSION['idDr'] = $datosUsuario["id_medico"];
	$_SESSION['idServ'] = $dts["id_servicio"];
	$_SESSION['IdCon'] = $dts["id_consultorio"];


	$cuantasCitasDetallesHay = getDetallesCitaAgendaValidar($hoyC,$_SESSION['idDr']);
	if ($cuantasCitasDetallesHay == 0) { // si no hay citas detalles es porque es el primer login del dia
		$horario = citasXdia($hoyC, $_SESSION['idDr']);
		$citasDelDia = count($horario);
		$insert = "";
		$insert2 = "";
		if ($citasDelDia > 0) {
			 for ($i = 0; $i < $citasDelDia; $i++) {
				$total = 0;
				$datosCita = citasOcupadasXdia($horario[$i]['id_horario'], $hoyC);
				$asistio = '';
				if ($datosCita['id_derecho'] == "-1") { //no hay cita a x hora
					$claseParaDia = claseParaDia($horario[$i]['tipo_cita']);
					$nombre = "";
					$datos = "";
					$botones = "";
				} else { // hay cita
					$datosDerecho = getDatosDerecho($datosCita['id_derecho']);
					$datosUsuario = getUsuarioCitaXid($datosCita['id_usuario']);
					if (!isset($datosUsuario['nombre'])) {
                        $sql = "select * from usuarios_contrarreferencias where id_usuario=" . $datosCita['id_usuario'];
                        $datosUsuario = ejecutarSQLAgenda($sql);
					}
					if (!isset($datosUsuario['nombre'])) $datosUsuario['nombre'] = '';
					$nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
					$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr(@$datosUsuario['nombre'], 0, 12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
					$claseParaDia = claseParaDia($horario[$i]['tipo_cita']);
					$citaDetalle = getDetallesCita($datosCita['id_cita'],'N');
					if (count($citaDetalle) > 0) {	// verificamos si ya existe el detalle de la cita en farmacia o es una cita que se agregó recientemente
						$_SESSION[$galleta_citas]['N'.$citaDetalle['id_cita']] = array(
							'id_cita' => $citaDetalle['id_cita'],
							'id_servicio' => $citaDetalle['id_servicio'],
							'id_derecho' => $citaDetalle['id_derecho'],
							'id_horario' => $horario[$i]['id_horario'],
							'nombre' => $nombre,
							'datos' => $datos,
							'clase' => $claseParaDia,
							'hora_inicio' => $horario[$i]['hora_inicio'],
							'hora_fin' => $horario[$i]['hora_fin'],
							'asistio' => $citaDetalle['asistio']
						);
					} else {
						if ($insert == "") $insert .= "INSERT INTO citas_detalles (id_cita, id_derecho, id_servicio, n_citas, fecha_estudio, fecha_medicamento, fecha_cita, hizo_contra, hizo_constancia, asistio, fecha, id_medico, tipo_cita, diagnostico) VALUES";
						if ($insert2 == "") $insert2 .= "INSERT INTO citas_detalles2 (id_cita, tipo_cita, hora_contra, hora_cita, hora_receta, hora_constancia, hora_estudio, hora_si_no, extra1, extra2) VALUES";
						$insert .= " ('" . $datosCita['id_cita'] . "','" . $datosCita['id_derecho'] . "','" . $_SESSION['idServ'] . "','0','','','','','','','" . $hoyC . "','" . $_SESSION['idDr'] . "','N',''),";
						$insert2 .= " ('" . $datosCita['id_cita'] . "','N','','','','','','','',''),";
						$_SESSION[$galleta_citas]['N'.$datosCita['id_cita']] = array(
							'id_cita' => $datosCita['id_cita'],
							'id_servicio' => $_SESSION['idServ'],
							'id_derecho' => $datosCita['id_derecho'],
							'id_horario' => $horario[$i]['id_horario'],
							'nombre' => $nombre,
							'datos' => $datos,
							'clase' => $claseParaDia,
							'hora_inicio' => $horario[$i]['hora_inicio'],
							'hora_fin' => $horario[$i]['hora_fin'],
							'asistio' => ''
						);
					}
				}
			 }
			 if ($insert != "") {
				$insert = substr($insert,0,strlen($insert)-1);
				$insert2 = substr($insert2,0,strlen($insert2)-1);
				$res = ejecutarSQL($insert);
				$res = ejecutarSQL($insert2);
			 }
		}
	} else { // ya se habia logeado en el día, ya solo hay que crear las variables de sesion
		$horario = citasXdia($hoyC, $_SESSION['idDr']);
		$citasDelDia = count($horario);
		if ($citasDelDia > 0) {
			 for ($i = 0; $i < $citasDelDia; $i++) {
				$total = 0;
				$datosCita = citasOcupadasXdia($horario[$i]['id_horario'], $hoyC);
				$asistio = '';
				if ($datosCita['id_derecho'] == "-1") { //no hay cita a x hora
					$claseParaDia = claseParaDia($horario[$i]['tipo_cita']);
					$nombre = "";
					$datos = "";
					$botones = "";
				} else { // hay cita
					$citaDetalle = getDetallesCita($datosCita['id_cita'],'N');
					$datosDerecho = getDatosDerecho($datosCita['id_derecho']);
					$datosUsuario = getUsuarioCitaXid($datosCita['id_usuario']);
					if (!isset($datosUsuario['nombre'])) {
                        $sql = "select * from usuarios_contrarreferencias where id_usuario=" . $datosCita['id_usuario'];
                        $datosUsuario = ejecutarSQLAgenda($sql);
					}
					if (!isset($datosUsuario['nombre'])) $datosUsuario['nombre'] = '';					
					$nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
					$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr(@$datosUsuario['nombre'], 0, 12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
					$claseParaDia = claseParaDia($horario[$i]['tipo_cita']);
					$citaDetalle = getDetallesCita(@$datosCita['id_cita'],'N');
					$_SESSION[$galleta_citas]['N'.@$citaDetalle['id_cita']] = array(
						'id_cita' => @$citaDetalle['id_cita'],
						'id_servicio' => @$citaDetalle['id_servicio'],
						'id_derecho' => @$citaDetalle['id_derecho'],
						'id_horario' => @$horario[$i]['id_horario'],
						'nombre' => @$nombre,
						'datos' => @$datos,
						'clase' => @$claseParaDia,
						'hora_inicio' => @$horario[$i]['hora_inicio'],
						'hora_fin' => @$horario[$i]['hora_fin'],
						'asistio' => @$citaDetalle['asistio']
					);
				}
			 }
		}
	}

	$citasExtemporaneas = getCitasExtemporaneas($hoyC, $_SESSION['idDr']);
	$totalCitas = count($citasExtemporaneas);
	$insert = "";
	$insert2 = "";
	if ($totalCitas > 0) {
		for ($i = 0; $i < $totalCitas; $i++) {
			$claseParaDia = claseParaDia($citasExtemporaneas[$i]['tipo_cita']);
			$datosDerecho = getDatosDerecho($citasExtemporaneas[$i]['id_derecho']);
			$datosUsuario = getUsuarioCitaXid($citasExtemporaneas[$i]['id_usuario']);
			if (!isset($datosUsuario['nombre'])) {
                $sql = "select * from usuarios_contrarreferencias where id_usuario=" . $citasExtemporaneas[$i]['id_usuario'];
                $datosUsuario = ejecutarSQLAgenda($sql);
			}
			if (!isset($datosUsuario['nombre'])) $datosUsuario['nombre'] = '';

			$nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
			$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $citasExtemporaneas[$i]['extra1'] . " - " . $citasExtemporaneas[$i]['observaciones'];
			$citaDetalle = getDetallesCita($citasExtemporaneas[$i]['id_cita'],'E');
			if (count($citaDetalle) > 0) {	// verificamos si ya existe el detalle de la cita en farmacia o es una cita que se agregó recientemente
				$_SESSION[$galleta_citasE]['E'.$citaDetalle['id_cita']] = array(
					'id_cita' => $citaDetalle['id_cita'],
					'id_servicio' => $citaDetalle['id_servicio'],
					'id_derecho' => $citaDetalle['id_derecho'],
					'id_horario' => $citasExtemporaneas[$i]['id_cita'],
					'nombre' => $nombre,
					'datos' => $datos,
					'clase' => $claseParaDia,
					'hora_inicio' => $citasExtemporaneas[$i]['hora_inicio'],
					'hora_fin' => $citasExtemporaneas[$i]['hora_fin'],
					'asistio' => $citaDetalle['asistio']
				);
			} else {
				if ($insert == "") $insert .= "INSERT INTO citas_detalles (id_cita, id_derecho, id_servicio, n_citas, fecha_estudio, fecha_medicamento, fecha_cita, hizo_contra, hizo_constancia, asistio, fecha, id_medico, tipo_cita, diagnostico) VALUES";
				if ($insert2 == "") $insert2 .= "INSERT INTO citas_detalles2 (id_cita, tipo_cita, hora_contra, hora_cita, hora_receta, hora_constancia, hora_estudio, hora_si_no, extra1, extra2) VALUES";
				$insert .= " ('" . $citasExtemporaneas[$i]['id_cita'] . "','" . $citasExtemporaneas[$i]['id_derecho'] . "','" . $_SESSION['idServ'] . "','0','','','','','','','" . $hoyC . "','" . $_SESSION['idDr'] . "','E',''),";
				$insert2 .= " ('" . $citasExtemporaneas[$i]['id_cita'] . "','E','','','','','','','',''),";
				$_SESSION[$galleta_citasE]['E'.$citasExtemporaneas[$i]['id_cita']] = array(
					'id_cita' => $citasExtemporaneas[$i]['id_cita'],
					'id_servicio' => $_SESSION['idServ'],
					'id_derecho' => $citasExtemporaneas[$i]['id_derecho'],
					'id_horario' => $citasExtemporaneas[$i]['id_cita'],
					'nombre' => $nombre,
					'datos' => $datos,
					'clase' => $claseParaDia,
					'hora_inicio' => $citasExtemporaneas[$i]['hora_inicio'],
					'hora_fin' => $citasExtemporaneas[$i]['hora_fin'],
					'asistio' => ''
				);
			}
		}
		if ($insert != "") {
			$insert = substr($insert,0,strlen($insert)-1);
			$insert2 = substr($insert2,0,strlen($insert2)-1);
			$res = ejecutarSQL($insert);
			$res = ejecutarSQL($insert2);
		}
	}
	
	$consulta = getConsultaCierre($hoyC, $_SESSION['idUsuario'], $_SESSION['idDr']);
	if (count($consulta) > 0) { // ya existe el registro, ya se habia logeado este usuario
		$n_logins = (int)$consulta['extra1']+1;
		$res = ejecutarSQL("UPDATE consulta_cierre SET extra1='" . $n_logins . "' WHERE fecha='" . $hoyC . "' AND id_usuario='" . $_SESSION['idUsuario'] . "' AND id_medico='" . $_SESSION['idDr'] . "' LIMIT 1;");
	} else { // registro nuevo
		$res = ejecutarSQL("INSERT INTO consulta_cierre VALUES('" . $hoyC . "', '" . $_SESSION['idUsuario'] . "', '" . $_SESSION['idDr'] . "', '" . date('Y-m-d H:i:s') . "', '', '0', '1', '');");
	}
	
	switch($row_hacerLogin['tipo_usuario']) {
		case '0':$out = "Sera direccionado a http://www.issstezapopan.net/medicamentos/admin para poder ingresar al sistema";  //administrador
				break;
		case '1':$out = "inicioMedico.php";  // medico
				break;
		case '2':$out = "inicioFarmacia.php";  //farmacia
				break;
		case '3':$out = "inicioExterna.php";  //consulta externa
				break;
		case '4':$out = "inicioColectivos.php";  //reportes
				break;
		case '5':$out = "inicioVisor.php";  //reportes
				break;
		case '8':$out = "inicioMedicoEsp.php";  //medico esp
				break;
	}
	
	print "1|" . $row_hacerLogin['nombre'] . "|" . $row_hacerLogin['tipo_usuario'] . "|" . $out;
} else {
	print "0|-1|-1|Nombre de Usuario o Contraseña Incorrectos";
}
?>