<?php
error_reporting(E_ERROR);
include_once('lib/misFunciones.php');
session_start ();

$datosRecetas = getRecetas("0");
$out = '<center><table border="0" cellpadding="0" cellspacing="0" width="780">';
$out .= '<tr><td colspan="6" class="tituloVentana">Recetas Canceladas</td></tr>';
$out .= '<tr><th>Receta</th><th>Se canceló</th><th>Motivo</th><th>M&eacute;dico</th><th>Servicio</th><th>Derechohabiente</th></tr>';
$tDatos = count($datosRecetas);

for ($i=0; $i<$tDatos; $i++) {
	$datosMedico = getMedicoXid($datosRecetas[$i]['id_medico']);
	$datosServicio = getServicioXid($datosRecetas[$i]['id_servicio']);
	$datosDerecho = getDatosDerecho($datosRecetas[$i]['id_derecho']);
	$datosReceta = getRecetaCancelada($datosRecetas[$i]['id_receta']);
	$out .= '<tr class="botones_menu"><td>' . $datosRecetas[$i]['codigo_barras'] . '</td><td>' . $datosReceta['fecha'] . ' - ' . $datosReceta['hora'] . '</td><td>' . $datosReceta['motivo'] . '</td><td>' . ponerAcentos($datosMedico['ap_p']) . ' ' . ponerAcentos($datosMedico['ap_m']) . ' ' . ponerAcentos($datosMedico['nombres']) . '</td><td>' . ponerAcentos($datosServicio) . '</td><td>' . ponerAcentos($datosDerecho['ap_p']) . ' ' . ponerAcentos($datosDerecho['ap_m']) . ' ' . ponerAcentos($datosDerecho['nombres']) . '</td></tr>';
}
$out .= '</table></center>';

print($out);

?>

