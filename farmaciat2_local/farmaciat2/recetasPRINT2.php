<?php
include_once('lib/misFunciones.php');

function getLeyendaMedicamento($id_medicamento) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	mysql_query("SET CHARACTER SET utf8"); 
	mysql_query("SET NAMES utf8"); 
	$query_query = "SELECT * FROM medicamentos_leyendas WHERE id_medicamento='" . $id_medicamento . "' LIMIT 1";
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	$ret = "";
	if ($totalRows_query>0){
			$ret=$row_query['leyenda'];
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}


if (isset($_REQUEST["id_receta"])) {
    $datosReceta = getRecetaXid($_REQUEST["id_receta"]);
	$datosUsuario = getUsuarioXid($datosReceta['id_usuario_elaboro']);
    $servicio = getServicioXid($datosReceta["id_servicio"]);
    $derecho = getDatosDerecho($datosReceta["id_derecho"]);
    $medico = getMedicoXid($datosReceta["id_medico"]);
    $conceptos = getDatosMedicamentoEnReceta($datosReceta["id_receta"]);
	if (strlen($datosUsuario['extra1'])>0) {
		$datosReceta['dgp'] = $datosUsuario['extra1'];
		$medico["cedula"] = $datosUsuario['login'];
		$medico["ap_p"] = $medico["ap_m"] = '';
		$medico["nombres"] = $datosUsuario['nombre'];
	}
    $tablaMed = array();
    $tablaMedF = array();
    $tablaMedCant = array();
    $tablaVale = array();
	$leyendas = array();
    $tablaMed[0] = '';
    $tablaMedF[0] = '';
    $tablaMedCant[0] = '';
    $tablaVale[0] = '';
    $tablaMed[1] = '';
    $tablaMedF[1] = '';
    $tablaMedCant[1] = '';
    $tablaVale[1] = '';
    if ($conceptos != "") {
        $medicamento = getDatosMedicamentos($conceptos[0]["id_medicamento"]);
		$leyendas[0] = getLeyendaMedicamento($conceptos[0]["id_medicamento"]);
        if ($conceptos[0]["extra1"] == "N") { // es medicamento sin existencia
            $tablaVale[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
							<tr>
								<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
							</tr>
							<tr>
								<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[0]["indicaciones"])) . '&nbsp;</td>
							</tr>
					</table>';
            if ($medicamento["id_medicamento"] == 0)
                $medicamento["id_medicamento"] = "";
/*				if (substr_count($conceptos[0]["unidad"],"-") == 0) $periodo = "DIAS"; else {
					$aPeriodo = explode("-", $conceptos[0]["unidad"]);
					$periodo = $aPeriodo[1];
				}
*/				$periodo = "DIAS";
            $tablaValeF[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
							<tr>
								<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
							</tr>
							<tr>
								<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[0]["indicaciones"])) . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top">
									<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
											<tr>
												<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">' . $periodo . ' DE TRATAMIENTO.</td>
											</tr>
											<tr>
												<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[0]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[0]["dias"] . '&nbsp;<br><br><span class="contenido8Bold" style="text-decoration: underline;">TELEFONO PACIENTE</span></td>
											</tr>
											<tr>
												<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top"><strong></strong></td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
											</tr>
									</table>
								</td>
							</tr>
					</table>';
        } else {
            if ($medicamento["id_medicamento"] != 0) {
/*				if (substr_count($conceptos[0]["unidad"],"-") == 0) $periodo = "DIAS"; else {
					$aPeriodo = explode("-", $conceptos[0]["unidad"]);
					$periodo = $aPeriodo[1];
				}
*/
				$periodo = "DIAS";
                $tablaMed[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
								</tr>
								<tr>
									<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[0]["indicaciones"])) . '&nbsp;</td>
								</tr>
						</table>';
                if ($medicamento["id_medicamento"] == 0)
                    $medicamento["id_medicamento"] = "";
                $tablaMedF[0] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 1</td>
								</tr>
								<tr>
									<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[0]["indicaciones"])) . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top">
										<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
												<tr>
													<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">' . $periodo . ' TRATAMIENTO.</td>
												</tr>
												<tr>
													<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[0]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[0]["dias"] . '&nbsp;<br><br><span class="contenido8Bold" style="text-decoration: underline;">TELEFONO PACIENTE</span></td>
												</tr>
												<tr>
													<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top"><strong></strong></td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
												</tr>
										</table>
									</td>
								</tr>
						</table>';
                $tablaMedCant[0] = '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido6" valign="top">CLAVE</td><td align="center" class="contenido6BordeIzq" valign="top">CANT.</td><td align="center" class="contenido6BordeIzq" valign="top">' . $periodo . ' TRATAMIENTO.</td>
								</tr>
								<tr>
									<td align="center" class="contenido8bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[0]["cantidad"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[0]["dias"] . '&nbsp;</td>
								</tr>
						</table>';
            }
        }
        $medicamento = getDatosMedicamentos($conceptos[1]["id_medicamento"]);
		$leyendas[1] = getLeyendaMedicamento($conceptos[1]["id_medicamento"]);
        if ($conceptos[1]["extra1"] == "N") { // es medicamento sin existencia
            $tablaVale[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
							<tr>
								<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
							</tr>
							<tr>
								<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[1]["indicaciones"])) . '&nbsp;</td>
							</tr>
						</table>';
            if ($medicamento["id_medicamento"] == 0)
                $medicamento["id_medicamento"] = "";
/*				if (substr_count($conceptos[1]["unidad"],"-") == 0) $periodo = "DIAS"; else {
					$aPeriodo = explode("-", $conceptos[1]["unidad"]);
					$periodo = $aPeriodo[1];
				}*/
				$periodo = "DIAS";
            $tablaValeF[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
							<tr>
								<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
							</tr>
							<tr>
								<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[1]["indicaciones"])) . '&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="contenido8" valign="top">
									<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
											<tr>
												<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">' . $periodo . ' TRATAMIENTO.</td>
											</tr>
											<tr>
												<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[1]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["dias"] . '&nbsp;<br><br><span class="contenido8Bold" style="text-decoration: underline;">NOMBRE PACIENTE</span></td>
											</tr>
											<tr>
												<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["cantidad"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
											</tr>
									</table>
								</td>
							</tr>
					</table>';
        } else {
            if ($medicamento["id_medicamento"] != 0) {
                $tablaMed[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
								</tr>
								<tr>
									<td align="center" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[1]["indicaciones"])) . '&nbsp;</td>
								</tr>
							</table>';
                if ($medicamento["id_medicamento"] == 0)
                    $medicamento["id_medicamento"] = "";
/*				if (substr_count($conceptos[1]["unidad"],"-") == 0) $periodo = "DIAS"; else {
					$aPeriodo = explode("-", $conceptos[1]["unidad"]);
					$periodo = $aPeriodo[1];
				}*/
				$periodo = "DIAS";
                $tablaMedF[1] = '<table width="100%" border="0" cellpadding="0" cellspacing="3" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido8bold">MEDICAMENTO NUMERO 2</td>
								</tr>
								<tr>
									<td align="left" class="contenido8">' . $medicamento["descripcion"] . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8bold" valign="top">INDICACIONES</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top" height="30">' . ponerAcentos(strtoupper($conceptos[1]["indicaciones"])) . '&nbsp;</td>
								</tr>
								<tr>
									<td align="left" class="contenido8" valign="top">
										<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
												<tr>
													<td align="center" class="contenido8Bold" valign="top">CLAVE</td><td align="center" class="contenido8BordeIzq" valign="top">CANT.</td><td align="center" class="contenido8BordeIzq" valign="top">' . $periodo . ' TRATAMIENTO.</td>
												</tr>
												<tr>
													<td align="center" class="contenido12bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top"><img src="barcode/barcode.php?code=' . $conceptos[1]["cantidad"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["dias"] . '&nbsp;<br><br><span class="contenido8Bold" style="text-decoration: underline;">NOMBRE PACIENTE</span></td>
												</tr>
												<tr>
													<td align="center" class="contenido12bold" valign="top"><img src="barcode/barcode.php?code=' . $medicamento["id_medicamento"] . '&tam=1"></td><td align="center" class="contenido12BordeIzq" valign="top">' . $conceptos[1]["cantidad"] . '&nbsp;</td><td align="center" class="contenido12BordeIzq" valign="top">&nbsp;</td>
												</tr>
										</table>
									</td>
								</tr>
						</table>';
                $tablaMedCant[1] = '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="tituloEncabezadoConBorde">
								<tr>
									<td align="center" class="contenido6" valign="top">CLAVE</td><td align="center" class="contenido6BordeIzq" valign="top">CANT.</td><td align="center" class="contenido6BordeIzq" valign="top">' . $periodo . ' TRATAMIENTO.</td>
								</tr>
								<tr>
									<td align="center" class="contenido8bold" valign="top">' . $medicamento["id_medicamento"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[1]["cantidad"] . '&nbsp;</td><td align="center" class="contenido8BordeIzq" valign="top">' . $conceptos[1]["dias"] . '&nbsp;</td>
								</tr>
						</table>';
            }
        }
    }
//	$medicamento = getDatosMedicamentos();
    if ($datosReceta != "") {
        $titulo = $datosReceta["serie"] . ponerCeros($datosReceta["folio"], 7);
        $dia = substr($datosReceta["fecha"], 6, 2);
        $mes = substr($datosReceta["fecha"], 4, 2);
        $ano = substr($datosReceta["fecha"], 0, 4);
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title>Receta: <?php echo $titulo ?></title>
                <style type="text/css">
                    @import url("lib/impresion.css") print;
                    @import url("lib/reportes.css") screen;
                </style>
            </head>

            <body onload="javascript: window.print();">
                <?php
                if (($tablaMed[0] != '') || ($tablaMed[1] != '')) {

                    echo '<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">&nbsp;P A C I E N T E</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td width="222" align="center" height="80"><img src="diseno/logoEncabezado.jpg" width="160" /></td>
                	<td align="center"><span class="tituloIssste">DIRECCION MEDICA</span><br /><span class="tituloEncabezado">RECETA MEDICA</span></td>
                	<td width="200" align="left"><img src="barcode/barcode.php?code=' . $datosReceta["codigo_barras"] . '&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado">' . $titulo . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6">ENTIDAD FEDERATIVA<br /><span class="contenido8">' . $datosReceta["entidad_federativa"] . '</span></td>
                	<td width="100" align="center" class="contenido6BordeIzq">
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center">' . $dia . '</td><td align="center">' . $mes . '</td><td align="center">' . $ano . '</td>
                        	</tr>
                        </table>
                    </td>
                	<td align="left" width="130" class="contenido6BordeIzq">CLAVE DE LA UNIDAD MEDICA<br /><span class="contenido8">' . $datosReceta["clave_unidad_medica"] . '</span></td>
                	<td align="left" class="contenido6BordeIzq">SERVICIO<br /><span class="contenido8">' . $servicio . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="left" class="contenido6">NOMBRE DEL PACIENTE<br /><span class="contenido8">' . ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]) . '</span></td>
                	<td align="left" class="contenido6BordeIzq"><span class="contenido8bold">DIAGNOSTICO</span><br /><span class="contenido8">' . ponerAcentos($datosReceta["numero_vale"]) . '</span></td>
                	<td align="left" width="100" class="contenido6BordeIzq">CEDULA<br /><span class="contenido8">' . $derecho["cedula"] . "/" . $derecho["cedula_tipo"] . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top">' . $tablaMed[0] . '</td>
                	<td align="center" width="50%" class="contenido6" valign="top">' . $tablaMed[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8">' . $datosReceta["dgp"] . '</span></td>
                    <td align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br /><span class="contenido8">' . $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]) . '</span></td>
                	<td width="70" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top">' . $tablaMedCant[0] . '</td>
                	<td align="center" width="50%" class="contenido6BordeIzq" valign="top">' . $tablaMedCant[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td style="height:14px;">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="tituloEncabezado" valign="top">' . $leyendas[0] . '</td>
                	<td align="center" width="50%" class="tituloEncabezado" valign="top">' . $leyendas[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p class="separador">&nbsp;</p>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">&nbsp;F A R M A C I A</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
               	  <td width="223" align="left" class="contenido6">
                  <table width="100%"><tr><td>
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center">' . $dia . '</td><td align="center">' . $mes . '</td><td align="center">' . $ano . '</td>
                        	</tr>
                      </table>
                        </td></tr><tr><td>
                        <table width="100%">
                          <tr>
                            <td width="80%"> NOMBRE DEL PACIENTE<br />
                              <span class="contenido8bold">' . ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]) . '</span> <br />
                              CEDULA<br />
                              <span class="contenido8">' . $derecho["cedula"] . "/" . $derecho["cedula_tipo"] . '</span></td>
                            <td width="20%"><img src="barcode/barcode.php?code=' . $derecho["cedula"] . '&tam=1" alt="barcode" /></td>
                          </tr>
                        </table></td></tr></table></td>
                	<td width="177" align="center">
                    	<table class="codigoMedico">
                        	<tr>
                            	<td align="center">CODIGO<br />BARRAS<br />MEDICO</td>
                            </tr>
                        </table>
                    </td>
                	<td width="144" align="left"><img src="diseno/logoEncabezado.jpg" width="80" /><br><img src="barcode/barcode.php?code=' . $datosReceta["codigo_barras"] . '&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado">' . $titulo . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8">' . $datosReceta["dgp"] . '</span></td>
                    <td align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br /><span class="contenido8">' . $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]) . '</span></td>
                    <td align="left" class="contenido6BordeIzq"><span class="contenido8bold">DIAGNOSTICO</span><br /><span class="contenido8">' . ponerAcentos($datosReceta["numero_vale"]) . '</span></td>
                	<td width="70" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top">' . $tablaMedF[0] . '</td>
                	<td align="center" width="50%" class="contenido6" valign="top">' . $tablaMedF[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td style="height:14px;">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="tituloEncabezado" valign="top">' . $leyendas[0] . '</td>
                	<td align="center" width="50%" class="tituloEncabezado" valign="top">' . $leyendas[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
</table>';
                }
                if (($tablaVale[0] != '') || ($tablaVale[1] != '')) {

                    echo '<br /><br /><br />
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">&nbsp;P A C I E N T E</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td width="222" align="center" height="80"><img src="diseno/logoEncabezado.jpg" width="160" /></td>
                	<td align="center"><span class="tituloIssste">DIRECCION MEDICA</span><br /><span class="tituloEncabezado">RECETA MEDICA</span></td>
                	<td width="200" align="left"><img src="barcode/barcode.php?code=' . $datosReceta["codigo_barras"] . '&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado">' . $titulo . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6">ENTIDAD FEDERATIVA<br /><span class="contenido8">' . $datosReceta["entidad_federativa"] . '</span></td>
                	<td width="100" align="center" class="contenido6BordeIzq">
                    	<table width="100%" border="0" cellpadding="3" cellspacing="0">
                        	<tr>
                            	<td align="center" width="30%">DIA</td><td align="center" width="30%">MES</td><td align="center">AÑO</td>
                            </tr>
                        	<tr>
                            	<td align="center">' . $dia . '</td><td align="center">' . $mes . '</td><td align="center">' . $ano . '</td>
                        	</tr>
                        </table>
                    </td>
                	<td align="left" width="130" class="contenido6BordeIzq">CLAVE DE LA UNIDAD MEDICA<br /><span class="contenido8">' . $datosReceta["clave_unidad_medica"] . '</span></td>
                	<td align="left" class="contenido6BordeIzq">SERVICIO<br /><span class="contenido8">' . $servicio . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
            	  <td align="left" class="contenido6">NOMBRE DEL PACIENTE<br />
            	    <span class="contenido8">' . ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]) . '</span></td>
            	  <td><img src="barcode/barcode.php?code=' . $derecho["cedula"] . '&tam=1" alt="barcode" /></td>
                	<td align="left" class="contenido6BordeIzq"><span class="contenido8bold">DIAGNOSTICO</span><br /><span class="contenido8">' . ponerAcentos($datosReceta["numero_vale"]) . '</span></td>
                	<td align="left" width="100" class="contenido6BordeIzq">CEDULA<br /><span class="contenido8">' . $derecho["cedula"] . "/" . $derecho["cedula_tipo"] . '</span></td><td></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top">' . $tablaVale[0] . '</td>
                	<td align="center" width="50%" class="contenido6" valign="top">' . $tablaVale[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8">' . $datosReceta["dgp"] . '</span></td>
                    <td width="965" align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br />
                  <span class="contenido8">' . $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]) . '</span></td>
                	<td width="311" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="contenido6" valign="top">' . $tablaMedCant[0] . '</td>
                	<td align="center" width="50%" class="contenido6BordeIzq" valign="top">' . $tablaMedCant[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td style="height:14px;">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="tituloEncabezado" valign="top">' . $leyendas[0] . '</td>
                	<td align="center" width="50%" class="tituloEncabezado" valign="top">' . $leyendas[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<p class="separador">&nbsp;</p>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
    	<td class="tituloEncabezadoConBorde" align="center">
        	<span class="copia">&nbsp;F A R M A C I A</span>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
                	<td width="230" align="left" class="contenido6"><table width="100%">
               	      <tr>
                	      <td><table width="100%" border="0" cellpadding="3" cellspacing="0">
                	        <tr>
                	          <td align="center" width="30%">DIA</td>
                	          <td align="center" width="30%">MES</td>
                	          <td align="center">AÑO</td>
              	          </tr>
                	        <tr>
                	          <td align="center">' . $dia . '</td>
                	          <td align="center">' . $mes . '</td>
                	          <td align="center">' . $ano . '</td>
              	          </tr>
              	        </table></td>
              	      </tr>
                	    <tr>
                	      <td><table width="100%">
                	        <tr>
                	          <td width="80%"> NOMBRE DEL PACIENTE<br />
                	            <span class="contenido8bold">' . ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]) . '</span> <br />
                	            CEDULA<br />
                	            <span class="contenido8">' . $derecho["cedula"] . "/" . $derecho["cedula_tipo"] . '</span></td>
                	          <td width="20%"><img src="barcode/barcode.php?code=' . $derecho["cedula"] . '&tam=1" alt="barcode" /></td>
              	          </tr>
              	        </table></td>
              	      </tr>
           	      </table></td>
                	<td align="center">
                    	<table class="codigoMedico">
                        	<tr>
                            	<td align="center">CODIGO<br />BARRAS<br />MEDICO</td>
                            </tr>
                        </table>
                    </td>
                	<td width="200" align="left"><img src="diseno/logoEncabezado.jpg" width="80" /><br><img src="barcode/barcode.php?code=' . $datosReceta["codigo_barras"] . '&tam=1" alt="barcode" /><br /><br />SERIE: <span class="tituloEncabezado">' . $titulo . '</span></td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="5" cellspacing="0">
            	<tr>
                	<td width="100" align="left" class="contenido6" valign="top">REG. D.G.P.<br /><span class="contenido8">' . $datosReceta["dgp"] . '</span></td>
                    <td width="688" align="left" class="contenido6BordeIzq">CLAVE, NOMBRE Y FIRMA DEL MEDICO TRATANTE<br />
                  <span class="contenido8">' . $medico["cedula"] . " - " . ponerAcentos($medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"]) . '</span></td>
                    <td width="271" align="left" class="contenido6BordeIzq"><span class="contenido8bold">DIAGNOSTICO</span><br />
                  <span class="contenido8">' . ponerAcentos($datosReceta["numero_vale"]) . '</span></td>
                	<td width="307" align="left" class="contenido6BordeIzq" valign="top">AUTORIZACION</td>
                </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td class="tituloEncabezadoConBorde">
        	<table width="100%" border="0" cellpadding="3" cellspacing="0">
            	<tr>
            	  <td align="center" class="contenido6" valign="top">' . $tablaValeF[0] . '</td>
            	  <td align="center" class="contenido6" valign="top">' . $tablaValeF[1] . '</td>
          	  </tr>
            </table>
        </td>
    </tr>
	<tr>
    	<td style="height:14px;">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td align="center" width="50%" class="tituloEncabezado" valign="top">' . $leyendas[0] . '</td>
                	<td align="center" width="50%" class="tituloEncabezado" valign="top">' . $leyendas[1] . '</td>
                </tr>
            </table>
        </td>
    </tr>
</table>';
                }
            } else {
                print("No existe la receta");
            }
        } else {
            print("Error en variable id receta");
        }

?>

        </body></html>