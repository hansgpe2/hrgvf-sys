<?php
session_start ();
include_once('lib/misFunciones.php');
require_once './reader/CompoundDocument.inc.php';
require_once './reader/CompoundDocument.inc.php';
require_once './reader/BiffWorkbook.inc.php';

if (isset($_REQUEST["archivo"])) {
	if ($_REQUEST["archivo"] != "") $fileName = $_REQUEST["archivo"];
}

if (!is_readable ($fileName)) die ('Cannot read ' . $fileName);

$doc = new CompoundDocument ('iso-8859-1');
$doc->parse (file_get_contents ($fileName));
$wb = new BiffWorkbook ($doc);
$wb->parse ();

$sentencias = array();
//$sentencias[] = "TRUNCATE TABLE `medicamentos_existencias`;";
foreach ($wb->sheets as $sheetName => $sheet)
{
	if($sheetName == SIAM_NOMBRE_TABLA) {
//		echo '<table cellspacing = "0">';
		for ($row = SIAM_FILA_INICIAL; $row < $sheet->rows (); $row ++)
		{
/*			echo '<tr>';
			for ($col = SIAM_COL_ID_MEDICAMENTO; $col < SIAM_COL_PRECIO; $col ++)
			{
	
				if (!isset ($sheet->cells [$row][$col])) continue;
				if ((is_null($sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value)) || (!is_numeric($sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value))) continue;
				$cell = $sheet->cells [$row][$col];
				echo '<td style = "' . $cell->style->css () . '" rowspan = "' . $cell->rowspan . '" colspan = "' . $cell->colspan . '">';
				echo is_null ($cell->value) ? '&nbsp;' : $cell->value;
				echo '</td>';
			}
			echo '</tr>';
*/			if ((is_null($sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value)) || (!is_numeric($sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value))) continue;
			$medicamento = getDatosMedicamentos($sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value);
			if ($medicamento["id_medicamento"] == 0) { // no existe el medicamento en la base de datos, hay que agregarlo
				$sentencias[] = "INSERT INTO medicamentos VALUES('" . $sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value . "','" . $sheet->cells [$row][SIAM_COL_PARTIDA]->value . "','','" . $sheet->cells [$row][SIAM_COL_DESCRIPCION]->value . "','','','" . str_replace("$","",$sheet->cells [$row][SIAM_COL_PRECIO]->value) . "','1','');";
				$sentencias[] = "INSERT INTO medicamentos_existencias VALUES('" . $sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value . "','" . $sheet->cells [$row][SIAM_COL_EXISTENCIA]->value . "','" . $sheet->cells [$row][SIAM_COL_EXISTENCIA]->value . "');";
			} else { // si el medicamento ya esta en la base de datos
				$existencia = getExistenciasMedicamentos($sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value);
				if ($existencia["id_medicamento"] == 0) { //si no hay existencias de este medicamento en la base de datos
					$sentencias[] = "INSERT INTO medicamentos_existencias VALUES('" . $sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value . "','" . $sheet->cells [$row][SIAM_COL_EXISTENCIA]->value . "','" . $sheet->cells [$row][SIAM_COL_EXISTENCIA]->value . "');";
				} else { // si ya hay existencias de este medicamento se guarda la existencia anterior
					$sentencias[] = "UPDATE medicamentos_existencias SET existencia_anterior='" . $existencia["existencia_actual"] . "', existencia_actual='" . $sheet->cells [$row][SIAM_COL_EXISTENCIA]->value . "' WHERE id_medicamento='" . $sheet->cells [$row][SIAM_COL_ID_MEDICAMENTO]->value . "';";
				}
			}
		}
//		echo '</table>';
	}
}

$resultado = "";
foreach($sentencias as $indice => $valor) {
	$res = ejecutarSQL($valor);
	if ($res[0] != "0") $resultado .= "- Error en query: " . $valor . "<br>";
}
if ($resultado == "") echo "ok"; else echo $resultado;
?>