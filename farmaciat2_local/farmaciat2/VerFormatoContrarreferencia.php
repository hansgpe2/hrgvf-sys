<?php
session_start();
include_once('lib/misFunciones.php');
function getContraReferencia1Pablo($idRef) {
    global $hostname_bdissste;
    global $username_bdisssteR;
    global $password_bdisssteR;
    global $database_bdisssteR;
    mysql_connect($hostname_bdissste, $username_bdisssteR, $password_bdisssteR);
    mysql_select_db($database_bdisssteR);
    $sql = "select * from contrarreferencias where id_contra=" . $idRef;
    $query = mysql_query($sql);
    $ret = array();
    if (mysql_errno() == 0)
        if (mysql_num_rows($query)>0)
            $ret = mysql_fetch_array($query);

    return $ret;
}

    $idContra=$_GET['folioContrarrefencia'];
    $ref= getContraReferencia1Pablo($idContra);
    $idDerecho =$ref['id_derecho'];
    $doc=$ref['id_medico'];
    $derecho = getDatosDerecho($idDerecho);
    $medico=getMedicoXid($doc);
	$servicio=  getServicioXid($ref['id_servicio']);
    $unidad = obtenerUnidadMedica($ref['id_unidad']);
	$medicinas=ObtenerMedicamentosXReferencia($idContra);
        $count=  count($medicinas);
    if(count($ref)>0)
    {
        $evo=$ref['evolucion'];
        $datclin=$ref['resumen'];
        $diag=$ref['diagnostico'];
		$diagRef=$ref['diagnostico_ref'];
        $tratamientos=$ref['tratamiento'];
        $recom=$ref['recomendaciones'];
		$fecha=$ref['fecha_contrarreferencia'];
        $fecha=date("d/m/Y",  strtotime($fecha));
		$cont=$ref['status_citas'];
		
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>CONTRARREFERENCIA</title>
        <style type="text/css">
            @import url("lib/impresion2.css") print;
        </style>
        <link href="lib/impresion2.css" rel="stylesheet" type="text/css">
    </head>
</head>

<body>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
<tr>
  <td width="271" align="center" class="encabezado"><table width="100%" border="0" cellspacing="5" cellpadding="5">
    <tr>
      <td width="95" align="center"><img src="diseno/logoEncabezado.jpg" alt="" width="90" height="107" /></td>
      <td width="317" align="center" class="encabezado"><p>HOSPITAL REGIONAL </p>
        <p>"DR. VALENTÍN GÓMEZ FARÍAS" <br />
          FORMATO DE CONTRARREFERENCIA</p></td>
      <td width="152"><p><img src="diseno/LogoHRVGF.jpg" alt="" width="141" height="104" /></p></td>
    </tr>
  </table></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:solid; border-width:1px;">
  <tr>
    <td><p><span class="titulos">CLINICA DE ADSCRIPCION</span><span class="datos"> <?php echo ponerAcentos($unidad['nombre']); ?></span></td>
  </tr>
  <tr>
    <td><p><span class="titulos">LOCALIDAD</span><span class="datos"> <?php echo $derecho['municipio'] . "," . $derecho['estado'] ?></span> &nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">&nbsp;&nbsp;&nbsp;&nbsp; FECHA</span><span class="datos"> <?php echo $fecha; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">CEDULA</span><span class="datos">: <?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo'] ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">NOMBRE</span>&nbsp;&nbsp;&nbsp;<span class="datos"><?php echo ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']); ?></span> <span class="titulos">SERVICIO QUE ENVIA:</span> <?php echo $servicio; ?></p></td>
  <tr>
    <td><?php
                        if ($cont == 0)
                            echo "El paciente  continuar&aacute; su tratamiento con su m&eacute;dico familiar";
                        else
                            echo "El paciente  continuar&aacute; tratamiento con el m&eacute;dico especialista";
                        ?></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:solid; border-width:1px;">
  <tr>
    <td><p align="center" class="encabezado">INFORME DEL MEDICO CONSULTADO</p></td>
  </tr>
  <tr>
    <td class="titulos">RESUMEN DE DATOS CLINICOS: </strong></td>
  </tr>
  <tr>
    <td class="datos"><?php echo ponerAcentos($datclin); ?></td>
  </tr>
  <tr>
    <td class="titulos">DIAGNOSTICOS DE:</td>
  </tr>
  <tr>
    <td><table width="100%" class="tablaDiag">
      <tr>
        <td class="titulos">Referencia</td>
        <td class="titulos">Contrarreferencia</td>
      </tr>
      <tr>
        <td class="datos"><?php echo ponerAcentos($diagRef); ?></td>
        <td class="datos"><?php echo ponerAcentos($diag); ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="titulos">SINTESIS DE LA EVOLUCION:</td>
  </tr>
  <tr>
    <td class="datos"><?php echo ponerAcentos($evo); ?></td>
  </tr>
  <tr>
    <td class="titulos">TRATAMIENTO INSTITUIDO:</td>
  </tr>
  <tr>
    <td class="datos"><?php echo ponerAcentos($tratamientos); 
    if(count($medicinas)){?>
      <table width="100%" border="1">
        <tr>
            <td class="encabezado">Clave</td>
          <td class="encabezado">Medicamento</td>
          <td class="encabezado">Tipo Tratamiento</td>
          <td class="encabezado">Tratamiento en Dias</td>
          <td class="encabezado">Cantidad de cajas</td>
          <td class="encabezado">Indicaciones</td>
        </tr>
        <?php
                            $i = 0;
                            
                            while ($i < $count) {
                                    $medicina = getDatosMedicamentos($medicinas[$i]['id_medicamento']);
                                    ?>
        <tr>
            <td><?php echo $medicina['id_medicamento']; ?></td>
            <td><?php $desc=strtok($medicina['descripcion'],"/");
                      echo $desc; ?></td>
          <td><?php
                                            if ($medicinas[$i]['cronico'] == 1)
                                                echo "Critico";
                                            else
                                                echo "Temporal"
                                                ?></td>
          <td><?php echo $medicinas[$i]['dias']; ?>&nbsp;</td>
          <td><?php echo $medicinas[$i]['cajas']; ?>&nbsp;</td>
          <td><?php echo nl2br($medicinas[$i]['indicaciones']); ?></td>
        </tr>
        <?php
                                    
                                $i++;
                            }
                            ?>
      </table>
    <?php } ?>
      <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td class="titulos">RECOMENDACIONES:</td>
  </tr>
  <tr>
    <td class="datos"><?php echo ponerAcentos($recom); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="30pt" align="center">_______________________________________________________</td>
  </tr>
  <tr>
    <td align="center"><strong><?php echo $medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'] . "<br /> DGP: " . $medico['ced_pro']."<br>".$servicio; ?></strong></td>
  </tr>
  <tr>
    <td colspan="4" align="center" class="nota">Para Revisar las recetas proporcionadas al paciente<br />
      Dirijase
      a la siguiente Direccion http://192.165.95.30/farmaciag/, ingrese con el usuario visorvisor, contrase&ntilde;a 123456</td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td height="168" colspan="4" align="center" class="nota"><p><strong>INDICACIONES:</strong><br>
    <ol>
      <li>Al derechohabiente contra referido  se deberá entregar por <strong>duplicado </strong>este documento para sus trámites.</li>
      <li>Deberá presentar uno de los originales  en el <strong>Departamento de Referencia y  Contrarreferencia</strong> de esta unidad <strong>Hosp.  Regional Dr. Valentín Gómez Farías.</strong></li>
    </ol>
      El duplicado se deberá presentarse en <strong>su clínica de adscripción</strong> que lo refirió  a esta unidad en un periodo <strong>no mayor a 15 días hábiles</strong>,  y <strong>solicitar  una cita</strong>  en la cual su <strong>médico familia</strong>r revisará la  Contrarreferencia del médicos de especialidad   y  Solicitara los medicamentos  necesarios para realizar el control y seguimiento necesario en su clínica de  adscripción 
      </td>
  </tr>
  <tr>
    <td class="nota"></td>
  </tr>
</table>
</body>
</html>
