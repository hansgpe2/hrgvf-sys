<?php
error_reporting(E_ALL^E_NOTICE);
session_start ();
include_once('lib/misFunciones.php');

if ((isset($_GET["serie"])) && (isset($_GET["folio"]))) {
	$serie = $_GET["serie"];
	$folio = intval($_GET["folio"]);
	$Terror = "";
	
	$folios = getFoliosXserie($serie);
	$tFolios = count($folios);
	if ($tFolios > 0) {
		for ($i=0; $i<$tFolios; $i++) {
			$folio_inicial_bd = intval($folios[$i]["folio_inicial"]);
			$folio_final_bd = intval($folios[$i]["folio_final"]);
			if (($folio >= $folio_inicial_bd) && ($folio <= $folio_final_bd)) {
				$medico = getMedicoXid($folios[$i]["id_medico"]);
				$Terror .= "Folio Asignado a: <br><b>" . $medico["ap_p"] . " " . $medico["ap_m"] . " " . $medico["nombres"] . "</b><br>el día: <br>" . formatoDia($folios[$i]["fecha_asignacion"],"tituloCitasXdia");
			}
		}
	}
	
	if ($Terror != "") {
		$receta = getRecetaXfolio($folio);
		if (count($receta) > 0) {
			$servicio = getServicioXid($receta["id_servicio"]);
			$derecho = getDatosDerecho($receta["id_derecho"]);
			$usuario_elaboro = getUsuarioXid($receta["id_usuario_elaboro"]);
			$usuario_surtio = getUsuarioXid($receta["id_usuario_surtio"]);
			$status = $statusRecetas[$receta["status"]];
			$fecha = formatoDia($receta["fecha"],"tituloCitasXdia");
			if (count($usuario_surtio) > 0) {
				$surtio = '<br>Usuario que la surtio: ' . $usuario_surtio["nombre"];
			} else {
				$surtio = '';
			}
			$Terror .= '<br><br>Folio utilizado el día<br><b>' . $fecha . ' ' . $receta["hora"] . 'hrs.</b><br><br>DATOS DE LA RECETA<br><br>Derechohabiente: ' . ponerAcentos($derecho["ap_p"] . " " . $derecho["ap_m"] . " " . $derecho["nombres"]) . '<br>Servicio: ' . $servicio . '<br>';
			$Terror .= 'Usuario que la elaboró: ' . $usuario_elaboro["nombre"] . $surtio . '<br>Estado de la Receta: <b>' . $status . '</b><br><br>';
		} else {
			$Terror .= '<br><br><b>Folio sin utilizar</b><br><br>';
		}
	}
	if ($Terror == "") {
		print("Folio sin asignar<br><br>");
	} else {
		print($Terror);
	}
} else {
	print("No se pudo ingresar al paciente, pongase en contacto con el administrador del sistema");
}
?>
