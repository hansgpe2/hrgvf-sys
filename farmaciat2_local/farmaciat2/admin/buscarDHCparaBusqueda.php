<?php
include_once('../lib/funcionesAdmin.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<?php
	$derecho = getDatosDerecho($_GET['cod_bar']);
    $out = "<select name=\"dh\" id=\"dh\">";
	if ($derecho['cedula'] == '-1') {
        $out .= "<option value=\"-1\" selected=\"selected\">NO EXISTE DERECHOHABIENTE CON CODIGO DE BARRAS " . $_GET['cod_bar'] . "</option>";
	} else {
        $out.= "<option value=\"" . $_GET['cod_bar'] . "|" . $derecho['cedula'] . "|" . $derecho['cedula_tipo'] . "|" . ponerAcentos($derecho['ap_p']) . "|" . ponerAcentos($derecho['ap_m']) . "|" . ponerAcentos($derecho['nombres']) . "|" . ponerAcentos($derecho['telefono']) . "|" . ponerAcentos($derecho['direccion']) . "|" . $derecho['estado'] . "|" . $derecho['municipio'] . "|" . $derecho['fecha_nacimiento'] . "\">" . $derecho['cedula'] . "/" . $derecho['cedula_tipo'] . " " . ponerAcentos($derecho['ap_p']) . " " . ponerAcentos($derecho['ap_m']) . " " . ponerAcentos($derecho['nombres']) . "</option>";
	}
	$out .= '</select>';
	echo $out;
?>
</body>
</html>
