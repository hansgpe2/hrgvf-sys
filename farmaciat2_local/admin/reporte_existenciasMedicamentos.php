<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start ();
require_once("../lib/funcionesAdmin.php");

function getMedicamentosConExistenciasOrden($orden) {
	global $hostname_bdissste;
	global $username_bdissste;
	global $password_bdissste;
	global $database_bdissste;
	$bdissste = mysql_pconnect($hostname_bdissste, $username_bdissste, $password_bdissste) or trigger_error(mysql_error(),E_USER_ERROR); 
	mysql_select_db($database_bdissste, $bdissste);
	$query_query = "SELECT m.id_medicamento, m.partida, m.codigo_barras, m.descripcion, m.presentacion, m.unidad, m.precio, m.status, m.extra1, e.id_medicamento, e.existencia_anterior, e.existencia_actual FROM medicamentos m, medicamentos_existencias e WHERE m.status='1' AND m.id_medicamento = e.id_medicamento ORDER BY " . $orden;
	$query = mysql_query($query_query, $bdissste) or die(mysql_error());
	$row_query = mysql_fetch_assoc($query);
	$totalRows_query = mysql_num_rows($query);
	$ret = array();
	if ($totalRows_query>0){
		do{
			$ret[]=array(
						'id_medicamento' => $row_query['id_medicamento'],
						'partida' => $row_query['partida'],
						'codigo_barras' => $row_query['codigo_barras'],
						'descripcion' => $row_query['descripcion'],
						'presentacion' => $row_query['presentacion'],
						'unidad' => $row_query['unidad'],
						'precio' => $row_query['precio'],
						'status' => $row_query['status'],
						'existencia_actual' => $row_query['existencia_actual'],
						'extra1' => $row_query['extra1']
						);
		}while($row_query = mysql_fetch_assoc($query));
	}
	@mysql_free_result($query);
	@mysql_close($dbissste);
	return $ret;
}


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {

	$orden = 'descripcion';
	$sentido = 'ASC';
	if (isset($_REQUEST["orden"])) {
		$orden = $_REQUEST["orden"];
	}
	if (isset($_REQUEST["sentido"])) {
		if ($_REQUEST["sentido"] == 'DESC') $sentido = 'ASC'; else $sentido = 'DESC';
	}
	
	$datos = getMedicamentosConExistenciasOrden($orden . ' ' . $sentido);
	$TDatos = count($datos);
	
	$out = '<table class="ventana">
          <tr class="tituloVentana">
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.id_medicamento&sentido=' . $sentido . '">Id.</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.descripcion&sentido=' . $sentido . '">Nombre</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.presentacion&sentido=' . $sentido . '">Cant.</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.unidad&sentido=' . $sentido . '">Unidad</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=m.extra1&sentido=' . $sentido . '">Grupo</a></td>
            <td><a href="' . $_SERVER['PHP_SELF'] . '?orden=e.existencia_actual&sentido=' . $sentido . '">Existencias</a></td>
          </tr>';
	
	for ($i=0;$i<$TDatos;$i++) {
		$out.= "<tr class='filas'><td>" . $datos[$i]['id_medicamento'] . "</td><td>" . htmlentities($datos[$i]['descripcion']) . "</td><td>" . $datos[$i]['presentacion'] . "</td><td>" . $datos[$i]['unidad'] . "</td><td>" . $tipoMedicamento[$datos[$i]['extra1']] . "</td><td>" . $datos[$i]['existencia_actual'] . "</td></tr>";
	}
//	$out.= "<tr><td align=\"center\" colspan=\"6\"><br><br><input name=\"agregar\" id=\"agregar\" type=\"button\" value=\"Agregar Medicamento\" class=\"botones\" onClick=\"javascript: location.href= 'medicamentos_agregar.php'\" /></td></tr>";
	$out.= "</table>";


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración - Control de Recetas</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Control de Recetas&nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center"><?php echo $out; ?>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php 
} ?>