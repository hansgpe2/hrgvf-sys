<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	print("No haz ingresado al sistema");
} else {
	if (isset($_GET['id_derecho'])) {
		$medi = getMedicamentosParaDerechohabientes();
		$tMedi = count($medi);		
		$inputs_medicamentos = '<p align="right"><input type="checkbox" id="todos" name="todos" onchange="javascript: todos_medicamentos()" /> Todos</p><table border="0" cellpadding="0" cellspacing="10" width="100%" style="font-size:10px;">';
		$columna = 0;
		for ($i=0; $i<$tMedi; $i++) {
			if ($columna == 3) {
				$columna = 0;
				$inputs_medicamentos .= '</tr>';
			}
			if ($columna == 0) $inputs_medicamentos .= '<tr>';
			$tieneMedicamento = tieneDerechoMedicamento($_GET['id_derecho'], $medi[$i]["id_medicamento"]);
			$checked = '';
			if ($tieneMedicamento) $checked = ' checked="checked" ';
			$inputs_medicamentos .= '<td><input type="checkbox" id="med_y' . $medi[$i]["id_medicamento"] . '" name="med_y' . $medi[$i]["id_medicamento"] . '"  value="' . $medi[$i]["id_medicamento"] . '"'. $checked . '>' . $medi[$i]["descripcion"] . '</td>';
			$columna++;
		}
		$inputs_medicamentos .= '</table>';
		print($inputs_medicamentos);
	} else {
	echo "no existe la variable id_derecho";
	}

}
?>