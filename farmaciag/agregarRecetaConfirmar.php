<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<?php

function statusParaReceta($id_medico, $id_derecho, $id_medicamento, $cantidad, $n_med) {
	$ret = array(
		'status' => '0', // 0=si se puede usar el medicamento para la receta, 1=error tiene tratamiento, 2=error no hay medicamento, 9=error no hay folios
		'txt' => '',
		'extra' => ''
	);
	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
	if ($folio["folio_actual"] < $folio["folio_final"]) { // si está el folio actual dentro de los folios disponibles entonces si podríamos agregar la receta
		if (tieneTratamiento($id_medicamento, $id_derecho)) {
			$ret['status'] = 1;
			$ret['txt'] = '<span style="color:#ff0000">- Tiene tratamiento vigente del medicamento ' . $n_med . '</span><br>';
		}
		if (!hayMedicamento($id_medicamento, $cantidad)) { 
			$ret['status'] = 2;
			$ret['txt'] = '<span style="color:#ff0000">- El medicamento ' . $n_med . ' no tiene existencias y se va a generar como vale</span><br>';
			$ret['extra'] = 'N';
		}
	} else { // mensaje de error por falta de folios
		$ret['status'] = 9;
		$ret['txt'] = '<span style="color:#ff0000">- No se generó Receta para Medicamento ' . $n_med . ' por falta de folios por parte del médico, pida se le asignen mas folios</span><br>';
	}
	return $ret;	
}

function agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, $entidad_federativa, $clave_uni_med, $diagnostico, $ced_prof, $id_usuario, $tipo_receta, $id_medicamento, $cantidad, $unidad, $dias, $diasTratamiento, $indicaciones, $extra, $n_med) {
	$ret = array(
		'id_receta' => '',
		'txt' => '',
		'extra' => ''
	);
	$folio = getFolioActual($id_medico); // traemos los valores del folio del dr
	$folioSiguiente = $folio["folio_actual"] + 1;
	$fecha = date("Ymd");
	$hora = date("H:i:s");
	$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','" . $id_medico . "','" . $id_servicio . "','" . $id_derecho . "','" . $fecha . "','" . $hora . "','','" . $entidad_federativa . "','" . $clave_uni_med . "','" . $diagnostico . "','" . $ced_prof . "','" . $id_usuario . "','0','1','" . $tipo_receta . "');";
	$res = ejecutarSQL($sql);
	$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], $id_medico, $id_derecho);
	$ret['id_receta'] = $id_receta;
	$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$sql = "UPDATE recetas SET codigo_barras='" . ponerCeros($id_receta,12) . "' WHERE id_receta='" . $id_receta . "' LIMIT 1";
	$res = ejecutarSQL($sql);
	$ret['txt'] .= '- Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
	$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamento . "','" . $cantidad . "','" . $unidad . "','" . (int)$dias * (int)$diasTratamiento . "','" . $indicaciones . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql);
	if ($extra == '') {
		restaExistenciasMedicamentos($id_medicamento, $cantidad);
		$ret['txt'] .= '- Medicamento ' . $n_med . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	} else {
		// aquí podriamos restar medicamento pero se iría al - (menos)
		$ret['txt'] .= '- Medicamento SIN EXISTENCIAS ' . $n_med . ' en Receta SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
	}
	return ($ret);
}

function agregar2doMedicamento ($id_receta, $id_medicamento, $cantidad, $unidad, $dias, $diasTratamiento, $indicaciones, $extra, $n_med) {
	$ret = '';
	$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $id_medicamento . "','" . $cantidad . "','" . $unidad . "','" . (int)$dias * (int)$diasTratamiento . "','" . $indicaciones . "','" . $extra . "','','','');";
	$res = ejecutarSQL($sql);
	if ($extra == '') {
		restaExistenciasMedicamentos($id_medicamento, $cantidad);
		$ret .= '- Medicamento ' . $n_med . ' agregado correctamente<br>';
	} else {
		// aquí podriamos restar medicamento pero se iría al - (menos)
		$ret .= '- Medicamento SIN EXISTENCIAS ' . $n_med . '  agregado correctamente<br>';
	}
	return $ret;
}

if ((isset($_GET["cantidades"])) && (isset($_GET["unidades"])) && (isset($_GET["dias"])) && (isset($_GET["medicamentos"])) && (isset($_GET["indicaciones"])) && (isset($_GET["id_derecho"])) && (isset($_GET["serie"])) && (isset($_GET["folio"])) && (isset($_GET["grupos"]))) {
	$cant = $_GET["cantidades"];
	$unid= $_GET["unidades"];
	$di = $_GET["dias"];
	$diT = $_GET["diasTratamiento"];
	$medi = $_GET["medicamentos"];
	$indi = $_GET["indicaciones"];
	$nuMed = $_GET["numerosMed"];
	$tipo_receta = $_GET["tipo_receta"];
//	$folio = $_GET["folio"];
	$grps = $_GET["grupos"];
	$diagnostico = $_GET["diagnostico"];
	$id_derecho = $_GET["id_derecho"];
	$serie = $_GET["serie"];
	$id_medico = $_SESSION["idDr"];

	$medicamentos = explode(",",$medi);
	$tmedicamentos = count($medicamentos);
	$ret = '';
	$id_receta = '';
	$id_receta2 = 0;
	$medico = getMedicoXid($id_medico);
	$id_servicio = regresarIdServicio($id_medico);
	switch ($tmedicamentos) {
		case '1': // un solo medicamento se recibió
			$status = statusParaReceta($id_medico, $id_derecho, $medi, $cant, $nuMed);
			switch ($status['status']) {
				case 0 : // todo bien para que se genere la receta
				case 2 : // se genera receta pero se genera vale porque no hay existencias del medicamento
					$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medi, $cant, $unid, $di, $diT, $indi, $status['extra'], $nuMed);
					$ret = $agregar['txt'];
					$id_receta = $agregar['id_receta'];
					break;
				case 1 : // tiene tratamiento vigiente
				case 9 : // no tiene folios el dr
					$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status['txt'];
					break;
			}
			break;
		case '2': // 2 medicamentos se recibieron
			$cantidades = explode(",",$cant);
			$unidades = explode(",",$unid);
			$dias = explode(",",$di);
			$diasTratamiento = explode(",",$diT);
			$indicaciones = explode(",",$indi);
			$grupos = explode(",",$grps);
			$numerosMed = explode(",",$nuMed);
			if (($grupos[$numerosMed[0]] == "2") || ($grupos[$numerosMed[1]] == "2") || ($grupos[$numerosMed[0]] == "3") || ($grupos[$numerosMed[1]] == "3")) { // si alguno de los medicamentos es de grupo 2 o 3 se deben generar en recetas separadas
			// receta del medicamento 1
				$status = statusParaReceta($id_medico, $id_derecho, $medicamentos[0], $cantidades[0], $numerosMed[0]);
				switch ($status['status']) {
					case 0 : // todo bien para que se genere la receta
					case 2 : // se genera receta pero se genera vale porque no hay existencias del medicamento
							$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
							$ret = $agregar['txt'];
							$id_receta = $agregar['id_receta'];
						break;
					case 1 : // tiene tratamiento vigiente
					case 9 : // no tiene folios el dr
						$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status['txt'];
						break;
				}
			// receta del medicamento 2
				$status = statusParaReceta($id_medico, $id_derecho, $medicamentos[1], $cantidades[1], $numerosMed[1]);
				switch ($status['status']) {
					case 0 : // todo bien para que se genere la receta
					case 2 : // se genera receta pero se genera vale porque no hay existencias del medicamento
							$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
							$ret .= $agregar['txt'];
							$id_receta2 = $agregar['id_receta'];
						break;
					case 1 : // tiene tratamiento vigiente
					case 9 : // no tiene folios el dr
						$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status['txt'];
						break;
				}
			} else { // si son grupo 4 o 1 (que se supone que no tienen del grupo 1) por default hago lo del grupo 4: medicamentos en la misma receta si son con existencia o recetas separadas si uno tiene existencias y otro no
				$status = statusParaReceta($id_medico, $id_derecho, $medicamentos[0], $cantidades[0], $numerosMed[0]);
				switch ($status['status']) {
					case 0 : // todo bien para que se genere la receta
						$status2 = statusParaReceta($id_medico, $id_derecho, $medicamentos[1], $cantidades[1], $numerosMed[1]);
						switch ($status2['status']) {
							case 0: // los 2 tienen existencia
								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$ret = $agregar['txt'];
								$id_receta = $agregar['id_receta'];
								$agregar2do = agregar2doMedicamento($id_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status2['extra'], $numerosMed[1]);
								$ret .= $agregar2do;
								break;
							case 2: // el medicamento 1 tiene existencia pero el 2 no, se generan en receta separada
								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$ret = $agregar['txt'];
								$id_receta = $agregar['id_receta'];
								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
								$ret .= $agregar['txt'];
								$id_receta2 = $agregar['id_receta'];
								break;
							case 1 : // tiene tratamiento vigiente el medicamento 2
							case 9 : // no tiene folios el dr
								$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status2['txt'];
								break;
						}
						break;
					case 2 : // se genera receta pero se genera vale porque no hay existencias del medicamento
						$status2 = statusParaReceta($id_medico, $id_derecho, $medicamentos[1], $cantidades[1], $numerosMed[1]);
						switch ($status2['status']) {
							case 0: // el medicamento 1 no tiene existencia pero el medicamento 2 si, se generan en recetas separadas
								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$ret = $agregar['txt'];
								$id_receta = $agregar['id_receta'];
								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status['extra'], $numerosMed[1]);
								$ret .= $agregar['txt'];
								$id_receta2 = $agregar['id_receta'];
								break;
							case 2: // el medicamento 1 no tiene existencia ni el medicamento 2, se generan en una sola receta
								$agregar = agregarReceta1medicamento ($id_medico, $id_servicio, $id_derecho, ENTIDAD_FEDERATIVA, CLAVE_UNIDAD_MEDICA, $diagnostico, $medico["ced_pro"], $_SESSION["idUsuario"], $tipo_receta, $medicamentos[0], $cantidades[0], $unidades[0], $dias[0], $diasTratamiento[0], $indicaciones[0], $status['extra'], $numerosMed[0]);
								$ret = $agregar['txt'];
								$id_receta = $agregar['id_receta'];
								$agregar2do = agregar2doMedicamento($id_receta, $medicamentos[1], $cantidades[1], $unidades[1], $dias[1], $diasTratamiento[1], $indicaciones[1], $status2['extra'], $numerosMed[1]);
								$ret .= $agregar2do;
								break;
							case 1 : // tiene tratamiento vigiente el medicamento 2
							case 9 : // no tiene folios el dr
								$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status2['txt'];
								break;
						}
						break;
					case 1 : // tiene tratamiento vigiente
					case 9 : // no tiene folios el dr
						$ret .= 'NO SE GENERO LA RECETA POR LO SIGUIENTE:<br />' . $status['txt'];
						break;
				}
			}
			break;
	}

	print($ret . "|" . $id_receta . "|" . $id_receta2);

} else {
	echo '<span style="color:#ff0000">No se pudo agregar la Receta, pongase en contacto con el administrador del sistema</span>';
}
?>
