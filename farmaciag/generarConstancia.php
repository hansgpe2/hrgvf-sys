<!Doctype html>
<html>
    <head>
        <link href="lib/misEstilos.css" rel="stylesheet" type="text/css">
        <script src="lib/arreglos.js"></script>
    <head>
    </head>
    <body>
        <?php
        include('lib/misFunciones.php');
        if (isset($_GET['idHorario'])) {
            $id_horario = $_GET['idHorario'];
            $id_cita = getCita($id_horario, date('Ymd'));
            $derecho = getDatosDerecho($id_cita["id_derecho"]);
            $id_servicio = regresarIdServicio($_SESSION["idDr"]);
            $servicio = getServicioXid($id_servicio);
            $medico = getMedicoXid($_SESSION["idDr"]);
        ?>
        <div align="center">
            <form action="Constancia.php" method="get" target="_blank" id="Constancia" onSubmit="return genConstancia();">
                <table width="800" border="0" class="tablaPrincipal">   
                    <tr>
                        <td colspan="2" class="tituloVentana">Constancia M&eacute;dica</td>

                    </tr>
                    <tr>
                        <td width="300" align="right" class="textosParaInputs">C&eacute;dula</td>
                        <td width="488"><?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo']; ?></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Nombre</td>
                        <td><?php echo $derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']; ?>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Fecha</td>
                        <td><?php echo date("d/m/Y"); ?>&nbsp;
                            <input name="fecha" type="hidden" id="hiddenField3" value="<?php echo date("Ymd"); ?>"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Hora de llegada</td>
                        <td><input type="text" name="hEnt" id="hEnt">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Hora de Salida</td>
                        <td><label for="hSalida"></label>
                            <input type="text" name="hSalida" id="hSalida"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Acompa&ntilde;ante</td>
                        <td><input name="Acomp" type="text" id="Acomp"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Medico</td>
                        <td><?php echo $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']; ?><input name="medico" type="hidden" value="<?php echo $medico['id_medico']; ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input name="idDerecho" type="hidden" id="idDerecho" value="<?php echo $id_cita['id_derecho']; ?>" ><input name="idServicio" type="hidden" id="idServicio" value="<?php echo $id_servicio; ?>" ></td>

                    </tr>
                    <tr>
                        <td>
                           
                    </tr>
            </form>
        </div>
<?php 

        } else if (isset($_GET['id_cita'])) {
            $id_cita = $_GET['id_cita'];
            $cita = getCitaExt($id_cita);
            $derecho = getDatosDerecho($cita["id_derecho"]);
            $servicio = getServicioXid($cita['id_servicio']);
            $medico = getMedicoXid($cita["id_medico"]);
            ?>
        <div align="center">
            <form action="Constancia.php" method="get" target="_blank" id="Constancia" onSubmit="return genConstancia();">
                <table width="800" border="0" class="tablaPrincipal">   
                    <tr>
                        <td colspan="2" class="tituloVentana">Constancia M&eacute;dica</td>

                    </tr>
                    <tr>
                        <td width="300" align="right" class="textosParaInputs">C&eacute;dula</td>
                        <td width="488"><?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo']; ?></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Nombre</td>
                        <td><?php echo $derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']; ?>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Fecha</td>
                        <td><?php echo date("d/m/Y"); ?>&nbsp;
                            <input name="fecha" type="hidden" id="hiddenField3" value="<?php echo date("Ymd"); ?>"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Hora de llegada</td>
                        <td><input type="text" name="hEnt" id="hEnt">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Hora de Salida</td>
                        <td><label for="hSalida"></label>
                            <input type="text" name="hSalida" id="hSalida"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Acompa&ntilde;ante</td>
                        <td><input name="Acomp" type="text" id="Acomp"></td>
                    </tr>
                    <tr>
                        <td align="right" class="textosParaInputs">Medico</td>
                        <td><?php echo $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres']; ?><input name="medico" type="hidden" value="<?php echo $medico['id_medico']; ?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input name="idDerecho" type="hidden" id="idDerecho" value="<?php echo $cita['id_derecho']; ?>" ><input name="idServicio" type="hidden" id="idServicio" value="<?php echo $cita['id_servicio']; ?>" ></td>

                    </tr>
                    <tr>
                        <td>
                            <?php    
        }
                            if ($_SESSION['tipoUsuario'] == 8) {
                                echo "  <input type='button' name='regresar' id='regresar' value='Regresar' class='botones'  onclick='javascript: inicio(\"inicioMedicoEsp.php\");' tabindex='11' />&nbsp;&nbsp;&nbsp;&nbsp;";
                            } else if ($_SESSION['tipoUsuario'] == 4) {
                                echo "<input type='button' name='regresar' id='regresar' value='Regresar' class='botones'  onclick='javascript: inicio(\"inicioMedico.php\");' tabindex='11' />&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                            ?>
                            <input type="submit" class="botones" value="Guardar"></td>
                    </tr>
                </table>
            </form>
        </div>
       

    </body>
</html>