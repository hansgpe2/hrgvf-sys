<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<?php
if (isset($_REQUEST["id_servicio"])) {
	$conceptos = array();
	$hayExistencias = true;
	$ret = '';
	foreach($_REQUEST as $nombre => $valor) {
		if (is_numeric($nombre)) {
			if (!hayMedicamento($nombre, $valor)) {
				$ret .= '<span style="color:#ff0000">- No hay existencias suficientes del medicamento ' . $nombre . '</span><br>';
				$hayExistencias = false;
			}
			$conceptos[$nombre] = $valor;
		}
	}
	if ($hayExistencias) {
		$folio = getFolioActual("0");
		$folioSiguiente = $folio["folio_actual"] + 1;
		$codigo_barras = getCodigoBarras();
		$sql = "INSERT INTO recetas VALUES(NULL,'" . $folio["serie"] . "','" . $folio["folio_actual"] . "','0','" . $_REQUEST["id_servicio"] . "','0','" . date("Ymd") . "','" . date("H:i:s") . "','" . $codigo_barras . "','" . ENTIDAD_FEDERATIVA . "','" . CLAVE_UNIDAD_MEDICA . "','','','" . $_SESSION["idUsuario"] . "','0','1','C');";
		$res = ejecutarSQL($sql);
		$sql = "UPDATE folios SET folio_actual='" . $folioSiguiente . "' WHERE id_folio='" . $folio["id_folio"] . "' LIMIT 1";
		$res = ejecutarSQL($sql);
		$sigCodigo = intval($codigo_barras)+1;
		$sql = "UPDATE codigo_barras SET codigo='" . ponerCeros($sigCodigo,12) . "' LIMIT 1";
		$res = ejecutarSQL($sql);
		$id_receta = getIdRecetaAgregada($folio["serie"], $folio["folio_actual"], "0", "0");
		$ret .= '- Colectivo SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " generada correctamente<br>";
		foreach($conceptos as $nombre => $valor) {
			$sql = "INSERT INTO recetas_conceptos VALUES(NULL,'" . $id_receta . "','" . $nombre . "','" . $valor . "','0','0','','','','','');";
			$res = ejecutarSQL($sql);
			restaExistenciasMedicamentos($nombre, $valor);
			$ret .= '- Medicamento ' . $nombre . ' en Colectivo SERIE: ' . $folio["serie"] . " FOLIO: " . ponerCeros($folio["folio_actual"],7) . " agregado correctamente<br>";
		}
	}
	print($ret . "|" . $id_receta);
	
} else {
	echo '<span style="color:#ff0000">No se pudo agregar el colectivo, pongase en contacto con el administrador del sistema</span>';
}
?>
