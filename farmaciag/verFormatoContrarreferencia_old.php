<?php
session_start();
include_once('lib/misFunciones.php');
$idContra = $_GET['folioContrarrefencia'];
$ref = getContraReferencia1($idContra);
$idDerecho = $ref['id_derecho'];
$doc = $ref['id_medico'];
$derecho = getDatosDerecho($idDerecho);
$medico = getMedicoXid($doc);
$servicio = getServicioXid($ref['id_servicio']);
$unidad = obtenerUnidadMedica($ref['id_unidad']);
$medicinas = ObtenerMedicamentosXReferencia($idContra);
$servicio=getServicioXid($ref['id_servicio']);
$count=  count($medicinas);
if (count($ref) > 0) {
    $evo = $ref['evolucion'];
    $datclin = $ref['resumen'];
    $diag = $ref['diagnostico'];
    $diagRef = $ref['diagnostico_ref'];
    $tratamientos = $ref['tratamiento'];
    $recom = $ref['recomendaciones'];
    $fecha = $ref['fecha_contrarreferencia'];
    $fecha = date("d/m/Y", strtotime($fecha));
    $cont=$ref['status_citas'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>CONTRARREFERENCIA</title>
        <style type="text/css">
            @import url("lib/impresion2.css") print;
        .firmaMedico {
	font-size: 10px;
}
        .firmaMedico {
	font-weight: bold;
}
        .firmaMedico {
	font-size: 12px;
}
        </style>
        <link href="lib/impresion2.css" rel="stylesheet" type="text/css">
    </head>
</head>

<body>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
  <tr>
    <td width="95" align="center"><img src="diseno/logoEncabezado.jpg" alt="" width="90" height="107" /></td>
    <td width="317" align="center" class="encabezado"><p>HOSPITAL REGIONAL </p>
      <p>"DR. VALENTÍN GÓMEZ FARÍAS" <br />
        FORMATO DE CONTRARREFERENCIA</p></td>
    <td width="152"><p><img src="diseno/LogoHRVGF.jpg" alt="" width="141" height="104" /></p></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:solid; border-width:1px;">
      <tr>
            <td><p> <span class="titulos">Folio</span>&nbsp;&nbsp;<?php echo $ref['id_contra']; ?>            
           <br><span class="titulos">CLINICA DE ADSCRIPCIÓN</span><span class="datos"> <?php echo ponerAcentos($unidad['nombre']); ?></span></td>
        </tr>
        <tr>
            <td><p><span class="titulos">LOCALIDAD:</span><span class="datos"> <?php echo ponerAcentos($derecho['municipio'] . "," . $derecho['estado']); ?></span><span class="titulos">&nbsp;&nbsp; FECHA:</span><span class="datos"> <?php echo $fecha; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">CÉDULA</span><span class="datos">: <?php echo $derecho['cedula'] . "/" . $derecho['cedula_tipo'] ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="titulos">NOMBRE:</span><span class="datos"><?php echo ponerAcentos($derecho['ap_p'] . " " . $derecho['ap_m'] . " " . $derecho['nombres']); ?></span>&nbsp;&nbsp;&nbsp; <span class="titulos">SERVICIO QUE ENVIA:</span> <?php echo $servicio; ?><br>
              <?php
if ($cont == 0)
    echo "El paciente  continuar&aacute; su tratamiento con su m&eacute;dico familiar";
else
    echo "El paciente  continuar&aacute; tratamiento con el m&eacute;dico especialista";
?>
          </p></td>
            </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border:solid; border-width:1px;">
        <tr>
            <td><p align="center" class="encabezado">INFORME DEL MÉDICO CONSULTADO</p></td>
        </tr>
        <tr>
            <td class="titulos">RESUMEN DE DATOS CLÍNICOS: </strong></td>
        </tr>
        <tr>
            <td class="datos"><?php echo ponerAcentos($datclin); ?></td>
        </tr>
        <tr>
            <td class="titulos">DIAGNÓSTICOS DE:</td>
        </tr>
        <tr>
            <td><table width="100%" class="tablaDiag">
                    <tr>
                        <td class="titulos">Referencia</td>
                        <td class="titulos">Contrarreferencia</td>
                    </tr>
                    <tr>
                        <td class="datos"><?php echo ponerAcentos($diagRef); ?></td>
                        <td class="datos"><?php echo ponerAcentos($diag); ?></td>
                    </tr>
                </table></td>
        </tr>
        <tr>
            <td class="titulos">SINTESIS DE LA EVOLUCIÓN:</td>
        </tr>
        <tr>
            <td class="datos"><?php echo ponerAcentos($evo); ?></td>
        </tr>
        <tr>
            <td class="titulos">TRATAMIENTO INSTITUIDO:</td>
        </tr>
        <tr>
            <td class="datos"><?php echo ponerAcentos($tratamientos); 
            if($count>0){?>
                <table width="100%" border="0">
                    <tr>
                        <td width="8%" class="encabezado">Clave</td>
                        <td width="18%" class="encabezado">Medicamento</td>
                        <td width="16%" class="encabezado">Tipo Tratamiento</td>
                        <td width="31%" class="encabezado">Tratamiento en Dias</td>
                        <td width="22%" class="encabezado">Cantidad de cajas</td>
                    </tr>
                    <?php
                    $i = 0;
                    while ($i < $count) {
                        $medicina = getDatosMedicamentos($medicinas[$i]['id_medicamento']);
                        ?>
                        <tr>
                        <td><?php echo $medicina['id_medicamento']; ?></td>
                            <td><?php echo substr($medicina['descripcion'], 0, 60); ?></td>
                            <td><?php
                    if ($medicinas[$i]['cronico'] == 1)
                        echo "Critico";
                    else
                        echo "Temporal"
                            ?></td>
                            <td><?php echo $medicinas[$i]['dias']; ?>&nbsp;</td>
                            <td width="5%"><?php echo $medicinas[$i]['cajas']; ?>&nbsp;</td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </table>
            <?php }
            ?>
          <p>&nbsp;</p></td>
        </tr>
        <tr>
            <td class="titulos">RECOMENDACIONES:</td>
        </tr>
        <tr>
            <td class="datos"><?php echo ponerAcentos($recom); ?></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td height="30pt" align="center">_______________________________________________________</td>
        </tr>
        <tr>
            <td align="center" class="firmaMedico"><?php echo $medico['titulo'] . " " . $medico['ap_p'] . " " . $medico['ap_m'] . " " . $medico['nombres'] . "<br /> DGP: " . $medico['ced_pro']."<br>".$servicio; ?></td>
        </tr>
        <tr>
            <td colspan="4" align="center" class="nota">Para Revisar las recetas proporcionadas al paciente<br />
                Dirijase
                a la siguiente Direccion http://192.165.95.30/farmaciag/, ingrese con el usuario visorvisor, contrase&ntilde;a 123456</td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td height="168" colspan="4" align="center" class="nota"><p><strong>INDICACIONES:</strong></p>
              <ol>
                <li>Al derechohabiente contra referido  se deberá entregar por <strong>duplicado </strong>este documento para sus trámites.</li>
                <li>Deberá presentar uno de los originales  en el <strong>Departamento de Referencia y  Contrarreferencia</strong> de esta unidad <strong>Hosp.  Regional Dr. Valentín Gómez Farías.</strong></li>
             
<li>El duplicado se deberá presentarse en <strong>su clínica de adscripción</strong> que lo refirió  a esta unidad en un periodo <strong>no mayor a 15 días hábiles</strong>,  y <strong>solicitar  una cita</strong>  en la cual su <strong>médico familia</strong>r revisará la  Contrarreferencia del médicos de especialidad   y  Solicitara los medicamentos  necesarios para realizar el control y seguimiento necesario en su clínica de  adscripción</li></ol>
<p><br>
</p></td>
        </tr>
    </table>
</body>
</html>
