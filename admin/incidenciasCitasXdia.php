<?php
session_start ();
require_once('../lib/monket-cal-parseAdmin.php');
//require_once("../lib/funcionesAdmin.php");

	if ($_SESSION['IdCon'] != $_GET['idConsultorio']) {
		$_SESSION['IdCon'] = $_GET['idConsultorio'];
		$_SESSION['idServ'] = "-1";
		$_SESSION['idDr'] = "-1";
	}else if ($_SESSION['idServ'] != $_GET['idServicio']) {
//		$_SESSION['IdCon'] = $_GET['idConsultorio'];
		$_SESSION['idServ'] = $_GET['idServicio'];
		$_SESSION['idDr'] = "-1";
	} else if ($_SESSION['idDr'] != $_GET['idMedico']) {
//		$_SESSION['IdCon'] = $_GET['idConsultorio'];
//		$_SESSION['idServ'] = $_GET['idServicio'];
		$_SESSION['idDr'] = $_GET['idMedico'];
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>

<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
	
    <table id="centro" class="centro" width="800" height="499">
    <tr><td valign="top" align="center">
    
                <form id="form">
		        <table border="0" cellpadding="0" cellspacing="0" width="800" class="ventana">
                    <tr>
                      <td class="tituloVentana" height="23" colspan="3">SELECCIONE CONSULTORIO -> SERVICIO -> MEDICO PARA VER SU AGENDA</td>
                    </tr>
        		    <tr><td width="100" align="center"><span class="titulo_seleccion">Consultorio:</span><div id="tituloIzq">
                		<select name="MCon" id="MCon" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesCon($_SESSION['IdCon']); ?>
                		</select>
              		</div></td><td width="250" align="center"><span class="titulo_seleccion">Servicio:</span><div id="tituloCen">
                		<select name="MSer" id="MSer" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesSer($_SESSION['IdCon'],$_SESSION['idServ']); ?>
                		</select>
              		</div></td><td width="450" align="center"><span class="titulo_seleccion">M&eacute;dico:</span><div id="tituloDer"> 
                		<select name="MMed" id="MMed" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesMed($_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']); ?>
                		</select>
              		</div></td></tr>
            	</table>
            	</form>
      <div id="contenido">
<table width="630" border="0" cellspacing="0" cellpadding="0" class="ventana">
<tr>
  <td class="tituloVentana" height="23"><?php echo formatoDia($_GET['getdate'], 'tituloCitasXdia'); ?></td>
</tr>
<tr>
  <td align="center"><br /><form action="incidenciasAgregarHorarios.php" method="post" name="forma" id="forma">

<?php $out = "";
	$out.= "
<table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
	$horario = citasXdia($_GET['getdate'],$_SESSION['idUsuario'],$_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']);
	$citasDelDia = count($horario);
	if ($citasDelDia > 0) {
		for($i=0;$i<$citasDelDia;$i++) {
			$datosCita = citasOcupadasXdia($horario[$i]['id_horario'],$_GET['getdate']);
			$datosDerecho = getDatosDerecho($datosCita['id_derecho']);
			if ($datosCita['id_derecho'] == "-1") { //no hay cita a x hora
				if ($horario[$i]['tipo_cita'] == "0") $claseParaDia = "citaXdiaLIBPRV"; //primera vez libre
				if ($horario[$i]['tipo_cita'] == "1") $claseParaDia = "citaXdiaLIBSUB"; //primera vez libre
				if ($horario[$i]['tipo_cita'] == "2") $claseParaDia = "citaXdiaLIBPRO"; //primera vez libre
				$nombre = "";
				$datos = "";
				$botones = "<a href=\"javascript: agregarCita('" . $horario[$i]['id_horario'] . "','" . $_GET['getdate'] . "','" . formatoHora($horario[$i]['hora_inicio']) . "','" . formatoHora($horario[$i]['hora_fin']) . "');\" title=\"Agregar Cita\"><img src=\"diseno/Symbol-Add.png\" border=\"0\"></a>";
			} else { // hay cita
				$nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
				$datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . $datosDerecho['direccion'];				
				if($horario[$i]['tipo_cita'] == 0) $claseParaDia = "citaXdiaPRV"; 
				if($horario[$i]['tipo_cita'] == 1) $claseParaDia = "citaXdiaSUB"; 
				if($horario[$i]['tipo_cita'] == 2) $claseParaDia = "citaXdiaPRO"; 
			}
			if (($datosCita['status'] > 2) && ($datosCita['status'] < 9)) { // esta de vacaciones, en congreso, etc.
				$nombre = $statusCitas[$datosCita['status']];
				$datos = $datosCita['observaciones'];
				$botones = "";
				$claseParaDia = "citaXdiaNOT"; 
			}
			$hoy = date('Ymd');
			$fecha = $_GET['getdate'];
			if ( $fecha < $hoy) $botones = ""; // si es una fecha anterior a hoy no pone botones
			$out.="
  <tr>
    <td class=\"" . $claseParaDia . "\">
        <table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td width=\"80\" rowspan=\"2\" class=\"citaXdiaHora\">" . formatoHora($horario[$i]['hora_inicio']) . " - " . formatoHora($horario[$i]['hora_fin']) . "</td>
            <td align=\"left\" width=\"540\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
			<td width=\"80\" rowspan=\"2\"><input name=\"bloq" . $horario[$i]['id_horario'] . "\" id=\"bloq" . $horario[$i]['id_horario'] . "\" value=\"" . $horario[$i]['id_horario'] . "-" . $_GET['getdate'] . "\" type=\"checkbox\" value=\"\" /></td>
          </tr>
          <tr>
            <td align=\"left\" class=\"citaXdiaInfo\">" . ponerAcentos($datos) . "</td>
          </tr>
        </table>
    </td>
  </tr>";
		}	
	} else {
		echo '<span class="textosParaInputs">NO HAY CITAS DISPONIBLES PARA ESTE DIA<br><br></span>';
	}
	$out.= "
</table>";
	echo $out;
?>
<a href="javascript: location.href = 'incidencias.php?idConsultorio=<?php echo $_GET['idConsultorio'] ?>&idServicio=<?php echo $_GET['idServicio'] ?>&idMedico=<?php echo $_GET['idMedico'] ?>&getdate=<?php echo $_GET['getdate'] ?>'" title="Regresar a la agenda mensual"><img src="../diseno/flechaIzq.png" alt="Regresar" border="0" /></a><br />
<input class="botones" type="button" name="eliminarIncidencias" id="eliminarIncidencias" value="Eliminar Incidencias en las Horas Seleccionadas" onclick="javascript: eliminarIncidenciaHorario();" />
<input class="botones" type="button" name="programarIncidencias" id="programarIncidencias" value="Programar Incidencias en las Horas Seleccionadas" onclick="javascript: agregarIncidenciaHorario();" />
<br /><div id="estadoEliminando"></div>
</form>
</td>
</tr>
</table>
<p align="center"><table width="500" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:10px;">
<tr>
<td width="35" height="25" class="citaXdiaPRV"></td><td width="129">Citas Primera Vez</td>
<td width="35" class="citaXdiaSUB"></td><td width="138">Citas Subsecuentes</td>
<td width="35" class="citaXdiaPRO"></td><td width="138">Citas Procedimientos</td>
</tr>
</table></p>
      </div>




    </td></tr>
    <tr><td align="center">
      <table width="800">
      <tr><td><div id="menu" style="display:none"><a href="#" title="Reportes" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reportes</a><br><br><br><br>
      			<a href="javascript: mostrarDiv('listaCitasReprogramar');" title="Reprogramar Citas" class="botones_menu"><div id="citasReprogramar"><img src="diseno/reprogramar.png" width="40" height="40" border="0" /></div><br>a Reprogramar</a><br><br>
                <div style="z-index:100; width:500px; height:200px; float:left; overflow:auto; position:absolute; background-color:#EEEEEE; border:solid; border-width:1px; border-color:#005AB3" id="listaCitasReprogramar">
                </div><br><br><br><br><br><br><br><br><br><br><br><br>
                <a href="#" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a>
              </div></td>
      <td align="center">
      <div id="contenido">
      </div>
      </td></tr></table>
    </td></tr></table>
    
    
    
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
