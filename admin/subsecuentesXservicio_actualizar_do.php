<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {

	$cuantos = 0;
	$out = '';
	$out2 = '<ul>';
	$id_usuario = $_POST['id_usuario'];
	$sql = "INSERT INTO usuarios_limitados (id_limite, id_usuario, id_consultorio, id_servicio, id_medico, extra) VALUES ";
	foreach($_POST as $nombre=>$valor) {
		$aNombre = explode("_", $nombre);
		if ($aNombre[0] == "med") {
			$sql .= "('', '" . $id_usuario . "', '" . $aNombre[1] . "', '" . $aNombre[2] . "', '" . $aNombre[3] . "',''),";
			$cuantos++;
			$servicio = getServicioXid($aNombre[2]);
			$medico = getMedicoXid($aNombre[3]);
			$out2 .= '<li>' . $servicio["nombre"] . ' - ' . $medico["ap_p"] . ' ' . $medico["ap_m"] . ' ' . $medico["nombres"] . '</li>';
		}
	}
	$out2 .= '</ul>';
	$sql = substr($sql,0,strlen($sql)-1) . ';';
	$query = "DELETE FROM usuarios_limitados WHERE id_usuario='" . $id_usuario . "';";
	$res = ejecutarSQL($query);
	$usuario = getUsuarioXid($id_usuario);
	if ($res[0] == 0) {// no hay error
		if ($cuantos == 0) { // si no se enviaron checkbox el mensaje es que quedó desbloqueado para todos los servicios
			$out .= 'Debido a que no seleccionaste ningún servicio y/o médico el usuario <b>' . $usuario['nombre'] . '</b> ha quedado <b>DESBLOQUEADO</b> para poder agendar en <b>TODOS LOS SERVICIOS Y MEDICOS</b>';
		} else {
			$out .= 'El usuario <b>' . $usuario['nombre'] . '</b> ha quedado <b>AUTORIZADO</b> para poder agendar SOLO en <b>LOS SERVICIOS Y MEDICOS</b> siguientes:' . $out2;
			$res2 = ejecutarSQL($sql);
		}
	} else { // hay error en el mysql
		echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de eliminar registros del usuario'); history.back();</script>";
	}

/*
	if ((isset($_POST['nombre'])) && (isset($_POST['clave'])) && (strlen(trim($_POST['nombre'])) != "") && (strlen(trim($_POST['clave'])) != "")) {
		$duplica = existeDuplica("SELECT * FROM servicios WHERE (nombre='" . $_POST['nombre'] . "' OR clave='" . $_POST['clave'] . "') AND st='1' AND id_servicio<>'" . $_POST['id_servicio'] . "'");
		if(!$duplica) {
			$query = "UPDATE servicios SET nombre='" . $_POST['nombre'] . "', clave='" . $_POST['clave'] . "', extra1='modificado por " . $_SESSION['idUsuario'] . "' WHERE id_servicio='" . $_POST['id_servicio'] . "' LIMIT 1";
			$res = ejecutarSQL($query);
			if ($res[0] == 0) {// no hay error
				$out = "ok";
			} else { // hay error en el mysql
				echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de modificar el servicio'); history.back();</script>";
			}
		} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Actualmente existe en la base de datos un servicio llamado " . $_POST['nombre'] . " o con clave " . $_POST['clave'] . ". Por favor introduzca un nombre o clave diferente'); history.back();</script>";
		}
	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No existe la variable nombre o clave del servicio o son espacios en blanco'); history.back();</script>";
	}
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td>Actualizar Servicios Autorizados</td>
          </tr>
		  <tr><td align="left"><br>
		      <span class="error"><?php echo $out; ?></span><br>
		  <p align="center">
		      <input name="continuar" id="continuar" type="button" value="Continuar" class="botones" onclick="javascript: location.replace('subsecuentesXservicio.php');" /></p>
		      &nbsp;&nbsp;</td></tr>
        </table>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>