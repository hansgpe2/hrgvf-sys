<?php
session_start ();
require_once("../lib/funcionesAdmin.php");
error_reporting(0);

if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	$medico = getMedicoXid($_GET['id_medico']);
	$serviciosXmedico = getServiciosXmedico($_GET['id_medico']);
	$consultorios = getConsultorios();
	$tConsultorios = count($consultorios);
	$opcionesConsultorios = "";
	$idConsultorio = "";
	$idServicio = "";
	for ($i=0;$i<$tConsultorios;$i++) {
		if ($serviciosXmedico[0]['id_consultorio'] ==  $consultorios[$i]['id_consultorio']) {
			$opcionesConsultorios.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\" selected=\"selected\">" . $consultorios[$i]['nombre'] . "</option>";
			$idConsultorio = $consultorios[$i]['id_consultorio'];
		} else {
			$opcionesConsultorios.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . $consultorios[$i]['nombre'] . "</option>";
		}
	}
	$servicios = getServicios();
	$tServicios = count($servicios);
	$opcionesServicios = "";
	for ($i=0;$i<$tServicios;$i++) {
		if ($serviciosXmedico[0]['id_servicio'] ==  $servicios[$i]['id_servicio']) {
			$opcionesServicios.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\" selected=\"selected\">" . $servicios[$i]['nombre'] . "</option>";
			$idServicio = $servicios[$i]['id_servicio'];
		} else {
			$opcionesServicios.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . $servicios[$i]['nombre'] . "</option>";
		}
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="horarios_modificar2.php" onsubmit="MM_validateForm('hora_entrada','','H','hora_salida','','H','intervalo_citas0','','RisNum','intervalo_citas1','','RisNum','intervalo_citas2','','RisNum');return document.MM_returnValue" method="post" name="forma">
        	<input name="hora_e" type="hidden" id="hora_e" value="<?php echo formatoHora($medico['hora_entrada']) ?>" />
        	<input name="hora_s" type="hidden" id="hora_s" value="<?php echo formatoHora($medico['hora_salida']) ?>" />
        	<input name="intervalo_c0" type="hidden" id="intervalo_c0" value="<?php echo $medico['intervalo_citas0'] ?>" />
        	<input name="intervalo_c1" type="hidden" id="intervalo_c1" value="<?php echo $medico['intervalo_citas1'] ?>" />
        	<input name="intervalo_c2" type="hidden" id="intervalo_c2" value="<?php echo $medico['intervalo_citas2'] ?>" />
        	<input name="id_consultorioA" type="hidden" id="id_consultorioA" value="<?php echo $idConsultorio ?>" />
        	<input name="id_servicioA" type="hidden" id="id_servicioA" value="<?php echo $idServicio ?>" />
        	<input name="id_medico" type="hidden" id="id_medico" value="<?php echo $_GET['id_medico'] ?>" />
    	<table class="ventana">
          <tr class="tituloVentana">
            <td colspan="2">Modificar Horario</td>
          </tr>
		  <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		  <tr><td colspan="2"><?php echo $nombreCompleto ?></td>
		    </tr>
		  <tr>
		    <td>Consultorio:</td>
		    <td><select name="id_consultorio" id="id_consultorio">
            	<?php echo $opcionesConsultorios; ?>
		      </select>
            </td>
          </tr>
		  <tr>
		    <td>Servicio:</td>
		    <td><select name="id_servicio" id="id_servicio">
            	<?php echo $opcionesServicios; ?>
		      </select>
            </td>
		  </tr>
		  <tr><td>Hora de Entrada:</td><td><input name="hora_entrada" type="text" size="10" maxlength="5" id="hora_entrada" value="<?php echo formatoHora($medico['hora_entrada']) ?>" /> 
		  hrs.</td>
		  </tr>
		  <tr><td>Hora de Salida:</td><td><input name="hora_salida" type="text" size="10" maxlength="5" id="hora_salida" value="<?php echo formatoHora($medico['hora_salida']) ?>" /> 
		  hrs.</td>
		  </tr>
		  <tr><td>Intervalo entre citas PRV:</td><td><input name="intervalo_citas0" type="text" size="10" maxlength="2" id="intervalo_citas0" value="<?php echo $medico['intervalo_citas0'] ?>" /> 
		  min.</td>
		  </tr>
		  <tr><td>Intervalo entre citas SUB:</td><td><input name="intervalo_citas1" type="text" size="10" maxlength="2" id="intervalo_citas1" value="<?php echo $medico['intervalo_citas1'] ?>" /> 
		  min.</td>
		  </tr>
		  <tr><td>Intervalo entre citas PRO:</td><td><input name="intervalo_citas2" type="text" size="10" maxlength="3" id="intervalo_citas2" value="<?php echo $medico['intervalo_citas2'] ?>" /> 
		  min.</td>
		  </tr>
		  <tr><td align="center" colspan="2"><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" onclick="" value="Continuar" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>