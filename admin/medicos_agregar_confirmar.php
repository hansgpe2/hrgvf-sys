<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if ((isset($_POST['cedula'])) && (isset($_POST['hora_entrada']))) {
		$duplica = existeDuplica("SELECT * FROM medicos WHERE (cedula='" . $_POST['cedula'] . "' OR n_empleado='" . $_POST['n_empleado'] . "') AND st='1'");
		if (!$duplica) { // if para saber si no existe un medico con la misma cedula o n. de empleado
			if ($_POST['cedula_tipo'] == "10") $tipo = "DR."; else $tipo = "DRA.";
			$query = "INSERT INTO medicos VALUES(NULL,'" . $_POST['cedula'] . "','" . $_POST['cedula_tipo'] . "','" . $tipo . "','" . $_POST['n_empleado'] . "','" . $_POST['ced_pro'] . "','" . $_POST['ap_p'] . "','" . $_POST['ap_m'] . "','" . $_POST['nombre'] . "','" . $_POST['turno'] . "','" . $_POST['telefono'] . "','" . $_POST['direccion'] . "','" . $_POST['tipo_medico'] . "','" . $_POST['id_servicio'] . "','0','" . quitarPuntosHora($_POST['hora_entrada']) . "','" . quitarPuntosHora($_POST['hora_salida']) . "','" . $_POST['intervalo_citas_PRV'] . "','" . $_POST['intervalo_citas_SUB'] . "','" . $_POST['intervalo_citas_PRO'] . "','','" . $_POST['observaciones'] . "','','1')";
			$query = utf8_decode($query);
			$query = quitarAcentos($query);
			$res = ejecutarSQL($query);
			if ($res[0] == 0) {// if para saber si se agrego correctamente el medico
				$id_medico = getMedicoXCedEmp($_POST['cedula'], $_POST['n_empleado']);
				if ($id_medico <> "") { // if para saber si se recuperó correctamente el id_medico del recien agregado
					$duplica2 = existeDuplica("SELECT * FROM servicios_x_consultorio WHERE id_consultorio='" . $_POST['id_consultorio'] . "' AND id_servicio='" . $_POST['id_servicio'] . "' AND id_medico='" . $id_medico . "'");
					if (!$duplica2) { // if para saber si no existe en servicios_x_consultorio un registro con id_consultorio, id_servicio e id_medico iguales a los que se tratan de dar de alta
						$query2 = "INSERT INTO servicios_x_consultorio VALUES ('" . $_POST['id_consultorio'] . "','" . $_POST['id_servicio'] . "','" . $id_medico . "','','');";
						$res2 = ejecutarSQL($query2);
						$query3 = "DELETE FROM horarios WHERE id_consultorio='" . $_POST['id_consultorio'] . "' AND id_servicio='" . $_POST['id_servicio'] . "' AND id_medico='" . $id_medico . "'";
						$res3 = ejecutarSQL($query3);

						$i = 0;
						$tPost = count($_POST)-1;
						foreach($_POST as $nombre_campo => $valor){
							if (($i >= 19) && ($i < $tPost)) {
								$horarios = split("[|]",$valor);
								if ($horarios[3] >= 0) {
									$query4 = "INSERT INTO horarios VALUES (NULL,'" . $_POST['id_consultorio'] . "','" . $_POST['id_servicio'] . "','" . $id_medico . "','" . $horarios[0] . "','" . quitarPuntosHora($horarios[1]) . "','" . quitarPuntosHora($horarios[2]) . "','" . $horarios[3] . "','');";
									$res4 = ejecutarSQL($query4);
								}
							}
						   $i++;
						}
						
					} else {  // else de if para saber si no existe en servicios_x_consultorio un registro con id_consultorio, id_servicio e id_medico iguales a los que se tratan de dar de alta
						echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Actualmente existe en la tabla servicios_x_consultorio un registro con id_consultorio, id_servicio e id_medico iguales'); history.back();</script>";
					}
				} else {  // else de if para saber si se recuperó correctamente el id_medico del recien agregado
					echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No se encontro el medico que se acaba de dar de alta para agregar su horario'); history.back();</script>";
				}
			} else { // else de if para saber si se agrego correctamente el medico
				echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de agregar el usuario'); history.back();</script>";
			}
		} else { // else de if para saber si no existe un medico con la misma cedula o n. de empleado
		echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Actualmente existe en la base de datos un medico con cedula " . $_POST['cedula'] . " o con No. de empleado " . $_POST['n_empleado'] . ".'); history.back();</script>";
		}
	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No existe la variable cedula del medico o hora de entrada'); history.back();</script>";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="consultorios_agregar_confirmar.php" onsubmit="MM_validateForm('nombre','','R');return document.MM_returnValue" method="post" name="forma">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td>Agregar M&eacute;dico</td>
          </tr>
		  <tr><td align="center"><br>
		      <span class="error">"<b><?php echo $_POST['titulo'] . " " . $_POST['ap_p'] . " " . $_POST['ap_m'] . " " .  $_POST['nombre']; ?></b>" se agregó correctamente</span><br>
		  <br>
		      <input name="continuar" id="continuar" type="button" value="Continuar" class="botones" onclick="javascript: location.replace('medicos.php');" />
		      &nbsp;&nbsp;</td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>