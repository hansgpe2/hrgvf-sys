<?php
session_start ();
require_once("../lib/funcionesAdmin.php");

if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if (isset($_POST['id_medico'])) {
		$horarios = getHorariosXmedico($_POST['id_medico']);
		$tHorarios = count($horarios);
		for ($i=0; $i<$tHorarios;$i++) {
			$citas = getCitasXHorario($horarios[$i]['id_horario']);
			$tCitas = count($citas);
			for ($j=0;$j<$tCitas;$j++) {
				$query_cita_eliminada = "INSERT INTO citas_eliminadas VALUES ('" . $citas[$j]['id_cita'] . "','" . $citas[$j]['id_horario'] . "','" . $citas[$j]['id_derecho'] . "','" . $citas[$j]['fecha_cita'] . "','" . $citas[$j]['status'] . "','" . $citas[$j]['observaciones'] . "','" . $citas[$j]['id_usuario'] . "','eliminada por " . $_SESSION['idUsuario'] . "');";
				$res = ejecutarSQL($query_cita_eliminada);
				if ($res[0] == 0) {// no hay error
					$query_cita = "DELETE FROM citas WHERE id_cita='" . $citas[$j]['id_cita'] . "' AND id_horario='" . $citas[$j]['id_horario'] . "' AND id_derecho='" . $citas[$j]['id_derecho'] . "' AND fecha_cita='" . $citas[$j]['fecha_cita'] . "'AND status='" . $citas[$j]['status'] . "'AND observaciones='" . $citas[$j]['observaciones'] . "' LIMIT 1";
					$res2 = ejecutarSQL($query_cita);
				} else { // hay error en el mysql
					echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de eliminar cita del medico'); history.back();</script>";
				}
			}
			
			$query_horario_eliminado = "INSERT INTO horarios_eliminados VALUES ('" . $horarios[$i]['id_horario'] . "','" . $horarios[$i]['id_consultorio'] . "','" . $horarios[$i]['id_servicio'] . "','" . $_POST['id_medico'] . "','" . $horarios[$i]['dia'] . "','" . $horarios[$i]['hora_inicio'] . "','" . $horarios[$i]['hora_fin'] . "','" . $horarios[$i]['tipo_cita'] . "','eliminado por " . $_SESSION['idUsuario'] . "');";
			$res3 = ejecutarSQL($query_horario_eliminado);
			if ($res3[0] == 0) {// no hay error
				$query_horario = "DELETE FROM horarios WHERE id_horario='" . $horarios[$i]['id_horario'] . "' AND id_consultorio='" . $horarios[$i]['id_consultorio'] . "' AND id_servicio='" . $horarios[$i]['id_servicio'] . "' AND id_medico='" . $_POST['id_medico'] . "' AND dia='" . $horarios[$i]['dia'] . "' AND hora_inicio='" . $horarios[$i]['hora_inicio'] . "' AND hora_fin='" . $horarios[$i]['hora_fin'] . "' AND tipo_cita='" . $horarios[$i]['tipo_cita'] . "' LIMIT 1";
				$res4 = ejecutarSQL($query_horario);
			} else { // hay error en el mysql
				echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de eliminar horario del medico'); history.back();</script>";
			}
		}
		$query = "UPDATE medicos SET st='0', extra1='eliminado por " . $_SESSION['idUsuario'] . "' WHERE id_medico='" . $_POST['id_medico'] . "' LIMIT 1";
		$res5 = ejecutarSQL($query);
		if ($res5[0] == 0) {// no hay error
			$out = "ok";
		} else { // hay error en el mysql
			echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de eliminar al medico'); history.back();</script>";
		}
	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No existe la variable id del medico'); history.back();</script>";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="consultorios_agregar_confirmar.php" onsubmit="MM_validateForm('nombre','','R');return document.MM_returnValue" method="post" name="forma">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td>Eliminar M&eacute;dico</td>
          </tr>
		  <tr><td align="center"><br>
		      <span class="error">"<b><?php echo $_POST['ap_p'] . " " . $_POST['ap_m'] . " " .  $_POST['nombre']; ?></b>" se elimin&oacute; correctamente</span><br>
		  <br>
		      <input name="continuar" id="continuar" type="button" value="Continuar" class="botones" onclick="javascript: location.replace('medicos.php');" />
		      &nbsp;&nbsp;</td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>