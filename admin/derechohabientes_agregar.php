<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	$total = count($tipo_cedulas);
	$opcionesTipoCedula = "";
	for ($i=0; $i<$total;$i++) {
		$opcionesTipoCedula.= "<option value=\"" . $tipo_cedulas[$i] . "\">" . $tipo_cedulas[$i] . "</option>";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="derechohabientes_agregar_confirmar.php" onsubmit="MM_validateForm('cedula','','C','n_empleado','','E','ap_p','','R','ap_m','','R','nombre','','R','estado','','R','municipio','','R','fecha_nac','','F');return document.MM_returnValue" method="post" name="forma">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td colspan="2">Agregar Derechohabiente</td>
          </tr>
		  <tr><td align="right">C&eacute;dula:</td><td align="left"><input name="cedula" type="text" size="15" maxlength="10" id="cedula" /></td></tr>
		  <tr><td align="right">Tipo de C&eacute;dula:</td>
		    <td align="left"><select name="cedula_tipo" id="cedula_tipo"><?php echo $opcionesTipoCedula ?>
		      </select>		    </td>
          </tr>
		  <tr><td align="right">Apellido Paterno:</td><td align="left"><input name="ap_p" type="text" size="20" maxlength="20" id="ap_p" /></td></tr>
		  <tr><td align="right">Apellido Materno:</td><td align="left"><input name="ap_m" type="text" size="20" maxlength="20" id="ap_m" /></td></tr>
		  <tr><td align="right">Nombre(s):</td><td align="left"><input name="nombre" type="text" size="20" maxlength="20" id="nombre" /></td></tr>
		  <tr><td align="right">Fecha de Nacimiento:</td><td align="left"><input name="fecha_nac" type="text" size="20" maxlength="10" id="fecha_nac" /></td></tr>
		  <tr><td align="right">Tel&eacute;fono:</td><td align="left"><input name="telefono" type="text" size="15" maxlength="10" id="telefono" /></td></tr>
		  <tr><td align="right">Direcci&oacute;n:</td><td align="left"><input name="direccion" type="text" size="46" maxlength="50" id="direccion" /></td></tr>
          <tr><td align="right">Estado:</td><td align="left"><select name="estado" id="estado" onchange="javascript: cargarMunicipios(this.value,'municipio');"></select></td></tr>
<script language="javascript" type="text/javascript">cargarEstados('estado');</script>
          <tr><td align="right">Municipio:</td><td align="left"><select name="municipio" id="municipio"></select></td></tr>
		  <tr><td align="center" colspan="2"><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" onclick="" value="Agregar Derechohabiente" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>