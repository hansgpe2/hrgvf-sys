<?php session_start ();
	
	if ($_SESSION['IdCon'] != $_GET['idConsultorio']) {
		$_SESSION['IdCon'] = $_GET['idConsultorio'];
		$_SESSION['idServ'] = "-1";
		$_SESSION['idDr'] = "-1";
	}else if ($_SESSION['idServ'] != $_GET['idServicio']) {
//		$_SESSION['IdCon'] = $_GET['idConsultorio'];
		$_SESSION['idServ'] = $_GET['idServicio'];
		$_SESSION['idDr'] = "-1";
	} else if ($_SESSION['idDr'] != $_GET['idMedico']) {
//		$_SESSION['IdCon'] = $_GET['idConsultorio'];
//		$_SESSION['idServ'] = $_GET['idServicio'];
		$_SESSION['idDr'] = $_GET['idMedico'];
	}
	include_once('../lib/funcionesAdmin.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>

<body>
<form id="form">
<table border="0" cellpadding="0" cellspacing="0" width="800" class="ventana">
        <tr>
          <td class="tituloVentana" height="23" colspan="3">SELECCIONE CONSULTORIO -> SERVICIO -> MEDICO PARA VER SU AGENDA</td>
        </tr>
<tr><td width="100" align="center"><span class="titulo_seleccion">Consultorio:</span><div id="tituloIzq">
    <select name="MCon" id="MCon" onchange="MM_jumpMenuAjax('parent',this,0)">
		<?php echo opcionesCon($_SESSION['IdCon']); ?>
    </select>
  </div></td><td width="250" align="center"><span class="titulo_seleccion">Servicio:</span><div id="tituloCen">
    <select name="MSer" id="MSer" onchange="MM_jumpMenuAjax('parent',this,0)">
		<?php echo opcionesSer($_SESSION['IdCon'],$_SESSION['idServ']); ?>
    </select>
  
  </div></td><td width="450" align="center"><span class="titulo_seleccion">M&eacute;dico:</span><div id="tituloDer"> 
    <select name="MMed" id="MMed" onchange="MM_jumpMenuAjax('parent',this,0)">
		<?php echo opcionesMed($_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']); ?>
    </select>
  </div></td></tr></table>
</form>
</body>
</html>
