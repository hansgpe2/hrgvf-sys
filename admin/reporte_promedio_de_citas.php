<?php
error_reporting(E_ERROR|E_WARNING|E_PARSE);
session_start ();
require_once('../lib/monket-cal-parseAdmin.php');
//require_once("../lib/funcionesAdmin.php");
$hoy=date("d-m-Y");
	if ($_SESSION['IdCon'] != $_GET['idConsultorio']) {
		$_SESSION['IdCon'] = $_GET['idConsultorio'];
		$_SESSION['idServ'] = "-1";
		$_SESSION['idDr'] = "-1";
	}else if ($_SESSION['idServ'] != $_GET['idServicio']) {
//		$_SESSION['IdCon'] = $_GET['idConsultorio'];
		$_SESSION['idServ'] = $_GET['idServicio'];
		$_SESSION['idDr'] = "-1";
	} else if ($_SESSION['idDr'] != $_GET['idMedico']) {
//		$_SESSION['IdCon'] = $_GET['idConsultorio'];
//		$_SESSION['idServ'] = $_GET['idServicio'];
		$_SESSION['idDr'] = $_GET['idMedico'];
	}
	$hoy=date("d-m-Y");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>

<?php echo getCalInitHTML();
?>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
<script language="javascript" type="text/javascript" src="../lib/gus_calendar.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
	
    <table id="centro" class="centro" width="800" height="499">
    <tr><td valign="top" align="center">
    
                <form id="form">
		        <table border="0" cellpadding="0" cellspacing="0" width="800" class="ventana">
                    <tr>
                      <td class="tituloVentana" height="23" colspan="3">SELECCIONE CONSULTORIO -> SERVICIO -> MEDICO PARA GENERAR SU REPORTE</td>
                    </tr>
        		    <tr><td width="100" align="center"><span class="titulo_seleccion">Consultorio:</span><div id="tituloIzq">
                		<select name="MCon" id="MCon" onchange="MM_jumpMenu2('parent',this,0,'reporte_promedio_de_citas.php');">
                    	<?php echo opcionesCon($_SESSION['IdCon']); ?>
                		</select>
              		</div></td><td width="250" align="center"><span class="titulo_seleccion">Servicio:</span><div id="tituloCen">
                		<select name="MSer" id="MSer" onchange="MM_jumpMenu2('parent',this,0,'reporte_promedio_de_citas.php');">
                    	<?php echo opcionesSer($_SESSION['IdCon'],$_SESSION['idServ']); ?>
                		</select>
              		</div></td><td width="450" align="center"><span class="titulo_seleccion">M&eacute;dico:</span><div id="tituloDer"> 
                		<select name="MMed" id="MMed" onchange="MM_jumpMenu2('parent',this,0,'reporte_promedio_de_citas.php');">
                    	<?php echo opcionesMed($_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']); ?>
                		</select>
              		</div></td></tr>
            	</table>
                <br /><br /><br />
            	</form>
<form id="datechooser" name="datechooser" method="POST" onsubmit="return validarReporte('reporte_promedio_de_citas_R.php','<?php echo $_SESSION['idUsuario'] ?>','<?php echo $_SESSION['IdCon'] ?>','<?php echo $_SESSION['idServ'] ?>','<?php echo $_SESSION['idDr'] ?>');" action="#">
  <table width="500" border="0" cellspacing="0" cellpadding="0" class="ventana">
    <tr>
      <td class="tituloVentana" height="23">REPORTE DE PROMEDIO DE CITAS</td>
    </tr>
    <tr>
      <td align="left" valign="top"><br />Reporte por: 
        <table width="200">
          <tr>
            <td><label>
              <input type="radio" name="RadioGroup1" value="unidad" id="RadioGroup1_0" />
              Unidad</label></td>
          </tr>
          <tr>
            <td><label>
              <input type="radio" name="RadioGroup1" value="consultorio" id="RadioGroup1_1" />
              Consultorio</label></td>
          </tr>
          <tr>
            <td><label>
              <input type="radio" name="RadioGroup1" value="servicio" id="RadioGroup1_2" />
              Servicio</label></td>
          </tr>
          <tr>
            <td><label>
              <input name="RadioGroup1" type="radio" id="RadioGroup1_3" value="medico" checked="checked" />
              Médico</label></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
    	<td><br />Fecha:<br /><div id="padre"  style="position:relative">de <INPUT type="text" name="date1" id="date1" onfocus="doShow('datechooser1','datechooser','date1')" value="<?php echo $hoy; ?>"><div enabled='false' id="datechooser1"></div><br />&nbsp;&nbsp;a <INPUT type="text" name="date2" id="date2" onfocus="doShow('datechooser2','datechooser','date2')" value="<?php echo $hoy; ?>"><div enabled='false' id="datechooser2"></div>
    	</div><br /><p align="center"><input name="boton" type="submit" value="Generar Reporte" class="botones" id="boton"  /></p><br />
    	</td>
    </tr>
  </table>
</form>



    </td></tr>
    <tr><td align="center">
      <table width="800">
      <tr><td><div id="menu" style="display:none"><a href="#" title="Reportes" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reportes</a><br><br><br><br>
      			<a href="javascript: mostrarDiv('listaCitasReprogramar');" title="Reprogramar Citas" class="botones_menu"><div id="citasReprogramar"><img src="diseno/reprogramar.png" width="40" height="40" border="0" /></div><br>a Reprogramar</a><br><br>
                <div style="z-index:100; width:500px; height:200px; float:left; overflow:auto; position:absolute; background-color:#EEEEEE; border:solid; border-width:1px; border-color:#005AB3" id="listaCitasReprogramar">
                </div><br><br><br><br><br><br><br><br><br><br><br><br>
                <a href="#" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a>
              </div></td>
      <td align="center">
      <div id="contenido">
      </div>
      </td></tr></table>
    </td></tr></table>
    
    
    
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
