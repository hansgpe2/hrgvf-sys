<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if ((isset($_POST['hora_entrada'])) && (isset($_POST['hora_salida']))) {
// mover citas a citas por reprogramar
		$hoy = date('Ymd', strtotime("now"));
		$citasAreprogramar = getCitasAreprogramar($_POST['id_consultorioA'], $_POST['id_servicioA'], $_POST['id_medico'], $hoy);
		$tCitas = count($citasAreprogramar);
		for ($i=0;$i<$tCitas;$i++) {
			$res = ejecutarSQL("INSERT INTO citas_por_reprogramar VALUES('" . $citasAreprogramar[$i]['id_cita'] . "','" . $citasAreprogramar[$i]['id_horario'] . "','" . $citasAreprogramar[$i]['id_derecho'] . "','" . $citasAreprogramar[$i]['fecha_cita'] . "','0','" . $citasAreprogramar[$i]['observaciones'] . "','" . $citasAreprogramar[$i]['id_usuario'] . "','a reprogramar por " . $_SESSION['idUsuario'] . "');");
			if ($res[0] == 0) {// if para saber si se agrego correctamente la cita a reprogramar
				$res2 = ejecutarSQL("DELETE FROM citas WHERE id_cita='" . $citasAreprogramar[$i]['id_cita'] . "' LIMIT 1");
				if ($res2[0] > 0) {// si no se pudo eliminar la cita
					echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No se pudo eliminar la cita de CITAS a reprogramar'); history.back();</script>";
				}
			} else {
				echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No se pudo agregar la cita a reprogramar'); history.back();</script>";
			}
		}
//  mover horarios a horarios eliminidados		
		$horarios = getHorariosXmedico($_POST['id_medico']);
		$tHorarios = count($horarios);
		for ($i=0; $i<$tHorarios;$i++) {
			$query_horario_eliminado = "INSERT INTO horarios_eliminados VALUES ('" . $horarios[$i]['id_horario'] . "','" . $horarios[$i]['id_consultorio'] . "','" . $horarios[$i]['id_servicio'] . "','" . $_POST['id_medico'] . "','" . $horarios[$i]['dia'] . "','" . $horarios[$i]['hora_inicio'] . "','" . $horarios[$i]['hora_fin'] . "','" . $horarios[$i]['tipo_cita'] . "','eliminado al mod horario por " . $_SESSION['idUsuario'] . "');";
			$res3 = ejecutarSQL($query_horario_eliminado);
			if ($res3[0] == 0) {// no hay error
				$query_horario = "DELETE FROM horarios WHERE id_horario='" . $horarios[$i]['id_horario'] . "' AND id_consultorio='" . $horarios[$i]['id_consultorio'] . "' AND id_servicio='" . $horarios[$i]['id_servicio'] . "' AND id_medico='" . $_POST['id_medico'] . "' AND dia='" . $horarios[$i]['dia'] . "' AND hora_inicio='" . $horarios[$i]['hora_inicio'] . "' AND hora_fin='" . $horarios[$i]['hora_fin'] . "' AND tipo_cita='" . $horarios[$i]['tipo_cita'] . "' LIMIT 1";
				$res4 = ejecutarSQL($query_horario);
			} else { // hay error en el mysql
				echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de eliminar horario del medico'); history.back();</script>";
			}
		}

// modificar hora entrada, salida e intervalo a medico		
		$query = "UPDATE medicos SET hora_entrada='" . quitarPuntosHora($_POST['hora_entrada']) . "', hora_salida='" . quitarPuntosHora($_POST['hora_salida']) . "', intervalo_citas0='" . $_POST['intervalo_citas0'] . "', intervalo_citas1='" . $_POST['intervalo_citas1'] . "', intervalo_citas2='" . $_POST['intervalo_citas2'] . "' WHERE id_medico='" . $_POST['id_medico'] . "' LIMIT 1";
		$res5 = ejecutarSQL($query);
		if ($res5[0] == 0) {// no hay error
			$out = "ok";
		} else { // hay error en el mysql
			echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de eliminar al medico'); history.back();</script>";
		}

//		print_r($_POST) ."<br>";
//		print_r($citasAreprogramar);

// agregar nuevo horario
		$i = 0;
		$tPost = count($_POST)-1;
		foreach($_POST as $nombre_campo => $valor){
			if (($i >= 10) && ($i < $tPost)) {
				$horarios = split("[|]",$valor);
				if ($horarios[3] >= 0) {
					$query4 = "INSERT INTO horarios VALUES (NULL,'" . $_POST['id_consultorio'] . "','" . $_POST['id_servicio'] . "','" . $_POST['id_medico'] . "','" . $horarios[0] . "','" . quitarPuntosHora($horarios[1]) . "','" . quitarPuntosHora($horarios[2]) . "','" . $horarios[3] . "','');";
					$res4 = ejecutarSQL($query4);
//echo $query4 ."<br>";
				}
			}
		   $i++;
		}
		
// eliminar anterior y asignar nuevo consultorio y servicio al dr
	$query2 = "DELETE FROM servicios_x_consultorio WHERE id_consultorio='" . $_POST['id_consultorioA'] . "' AND id_servicio='" . $_POST['id_servicioA'] . "' AND id_medico='" . $_POST['id_medico'] . "' LIMIT 1";
	$res2 = ejecutarSQL($query2);
	$query2 = "INSERT INTO servicios_x_consultorio VALUES ('" . $_POST['id_consultorio'] . "','" . $_POST['id_servicio'] . "','" . $_POST['id_medico'] . "','','');";
	$res2 = ejecutarSQL($query2);
			
	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No existe la variable hora de entrada u hora de salida'); history.back();</script>";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="consultorios_agregar_confirmar.php" onsubmit="MM_validateForm('nombre','','R');return document.MM_returnValue" method="post" name="forma">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td>Modificar horario</td>
          </tr>
		  <tr><td align="center"><br>
		      <span class="error">El horario se modific&oacute; correctamente<br>Las citas del horario anterior han quedado a reprogramaci&oacute;n</span><br>
		  <br>
		      <input name="continuar" id="continuar" type="button" value="Continuar" class="botones" onclick="javascript: location.replace('horarios.php');" />
		      &nbsp;&nbsp;</td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>