<?php
session_start ();
require_once("../lib/funcionesAdmin.php");
error_reporting(0);


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
//	if (($_POST['hora_e'] == $_POST['hora_entrada']) && ($_POST['hora_s'] == $_POST['hora_salida']) && ($_POST['intervalo_c0'] == $_POST['intervalo_citas0']) && ($_POST['intervalo_c1'] == $_POST['intervalo_citas1']) && ($_POST['intervalo_c2'] == $_POST['intervalo_citas2'])) {

		 // si no se cambiaron hora de entrada, ni salida ni intervalo de citas. Se debe cargar horario actual para su modificacion
		$tipo_modificacion = "parcial";
		$instrucciones = "SE HA CARGADO EL HORARIO ACTUAL DEL MEDICO PARA QUE PUEDAS CAMBIAR ALGUN TIPO DE CITA DE X HORA Y DIA.<br>LAS CITAS QUE RESULTEN AFACTADAS QUEDARAN A REPROGRAMACION";
		$horario = array();
		$horario[0] = "";
		for ($i=1;$i<8;$i++) {
			$hor = 1;	

			$hora_actual = $_POST['hora_entrada'];
			$totalHorarios = 0;
			while (strtotime($hora_actual . " + " . $_POST['intervalo_citas0'] . " minutes") <= strtotime($_POST['hora_salida'])) {
				$hora_actual = date("H:i",strtotime($hora_actual . " + " . $_POST['intervalo_citas0'] . " minutes"));
				$totalHorarios++;
			}
			$hora_actual = $_POST['hora_entrada'];
			while (strtotime($hora_actual . " + " . $_POST['intervalo_citas0'] . " minutes") <= strtotime($_POST['hora_salida'])) {
				$hora_temp = $hora_actual;
				$hora_actual = date("H:i",strtotime($hora_actual . " + " . $_POST['intervalo_citas0'] . " minutes"));
				if ($i == 7) {
					$dia = diaSemana(0);
				} else {
					$dia = diaSemana($i);
				}
				
				$hora_i = quitarPuntosHora($hora_temp);
				$hora_f = quitarPuntosHora($hora_actual);
				if ($i == 7) {
					$dia_s = diaSemana(0);
				} else {
					$dia_s = diaSemana($i);
				}
				$id_cons = $_POST['id_consultorioA'];
				$id_serv = $_POST['id_servicioA'];
				$id_medi = $_POST['id_medico'];
				$query_query = "SELECT * FROM horarios WHERE id_consultorio='" . $id_cons ."' AND id_servicio='" . $id_serv . "' AND id_medico='" . $id_medi . "' AND dia='" . $dia_s . "' AND hora_inicio='" . $hora_i . "' AND hora_fin='" . $hora_f . "' LIMIT 1";
				$horarioBD = getHorarioParaModificar($query_query);
				switch ($horarioBD['tipo_cita']) {
					case '0':
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|0\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|-1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
						break;
					case '1':
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|0\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|1\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|-1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
						break;
					case '2':
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|0\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|2\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|-1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
						break;
					default:
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|0\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|-1\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
						break;
				}
				$hor++;
			}
			
			if (strtotime($hora_actual) < strtotime($_POST['hora_salida'])) {
				$hora_i = quitarPuntosHora($hora_actual);
				$hora_f = quitarPuntosHora($_POST['hora_salida']);
				$dia_s = diaSemana($i);
				$id_cons = $_POST['id_consultorio'];
				$id_serv = $_POST['id_servicio'];
				$id_medi = $_POST['id_medico'];
				$query_query = "SELECT * FROM horarios WHERE id_consultorio='" . $id_cons ."' AND id_servicio='" . $id_serv . "' AND id_medico='" . $id_medi . "' AND dia='" . $dia_s . "' AND hora_inicio='" . $hora_i . "' AND hora_fin='" . $hora_f . "' LIMIT 1";
				$horarioBD = getHorarioParaModificar($query_query);
				switch ($horarioBD['tipo_cita']) {
					case '0':
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|0\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|-1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
						break;
					case '1':
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|0\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|1\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|-1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
						break;
					case '2':
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|0\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|2\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|-1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
						break;
					default:
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|0\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|-1\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
						break;
				}

			}
		}
/*	} else { // si cambio la hora entrada, salida o intervalo de citas, se carga un horario en blanco para su llenado y se tendra que pasar a reprogramacion todas las citas pendientes
		$tipo_modificacion = "total";
		$instrucciones = "EL HORARIO HA CAMBIADO YA SEA EN SU HORA DE ENTRADA, SALIDA O EL INTERVALO DE SUS CITAS.<br>LAS CITAS AGENDADAS DE FECHAS POSTERIORES A HOY QUEDARAN A REPROGRAMACION";
		$horario = array();
		$horario[0] = "";
		for ($i=1;$i<6;$i++) {
			$hor = 1;	
			$hora_actual = $_POST['hora_entrada'];
			$totalHorarios = 0;
			while (strtotime($hora_actual . " + " . $_POST['intervalo_citas0'] . " minutes") <= strtotime($_POST['hora_salida'])) {
				$hora_actual = date("H:i",strtotime($hora_actual . " + " . $_POST['intervalo_citas0'] . " minutes"));
				$totalHorarios++;
			}
			$hora_actual = $_POST['hora_entrada'];
			while (strtotime($hora_actual . " + " . $_POST['intervalo_citas0'] . " minutes") <= strtotime($_POST['hora_salida'])) {
				$hora_temp = $hora_actual;
				$hora_actual = date("H:i",strtotime($hora_actual . " + " . $_POST['intervalo_citas0'] . " minutes"));
				$dia = diaSemana($i);
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|0\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|-1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
				$hor++;
			}
			
			if (strtotime($hora_actual) < strtotime($_POST['hora_salida'])) {
						$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|0\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento ";
						$horario[$i] .= "<input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|-1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas0'] . "','" . $_POST['intervalo_citas1'] . "','" . $_POST['intervalo_citas2'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
			}
		}
	}
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
<script src="../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="horarios_modificar_confirmar.php" onsubmit="MM_validateForm('cedula','','C','ap_p','','R','ap_m','','R','nombre','','R','hora_entrada','','H','hora_salida','','H');return document.MM_returnValue" method="post" name="forma">
        <input name="hora_e" type="hidden" id="hora_e" value="<?php echo $_POST['hora_e']; ?>" />
        <input name="hora_s" type="hidden" id="hora_s" value="<?php echo $_POST['hora_s']; ?>" />
        <input name="intervalo_c0" type="hidden" id="intervalo_c0" value="<?php echo $_POST['intervalo_c0']; ?>" />
        <input name="id_consultorioA" type="hidden" id="id_consultorioA" value="<?php echo $_POST['id_consultorioA']; ?>" />
        <input name="id_servicioA" type="hidden" id="id_servicioA" value="<?php echo $_POST['id_servicioA']; ?>" />
        <input name="id_consultorio" type="hidden" id="id_consultorio" value="<?php echo $_POST['id_consultorio']; ?>" />
        <input name="id_servicio" type="hidden" id="id_servicio" value="<?php echo $_POST['id_servicio']; ?>" />
        <input name="id_medico" type="hidden" id="id_medico" value="<?php echo $_POST['id_medico']; ?>" />
        <input name="hora_entrada" type="hidden" id="hora_entrada" value="<?php echo $_POST['hora_entrada']; ?>" />
        <input name="hora_salida" type="hidden" id="hora_salida" value="<?php echo $_POST['hora_salida']; ?>" />
        <input name="intervalo_citas0" type="hidden" id="intervalo_citas0" value="<?php echo $_POST['intervalo_citas0']; ?>" />
        <input name="intervalo_citas1" type="hidden" id="intervalo_citas1" value="<?php echo $_POST['intervalo_citas1']; ?>" />
        <input name="intervalo_citas2" type="hidden" id="intervalo_citas2" value="<?php echo $_POST['intervalo_citas2']; ?>" />
        <input name="tipo_modificacion" type="hidden" id="tipo_modificacion" value="<?php echo $tipo_modificacion; ?>" />
        <table class="ventana">
          <tr class="tituloVentana">
            <td width="787">Modificar Horario</td>
          </tr>
          <tr><td class="error"><?php echo $instrucciones ?></td></tr>
		  <tr><td align="center"><div id="TabbedPanels1" class="TabbedPanels">
		    <ul class="TabbedPanelsTabGroup">
		      <li class="TabbedPanelsTab" tabindex="0">LUNES</li>
		      <li class="TabbedPanelsTab" tabindex="0">MARTES</li>
		      <li class="TabbedPanelsTab" tabindex="0">MIERCOLES</li>
		      <li class="TabbedPanelsTab" tabindex="0">JUEVES</li>
		      <li class="TabbedPanelsTab" tabindex="0">VIERNES</li>
		      <li class="TabbedPanelsTab" tabindex="0">SABADO</li>
		      <li class="TabbedPanelsTab" tabindex="0">DOMINGO</li>
		    </ul>
		    <div class="TabbedPanelsContentGroup">
		      <div id="LUNES" class="TabbedPanelsContent"><?php echo $horario[1]; ?></div>
		      <div id="MARTES" class="TabbedPanelsContent"><?php echo $horario[2]; ?></div>
		      <div id="MIERCOLES" class="TabbedPanelsContent"><?php echo $horario[3]; ?></div>
		      <div id="JUEVES" class="TabbedPanelsContent"><?php echo $horario[4]; ?></div>
		      <div id="VIERNES" class="TabbedPanelsContent"><?php echo $horario[5]; ?></div>
		      <div id="SABADO" class="TabbedPanelsContent"><?php echo $horario[6]; ?></div>
		      <div id="DOMINGO" class="TabbedPanelsContent"><?php echo $horario[7]; ?></div>
		    </div>
		    </div>
		    <br><br><br><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" onclick="" value="Continuar" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
//-->
</script>
</body>
</html>
<?php } ?>