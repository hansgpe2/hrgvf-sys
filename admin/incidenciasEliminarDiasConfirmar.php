<?php session_start ();
include_once('../lib/funcionesAdmin.php');

if 	(strlen($_GET['dias_incidencias']) < 1) {
	echo "<script languague=\"javascript\">alert('Selecciona al menos un día para programar incidencias'); history.back();</script>";
} else {

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
	
    <table id="centro" class="centro" width="800" height="499">
    <tr><td valign="top" align="center">
    
                <form id="form">
		        <table border="0" cellpadding="0" cellspacing="0" width="800" class="ventana">
                    <tr>
                      <td class="tituloVentana" height="23" colspan="3">SELECCIONE CONSULTORIO -> SERVICIO -> MEDICO PARA VER SU AGENDA</td>
                    </tr>
        		    <tr><td width="100" align="center"><span class="titulo_seleccion">Consultorio:</span><div id="tituloIzq">
                		<select name="MCon" id="MCon" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesCon($_SESSION['IdCon']); ?>
                		</select>
              		</div></td><td width="250" align="center"><span class="titulo_seleccion">Servicio:</span><div id="tituloCen">
                		<select name="MSer" id="MSer" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesSer($_SESSION['IdCon'],$_SESSION['idServ']); ?>
                		</select>
              		</div></td><td width="450" align="center"><span class="titulo_seleccion">M&eacute;dico:</span><div id="tituloDer"> 
                		<select name="MMed" id="MMed" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesMed($_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']); ?>
                		</select>
              		</div></td></tr>
            	</table>
            	</form>
      <div id="contenido">
          <table width="550" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td class="tituloVentana" height="23" colspan="2">ELIMINAR INCIDENCIAS A UNO O MAS DIAS</td>
            </tr>
            <tr>
              <td align="center">
              
<form id="formaCita" method="POST" action="javascript: validarAgregarIncidencia();">
<?php 

$dias = explode("|",$_GET['dias_incidencias']);
$tDias = count($dias);
if ($tDias > 1) {
	for ($i=0;$i<$tDias;$i++) {
		$incidencias = delIncidenciasXdia($_SESSION['IdCon'], $_SESSION['idServ'], $_SESSION['idDr'],$dias[$i], $_GET["observaciones"]);
	}
} else {
		$incidencias = delIncidenciasXdia($_SESSION['IdCon'], $_SESSION['idServ'], $_SESSION['idDr'],$_GET['dias_incidencias'], $_GET["observaciones"]);
}

?>

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" colspan="2" align="center">SE ELIMINARON CORRECTAMENTE LAS INCIDENCIAS</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Continuar" class="botones" onclick="javascript: location.href = 'incidencias.php?idConsultorio=<?php echo $_SESSION['IdCon'] ?>&idServicio=<?php echo $_SESSION['idServ'] ?>&idMedico=<?php echo $_SESSION['idDr'] ?>'" />
  &nbsp;&nbsp;&nbsp;&nbsp;
  <br /><br /></td>
  </tr>
</table>
</form>
              
              
              
              </td>
            </tr>
          </table>
      </div>




    </td></tr>
    <tr><td align="center">
      <table width="800">
      <tr><td><div id="menu" style="display:none"><a href="#" title="Reportes" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reportes</a><br><br><br><br>
      			<a href="javascript: mostrarDiv('listaCitasReprogramar');" title="Reprogramar Citas" class="botones_menu"><div id="citasReprogramar"><img src="diseno/reprogramar.png" width="40" height="40" border="0" /></div><br>a Reprogramar</a><br><br>
                <div style="z-index:100; width:500px; height:200px; float:left; overflow:auto; position:absolute; background-color:#EEEEEE; border:solid; border-width:1px; border-color:#005AB3" id="listaCitasReprogramar">
                </div><br><br><br><br><br><br><br><br><br><br><br><br>
                <a href="#" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a>
              </div></td>
      <td align="center">
      <div id="contenido">
      </div>
      </td></tr></table>
    </td></tr></table>
    
    
    
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>



</body>
</html>
<?php }
?>