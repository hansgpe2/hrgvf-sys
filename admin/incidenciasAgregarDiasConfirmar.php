<?php session_start ();
include_once('../lib/funcionesAdmin.php');

if 	(strlen($_GET['dias_incidencias']) < 1) {
	echo "<script languague=\"javascript\">alert('Selecciona al menos un día para programar incidencias'); history.back();</script>";
} else {

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
	
    <table id="centro" class="centro" width="800" height="499">
    <tr><td valign="top" align="center">
    
                <form id="form">
		        <table border="0" cellpadding="0" cellspacing="0" width="800" class="ventana">
                    <tr>
                      <td class="tituloVentana" height="23" colspan="3">SELECCIONE CONSULTORIO -> SERVICIO -> MEDICO PARA VER SU AGENDA</td>
                    </tr>
        		    <tr><td width="100" align="center"><span class="titulo_seleccion">Consultorio:</span><div id="tituloIzq">
                		<select name="MCon" id="MCon" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesCon($_SESSION['IdCon']); ?>
                		</select>
              		</div></td><td width="250" align="center"><span class="titulo_seleccion">Servicio:</span><div id="tituloCen">
                		<select name="MSer" id="MSer" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesSer($_SESSION['IdCon'],$_SESSION['idServ']); ?>
                		</select>
              		</div></td><td width="450" align="center"><span class="titulo_seleccion">M&eacute;dico:</span><div id="tituloDer"> 
                		<select name="MMed" id="MMed" onchange="MM_jumpMenu('parent',this,0)">
                    	<?php echo opcionesMed($_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']); ?>
                		</select>
              		</div></td></tr>
            	</table>
            	</form>
      <div id="contenido">
          <table width="550" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td class="tituloVentana" height="23" colspan="2">PROGRAMAR INCIDENCIAS A UNO O MAS DIAS</td>
            </tr>
            <tr>
              <td align="center">
              
<?php $dias = explode("|",$_GET['dias_incidencias']);
$tDias = count($dias);
$out = '';
for ($i=0;$i<$tDias;$i++) {
	$horario = citasXdia($dias[$i],$_SESSION['idUsuario'],$_SESSION['IdCon'],$_SESSION['idServ'],$_SESSION['idDr']);
	$citasDelDia = count($horario);
	if ($citasDelDia > 0) {
		for($j=0;$j<$citasDelDia;$j++) {
			$datosCita = citasOcupadasXdia($horario[$j]['id_horario'],$dias[$i]);
			if ($datosCita['id_derecho'] == "-1") { //no hay cita a x hora
				$out .= 'Se agregró incidencia a la cita: día - ' . $dias[$i] . ', ' .$horario[$j]['hora_inicio'] . ' ' .$horario[$j]['hora_fin'] .  '<br>';
				$query_query = "INSERT INTO citas VALUES (NULL,'" .  $horario[$j]['id_horario'] . "','-1','" . $dias[$i] . "','" . $_GET['tipo_incidencia'] . "','" . $_GET['observaciones'] . "','" . $_SESSION['idUsuario'] . "','','','');";
				$res = ejecutarSQL($query_query);			
			} else { // hay cita y tenemos que mandarla para reprogramacion
				$out .= 'Se agregró incidencia a la cita: día - ' . $dias[$i] . ', ' . $horario[$j]['dia'] . ' ' .$horario[$j]['hora_inicio'] . ' ' .$horario[$j]['hora_fin'] .  ' y se mandó a reprogramar cita: ' . $datosCita['id_cita'] . '<br>';
				$query_query = "INSERT INTO citas_por_reprogramar VALUES('" . $datosCita['id_cita'] . "','" . $horario[$j]['id_horario'] . "','" . $datosCita['id_derecho'] . "','" . $dias[$i] . "','0','" . $datosCita['observaciones'] . "','" . $datosCita['id_usuario'] . "','a reprogramar por incidencia por " . $_SESSION['idUsuario'] . "');";
				$res = ejecutarSQL($query_query);
				$query_query = "DELETE FROM citas WHERE id_cita='" . $datosCita['id_cita'] . "' LIMIT 1";
				$res2 = ejecutarSQL($query_query);
				$query_query = "INSERT INTO citas VALUES (NULL,'" .  $horario[$j]['id_horario'] . "','-1','" . $dias[$i] . "','" . $_GET['tipo_incidencia'] . "','" . $_GET['observaciones'] . "','" . $_SESSION['idUsuario'] . "','','','');";
				$res = ejecutarSQL($query_query);
			}
		}
	}
}
?>

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" colspan="2" align="center"><?php echo $out ?></td>
  </tr>
  <tr>
    <td class="textosParaInputs" colspan="2" align="center">SE AGREGARON CORRECTAMENTE LAS INCIDENCIAS</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center">
  <input type="button" name="regresar" id="regresar" value="Continuar" class="botones" onclick="javascript: location.href = 'incidencias.php?idConsultorio=<?php echo $_SESSION['IdCon'] ?>&idServicio=<?php echo $_SESSION['idServ'] ?>&idMedico=<?php echo $_SESSION['idDr'] ?>'" />
  &nbsp;&nbsp;&nbsp;&nbsp;
  <br /><br /></td>
  </tr>
</table>
              
              
              
              </td>
            </tr>
          </table>
      </div>




    </td></tr>
    <tr><td align="center">
      <table width="800">
      <tr><td><div id="menu" style="display:none"><a href="#" title="Reportes" class="botones_menu"><img src="diseno/printer.png" width="40" height="40" border="0" /><br>Reportes</a><br><br><br><br>
      			<a href="javascript: mostrarDiv('listaCitasReprogramar');" title="Reprogramar Citas" class="botones_menu"><div id="citasReprogramar"><img src="diseno/reprogramar.png" width="40" height="40" border="0" /></div><br>a Reprogramar</a><br><br>
                <div style="z-index:100; width:500px; height:200px; float:left; overflow:auto; position:absolute; background-color:#EEEEEE; border:solid; border-width:1px; border-color:#005AB3" id="listaCitasReprogramar">
                </div><br><br><br><br><br><br><br><br><br><br><br><br>
                <a href="#" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="40" height="40" border="0" /><br>Salir</a>
              </div></td>
      <td align="center">
      <div id="contenido">
      </div>
      </td></tr></table>
    </td></tr></table>
    
    
    
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>



</body>
</html>
<?php }
?>