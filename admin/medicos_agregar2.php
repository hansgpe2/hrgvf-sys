<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {

	$horario = array();
	$horario[0] = "";
	for ($i=1;$i<8;$i++) {
		$hor = 1;	
		$hora_actual = $_POST['hora_entrada'];
		$totalHorarios = 0;
		while (strtotime($hora_actual . " + " . $_POST['intervalo_citas_PRV'] . " minutes") <= strtotime($_POST['hora_salida'])) {
			$hora_actual = date("H:i",strtotime($hora_actual . " + " . $_POST['intervalo_citas_PRV'] . " minutes"));
			$totalHorarios++;
		}
		$hora_actual = $_POST['hora_entrada'];
		while (strtotime($hora_actual . " + " . $_POST['intervalo_citas_PRV'] . " minutes") <= strtotime($_POST['hora_salida'])) {
			$hora_temp = $hora_actual;
			$hora_actual = date("H:i",strtotime($hora_actual . " + " . $_POST['intervalo_citas_PRV'] . " minutes"));
			if ($i == 7) {
				$dia = diaSemana(0);
			} else {
				$dia = diaSemana($i);
			}
			$horario[$i] .= "<div id=\"div" . $dia . $hor . "\">" . $hora_temp . " a " . $hora_actual . " - <input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|0\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas_PRV'] . "','" . $_POST['intervalo_citas_SUB'] . "','" . $_POST['intervalo_citas_PRO'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Primera Vez <input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|1\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas_PRV'] . "','" . $_POST['intervalo_citas_SUB'] . "','" . $_POST['intervalo_citas_PRO'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Subsecuente <input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|2\" onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas_PRV'] . "','" . $_POST['intervalo_citas_SUB'] . "','" . $_POST['intervalo_citas_PRO'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Procedimiento <input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_temp . "|" . $hora_actual . "|-1\" checked onClick=\"javascript: rehacerHorario(this,'" . $_POST['intervalo_citas_PRV'] . "','" . $_POST['intervalo_citas_SUB'] . "','" . $_POST['intervalo_citas_PRO'] . "','div" . $dia . $hor . "'," . $totalHorarios . "," . $hor . ",'" . $dia . "','" . $_POST['hora_salida'] . "');\">Sin Cita<br><br></div>";
			$hor++;
		}
		
		if (strtotime($hora_actual) < strtotime($_POST['hora_salida'])) {
			$horario[$i] .= $hora_actual . " a " . $_POST['hora_salida'] . " - <input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|0\">Primera Vez <input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|1\">Subsecuente <input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|2\">Procedimiento <input type=\"radio\" name=\"hor" . $dia . $hor . "\" value=\"" . $dia . "|" . $hora_actual . "|" . $_POST['hora_salida'] . "|-1\" checked>Sin Cita<br><br>";
		}
	}
	$consultorios = getConsultorios();
	$tConsultorios = count($consultorios);
	$opcionesConsultorios = "";
	for ($i=0;$i<$tConsultorios;$i++) {
		$opcionesConsultorios.= "<option value=\"" . $consultorios[$i]['id_consultorio'] . "\">" . $consultorios[$i]['nombre'] . "</option>";
	}
	$servicios = getServicios();
	$tServicios = count($servicios);
	$opcionesServicios = "";
	for ($i=0;$i<$tServicios;$i++) {
		$opcionesServicios.= "<option value=\"" . $servicios[$i]['id_servicio'] . "\">" . $servicios[$i]['nombre'] . "</option>";
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
<script src="../SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="medicos_agregar_confirmar.php" onsubmit="MM_validateForm('id_consultorio','','R','id_servicio','','R','cedula','','C','ap_p','','R','ap_m','','R','nombre','','R','hora_entrada','','H','hora_salida','','H');return document.MM_returnValue" method="post" name="forma" id="forma">
        <input name="cedula" type="hidden" value="<?php echo $_POST['cedula']; ?>" />
        <input name="cedula_tipo" type="hidden" id="cedula_tipo" value="<?php echo $_POST['cedula_tipo']; ?>" />
        <input name="n_empleado" type="hidden" id="n_empleado" value="<?php echo $_POST['n_empleado']; ?>" />
        <input name="ced_pro" type="hidden" id="ced_pro" value="<?php echo $_POST['ced_pro']; ?>" />
        <input name="ap_p" type="hidden" id="ap_p" value="<?php echo $_POST['ap_p']; ?>" />
        <input name="ap_m" type="hidden" id="ap_m" value="<?php echo $_POST['ap_m']; ?>" />
        <input name="nombre" type="hidden" id="nombre" value="<?php echo $_POST['nombre']; ?>" />
        <input name="turno" type="hidden" id="turno" value="<?php echo $_POST['turno']; ?>" />
        <input name="telefono" type="hidden" id="telefono" value="<?php echo $_POST['telefono']; ?>" />
        <input name="direccion" type="hidden" id="direccion" value="<?php echo $_POST['direccion']; ?>" />
        <input name="tipo_medico" type="hidden" id="tipo_medico" value="<?php echo $_POST['tipo_medico']; ?>" />
        <input name="hora_entrada" type="hidden" id="hora_entrada" value="<?php echo $_POST['hora_entrada']; ?>" />
        <input name="hora_salida" type="hidden" id="hora_salida" value="<?php echo $_POST['hora_salida']; ?>" />
        <input name="intervalo_citas_PRV" type="hidden" id="intervalo_citas_PRV" value="<?php echo $_POST['intervalo_citas_PRV']; ?>" />
        <input name="intervalo_citas_SUB" type="hidden" id="intervalo_citas_SUB" value="<?php echo $_POST['intervalo_citas_SUB']; ?>" />
        <input name="intervalo_citas_PRO" type="hidden" id="intervalo_citas_PRO" value="<?php echo $_POST['intervalo_citas_PRO']; ?>" />
        <input name="observaciones" type="hidden" id="observaciones" value="<?php echo $_POST['observaciones']; ?>" />
    	<table class="ventana">
          <tr class="tituloVentana">
            <td colspan="2">Agregar M&eacute;dico - Horarios</td>
          </tr>
		  <tr>
		    <td width="95">Consultorio:</td>
		    <td width="692"><select name="id_consultorio" id="id_consultorio">
            	<option selected="selected" value=""></option>
            	<?php echo $opcionesConsultorios; ?>
		      </select>
            </td>
          </tr>
		  <tr>
		    <td>Servicio:</td>
		    <td><select name="id_servicio" id="id_servicio">
            	<option selected="selected" value=""></option>
            	<?php echo $opcionesServicios; ?>
		      </select>
            </td>
		  </tr>
		  <tr><td align="center" colspan="2"><div id="TabbedPanels1" class="TabbedPanels">
		    <ul class="TabbedPanelsTabGroup">
		      <li class="TabbedPanelsTab" tabindex="0">LUNES</li>
		      <li class="TabbedPanelsTab" tabindex="0">MARTES</li>
		      <li class="TabbedPanelsTab" tabindex="0">MIERCOLES</li>
		      <li class="TabbedPanelsTab" tabindex="0">JUEVES</li>
		      <li class="TabbedPanelsTab" tabindex="0">VIERNES</li>
		      <li class="TabbedPanelsTab" tabindex="0">SABADO</li>
		      <li class="TabbedPanelsTab" tabindex="0">DOMINGO</li>
		    </ul>
		    <div class="TabbedPanelsContentGroup">
		      <div id="LUNES" class="TabbedPanelsContent"><?php echo $horario[1]; ?></div>
		      <div id="MARTES" class="TabbedPanelsContent"><?php echo $horario[2]; ?></div>
		      <div id="MIERCOLES" class="TabbedPanelsContent"><?php echo $horario[3]; ?></div>
		      <div id="JUEVES" class="TabbedPanelsContent"><?php echo $horario[4]; ?></div>
		      <div id="VIERNES" class="TabbedPanelsContent"><?php echo $horario[5]; ?></div>
		      <div id="SABADO" class="TabbedPanelsContent"><?php echo $horario[6]; ?></div>
		      <div id="DOMINGO" class="TabbedPanelsContent"><?php echo $horario[7]; ?></div>
		    </div>
		    </div>
		    <br><br><br><br><br>
		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" onclick="" value="Continuar" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
<script type="text/javascript">
<!--
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
//-->
</script>
</body>
</html>
<?php } ?>