<?php
error_reporting(E_ERROR);
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	
	$id_usuario = $_GET["id_usuario"];
	$usuario = getUsuarioXid($id_usuario);
	$limitados = getLimitadosXid($id_usuario);
	$tLimitados = count($limitados);
	$servicios = getServiciosOnombre();
	$tServicios = count($servicios);
//	$datos = getServicios();
	$TDatos = count($datos);

	if ($tLimitados == 0) $out .= '<p align="center">' . $usuario["nombre"] . ' actualmente no tiene limitado el acceso a agendas</p>';
	$out .= '<p align="center">Selecciona servicio(s) y/o médico(s) a los que vas a <b>PERMITIR EL ACCESO A LA AGENDA</b><br>al usuario ' . $usuario["nombre"] . '</p>';
	$out .= '<p><input type="checkbox" id="todos1" name="todos1" onchange="javascript: selAll(this);">Seleccionar Todos</p>';
	if ($tServicios > 0) {
		$out .= '<ul id="subXserv">';
		for ($i=0; $i<$tServicios; $i++) {
			$medicos = getMedicosXServicio($servicios[$i]["id_servicio"]);
			$tMedicos = count($medicos);
			if ($tMedicos > 0) {
				$servSelected = '';
				$servicioAut = getLimitadosXidDistintoSer($id_usuario, $servicios[$i]['id_servicio']);
				if (count($servicioAut) == 1) $servSelected = ' checked="checked" ';
				$out .= '<li><input type="checkbox" name="ser_' . $servicios[$i]['id_servicio'] . '" id="ser_' . $servicios[$i]['id_servicio'] . '"' . $servSelected . ' onchange="javascript: selMeds(\'chkmed' . $servicios[$i]['id_servicio'] . '\', this);"> &nbsp;<a href="javascript: void(0);" onclick="javascript: mosDiv(\'divSer_' . $i . '\');">+</a> &nbsp;<label for="ser_' . $consultorios[$i]['id_consultorio'] . '">' . $servicios[$i]["nombre"] . '</label>';
				$out .= '<div id="divSer_' . $i . '" style="display:none;"><ul>';
				for ($j=0; $j<$tMedicos; $j++) {
					$mediSelected = '';
					$medicoAut = getLimitadosXidDistintoMed($id_usuario, $servicios[$i]['id_servicio'], $medicos[$j]['id_medico']);
					if (count($medicoAut) == 1) $mediSelected = ' checked="checked" ';
					$out .= '<li>&nbsp; <input type="checkbox" name="med_' . $medicos[$j]['id_consultorio'] . '_' . $servicios[$i]['id_servicio'] . '_' . $medicos[$j]['id_medico'] . '" id="med_' . $medicos[$j]['id_consultorio'] . '_' . $servicios[$i]['id_servicio'] . '_' . $medicos[$j]['id_medico'] . '" class="chkmed' . $servicios[$i]['id_servicio'] . '"' . $mediSelected . '> &nbsp;<label for="med_' . $medicos[$j]["id_medico"] . '">' . $medicos[$j]['ap_p'] . ' ' . $medicos[$j]['ap_m'] . ' ' . $medicos[$j]['nombres'] . '</label>';
					$out .= '</li>';
				}
				$out .= '</ul></div>';
				$out .= '</li>';
			}
		}
		$out .= '</ul>';
	}
	$out .= '<p><input type="checkbox" id="todos2" name="todos2" onchange="javascript: selAll(this);">Seleccionar Todos</p>';
	$out .= '<br><p align="center"><input type="button" name="enviar" id="enviar" value="Actualizar Servicios Autorizados" class="botones" onclick="javascript: enviarSerAct()">';
	
/*	
	
	$out .= "<table class=\"ventana\">
          <tr class=\"tituloVentana\">
            <td>Clave</td>
            <td>Servicio</td>
            <td>Acci&oacute;n</td>
          </tr>";
	
	for ($i=0;$i<$TDatos;$i++) {
		$out.= "<tr><td>" . $datos[$i]['clave'] . "</td><td>" . $datos[$i]['nombre'] . "</td><td><input name=\"mod" . $i . "\" id=\"mod" . $i . "\" type=\"button\" value=\"Modificar\" class=\"botones\" onClick=\"javascript: location.href= 'servicios_modificar.php?id_servicio=" . $datos[$i]['id_servicio'] . "'\" /> <input name=\"eli" . $i . "\" id=\"eli" . $i . "\" type=\"button\" value=\"Eliminar\" class=\"botones\" onClick=\"javascript: location.href= 'servicios_eliminar.php?id_servicio=" . $datos[$i]['id_servicio'] . "'\" /></td></tr>";
	}
	$out.= "</table>";
*/

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<form id="formaS" name="formaS" action="subsecuentesXservicio_actualizar_do.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id_usuario ?>" />
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="left"><?php echo $out; ?>
     </td></tr></table>
</td></tr></table>
</form>
<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>