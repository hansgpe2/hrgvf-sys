<?php
session_start ();
include_once('../lib/monket-cal-parseAdmin.php');

	if ($_SESSION['IdCon'] != $_GET['idConsultorio']) {
		$_SESSION['IdCon'] = $_GET['idConsultorio'];
		$_SESSION['idServ'] = "-1";
		$_SESSION['idDr'] = "-1";
	}else if ($_SESSION['idServ'] != $_GET['idServicio']) {
//		$_SESSION['IdCon'] = $_GET['idConsultorio'];
		$_SESSION['idServ'] = $_GET['idServicio'];
		$_SESSION['idDr'] = "-1";
	} else if ($_SESSION['idDr'] != $_GET['idMedico']) {
//		$_SESSION['IdCon'] = $_GET['idConsultorio'];
//		$_SESSION['idServ'] = $_GET['idServicio'];
		$_SESSION['idDr'] = $_GET['idMedico'];
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>

<?php echo getCalInitHTML();
?>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
	
    <table id="centro" class="centro" width="800" height="499">
    <tr><td valign="top" align="center"><div id="seleccion">&nbsp;</div>
<script language="javascript" type="text/javascript">
				var contenedor = document.getElementById('seleccion');
				var seleccion= new AjaxGET();
				seleccion.open("GET", "seleccionConSerDr.php?idConsultorio=-1&idServicio=-1&idMedico=-1",true);
				seleccion.onreadystatechange=function()
				{
					if (seleccion.readyState==4) // Readystate 4 significa que ya acab&oacute; de cargarlo
					{
						contenedor.innerHTML = seleccion.responseText;
						cargarAgenda()
					}
					if ((seleccion.readyState==1) ||(seleccion.readyState==2)||(seleccion.readyState==3))
					{
						contenedor.innerHTML = "<img src=\"../diseno/loading.gif\">";
					}
				}
				seleccion.send(null)
</script>
    
      <div id="contenido">
      </div>




    </td></tr>
    <tr><td align="center">
      <table width="800">
      <tr><td>&nbsp;</td>
      <td align="center">
      <div id="contenido">
      </div>
      </td></tr></table>
    </td></tr></table>
    
    
    
     </td></tr></table>
	<div id="incidenciasActuales">
    </div>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
