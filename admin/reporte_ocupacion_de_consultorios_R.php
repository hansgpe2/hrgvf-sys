<?php session_start ();
require_once("../lib/funcionesAdmin.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>OCUPACION DE CONSULTORIOS</title>
<style type="text/css">
	@import url("../lib/impresion.css") print;
	@import url("../lib/reportes.css") screen;
</style>
</head>
</head>

<body>

<?php // reporte por consultorio

if ($_GET['tipoReporte'] == 'consultorio') {
	$datosConsultorio = getConsultorioXid($_GET['idConsultorio']);
	$enca = "<tr><td colspan=\"14\" align=\"center\"><br>CONSULTORIO " . ponerAcentos($datosConsultorio['nombre']) . "</td></tr>";
	$out = "";
	$servicios = getServiciosXConsultorio($_GET['idConsultorio']);
	$totalServicios = count($servicios);
	for ($y=0; $y<$totalServicios; $y++) {
		$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$servicios[$y]['id_servicio']);
		$totalMedicos = count($medicos);
		for ($x=0; $x<$totalMedicos; $x++) {
			$citasL = getCitasXdiaXmedico("LUNES",$_GET['idConsultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
			$citasM = getCitasXdiaXmedico("MARTES",$_GET['idConsultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
			$citasI = getCitasXdiaXmedico("MIERCOLES",$_GET['idConsultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
			$citasJ = getCitasXdiaXmedico("JUEVES",$_GET['idConsultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
			$citasV = getCitasXdiaXmedico("VIERNES",$_GET['idConsultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
			$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
			$datosServicio = getServicioXid($servicios[$y]['id_servicio']);
			$claveDelMedico = $datosMedico['n_empleado'];
			$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
			$tipoMedico = $datosMedico['tipo_medico'];
			$especialidad = ponerAcentos($datosServicio['nombre']);
			$nombre = $nombreDelMedico . "<br><span class=\"contenido8bold\">" . $especialidad . "</span>";
			$horario = formatoHora($datosMedico['hora_entrada']) . "a" . formatoHora($datosMedico['hora_salida']);
			$out .= "<tr>
						<td align=\"left\" class=\"contenido8\">" . $nombre . "</td>
						<td align=\"left\" class=\"contenido8\">" . $claveDelMedico . "</td>
						<td align=\"left\" class=\"contenido8\">" . $tipoMedico . "</td>
						<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasL['hora']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasL['tipo']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasM['hora']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasM['tipo']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasI['hora']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasI['tipo']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasJ['hora']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasJ['tipo']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasV['hora']  . "</td>
						<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasV['tipo']  . "</td>
					</tr>";
			$claveUnidadMedica = "&nbsp;";
			$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
			$delegacion = "DELEGACION ESTATAL EN JALISCO";
		}
	}
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
  <tr>
	<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	  <tr>
		<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
		<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
		  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
		<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />OCUPACION DE CONSULTORIOS</td>
		<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td class=\"tituloEncabezado\" align=\"center\">Fecha Elaboraci&oacute;n</td>
		  </tr>
		  <tr>
			<td align=\"center\" class=\"contenido8bold\">" . date('d/m/Y') . "</td>
		  </tr>
		</table></td>
	  </tr>
	</table></td>
  </tr>
  <tr>
	<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
  </tr>
  <tr>
	<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
  </tr>
  <tr>
	<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
  </tr>
  <tr>
	<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">" . $enca . "
					  <tr>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">M&eacute;dico que Otorga</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horario</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Lunes</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Martes</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Mi&eacute;rcoles</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Jueves</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Viernes</td>
					  </tr>
					  <tr>
						<td width=\"350\" align=\"center\" class=\"contenido8bold\">Nombre</td>
						<td width=\"70\" align=\"center\" class=\"contenido8bold\">N. Empleado</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
					  </tr>" . $out . "
					</table></td>
				  </tr>
				</table>";
	echo $reporte;
}

if ($_GET['tipoReporte'] == 'unidad') {
	$consultorios = getConsultorios();
	$totalConsultorios = count($consultorios);
	$parcial = "";
	for($z=0;$z<$totalConsultorios; $z++) {
		$datosConsultorio = getConsultorioXid($consultorios[$z]['id_consultorio']);
		$enca = "<tr><td colspan=\"14\" align=\"center\"><br>CONSULTORIO " . ponerAcentos($datosConsultorio['nombre']) . "</td></tr>";
		$out = "";
		$servicios = getServiciosXConsultorio($consultorios[$z]['id_consultorio']);
		$totalServicios = count($servicios);
		for ($y=0; $y<$totalServicios; $y++) {
			$medicos = getMedicosXServicioXConsultorio($consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio']);
			$totalMedicos = count($medicos);
			for ($x=0; $x<$totalMedicos; $x++) {
				$citasL = getCitasXdiaXmedico("LUNES",$consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
				$citasM = getCitasXdiaXmedico("MARTES",$consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
				$citasI = getCitasXdiaXmedico("MIERCOLES",$consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
				$citasJ = getCitasXdiaXmedico("JUEVES",$consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
				$citasV = getCitasXdiaXmedico("VIERNES",$consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio'],$medicos[$x]['id_medico']);
				$datosMedico = getMedicoXid($medicos[$x]['id_medico']);
				$datosServicio = getServicioXid($servicios[$y]['id_servicio']);
				$claveDelMedico = $datosMedico['n_empleado'];
				$nombreDelMedico = ponerAcentos($datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
				$tipoMedico = $datosMedico['tipo_medico'];
				$especialidad = ponerAcentos($datosServicio['nombre']);
				$nombre = $nombreDelMedico . "<br><span class=\"contenido8bold\">" . $especialidad . "</span>";
				$horario = formatoHora($datosMedico['hora_entrada']) . "a" . formatoHora($datosMedico['hora_salida']);
				$out .= "<tr>
							<td align=\"left\" class=\"contenido8\">" . $nombre . "</td>
							<td align=\"left\" class=\"contenido8\">" . $claveDelMedico . "</td>
							<td align=\"left\" class=\"contenido8\">" . $tipoMedico . "</td>
							<td align=\"left\" class=\"contenido8\">" . $horario . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasL['hora']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasL['tipo']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasM['hora']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasM['tipo']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasI['hora']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasI['tipo']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasJ['hora']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasJ['tipo']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasV['hora']  . "</td>
							<td align=\"center\" valign=\"top\" class=\"contenido8\">" . $citasV['tipo']  . "</td>
						</tr>";
				$claveUnidadMedica = "&nbsp;";
				$unidadMedica = "H.R. VALENTIN GOMEZ FARIAS, ZAPOPAN";
				$delegacion = "DELEGACION ESTATAL EN JALISCO";
			}
		}
		$parcial .= "<table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">" . $enca . "
					  <tr>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">M&eacute;dico que Otorga</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td rowspan=\"2\" width=\"30\" align=\"center\" class=\"contenido8bold\">Horario</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Lunes</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Martes</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Mi&eacute;rcoles</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Jueves</td>
						<td colspan=\"2\" align=\"center\" class=\"contenido8bold\">Viernes</td>
					  </tr>
					  <tr>
						<td width=\"350\" align=\"center\" class=\"contenido8bold\">Nombre</td>
						<td width=\"70\" align=\"center\" class=\"contenido8bold\">N. Empleado</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Hora</td>
						<td width=\"15\" align=\"center\" class=\"contenido8bold\">Tipo</td>
					  </tr>" . $out . "
					</table>";
	}
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	<tr>
	<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
	  <tr>
		<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
		<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
		  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
		<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />OCUPACION DE CONSULTORIOS</td>
		<td width=\"150\" valign=\"bottom\" align=\"right\"><table width=\"150\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
		  <tr>
			<td class=\"tituloEncabezado\" align=\"center\">Fecha Elaboraci&oacute;n</td>
		  </tr>
		  <tr>
			<td align=\"center\" class=\"contenido8bold\">" . date('d/m/Y') . "</td>
		  </tr>
		</table></td>
	  </tr>
	</table></td>
	</tr>
	<tr>
	<td align=\"left\" class=\"tituloEncabezadoConBorde\">Datos de la Unidad Médica</td>
	</tr>
	<tr>
	<td align=\"left\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" width=\"20%\"><span class=\"contenido8bold\">Clave Unidad Médica: </span><span class=\"contenido8\">" . $claveUnidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Unidad Médica: </span><span class=\"contenido8\">" . $unidadMedica . "</span></td><td align=\"left\" width=\"40%\"><span class=\"contenido8bold\">Delegación: </span><span class=\"contenido8\">" . $delegacion . "</span></td></tr></table></td>
	</tr>
	<tr>
	<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
	</tr>
	<tr>
	<td>" . $parcial . "</td>
				  </tr>
				</table>";
	echo $reporte;
}
?>
</body>
</html>
