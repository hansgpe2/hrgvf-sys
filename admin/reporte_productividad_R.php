<?php 
set_time_limit(300);
session_start ();
require_once("../lib/funcionesAdmin.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>REPORTE DE PRODUCTIVIDAD</title>
<style type="text/css">
	@import url("../lib/impresion.css") print;
	@import url("../lib/reportes.css") screen;
</style>
</head>
</head>

<body>

<?php if($_GET['tipoReporte'] == "apellidos") $orden = " nombre ASC";
	$derecho = getProductividad($orden);
	$tDerecho = count($derecho);
	$out = "";
	for($i=0;$i<$tDerecho;$i++) {
		$out .= "<tr>
					<td width=\"35\" align=\"left\" class=\"contenido8\">" . $derecho[$i]['id_usuario'] . "</td>
					<td width=\"100\" align=\"left\" class=\"contenido8\">" . ponerAcentos($derecho[$i]['nombre']) . "</td>
					<td width=\"35\" align=\"left\" class=\"contenido8\">" . $derecho[$i]['citas']  . "</td>
				</tr>";
	}
	if ($out == "")	$out = "<tr height=\"30\"><td colspan=\"5\" align=\"center\" class=\"contenido8\">NO EXISTEN DERECHOHABIENTES</td></tr>";
	$reporte = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
				  <tr>
					<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td width=\"74\" align=\"left\"><img src=\"../diseno/logoEncabezado.png\" width=\"74\" height=\"74\" /></td>
						<td width=\"80\" class=\"tituloIssste\" align=\"left\">Instituto de<br />
						  Seguridad y<br />Servicios<br />Sociales de los<br />Trabajadores del<br />Estado</td>
						<td align=\"center\">SUBDIRECCIÓN GENERAL MÉDICA<br />REPORTE DE PRODUCTIVIDAD</td>
						<td width=\"150\" valign=\"bottom\" align=\"right\">&nbsp;</td>
					  </tr>
					</table></td>
				  </tr>
				  <tr>
					<td class=\"tituloEncabezadoConBorde\" height=\"2\"></td>
				  </tr>
				  <tr>
					<td><table width=\"100%\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">
					  <tr>
						<td width=\"35\" align=\"center\" class=\"contenido8bold\">id_usuario</td>
						<td width=\"250\" align=\"center\" class=\"contenido8bold\">Nombre</td>
						<td width=\"1000\" align=\"left\" class=\"contenido8bold\">Citas</td>
					  </tr>" . $out . "
					</table></td>
				  </tr>
				</table>";
	echo $reporte;
?>
</body>
</html>
