<?php
session_start ();
require_once("../lib/funcionesAdmin.php");

if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	if (isset($_POST['id_dh'])) {
		$citas = getCitasXdh($_POST['id_dh']);
		$tCitas = count($citas);
		for ($j=0;$j<$tCitas;$j++) {
			$query_cita_eliminada = "INSERT INTO citas_eliminadas VALUES ('" . $citas[$j]['id_cita'] . "','" . $citas[$j]['id_horario'] . "','" . $citas[$j]['id_derecho'] . "','" . $citas[$j]['fecha_cita'] . "','" . $citas[$j]['status'] . "','" . $citas[$j]['observaciones'] . "','" . $citas[$j]['id_usuario'] . "','eliminada con dh por " . $_SESSION['idUsuario'] . "');";
			$res = ejecutarSQL($query_cita_eliminada);
			if ($res[0] == 0) {// no hay error
				$query_cita = "DELETE FROM citas WHERE id_cita='" . $citas[$j]['id_cita'] . "' AND id_horario='" . $citas[$j]['id_horario'] . "' AND id_derecho='" . $citas[$j]['id_derecho'] . "' AND fecha_cita='" . $citas[$j]['fecha_cita'] . "'AND status='" . $citas[$j]['status'] . "'AND observaciones='" . $citas[$j]['observaciones'] . "' LIMIT 1";
				$res2 = ejecutarSQL($query_cita);
			} else { // hay error en el mysql
				echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de eliminar cita del derechohabiente'); history.back();</script>";
			}
		}
		$query = "UPDATE derechohabientes SET st='0', extra1='eliminado por " . $_SESSION['idUsuario'] . "' WHERE id_derecho='" . $_POST['id_dh'] . "' LIMIT 1";
		$res5 = ejecutarSQL($query);
		if ($res5[0] == 0) {// no hay error
			$out = "ok";
		} else { // hay error en el mysql
			echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Error en bd al tratar de eliminar al derechohabiente'); history.back();</script>";
		}
	} else {
	echo "<script  language=\"javascript\" type=\"text/javascript\">alert('No existe la variable id del derechohabiente'); history.back();</script>";
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="consultorios_agregar_confirmar.php" onsubmit="MM_validateForm('nombre','','R');return document.MM_returnValue" method="post" name="forma">
    	<table class="ventana">
          <tr class="tituloVentana">
            <td>Eliminar Derechohabiente</td>
          </tr>
		  <tr><td align="center"><br>
		      <span class="error">"<b><?php echo $_POST['ap_p'] . " " . $_POST['ap_m'] . " " .  $_POST['nombre']; ?></b>" se elimin&oacute; correctamente</span><br>
		  <br>
		      <input name="continuar" id="continuar" type="button" value="Continuar" class="botones" onclick="javascript: location.replace('derechohabientes.php');" />
		      &nbsp;&nbsp;</td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>