<?php
session_start ();
require_once("../lib/funcionesAdmin.php");


if ($_SESSION['idUsuario'] == "-1" ) {
	echo "<script  language=\"javascript\" type=\"text/javascript\">location.replace('index.php');</script>";
} else {
	$medico = getMedicoXid($_GET['id_medico']);
	if ($medico['cedula_tipo'] == "10") {
	$cedula_tipo = "<option value=\"10\" selected=\"selected\">10</option>
		      		<option value=\"20\">20</option>";
	} else {
	$cedula_tipo = "<option value=\"10\">10</option>
		      		<option value=\"20\" selected=\"selected\">20</option>";
	}
	if ($medico['turno'] == "MATUTINO") {
		$turno = "<option value=\"MATUTINO\" selected=\"selected\">MATUTINO</option>
		      <option value=\"VESPERTINO\">VESPERTINO</option>";
	} else {
		$turno = "<option value=\"MATUTINO\">MATUTINO</option>
		      <option value=\"VESPERTINO\" selected=\"selected\">VESPERTINO</option>";
	}
	if ($medico['tipo_medico'] == "ADSCRITO") {
		$tipo_medico = "<option value=\"ADSCRITO\" selected=\"selected\">ADSCRITO</option>
		      			<option value=\"SUPLENTE\">SUPLENTE</option>";
	} else {
		$tipo_medico = "<option value=\"ADSCRITO\">ADSCRITO</option>
		      			<option value=\"SUPLENTE\" selected=\"selected\">SUPLENTE</option>";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ISSSTE - Administración</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryMenuBarVertical.css" rel="stylesheet" type="text/css" />
<style type="text/css">
        @import "../lib/misEstilos.css";
</style>
<script language="javascript" type="text/javascript" src="../lib/admin.js"></script>
</head>

<body>
<center>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
	<table class="encabezado" id="encabezado" width="800" height="101"><tr><td>
	  <table width="800" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="../diseno/logoEncabezado.png" width="74" height="74" /></td>
          <td height="48" class="tituloEncabezado" valign="middle">Cita Médica Electrónica &nbsp;</td>
        </tr>
        <tr>
          <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
        </tr>
      </table>
	</td></tr></table>
    </td></tr><tr><td>
    <table id="centro" class="centro" width="807" height="499">
    <tr><td width="799" height="15" align="center" valign="top" bgcolor="#CEE2F6"><?php echo $menu[$_SESSION['tipoUsuario']]; ?>
    </td>
    </tr>
    <tr><td align="center">
    	<form action="medicos_eliminar_confirmar.php" onsubmit="MM_validateForm('cedula','','C','n_empleado','','E','ap_p','','R','ap_m','','R','nombre','','R');return document.MM_returnValue" method="post" name="forma">
        <input name="id_medico" type="hidden" id="id_medico" value="<?php echo $_GET['id_medico']; ?>" />
    	<table class="ventana">
          <tr class="tituloVentana">
            <td colspan="2">Eliminar M&eacute;dico</td>
          </tr>
		  <tr><td>C&eacute;dula:</td><td><input name="cedula" type="text" size="15" maxlength="10" id="cedula" value="<?php echo $medico['cedula']; ?>" readonly="readonly" /></td></tr>
		  <tr><td>Tipo de C&eacute;dula:</td>
		    <td><select name="cedula_tipo" id="cedula_tipo" disabled="disabled"><?php echo $cedula_tipo ?>
		      </select>		    </td>
          </tr>
		  <tr>
		    <td>No. de Empleado:</td>
		    <td><input name="n_empleado" type="text" size="15" maxlength="6" id="n_empleado" value="<?php echo $medico['n_empleado']; ?>" readonly="readonly" /></td>
		  </tr>
		  <tr>
		    <td>C&eacute;dula Profesional:</td>
		    <td><input name="ced_pro" type="text" size="15" maxlength="15" id="ced_pro" value="<?php echo $medico['ced_pro']; ?>" readonly="readonly" /></td>
		  </tr>
		  <tr><td>Apellido Paterno:</td><td><input name="ap_p" type="text" size="20" maxlength="20" id="ap_p" value="<?php echo ponerAcentos($medico['ap_p']); ?>" readonly="readonly" /></td></tr>
		  <tr><td>Apellido Materno:</td><td><input name="ap_m" type="text" size="20" maxlength="20" id="ap_m" value="<?php echo ponerAcentos($medico['ap_m']); ?>" readonly="readonly" /></td></tr>
		  <tr><td>Nombre(s):</td><td><input name="nombre" type="text" size="20" maxlength="20" id="nombre" value="<?php echo ponerAcentos($medico['nombres']); ?>" readonly="readonly" /></td></tr>
		  <tr><td>Turno:</td>
		    <td><select name="turno" id="turno" disabled="disabled"><?php echo $turno ?>
		      </select>		    </td>
          </tr>
		  <tr><td>Tel&eacute;fono:</td><td><input name="telefono" type="text" size="15" maxlength="10" id="telefono" value="<?php echo ponerAcentos($medico['telefono']); ?>" readonly="readonly" /></td></tr>
		  <tr><td>Direcci&oacute;n:</td><td><input name="direccion" type="text" size="46" maxlength="50" id="direccion" value="<?php echo ponerAcentos($medico['direccion']); ?>" readonly="readonly" /></td></tr>
		  <tr>
		    <td>Tipo de M&eacute;dico:</td>
		    <td><select name="tipo_medico" id="tipo_medico" disabled="disabled"><?php echo $tipo_medico; ?>
		      </select>		    </td>
		  </tr>
		  <tr>
		    <td colspan="2">Para modificar horario e intervalo de citas vaya a la seccion &quot;horarios&quot;</td>
		  </tr>
		  <tr><td>Observaciones:</td>
		    <td><textarea name="observaciones" id="observaciones" cols="45" rows="5" disabled="disabled"><?php echo ponerAcentos($medico['observaciones']); ?></textarea></td>
		  </tr>
		  <tr><td align="center" colspan="2"><br><span class="tituloVentana">NOTA</span><span class="error">: Al eliminar un médico se perder&aacute; toda informaci&oacute;n referente a &eacute;l (datos personales, horarios, citas)</span><br><br>

		      <input name="cancelar" id="cancelar" type="button" value="Cancelar" class="botones" onclick="javascript: history.back();" />
		      &nbsp;&nbsp;
		       <input name="agregar" type="submit" class="botones" id="agregar" onclick="" value="Eliminar Médico" />
          </td></tr>
        </table>
		</form>
     </td></tr></table>
</td></tr></table>

<script type="text/javascript">
<!--
var MenuBar1 = new Spry.Widget.MenuBar("menu", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
//-->
</script>
</center>
</body>
</html>
<?php } ?>