<?php
session_start ();
include_once('lib/misFunciones.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body class="soria">
<form id="formaCita" method="POST" action="javascript: validarAgregarContrato();">
<input name="id_contrato" type="hidden" id="id_contrato" value="" />
<input name="id_proveedor" type="hidden" id="id_proveedor" value="" />
<input name="vSubto" type="hidden" id="vSubto" value="0" />

<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td colspan="2" class="tituloVentana">AGREGAR CONTRATO</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NUMERO DE CONTRATO: </td>
    <td align="left"><input name="n_contrato" type="text" id="n_contrato" maxlength="13" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CONTRATO:</td>
    <td align="left">
      <select name="contrato" id="contrato">
        <option value="-1" selected="selected"> </option>
        <option value="0">Normal</option>
        <option value="1">De Gastos</option>
      </select>
    </td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">TIPO CONTRATO:</td>
    <td><select name="tipo_contrato" id="tipo_contrato" >
      <option value="-1" selected="selected"> </option>
      <option value="0">Licitacion</option>
      <option value="1">Directa Art. 41</option>
      <option value="2">Directa Art. 42</option>
    </select></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">PROVEEDOR:</td>
    <td>Nombre: <input name="razon_social" id="razon_social" type="text" size="30" readonly="readonly" />
      <br />
      RFC: <input name="rfc" id="rfc" type="text" size="30" readonly="readonly" />
    <input name="seleccionar" type="button" class="botones" id="seleccionar" onclick="javascript:  ocultarDiv('divAgregarProveedor'); mostrarDiv('buscar'); getElementById('nombreBuscar').focus();" value="Buscar Proveedor" />
    </td>
  </tr>
  <tr>
    <td colspan="4" align="center"><div id="modificarDH" style="display:'';"><input type="button" name="modificar" id="modificar" value="Modificar Datos del Proveedor" class="botones" disabled="disabled" onclick="javascript: habilitarParaModificarProveedor();" /></div></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">FECHA INICIO:</td>
    <td><input name="fecha_inicio" type="text" id="fecha_inicio" maxlength="10" value="<?php echo date('d/m/Y') ?>" readonly="readonly"/><a onclick="displayCalendar(document.forms[0].fecha_inicio,'dd/mm/yyyy',this,false)" title="Click para seleccionar la fecha de inicio del contrato" href="javascript: vacia();"><img src="diseno/calendario.png" border="0" /></a></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">FECHA TERMINO:</td>
    <td><input name="fecha_termino" type="text" id="fecha_termino" maxlength="10" value="<?php echo date('d/m/Y') ?>" readonly="readonly"/><a onclick="displayCalendar(document.forms[0].fecha_termino,'dd/mm/yyyy',this,false)" title="Click para seleccionar la fecha de termino del contrato" href="javascript: vacia();"><img src="diseno/calendario.png" border="0" /></a></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">DESCRIPCION DEL CONTRATO:</td>
    <td colspan="3"><textarea name="descripcion" cols="40" rows="4" id="descripcion"></textarea></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">MONTO TOTAL DEL CONTRATO: </td>
    <td align="left">$<input name="monto" type="text" id="monto" onfocus="javascript: borrarCero(this);" onkeydown="javascript:copiarAcantidad(this);" value="0" maxlength="13" /> pesos</td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">PARTIDA(S) QUE AFECTA:</td>
    <td align="left">
      <select name="partida1" id="partida1"><?php echo opcionesPartidasConSaldo(); ?>
      </select><br />Cantidad a utilizar: $<input name="cantidad1" type="text" id="cantidad1" value="0" size="10" maxlength="10" onfocus="javascript: borrarCero(this);" />pesos<br /><br />
      <select name="partida2" id="partida2"><?php echo opcionesPartidasConSaldo(); ?>
      </select><br />Cantidad a utilizar: $<input name="cantidad2" type="text" id="cantidad2" value="0" size="10" maxlength="10" onfocus="javascript: borrarCero(this);" />pesos<br /><br />
      <select name="partida3" id="partida3"><?php echo opcionesPartidasConSaldo(); ?>
      </select><br />Cantidad a utilizar: $<input name="cantidad3" type="text" id="cantidad3" value="0" size="10" maxlength="10" onfocus="javascript: borrarCero(this);" />pesos<br /><br />
      <select name="partida4" id="partida4"><?php echo opcionesPartidasConSaldo(); ?>
      </select><br />Cantidad a utilizar: $<input name="cantidad4" type="text" id="cantidad4" value="0" size="10" maxlength="10" onfocus="javascript: borrarCero(this);" />pesos<br /><br />
      <select name="partida5" id="partida5"><?php echo opcionesPartidasConSaldo(); ?>
      </select><br />Cantidad a utilizar: $<input name="cantidad5" type="text" id="cantidad5" value="0" size="10" maxlength="10" onfocus="javascript: borrarCero(this);" />pesos<br /><br />
    </td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DEPARTAMENTO DEL CONTRATO: </td>
    <td align="left"><select name="departamento" id="departamento"><?php echo opcionesDepartamentos(); ?>
      </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
  <tr>
    <td class="tituloVentana" colspan="5">CONCEPTOS DEL CONTRATO</td>
  </tr>
  <tr>
  	<td class="nombrePaciente">CANTIDAD</td><td class="nombrePaciente">DESCRIPCION</td><td class="nombrePaciente">P. UNITARIO</td><td class="nombrePaciente" align="center">IMPORTE</td>
  </tr>
  <tr>
    <td colspan="5" align="right"><div id="botonAgregarRenglon"><br /><a href="javascript:agregarRenglonFactura();" class="botones_menu">Agregar Otro Rengl&oacute;n</a></div></td>
  </tr>
  <tr>
  	<td colspan="5">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant1" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant1" /></td><td align="left"><input name="desc1" type="text" size="65" maxlength="200" id="desc1" /></td><td align="right" width="80"><input name="precio1" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio1" /></td><td align="center" width="150"><input name="importe1" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe1" /></td>
          </tr>
        </table>
        <div id="renglon2" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant2" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant2" /></td><td align="left"><input name="desc2" type="text" size="65" maxlength="200" id="desc2" /></td><td align="right" width="80"><input name="precio2" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio2" /></td><td align="center" width="150"><input name="importe2" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe2" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon3" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant3" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant3" /></td><td align="left"><input name="desc3" type="text" size="65" maxlength="200" id="desc3" /></td><td align="right" width="80"><input name="precio3" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio3" /></td><td align="center" width="150"><input name="importe3" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe3" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon4" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant4" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant4" /></td><td align="left"><input name="desc4" type="text" size="65" maxlength="200" id="desc4" /></td><td align="right" width="80"><input name="precio4" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio4" /></td><td align="center" width="150"><input name="importe4" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe4" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon5" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant5" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant5" /></td><td align="left"><input name="desc5" type="text" size="65" maxlength="200" id="desc5" /></td><td align="right" width="80"><input name="precio5" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio5" /></td><td align="center" width="150"><input name="importe5" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe5" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon6" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant6" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant6" /></td><td align="left"><input name="desc6" type="text" size="65" maxlength="200" id="desc6" /></td><td align="right" width="80"><input name="precio6" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio6" /></td><td align="center" width="150"><input name="importe6" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe6" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon7" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant7" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant7" /></td><td align="left"><input name="desc7" type="text" size="65" maxlength="200" id="desc7" /></td><td align="right" width="80"><input name="precio7" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio7" /></td><td align="center" width="150"><input name="importe7" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe7" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon8" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant8" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant8" /></td><td align="left"><input name="desc8" type="text" size="65" maxlength="200" id="desc8" /></td><td align="right" width="80"><input name="precio8" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio8" /></td><td align="center" width="150"><input name="importe8" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe8" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon9" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant9" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant9" /></td><td align="left"><input name="desc9" type="text" size="65" maxlength="200" id="desc9" /></td><td align="right" width="80"><input name="precio9" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio9" /></td><td align="center" width="150"><input name="importe9" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe9" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon10" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant10" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant10" /></td><td align="left"><input name="desc10" type="text" size="65" maxlength="200" id="desc10" /></td><td align="right" width="80"><input name="precio10" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio10" /></td><td align="center" width="150"><input name="importe10" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe10" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon11" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant11" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant11" /></td><td align="left"><input name="desc11" type="text" size="65" maxlength="200" id="desc11" /></td><td align="right" width="80"><input name="precio11" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio11" /></td><td align="center" width="150"><input name="importe11" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe11" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon12" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant12" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant12" /></td><td align="left"><input name="desc12" type="text" size="65" maxlength="200" id="desc12" /></td><td align="right" width="80"><input name="precio12" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio12" /></td><td align="center" width="150"><input name="importe12" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe12" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon13" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant13" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant13" /></td><td align="left"><input name="desc13" type="text" size="65" maxlength="200" id="desc13" /></td><td align="right" width="80"><input name="precio13" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio13" /></td><td align="center" width="150"><input name="importe13" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe13" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon14" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant14" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant14" /></td><td align="left"><input name="desc14" type="text" size="65" maxlength="200" id="desc14" /></td><td align="right" width="80"><input name="precio14" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio14" /></td><td align="center" width="150"><input name="importe14" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe14" /></td>
          </tr>
        </table>
        </div>
        <div id="renglon15" style="display:none;">
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td align="center" width="80"><input name="cant15" type="text" size="6" maxlength="6" onkeyup="javascript: calcularRenglonContrato(event,this);" id="cant15" /></td><td align="left"><input name="desc15" type="text" size="65" maxlength="200" id="desc15" /></td><td align="right" width="80"><input name="precio15" type="text" size="11" maxlength="8" onkeyup="javascript: calcularRenglonContratoP(event,this);" id="precio15" /></td><td align="center" width="150"><input name="importe15" type="text" size="11" maxlength="11" readonly="readonly" value="0" id="importe15" /></td>
          </tr>
        </table>
        </div>
  	</td>
  </tr>
  <tr>
    <td colspan="3" align="right" class="nombrePaciente">TOTAL</td>
    <td align="right"><div id="subtotal">$ 0.00</div></td>
  </tr>
  <tr>
    <td colspan="4" align="center">
  <input type="button" name="regresar" id="regresar" value="Regresar" class="botones"  onclick="javascript: infoPiso('<?php echo $datos_cama['id_piso'] ?>');" />&nbsp;&nbsp;&nbsp;&nbsp;
  <input type="submit" name="agregar" id="agregar" value="Agregar Contrato" class="botones" disabled="disabled" />
  <br /><br /><div id="enviando">&nbsp;</div></td>
  </tr>
</table>
</form>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="buscar" style=" display:none; height:150px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">BUSCAR PROVEEDOR</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
            <td colspan="2">
                <form id="selDH" method="POST" action="javascript: buscarProveedor(document.getElementById('nombreBuscar').value);">
                <table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="25" class="textosParaInputs" align="right">NOMBRE PROVEEDOR: </td>
                  <td align="left"><input type="text" name="nombreBuscar" id="nombreBuscar" maxlength="30" size="30" onkeyup="this.value = this.value.toUpperCase();"/>
                    <input name="buscar" type="submit" class="botones" id="buscar" value="Buscar..." /></td>
                </tr>
                <tr>
                  <td height="25" class="textosParaInputs" align="right">PROVEEDORES: </td>
                  <td align="left"><div id="proveedores">Ingrese el nombre del proveedor y haga click en Buscar...</div>
                  </td>
                </tr>
            
                <tr>
                  <td colspan="2" align="center"><input name="cerrar" type="button" class="botones" id="cerrar" onclick="javascript: ocultarDiv('buscar');" value="Cerrar" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="javascript: cargarDatosProveedor();" value="Seleccionar" /><br /><br /></td>
                  </tr>
            
                <tr>
                  <td colspan="2" align="center"></td>
                </tr>
              </table>
              </form>
          </td>
          </tr>
          </table>
        </div>
	</td>
</tr>
</table>

<form id="agregarProveedor" method="POST" action="javascript: agregarProveedorenCitaForma();">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="divAgregarProveedor" style=" display:none; height:0px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">AGREGAR PROVEEDOR</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td height="25" class="textosParaInputs" align="right">NOMBRE (RAZON SOCIAL): </td>
              <td align="left"><input type="text" name="nombreAgregar" id="nombreAgregar" maxlength="50" size="30" onkeyup="this.value = this.value.toUpperCase();" /></td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">RFC:</td>
    <td align="left"><input name="rfcAgregar" type="text" id="rfcAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CURP:</td>
    <td align="left"><input name="curpAgregar" type="text" id="curpAgregar" size="30" maxlength="30" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" size="40" maxlength="40" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">COLONIA:</td>
    <td align="left"><input name="coloniaAgregar" type="text" id="coloniaAgregar" size="25" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CIUDAD:</td>
    <td align="left"><input name="ciudadAgregar" type="text" id="ciudadAgregar" size="25" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /> <span class="textosParaInputs"> ESTADO: </span><input name="estadoAgregar" type="text" id="estadoAgregar" size="25" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">C.P.:</td>
    <td align="left"><input name="cpAgregar" type="text" id="cpAgregar" size="10" maxlength="5" /> <span class="textosParaInputs"> TELEFONO: </span><input name="telefonoAgregar" type="text" id="telefonoAgregar" size="20" maxlength="10" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE CONTACTO:</td>
    <td align="left"><input name="nombreContactoAgregar" type="text" id="nombreContactoAgregar" size="40" maxlength="35" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO CONTACTO:</td>
    <td align="left"><input name="telefonoContactoAgregar" type="text" id="telefonoContactoAgregar" size="25" maxlength="20" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">EMAIL CONTACTO:</td>
    <td align="left"><input name="emailContactoAgregar" type="text" id="emailContactoAgregar" size="40" maxlength="35" /></td>
  </tr>
    <tr>
      <td colspan="2" align="center"><div id="divBotones_EstadoAgregarProveedor"></div>
        </td>
      </tr>

    <tr>
      <td colspan="2" align="center"></td>
    </tr>
  </table>
        </div>
	</td>
</tr>
</table>
</form>

<form id="modificarProveedor" method="POST" action="javascript: modificarProveedorenCitaForma();">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
	<td>
        <div id="divModificarProveedor" style=" display:none; height:0px; margin-top:10px;">
          <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">MODIFICAR PROVEEDOR</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td height="25" class="textosParaInputs" align="right">NOMBRE (RAZON SOCIAL): </td>
              <td align="left"><input type="text" name="nombreModificar" id="nombreModificar" maxlength="50" size="30" onkeyup="this.value = this.value.toUpperCase();" /></td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">RFC:</td>
    <td align="left"><input name="rfcModificar" type="text" id="rfcModificar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CURP:</td>
    <td align="left"><input name="curpModificar" type="text" id="curpModificar" size="30" maxlength="30" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">DIRECCION:</td>
    <td align="left"><input name="direccionModificar" type="text" id="direccionModificar" size="40" maxlength="40" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">COLONIA:</td>
    <td align="left"><input name="coloniaModificar" type="text" id="coloniaModificar" size="25" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">CIUDAD:</td>
    <td align="left"><input name="ciudadModificar" type="text" id="ciudadModificar" size="25" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /> <span class="textosParaInputs"> ESTADO: </span><input name="estadoModificar" type="text" id="estadoModificar" size="25" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">C.P.:</td>
    <td align="left"><input name="cpModificar" type="text" id="cpModificar" size="10" maxlength="5" /> <span class="textosParaInputs"> TELEFONO: </span><input name="telefonoModificar" type="text" id="telefonoModificar" size="20" maxlength="10" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE CONTACTO:</td>
    <td align="left"><input name="nombreContactoModificar" type="text" id="nombreContactoModificar" size="40" maxlength="35" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">TELEFONO CONTACTO:</td>
    <td align="left"><input name="telefonoContactoModificar" type="text" id="telefonoContactoModificar" size="25" maxlength="20" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">EMAIL CONTACTO:</td>
    <td align="left"><input name="emailContactoModificar" type="text" id="emailContactoModificar" size="40" maxlength="35" /></td>
  </tr>
    <tr>
      <td colspan="2" align="center"><div id="modificarDHGuardar" style="display:none"><input name="cancelarMod" type="button" value="Cancelar" class="botones" onclick="javascript: cancelarModificacionesProveedor();" /> &nbsp;&nbsp;&nbsp; <input name="guardarMod" type="button" value="Guardar Modificaciones" class="botones" onclick="javascript: guardarModificacionesProveedor();" /> </div>
        </td>
      </tr>

    <tr>
      <td colspan="2" align="center"></td>
    </tr>
  </table>
        </div>
	</td>
</tr>
</table>
</form>

</body>
</html>
