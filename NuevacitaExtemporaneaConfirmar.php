<?php 
session_start();
include_once('lib/misFunciones.php');
date_default_timezone_set('America/Mexico_city');
$fecha=strtotime('+2 hours',time());
// echo $fecha;
$fechaYhora = date('d/m/Y - H:i', $fecha);
// echo '<br>'.$fechaYhora;
if($_SESSION['idUsuario'] > 0)
{
	if (!empty($_GET['estudio'])) { // la cita es para laboratorio
			
			$query_query = "INSERT INTO citas_extemporaneas VALUES (NULL,'" . $_SESSION['IdCon'] . "','" . $_SESSION['idServ'] . "','" . $_SESSION['idDr'] . "','" . $_GET['id_derecho'] . "','" . quitarPuntosHora($_GET['hora_inicio']) . "','" . quitarPuntosHora($_GET['hora_fin']) . "','" . $_GET['tipo_cita'] . "','" . $_GET['fechaCita'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',1);";
			$res = ejecutarSQL($query_query);
			$log = ejecutarLOG("cita lab extemporanea nueva | id_con: " . $_SESSION['IdCon'] . "  | id_ser: " . $_SESSION['idServ'] . "  | id_med: " . $_SESSION['idDr'] . " | dh: " . $_GET['id_derecho'] . " | fecha: " . $_GET['fechaCita'] . " | status: 1 | observaciones: " . $_GET['obs'] . " | hora_inicio: " . quitarPuntosHora($_GET['hora_inicio']) . " | hora_fin: " . quitarPuntosHora($_GET['hora_fin']));
           if ($res[0] == 0) {// no hay error
           	
				$cita = getCitaERecienAgregada($_SESSION['IdCon'], $_SESSION['idServ'], $_SESSION['idDr'], $_GET['id_derecho'], $_GET['fechaCita'], quitarPuntosHora($_GET['hora_inicio']), quitarPuntosHora($_GET['hora_fin']));
				if ($_GET['tipo_cita'] == 0) { // si el tipo de cita es 1ra vez
					echo 5;
					// si es 1ra vez agregar campo diagnostico N = normal E = extraordinaria
					$query_query = "INSERT INTO citas_detalles VALUES ('" . $cita['id_cita'] . "','E','" . $_GET['diagnostico'] . "','" . $_GET['concertada'] . "','');";
					$res = ejecutarSQL($query_query);
				}
			
				$datosHorario = getHorarioXid($_GET['idHorario']);
				if ($_GET['estudio'] != '') {
					echo "7";
	    			$query_queryLab = "INSERT INTO laboratorio_citas VALUES (NULL,'" . $_SESSION['IdCon'] . "','" . $_SESSION['idServ'] . "','" . $_SESSION['idDr'] . "','" . $_GET['id_derecho'] . "','" . quitarPuntosHora($_GET['hora_inicio']) . "','" . quitarPuntosHora($_GET['hora_fin']) . "','" . $_GET['tipo_cita'] . "','" . $_GET['fechaCita'] . "','1','E-" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',1,'" . $cita['id_cita'] . "','" . $_GET['estudio'] . "','" . $_GET['servEstudio'] . "');";
		           $res = ejecutarSQL($query_queryLab);
				}
			
				print($_SESSION['IdCon'] . "|" . $_SESSION['idServ'] . "|" . $_GET['id_derecho'] . "|" . $_GET['fechaCita'] . "|" . $_SESSION['idUsuario'] . "|" . $_GET['hora_inicio'] . "|" . $_GET['hora_fin'] . "|" . $contraReferencia);
			}
		}
		else
		{
			$tipo_cita=$_GET['tipo_cita'];
			if($tipo_cita==0){
				$citas=existeCitaPrevia($_GET['id_derecho'],$_SESSION['idServ'],$_GET['fechaCita'],$tipo_cita);
				if($citas)
				{
					echo "alert('Ya cuenta con cita de primera Vez, agende otro tipo de cita');";
					exit();
				}

			}
				$duplica = existeCita($_GET['id_derecho'],$_SESSION['idServ'],$_GET['fechaCita']);
				if(!$duplica)
				{
					$referencia=registroCitaServicioDer($_GET['id_derecho'],$_SESSION['idServ']);
					if(empty($referencia))
					{
						$tipo_cita=$_GET['tipo_cita'];
							switch ($tipo_cita) {
								case 0: 
								
									$sql="insert into referencia_contrarreferencia (id_derecho,id_servicio) values (".$_GET['id_derecho'].",".$_SESSION['idServ'].");";
									$ref=ejecutarSQL($sql);
									break;
								default:
									echo "alert('No cuenta con cita de primera Vez, agende de primera vez');";
						 			exit();
									break;
							}
					}
					else
					{
						if($referencia['cita_primera']==0)
							{
								$sql="delete from referencia_contrarreferencia where id_referencia=".$ref['id_referencia'];
								$query=ejecutarSQL($sql);
								$citas=contarCitas_sub($_GET['id_derecho'],$_SESSION['idServ'],$_GET['fechaCita']);
								$sql="insert into referencia_contrarreferencia (id_derecho,id_servicio,citas_subsecuentes) values(".$_GET['id_derecho'].",".$_SESSION['idServ'].",".$citas.")";
								$query=ejecutarSQL($sql);
							}
							else
							{
								$citas=$referencia['citas_subsecuentes']+1;
								$sql="UPDATE referencia_contrarreferencia set citas_subsecuentes=".$citas." where id_referencia=".$ref['id_referencia'];
								$query=ejecutarSQL($sql);
							}
					}
				$query_query = "INSERT INTO citas_extemporaneas VALUES (NULL,'" . $_SESSION['IdCon'] . "','" . $_SESSION['idServ'] . "','" . $_SESSION['idDr'] . "','" . $_GET['id_derecho'] . "','" . quitarPuntosHora($_GET['hora_inicio']) . "','" . quitarPuntosHora($_GET['hora_fin']) . "','" . $_GET['tipo_cita'] . "','" . $_GET['fechaCita'] . "','1','" . $_GET['obs'] . "','" . $_SESSION['idUsuario'] . "','" . $fechaYhora . "',1);";
				$res = ejecutarSQL($query_query);
				$log = ejecutarLOG("cita extemporanea nueva | id_con: " . $_SESSION['IdCon'] . "  | id_ser: " . $_SESSION['idServ'] . "  | id_med: " . $_SESSION['idDr'] . " | dh: " . $_GET['id_derecho'] . " | fecha: " . $_GET['fechaCita'] . " | status: 1 | observaciones: " . $_GET['obs'] . " | hora_inicio: " . quitarPuntosHora($_GET['hora_inicio']) . " | hora_fin: " . quitarPuntosHora($_GET['hora_fin']));
				if ($res[0] == 0) {// no hay error
					$contraReferencia = "no";
					$cita = getCitaERecienAgregada($_SESSION['IdCon'], $_SESSION['idServ'], $_SESSION['idDr'], $_GET['id_derecho'], $_GET['fechaCita'], quitarPuntosHora($_GET['hora_inicio']), quitarPuntosHora($_GET['hora_fin']));
					if ($_GET['tipo_cita'] == 0) { // si el tipo de cita es 1ra vez
						echo 17;
						// si es 1ra vez agregar campo diagnostico N = normal E = extraordinaria
						$query_query = "INSERT INTO citas_detalles VALUES ('" . $cita['id_cita'] . "','E','" . $_GET['diagnostico'] . "','" . $_GET['concertada'] . "','');";
						$res = ejecutarSQL($query_query);
					}
	
					print($_SESSION['IdCon'] . "|" . $_SESSION['idServ'] . "|" . $_GET['id_derecho'] . "|" . $_GET['fechaCita'] . "|" . $_SESSION['idUsuario'] . "|" . $_GET['hora_inicio'] . "|" . $_GET['hora_fin'] . "|" . $contraReferencia);
				}
				else
				{
					echo 'alert("Error, intente nuevamente o contacte a soporte Técnico");';
				}
			}
			else
			{
				print("alert('El derechohabiente ya cuenta con cita en el dia');");	
			}
			
			
	}
}
else
{
	$_SESSION['idUsuario'] = "-1";
    $_SESSION['tipoUsuario'] = "-1";
    $_SESSION['IdCon'] = "-1";
    $_SESSION['idServ'] = "-1";
    $_SESSION['idDr'] = "-1";
    echo "<script  language=\"javascript\" type=\"text/javascript\">alert('Tu sesion ha caducado'); location.replace('index.php');</script>";
}
 ?>

