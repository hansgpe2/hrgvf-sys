<?php

require_once'lib/fpdf.php';
include 'lib/misFunciones.php';

class Expediente extends FPDF {

    var $id;

    function Header() {
        $this->Image('diseno/logo_issste.jpg', 0.1, 0.5, 10, 1.6);
        $this->SetXY(17,1.6);
        $this->SetFont('times', 'B', 10);
        $this->Cell(2,0.5,ponerCeros($this->id, 8),1,0);
        $this->Ln(0.5);
        $this->SetFont('times', 'B', 10);
        $this->Cell(20, 1, utf8_decode("Hospital Regional \" Valentín Gomez Farías\" "), "T", 0, "C");
        $this->Ln();
        $this->Cell(20, 1, utf8_decode("Licencia Sanitaria No 00000360-A"), 0, 0, "C");
        $this->Ln();
    }

    function Expediente($idExp) {
        $this->FPDF("P", "cm", "Legal");
        $this->id = $idExp;
    }

}

$id = $_REQUEST['id_derecho'];
$derecho= getDatosDerecho($id);
$expediente=  getExpedienteXid($id);
$pdf = new Expediente($id);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(1, 0.5, utf8_decode("Hoja clínica"));
$pdf->Ln(1);
$pdf->Cell(2, 0.5, "NOMBRE:");
$pdf->SetFont('times', '', 9);
$pdf->Cell(15, 0.5, ponerAcentosPdf($derecho['ap_p'] . "       " . $derecho['ap_m'] . "         " . $derecho['nombres']));
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2,0.5,"CEDULA");
$pdf->SetFont('times', '', 9);
$pdf->Cell(3,0.5,$derecho['cedula']."/".$derecho['cedula_tipo']);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(7,0.5,"SERVICIO:______________________________");
$pdf->Cell(5,0.5,"CAMA:___________");
$pdf->Ln(1);
$pdf->Cell(20,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Antecedentes Hereditarios","L");
$pdf->Cell(15,0.5," ",1);
for($ren=0;$ren<9;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Antecedentes Personales"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("No psicologicos"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<9;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Antecedentes psicológicos"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<11;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Padecimiento Actual"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<15;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5," ","LB");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Interrogatorio"),"LT");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Por Aparatos y Sistemas"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  "","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Digestivo"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<11;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Respiratorio"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Circulatorio"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Urinario"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Genital"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Nervioso"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Organos de los Sentidos"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Sistemas Generales"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Terapeutica Empleada"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode(""),"LB");
$pdf->Cell(15,0.5," ",1);
$pdf->AddPage();
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(1, 0.5, utf8_decode("Hoja clínica"));
$pdf->Ln(1);
$pdf->Cell(2, 0.5, "NOMBRE:");
$pdf->SetFont('times', '', 9);
$pdf->Cell(15, 0.5, utf8_decode($derecho['ap_p'] . "       " . $derecho['ap_m'] . "         " . $derecho['nombres']));
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2,0.5,"CEDULA");
$pdf->SetFont('times', '', 9);
$pdf->Cell(3,0.5,$derecho['cedula']."/".$derecho['cedula_tipo']);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(7,0.5,"SERVICIO:______________________________");
$pdf->Cell(5,0.5,"CAMA:___________");
$pdf->Ln(1);
$pdf->Cell(5,0.5,  utf8_decode("Exploración Física"),"TL");
$pdf->Cell(15,0.5,"Habitos Exterior",1,0,"C");
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Talla"),"TL");
$pdf->Cell(15,0.5,"Peso Actual                           Peso Ideal                            ",1,0,"C");
$pdf->Ln();
$pdf->Cell(5,0.5,"Pulso","L");
$pdf->Cell(15,0.5,"T/A                  Respiraciones                       Temperatura               ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Inspección general"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<3;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Cabeza","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Cuello","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<3;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Tórax"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Abdomen","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Tracto Rectal y vaginal","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Extremidades","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Impresión Diagnostica"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<7;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5, " ","LB");
$pdf->Cell(15,0.5," ",1);
$pdf->Output();
?>
