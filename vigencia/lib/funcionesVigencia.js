function mostrarVentana(div,liga,user)
{
    $(div).load(liga, user);
}

function borrarReferencias(obj)
{
    if(obj.value="Cedula del DerechoHabiente" || obj.value=="Apellido Paterno" || obj.value=="Apellido Materno" || obj.value=="Nombre(s)" || obj.value=="DD/MM/AAAA")
        obj.value="";
}

function AgregarDH()
{
    $(".error").hide();
    data="cedula="+$("#cedulaAgregar").val()+"&tpoced="+$("#cedulaTipoAgregar").val()+"&app="+$("#ap_pAgregar").val()+"&apm="+$("#ap_mAgregar").val()+"&nombre="+$("#nombreAgregar").val()+"&fecnac="+$("#fecha_nacAgregar").val()+"&dir="+$("#direccionAgregar").val()+"&edo="+$("#estadoAgregar").val()+"&mun="+$("#municipioAgregar").val();
    ajax=$.ajax({
        url:"nuevoDHVALyAdd.php",
        data:data,
        type:'get'
    });
    mostrarVentana('contenido',ajax);    
}

function fechas(elEvento,obj){
    var evento = elEvento || window.event;
    if(evento.keyCode == 8){
    } else {
        var fecha = obj;
        if(fecha.value.length == 2 || fecha.value.length == 5){
            fecha.value += "/";
        }
    }
}

function validarAgregarDH()
{
    if(!$("#cedulaAgregar").val().match(/[A-Za-z]{4}[0-9]{6}/) && $("#cedulaAgregar").val()=="")
    {
        if($("#cedulaAgregar").val()=="")
            alert("introduzca la cedula del derechoHabiente");
        else
            alert("Introduzca una cedula correcta");
        return false;
    }
    else
    if($("#ap_pAgregar").val()=="" || $("#ap_pAgregar").val()=="Apellido Paterno") 
    {
        alert("introduzca el apellido Paterno");
        return false;
    }
    if($("#ap_mAgregar").val()==""|| $("#ap_pAgregar").val()=="Apellido Materno" ) 
    {
        alert("introduzca el apellido Materno");
        return false;
    }
    else
    if($("#nombreAgregar").val()==""|| $("#nombreAgregar").val()=="Apellido Materno" ) 
    {
        alert("introduzca el nombre del DerechoHabiente");
        return false;
    }
    else
    if($("#fecha_nacAgregar").val()==""|| $("#fecha_nacAgregar").val().match(/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/))
    {
        if($("#fecha_nacAgregar").val()=="")
            alert("Introduzca la fecha de Nacimiento");
        else
            alert("Introduzca una fecha correcta");
        return false;
    }
    if($("#telefonoAgregar").val()==""|| $("#telefonoAgregar").val().match(/^(01|[0-9]{2,3})[0-9]{8,7}/))
    {
        if($("#telefonoAgregar").val()=="")
            alert("introduzca un numero de telefono");
        else
            alert("Introduzca el numero de telefono con formato 01(lada)numero \n o (lada)telefono");
        return false;
    } 
    else
    if($("#direccionAgregar").val()==""){
        alert("introduzca la direccion del DerechoHabiente");
        return false;
    }
    else
    if($("#estadoAgregar option:selected").val()=="")
    {
        alert("introduzca la direccion del DerechoHabiente");
        return false;
    }
    else
    if($("#municipioAgregar option:selected").val()=="")
    {
        alert("introduzca la direccion del DerechoHabiente");
        return false;
    }
    else{
        AgregarDH();
        return true;
     
    }
}