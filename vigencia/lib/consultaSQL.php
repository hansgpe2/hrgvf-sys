<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of consultaSQL
 *
 * @author hans
 */
class consultaSQL {
    function __construct() {
   }
   
   public function select_order($tabla,$campos=array(),$orden="ASC") // tabla //campos->arreglos de campos //$orden ASC o DESC
   {
       $camposStr=  implode(",", $campos);
       $sql="select * from $tabla order by $camposStr $orden";
       return $sql;
   }
   
  public function select($tabla)
   {
       $sql="select * from $tabla";
       return $sql;
   }
   
   public function merge_array($arreglo)
   {
       $keys=  array_keys($arreglo);
       $valores=  array_values($arreglo);
       $result=array();
       $i=0;
       foreach ($arreglo as $key => $value) {
           $result[]=$keys[$i].'="'.$valores[$i].'"';
           $i++;
       }
       return $result;
   }

      public function select_where($tabla,$campos)
   {
       $camposStr=  implode(",", $this->merge_array($campos));
       $sql="select * from $tabla where $camposStr";
       return $sql;
   }
   
   public function select_campos($tabla,$campos)
   {
       $camposStr=  implode(",", $campos);
       $sql="select $camposStr from $tabla";
       return $sql;
   }
   
   public function select_campos_where($tabla,$campos=array(),$where=array())
   {
       //$camposFx=  array_map('merge_array', array_keys($campos),  array_values($campos));
       $aux=implode(",", $campos);
       $camposStr= substr($aux,0,  (strlen($aux)));
    $aux=  implode(",", $this->merge_array($where));
    $whereStr= substr($aux,0,  (strlen($aux)));
       
       $sql="select $camposStr from $tabla where $whereStr";
       return $sql;
   }
   
   public function select_campos_where_ord($tabla,$campos,$where,$camposOrder,$order="ASC") {
       $camposStr=  implode(",", $campos);
       $whereStr=  implode(",", $this->merge_array($where));
       $camposOrderStr=  implode(",", $camposOrder);
       $sql="select $camposStr from $tabla where $whereStr order by $camposOrderStr $order";
       return $sql;
   }
   
   public function select_order_groupby($tabla,$campos=array(),$camposG=array(),$orden="ASC") // tabla //campos->arreglos de campos //$orden ASC o DESC
   {
       $camposStr=  implode(",", $campos);
       $camposGStr= implode(",", $camposG);
       $sql="select * from $tabla group by $camposGStr order by $camposStr $orden ";
       return $sql;
   }
   
   public function select_campos_where_ord_group($tabla,$campos,$where,$camposOrder=array(),$order="ASC",$camposG=array()) {
       $camposFx=  array_map('merge_array', array_keys($campos),  array_values($campos));
       $camposStr=  implode(",", $campos);
       $whereStr=  implode(",", $this->merge_array($where));
       $camposOrderStr=  implode(",", $camposOrder);
       $camposGStr= implode(",", $camposG);
       $sql="select $camposStr from $tabla where $whereStr order by $camposOrderStr $order group by $camposGStr";
       return $sql;
   }
}

