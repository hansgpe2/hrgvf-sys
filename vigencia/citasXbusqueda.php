<?php

include_once('lib/misFunciones.php');

$citas = citasXderechohabiente($_GET['id_derecho']);
$tCitas = count($citas);
$out = "";
$out.= "<br><table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
if ($tCitas > 0) {
    for ($i = 0; $i < $tCitas; $i++) {
        $horario = getHorarioXid($citas[$i]['id_horario']);
        $datosCita = citasOcupadasXdia($citas[$i]['id_horario'], $citas[$i]['fecha_cita']);
        $datosDerecho = getDatosDerecho($_GET['id_derecho']);
        $datosMedico = getMedicoXid($horario['id_medico']);
        $datosServicio = getServicioXid($horario['id_servicio']);
        $nombreMedico = strtoupper($datosMedico['titulo'] . " " . $datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
        $nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
        switch ($datosCita['tipo_usuario']) {
            case 2:
                $datosCitaMedico = citaMedico($datosCita['id_cita']);
                $datosUsuario = getMedicoXid($datosCitaMedico['id_medico']);
                $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . ponerAcentos($datosUsuario['titulo'] . " " . $datosUsuario['ap_p'] . " " . $datosUsuario['ap_m'] . " " . $datosUsuario['nombres']) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
                break;
            case 3:
                $datosUsuario = getUsuarioReferencia($datosCita['id_usuario']);
                $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
                break;
            default:
                $datosUsuario = getUsuarioXid($datosCita['id_usuario']);
                $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $datosCita['extra1'] . " - " . $datosCita['observaciones'];
        }
        $datosCita = $datosServicio['nombre'] . " - " . $nombreMedico;
        if ($horario['tipo_cita'] == 0)
            $claseParaDia = "citaXdiaPRV";
        if ($horario['tipo_cita'] == 1)
            $claseParaDia = "citaXdiaSUB";
        if ($horario['tipo_cita'] == 2)
            $claseParaDia = "citaXdiaPRO";
        $fecha = formatoDia($citas[$i]['fecha_cita'], "imprimirCita");
        $out.="
				  <tr>
					<td class=\"" . $claseParaDia . "\">
						<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td width=\"180\" class=\"citaXdiaHora\">" . $fecha . " - " . formatoHora($horario['hora_inicio']) . " - " . formatoHora($horario['hora_fin']) . "</td>
							<td align=\"left\" width=\"520\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
						  </tr>
						  <tr>
							<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($datos) . "</td>
						  </tr>
						  <tr>
						  	<td>&nbsp;</td>
							<td align=\"left\" class=\"citaXdiaInfo\">" . ponerAcentos($datosCita) . "</td>
						  </tr>
						</table>
					</td>
				  </tr>";
    }
} else {
    $out .= "
				<tr>
					<td align=\"center\">
						NO EXISTEN CITAS DEL DERECHOHABIENTE
					</td>
				  </tr>	";
}

$out.= "</table>";
$out.="<p align=\"center\"><table width=\"500\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:10px;\">
<tr>
<td width=\"35\" height=\"25\" class=\"citaXdiaPRV\"></td><td width=\"129\">Citas Primera Vez</td>
<td width=\"35\" class=\"citaXdiaSUB\"></td><td width=\"138\">Citas Subsecuentes</td>
<td width=\"35\" class=\"citaXdiaPRO\"></td><td width=\"138\">Citas Procedimientos</td>
</tr>
</table></p>";
//	print($out);



$citas = citasExtXderechohabiente($_GET['id_derecho']);
$tCitas = count($citas);
//	$out = "";
$out.= "<br><table width=\"600\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">";
if ($tCitas > 0) {
    for ($i = 0; $i < $tCitas; $i++) {
//			$horario = getHorarioXid($citas[$i]['id_horario']);
//			$datosCita = citasOcupadasXdia($citas[$i]['id_horario'],$citas[$i]['fecha_cita']);
        $datosDerecho = getDatosDerecho($_GET['id_derecho']);
        $datosMedico = getMedicoXid($citas[$i]['id_medico']);
        $datosServicio = getServicioXid($citas[$i]['id_servicio']);
        $nombreMedico = strtoupper($datosMedico['titulo'] . " " . $datosMedico['ap_p'] . " " . $datosMedico['ap_m'] . " " . $datosMedico['nombres']);
        $nombre = strtoupper($datosDerecho['ap_p'] . " " . $datosDerecho['ap_m'] . " " . $datosDerecho['nombres']);
        $datosUsuario = getUsuarioXid($citas[$i]['id_usuario']);
        $datosCitaMedico = citaMedico($citas[$i]['id_cita']);
        $totalCitas2 = count($datosCitaMedico);
        if ($totalCitas2 > 0)
            $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . ponerAcentos($nombreMedico) . "... " . $citas[$i]['extra1'] . " - " . $citas[$i]['observaciones'];
        else
            $datos = $datosDerecho['cedula'] . "/" . $datosDerecho['cedula_tipo'] . " - " . $datosDerecho['telefono'] . " - " . substr($datosUsuario['nombre'], 0, 12) . "... " . $citas[$i]['extra1'] . " - " . $citas[$i]['observaciones'];
        $datosCita = $datosServicio['nombre'] . " - " . $nombreMedico;
        if ($horario['tipo_cita'] == 0)
            $claseParaDia = "citaXdiaPRV";
        if ($horario['tipo_cita'] == 1)
            $claseParaDia = "citaXdiaSUB";
        if ($horario['tipo_cita'] == 2)
            $claseParaDia = "citaXdiaPRO";
        $fecha = formatoDia($citas[$i]['fecha_cita'], "imprimirCita");
        $out.="
				  <tr>
					<td class=\"" . $claseParaDia . "\">
						<table width=\"700\" height=\"40\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						  <tr>
							<td width=\"180\" class=\"citaXdiaHora\">" . $fecha . " - " . formatoHora($horario['hora_inicio']) . " - " . formatoHora($horario['hora_fin']) . "</td>
							<td align=\"left\" width=\"520\" class=\"citaXdiaNombre\">" . ponerAcentos($nombre) . "</td>
						  </tr>
						  <tr>
							<td align=\"left\" class=\"citaXdiaInfo\" colspan=\"2\">" . ponerAcentos($datos) . "</td>
						  </tr>
						  <tr>
						  	<td>&nbsp;</td>
							<td align=\"left\" class=\"citaXdiaInfo\">" . ponerAcentos($datosCita) . "</td>
						  </tr>
						</table>
					</td>
				  </tr>";
    }
} else {
    $out .= "
				<tr>
					<td align=\"center\">
						NO EXISTEN CITAS DEL DERECHOHABIENTE
					</td>
				  </tr>	";
}

$out.= "</table>";
$out.="<p align=\"center\"><table width=\"500\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:10px;\">
<tr>
<td width=\"35\" height=\"25\" class=\"citaXdiaPRV\"></td><td width=\"129\">Citas Primera Vez</td>
<td width=\"35\" class=\"citaXdiaSUB\"></td><td width=\"138\">Citas Subsecuentes</td>
<td width=\"35\" class=\"citaXdiaPRO\"></td><td width=\"138\">Citas Procedimientos</td>
</tr>
</table></p>";
print($out);
?>
