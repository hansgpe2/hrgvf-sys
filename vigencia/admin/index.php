<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<script src="../SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="../SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="../lib/misEstilos.css" rel="stylesheet" />
<script src="../lib/jquery-1.7.1.js"></script>
<script src="lib/funciones.js"></script>
</head>

<body onUnload="salir();">
<div align="center">
<table class="ventana" border="0">
<tr><td>
<header>
<table border="0" class="tituloVentana">
<tr>
<td rowspan="2"><img src="../diseno/logoEncabezado.png" /></td><td class="tituloEncabezado">Control de Vigencia</td></tr>
<tr>
<td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado</td></tr></table>
</header></td></tr>
<tr><td>
<menu>
<ul id="MenuBar1" class="MenuBarHorizontal">
  <li><a class="MenuBarItemSubmenu" href="#">Clinicas</a>
    <ul>
      <li><a href="javascript:nuevaClinica()">Alta</a></li>
      <li><a href="#">Consulta</a></li>
      <li><a href="#">Bajas</a></li>
      <li><a href="#">Modificar</a></li>
</ul>
  </li>
  <li><a href="#" class="MenuBarItemSubmenu">Usuarios</a>
    <ul>
      <li><a href="#">Alta</a></li>
      <li><a href="#">Baja</a></li>
      <li><a href="#">Modificar</a></li>
    </ul>
  </li>
  <li><a href="../index.php">Salir</a>  </li>
</ul>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"../SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../SpryAssets/SpryMenuBarRightHover.gif"});
</script></menu></td></tr>
<tr><td>
<nav id="contenidoAdmin"></nav>
</td></tr>
</table>
</div>
</body>
</html>