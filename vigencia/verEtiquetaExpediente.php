<?php include 'lib/misFunciones.php'; 
$datos=getDatosDerecho($_GET['id_derecho']);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="lib/impresionExp.css" />
<script src="lib/arreglos.js"></script>
<style type="text/css">
.cedula {
	font-size: 48px;
}
.nombre {
	font-size: 24px;
	text-align:center;
	font-weight: bold;
}
.Etiqueta_expediente tr .Encabezado_Etiqueta {
	text-align: center;
}
.Etiqueta_expediente tr .Etiqueta_nombre .nombres {
	text-align: left;
	font-size: 30px;
}
</style>
</head>

    <body onload="rotar(codbar1,270);rotar(codbar2,90);">
<table width="904" class='Etiqueta_expediente'>
  <tr>
    <td rowspan='3' width='90'><img id="codbar1" src='/barcode/barcode.php?code=<?php echo $_GET['id_derecho']; ?>&tam=2' alt="" /></td>
    <td class='cedula' colspan='3'><?php echo $datos['cedula']; ?>/<?php echo $datos['cedula_tipo']; ?></span></td>
    <td rowspan='3' width='60'><img id="codbar2" src='/barcode/barcode.php?code=<?php echo $_GET['id_derecho']; ?>&tam=2' alt="" /></td>
    <td width="120"><img src='diseno/logo05.png' alt="" width='87' height='85'  /></td>
  </tr>
  <tr>
    <td class='Encabezado_Etiqueta' colspan='3'>Numero de Expediente</td>
  </tr>
  <tr>
    <td width="141" class='Etiqueta_nombre'><span class="nombre"><?php echo ponerAcentos($datos['ap_p']); ?>&nbsp;</span></td>
    <td width="151" class='Etiqueta_nombre'><span class="nombre"><?php echo ponerAcentos($datos['ap_m']); ?>&nbsp;</span></td>
    <td width="314" class='Etiqueta_nombre'><span class="nombre"><?php echo ponerAcentos($datos['nombres']); ?></span></td>
  </tr>
  <tr>
	<td>&nbsp;</td>  
    <td class='Encabezado_Etiqueta'>Apellido Paterno</td>
    <td class='Encabezado_Etiqueta'>Apellido Materno</td>
    <td class='Encabezado_Etiqueta'>Nombre(s)</td>
  </tr>
</table>
</body>
</html>