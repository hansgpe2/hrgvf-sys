﻿<?php
error_reporting(E_ALL^E_NOTICE);
include('lib/misFunciones.php');
 $tpoCedHom=array("10","40","41","50","51","70","71","90");
    $tpoDerHom=array("Trabajador","Esposo","Concubino","Padre","Abuelo","Hijo","Hijo de conyuge","Pensionado");
    $tpoCedMuj=array("20","30","31","32","60","61","80","81","91");
    $tpoDerMuj=array("Trabajadora","Esposa","Concubina","Mujer","Madre","Abuela","Hija","Hija de conyuge","Pensionada");
    $tpoCedGen=Array("92","99");
    $tpoDerGen=Array("Familiar de pensionado","No derechohabiente");
    $tpoCed=  array_merge($tpoCedHom,$tpoCedMuj,$tpoCedGen);
    $tpoDer=  array_merge($tpoDerHom,$tpoDerMuj,$tpoDerGen);
$idderecho=$_REQUEST['idDerecho'];
$derecho=getDatosDerecho($idderecho);
$direccion=explode(",",$derecho['direccion']);
$cp= $derecho['Codigo_Postal'] ;
$fechaNacimiento=explode("/",$derecho['fecha_nacimiento']);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
    <form name="agregarDH" id="agregarDH" action="javascript:validarModificarDH();" method="get">
    <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">MODIFICAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><input name="idDerecho" type="hidden" id="idDerecho" value="<?php echo $derecho['id_derecho']; ?>" /></td>
            </tr>
            <tr>
                <td height="25" align="right"><label for="cedulaAgregar" class="error"><span class="textosParaInputs">CEDULA:</span></label> </td>
              <td align="left"><input type="text" name="cedulaAgregar" id="cedulaAgregar" maxlength="10" onBlur="this.value = this.value.toUpperCase();if(this.value==''){this.value='Cedula del DerechoHabiente';}" onfocus="if(this.value=='Cedula del DerechoHabiente'){this.value=''}" value="<?php echo $derecho['cedula']; ?>"/>
                </label>
              </td>
                <tr><td  align="right" class="textosParaInputs">SEXO</td><td><select name="sexo" id="sexo" onchange="ObtenerTpoDerXSexo(this.id,'cedulaTipoAgregar')">
              <option value="-1"<?php if($derecho['sexo']==-1) echo " selected"; ?>></option>
              <option value="M"<?php if($derecho['sexo']==0) echo " selected"; ?>>Mujer</option>
              <option value="H"<?php if($derecho['sexo']==1) echo " selected"; ?>>Hombre</option>
              </select>
                        <input name="valorSexo" type="hidden" id="valorSexo" value="<?php if($derecho['sexo']==0) echo "M"; else echo "H"; ?>">
                        &nbsp;<span class="textosParaInputs">TIPO DE CEDULA</span>&nbsp;<select name="cedulaTipoAgregar" id="cedulaTipoAgregar">
                          <?php 
                          if($derecho['sexo']==0){
                              $tpoCed=$tpoCedMuj;
                              $tpoDer=$tpoDerMuj;
                          }
                          else
                          {
                              $tpoCed=$tpoCedHom;
                              $tpoDer=$tpoDerHom;
                          }
                          echo '<option value="-1"></option>';
                          for($i=0;$i<count($tpoCed);$i++)
                          {
                              if($derecho['cedula_tipo']==$tpoCed[$i]){
                                  echo '<option value="'.$tpoCed[$i].'" selected>'.$tpoCed[$i]." ".$tpoDer[$i]."</option>";
                                  $band=1;
                              }
                              else
                                  echo '<option value="'.$tpoCed[$i].'">'.$tpoCed[$i]." ".$tpoDer[$i]."</option>";
                          }
                          for($i=0;$i<count($tpoCedGen);$i++)
                          {
                              if(($derecho['cedula_tipo']==$tpoCedGen[$i]) && $band==0)
                                  echo '<option value="'.$tpoCedGen[$i].'" selected>'.$tpoCedGen[$i]." ".$tpoDerGen[$i]."</option>";
                              else
                                  echo '<option value="'.$tpoCedGen[$i].'">'.$tpoCedGen[$i]." ".$tpoDerGen[$i]."</option>";
                          }
                          ?>
                        </select>
                    </td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="center"
     colspan="2">NOMBRE DEL DERECHOHABIENTE</td></tr>
    <tr>
      <td height="25" class="textosParaInputs" align="right">APELLIDOS PATERNO</td>
    <td align="left"><input name="ap_pAgregar" type="text" id="ap_pAgregar" onBlur="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($derecho['ap_p']); ?>" size="20" maxlength="50" /><label for="ap_pAgregar" class="error"><span class="textosParaInputs">MATERNO</span></label>
        <input name="ap_mAgregar" type="text" id="ap_mAgregar" onfocus="if(this.value=='Apellido Materno'){this.value=''}" onBlur="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($derecho['ap_m']); ?>" size="20" maxlength="50" /><label for="ap_mAgregar" class="error"></label></td></tr>
        <tr><td height="25" class="textosParaInputs" align="right">NOMBRE(S)</td><td align="left">
      <input name="nombreAgregar" type="text" id="nombreAgregar" onfocus="if(this.value=='Nombre(s)'){this.value='';}" onblur="if(this.value==''){this.value='Nombre(s)';}this.value = this.value.toUpperCase();" onKeyUp="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($derecho['nombres']); ?>" size="50" maxlength="50" /><label for="nombreAgregar" class="error"></label></td>
  </tr>
  <tr>
      <td height="25" align="right"><label for="telefonoAgregar" class="error"><span class="textosParaInputs">TELEFONO:</span></label></td>
      <td align="left"><input name="telefonoAgregar" type="tel" id="telefonoAgregar" size="20" maxlength="10" value="<?php echo $derecho['telefono']; ?>" />
        <label for="fecha_nacAgregar" class="error"><span class="textosParaInputs"> F. NAC. </span></label>
        <span style="position:relative">
        <label for="mes1">m</label>
        <select name="mes1" id="mes1" tabindex="8" onchange="calendarios('1');">
          <?php
	if(count($fechaNacimiento)==3)	  
          $mes=$fechaNacimiento[1];
        else
            $mes=-1;
		  echo "<option value='-1'></option>";
		  $meses=array("ene","feb","mar","abril","may","jun","jul","ago","sept","oct","nov","dic");
		  for($i=1;$i<=12;$i++)
		  {
			  if($i<10){
			  if($mes==$i)
			  echo "<option value='0".$i."' selected>".$meses[$i-1]."</option>";
			  else
			  echo "<option value='0".$i."'>".$meses[$i-1]."</option>";
			  }
			  else
			  {
				  if($mes==$i)
			  echo "<option value='".$i."' selected>".$meses[$i-1]."</option>";
			  else
			  echo "<option value='".$i."'>".$meses[$i-1]."</option>";
			  }
		  }
        
		  ?>
        </select>
        <label for="dia1">d</label>
        <select name="dia1" id="dia1" tabindex="9">
          <?php
		 
		  $mes=$fechaNacimiento[1];
		  switch($mes)
    {
        case '01':
            $dias=31;
            break;
        case '02':
            $anio=$fechaNacimiento[2];
            if(($anio%4)==0)
                $dias=29;
            else
                $dias=28;
            break;
        case '03':
            $dias=30;
            break;
        case '04':
            $dias=30;
            break;
        case '05':
            $dias=31;
            break;
        case '06':
            $dias=30;
            break;
        case '07':
            $dias=31;
            break;
        case '08':
            $dias=31;
            break;
        case '09':
            $dias=30;
            break;
        case '10':
            $dias=31;
            break;
        case '11':
            $dias=30;
            break;
        case '12':
            $dias=31;
            break;			
    }
	for($i=1;$i<=$dias;$i++)
	{
		if($i==$fechaNacimiento[0])
		{
			if($i<10)
			echo "<option value='0".$i."' selected>$i</option>";
			else
			echo "<option value='$i' selected>$i</option>";
		}
		else
		if($i<10)
			echo "<option value='0".$i."'>$i</option>";
			else
			echo "<option value='$i'>$i</option>";
	}
		  ?>
          </select>
        <label for="anio1">a</label>
        <select name="anio1" id="anio1" tabindex="10">
          <?php
			$ini=date("Y");
			$fin=$ini-150;
                        if(count($fechaNacimiento)==3)
			$anioNac=$fechaNacimiento[2];
                        else
                            $anioNac=  date ("Y");
			for($i=$ini;$i>$fin;$i--)
			{
				if($i==$anioNac)
				echo "<option value='$i' selected>$i</option>";
				else
				echo "<option value='$i'>$i</option>";
			}
		?>
        </select>
      </span>
        <input name="fecha_nacAgregar" type="hidden" id="fecha_nacAgregar" value="<?php echo $derecho['fecha_nacimiento']; ?>"></td>
</td>
  </tr>
  <tr>
      <td height="25" class="textosParaInputs" align="right"><label for="direccionAgregar" class="error"><span class="textosParaInputs">DIRECCION:</span></label></td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" onkeyup="this.value = this.value.toUpperCase();" value="<?php echo ponerAcentos($direccion[0]); ?>" size="64" maxlength="50" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estadoAgregar" id="estadoAgregar" tabindex="14" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');">
      <script type="text/javascript">
                cargarEstados('estadoAgregar');
            </script>
    </select>
      <input name="valorEstado" type="hidden" id="valorEstado" value="<?php echo $derecho['estado']; ?>">
      <span class="textosParaInputs">MUNICIPIO: </span>
      <select name="municipioAgregar" id="municipioAgregar" tabindex="15" onchange="obtenerCodigoPostalMun('cp','estadoAgregar',this.id);cargarColonias('col','cp', 'municipioAgregar');">
      </select>
      <input name="valorMunicipio" type="hidden" id="valorMunicipio" value="<?php echo $derecho['municipio']; ?>"></td>
    </tr>
  <tr>
    <td class="textosParaInputs" align="right"> CODIGO POSTAL</td>
    <td><select name="cp" id="cp" tabindex="17" onChange="cargarColonias('col',this.id, 'municipioAgregar')"  value="0" >
      <?php echo CodigoPostalProc($derecho['estado'],$derecho['municipio'],$derecho['Codigo_Postal']); ?>
    </select>
      <input name="valColonia" type="hidden" id="valColonia" value="<?php echo $direccion[1]; ?>">
      <span class="textosParaInputs">COLONIA
      <select name="col" id="col" tabindex="16" onChange="obtenerCodigoPostal('cp','estadoAgregar','municipioAgregar',this.id);">
        <?php if(count($direccion)>1){
	echo ColoniaProcedencia($derecho['estado'],$derecho['municipio'],$direccion[1]);
    }
	else
        {
	echo ColoniaProcedencia($derecho['estado'],$derecho['municipio'],"-1");
        }
	 ?>
      </select>
      </span>
      <input name="valorCodigo" type="hidden" id="valorCodigo" value="<?php echo $derecho['Codigo_Postal']; ?>"></td>
      </tr>
  <tr>
    <td><span class="textosParaInputs">UNIDAD MEDICA DE ADSCRIPCIÓN:</span></td>
    <td align="left"><select name="Unidad" id="Unidad" tabindex="18">
    <?php echo UnidadMedicaProcedencia($derecho['estado'],$derecho['unidad_medica']); ?>
    </select>
      <input name="valorUmf" type="hidden" id="valorUmf" value="<?php echo $derecho['unidad_medica']; ?>"></td>
    </tr>
    <tr><td class="textosParaInputs" align="right">VIGENTE</td>
    <td><select id="vigente">
      <option value="-1" <?php if (!(strcmp(-1, $derecho['st']))) {echo "selected=\"selected\"";} ?>></option>
      <option value="1" <?php if (!(strcmp(1, $derecho['st']))) {echo "selected=\"selected\"";} ?>>Si</option>
      <option value="0" <?php if (!(strcmp(0, $derecho['st']))) {echo "selected=\"selected\"";} ?>>No</option></select></td></tr>
        <tr>
          <td class="textosParaInputs" align="right">E-MAIL:</td>
          <td align="left"><input name="emailAgregar" type="text" id="emailAgregar" size="30" tabindex="12" value="<?php echo $derecho['email']; ?>" />
            <span class="textosParaInputs">CELULAR</span>
          <input type="text" name="cel" value="<?php echo $derecho['celular']; ?>" maxlength="10" size="10" id="cel" tabindex="13" /></td>
        </tr>
        
            <tr>
              <td colspan="2" align="center"><div id="divBotones_EstadoAgregarDH">
                      <button id="enviar" type="button" class="botones" onclick="VerInformacion(<?php echo $idderecho; ?>)">Regresar</button>&nbsp;&nbsp;<input type="submit" id="guardar" value="Modificar" class="botones" />
              </div>
                </td>
              </tr>
        
            <tr>
              <td colspan="2" align="center"><div id="resultado"></div></td>
            </tr>
          </table></form>
</body>
</html>