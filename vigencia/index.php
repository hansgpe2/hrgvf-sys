<?php include'/lib/misFunciones.php';
session_start();
$_SESSION['idUsuario'] = "-1";
$_SESSION['tipoUsuario'] = "-1";
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Control de Vigencia de derechos</title>
<link href="lib/misEstilos.css" rel="stylesheet" />
<link href="lib/impresion.css" rel="stylesheet"  />
<link href="lib/impresionEtiquetas.css" rel="stylesheet"  />
<link href="lib/impresionExp.css" rel="stylesheet"  />
<script src="lib/jquery-1.7.1.js"></script>
<script src="lib/jquery.form.js"></script>
<script src="lib/arreglos.js"></script>
<style type="text/css">
    div.centro
{
    text-align: center;
    vertical-align: central;
    alignment-adjust: middle;
    position: relative;
    width: 100%;
}

div.centro table
{
    margin: auto;
    text-align: left;
}
</style>
</head>

<body bgcolor="#EEEEEE" onload="obtenerLogin();">
    <table style="width: 1024px;">
        <tbody class="ventana">
            <tr><td width="13%" style="width:25%;">&nbsp;&nbsp;&nbsp;</td><td width="82%" style="width:50% ">
        <table width="804" border="0" class="tablaPrincipal" id="tablaPrincipal">
            
  <tr>
    <td width="796"><table width="800" border="0" class="encabezado">
  <tr>
    <td width="104" height="74" rowspan="2"><img src="diseno/LOGO-ISSSTE.jpg" width="104" height="74" /></td>
    <td height="39" class="tituloEncabezado" valign="middle">Control de Vigencia</td>
  </tr>
  <tr>
    <td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
      <td height="70"><menu id="menu" style="display:none">
      <table width="100%" border="0" class="tablaPrincipal">
        <tr>
          <td width="150" valign="middle" align="center"></td>
          <td width="100" align="center"><a href="javascript: mostrarVentana('#contenido','buscar.php');" title="Consulta de DerechoHabientes" class="botones_menu"><img src="diseno/infoDH.png" width="64" height="64" /><br  />Consultar DerechoHabiente</a></td>
          <td width="56" align="center"><a href="javascript:mostrarVentana('#contenido','unificarDH.php');" title="Unificar Cedulas" class="botones_menu"><img src="diseno/unificarDH.png" width="70" height="70" /><br  />Unificar Derecho Habientes</a></td>
          <td width="56" align="center"><a href="javascript: mostrarVentana('#contenido','NuevoDerechoHabiente.php');" title="AgregarDH" class="botones_menu"><img src="diseno/agregarDH.png" width="70" height="70" /><br />Agregar DerechoHabiente</a></td>
          <td width="56" align="center">&nbsp;</td>
          <td width="57" align="center"><a href="logout.php" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="60" height="72" /><br />Salir</a></td>
        </tr>
      </table>
          </menu>
          <menu id="consultas" style="display:none"><table width="100%" border="0" class="tablaPrincipal">
  <tr>
    <td width="150" align="center"></td>
    <td width="100">&nbsp;</td>
    <td width="200" align="center">&nbsp;</td>
    <td width="100"><a href="javascript:mostrarVentana('#contenido','buscar.php');" title="Consulta de DerechoHabientes" class="botones_menu"><img src="diseno/infoDH.png" width="64" height="64" /><br  />Consultar DerechoHabiente</a></td>
    <td width="100">&nbsp;</td>
    <td align="center"><a href="logout.php" title="Salir" class="botones_menu"><img src="diseno/logout.png" width="60" height="72" /><br />
      Salir</a></td>
  </tr>
</table>
          </menu></td>
  </tr>
  <tr>
      <td height="200" align="center"><article id="contenido"></article></td>
  </tr>
  <tr>
    <td height="200">&nbsp;</td>
  </tr>
        </table>
                </td><td width="5%" style="width: 25%"></td></tr></tbody>
</table>
    <footer align="center">Sistema de Control de Vigencia 1.5</footer>
</body>
</html>
