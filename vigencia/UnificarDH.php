<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Documento sin título</title>
        <link href="lib/misEstilos.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <table class="ventana" cellpadding="0" cellspacing="0" width="800">
            <tr><td colspan="2" class="tituloVentana">Busqueda y unificacion de Cedulas</td></tr>
            <tr><td class="textosParaInputs" colspan="2">Buscar Por</td></tr><tr><td width="403">
                    <input type="radio" name="Busqueda" value="0" id="Nombre" onClick="CambiarBusqueda(this.id)"  />
                    Por Nombre</td><td width="395"><input name="Busqueda" id="Cedula" type="radio" value="1" checked="CHECKED" onClick="CambiarBusqueda(this.id);"  />Cedula Especifica</td></tr>
            <tr>
                <td colspan="3"><div id="busquedaCed">
                        <table width="700" border="0" cellspacing="0">
                            <tr><td>
                            <table width="700" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><form id="selDH" method="get" action="javascript: buscarDHbusqueda(document.getElementById('cedulaBuscar').value);"><table border="0">
                                                <tr>
                                                <td height="25" class="textosParaInputs" align="right">CEDULA: </td>
                                    <td align="left"><input type="text" name="cedulaBuscar" id="cedulaBuscar" maxlength="10"  onkeyup="this.value = this.value.toUpperCase();"/>
                                        <input name="buscar" type="button" class="botones" id="buscar" value="Buscar..." onClick="buscarDHbusqueda(document.getElementById('cedulaBuscar').value)" /><button onClick="mostrarDiv('campoBusqueda');mostrarDiv('campoDH');" class="botones" type="button">Cedula Adicional</button></td></tr>
                                            </table></form></td></tr></table></td></tr>                  
                        <tr><td><div id="campoBusqueda" style="display:none">
                                    <form action="javascript:buscarDHExtra(document.getElementById('cedula2').value);">
                                        <table border="0">
                                            <tr>
                                                <td class="textosParaInputs">CEDULA EXTRA</td><td>
                                                    <input type="text" name="cedula2" id="cedula2" onkeyup="this.value = this.value.toUpperCase();">
                                                    <input name="buscar2" type="button" class="botones" id="buscar2" value="Agregar Cedula" onClick="buscarDHExtra(document.getElementById('cedula2').value);"/></td>
                                            </tr></table>
                                    </form>
                                </div></td>
                        </tr>
                        <tr><td colspan="2"><table border="0">
                            <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE 1: </td>
                            <td align="left"><div id="derechohabientes">Ingrese la c&eacute;dula del derechohabiente y haga click en Buscar...</div></td></tr></table></td></tr>
                        <tr><td colspan="2"><div id="campoDH" style="display:none"><table border="0"><tr>
                                            <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE 2: </td>
                                            <td align="left"><div id="derechohabientes2">Ingrese la c&eacute;dula del derechohabiente y haga click en Agregar Cedula</div></td></tr></table></div></td></tr>
                        <tr>
                            <td height="25" class="textosParaInputs" align="right">&nbsp;</td>
                            <td align="center"><span class="textosParaInputs">
                                    <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onClick="cedulasDuplicadas();" value="Ver Cedulas Repetidas" />
                                </span></table></div><div id="busquedaNombre" style="display:none"><form id="selDH" method="POST" action="javascript:buscarDHNbusqueda(document.getElementById('ap_pB').value,document.getElementById('ap_mB').value,document.getElementById('nombreB').value);">
                            <table width="700" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="25" class="textosParaInputs" align="right">APELLIDO PATERNO: </td>
                                    <td align="left"><input type="text" name="ap_pB" id="ap_pB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                                    <td height="25" class="textosParaInputs" align="right">APELLIDO MATERNO: </td>
                                    <td align="left"><input type="text" name="ap_mB" id="ap_mB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" /></td>
                                </tr>
                                <tr>
                                    <td height="25" class="textosParaInputs" align="right">NOMBRE: </td>
                                    <td align="left" colspan="3"><input type="text" name="nombreB" id="nombreB" maxlength="25" onkeyup="this.value = this.value.toUpperCase();" />
                                        <input name="buscarN" type="submit" class="botones" id="buscarN" value="Buscar..." /></td>
                                </tr>
                                <tr>
                                    <td height="25" class="textosParaInputs" align="right">DERECHOHABIENTE: </td>
                                    <td align="left" colspan="3"><div id="derHN">Ingrese los datos del derechohabiente y haga click en Buscar...</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="center">
                                        <input name="seleccionarDH" type="button" class="botones" id="seleccionarDH" onclick="cedulasDuplicadasN();" value="Seleccionar" /><br /><br /></td>
                                </tr>

                                <tr>
                                    <td colspan="4" align="center"></td>
                                </tr>
                            </table>
                        </form></div></td>
            </tr>
            <tr><td colspan="2"></td></tr>
            <tr>
                <td colspan="2"><div id="citas" style="display:none"></div></td></tr>

        </table>
    </body>
</html>