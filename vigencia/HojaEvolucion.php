<?php
require_once'lib/fpdf.php';
define("FONT",'lib/font');
include 'lib/misFunciones.php';

class Expediente extends FPDF {

    function Header() {
        if(($this->PageNo()/2)!=0){
        $this->Image('diseno/logo_issste.jpg', 0.1, 0.5, 10, 1.6);
        $this->Ln(0.6);
		$this->SetX(17);
        $this->SetFont('times', 'B', 10);
        $this->Cell(2,1.7,utf8_decode("Hospital Regional \" Valentín Gomez Farías\" "),0,0,"R");
        $this->Ln(0.5);
        $this->SetFont('times', 'B', 10);
        $this->Cell(20,1.7, utf8_decode("Licencia Sanitaria No 00000360-A"), "T", 0, "R");
        $this->Ln();
        }
    }

    function Expediente() {
        $this->FPDF("P", "cm", "Letter");
    }
    
    function Footer() {
        $this->SetFont("arial","",8);
        $this->Cell(5,0.5,"321449");
        $this->Cell(10,0.5,utf8_decode("Evolución"));
        $this->Cell(5,0.5,"SM   3",0,0,"R");
    }

}
$id = $_REQUEST['id_derecho'];
$derecho= getDatosDerecho($id);
$expediente=  getExpedienteXid($id);


$pdf=new Expediente();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont("arial","B",10);
$pdf->Cell(3,0.5,  utf8_decode("Hoja de Evolución"));
$pdf->Ln();
$pdf->SetFont("arial","",10);
$pdf->Cell(15,0.5,"Nombre: ".ponerAcentosPdf($derecho['ap_p']."   ".$derecho['ap_m']."  ".$derecho['nombres']));
$pdf->Cell(5,0.5,"No.   ".ponerCeros($id, 8));
$pdf->Ln();
$pdf->Cell(8,0.5, utf8_decode("Cédula o Exp No.")."     ".$derecho['cedula']."/".$derecho['cedula_tipo']);
$pdf->Cell(8,0.5,"Servicio________________________");
$pdf->Cell(5,0.5,"Cama_______________");
$pdf->SetXY(1,6);
$pdf->SetFont("arial", "B",10);
$pdf->MultiCell(5,0.5,"Fecha, hora y  \n Nombre del medico",1);
$pdf->SetXY(6,6);
$pdf->MultiCell(15,0.5,  utf8_decode("Evolución \n "),1);
$pdf->SetY(6);
for($ren=0;$ren<19;$ren++)
{
    $pdf->Cell(5,1,"",1);
    $pdf->Cell(15,1,"",1);
    $pdf->Ln();
}
$pdf->AddPage();
$pdf->SetXY(1,4);
$pdf->SetFont("arial", "B",10);
$pdf->MultiCell(5,0.5,"Fecha, hora y  \n Nombre del medico",1);
$pdf->SetXY(6,4);
$pdf->MultiCell(15,0.5,  utf8_decode("Evolución \n "),1);
$pdf->SetY(4);
for($ren=0;$ren<21;$ren++)
{
    $pdf->Cell(5,1,"",1);
    $pdf->Cell(15,1,"",1);
    $pdf->Ln();
}
$pdf->Output();
?>
