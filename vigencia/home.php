<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" href="/lib/misEstilos.css" type="text/css">
        <link href="/lib/impresion.css" media="print" rel="stylesheet" type="text/css">
        <script src="lib/jquery-1.7.1.js"></script>
        <script src="lib/arreglos.js"></script>
</head>

<body>
<?php
session_start();
$_SESSION['idUsuario'] = "-1";
$_SESSION['tipoUsuario'] = "-1";
$_SESSION['IdCon'] = "-1";
$_SESSION['idServ'] = "-1";
$_SESSION['idDr'] = "-1";
include_once('lib/misFunciones.php');
?>
<table class="tablaPrincipal" id="tablaPrincipal" width="800" height="600" cellpadding="0" cellspacing="0"><tr><td>
                    <table class="encabezado" id="encabezado" width="800" height="100" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td rowspan="2" width="104" height="101" align="center" valign="middle"><img src="diseno/logoEncabezado.png" width="74" height="74"></td>
                            <td height="48" class="tituloEncabezado" valign="middle">Control de Vigencia</td></tr>
                        <tr><td class="subtituloEncabezado">Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado &nbsp;&nbsp;&nbsp;</td>
                        </tr>
                    </table></td></tr>
                    <tr><td align="center">
                    <table width="800">
                        <tr><td valign="top"><div id="menu" style="display:none"><table border="0" width="100%" class="tablaPrincipal"><td width="150"><a href="javascript:inicio('inicioVigencia.php');" title="inicio" class="botones_menu"><img src="diseno/_medHome.gif"><br>Inicio</a></td>
                                        <td width="100"><a href="" title="Consultar DerechoHabientes" class="botones_menu"><img src="diseno/infoDH.png" /><br />Informacion de DerechoHabiente</a></td>
                                        <td width="250">&nbsp;</td>
                                        <td width="100" align="center"><a href="" class="botones_menu" title="Unificar Expedientes"><img src="diseno/unificarDH.png" /><br>UnificarExpedientes</a></td>
                                        <td width="100" align="center"><a href="javascript: mostrarVentana('#contenido','AgregarDH.php',<?= $_SESSION['idUsuario'] ?>);" class="botones_menu" title="Dar de alta DerechoHabiente"><img src="diseno/agregarDH.png" /><br>Dar de Alta DerechoHabiente</a></td>
                                        <td width="250">&nbsp;</td>
                                        <td width="100">&nbsp;</td>
                                        <td width="100"><a href="logout.php" class="botones_menu" title="Salir"><img src="diseno/logout.png"><br>Salir</a></td>
                                    </table>
                                </div>
                                <div id="consultas" style="display:none">
                                    <table border="0" width="100%" class="tablaPrincipal">
                                        <tr><td width="150">
                                                <a href="" class="botones_menu" title="Inicio"><img src="diseno/_medHome.gif" /><br />Inicio</a>
                                            </td>
                                            <td width="100">&nbsp;</td>
                                            <td width="250" align="center"><a href="#" class="botones_menu" title="Consulta de Expedientes"><img src="diseno/buscar_expediente.png"><br>Consulta de Expediente</a></td>
                                            <td width="100"><a href="" class="botones_menu" title="Consulta de DerechoHabiente"><img src="diseno/infoDH.png"><br />Informacion de Derechohabiente</a></td>
                                            <td width="100">&nbsp;</td>
                                            <td width="250">&nbsp;</td>
                                            <td width="100">&nbsp;</td>
                                            <td width="100"><a href="logout.php" class="botones_menu" title="Salir"><img src="diseno/logout.png" /><br />Salir</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </td></tr></table>
                </td></tr>
            <tr><td align="center"><br><div id="contenido"></div></td></tr><tr><td height="300" colspan="5"></td></tr>
        </table></center>
</body>
</html>