<?php
include('lib/misFunciones.php');
$tipo_cedulas = array("10","20","30","31","32","40","41","50","51","60","61","70","71","80","81","90","91","92","99");
?>
<form name="agregarDH" id="agregarDH" submit="javascript:validarAgregarDH();">
    <table width="700" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
                <td height="25" align="right"><label for="cedulaAgregar" class="error"><span class="textosParaInputs">CEDULA:</span></label> </td>
              <td align="left"><input type="text" name="cedulaAgregar" id="cedulaAgregar" maxlength="10" onkeyup="this.value = this.value.toUpperCase();" onfocus="if(this.value=='Cedula del DerechoHabiente'){this.value=''}" value="Cedula del DerechoHabiente"/>
                 
              </td>
            </tr>
            <tr><td class="textosParaInputs">Sexo</td><td><select id="sexo" onchange="ObtenerTpoDerXSexo(this.id,'cedulaTipoAgregar');">
                        <option value="H">Hombre</option>
                        <option value="M">Mujer</option>
                    </select></td><td class="textosParaInputs">Tipo de DerechoHabiente</td><td><select name="cedulaTipoAgregar" id="cedulaTipoAgregar" onfocus="ObtenerTpoDerXSexo('sexo',this.id);"></select></td></tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">NOMBRE:</td>
    <td align="left"><input name="ap_pAgregar" type="text" id="ap_pAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" value="Apellido Paterno" onfocus="javascript:borrarReferencias(this);" /><label for="ap_pAgregar" class="error"></label>
        <input name="ap_mAgregar" type="text" id="ap_mAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" value="Apellido Materno" onfocus="javascript:borrarReferencias(this);" /><label for="ap_mAgregar" class="error"></label>
        <input name="nombreAgregar" type="text" id="nombreAgregar" size="20" maxlength="20" onkeyup="this.value = this.value.toUpperCase();" value="Nombre(s)" onfocus="javascript:borrarReferencias(this);" /><label for="nombreAgregar" class="error"></label></td>
  </tr>
  <tr>
      <td height="25" align="right"><label for="telefonoAgregar" class="error"><span class="textosParaInputs">TELEFONO:</span></label></td>
    <td align="left"><input name="telefonoAgregar" type="tel" id="telefonoAgregar" size="20" maxlength="10" value="" /><label for="fecha_nacAgregar" class="error"><span class="textosParaInputs"> FECHA NAC. </span></label><input name="fecha_nacAgregar" type="text/javascript" id="fecha_nacAgregar" size="20" maxlength="10" onkeydown="fechas(event, this);"/>
    </td>
</td>
  </tr>
  <tr>
      <td height="25" class="textosParaInputs" align="right"><label for="direccionAgregar" class="error"><span class="textosParaInputs">DIRECCION:</span></label></td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" size="64" maxlength="50" onkeyup="this.value = this.value.toUpperCase();" /></td>
  </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estadoAgregar" id="estadoAgregar" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');" onfocus="cargarEstados('estadoAgregar')"> 
    </select><span class="textosParaInputs">MUNICIPIO: </span><select name="municipioAgregar" id="municipioAgregar">
    </select>
    </td>
  </tr>
  <tr><td class="textosParaInputs">Unidad Medica de Adscripcion</td><td><div id="Unidad"></div></td></tr>
        
            <tr>
              <td colspan="2" align="center"><div id="divBotones_EstadoAgregarDH">
                      <button id="enviar" type="button" class="botones" onclick="">Regresar</button>&nbsp;&nbsp;<input type="button" id="guardar" value="Agregar" class="botones" onclick="javascript:validarAgregarDH();">
              </div>
                </td>
              </tr>
        
            <tr>
              <td colspan="2" align="center"></td>
            </tr>
          </table></form>