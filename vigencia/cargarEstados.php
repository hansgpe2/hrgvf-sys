<?php

include './Conexiones/agenda.php';
include './lib/misFunciones.php';
include './lib/consultaSQL.php';
$sqlF=new consultaSQL();
$link = new mysqli();
$link->connect($hostname_bdissste, $username_bdissste, $password_bdissste, $database_bdissste);
$estado = $_REQUEST['estado'];
$sql =$sqlF->select_order_groupby('estados', array("estado"), array("estado"));
$qEstados = $link->query($sql);
if ($link->error) {
    echo '-1|Error Base de datos:' . $link->error;
} else {
    $strOpcionEstados = '<option value="-1"></option>';
    while ($row = $qEstados->fetch_assoc()) {
        if ($row['estado'] == $estado)
            $strOpcionEstados.='<option value="' . $row['estado'] . '" selected>' . ponerAcentos($row['estado']) . '</option>';
        else
            $strOpcionEstados.='<option value="' . $row['estado'] . '">' . ponerAcentos($row['estado']) . '</option>';
    }
}echo $strOpcionEstados;

