<?php
require_once'lib/fpdf.php';
define("FONT",'lib/font');
include 'lib/misFunciones.php';
class Expediente extends FPDF {
    var $B;
var $I;
var $U;
var $HREF;

    function Expediente() {
        $this->FPDF("P", "cm", "Legal");
    }
    function WriteHTML($html)
{
    // Intérprete de HTML
    $html = str_replace("\n",' ',$html);
    $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            // Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,$e);
        }
        else
        {
            // Etiqueta
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                // Extraer atributos
                $a2 = explode(' ',$e);
                $tag = strtoupper(array_shift($a2));
                $attr = array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])] = $a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    // Etiqueta de apertura
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF = $attr['HREF'];
    if($tag=='BR')
        $this->Ln(5);
}

function CloseTag($tag)
{
    // Etiqueta de cierre
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF = '';
}

function SetStyle($tag, $enable)
{
    // Modificar estilo y escoger la fuente correspondiente
    $this->$tag += ($enable ? 1 : -1);
    $style = '';
    foreach(array('B', 'I', 'U') as $s)
    {
        if($this->$s>0)
            $style .= $s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    // Escribir un hiper-enlace
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}
}
$id = $_REQUEST['id_derecho'];
$derecho= getDatosDerecho($id);
$unidad=  UnidadMedicaXid($derecho['unidad_medica']);
$expediente=  getExpedienteXid($id);
print("<table width='800' border='0'><tr><td align='center'>INSTITUTO DE SEGURIDAD Y SERVICIOS SOCIALES DE LOS TRABAJADORES DEL ESTADO</td></tr>");
print("<tr><td align='center'>SUBDIRECCION MEDICA</td></tr>");

print("<tr><td align='center'>SOLICITUD DE CONSULTA DE ESPECIALIDAD</td></tr></table>");
$edad=date("Y")-date("Y",  strtotime($derecho['fecha_nacimiento']));
$html="<table border='1' width='800'><tr><td>De la clinica de Adscripci&oacute;n: ".htmlentities($unidad['nombre'])."</td><td rowspan='3'></tr>
    <tr><td>A la clinica de Especialidad: H.R Valentín Gomez Far&iacute;as</td></tr>
    <tr><td>Servicio de:                                    </td></tr>
    <tr><td>Derechohabiente:".$derecho['cedula_tipo']."</td></tr>
    <tr><td>".$derecho['ap_p']." ".$derecho['ap_m']." ".$derecho['nombres']."</td><td>$edad</td><td>".$derecho['id_derecho']."</td><td>".$derecho['cedula']."</td></tr>
    <tr><td colspan='4'>Deber&aacute; acudir a la Clinica mencionada el dia__________________________________________</td></tr></table>";
$html.="<p>&nbsp;</p><table border='0'><tr><td>PRESENTACIONES DEL CASO</td></tr>
    <tr><td colspan='2'>1° Fecha de la primera consulta del padecimiento actual_______________________________________________________</td></tr>
    <tr><td colspan='2'>2° Sintomas principales que motivaron la consulta____________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>3° Resumen Cronol&oacute;gico del interrogatorio (Antecedentes, padecimiento Actual_____________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>4° Resumen de la exploracion fisica (encuadre lo positivo y describa; subraye lo negativo): Cuero cabelludo-Piel-Ojos-</td></tr>
    <tr><td colspan='2'>Pupilas-Nariz-Dientes-Boca-Faringe-Am&iacute;gdalas-Antrum-Gl&aacute;ndulas salivales-Cadenas ganglionares-</td></tr>
    <tr><td colspan='2'>Tiroides-Mamas-Vasos perif&eacute;ricos-Coraz&oacute;n-Pulmones-H&iacute;gado-Ri&ntilde;ones-Bazo-Tacto Vaginal o rectal</td></tr>
    <tr><td colspan='2'>Pr&oacute;stata-Anillos inguinales y femorales-Columna-Articulaciones-Extremidades-Reflejos</tr></td>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td colspan='2'>_________________________________________________________________________________________________</td></tr>
    <tr><td  colspan='2'>5°Terap&eacute;utica empleada_______________________________________________________________________________</td></tr>
    <tr><td  colspan='2'>6° Impresi&oacute;n diagn&oacute;stico_______________________________________________________________________________</td></tr>
    <tr><td  colspan='2'>7° Motivo del env&iacute;o___________________________________________________________________________________</td></tr>
    <tr><td  colspan='2'>        Incapacidad desde__________________________________hasta________________________________</td></tr>
    <tr><td  colspan='2'>&nbsp;</td></tr>    
    <tr><td  colspan='2'>&nbsp;</td></tr>
    <tr><td  width='50%'>Vo Bo</td><td> Fecha:".date("d/m/Y")."            Hora:".date("H:i")."</td></tr>
    <tr><td  colspan='2'>&nbsp;</td></tr>    
    <tr><td  colspan='2'>&nbsp;</td></tr>    
    <tr><td colspan='2'>&nbsp;</td></tr>    
    <tr><td width='50%'>DIRECTOR DE LA CLINICA</td><td width='50%'>Nombre,clave y firma del M&eacute;dico Solicitante</td></tr>
    </table>";
print($html);
?>
