/*
SQLyog Ultimate v9.63 
MySQL - 5.0.67-community-nt : Database - sistema_agenducha
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistema_agenducha` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `sistema_agenducha`;

/*Table structure for table `derechohabientes` */

DROP TABLE IF EXISTS `derechohabientes`;

CREATE TABLE `derechohabientes` (
  `id_derecho` int(11) NOT NULL auto_increment,
  `cedula` varchar(10) collate utf8_spanish_ci NOT NULL,
  `cedula_tipo` varchar(2) collate utf8_spanish_ci NOT NULL,
  `ap_p` varchar(20) character set utf8 collate utf8_spanish2_ci NOT NULL,
  `ap_m` varchar(20) collate utf8_spanish_ci NOT NULL,
  `nombres` varchar(20) collate utf8_spanish_ci NOT NULL,
  `fecha_nacimiento` varchar(10) collate utf8_spanish_ci NOT NULL,
  `telefono` varchar(10) collate utf8_spanish_ci NOT NULL,
  `celular` varchar(10) collate utf8_spanish_ci NOT NULL default '0',
  `direccion` varchar(50) collate utf8_spanish_ci NOT NULL,
  `colonia` varchar(60) collate utf8_spanish_ci NOT NULL,
  `cp` int(11) NOT NULL default '0',
  `estado` varchar(35) collate utf8_spanish_ci NOT NULL,
  `municipio` varchar(50) character set ascii collate ascii_bin NOT NULL,
  `status` varchar(1) collate utf8_spanish_ci NOT NULL,
  `extra1` varchar(25) collate utf8_spanish_ci NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `st` varchar(1) collate utf8_spanish_ci NOT NULL,
  `email` varchar(30) collate utf8_spanish_ci default 'NULL',
  `unidad_medica` int(11) default '1',
  PRIMARY KEY  (`id_derecho`)
) ENGINE=MyISAM AUTO_INCREMENT=107577 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
