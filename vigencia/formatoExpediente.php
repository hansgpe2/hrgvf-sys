<?php
error_reporting(E_ALL^E_NOTICE);
include_once 'lib/misFunciones.php';
ob_clean();
require_once'lib/fpdf.php';

//require 'lib/mc_table.php';
class Expediente extends FPDF {
var $id;
function Codabar($xpos, $ypos, $code, $start='A', $end='A', $basewidth=0.035, $height=0.5) {
    $barChar = array (
        '0' => array (6.5, 10.4, 6.5, 10.4, 6.5, 24.3, 17.9),
        '1' => array (6.5, 10.4, 6.5, 10.4, 17.9, 24.3, 6.5),
        '2' => array (6.5, 10.0, 6.5, 24.4, 6.5, 10.0, 18.6),
        '3' => array (17.9, 24.3, 6.5, 10.4, 6.5, 10.4, 6.5),
        '4' => array (6.5, 10.4, 17.9, 10.4, 6.5, 24.3, 6.5),
        '5' => array (17.9,    10.4, 6.5, 10.4, 6.5, 24.3, 6.5),
        '6' => array (6.5, 24.3, 6.5, 10.4, 6.5, 10.4, 17.9),
        '7' => array (6.5, 24.3, 6.5, 10.4, 17.9, 10.4, 6.5),
        '8' => array (6.5, 24.3, 17.9, 10.4, 6.5, 10.4, 6.5),
        '9' => array (18.6, 10.0, 6.5, 24.4, 6.5, 10.0, 6.5),
        '$' => array (6.5, 10.0, 18.6, 24.4, 6.5, 10.0, 6.5),
        '-' => array (6.5, 10.0, 6.5, 24.4, 18.6, 10.0, 6.5),
        ':' => array (16.7, 9.3, 6.5, 9.3, 16.7, 9.3, 14.7),
        '/' => array (14.7, 9.3, 16.7, 9.3, 6.5, 9.3, 16.7),
        '.' => array (13.6, 10.1, 14.9, 10.1, 17.2, 10.1, 6.5),
        '+' => array (6.5, 10.1, 17.2, 10.1, 14.9, 10.1, 13.6),
        'A' => array (6.5, 8.0, 19.6, 19.4, 6.5, 16.1, 6.5),
        'T' => array (6.5, 8.0, 19.6, 19.4, 6.5, 16.1, 6.5),
        'B' => array (6.5, 16.1, 6.5, 19.4, 6.5, 8.0, 19.6),
        'N' => array (6.5, 16.1, 6.5, 19.4, 6.5, 8.0, 19.6),
        'C' => array (6.5, 8.0, 6.5, 19.4, 6.5, 16.1, 19.6),
        '*' => array (6.5, 8.0, 6.5, 19.4, 6.5, 16.1, 19.6),
        'D' => array (6.5, 8.0, 6.5, 19.4, 19.6, 16.1, 6.5),
        'E' => array (6.5, 8.0, 6.5, 19.4, 19.6, 16.1, 6.5),
    );
    $this->SetFont('Arial','',12);
    $this->Text($xpos, $ypos + $height+0.5,"expediente ". $code);
    $this->SetFillColor(0);
    $code = strtoupper($start.$code.$end);
    for($i=0; $i<strlen($code); $i++){
        $char = $code[$i];
        if(!isset($barChar[$char])){
            $this->Error('Invalid character in barcode: '.$char);
        }
        $seq = $barChar[$char];
        for($bar=0; $bar<7; $bar++){
            $lineWidth = $basewidth*$seq[$bar]/6.5;
            if($bar % 2 == 0){
                $this->Rect($xpos, $ypos, $lineWidth, $height, 'F');
            }
            $xpos += $lineWidth;
        }
        $xpos += $basewidth*10.4/6.5;
    }
}

    function Header() {
        if ($this->PageNo() == 1) {
            $this->Image('diseno/issste2013.jpg', 0.1, 0.5, 5.54, 1.6);
            $this->Ln(1.1);
            $this->SetFont('times', 'B', 10);
            $this->Cell(20, 1, "HOSPITAL REGIONAL DR. VALENTIN GOMEZ FARIAS DEL ISSSTE, ZAPOPAN, JAL.", "T", 0, "C");
            $this->Ln();
            $this->Cell(20,1,"Licencia Sanitaria 00000360-A",0,0,"R");
            $this->Ln();
        } else
        if ($this->PageNo() >= 2 && $this->PageNo() <= 4) {
            if (($this->PageNo() / 2) != 0) {
                $this->Image('diseno/issste2013.jpg', 0.1, 0.5, 10, 1.6);
                $this->Ln(0.6);
                $this->SetX(17);
                $this->SetFont('times', 'B', 10);
                $this->Cell(2, 1.7, utf8_decode("Hospital Regional \" Valentín Gomez Farías\" "), 0, 0, "R");
                $this->Ln(0.5);
                $this->SetFont('times', 'B', 10);
                $this->Cell(20, 1.7, utf8_decode("Licencia Sanitaria No 00000360-A"), "T", 0, "R");
                $this->Ln();
            }
        }if($this->PageNo()>=5)
        {
            $this->Image('diseno/issste2013.jpg', 0.1, 0.5, 10, 1.6);
        $this->SetXY(17,1.6);
        $this->SetFont('times', 'B', 10);
        //$this->Cell(2,0.5,ponerCeros($this->id, 8),1,0);
        $this->Ln(0.5);
        $this->SetFont('times', 'B', 10);
        $this->Cell(20, 1, utf8_decode("Hospital Regional \" Valentín Gomez Farías\" "), "T", 0, "C");
        $this->Ln();
        $this->Cell(20, 1, utf8_decode("Licencia Sanitaria No 00000360-A"), 0, 0, "C");
        $this->Ln();
        }
    }

    function Expediente($id) {
        $this->FPDF("P", "cm", "Letter");
		$this->id=$id;
    }
     function Footer() {
         if($this->PageNo()==1)
         {
             $this->Cell(20,1,"Av. Soledad Orozco Colonia el Capullo,Zapopan Jalisco. Codigo Postal 45150","T",0,"C");
         }
         if ($this->PageNo() >= 2 && $this->PageNo() <= 4){
        $this->SetFont("arial","",8);
        $this->Cell(5,0.5,"321449");
        $this->Cell(10,0.5,utf8_decode("Evolución"));
        $this->Cell(5,0.5,"SM   3",0,0,"R");
         }
    }
}

$id = $_REQUEST['id_derecho'];
$derecho = getDatosDerecho($id);
$direccion = explode(",", $derecho['direccion']);
$cp = $derecho['Codigo_Postal'];
$expediente = getExpedienteXid($id);
$unimed = UnidadMedicaXid($derecho['unidad_medica']);
if (count($unimed) == 0)
    $unimed = array('nombre' => '');
$edad = getEdadXfechaNac($derecho['fecha_nacimiento']);
$pdf = new Expediente($id);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(1, 0.5, "Hoja frontal");
$pdf->Ln(1);
$pdf->Cell(2,0.5,"Cedula:");
$pdf->SetFont('times', '', 9);
$pdf->Cell(15,0.5,$derecho['cedula']."/".$derecho['cedula_tipo']);
$pdf->Ln();
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2, 0.5, "NOMBRE:");
$pdf->SetFont('times', '', 9);
$pdf->Cell(15, 0.5, ponerAcentosPdf($derecho['ap_p'] . "       " . $derecho['ap_m'] . "         " . $derecho['nombres']));
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(4.5, 0.5, "CLINICA DE ADSCRIPCION:");
$pdf->SetFont('times', '', 9);
$pdf->Cell(15, 0.5, ponerAcentosPdf($unimed['nombre']) . "    (" . ponerAcentosPdf($derecho['municipio']) . ", " . ponerAcentosPdf($derecho['estado']) . ")");
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(3, 0.5, "EXPEDIENTE No.");
$pdf->SetFont('times', '', 9);
$pdf->Cell(2, 0.7, $id);
$pdf->Codabar(15,1,$id);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2, 0.5, "Edad ");
$pdf->SetFont('times', '', 9);
$pdf->Cell(2, 0.5, $edad);
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(3.5, 0.5, "Fecha de Elaboracion ");
$pdf->SetFont('times', '', 9);
$pdf->Cell(2, 0.5, date("d/m/Y", strtotime($expediente['fecha_creacion'])));
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2, 0.5, "Servicio(s):");
$pdf->setFont('Times', '', 9);
$pdf->Cell(18, 0.5, "___________________________________________________________________________");
//$pdf->Line(0.1,,80,40);
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(3, 0.5, "DOMICILIO");
$pdf->setFont('Times', '', 9);
$pdf->Cell(12, 0.5, $direccion[0]);
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(3, 0.5, "COLONIA:");
$pdf->setFont('Times', '', 9);
$pdf->Cell(5, 0.5, ponerAcentosPdf($direccion[1]));
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2, 0.5, "Municipio   ");
$pdf->setFont('Times', '', 9);
$pdf->Cell(10, 0.5, $derecho['municipio']);
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(3, 0.5, "CODIGO POSTAL");
$pdf->setFont('Times', '', 9);
$pdf->Cell(2, 0.5, $derecho['Codigo_Postal']);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2, 0.5, "TELEFONO");
$pdf->setFont('Times', '', 9);
$pdf->Cell(2, 0.5, $derecho['telefono']);
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2, 0.5, "Celular:");
$pdf->setFont('Times', '', 9);
$pdf->Cell(2, 0.5, $derecho['celular']);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(3, 0.5, "Correo Electronico:");
$pdf->setFont('Times', '', 9);
$pdf->Cell(2, 0.5, $derecho['email']);
$pdf->Ln(2);
$pdf->setFont('Times', 'B', 9);
$pdf->MultiCell(10, 0.5, "
MEDICO TRATANTE:_______________________________________
_______________________________________________________
RESIDENTES:____________________________________________
_______________________________________________________
_______________________________________________________
_______________________________________________________
_______________________________________________________
DIAGNOSTICO:___________________________________________
_______________________________________________________
_______________________________________________________
_______________________________________________________
_______________________________________________________
TRATAMIENTO:____________________________________________
_______________________________________________________
_______________________________________________________
_______________________________________________________
_______________________________________________________
Alergico A_____________________________________________
_______________________________________________________");
$pdf->SetXY(15, 12);
$pdf->MultiCell(10, 0.5, "
ORDEN DEL EXPEDIENTE
(De adelante hacia " . utf8_decode("atrás") . ")
    
1.-Hoja frontal
2.-Historia Clinica
3.-Examenes de Laboratorio
4.-Reporte de Rayos X
5.-Otros Estudios de Gabinete
6.-Hojas de " . utf8_decode("Evolución") . "
7.-Control de Liquidos
8.-Hojas de Enfermeria
9.-Otros Documentos:
(Administrativos, parte medico de
lesiones,Responsiva,Notas de 
Trabajo Social ETC.)
Grupo Sanguineo______
Fac Rh_______________

" . utf8_decode(
                "La Subdirección General Medica Informa:
    El sistema de datos personales Expediente
    Clínico tiene la finalidad de registrar las
    Acciones de la atención médica. Los 
    Datos recabados serán protegidos, incorpo
    rados y tratados conforme a lo dispuesto
    Por el Decimoséptimo de los linamientos de
    protección de Datos Personales. (D:O:F)"));
//Formato de Hoja de evolución
$pdf->AddPage("P","letter");
$pdf->SetFont("arial","B",10);
$pdf->Codabar(15,1,$id);
$pdf->Cell(3,0.5,  utf8_decode("Hoja de Evolución"));
$pdf->Ln();
$pdf->SetFont("arial","",10);
$pdf->Cell(15,0.5,"Nombre: ".ponerAcentosPdf($derecho['ap_p']."   ".$derecho['ap_m']."  ".$derecho['nombres']));
$pdf->Cell(5,0.5,"No.   ".ponerCeros($id, 8));
$pdf->Ln();
$pdf->Cell(8,0.5, utf8_decode("Cédula o Exp No.")."     ".$derecho['cedula']."/".$derecho['cedula_tipo']);
$pdf->Cell(8,0.5,"Servicio________________________");
$pdf->Cell(5,0.5,"Cama_______________");
$pdf->SetXY(1,6);
$pdf->SetFont("arial", "B",10);
$pdf->MultiCell(5,0.5,"Fecha, hora y  \n Nombre del medico",1);
$pdf->SetXY(6,6);
$pdf->MultiCell(15,0.5,  utf8_decode("Evolución \n "),1);
$pdf->SetY(6);
for($ren=0;$ren<19;$ren++)
{
    $pdf->Cell(5,1,"",1);
    $pdf->Cell(15,1,"",1);
    $pdf->Ln();
}
$pdf->AddPage("P","letter");
$pdf->Codabar(15,1,$id);
$pdf->SetXY(1,4);
$pdf->SetFont("arial", "B",10);
$pdf->MultiCell(5,0.5,"Fecha, hora y  \n Nombre del medico",1);
$pdf->SetXY(6,4);
$pdf->MultiCell(15,0.5,  utf8_decode("Evolución \n "),1);
$pdf->SetY(4);
for($ren=0;$ren<21;$ren++)
{
    $pdf->Cell(5,1,"",1);
    $pdf->Cell(15,1,"",1);
    $pdf->Ln();
}
//Formato de Hoja Clínica
$pdf->AddPage();
$pdf->Codabar(15,1,$id);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(1, 0.5, utf8_decode("Hoja clínica"));
$pdf->Ln(1);
$pdf->Cell(2, 0.5, "NOMBRE:");
$pdf->SetFont('times', '', 9);
$pdf->Cell(15, 0.5, ponerAcentosPdf($derecho['ap_p'] . "       " . $derecho['ap_m'] . "         " . $derecho['nombres']));
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2,0.5,"CEDULA");
$pdf->SetFont('times', '', 9);
$pdf->Cell(3,0.5,$derecho['cedula']."/".$derecho['cedula_tipo']);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(7,0.5,"SERVICIO:______________________________");
$pdf->Cell(5,0.5,"CAMA:___________");
$pdf->Ln(1);
$pdf->Cell(20,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Antecedentes Hereditarios","L");
$pdf->Cell(15,0.5," ",1);
for($ren=0;$ren<9;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Antecedentes Personales"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("No psicologicos"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<9;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Antecedentes psicológicos"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<14;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Padecimiento Actual"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<16;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5," ","LB");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Interrogatorio"),"LT");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Por Aparatos y Sistemas"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  "","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Digestivo"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<11;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Respiratorio"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
$pdf->Codabar(15,1,$id);
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Circulatorio"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Urinario"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Genital"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Nervioso"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Organos de los Sentidos"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Sistemas Generales"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Terapeutica Empleada"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<4;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode(""),"LB");
$pdf->Cell(15,0.5," ",1);
$pdf->AddPage();
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(1, 0.5, utf8_decode("Hoja clínica"));
$pdf->Ln(1);
$pdf->Cell(2, 0.5, "NOMBRE:");
$pdf->SetFont('times', '', 9);
$pdf->Cell(15, 0.5, utf8_decode($derecho['ap_p'] . "       " . $derecho['ap_m'] . "         " . $derecho['nombres']));
$pdf->Ln(1);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(2,0.5,"CEDULA");
$pdf->SetFont('times', '', 9);
$pdf->Cell(3,0.5,$derecho['cedula']."/".$derecho['cedula_tipo']);
$pdf->Codabar(15,1,$id);
$pdf->setFont('Times', 'B', 9);
$pdf->Cell(7,0.5,"SERVICIO:______________________________");
$pdf->Cell(5,0.5,"CAMA:___________");
$pdf->Ln(1);
$pdf->Cell(5,0.5,  utf8_decode("Exploración Física"),"TL");
$pdf->Cell(15,0.5,"Habitos Exterior",1,0,"C");
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Talla"),"TL");
$pdf->Cell(15,0.5,"Peso Actual                           Peso Ideal                            ",1,0,"C");
$pdf->Ln();
$pdf->Cell(5,0.5,"Pulso","L");
$pdf->Cell(15,0.5,"T/A                  Respiraciones                       Temperatura               ",1);
$pdf->Ln();
$pdf->Cell(5,0.5,  utf8_decode("Inspección general"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<3;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Cabeza","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Cuello","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<3;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Tórax"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Abdomen","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Tracto Rectal y vaginal","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,"Extremidades","L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<2;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5,  utf8_decode("Impresión Diagnostica"),"L");
$pdf->Cell(15,0.5," ",1);
$pdf->Ln();
for($ren=0;$ren<7;$ren++)
{
    $pdf->Cell(5,0.5," ","L");
    $pdf->Cell(15,0.5," ",1);
    $pdf->Ln();
}
$pdf->Cell(5,0.5, " ","LB");
$pdf->Cell(15,0.5," ",1);
$pdf->Output();
?>