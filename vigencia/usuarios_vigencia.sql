-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-09-2012 a las 15:21:09
-- Versión del servidor: 6.0.10-alpha-community
-- Versión de PHP: 5.4.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_agenducha`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_vigencia`
--

CREATE TABLE IF NOT EXISTS `usuarios_vigencia` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `pass` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_usuario` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `id_consultorio` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `status` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `sesionId` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `extra1` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `st` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuarios_vigencia`
--

INSERT INTO `usuarios_vigencia` (`id_usuario`, `login`, `pass`, `nombre`, `tipo_usuario`, `id_consultorio`, `id_servicio`, `status`, `sesionId`, `extra1`, `st`) VALUES
(1, 'VIGENCIA', '123456', 'USUARIO DE VIGENCIA', '1', 0, 0, '0', '', '', '1'),
(2, 'ESPECIAL', 'VISOR12', 'VISOR DE VIGENCIA', '3', 0, 0, '0', '', '', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
