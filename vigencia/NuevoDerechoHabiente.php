﻿<?php
$tipo_cedulas = array("10","20","30","31","32","40","41","50","51","60","61","70","71","80","81","90","91","92","99");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
    <form name="agregarDH" id="agregarDH" action="nuevoDHValyAdd.php"  method="post">
    <table width="889" border="0" cellspacing="0" cellpadding="0" class="ventana">
            <tr>
              <td colspan="2" class="tituloVentana">AGREGAR DERECHOHABIENTE</td>
            </tr>
            <tr>
              <td width="277">&nbsp;</td>
              <td width="562">&nbsp;</td>
            </tr>
            <tr>
                <td height="25" align="right"><label for="cedulaAgregar" class="error"><span class="textosParaInputs">CEDULA:</span></label> </td>
              <td align="left"><input name="cedulaAgregar" type="text" id="cedulaAgregar" tabindex="1" onfocus="if(this.value=='Cedula del DerechoHabiente'){this.value=''}" onblur="if(this.value==''){this.value='Cedula del DerechoHabiente';}this.value = this.value.toUpperCase();" value="Cedula del DerechoHabiente" maxlength="10"/>
                </label>
              </td>
                <tr><td  align="right" class="textosParaInputs">SEXO</td><td><select name="sexo" id="sexo" tabindex="2" onchange="ObtenerTpoDerXSexo(this.id,'cedulaTipoAgregar')">
              <option value="-1" selected="selected"></option>
              <option value="M">Mujer</option>
              <option value="H">Hombre</option>
              </select>
                        &nbsp;<span class="textosParaInputs">TIPO DE CEDULA</span>&nbsp;<select name="cedulaTipoAgregar" id="cedulaTipoAgregar" tabindex="3" onfocus="ObtenerTpoDerXSexo('sexo', this.id);">
              </select></td>
            </tr>
  <tr>
    <td height="25" class="textosParaInputs" align="center" colspan="4">NOMBRE DE DERECHOHABIENTE</td></tr>
    <tr><td class="textosParaInputs" align="right">APELLIDO PATERNO</td>
    <td align="left"><input name="ap_pAgregar" type="text" id="ap_pAgregar" tabindex="4" onfocus="javascript:borrarReferencias(this);" onblur="this.value = this.value.toUpperCase();" size="20" maxlength="50" /><label for="ap_pAgregar" class="error"></label><span class="textosParaInputs"> MATERNO</span><input name="ap_mAgregar" type="text" id="ap_mAgregar" tabindex="5" onfocus="javascript:borrarReferencias(this);" onblur="this.value = this.value.toUpperCase();" size="20" maxlength="50" /><label for="ap_mAgregar" class="error"></label></td></tr>
        <tr><td class="textosParaInputs" align="right">NOMBRE(S)</td><td align="left">
        <input name="nombreAgregar" type="text" id="nombreAgregar"  tabindex="6" onfocus="javascript:borrarReferencias(this);" onblur="this.value = this.value.toUpperCase();" size="50" maxlength="50" /><label for="nombreAgregar" class="error"></label></td>
  </tr>
  <tr>
      <td height="25" align="right"><label for="telefonoAgregar" class="error"><span class="textosParaInputs">TELEFONO: (01)</span></label></td>
      <td align="left"><input name="telefonoAgregar" type="text" id="telefonoAgregar" tabindex="7" onfocus="if(this.value=='lada-numero'){this.value=''}" onblur="if(this.value==''){this.value='lada-numero'}" value="lada-numero" size="20" maxlength="10" /><label for="fecha_nacAgregar" class="error"><span class="textosParaInputs"> FECHA NAC.</span></label>
        <span style="position:relative">
        <label for="mes1">mes</label>
        <select name="mes1" id="mes1" tabindex="8" onchange="calendarios('1');">
          <option value="-1"></option>
          <option value="01">ene</option>
          <option value="02">feb</option>
          <option value="03">mar</option>
          <option value="04">abr</option>
          <option value="05">may</option>
          <option value="06">jun</option>
          <option value="07">jul</option>
          <option value="08">ago</option>
          <option value="09">sept</option>
          <option value="10">oct</option>
          <option value="11">nov</option>
          <option value="12">dic</option>
        </select>
        <label for="dia1">dia</label>
        <select name="dia1" id="dia1" tabindex="9">
          <option value="-1"></option>
          <option value="01">1</option>
          <option value="02">2</option>
          <option value="03">3</option>
          <option value="04">4</option>
          <option value="05">5</option>
          <option value="06">6</option>
          <option value="07">7</option>
          <option value="08">8</option>
          <option value="09">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
        </select>
        <label for="anio1">año</label>
        <select name="anio1" id="anio1" tabindex="10">
        <?php
			$ini=date("Y");
			$fin=$ini-150;
			for($i=$ini;$i>$fin;$i--)
			{
				echo "<option value='$i'>$i</option>";
			}
		?></select>
      </span>      </td>
<td width="50"></td>
  </tr>
  <tr>
      <td height="25" class="textosParaInputs" align="right"><label for="direccionAgregar" class="error"><span class="textosParaInputs">DIRECCIÓN:</span></label></td>
    <td align="left"><input name="direccionAgregar" type="text" id="direccionAgregar" size="64" maxlength="50" onkeyup="this.value = this.value.toUpperCase();" tabindex="11" /></td>
  </tr>
  <tr><td></td><td></td></tr>
  <tr>
    <td height="25" class="textosParaInputs" align="right">ESTADO:</td>
    <td align="left"><select name="estadoAgregar" id="estadoAgregar" tabindex="12" onchange="javascript: cargarMunicipios(this.value,'municipioAgregar');">
      <script type="text/javascript">
                cargarEstados('estadoAgregar','-1');
            </script>
    </select>
      <span class="textosParaInputs">MUNICIPIO: </span>
      <select name="municipioAgregar" id="municipioAgregar" tabindex="13" onchange="obtenerCodigoPostalMun('cp','estadoAgregar',this.id);cargarColonias('col','cp', 'municipioAgregar');">
      </select></td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right"> CODIGO POSTAL</td>
    <td><select name="cp" id="cp" tabindex="14" onchange="cargarColonias('col',this.id, 'municipioAgregar')"  value="0" >
    </select>
      <span class="textosParaInputs">COLONIA</span>
<select name="col" id="col" tabindex="15" onchange="obtenerCodigoPostal('cp','estadoAgregar','municipioAgregar',this.id);">
</select></td>
      </tr>
  <tr>
    <td><span class="textosParaInputs">UNIDAD MEDICA DE ADSCRIPCIÓN:</span></td>
    <td align="left"><select name="Unidad" id="Unidad" tabindex="16">
    </select></td>
    </tr>
        <tr>
          <td class="textosParaInputs" align="right">E-MAIL:</td>
          <td align="left"><input name="emailAgregar" type="text" id="emailAgregar" size="30" tabindex="17" />
            <span class="textosParaInputs">CELULAR</span>
            <input type="text" name="cel" value="" maxlength="10" size="10" id="cel" tabindex="18" /></td>
          </tr>
        
            <tr>
              <td class="textosParaInputs" align="right">&nbsp;</td>
              <td align="left">&nbsp;</td>
              </tr>
        <tr>
          <td colspan="2" align="center"><div id="divBotones_EstadoAgregarDH">
            <button id="enviar" type="button" class="botones" onclick="">Regresar</button>
            &nbsp;&nbsp;
            <input name="" type="button" class="botones" id="guardar" tabindex="19" onclick="javascript:validarAgregarDH();" value="Agregar" />
          </div></td>
        </tr>
            <tr>
              <td colspan="2" align="center"><div id="resultado"></div></td>
            </tr>
</table></form>
</body>
</html>