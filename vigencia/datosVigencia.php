<?php
session_start();
$id_derecho=$_REQUEST['id_derecho']; 
require_once('Connections/bdissste.php');
require_once('Connections/bdExpedientes.php');
require_once('lib/misFunciones.php');
mysql_connect($hostname_bdissste,$username_bdissste,$password_bdissste);
mysql_select_db($database_bdissste);
$query="select * from derechohabientes where id_derecho=".$id_derecho;
$derecho=  ejecutarSQL($query);
if(count($derecho)>0)
{
	$unimed=  UnidadMedicaXid($derecho['unidad_medica']);
	if(count($unimed)>0)
	$nomClin=$unimed['nombre'];
	else
	$nomClin="Clinica no asignada";
	mysql_select_db($database_bdExpedientes);
	$query="select * from expedientes where id_expediente=".$derecho['id_derecho'];
	$sqlExp=mysql_query($query);
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="lib/misEstilos.css" rel="stylesheet" type="text/css">
</head>

<body>
<table width="700" border="0" class="tablaPrincipal">
  <tr>
    <td class="tituloVentana" colspan="5">Informaci&oacute;n del Derecho Habiente</td>
    
  </tr>
  <tr>
    <td colspan="5"><?php if($_SESSION['tipoUsuario']!=3) {?><input type="button" value="Modificar Datos" onClick="modificarDerecho('citas','<?php echo $id_derecho; ?>')" ><?php } ?></td>
  </tr>
  <tr>
    <td width="338" class="textosParaInputs" align="right">CEDULA:</td>
    <td colspan="3" align="left"><?php echo $derecho['cedula']."/".$derecho['cedula_tipo']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">NOMBRE:</td>
    <td colspan="3" align="left"><?php echo ponerAcentos($derecho['ap_p']." ".$derecho['ap_m']." ".$derecho['nombres']); ?>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">FECHA DE NACIMIENTO</td>
    <td width="131"><?php echo $derecho['fecha_nacimiento']; ?>&nbsp;</td>
    <td width="112" class="textosParaInputs">TELEFONO</td>
    <td width="99"><?php echo $derecho['telefono']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td class="textosParaInputs" align="right">DIRECCION</td>
    <td colspan="3" align="left"></span>&nbsp;<?php echo ponerAcentos($derecho['direccion']." Codigo Postal ".$derecho['Codigo_Postal']); ?></td>
  </tr>
  <tr>
    <td align="right" class="textosParaInputs">E-MAIL</td>
    <td colspan="3"><?php echo $derecho['email']; ?>&nbsp;</td>
  </tr>
  <tr>
    <td align="right" class="textosParaInputs">ESTADO</td>
    <td><?php echo ponerAcentos($derecho['estado']); ?>&nbsp;</td>
    <td align="right" class="textosParaInputs">MUNICIPIO</td>
    <td><?php echo ponerAcentos($derecho['municipio']); ?>&nbsp;</td>
  </tr>
  <tr>
    <td align="right" class="textosParaInputs">UNIDAD MEDICA DE PROCEDENCIA</td>
    <td colspan="3"><?php 
	echo htmlentities ($nomClin); ?>&nbsp;</td>
  </tr>
  <tr>
    <td align="right" class="textosParaInputs">ESTATUS</td>
    <td colspan="3"><?php if($derecho['st']==1)
	echo "Vigente";
	else
	echo "No Vigente";  ?></td>
  </tr>
  <tr><td class="textosParaInputs" align="right">
  CELULAR
  </td><td><?php echo $derecho['celular']; ?></td></tr>
  <tr>
    <td colspan="5">&nbsp;</td>
    
  </tr>
  <tr>
    <td colspan="5" class="tituloVentana">Informacion del Expediente</td>
  </tr>
  <tr>
  <td colspan="5">
  <?php
  if(mysql_errno()==0){ 
  if(mysql_num_rows($sqlExp)>0)
  {
	  $expediente=mysql_fetch_array($sqlExp);
	  ?>
     <table width="100%">
     <tr><td class="subtituloEncabezado">Status</td>
     <td colspan="2">Expediente Creado</td>
     </tr>
     <tr>
     <td class="subtituloEncabezado">Imprimir Formatos</td>
     <td><?php echo ponerCeros($expediente['id_expediente'],8); ?></td><td><input type="button" name="imp_<?php echo $expediente["id_expediente"]?>" id="imp_<?php echo$expediente["id_expediente"]?>" onclick="javascript: expedientesImprimir('<?php echo $expediente["id_expediente"];?>');" value="Etiqueta carnet">
       <input type="button" name="imp_<?php echo$expediente["id_expediente"]?>" id="imp_<?php echo$expediente["id_expediente"]?>2" onclick="window.open('verEtiquetaExpediente.php?id_derecho='+<?php echo $expediente["id_expediente"];?>);" value="Etiqueta Expediente">
       <input type="button" name="imp_<?php echo$expediente["id_expediente"]?>2" id="imp_<?php echo $expediente["id_expediente"]?>3" onclick="javascript:ImprimirExpediente(<?php echo $expediente["id_expediente"]; ?>);"" value="Formato Expediente"></td></tr></table>
      <?php
  }
  else
  {?>
  <table width="100%">
     <tr><td class="subtituloEncabezado">Status</td>
     <td colspan="2">Expediente No Creado</td>
     </tr>
     <tr>
     <td class="subtituloEncabezado">Imprimir Formatos</td>
     <td><?php if(isset($expediente))
	 echo ponerCeros($expediente['id_expediente'],8); ?></td><td><input type="button" name="imp_<?php echo $derecho["id_derecho"]?>" id="imp_<?php echo $derecho["id_derecho"]?>" onclick="javascript: crearExpediente('<?php echo $derecho["id_derecho"];?>');" value="Crear Expediente"></td></tr></table>
  <?php }
  }
  else{?>
  <table width="100%">
     <tr><td class="subtituloEncabezado">Status</td>
     <td colspan="2">Expediente No Creado</td>
     </tr>
     <tr>
     <td class="subtituloEncabezado">Imprimir Formatos</td>
     <td><?php echo ponerCeros($derecho['id_derecho'],8); ?></td><td><input type="button" name="imp_<?php echo $derecho["id_derecho"]?>" id="imp_<?php echo $derecho["id_derecho"]?>" onclick="javascript: crearExpediente('<?php echo $derecho["id_derecho"];?>');" value="Crear Expediente"></td></tr></table>
  <?php }
  ?><div id='expediente'></div></td></tr>
</table>
</body>
</html>
