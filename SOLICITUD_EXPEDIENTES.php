<?php
session_start ();
include_once('lib/misFunciones.php');
include_once('lib/misFuncionesExpedientes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Ficha para Archivo</title>
<style type="text/css">
	@import url("lib/impresion.css") print;
	@import url("lib/reportes.css") screen;
</style>
</head>
</head>

<body onload="window.print();">

<?php

$id_solicitud = 1;
$resp = "";
switch ($_GET['tipoReporte']) {
// reporte por medico
	case 'medico':
		$resp .= getSolicitudExpedientes($id_solicitud, $_GET['idConsultorio'], $_GET['idServicio'], $_GET['idMedico'], $_GET['idUsuario'], $_GET['tipoReporte'], $_GET['fechaI'], $_GET['fechaF'], "");
		break;
// reporte por servicio
	case 'servicio':
		$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$_GET['idServicio']);
		$totalMedicos = count($medicos);
		for ($x=0; $x<$totalMedicos; $x++) {
			$resp .= getSolicitudExpedientes($id_solicitud, $_GET['idConsultorio'], $_GET['idServicio'], $medicos[$x]['id_medico'], $_GET['idUsuario'], $_GET['tipoReporte'], $_GET['fechaI'], $_GET['fechaF'], "<H1 class=\"SaltoDePagina\"> </H1>");
		}
		break;
// reporte por consultorio
	case 'consultorio':
		$servicios = getServiciosXConsultorio($_GET['idConsultorio']);
		$totalServicios = count($servicios);
		for ($y=0; $y<$totalServicios; $y++) {
			$medicos = getMedicosXServicioXConsultorio($_GET['idConsultorio'],$servicios[$y]['id_servicio']);
			$totalMedicos = count($medicos);
			for ($x=0; $x<$totalMedicos; $x++) {
				$resp .= getSolicitudExpedientes($id_solicitud, $_GET['idConsultorio'], $servicios[$y]['id_servicio'], $medicos[$x]['id_medico'], $_GET['idUsuario'], $_GET['tipoReporte'], $_GET['fechaI'], $_GET['fechaF'], "<H1 class=\"SaltoDePagina\"> </H1>");
			}
		}
		break;
// reporte por unidad
	case 'unidad':
		$consultorios = getConsultorios();
		$totalConsultorios = count($consultorios);
		for($z=0;$z<$totalConsultorios; $z++) {
			$servicios = getServiciosXConsultorio($consultorios[$z]['id_consultorio']);
			$totalServicios = count($servicios);
			for ($y=0; $y<$totalServicios; $y++) {
				$medicos = getMedicosXServicioXConsultorio($consultorios[$z]['id_consultorio'],$servicios[$y]['id_servicio']);
				$totalMedicos = count($medicos);
				for ($x=0; $x<$totalMedicos; $x++) {
					$resp .= getSolicitudExpedientes($id_solicitud, $consultorios[$z]['id_consultorio'], $servicios[$y]['id_servicio'], $medicos[$x]['id_medico'], $_GET['idUsuario'], $_GET['tipoReporte'], $_GET['fechaI'], $_GET['fechaF'], "<H1 class=\"SaltoDePagina\"> </H1>");
				}
			}
		}
		break;
}

echo $resp;

?>
</body>
</html>
